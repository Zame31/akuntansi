<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
Route::get('/home','DashboardController@index')->name('home');
Route::get( '/', ['as'=>'home', 'uses'=> 'DashboardController@index']);
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::post('reset_password', 'Auth\LoginController@reset_password')->name('reset_password');
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// DASHBOARD
Route::get('data/dashboard/{id}', 'DashboardController@data')->name('data.dashboard');

Route::group(['middleware' => 'auth'], function () {

Route::get('getMenu','Controller@getMenu')->name('role.getMenu');


Route::post('user/changePassword', 'DashboardController@changePassword')->name('user.changePassword');


//ref

Route::get('/set_end_of_day','DashboardController@set_end_of_day');
Route::get('/coa','DashboardController@coa');
Route::get('/ref_dokumen','DashboardController@dok');
Route::get('/dbcr','DashboardController@dbcr');
Route::get('/fee','DashboardController@fee');
Route::get('/fee1','DashboardController@fee1');
Route::get('/refPolis','DashboardController@refPolis');
Route::get('/byPolis','DashboardController@byPolis');
Route::get('/cekPolis','DashboardController@cekPolis');
Route::get('/cekSalesPolis','DashboardController@cekSalesPolis');

Route::get('/history_claim','DashboardController@history_claim');
Route::get('/history_notes','DashboardController@history_notes');



Route::get('/endofyear','DashboardController@endofyear');





Route::get('/cek_email','DashboardController@cek_email');
Route::get('/province/{id}','DashboardController@ref_province');



Route::prefix('ref')->group(function () {
    // Index Reff
    Route::get('agent_type','reffController@refIndex')->name('agent_type');
    Route::get('bank_account','reffController@refIndex')->name('bank_account');
    Route::get('city','reffController@refIndex')->name('city');
    Route::get('coa_group','reffController@refIndex')->name('coa_group');
    Route::get('coa_type','reffController@refIndex')->name('coa_type');
    Route::get('customer_segment','reffController@refIndex')->name('customer_segment');
    Route::get('customer_type','reffController@refIndex')->name('customer_type');
    Route::get('doc_type','reffController@refIndex')->name('doc_type');
    Route::get('paid_status','reffController@refIndex')->name('paid_status');
    Route::get('product','reffController@refIndex')->name('product');
    Route::get('proposal','reffController@refIndex')->name('proposal');
    Route::get('province','reffController@refIndex')->name('province');
    Route::get('quotation','reffController@refIndex')->name('quotation');
    Route::get('segment','reffController@refIndex')->name('segment');
    Route::get('tx_type','reffController@refIndex')->name('tx_type');
    Route::get('user_role','reffController@refIndex')->name('user_role');
    Route::get('workflow','reffController@refIndex')->name('workflow');
    Route::get('master_company','reffController@refIndex')->name('master_company');
    Route::get('master_branch','reffController@refIndex')->name('master_branch');
    Route::get('master_user','reffController@refIndex')->name('master_user');
    Route::get('master_config','reffController@refIndex')->name('master_config');
    Route::get('master_workflow','reffController@refIndex')->name('master_workflow');
    Route::get('master_user','reffController@refIndex')->name('master_user');
    Route::get('inventory','reffController@refIndex')->name('inventory');
    Route::get('master_coa','reffController@refIndex')->name('master_coa');
    Route::get('bdd','reffController@refIndex')->name('bdd');
    Route::get('customer_group','reffController@refIndex')->name('customer_group');
    Route::get('agent','reffController@refIndex')->name('agent');
    Route::get('branch_monitoring','reffController@refIndex')->name('branch_monitoring');
    Route::get('menu','reffController@refIndex')->name('menu');
    Route::get('sub_menu','reffController@refIndex')->name('sub_menu');
    Route::get('manajemen_user','reffController@refIndex')->name('manajemen_user');
    Route::get('underwriter','reffController@refIndex')->name('underwriter');
    Route::get('qs_content','reffController@refIndex')->name('qs_content');
    Route::get('qs_content/action/{act}/{id?}','reffController@createQsContent')->name('qs_content.action', ['act' => 'act']);

    Route::get('branch_customer','reffController@refIndex')->name('branch_customer');
    Route::get('insured_detail','reffController@refIndex')->name('insured_detail');

    Route::get('insured_detail/change_data_selected','reffController@changeInsuredDetail')->name('insured_detail.change_data_selected');

    Route::get('rate_usia','reffController@refIndex')->name('rate_usia');
    Route::get('rate_usia/getData','reffController@getDataRefUsia')->name('rate_usia.get_data');


    Route::get('master_coa/history_tx/exportExcel','reffController@exportHistoryTxExcel');
    Route::get('master_coa/history_tx/ExportPdf','reffController@exportHistoryTx');
    Route::get('master_coa/history_tx/{id}','reffController@showHistoryTx');
    Route::post('master_coa/history_tx/getHistory','reffController@getHistoryTx')->name('master_coa.search_history_tx');

    // Export History Tx



    // get province
    Route::get('master_company/get_city','reffController@getCity')->name('master_company.get_city');
    Route::get('bdd/get_branch','reffController@getBranch')->name('bdd.get_branch');
    Route::get('bdd/get_coa','reffController@getCOAForBDD')->name('bdd.get_coa');
    Route::get('bank_account/get_branch','reffController@getBranchBankAccount')->name('bank_account.get_branch');

    Route::get('bank_account/get_coa_no','reffController@getCoaNo')->name('bank_account.get_coaNo');
    Route::get('master_coa/get_company','reffController@getCompany')->name('master_coa.get_company');
    Route::get('master_coa/get_coa_parent','reffController@getCOAParent')->name('master_coa.get_coa_parent');



    //Actif Reff
    Route::get('agent_type/active','reffController@refSetActive')->name('agent_type.setActive');
    Route::get('bank_account/active','reffController@refSetActive')->name('bank_account.setActive');
    Route::get('user_role/active','reffController@refSetActive')->name('user_role.setActive');
    Route::get('coa_group/active','reffController@refSetActive')->name('coa_group.setActive');
    Route::get('coa_type/active','reffController@refSetActive')->name('coa_type.setActive');
    Route::get('segment/active','reffController@refSetActive')->name('segment.setActive');
    Route::get('customer_type/active','reffController@refSetActive')->name('customer_type.setActive');
    Route::get('customer_segment/active','reffController@refSetActive')->name('customer_segment.setActive');
    Route::get('paid_status/active','reffController@refSetActive')->name('paid_status.setActive');
    Route::get('workflow/active','reffController@refSetActive')->name('workflow.setActive');
    Route::get('tx_type/active','reffController@refSetActive')->name('tx_type.setActive');
    Route::get('master_company/active','reffController@refSetActive')->name('master_company.setActive');
    Route::get('master_branch/active','reffController@refSetActive')->name('master_branch.setActive');
    Route::get('master_config/active','reffController@refSetActive')->name('master_config.setActive');
    Route::get('master_user/active','reffController@refSetActive')->name('master_user.setActive');
    Route::get('quotation/active','reffController@refSetActive')->name('quotation.setActive');
    Route::get('proposal/active','reffController@refSetActive')->name('proposal.setActive');
    Route::get('inventory/active','reffController@refSetActive')->name('inventory.setActive');
    Route::get('master_coa/active','reffController@refSetActive')->name('master_coa.setActive');
    Route::get('customer_group/active','reffController@refSetActive')->name('customer_group.setActive');
    Route::get('bdd/active','reffController@refSetActive')->name('bdd.setActive');
    Route::get('agent/active','reffController@refSetActive')->name('agent.setActive');
    Route::get('product/active','reffController@refSetActive')->name('product.setActive');
    Route::get('underwriter/active','reffController@refSetActive')->name('underwriter.setActive');
    Route::get('qs_content/active','reffController@refSetActive')->name('qs_content.setActive');

    Route::get('branch_customer/active','reffController@refSetActive')->name('branch_customer.setActive');
    Route::get('insured_detail/active','reffController@refSetActive')->name('insured_detail.setActive');





    Route::get('master_branch/open','reffController@setBranchOpen')->name('master_branch.setOpen');






    //delete reff
    Route::get('city/delete','reffController@refDelete')->name('city.delete');
    Route::get('province/delete','reffController@refDelete')->name('province.delete');
    Route::get('master_workflow/delete','reffController@refDelete')->name('master_workflow.delete');
    Route::get('menu/delete','reffController@refDelete')->name('menu.delete');
    Route::get('sub_menu/delete','reffController@refDelete')->name('sub_menu.delete');
    Route::get('manajemen_user/delete','reffController@refDelete')->name('manajemen_user.delete');
    Route::get('insured_detail/delete','reffController@refDelete')->name('insured_detail.delete');
    Route::get('rate_usia/delete', 'reffController@refDelete')->name('rate_usia.delete');

    //store
    Route::post('agent_type/store','reffController@refStore')->name('agent_type.store');
    Route::post('bank_account/store','reffController@refStore')->name('bank_account.store');
    Route::post('user_role/store','reffController@refStore')->name('user_role.store');
    Route::post('coa_group/store','reffController@refStore')->name('coa_group.store');
    Route::post('coa_type/store','reffController@refStore')->name('coa_type.store');
    Route::post('segment/store','reffController@refStore')->name('segment.store');
    Route::post('customer_type/store','reffController@refStore')->name('customer_type.store');
    Route::post('customer_segment/store','reffController@refStore')->name('customer_segment.store');
    Route::post('paid_status/store','reffController@refStore')->name('paid_status.store');
    Route::post('workflow/store','reffController@refStore')->name('workflow.store');
    Route::post('city/store','reffController@refCityStore')->name('city.store');
    Route::post('province/store','reffController@refProvinceStore')->name('province.store');
    Route::post('tx_type/store','reffController@refStore')->name('tx_type.store');
    Route::post('master_company/store','reffController@refStore')->name('master_company.store');
    Route::post('master_branch/store','reffController@refStore')->name('master_branch.store');
    Route::post('master_config/store','reffController@refStore')->name('master_config.store');
    Route::post('master_user/store','reffController@refStore')->name('master_user.store');
    Route::post('quotation/store','reffController@refStore')->name('quotation.store');
    Route::post('proposal/store','reffController@refStore')->name('proposal.store');
    Route::post('master_workflow/store','reffController@refStore')->name('master_workflow.store');
    Route::post('master_workflow/store','reffController@refStore')->name('master_workflow.store');
    Route::post('master_coa/store','reffController@refStore')->name('master_coa.store');
    Route::post('customer_group/store','reffController@refStore')->name('customer_group.store');
    Route::post('bdd/store','reffController@refStore')->name('bdd.store');
    Route::post('ref_inventory/store','reffController@refStore')->name('ref_inventory.store');
    Route::post('agent/store','reffController@refStore')->name('agent.store');
    Route::post('menu/store','reffController@refStore')->name('menu.store');
    Route::post('sub_menu/store','reffController@refStore')->name('sub_menu.store');
    Route::post('manajemen_user/store','reffController@refStore')->name('manajemen_user.store');
    Route::post('product/store','reffController@refStore')->name('product.store');
    Route::post('underwriter/store','reffController@refStore')->name('underwriter.store');
    Route::post('qs_content/store','reffController@refStore')->name('qs_content.store');
    Route::post('rate_usia/store','reffController@refStore')->name('rate_usia.store');

    Route::post('branch_customer/store','reffController@refStore')->name('branch_customer.store');
    Route::post('insured_detail/store','reffController@refStore')->name('insured_detail.store');
    Route::post('insured_detail/store_single_input','reffController@storeInsuredSingle')->name('insured_detail.store_single_input');

    Route::get('insured_detail/create','reffController@createInsuredDetail')->name('insured_detail.create');




    // insert coa parent store
    Route::get('master_coa/store_parent_coa','reffController@storeParentCOA')->name('master_coa.parent_coa_create');
    Route::get('master_coa/cheack_coa_parent','reffController@checkCOAParent')->name('master_coa.check_coa_parent');





    // show edit
    Route::get('agent_type/{id}','reffController@refShowEdit')->name('agent_type.show', ['id' => 'id']);
    Route::get('bank_account/{id}','reffController@refShowEdit')->name('bank_account.show', ['id' => 'id']);
    Route::get('user_role/{id}','reffController@refShowEdit')->name('user_role.show', ['id' => 'id']);
    Route::get('coa_group/{id}','reffController@refShowEdit')->name('coa_group.show', ['id' => 'id']);
    Route::get('coa_type/{id}','reffController@refShowEdit')->name('coa_type.show', ['id' => 'id']);
    Route::get('segment/{id}','reffController@refShowEdit')->name('segment.show', ['id' => 'id']);
    Route::get('customer_type/{id}','reffController@refShowEdit')->name('customer_type.show', ['id' => 'id']);
    Route::get('customer_segment/{id}','reffController@refShowEdit')->name('customer_segment.show', ['id' => 'id']);
    Route::get('paid_status/{id}','reffController@refShowEdit')->name('paid_status.show', ['id' => 'id']);
    Route::get('workflow/{id}','reffController@refShowEdit')->name('workflow.show', ['id' => 'id']);
    Route::get('city/{id}','reffController@refShowEdit')->name('city.show', ['id' => 'id']);
    Route::get('province/{id}','reffController@refShowEdit')->name('province.show', ['id' => 'id']);
    Route::get('tx_type/{id}','reffController@refShowEdit')->name('tx_type.show', ['id' => 'id']);
    Route::get('master_company/{id}','reffController@refShowEdit')->name('master_company.show', ['id' => 'id']);
    Route::get('master_branch/{id}','reffController@refShowEdit')->name('master_branch.show', ['id' => 'id']);
    Route::get('master_config/{id}','reffController@refShowEdit')->name('master_config.show', ['id' => 'id']);
    Route::get('master_user/{id}','reffController@refShowEdit')->name('master_user.show', ['id' => 'id']);
    Route::get('quotation/{id}','reffController@refShowEdit')->name('quotation.show', ['id' => 'id']);
    Route::get('proposal/{id}','reffController@refShowEdit')->name('proposal.show', ['id' => 'id']);
    Route::get('master_workflow/{id}','reffController@refShowEdit')->name('master_workflow.show', ['id' => 'id']);
    Route::get('inventory/{id}','reffController@refShowEdit')->name('inventory.show', ['id' => 'id']);
    Route::get('master_coa/{id}','reffController@refShowEdit')->name('master_coa.show', ['id' => 'id']);
    Route::get('customer_group/{id}','reffController@refShowEdit')->name('customer_group.show', ['id' => 'id']);
    Route::get('bdd/{id}','reffController@refShowEdit')->name('bdd.show', ['id' => 'id']);
    Route::get('agent/{id}','reffController@refShowEdit')->name('agent.show', ['id' => 'id']);
    Route::get('menu/{id}','reffController@refShowEdit')->name('menu.show', ['id' => 'id']);
    Route::get('sub_menu/{id}','reffController@refShowEdit')->name('sub_menu.show', ['id' => 'id']);
    Route::get('manajemen_user/{id}','reffController@refShowEdit')->name('manajemen_user.show', ['id' => 'id']);
    Route::get('product/{id}','reffController@refShowEdit')->name('product.show', ['id' => 'id']);
    Route::get('underwriter/{id}','reffController@refShowEdit')->name('underwriter.show', ['id' => 'id']);

    Route::get('branch_customer/{id}','reffController@refShowEdit')->name('branch_customer.show', ['id' => 'id']);
    Route::get('insured_detail/{id}','reffController@refShowEdit')->name('insured_detail.show', ['id' => 'id']);





    // reset password
    Route::get('master_user/reset_password/{id}','reffController@resetPassUser')->name('master_user.reset_password', ['id' => 'id']);

    Route::get('insured_detail/product/{id}','reffController@editInsuredDetail')->name('insured_detail.show_product', ['id' => 'id']);


});



// ACCOUNTING
Route::get('/accouting/single', 'TransaksiController@transaksi_single')->name('transaksi_single');
Route::get('/accouting/list_reversal', 'TransaksiController@list_reversal')->name('list_reversal');
Route::get('/accouting/list_trx_success', 'TransaksiController@list_trx_success')->name('list_trx_success');
Route::get('/accouting/list_rev_success', 'TransaksiController@list_rev_success')->name('list_rev_success');
Route::get('/accouting/list_overdue', 'TransaksiController@list_overdue')->name('list_overdue');


Route::resource('transaksi', 'TransaksiController');
Route::get('/transaksi_baru', 'TransaksiController@transaksi_baru')->name('transaksi_baru');
Route::post( 'insert_tr_baru', ['as'=>'insert_tr_baru', 'uses'=> 'TransaksiController@insert_tr_baru']);
Route::get('/list_tr_baru', 'TransaksiController@list_tr_baru')->name('list_tr_baru');
Route::post('/kirimAproval', 'TransaksiController@kirimAproval');
Route::post('/hapusAproval', 'TransaksiController@hapusAproval');

Route::get('/transaksi_detail', 'TransaksiController@transaksi_detail')->name('transaksi_detail');
Route::get('/transaksi_detail/ExportPdf', 'TransaksiController@export_pdf_transaksi_detail')->name('export_pdf_transaksi_detail');

Route::get('/inventory/ExportPdf', 'TransaksiController@export_pdf_inventory')->name('export_pdf_inventory');
Route::get('/bdd/ExportPdf', 'TransaksiController@export_pdf_bdd')->name('export_pdf_bdd');

Route::get('/inventory/ExportExcel', 'TransaksiController@export_excel_inventory')->name('export_excel_inventory');
Route::get('/bdd/ExportExcel', 'TransaksiController@export_excel_bdd')->name('export_excel_bdd');


Route::post('kirim_reversal', 'TransaksiController@kirim_reversal');

Route::post('/kirim_approve_reversal', 'TransaksiController@kirim_approve_reversal');
Route::post('/hapus_approve_reversal', 'TransaksiController@hapus_approve_reversal');




Route::get('/list_app_baru', 'ApprovalController@list_app_baru')->name('list_app_baru');
Route::get('Approval/detail', 'ApprovalController@detail_app');
Route::post('kirimAprove', 'ApprovalController@kirimAproval');
Route::post('hapusAprove', 'ApprovalController@hapusAproval');




// CUSTOMER
Route::get('/customer_baru', 'CustomerController@customer_baru')->name('customer_baru');
Route::get('/customer_list', 'CustomerController@customer_list')->name('customer_list');
Route::get('/customer_approval', 'CustomerController@customer_approval')->name('customer_approval');
Route::get('/customer_list_active', 'CustomerController@customer_list_active')->name('customer_list_active');
Route::get('/get_data_customer', 'CustomerController@get_data_customer')->name('c.get_data_customer');


// TAMBAHAN
Route::get('/customer_list_non_active', 'CustomerController@customer_list_non_active')->name('customer_list_non_active');
Route::get('/customer_detail_active/{id}', 'CustomerController@customer_detail_active')->name('customer_detail_active');
Route::get('customer/set_active','CustomerController@setActive')->name('customer.set_active');
Route::get('/customer_detail_non_active/{id}', 'CustomerController@customer_detail_non_active')->name('customer_detail_non_active');

Route::post( 'insert_customer_baru', ['as'=>'insert_customer_baru', 'uses'=> 'CustomerController@insert_customer_baru']);
Route::post('/kirimAproval_cust', 'CustomerController@kirimAproval');
Route::post('/hapusAproval_cust', 'CustomerController@hapusAproval');
Route::get('/edit_customer', 'CustomerController@edit_customer');
Route::post( 'update_customer_baru', ['as'=>'update_customer_baru', 'uses'=> 'CustomerController@update_customer_baru']);
Route::get('/det_cust', 'CustomerController@det_cust');
Route::post('/kirimAproval_app', 'CustomerController@kirimAproval_app');
Route::post('/hapusAproval_app', 'CustomerController@hapusAproval_app');



// INVOICE
Route::get('/get_polis','InvoiceController@get_polis');
Route::post('/get_data_payment','InvoiceController@get_data_payment')->name('invoice.get_data_payment');


Route::get('/list_reversal_payment', 'InvoiceController@list_reversal_payment')->name('list_reversal_payment');
Route::get('/list_reversal_split', 'InvoiceController@list_reversal_split')->name('list_reversal_split');

Route::get('/approval_split_payment', 'InvoiceController@approval_split_payment')->name('approval_split_payment');
Route::get('/complete_payment', 'InvoiceController@complete_payment')->name('complete_payment');
Route::get('/invoice/baru', 'InvoiceController@invoice_baru')->name('invoice_baru');
Route::get('/invoice/list', 'InvoiceController@invoice_list')->name('invoice_list');
Route::get('/invoice/approval', 'InvoiceController@invoice_approval')->name('invoice_approval');
Route::get('/invoice/approval/payment', 'InvoiceController@invoice_approval_payment')->name('invoice_approval_payment');
Route::get('/invoice/paid', 'InvoiceController@invoice_paid')->name('invoice_paid');
Route::get('/invoice/dropped', 'InvoiceController@invoice_dropped')->name('invoice_dropped');
Route::get('/invoice/active', 'InvoiceController@invoice_active')->name('invoice_active');
Route::post( 'insert_invoice_baru', ['as'=>'insert_invoice_baru', 'uses'=> 'InvoiceController@insert_invoice_baru']);

// Tambahan
Route::post( 'insert_invoice_baru_endorsement', ['as'=>'insert_invoice_baru_endorsement', 'uses'=> 'InvoiceController@insert_invoice_baru_endorsement']);
Route::post( 'insert_invoice_baru_installment', ['as'=>'insert_invoice_baru_installment', 'uses'=> 'InvoiceController@insert_invoice_baru_installment']);




Route::post( 'update_invoice_baru', ['as'=>'update_invoice_baru', 'uses'=> 'InvoiceController@update_invoice_baru']);

Route::post('/kirimAproval_invoice', 'InvoiceController@kirimAproval_invoice');
Route::post('/hapusAproval_invoice', 'InvoiceController@hapusAproval_invoice');
Route::get('edit_invoice', 'InvoiceController@edit_invoice');
Route::get('edit_endorse', 'InvoiceController@edit_endorse');


Route::post('/kirimAproval_reversal', 'InvoiceController@kirimAproval_reversal');
Route::post('/hapusAproval_reversal', 'InvoiceController@hapusAproval_reversal');

Route::post('/kirimAproval_reversal1', 'InvoiceController@kirimAproval_reversal1');
Route::post('/hapusAproval_reversal1', 'InvoiceController@hapusAproval_reversal1');


Route::post('/kirimAproval_invoice1', 'InvoiceController@kirimAproval_invoice1');
Route::post('/hapusAproval_invoice1', 'InvoiceController@hapusAproval_invoice1');

Route::get('detail_invoice', 'InvoiceController@detail_invoice');
Route::get('detail_invoice_schedule', 'InvoiceController@detail_invoice_schedule');

Route::post('/kirimAproval_invoice_active', 'InvoiceController@kirimAproval_invoice_active');
Route::post('/hapusAproval_invoice_active', 'InvoiceController@hapusAproval_invoice_active');


Route::post('/split_paid', 'InvoiceController@split_paid');

Route::post('/reversal_payment', 'InvoiceController@reversal_payment');

Route::post('/kirimAproval_app_split', 'InvoiceController@kirimAproval_app_split');
Route::post('/hapusAproval_app_split', 'InvoiceController@hapusAproval_app_split');


Route::post('/kirimAproval_invoice_paid', 'InvoiceController@kirimAproval_invoice_paid');
Route::post('/hapusAproval_invoice_paid', 'InvoiceController@hapusAproval_invoice_paid');

Route::post('/kirimAproval_invoice_paid1', 'InvoiceController@kirimAproval_invoice_paid1');
Route::post('/hapusAproval_invoice_paid1', 'InvoiceController@hapusAproval_invoice_paid1');


Route::post('/drop_selected', 'InvoiceController@drop_selected');

Route::get('/endorse_baru', 'InvoiceController@endorse_baru')->name('endorse_baru');
Route::get('/installment_baru', 'InvoiceController@installment_baru')->name('installment_baru');


//REPORT
Route::get('/report/balance_sheet', 'ReportController@balance_sheet')->name('balance_sheet');
Route::get('/report/laba_rugi', 'ReportController@laba_rugi')->name('laba_rugi');
Route::get('/report/cashflow', 'ReportController@cashflow')->name('cashflow');
Route::get('/report/cashflow/detail', 'ReportController@cashflow_detail')->name('cashflow.detail');

Route::get('/report/sales_product', 'ReportController@sales_product')->name('sales_product');
Route::get('/report/sales_branch', 'ReportController@sales_branch')->name('sales_branch');
Route::get('/report/sales_officer', 'ReportController@sales_officer')->name('sales_officer');

// Tambahan REPORT
Route::get('/report/nominative_report', 'ReportController@nominativeReport')->name('nominative_report');
Route::get('/report/nominative_report/export', 'ReportController@exportNominativeReport')->name('export.nominative_report');

Route::get('/report/due_date_installment', 'ReportController@dueDateInstallment')->name('due_date_installment');
Route::get('/report/due_date_installment/export', 'ReportController@exportDueDateInstallment')->name('export.due_date_installment');

Route::get('/report/qs_report/{type}', 'ReportController@qsReport')->name('qs_report');
Route::get('/report/qs_report_export', 'ReportController@qsReport_export')->name('export.qs_report');

Route::get('/report/renewal_list_report', 'ReportController@renewal_list_report')->name('renewal_list_report');
Route::post('/report/renewal_list_report/data', 'ReportController@data_renewal_list_report')->name('data_renewal_list_report');
Route::get('/report/renewal_list_report/export', 'ReportController@export_renewal_list_report')->name('export_renewal_list_report');

Route::get('/report/renewal_list_due_date', 'ReportController@renewal_list_due_date')->name('renewal_list_due_date');
Route::post('/report/renewal_list_due_date/data', 'ReportController@data_renewal_list_due_date')->name('data_renewal_list_due_date');
Route::get('/report/renewal_list_due_date/export', 'ReportController@export_renewal_list_due_date')->name('export_renewal_list_due_date');


Route::get('/report/claim', 'ReportController@claimReport')->name('claim_report');
Route::get('/report/claim/export', 'ReportController@exportClaimReport')->name('export.claim_report');


Route::get('/report/download_laporan', 'ReportController@download_laporan')->name('download_laporan.list');

Route::get('/report/debit_note_pending_split_payment', 'ReportController@invoicePendingSplitPayment')->name('invoice_pending_split_payment');
Route::get('/report/debit_note_pending_split_payment/export', 'ReportController@exportInvoicePendingSplitPayment')->name('export.invoice_pending_split_payment');

Route::get('/report/installment_pending_split_payment', 'ReportController@installmentPendingSplitPayment')->name('installment_pending_split_payment');
Route::get('/report/installment_pending_split_payment/export', 'ReportController@exportInstallmentPendingSplitPayment')->name('export.installment_pending_split_payment');

Route::get('/report/detail_product_export', 'ReportController@detail_product_export')->name('export.detail_product_export');
Route::post('/report/detail_product_import', 'ReportController@detail_product_import')->name('import.detail_product_import');
Route::post('/report/table_detail_product_export', 'ReportController@table_detail_product_export')->name('export.table_detail_product_export');



Route::get('/report/sales_customer_segment', 'ReportController@salesCustomerSegment')->name('sales_customer_segment');
Route::get('/report/sales_company', 'ReportController@salesCompany')->name('sales_company');

// lvl 0
Route::get('/report/sales_officer_parent', 'ReportController@salesOfficer')->name('sales_officer_parent');

// lvl 1
Route::get('/report/sales_officer_detail', 'ReportController@salesOfficerDetail')->name('sales_officer_detail');

// lvl 2
Route::get('/report/sales_officer_detail_customer', 'ReportController@salesOfficerDetailCustomer')->name('sales_officer_detail_customer');





Route::post('/report/savefolder', 'ReportController@saveToFolder')->name('report.saveToFolder');
Route::post('/report/sendMailAny', 'ReportController@sendMailAny')->name('report.sendMailAny');






Route::get('/report/sales_detail', 'ReportController@sales_detail')->name('sales_detail');


//opex
Route::get('/opex', 'OpexController@index')->name('opex');
Route::post('/insert_opex', 'OpexController@insert_opex');
Route::get('update_opex', 'OpexController@update_opex');


Route::get('/report/sales_detail', 'ReportController@sales_detail')->name('sales_detail');


// INVENTORY
Route::get('inventory/scheduleListCancel', 'InventoryController@scheduleListCancel')->name('inventory.scheduleListCancel');
Route::get('inventory/scheduleList', 'InventoryController@scheduleList')->name('inventory.scheduleList');
Route::get('inventory/index', 'InventoryController@index')->name('inventory.index');
Route::get('inventory/form_inventory/{id}', 'InventoryController@form_inventory')->name('inventory.form');
Route::post('inventory/store','InventoryController@store')->name('inventory.store');
Route::post('refinventory/get', 'InventoryController@refinventory')->name('refinventory.get');
Route::get('inventory/detail/{id}', 'InventoryController@inventory_detail')->name('inventory.detail');

    // DATA TABLE
    Route::get('data/inventory', 'InventoryController@data')->name('data.inventory');
    Route::get('data_approve/inventory', 'InventoryController@data_approve')->name('data_approve.inventory');
    Route::get('data_success/inventory', 'InventoryController@data_success')->name('data_success.inventory');
    Route::get('data_schedule/inventory', 'InventoryController@data_schedule')->name('data_schedule.inventory');
    Route::get('data_schedule_cancel/inventory', 'InventoryController@data_schedule_cancel')->name('data_schedule_cancel.inventory');
    Route::get('data_reversal/inventory', 'InventoryController@data_reversal')->name('data_reversal.inventory');
    Route::get('data_deleted/inventory', 'InventoryController@data_deleted')->name('data_deleted.inventory');




// APPROVAL
Route::post('/kirimAprovalInventory', 'InventoryController@kirimAproval');
Route::post('/hapusAprovalInventory', 'InventoryController@hapusAproval');
Route::post('/giveAprovalInventory', 'InventoryController@giveAproval');
Route::post('/rejectAprovalInventory', 'InventoryController@rejectAproval');

// REVERSAL
Route::post('/hapusAprovalInventoryRev', 'InventoryController@hapusAprovalRev');
Route::post('/giveHapusAprovalInventoryRev', 'InventoryController@giveHapusAprovalRev');
Route::post('/rejectHapusAprovalInventoryRev', 'InventoryController@rejectHapusAprovalRev');

// CANCEL PAYMENT
Route::post('/CancelPayInventorySc', 'InventoryController@CancelPaySc');
Route::post('/GiveApprovalCancelPaySc', 'InventoryController@GiveApprovalCancelPaySc');

// SCHEDULE
Route::post('/kirimAprovalInventorySc', 'InventoryController@kirimAprovalSc');
Route::post('/giveAprovalInventorySc', 'InventoryController@giveAprovalSc');
Route::post('/rejectAprovalInventorySc', 'InventoryController@rejectAprovalSc');
Route::post('/RejectCancelPayment', 'InventoryController@RejectCancelPayment');
// END INVENTORY


// INSTALLMENT
Route::get('installment/scheduleListCancel', 'InstallmentController@scheduleListCancel')->name('installment.scheduleListCancel');
Route::get('installment/scheduleList', 'InstallmentController@scheduleList')->name('installment.scheduleList');
Route::get('installment/scheduleListPaid', 'InstallmentController@scheduleListPaid')->name('installment.scheduleListPaid');
Route::get('installment/scheduleListComplete', 'InstallmentController@scheduleListComplete')->name('installment.scheduleListComplete');

Route::get('installment/split_approval_list', 'InstallmentController@split_approval_list')->name('installment.split_approval_list');
Route::get('installment/split_approval_list_reversal', 'InstallmentController@split_approval_list_reversal')->name('installment.split_approval_list_reversal');


Route::get('installment/index', 'InstallmentController@index')->name('installment.index');
Route::get('installment/form_installment/{id}', 'InstallmentController@form_installment')->name('installment.form');
Route::get('installment/form_sc/{id}', 'InstallmentController@form_sc')->name('installment.form_sc');
Route::post('installment/store','InstallmentController@store')->name('installment.store');
Route::post('installment/store_sc','InstallmentController@store_sc')->name('installment.store_sc');

Route::post('refinstallment/get', 'InstallmentController@refinstallment')->name('refinstallment.get');
Route::get('installment/detail/{id}', 'InstallmentController@installment_detail')->name('installment.detail');

    // DATA TABLE
    Route::get('data/installment', 'InstallmentController@data')->name('data.installment');
    Route::get('data_approve/installment', 'InstallmentController@data_approve')->name('data_approve.installment');
    Route::get('data_success/installment', 'InstallmentController@data_success')->name('data_success.installment');
    Route::get('data_schedule/installment', 'InstallmentController@data_schedule')->name('data_schedule.installment');
    Route::get('data_schedule_cancel/installment', 'InstallmentController@data_schedule_cancel')->name('data_schedule_cancel.installment');
    Route::get('data_schedule_paid/installment', 'InstallmentController@data_schedule_paid')->name('data_schedule_paid.installment');
    Route::get('data_schedule_complete/installment', 'InstallmentController@data_schedule_complete')->name('data_schedule_complete.installment');

    Route::get('data_reversal/installment', 'InstallmentController@data_reversal')->name('data_reversal.installment');
    Route::get('data_deleted/installment', 'InstallmentController@data_deleted')->name('data_deleted.installment');

    Route::get('data_split_approval/installment', 'InstallmentController@data_split_approval')->name('data_split_approval.installment');
    Route::get('data_split_approval_reversal/installment', 'InstallmentController@data_split_approval_reversal')->name('data_split_approval_reversal.installment');


// APPROVAL
Route::post('/kirimAprovalInstallment', 'InstallmentController@kirimAproval');
Route::post('/hapusAprovalInstallment', 'InstallmentController@hapusAproval');
Route::post('/giveAprovalInstallment', 'InstallmentController@giveAproval');
Route::post('/rejectAprovalInstallment', 'InstallmentController@rejectAproval');

Route::post('/kirimAprovalInstallmentSplit', 'InstallmentController@kirimAprovalSplit');



// REVERSAL
Route::post('/hapusAprovalInstallmentRev', 'InstallmentController@hapusAprovalRev');
Route::post('/giveHapusAprovalInstallmentRev', 'InstallmentController@giveHapusAprovalRev');
Route::post('/rejectHapusAprovalInstallmentRev', 'InstallmentController@rejectHapusAprovalRev');

// CANCEL PAYMENT
Route::post('/CancelPayInstallmentSc', 'InstallmentController@CancelPaySc');
Route::post('/GiveApprovalCancelPayInstallmentSc', 'InstallmentController@GiveApprovalCancelPaySc');

// SCHEDULE
Route::post('/kirimAprovalInstallmentSc', 'InstallmentController@kirimAprovalSc');
Route::post('/giveAprovalInstallmentSc', 'InstallmentController@giveAprovalSc');
Route::post('/rejectAprovalInstallmentSc', 'InstallmentController@rejectAprovalSc');
Route::post('/RejectCancelPaymentInstallment', 'InstallmentController@RejectCancelPayment');

// SPLIT
// Route::post('/kirimAprovalInstallmentSplit', 'InstallmentController@kirimAprovalSplit');
Route::post('/giveAprovalInstallmentSplit', 'InstallmentController@giveAprovalSplit');
Route::post('/rejectAprovalInstallmentSplit', 'InstallmentController@rejectAprovalSplit');
// Route::post('/RejectSplitPaymentInstallment', 'InstallmentController@RejectCancelPayment');

// REVERSAL SPLIT
Route::post('/kirimAprovalInstallmentSplitRev', 'InstallmentController@kirimAprovalSplitRev');
Route::post('/giveAprovalInstallmentSplitRev', 'InstallmentController@giveAprovalSplitRev');
Route::post('/rejectAprovalInstallmentSplitRev', 'InstallmentController@rejectAprovalSplitRev');

    //END INSTALLMENT


// BDD
Route::get('biaya_ditangguhkan/index', 'BiayaDitangguhkanController@index')->name('biaya_ditangguhkan.index');
Route::get('biaya_ditangguhkan/form_biaya_ditangguhkan/{id}', 'BiayaDitangguhkanController@form_biaya_ditangguhkan')->name('biaya_ditangguhkan.form');
Route::post('biaya_ditangguhkan/store','BiayaDitangguhkanController@store')->name('biaya_ditangguhkan.store');

Route::get('data/biaya_ditangguhkan', 'BiayaDitangguhkanController@data')->name('data.biaya_ditangguhkan');
Route::get('data_approve/biaya_ditangguhkan', 'BiayaDitangguhkanController@data_approve')->name('data_approve.biaya_ditangguhkan');
Route::get('data_success/biaya_ditangguhkan', 'BiayaDitangguhkanController@data_success')->name('data_success.biaya_ditangguhkan');
Route::get('data_schedule/biaya_ditangguhkan', 'BiayaDitangguhkanController@data_schedule')->name('data_schedule.biaya_ditangguhkan');
Route::get('data_schedule_cancel/biaya_ditangguhkan', 'BiayaDitangguhkanController@data_schedule_cancel')->name('data_schedule_cancel.biaya_ditangguhkan');
Route::get('data_reversal/biaya_ditangguhkan', 'BiayaDitangguhkanController@data_reversal')->name('data_reversal.biaya_ditangguhkan');
Route::get('data_deleted/biaya_ditangguhkan', 'BiayaDitangguhkanController@data_deleted')->name('data_deleted.biaya_ditangguhkan');

Route::post('refbiaya_ditangguhkan/get', 'BiayaDitangguhkanController@refbiaya_ditangguhkan')->name('refbiaya_ditangguhkan.get');
Route::get('biaya_ditangguhkan/detail/{id}', 'BiayaDitangguhkanController@biaya_ditangguhkan_detail')->name('biaya_ditangguhkan.detail');

Route::get('biaya_ditangguhkan/scheduleListCancel', 'BiayaDitangguhkanController@scheduleListCancel')->name('biaya_ditangguhkan.scheduleListCancel');
Route::get('biaya_ditangguhkan/scheduleList', 'BiayaDitangguhkanController@scheduleList')->name('biaya_ditangguhkan.scheduleList');
// APPROVAL
Route::post('/kirimAprovalBdd', 'BiayaDitangguhkanController@kirimAproval');
Route::post('/hapusAprovalBdd', 'BiayaDitangguhkanController@hapusAproval');
Route::post('/giveAprovalBdd', 'BiayaDitangguhkanController@giveAproval');
Route::post('/rejectAprovalBdd', 'BiayaDitangguhkanController@rejectAproval');


// REVERSAL
Route::post('/hapusAprovalBddRev', 'BiayaDitangguhkanController@hapusAprovalRev');
Route::post('/giveHapusAprovalBddRev', 'BiayaDitangguhkanController@giveHapusAprovalRev');
Route::post('/rejectHapusAprovalBddRev', 'BiayaDitangguhkanController@rejectHapusAprovalRev');

// CANCEL PAYMENT
Route::post('/CancelPayBddSc', 'BiayaDitangguhkanController@CancelPaySc');
Route::post('/GiveApprovalCancelPayBddSc', 'BiayaDitangguhkanController@GiveApprovalCancelPaySc');

// SCHEDULE
Route::post('/kirimAprovalBddSc', 'BiayaDitangguhkanController@kirimAprovalSc');
Route::post('/giveAprovalBddSc', 'BiayaDitangguhkanController@giveAprovalSc');
Route::post('/rejectAprovalBddSc', 'BiayaDitangguhkanController@rejectAprovalSc');
Route::post('/RejectCancelPaymentBdd', 'BiayaDitangguhkanController@RejectCancelPayment');

// END BDD

// BATCH
Route::get('batch/index', 'BatchController@index')->name('batch.index');
Route::get('batch_jurnal', 'BatchController@batch_jurnal');
Route::get('batch_bdd', 'BatchController@batch_bdd');
Route::get('batch_inventory', 'BatchController@batch_inventory');

// CETAK
Route::get('/print/balance_sheet', 'CetakController@balance_sheet')->name('cetak.balance_sheet');
Route::get('/print/balance_sheet/view', 'CetakController@balance_sheet_view')->name('cetak.balance_sheet.view');


Route::post('/endSession', 'Controller@endSession');

//Claim
Route::get('/claim', 'ClaimController@index')->name('claim');
Route::post('/insert_claim', 'ClaimController@insert');
Route::post('/send_claim', 'ClaimController@send_claim');
Route::get('/byClaim', 'ClaimController@byClaim');
Route::get('/delete_claim', 'ClaimController@delete_claim');
Route::get('/detail_claim', 'ClaimController@detail_claim');

Route::get('/approval_claim', 'ClaimController@list_approval')->name('approval_claim');

Route::post('/approve_claim', 'ClaimController@approve_claim');
Route::post('/reject_claim', 'ClaimController@reject_claim');

Route::post('/rev_split', 'InvoiceController@rev_split');
Route::post('/rev_split_new', 'InvoiceController@rev_split_new');
Route::post('/rev_split_reject_new', 'InvoiceController@rev_split_reject_new');


// QUOTATION SLIP
Route::prefix('quotation_slip')->group(function () {
    Route::get('/', 'QuotationSlipController@index')->name('quotation_slip');
    Route::get('client', 'QuotationSlipController@indexClient')->name('quotation_slip.client');
    Route::get('create', 'QuotationSlipController@create')->name('quotation_slip.create', ['type' => 'type']);
    Route::get('cancel', 'QuotationSlipController@setCancel')->name('quotation_slip.set_cancel');
    Route::get('show/{id}/{type}', 'QuotationSlipController@show')->name('quotation_slip.show', ['id' => 'id', 'type' => 'type']);
    Route::get('get_cust_addr', 'QuotationSlipController@getCustomerAddress')->name('quotation_slip.get_cust_addr');
    Route::get('export/{id}', 'QuotationSlipController@export')->name('quotation_slip.export', ['id' => 'id']);
    Route::get('get_qs_content', 'QuotationSlipController@getQsContent');
    Route::post('store', 'QuotationSlipController@store')->name('quotation_slip.store');
    Route::get('get_detail_insured/{id}', 'QuotationSlipController@getDetailInsured')->name('quotation_slip.get_detail_insured', ['id' => 'id']);
    Route::post('approval', 'QuotationSlipController@sendApprove')->name('quotation_slip.approval');
    Route::post('approve', 'QuotationSlipController@approve')->name('quotation_slip.approve');
    Route::post('reject', 'QuotationSlipController@reject')->name('quotation_slip.reject');
});


Route::prefix('cover_note')->group(function () {
    Route::get('/', 'CoverNoteController@index')->name('cover_note');
    Route::get('create', 'CoverNoteController@create')->name('cover_note.create');
    Route::get('cancel', 'CoverNoteController@setCancel')->name('cover_note.set_cancel');
    Route::get('show/{id}', 'CoverNoteController@show')->name('cover_note.show', ['id' => 'id']);
    Route::get('get_cust_addr', 'CoverNoteController@getCustomerAddress')->name('cover_note.get_cust_addr');
    Route::get('export/{id}', 'CoverNoteController@export')->name('cover_note.export', ['id' => 'id']);
    Route::post('store', 'CoverNoteController@store')->name('cover_note.store');
    Route::get('/get_list_qs_no','CoverNoteController@getListQSNo')->name('cover_note.get_list_qs_no');
    Route::get('/get_data_qs','CoverNoteController@getDataQS')->name('cover_note.get_data_qs');
    Route::get('get_detail_insured/{id}', 'CoverNoteController@getDetailInsured')->name('cover_note.get_detail_insured', ['id' => 'id']);
    Route::get('get_list_filter_qs', 'CoverNoteController@getListFilterQS');
});

Route::prefix('confirmation_of_cover')->group(function () {
    Route::get('/', 'COCController@index')->name('confirmation_of_cover');
    Route::get('create', 'COCController@create')->name('confirmation_of_cover.create');
    Route::get('cancel', 'COCController@setCancel')->name('confirmation_of_cover.set_cancel');
    Route::get('show/{id}', 'COCController@show')->name('confirmation_of_cover.show', ['id' => 'id']);
    Route::get('get_cust_addr', 'COCController@getCustomerAddress')->name('confirmation_of_cover.get_cust_addr');
    Route::get('export/{id}', 'COCController@export')->name('confirmation_of_cover.export', ['id' => 'id']);
    Route::post('store', 'COCController@store')->name('confirmation_of_cover.store');
    Route::get('/get_list_cn_no','COCController@getListCNNo')->name('confirmation_of_cover.get_list_cn_no');
    Route::get('/get_data_cn','COCController@getDataCN')->name('confirmation_of_cover.get_data_cn');
    Route::get('get_list_filter_cn', 'COCController@getListFilterCN');
    Route::get('get_detail_insured/{id}', 'COCController@getDetailInsured')->name('confirmation_of_cover.get_detail_insured', ['id' => 'id']);
});

Route::prefix('proposal_coverage')->group(function (){
    Route::get('/', 'ProposalConverageController@index')->name('proposal_coverage');
    Route::get('create', 'ProposalConverageController@create')->name('proposal_coverage.create');
    Route::get('show/{id}', 'ProposalConverageController@show')->name('proposal_coverage.show', ['id' => 'id']);
});

Route::get('get_insured_detail', 'QuotationSlipController@get_insured_detail')->name('get_insured_detail');

Route::post( 'insert_officer_baru', ['as'=>'insert_officer_baru', 'uses'=> 'CustomerController@insert_officer_baru']);
Route::post( 'insert_agent_baru', ['as'=>'insert_agent_baru', 'uses'=> 'CustomerController@insert_agent_baru']);
Route::get('/get_list_officer', 'CustomerController@get_list_officer')->name('get_list_officer');
Route::get('/get_list_agent', 'CustomerController@get_list_agent')->name('get_list_agent');
Route::get('/get_agent_by_cust_id/{id}', 'CustomerController@get_agent_by_cust_id');




    // MARKETING
    Route::prefix('marketing')->group(function () {
        Route::get('/list/{type}', 'MarketingController@list')->name('marketing.list');
        Route::get('/create/{type}', 'MarketingController@create')->name('marketing.create');
        Route::get('/data/{type}/{id}', 'MarketingController@data')->name('marketing.data');
        Route::post('/store/{type}', 'MarketingController@store')->name('marketing.store');
        Route::post('/approval/{type}', 'MarketingController@approval')->name('marketing.approval');
    });

    //CLAIM BARU
    Route::get('claim/get_detail_product_list', 'ClaimController@getDetailProduct');
    Route::get('claim/get_list_keyword', 'ClaimController@getListKeyword');
    Route::get('claim/get_list_data_filter', 'ClaimController@getListDataFilter');
    Route::get('claim/get_detail_product', 'ClaimController@getAllDetailProduct');

});



Route::get('printword/{type}/{id}', 'CetakController@generateDocx')->name('cetak.word');
Route::get('printwordSc/{invoice_type}/{type}/{id}', 'CetakController@generateDocxSc')->name('cetakSc.word');

Route::get('insurance/printword/{type}/{id}', 'CetakController@cetakInsurace');
Route::get('insurance/printexcel/{type}/{id}', 'CetakController@cetakLampiranInsurace');



