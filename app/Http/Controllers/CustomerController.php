<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;

class CustomerController extends Controller
{
  public function customer_baru(Request $request)
    {
        $cust_type = \DB::select('SELECT * FROM ref_cust_type where is_active=true order by seq asc');
        $agent = \DB::select('SELECT * FROM ref_agent where is_active=true AND is_parent=true order by seq asc');
        $city = \DB::select('SELECT * FROM ref_city');
        $cust = \DB::select('SELECT * FROM ref_cust_group');
        $selectAgent = \DB::TABLE('ref_agent')
                            ->where('is_active', 't')
                            ->whereNotNull('parent_id')
                            ->get();

        $custBranch = \DB::TABLE('master_branch_cust')
                            ->where('is_active', 't')
                            ->get();

        $param['cust_type']=$cust_type;
        $param['agent']=$agent;
        $param['city']=$city;
        $param['cust']=$cust;
        $param['selectAgent']=$selectAgent;
        $param['custBranch']=$custBranch;
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'customer.form',$param);
        }else {
            return view('master.master')->nest('child', 'customer.form',$param);
        }
    }
     public function customer_list(Request $request)
    {
        $data = \DB::select("select a.*,b.full_name as definition from master_customer a
            left join ref_agent b on b.id=a.agent_id where a.is_active=false and wf_status_id in (1,3) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
        $param['data']=$data;
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'customer.list',$param);
        }else {
            return view('master.master')->nest('child', 'customer.list',$param);
        }
    }

    public function edit_customer(Request $request){
        $data = \DB::select('select * from master_customer where id='.$request->get('id'));
        $param['data']=$data;

        $cust_type = \DB::select('SELECT * FROM ref_cust_type where is_active=true order by seq asc');
        $agent = \DB::select('SELECT * FROM ref_agent_type where is_active=true order by seq asc');
        $city = \DB::select('SELECT * FROM ref_city');
        $province = \DB::select('SELECT * FROM ref_province where province_code='.$data[0]->province_id);

        $selectAgent = \DB::TABLE('ref_agent')
                            ->where('is_active', 't')
                            ->whereNotNull('parent_id')
                            ->get();

        $param['cust_type']=$cust_type;
        $param['agent']=$agent;
        $param['city']=$city;
        $param['province']=$province;
        $param['selectAgent']=$selectAgent;
        return view('master.master')->nest('child', 'customer.edit_form',$param);

    }
    public function customer_list_active(Request $request)
    {
        $data = \DB::select("select a.*,b.full_name as definition from master_customer a
            left join ref_agent b on b.id=a.agent_id where a.wf_status_id=9 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and a.is_active=true");
        $param['data']=$data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'customer.list_active',$param);
        }else {
            return view('master.master')->nest('child', 'customer.list_active',$param);
        }
    }

    public function get_data_customer(Request $request)
    {
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');

        return response()->json([
            'rc' => 1,
            'rm' => $ref_customer
        ]);
    }

    public function customer_list_non_active(Request $request)
    {
        $data = \DB::select("select a.*,b.full_name as definition from master_customer a
            left join ref_agent b on b.id=a.agent_id where a.wf_status_id=9 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and a.is_active=false");
        $param['data']=$data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'customer.list_non_active',$param);
        }else {
            return view('master.master')->nest('child', 'customer.list_non_active',$param);
        }
    }

    public function setActive(Request $request)
    {
        $message = '';
        $setActive = $request->input('active');

        if ( $setActive == 'f' ) {
            DB::TABLE("master_customer")->where('id', $request->input('id'))->update([
                'is_active' => 'f'
            ]);
            $message = 'Customer berhasil dinonaktifkan';
        } else {
            DB::TABLE("master_customer")->where('id', $request->input('id'))->update([
                'is_active' => 't'
            ]);
            $message = 'Customer berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $message
        ]);
    }

    public function customer_detail_active($id,Request $request)
    {
        $data = \DB::select('select a.*,b.definition,c.city_name,d.province_name,e.definition as cust_type from master_customer a
        left join ref_agent_type b on b.id=a.agent_id
        left join ref_city c on c.city_code=a.city_id
        left join ref_province d on d.province_code=a.province_id
        left join ref_cust_type e on e.id=a.cust_type_id where a.id='.$id);

        $ins= \DB::select('select a.*,c.definition from master_sales a
            left join ref_product c on c.id=a.product_id
            where a.customer_id='.$id);

        $param['data']=$data;
        $param['ins']=$ins;


        $upd = \DB::select('select * from master_customer where id='.$id);
        $param['upd']=$upd;

        $cust_type = \DB::select('SELECT * FROM ref_cust_type where is_active=true order by seq asc');
        $agent = \DB::select('SELECT * FROM ref_agent_type where is_active=true order by seq asc');
        $city = \DB::select('SELECT * FROM ref_city');
        $province = \DB::select('SELECT * FROM ref_province where province_code='.$upd[0]->province_id);

        $selectAgent = \DB::TABLE('ref_agent')
                            ->where('is_active', 't')
                            ->whereNotNull('parent_id')
                            ->get();

        $custBranch = \DB::TABLE('master_branch_cust')
                        ->where('is_active', 't')
                        ->get();

        $param['cust_type']=$cust_type;
        $param['agent']=$agent;
        $param['city']=$city;
        $param['province']=$province;
        $param['selectAgent'] = $selectAgent;
        $param['custBranch'] = $custBranch;

        $param['routeTable'] = route('marketing.data',["list_policy_customer",$id]); 

        return view('master.master')->nest('child', 'customer.detail_active',$param);

    }

    public function customer_detail_non_active($id,Request $request)
    {
        $data = \DB::select('select a.*,b.definition,c.city_name,d.province_name,e.definition as cust_type from master_customer a
        left join ref_agent_type b on b.id=a.agent_id
        left join ref_city c on c.city_code=a.city_id
        left join ref_province d on d.province_code=a.province_id
        left join ref_cust_type e on e.id=a.cust_type_id where a.id='.$id);

        $ins= \DB::select('select a.*,c.definition from master_sales a
            left join ref_product c on c.id=a.product_id
            where a.customer_id='.$id);

        $param['data']=$data;
        $param['ins']=$ins;


        $upd = \DB::select('select * from master_customer where id='.$id);
        $param['upd']=$upd;

        $cust_type = \DB::select('SELECT * FROM ref_cust_type where is_active=true order by seq asc');
        $agent = \DB::select('SELECT * FROM ref_agent_type where is_active=true order by seq asc');
        $city = \DB::select('SELECT * FROM ref_city');
        $province = \DB::select('SELECT * FROM ref_province where province_code='.$upd[0]->province_id);

        $param['cust_type']=$cust_type;
        $param['agent']=$agent;
        $param['city']=$city;
        $param['province']=$province;

        return view('master.master')->nest('child', 'customer.detail_non_active',$param);

    }

    public function customer_approval(Request $request)
    {
        $data = \DB::select("select a.*,b.full_name as definition from master_customer a
            left join ref_agent b on b.id=a.agent_id where a.wf_status_id=2 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
        $param['data']=$data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'customer.approval',$param);
        }else {
            return view('master.master')->nest('child', 'customer.approval',$param);
        }
    }

    public function insert_customer_baru(Request $request){
        $results = \DB::select("SELECT max(id) AS id from master_customer");
        $id='';
        if($results){
            $id=$results[0]->id + 1;
        }else{
            $id=1;
        }

        $is_branch;
        if ( $request->is_branch == '1') {
            $is_branch = true;
        } else {
            $is_branch = false;
        }

        $extAgent = $request->select_agent;

        if ($request->file('img')) {
            $destination_path = public_path('upload_customer');
            $files = $request->file('img');
            $filename = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $id."_".$filename);

            DB::table('master_customer')->insert(
                [
                    'id' => $id,
                    'full_name' => $request->input('full_name'),
                    'short_name' => $request->input('short_name'),
                    'email' => $request->input('email'),
                    'phone_no' => $request->input('phone'),
                    'address' => $request->input('alamat'),
                    'city_id' => $request->input('city'),
                    'province_id' => $request->input('province'),
                    'cust_type_id' => $request->input('cust_type'),
                    'agent_id' => $request->input('agent'),
                    'cust_group_id' => $request->input('cust_group'),
                    'is_active' => true,
                    'wf_status_id' => 9,// langsung approve
                    'image_url' => $filename,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_crt_id' => Auth::user()->id,
                    'ext_agent_id' => $extAgent,
                    'home_phone' => $request->input('home_phone_number'),
                    'fax_no' => $request->input('fax_number'),
                    'is_branch' => $is_branch,
                    'cust_branch_id' => $request->cust_branch_id
                ]
            );
        }else{
            DB::table('master_customer')->insert(
                [
                    'id' => $id,
                    'full_name' => $request->input('full_name'),
                    'short_name' => $request->input('short_name'),
                    'email' => $request->input('email'),
                    'phone_no' => $request->input('phone'),
                    'address' => $request->input('alamat'),
                    'city_id' => $request->input('city'),
                    'province_id' => $request->input('province'),
                    'cust_type_id' => $request->input('cust_type'),
                    'agent_id' => $request->input('agent'),
                    'cust_group_id' => $request->input('cust_group'),
                    'is_active' => true,
                    'wf_status_id' => 9,
                    'image_url' => '72_hd.jpg',
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_crt_id' => Auth::user()->id,
                    'ext_agent_id' => $extAgent,
                    'home_phone' => $request->input('home_phone_number'),
                    'fax_no' => $request->input('fax_number'),
                    'is_branch' => $is_branch,
                    'cust_branch_id' => $request->cust_branch_id
                ]
            );
        }


        return response()->json([
            'rc' => 1,
            'rm' => 'berhasil'
        ]);

    }

    public function update_customer_baru(Request $request){

        $extAgent = $request->select_agent;

        $is_branch;
        $cust_branch_id;

        if ( $request->is_branch == '1') {
            $is_branch = true;
            $cust_branch_id = $request->cust_branch_id;
        } else {
            $is_branch = false;
            $cust_branch_id = NULL;
            $request->cust_branch_id = NULL;
        }


        if($request->file('img')){
            $destination_path = public_path('upload_customer');
            $files = $request->file('img');
            $filename = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $request->input('id')."_".$filename);

            DB::table('master_customer')
            ->where('id', $request->input('id'))
            ->update(
                [
                    'full_name' => $request->input('full_name'),
                    'short_name' => $request->input('short_name'),
                    'email' => $request->input('email'),
                    'phone_no' => $request->input('phone'),
                    'address' => $request->input('alamat'),
                    'city_id' => $request->input('city'),
                    'province_id' => $request->input('province'),
                    'cust_type_id' => $request->input('cust_type'),
                    'agent_id' => $request->input('agent'),
                    'cust_group_id' => $request->input('cust_group'),
                    'image_url' => $filename,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_crt_id' => Auth::user()->id,
                    'ext_agent_id' => $extAgent,
                    'home_phone' => $request->input('home_phone_number'),
                    'fax_no' => $request->input('fax_number'),
                    'is_branch' => $is_branch,
                    'cust_branch_id' => $cust_branch_id
                ]
            );
        }else{
            DB::table('master_customer')
            ->where('id', $request->input('id'))
            ->update(
                [
                    'full_name' => $request->input('full_name'),
                    'short_name' => $request->input('short_name'),
                    'email' => $request->input('email'),
                    'phone_no' => $request->input('phone'),
                    'address' => $request->input('alamat'),
                    'city_id' => $request->input('city'),
                    'province_id' => $request->input('province'),
                    'cust_type_id' => $request->input('cust_type'),
                    'agent_id' => $request->input('agent'),
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_crt_id' => Auth::user()->id,
                    'ext_agent_id' => $extAgent,
                    'home_phone' => $request->input('home_phone_number'),
                    'fax_no' => $request->input('fax_number'),
                    'is_branch' => $is_branch,
                    'cust_branch_id' => $cust_branch_id
                ]
            );
        }

        return response()->json([
            'rc' => 1,
            'rm' => 'berhasil'
        ]);
    }

    public function kirimAproval(Request $request){
        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_customer where is_active=false and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_customer')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 2,'is_active'=>false]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_customer')
                ->where('id', $item)
                ->update(['wf_status_id' => 2,'is_active'=>false]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_customer where wf_status_id in (1,3) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_customer')->where('id', '=',$item->id)->delete();

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_customer')->where('id', '=',$item)->delete();
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function kirimAproval_app(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_customer where wf_status_id=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_customer')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'is_active'=>true,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_customer')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'is_active'=>true,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_app(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_customer where wf_status_id=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_customer')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 3,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_customer')
                ->where('id', $item)
                ->update(['wf_status_id' => 3,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function det_cust(Request $request){
        $results = \DB::select("select a.*,b.definition,c.city_name,d.province_name,e.definition as cust_type from master_customer a
        left join ref_agent_type b on b.id=a.agent_id
        left join ref_city c on c.city_code=a.city_id
        left join ref_province d on d.province_code=a.province_id
        left join ref_cust_type e on e.id=a.cust_type_id where a.id=".$request->get('id'));
        return json_encode($results);
    }

    public function insert_officer_baru (Request $request) {

        $arrData = $request->all();
        $lastSeq = DB::TABLE('ref_agent')->max('seq');
        if ( is_null($lastSeq) ) {
            $lastSeq = 1;
        } else {
            $lastSeq += 1;
        }


        DB::TABLE('ref_agent')->insert([
            'full_name' => $arrData['full_name_officer'],
            'code_name' => strtolower($arrData['code_name_officer']),
            'address' => $arrData['address_officer'],
            'phone_no' => $arrData['phone_no_officer'],
            'email' => $arrData['email_officer'],
            'is_active' => 't',
            'seq' => $lastSeq,
            'user_crt_id' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'company_id' => Auth::user()->company_id,
            'is_parent' => 't',
            'parent_id' => NULL
        ]);

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses'
        ]);
    }

    public function insert_agent_baru(Request $request) {
        $arrData = $request->all();


        $lastSeq = DB::TABLE('ref_agent')->max('seq');
        if ( is_null($lastSeq) ) {
            $lastSeq = 1;
        } else {
            $lastSeq += 1;
        }


        DB::TABLE('ref_agent')->insert([
            'full_name' => $arrData['full_name_agent'],
            'code_name' => strtolower($arrData['code_name_agent']),
            'address' => $arrData['address_agent'],
            'phone_no' => $arrData['phone_no_agent'],
            'email' => $arrData['email_agent'],
            'is_active' => 't',
            'seq' => $lastSeq,
            'user_crt_id' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'company_id' => Auth::user()->company_id,
            'is_parent' => FALSE,
            'parent_id' => $arrData['officer_agent']
        ]);

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses'
        ]);
    }

    public function get_list_officer(Request $request)
    {
        $agent = \DB::select('SELECT * FROM ref_agent where is_active=true AND is_parent=true order by seq asc');

        return response()->json([
            'rc' => 1,
            'rm' => $agent
        ]);
    }

    public function get_list_agent(Request $request) {
        $selectAgent = \DB::TABLE('ref_agent')
                            ->where('is_active', 't')
                            ->whereNotNull('parent_id')
                            ->get();

        return response()->json([
            'rc' => 1,
            'rm' => $selectAgent
        ]);
    }

    public function get_agent_by_cust_id(Request $request, $id) {

        $dataAgent = DB::TABLE('master_customer')
                        ->where('id', $id)
                        ->first();


        $data = \DB::TABLE('ref_agent')
                            ->where('id', $dataAgent->ext_agent_id)
                            ->where('is_active', 't')
                            ->first();


        return response()->json([
            'rc' => 1,
            'rm' => $data
        ]);
    }

}
