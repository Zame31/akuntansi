<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\ResponseClass;
use App\Services\AsignNasabahService;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use DB;
use Response;
use PDF;
use Hash;
use Auth;
use Excel;
class CronController extends Controller{

    public function getShortCodeBranch($idBranch) {
        $dt = DB::TABLE('master_branch')->select('short_code')->where('id', $idBranch)->first();
        return $dt->short_code;
    }

    public function batch_jurnal ($type_batch, $tgl){
        // dd($tgl);
        $branch_id = 1;
        $company_id = 1;
        $system_date = collect(\DB::select("select * from ref_system_date"))->first();
        if ($type_batch == 'day') {
            $month=date('m',strtotime($system_date->current_date));
            $year=date('Y',strtotime($system_date->current_date));
        // }else {
        //     $month=date('m',strtotime($tgl));
        //     $year=date('Y',strtotime($tgl));
        }else{
            return response()->json([
                'rc' => 1,
                'rm' => "Type batch bukan day",
            ]);
        }
        $batch = collect(\DB::select("select * from master_financial where branch_id=1 and company_id=1 and report_date = '".date('Y-m-d',strtotime($tgl))."'"))->first();
        if($batch){
          DB::table('master_financial')->where('id', $batch->id)->delete();
        }

        $posisi_keuangan_inti = collect(\DB::select("select
            sum(case when coa_no in ('100','101','102','103') then last_os end) as total_aset,
            sum(case when coa_no in ('301') then last_os end) as profit_loss,
            sum(case when coa_no in ('400') then last_os end) as total_income,
            sum(case when coa_no in ('500') then last_os end) as total_outcome,
            sum(case when coa_no in ('500001') then last_os end) as marketing_outcome,
            sum(case when coa_no in ('500002') then last_os end) as building_outcome,
            sum(case when coa_no in ('500003') then last_os end) as general_outcome,
            sum(case when coa_no in ('500004') then last_os end) as resources_outcome
            from
            (select
            coa_no,
            coa_name,
            case when coa_no in ('301') then last_os +
            (WITH RECURSIVE cte AS (
               SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
               FROM   master_coa
               WHERE  coa_parent_id IS NULL and company_id=".$company_id." and branch_id=".$branch_id." and is_active=true
               UNION  ALL
               SELECT c.coa_no, p.coa_no, p.last_os
               FROM   cte c
               JOIN   (select * from master_coa where company_id=".$company_id." and branch_id=".$branch_id." and is_active=true) p USING (coa_parent_id)
            )
            SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
            FROM   cte
            where coa_no in ('400','500')) else last_os end as last_os
            from
            (select
            a.coa_no,
            coa_name,
            case when a.coa_no = b.coa_parent_id then b.nom
               when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
            from master_coa a left join
            (select coa_parent_id, sum(coalesce(last_os,0)) as nom
            from master_coa
            where coa_parent_id is  not null and length(coa_parent_id) = 6
            group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
            (WITH RECURSIVE cte AS (
               SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
               FROM   master_coa
               WHERE  coa_parent_id IS NULL and company_id=".$company_id." and branch_id=".$branch_id." and is_active=true
               UNION  ALL
               SELECT c.coa_no, p.coa_no, p.last_os
               FROM   cte c
               JOIN   (select * from master_coa where company_id=".$company_id." and branch_id=".$branch_id." and is_active=true) p USING (coa_parent_id)
            )
            SELECT coa_no, sum(last_os) AS last_os
            FROM   cte
            GROUP  BY 1) c on a.coa_no = c.coa_no where a.company_id=".$company_id." and a.branch_id=".$branch_id." and a.is_active=true) x) x;"))->first();



            $results = \DB::select("SELECT max(id) AS id from master_financial");
            if($results){
                $id=$results[0]->id + 1;
            }else{
                $id=1;
            }
            $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

            DB::table('master_financial')->insert(
                [
                    'id' => $id,
                    'report_date' => $systemDate->current_date,
                    'total_aset' => $posisi_keuangan_inti->total_aset,
                    'profit_loss' =>$posisi_keuangan_inti->profit_loss,
                    'total_income' => $posisi_keuangan_inti->total_income,
                    'total_outcome' => $posisi_keuangan_inti->total_outcome,
                    'marketing_outcome' => $posisi_keuangan_inti->marketing_outcome,
                    'building_outcome' => $posisi_keuangan_inti->building_outcome,
                    'general_outcome' => $posisi_keuangan_inti->general_outcome,
                    'resources_outcome' => $posisi_keuangan_inti->resources_outcome,
                    'date_dim_id' => date('Ymd',strtotime($systemDate->current_date)),
                    'company_id' =>$company_id,
                    'branch_id' => $branch_id
                ]
            );

            $upd_date = date('Y-m-d', strtotime('+1 days', strtotime($systemDate->current_date)));
            $last_datez = date('Y-m-d', strtotime('+1 days', strtotime($systemDate->last_date)));

            if ($last_datez > date('Y-m-d')) {

            }else {

                if ($type_batch == 'day') {
                    DB::table('ref_system_date')
                    ->where('id', 1)
                    ->update(['current_date' => $upd_date,'last_date'=>$systemDate->current_date]);
                }

            }

            $results = \DB::select("SELECT max(id) AS id from ref_batch");
            if($results){
                $id=$results[0]->id + 1;
            }else{
                $id=1;
            }
            DB::table('ref_batch')->insert(
                [
                    'id' => $id,
                    'processing_date' => date('Y-m-d',strtotime($tgl)),
                    'processing_time' => date('Y-m-d H:i:s',strtotime($tgl)),
                    'is_done' =>true,
                    'company_id' =>$company_id,
                    'branch_id' => $branch_id
                ]
            );
            return response()->json([
                'rc' => 200,
                'rm' => "Batch Proses Finished",
            ]);
    }

    public function saveToFolder($type)
    {
        // $path = env('PUBLIC_PATH');

        // $path = \Storage::disk('local');
        // dd($path);
        // $type =
            $sy = collect(\DB::SELECT('SELECT * FROM ref_system_date'))->first();
            $system_date = date('Ymd',strtotime($sy->last_date));

            // $system_date = date('Ymd', strtotime('-1 days', strtotime($sy->current_date)));
            // $system_date_n = date('Y-m-d', strtotime('-1 days', strtotime($sy->current_date)));


            $destination_path = public_path('batch_file/'.$sy->last_date);
            if(!File::isDirectory($destination_path)){
                File::makeDirectory($destination_path, 0777, true, true);
            }

            // KONSOL
            $destination_path_konsol = public_path('batch_file/'.$sy->last_date.'/KONSOL');
            if(!File::isDirectory($destination_path_konsol)){
                File::makeDirectory($destination_path_konsol, 0777, true, true);
            }


            $b_name = 'KONSOL_';

            if($type == 'bs') {

              $param["aktiva"] = $this->getBalanceSheet('aktiva','konsol');
              $param["pasiva"] = $this->getBalanceSheet('pasiva','konsol');
              $param["laba_rugi"] = $this->getLabarugi('laba_rugi','konsol');

              $fname = $b_name."BS".$system_date;

              $pdf = PDF::loadView('cetak.pdf_balance_sheet', $param)->setPaper('a4', 'potrait');

              $data = $pdf->output();
              $file = fopen($destination_path_konsol."/".$fname.".pdf", 'w');
              fwrite($file, $data);
              fclose($file);


            }elseif ($type == 'pl') {
              $param["aktiva"] = $this->getBalanceSheet('aktiva','konsol');
              $param["pasiva"] = $this->getBalanceSheet('pasiva','konsol');
              $param["laba_rugi"] = $this->getLabarugi('laba_rugi','konsol');

              $fname = $b_name."PL".$system_date;

              $pdf = PDF::loadView('cetak.pdf_profit_loss_cron', $param)->setPaper('a4', 'potrait');

              $data = $pdf->output();
              $file = fopen($destination_path_konsol."/".$fname.".pdf", 'w');
              fwrite($file, $data);
              fclose($file);


            }elseif ($type == 'cn') {

                $ext = '';
                $sts = '';

                for ($i=1; $i <= 3; $i++) {

                    if ($i == 1) {
                        $sts = 'Aktif';
                        $fname = $b_name."CN".$system_date."A";
                    }elseif ($i == 2) {
                        $sts = 'Paid';
                        $fname = $b_name."CN".$system_date."P";
                    }elseif ($i == 3) {
                        $sts = 'Completed';
                        $fname = $b_name."CN".$system_date."C";
                    }

                    $param['data'] = $this->getQueryNominativeReport($sts);
                    $param['status'] = $sts;
                    $param['short_code'] = $this->getShortCodeBranch(1);

                    $pdf = PDF::loadView('cetak.pdf_nominative_report', $param)->setPaper('a4', 'landscape');

                    $data = $pdf->output();
                    $file = fopen($destination_path_konsol."/".$fname.".pdf", 'w');
                    fwrite($file, $data);
                    fclose($file);

                    Excel::create($fname, function($excel) use ($param){
                        $excel->sheet('Sheet 1',function($sheet) use ($param){
                            $sheet->loadView('cetak.xlsx_nominative_report', $param);
                        });
                    })->store('xlsx', $destination_path_konsol);
                    #Store and export

                }

              // MASTER COA
            }elseif ($type == 'coa') {
                $param['data'] = 'waw';

                $fname = $b_name."COA".$system_date;
                Excel::create($fname, function($excel) use ($param){
                    $excel->sheet('Sheet 1',function($sheet) use ($param){
                        $sheet->loadView('cetak.xlsx_coa', $param);
                    });
                })->store('xlsx', $destination_path_konsol);

            }elseif ($type == 'due_date') {

              $param['data'] = json_decode($this->data_renewal_list_due_date());

              $fname = $b_name."RENEWAL_LIST_DUE_DATE".$system_date;

              Excel::create($fname, function($excel) use ($param){
                  $excel->sheet('Sheet 1',function($sheet) use ($param){
                      $sheet->loadView('cetak.xlsx_renewal_list_due_date', $param);
                  });
              })->store('xlsx', $destination_path_konsol);

              Excel::create("RENEWAL_LIST_DUE_DATE".$system_date, function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_renewal_list_due_date', $param);
                });
            })->store('xlsx', public_path('batch_file'));

          }

            // JIKA BRANCH
            $getBranch = \DB::SELECT("SELECT * FROM master_branch where is_ho = 'f'");

            foreach ($getBranch as $keyBranch => $vBranch) {

              $destination_path_branch = public_path('batch_file/'.$sy->last_date.'/'.$vBranch->short_code);
              if(!File::isDirectory($destination_path_branch)){
                  File::makeDirectory($destination_path_branch, 0777, true, true);
              }

              if ($type == 'bs') {

                $param["aktiva"] = $this->getBalanceSheet('aktiva',$vBranch->id);
                $param["pasiva"] = $this->getBalanceSheet('pasiva',$vBranch->id);
                $param["laba_rugi"] = $this->getLabarugi('laba_rugi',$vBranch->id);

                $fname = $vBranch->short_code."_BS".$system_date;

                $pdf = PDF::loadView('cetak.pdf_balance_sheet', $param)->setPaper('a4', 'potrait');

                $data = $pdf->output();
                $file = fopen($destination_path_branch."/".$fname.".pdf", 'w');
                fwrite($file, $data);
                fclose($file);


              }elseif ($type == 'pl') {
                $param["aktiva"] = $this->getBalanceSheet('aktiva',$vBranch->id);
                $param["pasiva"] = $this->getBalanceSheet('pasiva',$vBranch->id);
                $param["laba_rugi"] = $this->getLabarugi('laba_rugi',$vBranch->id);

                $fname = $vBranch->short_code."_PL".$system_date;

                $pdf = PDF::loadView('cetak.pdf_profit_loss_cron', $param)->setPaper('a4', 'potrait');

                $data = $pdf->output();
                $file = fopen($destination_path_branch."/".$fname.".pdf", 'w');
                fwrite($file, $data);
                fclose($file);


              }elseif ($type == 'cn') {

                  $ext = '';
                  $sts = '';

                  for ($i=1; $i <= 3; $i++) {

                      if ($i == 1) {
                          $sts = 'Aktif';
                          $fname = $vBranch->short_code."_CN".$system_date."A";
                      }elseif ($i == 2) {
                          $sts = 'Paid';
                          $fname = $vBranch->short_code."_CN".$system_date."P";
                      }elseif ($i == 3) {
                          $sts = 'Completed';
                          $fname = $vBranch->short_code."_CN".$system_date."C";
                      }

                      $param['data'] = $this->getQueryNominativeReport($sts);
                      $param['status'] = $sts;
                      $pdf = PDF::loadView('cetak.pdf_nominative_report', $param)->setPaper('a4', 'landscape');

                      $data = $pdf->output();
                      $file = fopen($destination_path_branch."/".$fname.".pdf", 'w');
                      fwrite($file, $data);
                      fclose($file);

                      Excel::create($fname, function($excel) use ($param){
                          $excel->sheet('Sheet 1',function($sheet) use ($param){
                              $sheet->loadView('cetak.xlsx_nominative_report', $param);
                          });
                      })->store('xlsx', $destination_path_branch);
                      #Store and export

                  }

                // MASTER COA
              }elseif ($type == 'coa') {
                  $param['data'] = 'waw';

                  $fname = $vBranch->short_code."_COA".$system_date;
                  Excel::create($fname, function($excel) use ($param){
                      $excel->sheet('Sheet 1',function($sheet) use ($param){
                          $sheet->loadView('cetak.xlsx_coa', $param);
                      });
                  })->store('xlsx', $destination_path_branch);

              }

              elseif ($type == 'due_date') {

                $param['data'] = json_decode($this->data_renewal_list_due_date($request));

                $fname = $vBranch->short_code."_RENEWAL_LIST_DUE_DATE".$system_date;
                Excel::create($fname, function($excel) use ($param){
                    $excel->sheet('Sheet 1',function($sheet) use ($param){
                        $sheet->loadView('cetak.xlsx_renewal_list_due_date', $param);
                    });
                })->store('xlsx', $destination_path_branch);

            }

            }

            return json_encode(['rc'=>3,'rm'=>'success','path'=>"RENEWAL_LIST_DUE_DATE".$system_date]);

    }


    public function getLabarugi($type,$branch)
    {
      $idbranch = 1;

          if($branch=='konsol') {

            $arrBranch = DB::table('master_branch')
                              ->select('id')
                              ->where('is_ho', 'f')
                              ->where('company_id', 1)
                              ->pluck('id')
                              ->toArray();

            $arrBranch = join(',', $arrBranch);

            $laba_rugi = \DB::select("SELECT
                                            coa_no,
                                            coa_name,
                                            balance_type_id,
                                            coa_type_id,
                                            CASE
                                              WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                            END AS last_os
                                      FROM (
                                              SELECT coa_no,
                                                      coa_name,
                                                      balance_type_id,
                                                      coa_type_id,
                                                      sum(last_os) AS last_os
                                              FROM
                                              master_coa
                                              WHERE coa_type_id IN (3,4) AND
                                                    is_active = true AND
                                                    company_id = " . 1 . "   AND
                                                    branch_id IN (" . $arrBranch . ") AND
                                                    LEFT(coa_no, 3) != '600'
                                              GROUP BY coa_no,
                                                  coa_name,
                                                  balance_type_id,
                                                  coa_type_id
                                              ORDER BY coa_no ASC
                                            ) a LEFT JOIN (
                                                              SELECT
                                                                coa_parent_id,
                                                                sum(coalesce(last_os, 0)) AS nom
                                                              FROM
                                                                master_coa
                                                              WHERE
                                                                coa_parent_id IS NOT NULL AND
                                                                is_active = true AND
                                                                company_id = " . 1 . " AND
                                                                branch_id IN (" . $arrBranch . ")
                                                              GROUP BY coa_parent_id
                                                          ) b ON a.coa_no = b.coa_parent_id
                                      ORDER BY coa_no ASC");

          }else{

            $laba_rugi = \DB::select("SELECT
                                            coa_no,
                                            coa_name,
                                            balance_type_id,
                                            coa_type_id,
                                            CASE
                                              WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                            END AS last_os
                                      FROM (
                                              SELECT coa_no,
                                                      coa_name,
                                                      balance_type_id,
                                                      coa_type_id,
                                                      sum(last_os) AS last_os
                                              FROM
                                              master_coa
                                              WHERE coa_type_id IN (3,4) AND
                                                    is_active = true AND
                                                    company_id = " . 1 . "   AND
                                                    branch_id IN (" . $idbranch . ") AND
                                                    LEFT(coa_no, 3) != '600'
                                              GROUP BY coa_no,
                                                  coa_name,
                                                  balance_type_id,
                                                  coa_type_id
                                              ORDER BY coa_no ASC
                                            ) a LEFT JOIN (
                                                              SELECT
                                                                coa_parent_id,
                                                                sum(coalesce(last_os, 0)) AS nom
                                                              FROM
                                                                master_coa
                                                              WHERE
                                                                coa_parent_id IS NOT NULL AND
                                                                is_active = true AND
                                                                company_id = " . 1 . " AND
                                                                branch_id IN (" . $idbranch . ")
                                                              GROUP BY coa_parent_id
                                                          ) b ON a.coa_no = b.coa_parent_id
                                      ORDER BY coa_no ASC");
          }

          return $laba_rugi;
    }

    public function getBalanceSheet($type,$branch)
    {
       // $idbranch = 'konsol';
       $idbranch = 1;

        if($branch=='konsol'){

          $arrBranch = DB::table('master_branch')
                            ->select('id')
                            ->where('is_ho', 'f')
                            ->where('company_id', 1)
                            ->pluck('id')
                            ->toArray();

          $arrBranch = join(',', $arrBranch);

          // Aktiva Paling Baru
          $aktiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . 1 . " and branch_id in (" . $arrBranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . 1 . " and branch_id in (" . $arrBranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 1
                                        and company_id = " . 1 . "
                                        and branch_id in (" . $arrBranch . ")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (select coa_no, sum(last_os) as last_os from master_coa where
                                        coa_parent_id is null
                                        and company_id = " . 1 ."
                                        and branch_id in (" . $arrBranch . ")
                                        and is_active = true
                                        group by coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no, sum(last_os)as last_os, coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . 1 . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                          group by coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

          // Pasiva Paling Baru
          $pasiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os +
                                  (with recursive cte as (
                                select
                                  a.coa_no,
                                  a.coa_no as coa_parent_id,
                                  coalesce(a.last_os, 0) as last_os
                                from
                                  (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . 1 . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                union all
                                select
                                  c.coa_no,
                                  p.coa_no,
                                  p.last_os
                                from
                                  cte c
                                join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . 1 . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                    using (coa_parent_id) )
                                select
                                  sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                from
                                  cte
                                where
                                  coa_no in ('400','500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 2
                                        and company_id = " . 1 ."
                                        and branch_id in (" . $arrBranch .")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . 1 . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . 1 . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

        }else{
          // BUKAN KONSOL

          // Aktiva Paling Baru
          $aktiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . 1 . " and branch_id in (" . $idbranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . 1 . " and branch_id in (" . $idbranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 1
                                        and company_id = " . 1 . "
                                        and branch_id in (" . $idbranch . ")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (select coa_no, sum(last_os) as last_os from master_coa where
                                        coa_parent_id is null
                                        and company_id = " . 1 ."
                                        and branch_id in (" . $idbranch . ")
                                        and is_active = true
                                        group by coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no, sum(last_os)as last_os, coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . 1 . "
                                          and branch_id in (" . $idbranch . ")
                                          and is_active = true
                                          group by coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

          // Pasiva Paling Baru
          $pasiva = \DB::SELECT("select
                                    coa_no,
                                    coa_name,
                                    sum(case when coa_no in ('301') then last_os +
                                    (with recursive cte as (
                                  select
                                    a.coa_no,
                                    a.coa_no as coa_parent_id,
                                    coalesce(a.last_os, 0) as last_os
                                  from
                                    (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . 1 . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                  union all
                                  select
                                    c.coa_no,
                                    p.coa_no,
                                    p.last_os
                                  from
                                    cte c
                                  join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . 1 . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                      using (coa_parent_id) )
                                  select
                                    sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                  from
                                    cte
                                  where
                                    coa_no in ('400','500', '600')) else last_os end )as last_os
                                  from
                                    (
                                    select
                                      a.coa_no,
                                      coa_name,
                                      d.coa_parent_id,
                                      case
                                        when a.coa_no = b.coa_parent_id then b.nom
                                        when a.coa_no = c.coa_no then c.last_os
                                        else coalesce(a.last_os, 0) end as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          coa_name,
                                          sum(last_os) as last_os,
                                          coa_type_id
                                        from
                                          master_coa
                                        where
                                          coa_type_id = 2
                                          and company_id = " . 1 ."
                                          and branch_id in (" . $idbranch .")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_name,
                                          coa_type_id ) a
                                      left join (
                                        select
                                          coa_parent_id,
                                          sum(coalesce(last_os, 0)) as nom
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is not null
                                          and length(coa_parent_id) = 6
                                        group by
                                          coa_parent_id ) b on
                                        a.coa_no = b.coa_parent_id
                                      left join (with recursive cte as (
                                        select
                                          a.coa_no,
                                          a.coa_no as coa_parent_id,
                                          coalesce(a.last_os, 0) as last_os
                                        from
                                          (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . 1 . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                      union all
                                        select
                                          c.coa_no,
                                          p.coa_no,
                                          p.last_os
                                        from
                                          cte c
                                        join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . 1 . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                            using (coa_parent_id) )
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          cte
                                        group by
                                          1) c on
                                        a.coa_no = c.coa_no
                                      left join (
                                        select
                                          distinct coa_no,
                                          case
                                            when coa_parent_id isnull then coa_no
                                            else coa_parent_id end
                                          from
                                            master_coa) d on
                                        a.coa_no = d.coa_no
                                      order by
                                        d.coa_parent_id,
                                        a.coa_no asc)x
                                  group by
                                    coa_no,
                                    coa_name ,
                                    coa_parent_id
                                  order by
                                    coa_parent_id,
                                    coa_no");


        } // end if


        if ($type == 'aktiva') {
          return $aktiva;
        }
        elseif ($type == 'pasiva') {
          return $pasiva;
        }

    }

    public function getQueryNominativeReport($filter)
    {
        switch ( $filter ) {
            case "All" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id
                            WHERE
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 0) OR
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 1) OR
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 1 AND is_splitted = true) AND
                                MS.branch_id = " . 1 . " AND
                                MS.company_id = " . 1 . "
                            ORDER BY MS.id ASC");
            break;
            case "Aktif" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id

                            WHERE
                                MS.wf_status_id = 9 AND MS.paid_status_id = 0 AND
                                MS.branch_id = " . 1 . " AND
                                MS.company_id = " . 1 . "
                            ORDER BY MS.id ASC");
            break;
            case "Paid" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id
                            WHERE
                                MS.wf_status_id = 9 AND MS.paid_status_id = 1 AND
                                MS.branch_id = " . 1 . " AND
                                MS.company_id = " . 1 . "
                            ORDER BY MS.id ASC");
            break;
            case "Completed" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id
                            WHERE
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 1 AND is_splitted = true) AND
                                MS.branch_id = " . 1 . " AND
                                MS.company_id = " . 1 . "
                            ORDER BY MS.id ASC");
            break;


        }

        return $data;
    }

    public function data_renewal_list_due_date()
    {
      $mc = collect(DB::select("SELECT due_date from master_company"))->first();

      $data = DB::select("SELECT ra.full_name, mc.full_name as nama_client, msp.police_name,
        rp.definition, msp.start_date_polis, msp.end_date_polis, ru.definition as uw_def, msp.polis_no,
        rv.deskripsi, msp.ins_amount, msp.net_amount, '' as netto_brokerage, msp.invoice_type_id, msp.id as id_msp,
      CURRENT_DATE + INTERVAL '".$mc->due_date." day' as hari_cuy
      from master_sales_polis msp
      left join ref_agent ra on ra.id = msp.agent_id
      left join master_customer mc on mc.id = msp.customer_id
      left join ref_product rp on rp.id = msp.product_id
      left join ref_underwriter ru on ru.id = msp.underwriter_id
      left join ref_valuta rv on rv.id = msp.valuta_id
      where msp.wf_status_id = 9 and msp.end_date_polis < CURRENT_DATE + INTERVAL '".$mc->due_date." day'");

      return json_encode(['data' => $data]);
    }


}
