<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Response;
use Hash;
use Auth;
use Excel;
use Request as Req;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class reffController extends Controller
{
    //
    public function refIndex(Request $request)
    {

        $fullURL = $request->url();
        $reffName = explode('ref/', $fullURL);

        switch ($reffName[1]) {
            case 'agent_type' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_agent_type WHERE company_id = ". Auth::user()->company_id ." ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RAT.id, RAT.definition, RAT.is_active, MC.company_name
                                            FROM ref_agent_type RAT LEFT JOIN master_company MC ON RAT.company_id = MC.id
                                            ORDER BY id ASC");
                }

                $view = 'reff.agent_type.index';
            break;
            case 'bank_account' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT RBA.id, RBA.definition, RBA.coa_no, RBA.is_active, MC.coa_name, MB.branch_name, MCOM.company_name
                                        FROM ref_bank_account RBA LEFT JOIN master_coa MC ON RBA.coa_id = MC.id
                                                                  LEFT JOIN master_branch MB ON RBA.branch_id = MB.id
                                                                  LEFT JOIN master_company MCOM ON RBA.company_id = MCOM.id
                                        WHERE RBA.company_id = ". Auth::user()->company_id ." ORDER BY RBA.id ASC");
                } else {
                    $results = \DB::select("SELECT RBA.id, RBA.definition, RBA.coa_no, RBA.is_active, MC.coa_name, MB.branch_name, MCOM.company_name
                                        FROM ref_bank_account RBA LEFT JOIN master_coa MC ON RBA.coa_id = MC.id
                                                                  LEFT JOIN master_branch MB ON RBA.branch_id = MB.id
                                                                  LEFT JOIN master_company MCOM ON RBA.company_id = MCOM.id
                                        ORDER BY RBA.id ASC");
                }

                $view = 'reff.bank_account.index';
            break;
            case 'city' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_city WHERE company_id = " . Auth::user()->company_id . " ORDER BY city_code ASC");
                } else {
                    $results = \DB::select("SELECT * FROM ref_city ORDER BY city_code ASC");
                }

                $view = 'reff.city.index';
            break;
            case 'coa_group' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_coa_group WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RCG.id, RCG.definition, RCG.is_active, MC.company_name
                                            FROM ref_coa_group RCG LEFT JOIN master_company MC ON RCG.company_id = MC.id
                                            ORDER BY id ASC");
                }

                $view = 'reff.coa_group.index';
            break;
            case 'coa_type' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_coa_type WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RCT.id, RCT.definition, RCT.is_active, MC.company_name
                                            FROM ref_coa_type RCT LEFT JOIN master_company MC ON RCT.company_id = MC.id
                                            ORDER BY id ASC");
                }
                $view = 'reff.coa_type.index';
            break;
            case 'customer_segment' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_cust_segment WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RCS.id, RCS.definition, RCS.is_active, MC.company_name
                                            FROM ref_cust_segment RCS LEFT JOIN master_company MC ON RCS.company_id = MC.id
                                            ORDER BY id ASC");
                }
                $view = 'reff.customer_segment.index';
            break;
            case 'customer_type' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_cust_type WHERE company_id = " . Auth::user()->company_id . "ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RCT.id, RCT.definition, RCT.is_active, MC.company_name
                                            FROM ref_cust_type RCT LEFT JOIN master_company MC ON RCT.company_id = MC.id
                                            ORDER BY id ASC");
                }

                // $results = \DB::select("SELECT * FROM ref_cust_type ORDER BY id ASC");
                $view = 'reff.customer_type.index';
            break;
            case 'doc_type' :
                $results = \DB::select("SELECT * FROM ref_doc_type ORDER BY id ASC");
                $view = 'reff.doc_type.index';
            break;
            case 'paid_status' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_paid_status WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RPS.id, RPS.definition, RPS.is_active, MC.company_name
                                            FROM ref_paid_status RPS LEFT JOIN master_company MC ON RPS.company_id = MC.id
                                            ORDER BY id ASC");
                }
                $view = 'reff.paid_status.index';
            break;
            case 'product' :
                $results = \DB::select("SELECT * FROM ref_product WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                $view = 'reff.product.index';
            break;
            case 'proposal' :
                $results = \DB::select("SELECT RP.id, RP.qs_no, RP.qs_date, RP.is_active, MU.fullname, MB.short_code, MB.branch_name
                                        FROM ref_proposal RP LEFT JOIN master_user MU ON RP.officer_id = MU.id
                                                             LEFT JOIN master_branch MB ON RP.branch_id = MB.id
                                        WHERE RP.company_id = " . Auth::user()->company_id . "
                                        ORDER BY RP.id ASC");
                $view = 'reff.proposal.index';
            break;
            case 'province' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_province WHERE company_id = " . Auth::user()->company_id . " ORDER BY province_code ASC");
                } else {
                    $results = \DB::select("SELECT RP.province_code, RP.province_name, RP.province_code, MC.company_name
                                            FROM ref_province RP JOIN master_company MC ON RP.company_id = MC.id
                                            ORDER BY province_code ASC");
                }
                $view = 'reff.province.index';
            break;
            case 'quotation' :
                $results = \DB::select("SELECT RQ.id, RQ.qs_no, RQ.qs_date, RQ.is_active, MU.fullname, MB.short_code, MB.branch_name
                                    FROM ref_quotation RQ LEFT JOIN master_user MU ON RQ.officer_id = MU.id
                                                          LEFT JOIN master_branch MB ON RQ.branch_id = MB.id
                                    ORDER BY RQ.id ASC");
                $view = 'reff.quotation.index';
            break;
            case 'segment' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_segment WHERE company_id = ". Auth::user()->company_id ." ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RS.id, RS.definition, RS.is_active, MC.company_name
                                            FROM ref_segment RS LEFT JOIN master_company MC ON RS.company_id = MC.id
                                            ORDER BY id ASC");
                }
                $view = 'reff.segment.index';
            break;
            case 'tx_type' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_tx_type WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RTT.id, RTT.definition, RTT.is_active, RTT.short_code, MC.company_name
                                            FROM ref_tx_type RTT LEFT JOIN master_company MC ON RTT.company_id = MC.id
                                            ORDER BY id ASC");
                }

                $view = 'reff.tx_type.index';
            break;
            case 'user_role' :
                $results = \DB::select("SELECT * FROM ref_user_role ORDER BY id ASC");
                $view = 'reff.user_role.index';
            break;
            case 'workflow' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select("SELECT * FROM ref_workflow_status WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                } else {
                    $results = \DB::select("SELECT RWS.id, RWS.definition, RWS.is_active, MC.company_name
                                            FROM ref_workflow_status RWS LEFT JOIN master_company MC ON RWS.company_id = MC.id
                                            ORDER BY id ASC");
                }
                $view = 'reff.workflow.index';
            break;
            case 'master_company' :
                $results = \DB::select("SELECT *
                                        FROM master_company MC JOIN ref_province RP ON RP.province_code = MC.province_id
                                                               JOIN ref_city RC ON RC.city_code = mc.city_id
                                        WHERE MC.id = " . Auth::user()->company_id . "
                                        ORDER BY MC.id ASC");
                $view = 'reff.master_company.index';
            break;
            case 'master_branch' :
                if ( Auth::user()->user_role_id != 5) {
                    // Selain User Role Admin
                    $results = \DB::select("SELECT *
                                            FROM master_branch MB LEFT JOIN ref_province RP ON RP.province_code = MB.province_id
                                                                  LEFT JOIN ref_city RC ON RC.city_code = MB.city_id
                                                                  WHERE MB.company_id = " . Auth::user()->company_id . "
                                                                  ORDER BY MB.id ASC");
                } else {
                    $results = \DB::select("SELECT * FROM master_branch MB LEFT JOIN ref_province RP ON RP.province_code = MB.province_id LEFT JOIN ref_city RC ON RC.city_code = MB.city_id ORDER BY MB.id ASC");
                }

                $view = 'reff.master_branch.index';
            break;
            case 'master_config' :
                $results = \DB::select("SELECT *
                                        FROM master_config
                                        WHERE company_id = " . Auth::user()->company_id . " ORDER BY id ASC");
                $view = 'reff.master_config.index';
            break;
            case 'master_workflow' :
                if ( Auth::user()->user_role_id != 5) {
                    $results = \DB::select("SELECT
                                            mw.company_id, mc.company_name,
                                            ur.id, ur.definition user_role,
                                            mw.current_status_id, rws.definition current_status_name,
                                            mw.next_status_id, rws1.definition next_status_name,
                                            mw.prev_status_id, rws2.definition prev_status_name
                                        FROM
                                            master_workflow mw
                                            LEFT JOIN ref_workflow_status rws ON mw.current_status_id = rws.id
                                            LEFT JOIN ref_workflow_status rws1 ON mw.next_status_id = rws1.id
                                            LEFT JOIN ref_workflow_status rws2 ON mw.prev_status_id = rws2.id
                                            LEFT JOIN master_company mc ON mw.company_id = mc.id
                                            LEFT JOIN ref_user_role ur ON ur.id = mw.user_role_id
                                        WHERE mw.company_id = " . Auth::user()->company_id);
                } else {
                    $results = \DB::select("SELECT
                                            mw.company_id, mc.company_name,
                                            ur.id, ur.definition user_role,
                                            mw.current_status_id, rws.definition current_status_name,
                                            mw.next_status_id, rws1.definition next_status_name,
                                            mw.prev_status_id, rws2.definition prev_status_name
                                        FROM
                                            master_workflow mw
                                            LEFT JOIN ref_workflow_status rws ON mw.current_status_id = rws.id
                                            LEFT JOIN ref_workflow_status rws1 ON mw.next_status_id = rws1.id
                                            LEFT JOIN ref_workflow_status rws2 ON mw.prev_status_id = rws2.id
                                            LEFT JOIN master_company mc ON mw.company_id = mc.id
                                            LEFT JOIN ref_user_role ur ON ur.id = mw.user_role_id");
                }

                $view = 'reff.master_workflow.index';
            break;
            case 'master_user' :
                if ( Auth::user()->user_role_id != 5) {
                    $results = \DB::select("SELECT MU.id, MU.username, MU.fullname, MU.is_active, MB.short_code, MB.branch_name, MC.company_name, RUR.definition, MU.user_role_id
                                            FROM master_user MU LEFT JOIN master_branch MB ON MU.branch_id = MB.id
                                                                LEFT JOIN master_company MC ON MC.id = MU.company_id
                                                                LEFT JOIN ref_user_role RUR ON RUR.id = MU.user_role_id
                                                                WHERE MU.company_id = " . Auth::user()->company_id . "
                                                                ORDER BY MU.id DESC");
                } else {
                    $results = \DB::select("SELECT MU.id, MU.username, MU.fullname, MU.is_active, MB.branch_name, MC.company_name, RUR.definition, MU.user_role_id
                                            FROM master_user MU LEFT JOIN master_branch MB ON MU.branch_id = MB.id
                                                                LEFT JOIN master_company MC ON MC.id = MU.company_id
                                                                LEFT JOIN ref_user_role RUR ON RUR.id = MU.user_role_id
                                                                     ORDER BY MU.id DESC");
                }

                $view = 'reff.master_user.index';
            break;
            case 'inventory' :
                $results = \DB::select("SELECT * FROM ref_inventory ORDER BY id ASC");
                $view = 'reff.inventory.index';
            break;
            case 'master_coa' :
                if ( Auth::user()->user_role_id != 5) {
                    // $results = \DB::select("SELECT MC.id, MC.is_active, MC.coa_no, MC.coa_name, MC.balance_type_id, MC.last_os, RCF.definition
                    //                     FROM master_coa MC
                    //                     LEFT JOIN ref_coa_type RCF ON MC.coa_type_id = RCF.id
                    //                     WHERE RCF.is_active = 't' AND MC.company_id = " . Auth::user()->company_id . " AND MC.branch_id = " . Auth::user()->branch_id ."
                    //                     ORDER BY id ASC");

                    $results = \DB::select("SELECT DISTINCT
                                                MC.id,
                                                MC.is_active,
                                                MC.coa_no,
                                                MC.coa_name,
                                                MC.balance_type_id,
                                                MC.last_os,
                                                RCF.definition AS coa_type,
                                                RCG.definition AS coa_group,
                                                MC.coa_parent_id,
                                                MCP.coa_name AS coa_parent
                                            FROM
                                                master_coa MC   LEFT JOIN ref_coa_type RCF ON MC.coa_type_id = RCF.id
                                                                LEFT JOIN ref_coa_group RCG ON RCG.id = MC.coa_group_id
                                                                LEFT JOIN master_coa MCP ON MC.coa_parent_id = MCP.coa_no
                                            WHERE
                                                RCF.is_active = 't' AND
                                                MC.company_id = " . Auth::user()->company_id . " AND
                                                MC.branch_id = " . Auth::user()->branch_id . "
                                            ORDER BY
                                                MC.id ASC");



                } else {
                    // $results = \DB::select("SELECT MC.id, MC.is_active, MC.coa_no, MC.coa_name, MC.balance_type_id, MC.last_os, RCF.definition
                    //                     FROM master_coa MC
                    //                     LEFT JOIN ref_coa_type RCF ON MC.coa_type_id = RCF.id
                    //                     WHERE RCF.is_active = 't'
                    //                     ORDER BY id ASC");
                    $results = \DB::select("SELECT DISTINCT
                                                MC.id,
                                                MC.is_active,
                                                MC.coa_no,
                                                MC.coa_name,
                                                MC.balance_type_id,
                                                MC.last_os,
                                                RCF.definition AS coa_type,
                                                RCG.definition AS coa_group,
                                                MC.coa_parent_id,
                                                MCP.coa_name AS coa_parent
                                            FROM
                                                master_coa MC   LEFT JOIN ref_coa_type RCF ON MC.coa_type_id = RCF.id
                                                                LEFT JOIN ref_coa_group RCG ON RCG.id = MC.coa_group_id
                                                                LEFT JOIN master_coa MCP ON MC.coa_parent_id = MCP.coa_no
                                            WHERE
                                                RCF.is_active = 't'
                                            ORDER BY
                                                MC.id ASC");

                }

                $view = 'reff.master_coa.index';
            break;
            case 'customer_group' :
                if ( Auth::user()->user_role_id != 5) {
                    $results = \DB::select('SELECT RCG.id, RCG.group_name, RCG.address, MP.company_name, RCG.phone_no, RCG.is_active
                                        FROM "ref_cust_group" RCG LEFT JOIN master_company MP ON RCG.company_id = MP."id"
                                        WHERE RCG.company_id = ' . Auth::user()->company_id . '
                                        ORDER BY RCG.id ASC');
                } else {
                    $results = \DB::select('SELECT RCG.id, RCG.group_name, RCG.address, MP.company_name, RCG.phone_no, RCG.is_active
                                        FROM "ref_cust_group" RCG LEFT JOIN master_company MP ON RCG.company_id = MP."id"
                                        ORDER BY RCG.id ASC');
                }

                $view = 'reff.customer_group.index';
            break;
            case 'bdd' :
                if ( Auth::user()->user_role_id != 5) {
                    $results = \DB::select('SELECT RB.id, RB.is_active, RB.bdd_name, RB.coa_no, RB.coa_no_amor, MB.branch_name, MB.short_code
                                            FROM ref_bdd RB LEFT JOIN master_branch MB ON RB.branch_id = MB.id
                                            WHERE RB.company_id = ' . Auth::user()->company_id . ' ORDER BY RB.id ASC');
                } else {
                    $results = \DB::select('SELECT RB.id, RB.is_active, RB.bdd_name, RB.coa_no, RB.coa_no_amor, MB.branch_name
                                            FROM ref_bdd RB LEFT JOIN master_branch MB ON RB.branch_id = MB.id
                                            ORDER BY RB.id ASC');
                }

                $view = 'reff.bdd.index';
            break;
            case 'agent' :
                if ( Auth::user()->user_role_id != 5 ) {
                    $results = \DB::select('SELECT
                                                RA.id,
                                                RA.full_name,
                                                RA.is_active,
                                                RA.code_name,
                                                RA.address,
                                                RA.phone_no,
                                                RA.email
                                            FROM ref_agent RA
                                            WHERE
                                                RA.company_id = ' . Auth::user()->company_id . '
                                            ORDER BY RA.id ASC');
                } else {
                    $results = \DB::select('SELECT
                                                RA.id,
                                                RA.full_name,
                                                RA.is_active,
                                                RA.code_name,
                                                RA.address,
                                                RA.phone_no,
                                                RA.email,
                                                RAP.full_name AS officer
                                            FROM ref_agent RA LEFT JOIN ref_agent RAP ON RA.id = RAP.parent_id
                                            ORDER BY RA.id ASC');
                }
                $view = 'reff.agent.index';
            break;
            case 'branch_monitoring' :
                if ( Auth::user()->user_role_id != 5) {
                    // Role Selain super admin
                    if ( Auth::user()->user_role_id == 7 ) {
                        // Admin Cabang
                        $results = \DB::select("SELECT *
                                                FROM master_branch MB JOIN ref_province RP ON RP.province_code = MB.province_id
                                                                    JOIN ref_city RC ON RC.city_code = MB.city_id
                                                                    WHERE MB.company_id = " . Auth::user()->company_id . " AND MB.id = " . Auth::user()->branch_id . "
                                                                    ORDER BY MB.id ASC");
                    } else {
                        // Bukan Admin Cabang
                        $results = \DB::select("SELECT *
                                                FROM master_branch MB JOIN ref_province RP ON RP.province_code = MB.province_id
                                                                    JOIN ref_city RC ON RC.city_code = MB.city_id
                                                                    WHERE MB.company_id = " . Auth::user()->company_id . "
                                                                    ORDER BY MB.id ASC");
                    }

                } else {
                    $results = \DB::select("SELECT * FROM master_branch MB JOIN ref_province RP ON RP.province_code = MB.province_id JOIN ref_city RC ON RC.city_code = MB.city_id ORDER BY MB.id ASC");
                }
                $view = 'reff.master_branch.monitoring_index';
            break;
            case 'menu' :
                $results = \DB::select("SELECT * FROM ref_user_role ORDER BY id ASC");
                $view = 'reff.menu.index';
            break;
            case 'sub_menu' :
                $results = \DB::select("SELECT * FROM ref_menu RM JOIN ref_sub_menu RSM ON RM.id = RSM.idmenu ORDER BY RM.id ASC");
                $view = 'reff.sub_menu.index';
            break;
            case 'manajemen_user':
                $results = \DB::select("SELECT MU.id, RUR.definition, RSM.submenu
                                        FROM manajemen_user MU
                                            JOIN ref_user_role RUR ON MU.idrole = RUR.id
                                            JOIN ref_sub_menu RSM ON RSM.id = MU.id_sub_menu
                                            ORDER BY MU.id ASC");
                $view = 'reff.manajemen_user.index';
            break;
            case 'underwriter':
                $results = \DB::select("SELECT
                                            RU.*,
                                            MB.short_code,
                                            MB.branch_name
                                        FROM ref_underwriter RU LEFT JOIN master_branch MB ON RU.branch_id = MB.id
                                        WHERE RU.company_id = " . Auth::user()->company_id . " ORDER BY RU.id ASC");
                // $results = \DB::select("SELECT RU.id, RU.definition, RU.is_active, MB.branch_name
                //                         FROM ref_underwriter RU LEFT JOIN master_branch MB ON RU.branch_id = MB.id
                //                         WHERE RU.company_id = " . Auth::user()->company_id . " ORDER BY RU.id ASC");

                $view = 'reff.underwriter.index';
            break;
            case 'qs_content' :
                $results = DB::TABLE('ref_qs_content')
                            ->select(
                                'ref_qs_content.*',
                                'ref_product.definition'
                            )
                            ->leftJoin('ref_product', 'ref_product.id', '=', 'ref_qs_content.product_id')
                            ->orderBy('id', 'DESC')
                            ->get();
                $view = 'reff.qs_content.index';
            break;
            case 'branch_customer' :
                $results = DB::TABLE('master_branch_cust')
                                ->leftJoin('ref_city', 'ref_city.city_code', '=', 'master_branch_cust.city_id')
                                ->leftJoin('ref_province', 'ref_province.province_code', '=', 'master_branch_cust.province_id')
                                ->orderBy('master_branch_cust.id', 'DESC')
                                ->get();

                $view = 'reff.branch_customer.index';
            break;
            case 'insured_detail' :
                $results = DB::TABLE('ref_insured_detail')
                                ->select(
                                    'ref_insured_detail.*',
                                    'ref_product.definition'
                                )
                                ->leftJoin('ref_product', 'ref_product.id', '=', 'ref_insured_detail.product_id')
                                ->orderBy('ref_insured_detail.product_id', 'ASC')
                                ->orderBy('ref_insured_detail.bit_id', 'ASC')
                                ->get();

                $view = 'reff.insured_detail.index';
            break;
            case 'rate_usia' :
                $results = DB::TABLE('ref_rate_usia')
                                ->orderBy('usia', 'ASC')
                                ->orderBy('tenor', 'ASC')
                                ->get();

                $view = 'reff.rate_usia.index';
            break;
        }

       $param['data'] = $results;

       if (Req::ajax()) {
            return view('master.only_content')->nest('child', $view, $param);
       } else {
            return view('master.master')->nest('child', $view, $param);
       }


    }

    public function getCity(Request $request)
    {
        $provCode = $request->input('province_code');
        $refCity = DB::table("ref_city")->where('province_code', $provCode)->get();
        return response()->json([
            'rc' => 1,
            'data' => $refCity
        ]);
    }

    public function getCoaNo(Request $request)
    {
        $idBranch = $request->input('id_branch');
        $dtMasterCOA = DB::table('master_coa')
                        ->where('is_active', 't')
                        ->where('is_parent', 'f')
                        ->where('branch_id', $idBranch)
                        ->orderBy('coa_no', 'ASC')
                        ->get();
        return response()->json([
            'rc' => 1,
            'data' => $dtMasterCOA
        ]);
    }

    public function exportHistoryTx(Request $request)
    {
        $coaNo = $request->input('coaNo');
        $tglMulai = join('/', array_reverse(explode('/', $request->input('tglMulai'))));
        $tglSelesai = join('/', array_reverse(explode('/', $request->input('tglSelesai'))));

        $param['data'] = DB::table('master_tx')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->whereNotNull('seq_approval')
                        ->where('coa_id', $coaNo)
                        ->whereBetween('tx_date', [$tglMulai, $tglSelesai])
                        ->where(function($query){
                            $query
                                ->where('id_workflow', 9)
                                  ->orWhere('id_workflow', 3)
                                    ->orWhere('id_workflow', 12)
                                    ->orWhere('id_workflow', 15);
                        })
                        ->orderBy('id', 'asc')
                        ->get();

        $param['coa'] = DB::table('master_coa')->select('coa_no', 'coa_name')->where('coa_no', $coaNo)->first();
        $param['tglMulai'] = date('d F Y', strtotime($tglMulai));
        $param['tglSelesai'] = date('d F Y', strtotime($tglSelesai));

        $pdf = PDF::loadView('reff.master_coa.pdf_history_tx',$param)->setPaper('a4', 'potrait');
        return $pdf->stream('History Transaksi_' . date('dmY_His') . '.pdf');
    }

    public function exportHistoryTxExcel(Request $request)
    {
        $coaNo = $request->input('coaNo');
        $tglMulai = join('/', array_reverse(explode('/', $request->input('tglMulai'))));
        $tglSelesai = join('/', array_reverse(explode('/', $request->input('tglSelesai'))));

        $param['data'] = DB::table('master_tx')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->whereNotNull('seq_approval')
                        ->where('coa_id', $coaNo)
                        ->whereBetween('tx_date', [$tglMulai, $tglSelesai])
                        ->where(function($query){
                            $query
                                ->where('id_workflow', 9)
                                  ->orWhere('id_workflow', 3)
                                    ->orWhere('id_workflow', 12)
                                    ->orWhere('id_workflow', 15);
                        })
                        ->orderBy('id', 'asc')
                        ->get();

        $param['coa'] = DB::table('master_coa')->select('coa_no', 'coa_name')->where('coa_no', $coaNo)->first();
        $param['tglMulai'] = date('d F Y', strtotime($tglMulai));
        $param['tglSelesai'] = date('d F Y', strtotime($tglSelesai));


        $fname = 'History_Transaksi_' . date('dmY_His');

        Excel::create($fname, function($excel) use ($param){
            $excel->sheet('Sheet 1',function($sheet) use ($param){
                $sheet->setColumnFormat(array('B' => '#0'));
                $sheet->loadView('reff.master_coa.excel_history_tx', $param);
            });
        })->export('xlsx');

        // $pdf = PDF::loadView('reff.master_coa.pdf_history_tx',$param)->setPaper('a4', 'potrait');
        // return $pdf->stream('History Transaksi_' . date('dmY_His') . '.pdf');
    }

    public function getCOAParent(Request $request)
    {
        $id = $request->input('coa_group_id');

        if ( $id == 5 ) {
            // BEBAN
            $COA = DB::table('master_coa')
                ->where('coa_group_id', $id)
                ->where('coa_parent_id', '500')
                ->where('company_id', Auth::user()->company_id)
                ->where('branch_id', Auth::user()->branch_id)
                ->get();
        } else {
            $COA = DB::table('master_coa')
                ->where('coa_group_id', $id)
                ->whereNull('coa_parent_id')
                ->where('company_id', Auth::user()->company_id)
                ->where('branch_id', Auth::user()->branch_id)
                ->get();
        }

        return response()->json([
            'rc' => 1,
            'data' => $COA
        ]);
    }

    public function getCompany(Request $request)
    {
        $branchId = $request->input('branch_id');
        $branch = DB::table('master_branch')->where('id', $branchId)->first();
        if (is_null($branchId)) {
            $reffCompany = "";
        } else {
            $reffCompany = DB::table("master_company")->where('id', $branch->company_id)->where('is_active', 't')->get();
        }

        return response()->json([
            'rc' => 1,
            'data' => $reffCompany
        ]);
    }

    public function getBranch(Request $request)
    {
        $id_company = $request->input('id_company');
        if ( $id_company != "null" ) {
            $dtBranch = DB::table('master_branch')->where('company_id', $id_company)->get();
        } else {
            $dtBranch = "";
        }
        return response()->json([
            'rc' => 1,
            'data' => $dtBranch
        ]);
    }

    public function getBranchBankAccount(Request $request)
    {
        $id_company = $request->input('id_company');
        if ( $id_company != "null" ) {
            $dtBranch = DB::table('master_branch')->where('company_id', $id_company)->get();
        } else {
            $dtBranch = "";
        }
        return response()->json([
            'rc' => 1,
            'data' => $dtBranch
        ]);
    }

    public function resetPassUser($id) {
        DB::table('master_user')->where('id', $id)->update([
            'password' => bcrypt('welcome1'),
            'is_login' => 'f'
        ]);

        return response()->json([
            'rc' => 1,
            'rm' => 'Password berhasil direset'
        ]);
    }

    public function refCityStore(Request $request)
    {
        $id = $request->input('id'); // city id
        $cityCode = $request->input('city_code');
        $cityName = $request->input('city_name');
        $provId = $request->input('province_id');
        $seq = $request->input('seq');
        $companyId = $request->input('id_company');

        if ( is_null($companyId) ) {
            $companyId = Auth::user()->company_id;
        }

        if (is_null($id)) {
            // insert data

            // validate kode kota
            $sameCity = DB::table('ref_city')->where('city_code', $cityCode)
                                 ->where('province_code', $provId)
                                 ->first();

            if (!is_null($sameCity)) {
                return response()->json([
                    'rc' => 1,
                    'rm' => "Kode kota sudah digunakan"
                ]);
            }

            // get last seq
            $lastSeq = DB::SELECT('SELECT seq FROM ref_city ORDER BY seq DESC LIMIT 1');
            if (empty($lastSeq)) {
                $newSeq = 1;
            } else {
                $newSeq = $lastSeq[0]->seq + 1;
            }

            DB::table('ref_city')->insert([
                'province_code' => $provId,
                'city_code' => $cityCode,
                'city_name' => strtoupper($cityName),
                'seq' => $newSeq,
                'company_id' => $companyId
            ]);

            return response()->json([
                'rc' => 0,
                'rm' => "Data Berhasil Disimpan"
            ]);
        } else {
            // update data

            // delete city
            DB::table('ref_city')->where('city_code', $id)->delete();

            DB::table('ref_city')->insert([
                'province_code' => $provId,
                'city_code' => $cityCode,
                'city_name' => strtoupper($cityName),
                'seq' => $seq,
                'company_id' => $companyId
            ]);

            return response()->json([
                'rc' => 0,
                'rm' => "Data Berhasil Diperbaharui"
            ]);
        }
    } //  end function

    public function refProvinceStore(Request $request)
    {
        $id = $request->input('id'); // province id
        $provinceCode = $request->input('province_code');
        $provinceName = $request->input('province_name');
        $companyId = $request->input('id_company');

        if ( is_null($companyId) ) {
            $companyId = Auth::user()->company_id;
        }

        if (is_null($id)) {
            // insert data

            // validate kode kota
            $sameProvince = DB::table('ref_province')
                                 ->where('province_code', $provinceCode)
                                 ->first();

            if (!is_null($sameProvince)) {
                return response()->json([
                    'rc' => 1,
                    'rm' => "Kode provinsi sudah digunakan"
                ]);
            }

            // get last seq
            $lastSeq = DB::SELECT('SELECT seq FROM ref_province ORDER BY seq DESC LIMIT 1');

            if (empty($lastSeq)) {
                $newSeq = 1;
            } else {
                $newSeq = $lastSeq[0]->seq + 1;
            }

            DB::table('ref_province')->insert([
                'province_code' => $provinceCode,
                'province_name' => strtoupper($provinceName),
                'seq' => $newSeq,
                'company_id' => $companyId
            ]);

            return response()->json([
                'rc' => 0,
                'rm' => "Data Berhasil Disimpan"
            ]);
        } else {
            // update data
            $seq = $request->input('seq');

            // delete city
            DB::table('ref_province')->where('province_code', $id)->delete();

            DB::table('ref_province')->insert([
                'province_code' => $provinceCode,
                'province_name' => $provinceName,
                'seq' => $seq,
                'company_id' => $companyId
            ]);

            return response()->json([
                'rc' => 0,
                'rm' => "Data Berhasil Diperbaharui"
            ]);
        }
    } //  end function

    public function storeParentCOA(Request $request)
    {
        $countCOAParent = DB::table('master_coa')
                        ->where('is_active', 't')
                        ->where('is_parent', 't')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('company_id', Auth::user()->company_id)
                        ->count();

        if ( $countCOAParent <= 0) {
            $dtParentCOA = DB::table('master_coa')->where('is_active', 't')->where('branch_id', '1')->where('is_parent', 't')->get();
            foreach ($dtParentCOA as $item) {

                // get last id
                $lastData = DB::SELECT('SELECT MAX(id) AS id FROM master_coa');
                if (empty($lastData[0]->id)) {
                    $newId = 1;
                } else {
                    $newId = $lastData[0]->id + 1;
                }

                $dataInsert = [
                    'id' => $newId,
                    'company_id' => Auth::user()->company_id,
                    'branch_id' => Auth::user()->branch_id,
                    'coa_no' => $item->coa_no,
                    'valuta_code' => $item->valuta_code,
                    'coa_name' => $item->coa_name,
                    'coa_type_id' => $item->coa_type_id,
                    'is_parent' => 't',
                    'coa_group_id' => $item->coa_group_id,
                    'balance_type_id' => $item->balance_type_id,
                    'user_crt_id' => Auth::user()->id,
                    'created_at' => date('Y-m-d h:i:s'),
                    'last_os' => $item->last_os,
                    'coa_parent_id' => $item->coa_parent_id,
                    'is_active' => $item->is_active
                ];

                DB::table('master_coa')->insert($dataInsert);
            }

            $rc = 0;
            $message = "COA parent berhasil dibuat";
        } else {
            $rc = 1;
            $message = "COA parent sudah ada";
        }

        return response()->json([
            'rc' => $rc,
            'rm' => $message
        ]);
    }

    public function checkCOAParent(Request $request)
    {
        $countCOAParent = DB::table('master_coa')
                        ->where('is_active', 't')
                        // ->where('is_parent', 't')
                        ->whereNull('coa_parent_id')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('company_id', Auth::user()->company_id)
                        ->count();

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses!',
            'jmlParent' => $countCOAParent
        ]);
    }

    public function refDelete(Request $request)
    {
        $reffOrder = $request->input('ref_order');
        switch ($reffOrder) {
            // delete city
            case '11' :
                $cityCode = $request->input('id');
                $provCode = $request->input('id_province');
                DB::table('ref_city')->where('city_code', $cityCode)
                                    ->where('province_code', $provCode)
                                    ->delete();
            break;
            case '12' :
                $provCode = $request->input('id');
                DB::table('ref_province')->where('province_code', $provCode)->delete();
            break;
            case '20' :
                $idCompany = $request->input('id');
                $idCurrStatus = $request->input('id_current_status');
                DB::table('master_workflow')->where('company_id', $idCompany)
                                    ->where('current_status_id', $idCurrStatus)
                                    ->delete();
            break;
            case "26" :
                $id = $request->input('id_menu');
                DB::table('ref_menu')->where('id', $id)->delete();
            break;
            case "27" :
                $id = $request->input('id');
                DB::table('ref_sub_menu')->where('id', $id)->delete();
            break;
            case "28" :
                $id = $request->input('id');
                DB::table('manajemen_user')->where('id', $id)->delete();
            break;
            case "101":
                $id = $request->input('id');
                $productId = $request->product_id;
                DB::TABLE('ref_insured_detail')->where('id', $id)->delete();
            break;
            case "102":
                $id = $request->input('id');
                DB::TABLE('ref_rate_usia')->where('id', $id)->delete();
            break;
        }

        return response()->json([
            'rc' => 0,
            'rm' => "Data Berhasil Dihapus"
        ]);
    }

    public function refStore(Request $request)
    {

        $refOrder = $request->input('ref_order');
        $id = $request->input('id');
        if (is_null($id)) {
            // insert data

            switch ($refOrder) {
                case "1" :
                    // insert agent type

                    $definition = $request->input('definition');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        // insert data from admin pusat
                        $idCompany = Auth::user()->company_id;
                    }

                    // get seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_agent_type');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    // get last id
                    $lastId = DB::SELECT('SELECT MAX(id) AS id FROM ref_agent_type');
                    if (empty($lastId[0]->id)) {
                        $newId = 1;
                    } else {
                        $newId = $lastId[0]->id + 1;
                    }

                    DB::table('ref_agent_type')->insert([
                        'id' => $newId,
                        'definition' => $definition,
                        'is_active' => 't',
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                    ]);
                break;

                // insert bank account
                case "2" :
                    $bankName = $request->input('bank');
                    $idCompany = $request->input('id_company');
                    $coaId = $request->input('coa_no');
                    $branchId = $request->input('branch_id');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get coa no
                    $data = DB::SELECT('SELECT coa_no FROM master_coa WHERE id='.$coaId);

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_bank_account');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    DB::table('ref_bank_account')->insert([
                        'definition' => $bankName,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't',
                        'coa_id' => $coaId,
                        'coa_no' => $data[0]->coa_no,
                        'branch_id' => $branchId
                    ]);
                break;

                // insert user role
                case "3" :
                    $userRole = $request->input('user_role');
                    $idCompany = $request->input('id_company');

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_user_role');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    DB::table('ref_user_role')->insert([
                        'definition' => $userRole,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert coa group
                case "4" :
                    $coa = $request->input('coa_group');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_coa_group');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    DB::table('ref_coa_group')->insert([
                        'definition' => $coa,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert coa type
                case "5" :

                    $coaType = $request->input('coa_type');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_coa_type');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    DB::table('ref_coa_type')->insert([
                        'definition' => $coaType,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert coa segment
                case "6" :
                    $coaSegment = $request->input('coa_segment');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_segment');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    DB::table('ref_segment')->insert([
                        'definition' => $coaSegment,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert cust type
                case "7" :
                    $custType = $request->input('customer_type');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastData = DB::SELECT('SELECT MAX(id) AS id, MAX(seq) AS seq FROM ref_cust_type');
                    if (empty($lastData[0]->id) && empty($lastData[0]->seq)) {
                        $newSeq = 1;
                        $newId = 1;
                    } else {
                        $newSeq = $lastData[0]->seq + 1;
                        $newId = $lastData[0]->id + 1;
                    }

                    DB::table('ref_cust_type')->insert([
                        'id' => $newId,
                        'definition' => $custType,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert customer segment
                case "8" :
                    $custSegment = $request->input('customer_segment');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_cust_segment');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    DB::table('ref_cust_segment')->insert([
                        'definition' => $custSegment,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert paid status
                case "9" :
                    $paidSts = $request->input('paid_status');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastData = DB::SELECT('SELECT MAX(id) AS id, MAX(seq) AS seq FROM ref_paid_status');
                    if (empty($lastData[0]->seq) && empty($lastData[0]->id)) {
                        $newSeq = 1;
                        $newId = 1;
                    } else {
                        $newSeq = $lastData[0]->seq + 1;
                        $newId = $lastData[0]->id + 1;
                    }

                    DB::table('ref_paid_status')->insert([
                        'id' => $newId,
                        'definition' => $paidSts,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert workflow status
                case "10" :
                    $wfSts = $request->input('workflow_status');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastData = DB::SELECT('SELECT MAX(id) AS id, MAX(seq) AS seq FROM ref_workflow_status');
                    if (empty($lastData[0]->seq) && empty($lastData[0]->id)) {
                        $newSeq = 1;
                        $newId = 1;
                    } else {
                        $newSeq = $lastData[0]->seq + 1;
                        $newId = $lastData[0]->id + 1;
                    }

                    DB::table('ref_workflow_status')->insert([
                        'id' => $newId,
                        'definition' => $wfSts,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;

                // insert tx type
                case "13" :

                    $definition = $request->input('definition');
                    $sortCode = $request->input('short_code');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get last seq
                    $lastData = DB::SELECT('SELECT MAX(id) AS id, MAX(seq) AS seq FROM ref_tx_type');
                    if (empty($lastData[0]->id) && empty($lastData[0]->seq)) {
                        $newSeq = 1;
                        $newId = 1;
                    } else {
                        $newSeq = $lastData[0]->seq + 1;
                        $newId = $lastData[0]->id + 1;
                    }

                    DB::table('ref_tx_type')->insert([
                        'id' => $newId,
                        'definition' => $definition,
                        'seq' => $newSeq,
                        'company_id' => $idCompany,
                        'short_code' => $sortCode,
                        'is_active' => 't'
                    ]);
                break;

                // insert company
                case "14" :

                    $companyName = $request->input('company_name');
                    $address = $request->input('address');
                    $provId = $request->input('province_id');
                    $cityId = $request->input('city_id');
                    $longitude = $request->input('longitude');
                    $latitude = $request->input('latitude');
                    $shortCode = $request->input('short_code');
                    $leaderName = $request->input('leader_name');

                    $user = Auth::user();

                    DB::table('master_company')->insert([
                        'company_name' => $companyName,
                        'address' => $address,
                        'city_id' => $cityId,
                        'province_id' => $provId,
                        'long' => $longitude,
                        'lat' => $latitude,
                        'short_code' => strtoupper($shortCode),
                        'leader_name' => $leaderName,
                        'user_crt_id' => $user->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'is_active' => 't'
                    ]);
                break;

                // insert branch
                case "15" :

                    $branchName = $request->input('branch_name');
                    $address = $request->input('address');
                    $provId = $request->input('province_id');
                    $cityId = $request->input('city_id');
                    // $longitude = $request->input('longitude');
                    // $latitude = $request->input('latitude');
                    $latitude = NULL;
                    $longitude = NULL;
                    $shortCode = $request->input('short_code');
                    $leaderName = $request->input('leader_name');
                    $jabatan = $request->input('jabatan');
                    $isHO = false;


                    if ( !is_null($request->input('is_ho')) ) {

                        if ( $request->input('is_ho') == 1 ) {
                            $isHO = true;
                        } else {
                            $isHO = false;
                        }

                    }

                    $user = Auth::user();

                    DB::table('master_branch')->insert([
                        'branch_name' => $branchName,
                        'address' => $address,
                        'city_id' => $cityId,
                        'province_id' => $provId,
                        'long' => $longitude,
                        'lat' => $latitude,
                        'short_code' => strtoupper($shortCode),
                        'leader_name' => $leaderName,
                        'user_crt_id' => $user->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'is_active' => 't' ,
                        'company_id' => $user->company_id,
                        'jabatan' => $jabatan,
                        'is_ho' => $isHO
                    ]);
                break;

                // insert master config
                case "16" :

                    $parmName = $request->input('parm_name');
                    $val = $request->input('value');
                    $idCompany = Auth::user()->company_id;

                    DB::table('master_config')->insert([
                        'parm_name' => $parmName,
                        'value' => $val,
                        'is_active' => 't',
                        'company_id' => $idCompany
                    ]);
                break;

                // insert master user
                case "17" :

                    if ( $request->user_type == '2' && $request->role == '11') {
                        // User Client
                        $id_customer = $request->customer;
                        $dataCust = DB::TABLE('master_customer')
                                        ->where('id', $id_customer)
                                        ->first();

                        $branch_customer = $dataCust->cust_branch_id;
                    } else {
                        $id_customer = NULL;
                        $branch_customer = NULL;
                    }

                    $user = Auth::user();
                    $userName = $request->input('username');
                    $fullName = $request->input('fullname');
                    $branchId = $request->input('branch_id');
                    $roleId = $request->input('role_id');
                    $shortCode = $request->input('short_code');

                    if ( $request->input('id_company') == null) {
                        // admin insert user baru
                        $idCompany = Auth::user()->company_id;
                    } else {
                        // super admin insert
                        $idCompany = $request->input('id_company');
                    }

                    //concate time now with original name image
                    $nameFormatImg = str_replace(' ', '_', $request->file('user_image')->getClientOriginalName());
                    $timeNow = time();
                    $imgBranchName = $timeNow . '-' . $nameFormatImg;
                    $urlImg = 'img/' . $imgBranchName;

                    DB::table('master_user')->insert([
                        'username' => $userName,
                        'fullname' =>  $fullName,
                        'branch_id' => $branchId,
                        'user_role_id' => $roleId,
                        'short_code' => strtoupper($shortCode),
                        'user_crt_id' => $user->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'password' => bcrypt('welcome1'),
                        'is_active' => 't',
                        'company_id' => $idCompany,
                        'image_url' => $urlImg,
                        'is_login' => 'f',
                        'branch_cust_id' => $branch_customer,
                        'customer_id' => $id_customer
                    ]);

                    // store image in disk (img_upload)
                    $request->file('user_image')->move(public_path('/img'), $imgBranchName);

                break;

                // insert quotation
                case "18" :
                    $userId = $request->input('user_id');
                    $qsNo = $request->input('qs_no');
                    $qsDate = date('Y-m-d', strtotime($request->input('qs_date')));
                    $branch_id = "";
                    $company_id = "";

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_quotation');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    $user = Auth::user();

                    $dtUser = DB::SELECT('SELECT branch_id, company_id FROM master_user WHERE id = ' . $userId);

                    if ( !empty($dtUser) ) {
                        $branch_id = $dtUser[0]->branch_id;
                        $company_id = $dtUser[0]->company_id;
                    }

                    DB::table('ref_quotation')->insert([
                        'qs_no' => $qsNo,
                        'qs_date' => $qsDate,
                        'seq' => $newSeq,
                        'officer_id' => $userId,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => $user->id,
                        'is_active' => 't',
                        'branch_id' => $branch_id,
                        'company_id' => $company_id
                    ]);

                break;

                // insert proposal
                case "19" :
                    $userId = $request->input('user_id');
                    $qsNo = $request->input('qs_no');
                    $qsDate = date('Y-m-d', strtotime($request->input('qs_date')));
                    $branch_id = "";
                    $company_id = "";

                    // get last seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_proposal');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    $user = Auth::user();

                    $dtUser = DB::SELECT('SELECT branch_id, company_id FROM master_user WHERE id = ' . $userId);

                    if ( !empty($dtUser) ) {
                        $branch_id = $dtUser[0]->branch_id;
                        $company_id = $dtUser[0]->company_id;
                    }

                    DB::table('ref_proposal')->insert([
                        'qs_no' => $qsNo,
                        'qs_date' => $qsDate,
                        'seq' => $newSeq,
                        'officer_id' => $userId,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => $user->id,
                        'is_active' => 't',
                        'branch_id' => $branch_id,
                        'company_id' => $company_id
                    ]);

                break;

                // insert master workflow
                case "20" :
                    $idCompany = $request->input('id_company');
                    $prevWorkflow = $request->input('prev_workflow');
                    $currWorkflow = $request->input('current_workflow');
                    $nextWorkflow = $request->input('next_workflow');
                    $roleId = $request->input('role_id');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    $sameData = DB::table('master_workflow')->where('company_id', $idCompany)
                                                ->where('current_status_id', $currWorkflow)
                                                ->first();

                    if (!is_null($sameData)) {
                        return response()->json([
                            'rc' => 1,
                            'rm' => 'Workflow sudah ada'
                        ]);
                    }


                    DB::table('master_workflow')->insert([
                        'company_id' => $idCompany,
                        'current_status_id' => $currWorkflow,
                        'prev_status_id' => $prevWorkflow,
                        'next_status_id' => $nextWorkflow,
                        'user_role_id' => $roleId
                    ]);

                break;

                // insert inventory
                case "21":
                    $description = $request->input('inventory_name');
                    $amor_month = $request->input('amor_month');
                    $coaNo = $request->input('coa_no');
                    $coaNoAmor = $request->input('coa_no_amor');

                    // get lase seq
                    $lastSeq = DB::SELECT('SELECT MAX(seq) AS seq FROM ref_inventory');
                    if (empty($lastSeq[0]->seq)) {
                        $newSeq = 1;
                    } else {
                        $newSeq = $lastSeq[0]->seq + 1;
                    }

                    DB::table('ref_inventory')->insert([
                        'description' => $description,
                        'amor_month' => $amor_month,
                        'seq' => $newSeq,
                        'coa_no' => $coaNo,
                        'coa_no_amor' => $coaNoAmor,
                        'is_active' => 't'
                    ]);

                break;

                // insert master coa
                case "22":
                    $branchId = Auth::user()->branch_id;
                    $COAParentId = $request->input('coa_parent_id');
                    $companyId = Auth::user()->company_id;
                    $COANo = $COAParentId . '' . $request->input('coa_no');
                    $COAName = $request->input('coa_name');
                    $valutaCode = $request->input('valuta_code');
                    $COAGroupId = $request->input('coa_group_id');
                    $isParent = $request->input('is_parent');
                    // $balanceTypeId = $request->input('balance_type_id');

                    $COATypeId = $request->input('coa_type_id');
                    // dd($request->all());

                    if ( is_null($request->input('last_os')) ) {
                        $lastOS = 0;
                    } else {
                        $lastOS = str_replace('.', '', $request->input('last_os'));
                        $lastOS = str_replace(',', '.', $lastOS);
                    }

                    $isCOANoExists = \DB::TABLE('master_coa')->select('coa_no')
                                                            ->where('branch_id', Auth::user()->branch_id)
                                                            ->where('company_id', Auth::user()->company_id)
                                                            ->where('coa_no', $COANo)
                                                            ->get();

                    if ( count($isCOANoExists) > 0 ) {
                        return response()->json([
                            'rc' => 1,
                            'rm' => 'COA No sudah ada'
                        ]);
                    }

                    $dt = \DB::TABLE('master_coa')->select('balance_type_id')
                                                  ->where('coa_no', $COAParentId)
                                                  ->first();

                    $balanceTypeId = $dt->balance_type_id;

                     // get last id
                     $lastData = DB::SELECT('SELECT MAX(id) AS id FROM master_coa');
                     if (empty($lastData[0]->id)) {
                         $newId = 1;
                     } else {
                         $newId = $lastData[0]->id + 1;
                     }

                    $dataInsert = [
                        'id' => $newId,
                        'company_id' => $companyId,
                        'branch_id' => $branchId,
                        'coa_no' => $COANo,
                        'valuta_code' => $valutaCode,
                        'coa_name' => $COAName,
                        'coa_type_id' => $COATypeId,
                        'coa_group_id' => $COAGroupId,
                        'balance_type_id' => $balanceTypeId,
                        'user_crt_id' => Auth::user()->id,
                        'created_at' => date('Y-m-d h:i:s'),
                        'last_os' => $lastOS,
                        'is_active' => 't',
                        'coa_parent_id' => $COAParentId,
                        'is_parent' => $isParent
                    ];

                    // if ($COANo == '1000') {
                    //     // insert coa as child
                    //     $dataInsert['coa_parent_id'] = $COAParentId;
                    //     $dataInsert['is_parent'] = 'f';

                    //     // get last coa no
                    //     $lastCOA = DB::SELECT("SELECT max(coa_no) AS coa_no FROM master_coa WHERE coa_parent_id='" . $COAParentId . "' AND branch_id = " . Auth::user()->branch_id);

                    //     if (empty($lastCOA[0]->coa_no)) {
                    //         $COANo = $COAParentId . '001';
                    //     } else {
                    //         $COANo = $lastCOA[0]->coa_no + 1;
                    //     }

                    //     $dataInsert['coa_no'] = $COANo;

                    // } else {
                    //     $dataInsert['is_parent'] = 't';
                    // }

                    DB::table('master_coa')->insert($dataInsert);
                break;

                // insert customer group
                case "23":
                    $groupName = $request->input('group_name');
                    $address = $request->input('address');
                    $noTelp = $request->input('no_telp');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get max seq
                    $seq = DB::table('ref_cust_group')->max('seq');

                    if (empty($seq)) {
                        $seq = 1;
                    } else {
                        $seq += 1;
                    }

                    if ( !is_null($request->file('img')) ) {
                        //concate time now with original name image
                        $nameFormatImg = str_replace(' ', '_', $request->file('img')->getClientOriginalName());
                        $timeNow = time();
                        $imgBranchName = $timeNow . '-' . $nameFormatImg;
                        $urlImg = 'img/' . $imgBranchName;

                        // store image in disk (img_upload)
                        $request->file('img')->move(public_path('/img'), $imgBranchName);
                    } else {
                        $urlImg = null;
                    }


                    DB::table('ref_cust_group')->insert([
                        'group_name' => $groupName,
                        'address' =>  $address,
                        'phone_no' => $noTelp,
                        'seq' => $seq,
                        'is_active' => 't',
                        'company_id' => $idCompany,
                        'url_image' => $urlImg,
                    ]);


                break;

                //insert bdd code
                case "24":
                        $bddName = $request->input('bdd_name');
                        $coaNo = $request->input('coa_no');
                        $coaNoAmor = $request->input('coa_no_amor');
                        $branchId = $request->input('branch_id');
                        // $companyId = $request->input('id_company');
                        $companyId = Auth::user()->company_id;

                        // get max seq
                        $seq = DB::table('ref_bdd')->max('seq');

                        if (empty($seq)) {
                            $seq = 1;
                        } else {
                            $seq += 1;
                        }

                        DB::table('ref_bdd')->insert([
                            'bdd_name' => $bddName,
                            'coa_no' => $coaNo,
                            'coa_no_amor' => $coaNoAmor,
                            'is_active' =>  't',
                            'seq' => $seq,
                            'branch_id' => $branchId,
                            'company_id' => $companyId
                        ]);
                break;

                //insert agent
                case "25":


                        $fullName = $request->input('full_name');
                        $codeName = $request->input('code_name');
                        $address = $request->input('address');
                        $phoneNo = $request->input('phone_no');
                        $email = $request->input('email');
                        $parentId = $request->input('parent_id');
                        $isParent = "";


                        if ( $parentId == '1000' ) {
                            $isParent = 't';
                            $parentId = null;
                        } else {
                            $isParent = 'f';
                        }

                        $dtEmailSama = DB::table('ref_agent')->select('email')->where('email', $email)->first();

                        if (!is_null($dtEmailSama)) {
                            return response()->json([
                                'rc' => 1,
                                'rm' => 'Email sudah digunakan'
                            ]);
                        }

                        // get max seq
                        $seq = DB::table('ref_agent')->max('seq');

                        if (empty($seq)) {
                            $seq = 1;
                        } else {
                            $seq += 1;
                        }

                        if ( Auth::user()->user_role_id != 5 ) {
                            // User admin pusat
                            $companyId = Auth::user()->company_id;
                        } else {
                            // User super admin
                            $companyId = null;
                        }

                        DB::table('ref_agent')->insert([
                            'full_name' => $fullName,
                            'code_name' => $codeName,
                            'address' => $address,
                            'phone_no' => $phoneNo,
                            'email' => $email,
                            'is_active' =>  't',
                            'seq' => $seq,
                            'created_at' => date('Y-m-d H:i:s'),
                            'user_crt_id' => Auth::user()->id,
                            'company_id' => $companyId,
                            'is_parent' => $isParent,
                            'parent_id' => $parentId
                        ]);
                break;

                // insert menu
                case "26":
                    $menuName = $request->input('menu_name');
                    $type = $request->input('type');
                    DB::table('ref_menu')->insert([
                        'menu' => $menuName,
                        'type' => $type
                    ]);
                break;

                // insert sub menu
                case "27":
                    $idMenu = $request->input('id_menu');
                    $subMenu = $request->input('sub_menu');
                    $url = $request->input('url');

                    DB::table('ref_sub_menu')->insert([
                        'idmenu' => $idMenu,
                        'submenu' => $subMenu,
                        'url' => $url
                    ]);
                break;

                // insert manajemen user
                case "28":
                    $idRole = $request->input('id_role');
                    $idSubMenu = $request->input('id_sub_menu');

                    DB::table('manajemen_user')->insert([
                        'idrole' => $idRole,
                        'id_sub_menu' => $idSubMenu
                    ]);
                break;

                // insert product
                case "29":
                    $productName = $request->input('product_name');
                    // get max seq
                    $seq = DB::table('ref_product')->max('seq');

                    if (empty($seq)) {
                        $seq = 1;
                    } else {
                        $seq += 1;
                    }

                    if ( Auth::user()->user_role_id != 5 ) {
                        // User admin pusat
                        $companyId = Auth::user()->company_id;
                    } else {
                        // User super admin
                        $companyId = null;
                    }

                    DB::table('ref_product')->insert([
                        'definition' => $productName,
                        'seq' => $seq,
                        'is_active' => 't',
                        'company_id' => $companyId
                    ]);
                break;

                // insert underwriter
                case "30":
                    $definition = $request->input('definition');
                    $branchId = $request->input('branch_id');
                    $underwriter_code = $request->input('underwriter_code');
                    $address = $request->input('address');
                    $phone_no = $request->input('phone_no');
                    $fax_no = $request->input('fax_no');
                    $email = $request->input('email');
                    $pic = $request->input('pic');
                    $acc_no = $request->input('acc_no');
                    $acc_name = $request->input('acc_name');
                    $bank_name = $request->input('bank_name');
                    // get max seq
                    $seq = DB::table('ref_underwriter')->max('seq');
                    if (empty($seq)) {
                        $seq = 1;
                    } else {
                        $seq += 1;
                    }

                    if ( Auth::user()->user_role_id != 5 ) {
                        // User admin pusat
                        $companyId = Auth::user()->company_id;
                    } else {
                        // User super admin
                        $companyId = null;
                    }

                    DB::table('ref_underwriter')->insert([
                        'definition' => $definition,
                        'company_id' => $companyId,
                        'branch_id' => $branchId,
                        'is_active' => 't',
                        'seq' => $seq,
                        'underwriter_code' => $underwriter_code,
                        'address' => $address,
                        'phone_no' => $phone_no,
                        'fax_no' => $fax_no,
                        'email' => $email,
                        'pic' => $pic,
                        'acc_no' => $acc_no,
                        'acc_name' => $acc_name,
                        'bank_name' => $bank_name,
                    ]);
                break;

                // Insert QS Content
                case '99' :
                    $arrData = $request->all();

                    $lastId = DB::TABLE('ref_qs_content')->max('id');

                    if ( is_null($lastId) ) {
                        $lastId = 1;
                    } else {
                        $lastId += 1;
                    }

                    DB::TABLE('ref_qs_content')->insert([
                        'id' => $lastId,
                        'product_id' => $arrData['product'],
                        'form_wording' => $arrData['form_wording'],
                        'the_business' => $arrData['the_business'],
                        'coverage' => $arrData['coverage_content'],
                        'main_exclusions' => $arrData['main_exclusions_content'],
                        'deductible' => $arrData['deductible_content'],
                        'clauses' => $arrData['clauses_content'],
                        'period_default' => $arrData['period_default'],
                        'is_active' => 't'
                    ]);
                break;

                // Insert Branch Customer
                case "100" :
                    $branchName = $request->input('branch_name');
                    $address = $request->input('address');
                    $provId = $request->input('province_id');
                    $cityId = $request->input('city_id');
                    $latitude = NULL;
                    $longitude = NULL;
                    $shortCode = $request->input('short_code');
                    $leaderName = $request->input('leader_name');
                    $jabatan = $request->input('jabatan');
                    $isHO = false;

                    $lastId = DB::TABLE('master_branch_cust')->max('id');
                    if ( is_null($lastId) ) {
                        $lastId = 1;
                    } else {
                        $lastId += 1;
                    }


                    if ( !is_null($request->input('is_ho')) ) {

                        if ( $request->input('is_ho') == 1 ) {
                            $isHO = true;
                        } else {
                            $isHO = false;
                        }

                    }

                    $user = Auth::user();

                    DB::table('master_branch_cust')->insert([
                        'id' => $lastId,
                        'branch_name' => $branchName,
                        'address' => $address,
                        'city_id' => $cityId,
                        'province_id' => $provId,
                        'long' => $longitude,
                        'lat' => $latitude,
                        'short_code' => strtoupper($shortCode),
                        'leader_name' => $leaderName,
                        'user_crt_id' => $user->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'is_active' => 't' ,
                        'company_id' => $user->company_id,
                        'jabatan' => $jabatan,
                        'is_ho' => $isHO
                    ]);
                break;

                // Insert Insured Detail
                case '101':

                    DB::beginTransaction();
                    try {
                        $arrData = $request->all();


                        $product_id = $arrData['product'];

                        $arrBitId = [12, 8, 29, 28, 21];

                        $dataExist = DB::TABLE('ref_insured_detail')
                                        ->where('product_id', $product_id)
                                        ->whereIn('bit_id', $arrBitId)
                                        ->get();

                        if ( count($dataExist) == 0 ) {
                            // Tidak ada data detail sama sekali
                            $collectData = [];

                            // Ambil Calculation Dari Product Flexa
                            $calc = DB::TABLE('ref_insured_detail')
                                    ->whereIn('bit_id', $arrBitId)
                                    ->where('product_id', 2) // Product Flexa
                                    ->get();

                            foreach ( $calc as $item ) {

                                $lastId = DB::TABLE('ref_insured_detail')->max('id');
                                if ( is_null($lastId) ) {
                                    $lastId = 1;
                                } else {
                                    $lastId += 1;
                                }

                                $collectData = [
                                    'id' => $lastId,
                                    'product_id' => $product_id,
                                    'bit_id' => $item->bit_id,
                                    'label' => $item->label,
                                    'is_number' => $item->is_number,
                                    'is_active' => $item->is_active,
                                    'input_name' => $item->input_name,
                                    'is_filterable' => $item->is_filterable,
                                    'is_currency' => $item->is_currency,
                                    'is_required' => $item->is_required,
                                    'is_showing' => $item->is_showing,
                                    'is_rate' => $item->is_rate,
                                ];

                                DB::TABLE('ref_insured_detail')->insert($collectData);

                            } // ed foreach

                            $i = 1;

                        } else {
                            $i = DB::TABLE('ref_insured_detail')->where('product_id', $product_id)->max('bit_id');
                            $i += 1;
                        }


                        foreach ( $arrData['label'] as $idx => $item ) {

                            if ( in_array($i, $arrBitId) ) {
                                $i += 1;
                            }

                            $input_name = str_replace('_', '', $item);
                            $input_name = str_replace(' ', '_', $input_name);
                            $input_name = strtolower($input_name);

                            $lastId = DB::TABLE('ref_insured_detail')->max('id');
                            if ( is_null($lastId) ) {
                                $lastId = 1;
                            } else {
                                $lastId += 1;
                            }

                            switch ( $arrData['format_input'][$idx] ) {
                                case '1':
                                    // Format Angka
                                    $is_number = 't';
                                    $is_rate = 'f';
                                    $is_currency = 'f';
                                    $is_date = 'f';
                                break;
                                case '2':
                                    // Format Mata Uang
                                    $is_number = 't';
                                    $is_rate = 'f';
                                    $is_currency = 't';
                                    $is_date = 'f';
                                break;
                                case '3':
                                    // Format Presentase
                                    $is_number = 't';
                                    $is_rate = 't';
                                    $is_currency = 'f';
                                    $is_date = 'f';
                                break;
                                case '4':
                                    // Format Text
                                    $is_number = 'f';
                                    $is_rate = 'f';
                                    $is_currency = 'f';
                                    $is_date = 'f';
                                break;
                                case '5':
                                    // Format Tanggal
                                    $is_number = 'f';
                                    $is_rate = 'f';
                                    $is_currency = 'f';
                                    $is_date = 't';
                                break;
                            }

                            $collectData = [
                                'id' => $lastId,
                                'product_id' => $product_id,
                                'bit_id' => $i,
                                'label' => $item,
                                'is_number' => $is_number,
                                'is_active' => 't',
                                'input_name' => $input_name,
                                'is_filterable' => $arrData['is_filterable'][$idx],
                                'is_currency' => $is_currency,
                                'is_required' => $arrData['is_required'][$idx],
                                'is_showing' => $arrData['is_showing'][$idx],
                                'is_rate' => $is_rate,
                                'is_date' => $is_date
                            ];

                            DB::TABLE('ref_insured_detail')->insert($collectData);
                            $i++;
                        } // end foreach

                        DB::commit();
                    } catch ( \Exception $e) {

                        DB::rollback();
                        dd($e->getMessage());

                    } // end try

                break;

                // INSERT OR EDIT REF RATE USIA
                case '102':
                    DB::beginTransaction();
                    try {
                        
                        if ( isset($request->type) ) {
                            // INSERT
                            $arrData = $request->all();
                            $cekData = DB::TABLE('ref_rate_usia')
                                            ->where('usia', $arrData['usia_input'])
                                            ->where('tenor', $arrData['tenor_input'])
                                            ->first();

                            if ( !is_null($cekData) ) {
                                return response()->json([
                                    'rc' => 1,
                                    'rm' => 'Usia ' . $arrData['usia_input'] . ' dan Tenor ' . $arrData['tenor_input'] . ' sudah ada'
                                ]);
                            }

                            $id = DB::TABLE('ref_rate_usia')->max('id');
                            if ( is_null($id) ) {
                                $id = 1;
                            } else {
                                $id += 1;
                            }

                            DB::TABLE('ref_rate_usia')
                                ->insert([
                                    'id' => $id,
                                    'usia' => $arrData['usia_input'],
                                    'tenor' => $arrData['tenor_input'],
                                    'rate' => str_replace(',', '', $arrData['rate_input'])
                                ]);
                        } else {
                            // Update Rate
                            $arrData = json_decode($request->data, true);
                            foreach ( $arrData['rate'] as $idx => $item ) {
                                DB::TABLE('ref_rate_usia')
                                        ->where('id', $arrData['id'][$idx]['value'])
                                        ->update([
                                            'rate' => $item['value']
                                        ]);
                            }
                        } // end if
                        

                        DB::commit();
                    } catch ( \Exception $e) {
                        DB::rollback();
                        dd($e->getMessage());

                    } // end try
                break;


            }

            return response()->json([
                'rc' => 0,
                'rm' => "Data Berhasil Disimpan"
            ]);
        } else {
            // update data

            switch ($refOrder) {
                // update agent type
                case "1" :

                    $definition = $request->input('definition');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        // insert data from admin pusat
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_agent_type')->where('id', $id)->update([
                        'definition' => $definition,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany,
                        'is_active' => 't'
                    ]);
                break;
                // update bank account
                case "2" :
                    $bankName = $request->input('bank');
                    $idCompany = $request->input('id_company');
                    $coaId = $request->input('coa_no');
                    $seq = $request->input('seq');
                    $branchId = $request->input('branch_id');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // get coa no
                    $data = DB::SELECT('SELECT coa_no FROM master_coa WHERE id='.$coaId);


                    DB::table('ref_bank_account')->where('id', $id)->update([
                        'definition' => $bankName,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany,
                        'coa_id' => $coaId,
                        'coa_no' => $data[0]->coa_no,
                        'branch_id' => $branchId
                    ]);
                break;
                // update user role
                case "3" :


                    $userRole = $request->input('user_role');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    DB::table('ref_user_role')->where('id', $id)->update([
                        'definition' => $userRole,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update coa group
                case "4" :

                    $coa = $request->input('coa_group');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_coa_group')->where('id', $id)->update([
                        'definition' => $coa,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update coa type
                case "5" :

                    $coaType = $request->input('coa_type');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_coa_type')->where('id', $id)->update([
                        'definition' => $coaType,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update coa segment
                case "6" :

                    $coaSegment = $request->input('coa_segment');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_segment')->where('id', $id)->update([
                        'definition' => $coaSegment,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update customer type
                case "7" :

                    $custType = $request->input('customer_type');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_cust_type')->where('id', $id)->update([
                        'definition' => $custType,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update customer segment
                case "8" :

                    $custSegment = $request->input('customer_segment');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_cust_segment')->where('id', $id)->update([
                        'definition' => $custSegment,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update paid status
                case "9" :

                    $paidSts = $request->input('paid_status');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_paid_status')->where('id', $id)->update([
                        'definition' => $paidSts,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update workflow status
                case "10" :

                    $wfSts = $request->input('workflow_status');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_workflow_status')->where('id', $id)->update([
                        'definition' => $wfSts,
                        'seq' => (int)$seq,
                        'company_id' => $idCompany
                    ]);
                break;
                // update tx type
                case "13" :

                    $definition = $request->input('definition');
                    $sortCode = $request->input('short_code');
                    $idCompany = $request->input('id_company');
                    $seq = $request->input('seq');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    DB::table('ref_tx_type')->where('id', $id)->update([
                        'definition' => $definition,
                        'seq' => (int)$seq,
                        'short_code' => $sortCode,
                        'company_id' => $idCompany
                    ]);
                break;

                // update master company
                case "14" :

                    $companyName = $request->input('company_name');
                    $address = $request->input('address');
                    $provId = $request->input('province_id');
                    $cityId = $request->input('city_id');
                    $longitude = $request->input('longitude');
                    $latitude = $request->input('latitude');
                    $shortCode = $request->input('short_code');
                    $leaderName = $request->input('leader_name');

                    $user = Auth::user();

                    DB::table('master_company')->where('id', $id)->update([
                        'company_name' => $companyName,
                        'address' => $address,
                        'city_id' => $cityId,
                        'province_id' => $provId,
                        'long' => $longitude,
                        'lat' => $latitude,
                        'short_code' => strtoupper($shortCode),
                        'leader_name' => $leaderName,
                        'user_upd_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                break;


                // update master branch
                case "15" :

                    $branchName = $request->input('branch_name');
                    $address = $request->input('address');
                    $provId = $request->input('province_id');
                    $cityId = $request->input('city_id');
                    // $longitude = $request->input('longitude');
                    // $latitude = $request->input('latitude');
                    $longitude = NULL;
                    $latitude = NULL;
                    $shortCode = $request->input('short_code');
                    $leaderName = $request->input('leader_name');
                    $jabatan = $request->input('jabatan');


                    $user = Auth::user();

                    DB::table('master_branch')->where('id', $id)->update([
                        'branch_name' => $branchName,
                        'address' => $address,
                        'city_id' => $cityId,
                        'province_id' => $provId,
                        'long' => $longitude,
                        'lat' => $latitude,
                        'short_code' => strtoupper($shortCode),
                        'leader_name' => $leaderName,
                        'user_upd_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'company_id' => $user->company_id,
                        'jabatan' => $jabatan
                    ]);
                break;


                // update master config
                case "16" :

                    $parmName = $request->input('parm_name');
                    $val = $request->input('value');

                    DB::table('master_config')->where('id', $id)->update([
                        'parm_name' => $parmName,
                        'value' => $val,
                    ]);

                break;

                // update master user
                case "17" :

                    if ( $request->user_type == '2' && $request->role == '11') {
                        // User Client
                        $id_customer = $request->customer;
                        $dataCust = DB::TABLE('master_customer')
                                        ->where('id', $id_customer)
                                        ->first();

                        $branch_customer = $dataCust->cust_branch_id;
                    } else {
                        $id_customer = NULL;
                        $branch_customer = NULL;
                    }


                    $user = Auth::user();
                    $userName = $request->input('username');
                    $fullName = $request->input('fullname');
                    $branchId = $request->input('branch_id');
                    $roleId = $request->input('role_id');
                    $shortCode = $request->input('short_code');

                    if ( $request->input('id_company') == null) {
                        // admin insert user baru
                        $idCompany = Auth::user()->company_id;
                    } else {
                        // super admin insert
                        $idCompany = $request->input('id_company');
                    }

                    $dataUser = [
                        'username' => $userName,
                        'fullname' =>  $fullName,
                        'branch_id' => $branchId,
                        'user_role_id' => $roleId,
                        'short_code' => strtoupper($shortCode),
                        'company_id' => $idCompany,
                        'branch_cust_id' => $branch_customer,
                        'customer_id' => $id_customer
                    ];

                    if($request->hasFile('user_image')) {
                        // remove emage previous
                        $urlImgDb = DB::table('master_user')->select('image_url')->where('id', $id)->first();
                        $fullUrlImgDel = public_path() . '/' . $urlImgDb->image_url;

                        if ( file_exists($fullUrlImgDel)) {
                            unlink($fullUrlImgDel);
                        }

                        //concate time now with original name image
                        $nameFormatImg = str_replace(' ', '_', $request->file('user_image')->getClientOriginalName());
                        $timeNow = time();
                        $imgBranchName = $timeNow . '-' . $nameFormatImg;
                        $urlImg = 'img/' . $imgBranchName;


                        // store image in disk (img_upload)
                        $request->file('user_image')->move(public_path('/img'), $imgBranchName);

                        $dataUser['image_url'] = $urlImg;
                    }

                    // update data user
                    DB::table('master_user')->where('id', $id)->update($dataUser);

                break;


                // update quotation
                case "18" :
                    $userId = $request->input('user_id');
                    $qsNo = $request->input('qs_no');
                    $qsDate = date('Y-m-d', strtotime($request->input('qs_date')));
                    $seq = $request->input('seq');

                    $user = Auth::user();

                    $dtUser = DB::SELECT('SELECT branch_id, company_id FROM master_user WHERE id = ' . $userId);

                    if ( !empty($dtUser) ) {
                        $branch_id = $dtUser[0]->branch_id;
                        $company_id = $dtUser[0]->company_id;
                    }

                    DB::table('ref_quotation')->where('id', $id)->update([
                        'qs_no' => $qsNo,
                        'qs_date' => $qsDate,
                        'officer_id' => $userId,
                        'seq' => $seq,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_id' => $user->id,
                        'company_id' => $company_id,
                        'branch_id' => $branch_id
                    ]);

                break;

                // update proposal
                case "19" :
                    $userId = $request->input('user_id');
                    $qsNo = $request->input('qs_no');
                    $qsDate = date('Y-m-d', strtotime($request->input('qs_date')));
                    $seq = $request->input('seq');
                    $company_id = "";
                    $branch_id = "";

                    $dtUser = DB::SELECT('SELECT branch_id, company_id FROM master_user WHERE id = ' . $userId);

                    if ( !empty($dtUser) ) {
                        $branch_id = $dtUser[0]->branch_id;
                        $company_id = $dtUser[0]->company_id;
                    }

                    $user = Auth::user();

                    DB::table('ref_proposal')->where('id', $id)->update([
                        'qs_no' => $qsNo,
                        'qs_date' => $qsDate,
                        'officer_id' => $userId,
                        'seq' => $seq,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_id' => $user->id,
                        'company_id' => $company_id,
                        'branch_id' => $branch_id,
                    ]);

                break;

                // update master workflow
                case "20" :
                    $idCompany = $request->input('id_company');
                    $prevWorkflow = $request->input('prev_workflow');
                    $currWorkflow = $request->input('current_workflow');
                    $nextWorkflow = $request->input('next_workflow');
                    $roleId = $request->input('role_id');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    // $sameData = DB::table('master_workflow')->where('company_id', $idCompany)
                    //                             ->where('current_status_id', $currWorkflow)
                    //                             ->first();

                    // if (!is_null($sameData)) {
                    //     return response()->json([
                    //         'rc' => 1,
                    //         'rm' => 'Workflow sudah ada'
                    //     ]);
                    // }

                    $oldIdCompany = $request->input('id');
                    $oldCurrId = $request->input('id_current_status');

                    // delete previous workflow
                    DB::table('master_workflow')
                        ->where('company_id', $oldIdCompany)
                        ->where('current_status_id', $oldCurrId)
                        ->delete();

                    // insert new data workflow
                    DB::table('master_workflow')->insert([
                        'company_id' => $idCompany,
                        'current_status_id' => $currWorkflow,
                        'prev_status_id' => $prevWorkflow,
                        'next_status_id' => $nextWorkflow,
                        'user_role_id' => $roleId
                    ]);

                break;

                // update inventory
                case "21" :
                    $id = $request->input('id');
                    $description = $request->input('inventory_name');
                    $amor_month = $request->input('amor_month');
                    $seq = $request->input('seq');
                    $coaNo = $request->input('coa_no');
                    $coaNoAmor = $request->input('coa_no_amor');

                    DB::table('ref_inventory')->where('id', $id)->update([
                        'description' => $description,
                        'amor_month' => $amor_month,
                        'coa_no' => $coaNo,
                        'coa_no_amor' => $coaNoAmor,
                        'seq' => $seq,
                    ]);

                break;

                // update master coa
                case "22":

                    // if (is_null($request->input('last_os'))) {
                    //     $lastOS = 0;
                    // } else {
                    //     $lastOS = str_replace('.', '', $request->input('last_os'));
                    // }



                    $COAParentId = $request->input('coa_parent_id');
                    $lastData = DB::table('master_coa')->where('id', $id)->first();
                    if ( $lastData->coa_parent_id == $COAParentId ) {
                        $COANo = $request->input('coa_no');
                    } else {
                        $COANo = $COAParentId . '' . $request->input('coa_no');
                    }

                    $branchId = Auth::user()->branch_id;
                    $companyId = Auth::user()->company_id;

                    // $COANo = $request->input('coa_no');
                    $COAName = $request->input('coa_name');
                    $valutaCode = $request->input('valuta_code');
                    $COATypeId = $request->input('coa_type_id');
                    $COAGroupId = $request->input('coa_group_id');
                    $isParent = $request->input('is_parent');
                    // $balanceTypeId = $request->input('balance_type_id');

                    $isCOANoExists = \DB::TABLE('master_coa')->select('coa_no')
                                                            ->where('branch_id', Auth::user()->branch_id)
                                                            ->where('company_id', Auth::user()->company_id)
                                                            ->where('coa_no', $COANo)
                                                            ->where('id', '<>', $id)
                                                            ->get();

                    if ( count($isCOANoExists) > 0 ) {
                        return response()->json([
                            'rc' => 1,
                            'rm' => 'COA No sudah ada'
                        ]);
                    }

                    $dataInsert = [
                        'company_id' => $companyId,
                        'branch_id' => $branchId,
                        'coa_no' => $COANo,
                        'valuta_code' => $valutaCode,
                        'coa_name' => $COAName,
                        'coa_type_id' => $COATypeId,
                        'coa_group_id' => $COAGroupId,
                        // 'balance_type_id' => $balanceTypeId,
                        'user_upd_at' => Auth::user()->id,
                        'updated_at' => date('Y-m-d h:i:s'),
                        'coa_parent_id' => $COAParentId,
                        'is_parent' => $isParent
                        // 'last_os' => $lastOS
                    ];

                    // $oldData = DB::table('master_coa')->where('id', $id)->first();

                    // if ($oldData->coa_parent_id != $COAParentId) {
                    //     // get last coa no
                    //     $lastCOA = DB::SELECT("SELECT MAX(coa_no) AS coa_no FROM master_coa WHERE coa_parent_id='" . $COAParentId . "'");
                    //     $dataInsert['coa_parent_id'] = $COAParentId;

                    //     if (empty($lastCOA[0]->coa_no)) {
                    //         $COANo = $COAParentId . '001';
                    //     } else {
                    //         $COANo = $lastCOA[0]->coa_no + 1;
                    //     }

                    //     $dataInsert['coa_no'] = $COANo;
                    // } else {
                    //     $dataInsert['coa_parent_id'] = $COAParentId;
                    //     $dataInsert['is_parent'] = 'f';
                    // }

                    DB::table('master_coa')->where('id', $id)->update($dataInsert);
                break;

                // update customer group
                case "23" :
                    $groupName = $request->input('group_name');
                    $address = $request->input('address');
                    $phone_no = $request->input('no_telp');
                    $seq = $request->input('seq');
                    $idCompany = $request->input('id_company');

                    if ( is_null($idCompany) ) {
                        $idCompany = Auth::user()->company_id;
                    }

                    $data = [
                        'group_name' => $groupName,
                        'address' =>  $address,
                        'company_id' => $idCompany,
                        'phone_no' => $phone_no,
                        'seq' => $seq,
                    ];

                    if($request->hasFile('img')) {
                        // remove emage previous
                        $urlImgDb = DB::table('ref_cust_group')->select('url_image')->where('id', $id)->first();
                        $fullUrlImgDel = public_path() . '\\' . $urlImgDb->url_image;

                        if ( file_exists($fullUrlImgDel)) {
                            unlink($fullUrlImgDel);
                        }


                        //concate time now with original name image
                        $nameFormatImg = str_replace(' ', '_', $request->file('img')->getClientOriginalName());
                        $timeNow = time();
                        $imgBranchName = $timeNow . '-' . $nameFormatImg;
                        $urlImg = 'img/' . $imgBranchName;


                        // store image in disk (img_upload)
                        $request->file('img')->move(public_path('/img'), $imgBranchName);

                        $data['url_image'] = $urlImg;
                    }

                    // update data user
                    DB::table('ref_cust_group')->where('id', $id)->update($data);
                break;

                //update bdd
                case "24":
                        $id = $request->input('id');
                        $bddName = $request->input('bdd_name');
                        $coaNo = $request->input('coa_no');
                        $coaNoAmor = $request->input('coa_no_amor');
                        $seq = $request->input('seq');
                        $branchId = $request->input('branch_id');
                        // $companyId = $request->input('id_company');
                        $companyId = Auth::user()->company_id;


                        DB::table('ref_bdd')->where('id', $id)->update([
                            'bdd_name' => $bddName,
                            'coa_no' => $coaNo,
                            'coa_no_amor' => $coaNoAmor,
                            'seq' => $seq,
                            'branch_id' => $branchId,
                            'company_id' => $companyId
                        ]);
                break;

                //update agent
                case "25":
                        $fullName = $request->input('full_name');
                        $codeName = $request->input('code_name');
                        $address = $request->input('address');
                        $phoneNo = $request->input('phone_no');
                        $email = $request->input('email');
                        $id = $request->input('id');
                        $seq = $request->input('seq');

                        $parentId = $request->input('parent_id');
                        $isParent = "";

                        if ( $parentId == '1000' ) {
                            $isParent = 't';
                            $parentId = null;
                        } else {

                            if ( $id == $parentId ) {
                                // parent dirinya sendiri
                                $isParent = 't';
                                $parentId = null;
                            } else {
                                $isParent = 'f';
                            }
                        }

                        $dtEmailSama = DB::table('ref_agent')
                                            ->select('email')
                                            ->where('email', $email)
                                            ->where('id', '!=', $id)
                                            ->first();

                        if (!is_null($dtEmailSama)) {
                            return response()->json([
                                'rc' => 1,
                                'rm' => 'Email sudah digunakan'
                            ]);
                        }

                        if ( Auth::user()->user_role_id != 5 ) {
                            // User admin pusat
                            $companyId = Auth::user()->company_id;
                        } else {
                            // User super admin
                            $companyId = null;
                        }

                        DB::table('ref_agent')->where('id', $id)->update([
                            'full_name' => $fullName,
                            'code_name' => $codeName,
                            'address' => $address,
                            'phone_no' => $phoneNo,
                            'email' => $email,
                            'is_active' =>  't',
                            'seq' => $seq,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'user_upd_id' => Auth::user()->id,
                            'company_id' => $companyId,
                            'is_parent' => $isParent,
                            'parent_id' => $parentId
                        ]);
                break;

                // setting menu for user
                case "26" :
                    $idRole = $request->input('id');
                    $roleName = $request->input('role_name');
                    $arrMenu = $request->input('menu');
                    $arrSubMenu = $request->input('subMenu');

                    foreach ($request->subMenu as $key) {
                        $seq = DB::table('manajemen_user')->max('id');

                        if (empty($seq)) {
                            $seq = 1;
                        } else {
                            $seq += 1;
                        }

                        DB::table('manajemen_user')->insert([
                            'idrole' => $idRole,
                            'id_sub_menu' => $key,
                            'id' => $seq,
                        ]);
                    }


                    // delete data role previous
                    DB::table('ref_rel_menu')->where('id_role', $idRole)->delete();

                    foreach ($arrSubMenu as $code) {
                        // insert table ref_rel_role
                        DB::table('ref_rel_menu')->insert([
                            'id_role' => $idRole,
                            'code_menu' => $code
                        ]);
                    }
                break;

                // update sub menu
                case "27":
                    $idMenu = $request->input('id_menu');
                    $subMenu = $request->input('sub_menu');
                    $url = $request->input('url');

                    DB::table('ref_sub_menu')->where('id', $id)->update([
                        'idmenu' => $idMenu,
                        'submenu' => $subMenu,
                        'url' => $url
                    ]);
                break;

                // update manajemen user
                case "28":
                    $idRole = $request->input('id_role');
                    $idSubMenu = $request->input('id_sub_menu');

                    DB::table('manajemen_user')->where('id', $id)->update([
                        'idrole' => $idRole,
                        'id_sub_menu' => $idSubMenu
                    ]);
                break;

                // update product
                case "29":
                    $productName = $request->input('product_name');
                    $id = $request->input('id');
                    $seq = $request->input('seq');

                    if ( Auth::user()->user_role_id != 5 ) {
                        // User admin pusat
                        $companyId = Auth::user()->company_id;
                    } else {
                        // User super admin
                        $companyId = null;
                    }

                    DB::table('ref_product')->where('id', $id)->update([
                        'definition' => $productName,
                        'seq' => $seq,
                        'company_id' => $companyId
                    ]);
                break;

                // update underwriter
                case "30":
                    $definition = $request->input('definition');
                    $id = $request->input('id');
                    $seq = $request->input('seq');
                    $branchId = $request->input('branch_id');
                    $underwriter_code = $request->input('underwriter_code');
                    $address = $request->input('address');
                    $phone_no = $request->input('phone_no');
                    $fax_no = $request->input('fax_no');
                    $email = $request->input('email');
                    $pic = $request->input('pic');
                    $acc_no = $request->input('acc_no');
                    $acc_name = $request->input('acc_name');
                    $bank_name = $request->input('bank_name');

                    if ( Auth::user()->user_role_id != 5 ) {
                        // User admin pusat
                        $companyId = Auth::user()->company_id;
                    } else {
                        // User super admin
                        $companyId = null;
                    }

                    DB::table('ref_underwriter')->where('id', $id)->update([
                        'definition' => $definition,
                        'seq' => $seq,
                        'company_id' => $companyId,
                        'branch_id' => $branchId,
                        'underwriter_code' => $underwriter_code,
                        'address' => $address,
                        'phone_no' => $phone_no,
                        'fax_no' => $fax_no,
                        'email' => $email,
                        'pic' => $pic,
                        'acc_no' => $acc_no,
                        'acc_name' => $acc_name,
                        'bank_name' => $bank_name,
                    ]);
                break;

                // Update Qs Content
                case '99' :
                    $arrData = $request->all();
                    DB::TABLE('ref_qs_content')->where('id', $arrData['id'])->update([
                        'product_id' => $arrData['product'],
                        'form_wording' => $arrData['form_wording'],
                        'the_business' => $arrData['the_business'],
                        'coverage' => $arrData['coverage_content'],
                        'main_exclusions' => $arrData['main_exclusions_content'],
                        'deductible' => $arrData['deductible_content'],
                        'clauses' => $arrData['clauses_content'],
                        'period_default' => $arrData['period_default'],
                    ]);
                break;

                // Update Master Branch Customer
                case "100" :

                    $branchName = $request->input('branch_name');
                    $address = $request->input('address');
                    $provId = $request->input('province_id');
                    $cityId = $request->input('city_id');
                    $longitude = NULL;
                    $latitude = NULL;
                    $shortCode = $request->input('short_code');
                    $leaderName = $request->input('leader_name');
                    $jabatan = $request->input('jabatan');


                    $user = Auth::user();

                    DB::table('master_branch_cust')->where('id', $id)->update([
                        'branch_name' => $branchName,
                        'address' => $address,
                        'city_id' => $cityId,
                        'province_id' => $provId,
                        'long' => $longitude,
                        'lat' => $latitude,
                        'short_code' => strtoupper($shortCode),
                        'leader_name' => $leaderName,
                        'user_upd_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'company_id' => $user->company_id,
                        'jabatan' => $jabatan
                    ]);
                break;

            }

            return response()->json([
                'rc' => 0,
                'rm' => "Data Berhasil Diperbaharui"
            ]);
        }
    } // end function

    public function refSetActive(Request $request)
    {
        $refOrder = $request->input('ref_order');
        $id = (int)$request->input('id');
        switch ($refOrder) {
            // set active agent type
            case '1' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_agent_type')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Jenis agen berhasil dinonaktifkan';
                } else {
                    DB::table('ref_agent_type')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Jenis agen berhasil diaktifkan';
                }
            break;
            // set active bank account
            case '2' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_bank_account')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Account bank berhasil dinonaktifkan';
                } else {
                    DB::table('ref_bank_account')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Account bank berhasil diaktifkan';
                }
            break;
            // set active user role
            case '3' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_user_role')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'User Role berhasil dinonaktifkan';
                } else {
                    DB::table('ref_user_role')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'User Role berhasil diaktifkan';
                }
            break;
            // set active coa group
            case '4' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_coa_group')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'COA Group berhasil dinonaktifkan';
                } else {
                    DB::table('ref_coa_group')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'COA Group berhasil diaktifkan';
                }
            break;
            // set active coa type
            case '5' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_coa_type')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'COA Type berhasil dinonaktifkan';
                } else {
                    DB::table('ref_coa_type')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'COA Type berhasil diaktifkan';
                }
            break;
            // set active coa segment
            case '6' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_segment')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'COA Segment berhasil dinonaktifkan';
                } else {
                    DB::table('ref_segment')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'COA Segment berhasil diaktifkan';
                }
            break;
            // set active customer type
            case '7' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_cust_type')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Customer type berhasil dinonaktifkan';
                } else {
                    DB::table('ref_cust_type')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Customer type berhasil diaktifkan';
                }
            break;
            // set active customer segment
            case '8' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_cust_segment')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Customer segment berhasil dinonaktifkan';
                } else {
                    DB::table('ref_cust_segment')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Customer segment berhasil diaktifkan';
                }
            break;
            // set active paid status
            case '9' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_paid_status')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Paid status berhasil dinonaktifkan';
                } else {
                    DB::table('ref_paid_status')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Paid status berhasil diaktifkan';
                }
            break;
            // set active workflow status
            case '10' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_workflow_status')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Workflow status berhasil dinonaktifkan';
                } else {
                    DB::table('ref_workflow_status')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Workflow status berhasil diaktifkan';
                }
            break;
            // set active tx type
            case '13' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_tx_type')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Transaction type berhasil dinonaktifkan';
                } else {
                    DB::table('ref_tx_type')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Transaction type berhasil diaktifkan';
                }
            break;
            // set active master company
            case '14' :
                $isActive = $request->input('active');
                $user = Auth::user();

                if ($isActive == 't') {
                    DB::table('master_company')->where('id', $id)->update([
                        'is_active' => 'f',
                        'user_upd_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $reqMessage = 'Company berhasil dinonaktifkan';
                } else {
                    DB::table('master_company')->where('id', $id)->update([
                        'is_active' => 't',
                        'user_upd_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $reqMessage = 'Company berhasil diaktifkan';
                }
            break;
            // set active master branch
            case '15' :
                $isActive = $request->input('active');
                $user = Auth::user();

                if ($isActive == 't') {
                    DB::table('master_branch')->where('id', $id)->update([
                        'is_active' => 'f',
                        'user_upd_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $reqMessage = 'Branch berhasil dinonaktifkan';
                } else {
                    DB::table('master_branch')->where('id', $id)->update([
                        'is_active' => 't',
                        'user_upd_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    $reqMessage = 'Branch berhasil diaktifkan';
                }
            break;
            // set active master config
            case '16' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('master_config')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Konfigurasi berhasil dinonaktifkan';
                } else {
                    DB::table('master_config')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Konfigurasi berhasil diaktifkan';
                }
            break;
            // set active master user
            case '17' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('master_user')->where('id', $id)->update([
                        'is_active' => 'f',
                    ]);
                    $reqMessage = 'User berhasil dinonaktifkan';
                } else {
                    DB::table('master_user')->where('id', $id)->update([
                        'is_active' => 't',
                    ]);
                    $reqMessage = 'User berhasil diaktifkan';
                }
            break;
            // set active quotation
            case '18' :
                $user = Auth::user();
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_quotation')->where('id', $id)->update([
                        'is_active' => 'f',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_id' => $user->id
                    ]);
                    $reqMessage = 'Quotation berhasil dinonaktifkan';
                } else {
                    DB::table('ref_quotation')->where('id', $id)->update([
                        'is_active' => 't',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_id' => $user->id
                    ]);
                    $reqMessage = 'Quotation berhasil diaktifkan';
                }
            break;
            // set active proposal
            case '19' :
                $user = Auth::user();
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_proposal')->where('id', $id)->update([
                        'is_active' => 'f',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_id' => $user->id
                    ]);
                    $reqMessage = 'Proposal berhasil dinonaktifkan';
                } else {
                    DB::table('ref_quotation')->where('id', $id)->update([
                        'is_active' => 't',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_id' => $user->id
                    ]);
                    $reqMessage = 'Proposal berhasil diaktifkan';
                }
            break;
            // set active inventory
            case '21' :
                $user = Auth::user();
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_inventory')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Inventory berhasil dinonaktifkan';
                } else {
                    DB::table('ref_inventory')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Inventory berhasil diaktifkan';
                }
            break;
            // set active master coa
            case '22' :
                $user = Auth::user();
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('master_coa')->where('id', $id)->update([
                        'is_active' => 'f',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_at' => $user->id,
                    ]);
                    $reqMessage = 'Master COA berhasil dinonaktifkan';
                } else {
                    DB::table('master_coa')->where('id', $id)->update([
                        'is_active' => 't',
                        'updated_at' => date('Y-m-d H:i:s'),
                        'user_upd_at' => $user->id,
                    ]);
                    $reqMessage = 'Master COA berhasil diaktifkan';
                }
            break;
            // set active customer group
            case '23' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_cust_group')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Customer Group berhasil dinonaktifkan';
                } else {
                    DB::table('ref_cust_group')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Customer Group berhasil diaktifkan';
                }
            break;
            // set active Bdd
            case '24' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_bdd')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Bdd berhasil dinonaktifkan';
                } else {
                    DB::table('ref_bdd')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Bdd berhasil diaktifkan';
                }
            break;
            // set active agent
            case '25' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_agent')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Agent berhasil dinonaktifkan';
                } else {
                    DB::table('ref_agent')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Agent berhasil diaktifkan';
                }
            break;

            // set active product
            case '26' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_product')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Produk berhasil dinonaktifkan';
                } else {
                    DB::table('ref_product')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Produk berhasil diaktifkan';
                }
            break;

            // set active underwriter
            case '27' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_underwriter')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Underwriter berhasil dinonaktifkan';
                } else {
                    DB::table('ref_underwriter')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Underwriter berhasil diaktifkan';
                }
            break;

            // set active underwriter
            case '99' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_qs_content')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Content berhasil dinonaktifkan';
                } else {
                    DB::table('ref_qs_content')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Content berhasil diaktifkan';
                }
            break;

            // set active branch customer
            case '100' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('master_branch_cust')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Branch Customer berhasil dinonaktifkan';
                } else {
                    DB::table('master_branch_cust')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Branch Customer berhasil diaktifkan';
                }
            break;

            // set active insured detail
            case '101' :
                $isActive = $request->input('active');
                if ($isActive == 't') {
                    DB::table('ref_insured_detail')->where('id', $id)->update([
                        'is_active' => 'f'
                    ]);
                    $reqMessage = 'Berhasil dinonaktifkan';
                } else {
                    DB::table('ref_insured_detail')->where('id', $id)->update([
                        'is_active' => 't'
                    ]);
                    $reqMessage = 'Berhasil diaktifkan';
                }
            break;

        } // end switch

        return response()->json([
            'rc' => 1,
            'rm' => $reqMessage
        ]);

    } // end function

    public function setBranchOpen(Request $request)
    {
        $isOpen = $request->input('open');
        $id = $request->input('id');
        $user = Auth::user();
        if ($isOpen == 't') {
            $dtSystemDate = DB::table('ref_system_date')->select('current_date')->first();
            $currDate = date('Y-m-d');

            if (!empty($dtSystemDate)) {
                if ( $dtSystemDate->current_date != $currDate ) {
                    return response()->json([
                        'rc' => 2,
                        'rm' => 'Anda belum melakukan End Of Day'
                    ]);
                }
            }

            DB::table('master_branch')->where('id', $id)->update([
                'is_open' => 't',
                'user_upd_id' => $user->id,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $reqMessage = 'Branch berhasil dibuka';
        } else {
            DB::table('master_branch')->where('id', $id)->update([
                'is_open' => 'f',
                'user_upd_id' => $user->id,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            $reqMessage = 'Branch berhasil ditutup';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $reqMessage
        ]);
    }

    public function refShowEdit(Request $request, $id)
    {
        $refOrder = $request->input('ref_order');
        switch ($refOrder) {
            // agent type
            case '1' :
                $data = DB::table('ref_agent_type')->find($id);
            break;
            case '2' :
                $data = DB::table('ref_bank_account')->find($id);
            break;
            case '3' :
                $data = DB::table('ref_user_role')->find($id);
            break;
            case '4' :
                $data = DB::table('ref_coa_group')->find($id);
            break;
            case '5' :
                $data = DB::table('ref_coa_type')->find($id);
            break;
            case '6' :
                $data = DB::table('ref_segment')->find($id);
            break;
            case '7' :
                $data = DB::table('ref_cust_type')->find($id);
            break;
            case '8' :
                $data = DB::table('ref_cust_segment')->find($id);
            break;
            case '9' :
                $data = DB::table('ref_paid_status')->find($id);
            break;
            case '10' :
                $data = DB::table('ref_workflow_status')->find($id);
            break;
            case '11' :
                $data = DB::table('ref_city')->where('city_code', $id)->first();
            break;
            case '12' :
                $data = DB::table('ref_province')->where('province_code', $id)->first();
            break;
            case '13' :
                $data = DB::table('ref_tx_type')->find($id);
            break;
            case '14' :
                $data = DB::table('master_company')->find($id);
            break;
            case '15' :
                $data = DB::table('master_branch')->find($id);
            break;
            case '16' :
                $data = DB::table('master_config')->find($id);
            break;
            case '17' :
                $data = DB::table('master_user')->find($id);
            break;
            case '18' :
                $data = DB::table('ref_quotation')->find($id);
            break;
            case '19' :
                $data = DB::table('ref_proposal')->find($id);
            break;
            case '20' :
                $currStatusId = $request->input('status_id');
                $data = DB::table('master_workflow')
                            ->where('company_id', $id)
                            ->where('current_status_id', $currStatusId)
                            ->first();
            break;
            case '21' :
                $data = DB::table('ref_inventory')->find($id);
            break;
            case '22' :
                $data = DB::table('master_coa')->find($id);
            break;
            case '23' :
                $data = DB::table('ref_cust_group')->find($id);
            break;
            case '24' :
                $data = DB::table('ref_bdd')->find($id);
            break;
            case '25' :
                $data = DB::table('ref_agent')->find($id);
            break;
            // setting menu user role
            case '26' :
                $data['userRole'] = DB::table('ref_user_role')->select('id', 'definition')->where('id', $id)->first();
                $data['menuAccess'] = DB::table('ref_rel_menu')->where('id_role', $id)->get();
                $type = [];
                $nama = null;
                if ( count($data['menuAccess']) > 0 ) {
                    foreach ($data['menuAccess'] as $item) {
                        $dt = DB::table('ref_menu')->select('type')->where('code' , $item->code_menu)->first();
                        if ( !is_null($dt) ) {
                            if ($dt->type != $nama) {
                                $type[] = $dt;
                            }
                            $nama = $dt->type;
                        }

                    }
                }
                $data['typeMenu'] = $type;
            break;
            case '27' :
                $data = DB::table('ref_sub_menu')->find($id);
            break;
            case '28' :
                $data = DB::table('manajemen_user')->find($id);
            break;
            case '29' :
                $data = DB::table('ref_product')->find($id);
            break;
            case '30' :
                $data = DB::table('ref_underwriter')->find($id);
            break;
            case '100' :
                $data = DB::table('master_branch_cust')->find($id);
            break;
            case '101' :
                $data = DB::TABLE('ref_insured_detail')
                            ->leftJoin('ref_product', 'ref_product.id', '=', 'ref_insured_detail.product_id')
                            ->where('ref_insured_detail.id', $id)
                            ->first();
            break;
        }

        return response()->json([
            'rc' => 1,
            'data' => $data
        ]);
    } // end function

    function showHistoryTx($id, Request $request)
    {
        $results = DB::table('master_coa')
        ->where('coa_no', $id)
        ->where('branch_id',Auth::user()->branch_id)
        ->first();

        $param['data'] = $results;
        $param['type'] = $request->get('type');
        $param['idbranch'] = Auth::user()->branch_id;

        if (Req::ajax()) {
                return view('master.only_content')->nest('child', 'reff.master_coa.history_tx', $param);
        } else {
                return view('master.master')->nest('child', 'reff.master_coa.history_tx', $param);
        }
    }

    function getHistoryTx(Request $request)
    {

        $coaNo = $request->input('coa_no');
        $idbranch = $request->input('idbranch');

        // $tglMulai = join('/', array_reverse(explode('/', $request->input('tgl_mulai'))));
        // $tglSelesai = join('/', array_reverse(explode('/', $request->input('tgl_selesai'))));

        if ( is_null($idbranch) ) {
            // Cabang yang login
            $tglMulai = date('Y-m-d', strtotime($request->input('tgl_mulai')));
            $tglSelesai = date('Y-m-d', strtotime($request->input('tgl_selesai')));
            $result = DB::table('master_tx')
                            ->whereNotNull('seq_approval')
                            ->where('coa_id', $coaNo)
                            ->where('branch_id', Auth::user()->branch_id)
                            ->whereBetween('tx_date', [$tglMulai, $tglSelesai])
                            ->where(function($query){
                                $query->where('id_workflow', 9)
                                      ->orWhere('id_workflow', 3)
                                      ->orWhere('id_workflow', 12)
                                      ->orWhere('id_workflow', 15);
                            })
                            ->orderBy('seq_approval', 'asc')
                            ->get();
        } else {
            $tglMulai = date('Y-m-d', strtotime($request->input('tgl_mulai')));
            $tglSelesai = date('Y-m-d', strtotime($request->input('tgl_selesai')));
            $result = DB::table('master_tx')
                            ->whereNotNull('seq_approval')
                            ->where('coa_id', $coaNo)
                            ->where('branch_id', $idbranch)
                            ->whereBetween('tx_date', [$tglMulai, $tglSelesai])
                            ->where(function($query){
                                $query->where('id_workflow', 9)
                                    ->orWhere('id_workflow', 3)
                                    ->orWhere('id_workflow', 12)
                                    ->orWhere('id_workflow', 15);
                            })
                            ->orderBy('seq_approval', 'asc')
                            ->get();
        }


        return response()->json([
            'rc' => 1,
            'data' => $result,
            'no_rek' => $coaNo
        ]);
    }

    public function getCOAForBDD(Request $request)
    {
        $coa = \DB::table('master_coa')
                        ->where('is_active', 't')
                        ->where('is_parent', 'f')
                        ->whereNotNull('coa_parent_id')
                        ->where('branch_id', $request->input('idBranch'))
                        ->orderBy('coa_no', 'ASC')
                        ->get();

        return response()->json([
            'rc' => 1,
            'data' => $coa
        ]);
    }

    public function createQsContent(Request $request, $action, $id=null) {


        $product = DB::TABLE('ref_product')->where('is_active', 't');
        $idProduct = DB::TABLE('ref_qs_content')->select('product_id');

        if ( !is_null($id) ) {
            // Edit
            $data = DB::TABLE('ref_qs_content')
                        ->where('id', $id)
                        ->first();

            $idProduct = $idProduct->where('product_id', '!=', $data->product_id);
            $param['data'] = $data;
        }

        $idProduct = $idProduct->get();
        $arrIdProduct = $idProduct->pluck('product_id')->toArray();
        $product = $product->whereNotIn('id', $arrIdProduct);
        $product = $product->get();

        $param['action'] = $action;
        $param['products'] = $product;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'reff.qs_content.form', $param);
        } else {
            return view('master.master')->nest('child', 'reff.qs_content.form', $param);
        }
    }

    public function storeInsuredSingle(Request $request)
    {

        $input_name = str_replace('_', '', $request->label);
        $input_name = str_replace(' ', '_', $input_name);
        $input_name = strtolower($input_name);


        if ( $request->format_input == 1 ) {
            // Angka
            $is_number = 't';
            $is_currency = 'f';
            $is_rate = 'f';
            $is_date = 'f';
        } elseif ( $request->format_input == 2 ) {
            // Mata Uang
            $is_currency = 't';
            $is_number = 't';
            $is_rate = 'f';
            $is_date = 'f';
        } else if ( $request->format_input == 3 ) {
            // Presentase
            $is_rate = 't';
            $is_number = 't';
            $is_currency = 'f';
            $is_date = 'f';
        } else if ( $request->format_input == 5 ) {
            // Tanggal
            $is_rate = 'f';
            $is_number = 'f';
            $is_currency = 'f';
            $is_date = 't';
        } else {
            // Format Input 4
            // Text
            $is_rate = 'f';
            $is_number = 'f';
            $is_currency = 'f';
            $is_date = 'f';
        }

        DB::TABLE('ref_insured_detail')
                ->where('id', $request->id)
                ->update([
                    'label' => $request->label,
                    'input_name' => $input_name,
                    'is_number' => $is_number,
                    'is_currency' => $is_currency,
                    'is_rate' => $is_rate,
                    'is_required' => $request->is_required,
                    'is_showing' => $request->is_showing,
                    'is_filterable' => $request->is_filterable,
                    'is_date' => $is_date
                ]);

        return response()->json([
            'rc' => 0,
            'rm' => "Data Berhasil Disimpan"
        ]);

    }

    public function createInsuredDetail(Request $request) {
        $arrProd = DB::TABLE('ref_insured_detail')
                ->select('product_id')
                ->distinct()
                ->get();


        $arrProd = $arrProd->pluck('product_id')->toArray();


        $product = DB::TABLE('ref_product')->get();
                        // ->whereNotIn('id', $arrProd)


        $param['product'] = $product;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'reff.insured_detail.create', $param);
        } else {
            return view('master.master')->nest('child', 'reff.insured_detail.create', $param);
        }
    }

    public function editInsuredDetail(Request $request, $id) {

        $product = DB::TABLE('ref_product')
                    ->where('id', $id)
                    ->get();

        $data = DB::TABLE('ref_insured_detail')
                    ->where('product_id', $id)
                    ->get();

        $param['detail'] = $data;
        $param['product'] = $product;

        $param['id'] = $id;
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'reff.insured_detail.edit', $param);
        } else {
            return view('master.master')->nest('child', 'reff.insured_detail.edit', $param);
        }

    } // end function

    public function changeInsuredDetail(Request $request) {

        $type = $request->type;
        $arrId = $request->data;
        $condition = "";

        switch ( $type ) {
            case 'non_aktif' :
                $condition = "SET is_active = 'f'";
            break;
            case 'aktif' :
                $condition = "SET is_active = 't'";
            break;
            case 'non_required' :
                $condition = "SET is_required = 'f'";
            break;
            case 'required' :
                $condition = "SET is_required = 't'";
            break;
            case 'non_filterable' :
                $condition = "SET is_filterable = 'f'";
            break;
            case 'filterable' :
                $condition = "SET is_filterable = 't'";
            break;
        }

        foreach ( $arrId as $id ) {
            $query = "UPDATE ref_insured_detail " . $condition . " WHERE id=" . $id;
            DB::SELECT($query);
        } // end foreach

        return response()->json([
            'rc' => 1,
            'rm' => 'Berhasil diubah'
        ]);
    } // end function

    public function getDataRefUsia(Request $request)
    {
        $data = DB::TABLE('ref_rate_usia')
                    ->orderBy('usia', 'ASC')
                    ->orderBy('tenor', 'ASC')
                    ->get();

        return DataTables::of($data)
            ->addColumn('action', function ($item) {
                return '<button class="btn btn-sm btn-danger> Hapus <button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    } // end function

}
