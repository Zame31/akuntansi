<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Validator, Redirect, Auth, Session, DB, Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
      public function username()
    {
        return 'username';
    }


    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $user = DB::table('master_user')->where('username', $request->input('username'))->first();
        if (!is_null($user)) {
            
            if (!Hash::check($request->input('password'), $user->password)){
                return response()->json([
                    'rc' => 0,
                    'rm' => 'Username atau Password salah'
                ]);
            }

            if (!$user->is_active){
                return response()->json([
                    'rc' => 1,
                    'rm' => 'Akun anda tidak aktif. Silahkan hubungi admin.'
                ]);
            }

            if (!$user->is_login){
                return response()->json([
                    'rc' => 2,
                    'rm' => 'success',
                    'id_user' => $user->id
                ]);
            }

            Session::put('id', $user->id);
            Session::put('roleId', $user->user_role_id);
            Auth::loginUsingId($user->id);
            return response()->json([
                'rc' => 3,
                'rm' => 'success'
            ]);
            

        } else {
            // login failed
            return response()->json([
                'rc' => 0,
                'rm' => 'Username atau Password salah'
            ]);
        }

        // if (Auth::attempt($credentials)) {
        //     $user = Auth::user();
        //     Session::put('id', Auth::id());
        //     Session::put('roleId', $user->user_role_id);
        //     if ($user->is_active == false) {
        //         return response()->json([
        //             'rc' => 1,
        //             'rm' => 'Akun anda tidak aktif. Silahkan hubungi admin'
        //         ]);    
        //     }

        //     // return redirect('/home');
        // } else {
        //     // login failed
        //     return response()->json([
        //         'rc' => 0,
        //         'rm' => 'Username atau Password salah'
        //     ]);
        // }
    }

    public function reset_password(Request $request)
    {
        
        $passHashed = bcrypt($request->input('new_password'));
        $idUser = $request->input('id');
        DB::table('master_user')->where('id', $idUser)->update([
            'password' => $passHashed,
            'is_login' => 't'
        ]);

        return response()->json([
            'rc' => 0,
            'rm' => 'Password berhasil diubah'
        ]);
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function setSession($user){
        session([
            'user' => $user
        ]);
    }
}
