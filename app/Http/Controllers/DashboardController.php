<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use App\User;

use Request as Req;
use Illuminate\Support\Collection;
use Yajra\DataTables\DataTables;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index(Request $request)
    {
      if(Auth::user()->user_role_id==5){
        $branch = DB::select("select * from master_branch");
       $company = DB::select("select * from master_company");
       $param['branch']=$branch;
       $param['company']=$company;
       $param['role']=Auth::user()->user_role_id;

      }else{
        $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
       $company = DB::select("select * from master_company");
       $param['branch']=$branch;
       $param['company']=$company;
       $param['role']=Auth::user()->user_role_id;
      }


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'home',$param);
        }else {
            return view('master.master')->nest('child', 'home',$param);
        }
    }

    public function coa(Request $request){
      //if($request->value){
        /*
        $coa=DB::table('master_coa')
        ->select('coa_no','coa_name','balance_type_id')
        ->whereNotIn('coa_no', $request->value)
        ->where('is_parent',false)
        ->OrderBy('id','asc')
        ->get();
        */
      //}else{

       $coa = \DB::select("SELECT coa_no,coa_name,balance_type_id FROM master_coa where is_active is true and branch_id = ".Auth::user()->branch_id." and ((is_parent=false and coa_parent_id is not null) or coa_no in ('302','303')) order by id asc");

        //$coa = \DB::select('SELECT coa_no,coa_name,balance_type_id FROM master_coa where is_parent=false');

      //}
      return json_encode($coa);

    }
    public function cek_email(Request $request){

      if($request->get('idcust')){
        $query = \DB::select("SELECT * FROM master_customer where id=".$request->get('idcust'));
        if($query[0]->email!=$request->get('id')){
            $email = \DB::select("SELECT * FROM master_customer where email='".$request->get('id')."'");
            return json_encode($email);
        }else{
            $email = '';
            return json_encode($email);
        }


      }else{
        $email = \DB::select("SELECT * FROM master_customer where email='".$request->get('id')."'");
        return json_encode($email);
      }



    }

    public function ref_province($id){
      $city = \DB::select('SELECT * FROM ref_city where city_code='.$id);
      $prov = \DB::select('SELECT * FROM ref_province where province_code='.$city[0]->province_code);
      return json_encode($prov);

    }
    public function dok(Request $request){
        if($request->value){

        $dokumen =DB::table('ref_doc_type')
        ->whereNotIn('id', $request->value)
        ->where('is_active',true)
        ->OrderBy('seq','asc')
        ->get();
      }else{
        $dokumen = \DB::select('SELECT * FROM ref_doc_type where is_active=true order by seq asc');

      }

        return json_encode($dokumen);
    }

    public function dbcr(Request $request)
    {
       $dbcr = \DB::select("SELECT balance_type_id  FROM master_coa where coa_no='".$request->get('id')."'");
       return json_encode($dbcr);
    }

    public function refPolis(Request $request)
    {
       $polis = \DB::select("SELECT polis_no  FROM master_sales where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
       return json_encode($polis);
    }

    public function byPolis(Request $request)
    {
       $polis = \DB::select("select
                                a.id,
                                b.full_name,
                                c.definition,
                                e.full_name as agent,
                                f.qs_no,g.definition as bank
                            from master_sales a
                            left join master_customer b on b.id=a.customer_id
                            left join ref_product c on c.id=a.product_id
                            left join ref_paid_status d on d.id=a.paid_status_id
                            left join ref_agent e on e.id=a.agent_id
                            left join ref_quotation f on f.id=a.qs_no
                            left join ref_bank_account g on g.id=a.afi_acc_no where a.polis_no='".$request->get('id')."'");
       return json_encode($polis);
    }

    public function cekPolis(Request $request)
    {
       $polis = \DB::select("select * from master_sales a where a.polis_no='".$request->get('id')."'");
       return json_encode($polis);
    }
    public function cekSalesPolis(Request $request)
    {
       $polis = \DB::select("select * from master_sales_polis a where a.polis_no='".$request->get('id')."'");
       return json_encode($polis);
    }

    public function history_claim(Request $request)
    {
       $data = \DB::select("select a.*,b.status_definition from master_claim a left join ref_claim_status b on b.id=a.claim_status_id where polis_no='".$request->get('id')."' and workflow_status_id in (1,3,4,5) order by a.id desc");

       return DataTables::of($data)
       ->addColumn('action', function ($data) {

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" onclick="editdata('.$data->id.')">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>
              <a class="dropdown-item" onclick="deletedata('.$data->id.')">
                  <i class="la la-edit"></i>
                  <span>Delete</span>
              </a>
            </div>
        </div>
        ';
        })
       ->addColumn('cek', function ($data) {
        return '
        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
            <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
          <span></span>
        </label>';
        })
       ->addColumn('workflow_status_id',function($data) {
            if($data->workflow_status_id==1){

              return '<span class="kt-badge kt-badge--warning kt-badge--inline">new claim</span>';

            }elseif($data->workflow_status_id==3){

              return '<span class="kt-badge kt-badge--success kt-badge--inline">approve</span>';

            }elseif($data->workflow_status_id==4){

              return '<span class="kt-badge kt-badge--danger kt-badge--inline">reject</span>';
            }else{
              return '<span class="kt-badge kt-badge--info kt-badge--inline">update progress</span>';
            }

        })
       ->editColumn('claim_start_date',function($data) {
           if ( is_null($data->claim_start_date)) {
               return '-';
           }
            return date('d M Y',strtotime($data->claim_start_date));
        })
       ->editColumn('claim_end_date',function($data) {
            return date('d M Y',strtotime($data->claim_end_date));
        })
       ->editColumn('loss_amount',function($data) {
            return number_format($data->loss_amount,2,',','.');
        })
       ->editColumn('claim_amount',function($data) {
            return number_format($data->claim_amount,2,',','.');
        })
       ->rawColumns(['action','cek','workflow_status_id'])
       ->make(true);

    }

    public function history_notes(Request $request)
    {
       $data = \DB::select("SELECT b.ref_code_type,b.notes,b.created_at,c.fullname FROM master_sales a
          left join master_memo b on b.ref_code=a.id
          left join master_user c on c.id=b.user_crt_id
          where b.ref_code=".$request->get('id'));

       return DataTables::of($data)
      ->editColumn('created_at',function($data) {
            return date('d M Y',strtotime($data->created_at));
        })
       ->make(true);

    }

    public function data($filter)
    {


        if ($filter == "all") {
            $f = "";
        }
        elseif ($filter == "t_last_month") {
            $f = "  EXTRACT(MONTH FROM ms.created_at) = ".date("m", strtotime("-1 months"))." and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');
        }
        elseif ($filter == "t_month") {
            $f = " EXTRACT(MONTH FROM ms.created_at) = ".date('m')." and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');
        }
        elseif ($filter == "year_t_date") {
            $f = " EXTRACT(MONTH FROM ms.created_at) <= ".date('m')." and EXTRACT(MONTH FROM ms.created_at) >= 1
            and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');

        }


        if(Auth::user()->user_role_id==6){

          $c_all = collect(\DB::select("select
        sum(case when coa_no in ('100','101','102','103') then last_os end) as total_aset,
        sum(case when coa_no in ('301') then last_os end) as profit_loss,
        sum(case when coa_no in ('400') then last_os end) as total_income,
        sum(case when coa_no in ('500') then last_os end) as total_outcome,
        sum(case when coa_no in ('500001') then last_os end) as marketing_outcome,
        sum(case when coa_no in ('500002') then last_os end) as building_outcome,
        sum(case when coa_no in ('500003') then last_os end) as general_outcome,
        sum(case when coa_no in ('500004') then last_os end) as resources_outcome
        from
        (select
        coa_no,
        coa_name,
        case when coa_no in ('301') then last_os +
        (WITH RECURSIVE cte AS (
           SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
           FROM   master_coa
           WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
           UNION  ALL
           SELECT c.coa_no, p.coa_no, p.last_os
           FROM   cte c
           JOIN   (select * from master_coa where company_id=".Auth::user()->company_id.") p USING (coa_parent_id)
        )
        SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
        FROM   cte
        where coa_no in ('400','500')) else last_os end as last_os
        from
        (select
        a.coa_no,
        coa_name,
        case when a.coa_no = b.coa_parent_id then b.nom
           when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
        from master_coa a left join
        (select coa_parent_id, sum(coalesce(last_os,0)) as nom
        from master_coa
        where coa_parent_id is  not null and length(coa_parent_id) = 6
        group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
        (WITH RECURSIVE cte AS (
           SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
           FROM   master_coa
           WHERE  coa_parent_id IS NULL
           UNION  ALL
           SELECT c.coa_no, p.coa_no, p.last_os
           FROM   cte c
           JOIN   master_coa p USING (coa_parent_id)
        )
        SELECT coa_no, sum(last_os) AS last_os
        FROM   cte
        GROUP  BY 1) c on a.coa_no = c.coa_no where company_id=".Auth::user()->company_id.") x) x "))->first();


          $c_customer = collect(\DB::select("select COALESCE(sum(last_os),0) as d from master_coa where coa_no = '200006' and company_id=".Auth::user()->company_id))->first();
          $c_insured = collect(\DB::select("select COALESCE(sum(ins_fee),0) as d from master_sales ms WHERE is_premi_paid is null and invoice_type_id in (0,1) and wf_status_id in (9,16,17,18) and company_id=".Auth::user()->company_id." and ".$f))->first();
          $c_premi = collect(\DB::select("select COALESCE(sum(agent_fee_amount),0) as d from master_sales ms WHERE is_agent_paid is null and invoice_type_id in (0,1) and wf_status_id in (9,16,17,18) and company_id=".Auth::user()->company_id))->first();
          $c_fee = collect(\DB::select("select COALESCE(sum(comp_fee_amount+admin_amount),0) as d from master_sales ms WHERE is_company_paid is null and invoice_type_id in (0,1) and wf_status_id in (9,16,17,18) and company_id=".Auth::user()->company_id." and ".$f))->first();




        }else{
          $c_all = collect(\DB::select("select
        sum(case when coa_no in ('100','101','102','103') then last_os end) as total_aset,
        sum(case when coa_no in ('301') then last_os end) as profit_loss,
        sum(case when coa_no in ('400') then last_os end) as total_income,
        sum(case when coa_no in ('500') then last_os end) as total_outcome,
        sum(case when coa_no in ('500001') then last_os end) as marketing_outcome,
        sum(case when coa_no in ('500002') then last_os end) as building_outcome,
        sum(case when coa_no in ('500003') then last_os end) as general_outcome,
        sum(case when coa_no in ('500004') then last_os end) as resources_outcome
        from
        (select
        coa_no,
        coa_name,
        case when coa_no in ('301') then last_os +
        (WITH RECURSIVE cte AS (
           SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
           FROM   master_coa
           WHERE  coa_parent_id IS NULL and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id."
           UNION  ALL
           SELECT c.coa_no, p.coa_no, p.last_os
           FROM   cte c
           JOIN   (select * from master_coa where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id.") p USING (coa_parent_id)
        )
        SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
        FROM   cte
        where coa_no in ('400','500')) else last_os end as last_os
        from
        (select
        a.coa_no,
        coa_name,
        case when a.coa_no = b.coa_parent_id then b.nom
           when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
        from master_coa a left join
        (select coa_parent_id, sum(coalesce(last_os,0)) as nom
        from master_coa
        where coa_parent_id is  not null and length(coa_parent_id) = 6
        group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
        (WITH RECURSIVE cte AS (
           SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
           FROM   master_coa
           WHERE  coa_parent_id IS NULL
           UNION  ALL
           SELECT c.coa_no, p.coa_no, p.last_os
           FROM   cte c
           JOIN   master_coa p USING (coa_parent_id)
        )
        SELECT coa_no, sum(last_os) AS last_os
        FROM   cte
        GROUP  BY 1) c on a.coa_no = c.coa_no where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id.") x) x "))->first();

          $c_customer = collect(\DB::select("select COALESCE(sum(last_os),0) as d from master_coa where coa_no = '200006' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id))->first();
          $c_insured = collect(\DB::select("select COALESCE(sum(ins_fee),0) as d from master_sales ms WHERE is_premi_paid is null and invoice_type_id in (0,1) and wf_status_id in (9,16,17,18) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." and ".$f))->first();
          $c_premi = collect(\DB::select("select COALESCE(sum(agent_fee_amount),0) as d from master_sales ms WHERE is_agent_paid is null and invoice_type_id in (0,1) and wf_status_id in (9,16,17,18) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id))->first();
          $c_fee = collect(\DB::select("select COALESCE(sum(comp_fee_amount+admin_amount),0) as d from master_sales ms WHERE is_company_paid is null and invoice_type_id in (0,1) and wf_status_id in (9,16,17,18) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." and ".$f))->first();

        }

        // $c_premi = collect(\DB::select("select COALESCE(sum(premi_amount),0) as d from master_sales ms where wf_status_id not in(1) and ".$f))->first();
         // $c_fee = collect(\DB::select("select COALESCE(sum(comp_fee_amount),0) as d from master_sales ms where ".$f))->first();
        // $c_profit = collect(\DB::select("select COALESCE(sum(last_os),0) as d from master_coa ms".$f))->first();

        $c_profit['d'] = $c_all->profit_loss;

        $c_customer_type = \DB::select("select rct.id,definition,COALESCE(sum(net_amount),0) as d,
        concat( COALESCE(sum(net_amount) / (SELECT sum(net_amount) as zz from master_sales  ms where".$f." )*100,0) ,' %' ) as presentase
        from ref_cust_type rct
        left join master_customer mc on mc.cust_type_id = rct.id
        left join master_sales ms on ms.customer_id = mc.id where ".$f."
				GROUP BY rct.id,definition");

        $c_segment = \DB::select("select rct.id,definition,COALESCE(sum(net_amount),0) as d,
        COALESCE((sum(net_amount) / (SELECT sum(net_amount) as zz from master_sales ms where".$f."))*100,0) as presentase
        from ref_cust_segment rct
        left join master_sales ms on ms.segment_id = rct.id where ".$f."
        GROUP BY rct.id,definition");

        // $t_sales = \DB::select("select full_name, count(full_name) as d,
        // concat( COALESCE(round((round(count(full_name),2) / (SELECT round(count(agent_id),2) as zz from master_sales ms where".$f."))*100,2),0) ,' %' ) as presentase
        //        from ref_agent ra
        //        left join master_sales ms on ms.agent_id = ra.id where ".$f."
        //        GROUP BY full_name
        //        order by d desc
        //        limit 3");

        $t_sales = \DB::select("select full_name, COALESCE(sum(premi_amount),0) as d,
        COALESCE(sum(premi_amount),0) / (SELECT COALESCE(sum(premi_amount),0) as zz from master_sales ms where".$f.")*100  as presentase from ref_agent ra
               left join master_sales ms on ms.agent_id = ra.id where ".$f."
							 GROUP BY full_name
               order by d desc
               limit 3");

        $t_premi = \DB::select("select full_name, COALESCE(sum(premi_amount),0) as d,
        COALESCE((sum(premi_amount) / (SELECT sum(premi_amount) as zz from master_sales ms where".$f."))*100,0) as presentase
        from master_customer ra
        left join master_sales ms on ms.customer_id = ra.id where".$f."
        GROUP BY full_name
        order by d desc
        limit 3");

        $data['c_insured'] = $c_insured;
        $data['c_customer'] = $c_customer;
        $data['c_premi'] = $c_premi;
        $data['c_fee'] = $c_fee;
        $data['c_profit'] = $c_profit;

        if ($t_sales) {
            foreach ($t_sales as $key => $value) {
                $temp_t_sales_name[] = $value->full_name;
                $temp_t_sales_data[] = $value->d;
                $temp_t_sales_pres[] = $value->presentase;
            }
        }else {
            $temp_t_sales_name[] = "-";
            $temp_t_sales_data[] = 0;
            $temp_t_sales_pres[] = "0%";
        }

        if ($t_premi) {
            foreach ($t_premi as $key => $value) {
                $temp_t_premi_name[] = $value->full_name;
                $temp_t_premi_data[] = $value->d;
                $temp_t_premi_pres[] = $value->presentase;
            }
        }else {
            $temp_t_premi_name[] = "-";
            $temp_t_premi_data[] = 0;
            $temp_t_premi_pres[] = "0%";
        }


        if ($c_segment) {
            foreach ($c_segment as $key => $value) {
                $temp_seg_def[] = $value->definition;
                $temp_seg_data[] = $value->d;
                $temp_seg_pres[] = $value->presentase;
            }
        }else {
            $temp_seg_def[] = "-";
            $temp_seg_data[] = 0;
            $temp_seg_pres[] = "0%";
        }


        if ($c_customer_type) {
            foreach ($c_customer_type as $key => $value) {
                $temp_cust_def[] = $value->definition;
                $temp_cust_data[] = $value->d;
                $temp_cust_pres[] = $value->presentase;
            }
        }else {
            $temp_cust_def[] = "-";
            $temp_cust_data[] = 0;
            $temp_cust_pres[] = "0%";
        }



        $data['cust_def'] = $temp_cust_def;
        $data['cust_data'] = $temp_cust_data;
        $data['cust_pres'] = $temp_cust_pres;

        $data['seg_def'] = $temp_seg_def;
        $data['seg_data'] = $temp_seg_data;
        $data['seg_pres'] = $temp_seg_pres;

        $data['t_sales_nama'] = $temp_t_sales_name;
        $data['t_sales_data'] = $temp_t_sales_data;
        $data['t_sales_pres'] = $temp_t_sales_pres;

        $data['t_premi_nama'] = $temp_t_premi_name;
        $data['t_premi_data'] = $temp_t_premi_data;
        $data['t_premi_pres'] = $temp_t_premi_pres;



        $comma = ",";

        for ($bulan=1; $bulan <= 12; $bulan++) {
            if ($bulan == 12) {$comma = "";} else {$comma = ",";}

            if(Auth::user()->user_role_id==6){


            $c_grafik_premi = collect(\DB::select("select COALESCE(sum(comp_fee_amount),0) as d from master_sales ms where is_company_paid = 1
            and company_id=".Auth::user()->company_id ." and EXTRACT(MONTH FROM created_at) = ".$bulan))->first();

            $c_grafik_profit = collect(\DB::select("select COALESCE(sum(profit_loss),0) as d from master_financial
            where company_id=".Auth::user()->company_id ." and EXTRACT(MONTH FROM report_date) = ".$bulan))->first();

          }else{
              $c_grafik_premi = collect(\DB::select("select COALESCE(sum(comp_fee_amount),0) as d from master_sales ms where is_company_paid = 1 and
              branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id ." and EXTRACT(MONTH FROM created_at) = ".$bulan))->first();

              $c_grafik_profit = collect(\DB::select("select COALESCE(sum(profit_loss),0) as d from master_financial
              where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id ." and EXTRACT(MONTH FROM report_date) = ".$bulan))->first();

            }

            $temp_grafik_premi[] = $c_grafik_premi->d;

            $temp_grafik_profit[] = $c_grafik_profit->d;
        }



        $data_grafik['Comm Due To Us'] = $temp_grafik_premi;
        $data_grafik['Profit/Loss'] = $temp_grafik_profit;

        //LINE GRAFIK
        $data['grafik_premi'] = $data_grafik;

        return json_encode($data);
    }


    public function fee(Request $request)
    {
       $premi_amount_config = \DB::select("select value as jml from master_config where id = 13 and company_id=".Auth::user()->company_id);
       $discount_amount_config = \DB::select("select value as jml from master_config where id = 9 and company_id=".Auth::user()->company_id);
       $fee_agent_config = \DB::select("select value as jml from master_config where id = 2 and company_id=".Auth::user()->company_id);
       $fee_internal_config = \DB::select("select value  as jml from master_config where id = 3 and company_id=".Auth::user()->company_id);
       $tax_amount_config = \DB::select("select value  as jml from master_config where id = 8 and company_id=".Auth::user()->company_id);

       $jml=str_replace(',', '.', $request->get('jml'));


       $premi_amount=($premi_amount_config[0]->jml * $jml) / 100;
       $discount=($discount_amount_config[0]->jml * $premi_amount) / 100;
       $nett_amount=$premi_amount - $discount;
       $fee_agent=($fee_agent_config[0]->jml * $premi_amount) / 100;
       $komisi_perusahaan=($fee_internal_config[0]->jml * $premi_amount) / 100;


       $asuransi = $nett_amount - $fee_agent - $komisi_perusahaan;



       $tax_amount=($tax_amount_config[0]->jml * $fee_agent) / 100;


       return json_encode(['premi_amount'=>$premi_amount,'discount'=>$discount,'nett_amount'=>$nett_amount,'fee_agent'=>$fee_agent,'komisi_perusahaan'=>$komisi_perusahaan,'asuransi'=>$asuransi,'tax_amount'=>$tax_amount]);
    }

    public function fee1(Request $request)
    {
       $fee_agent_config = \DB::select("select value as jml from master_config where id = 2 and company_id=".Auth::user()->company_id);
       $fee_internal_config = \DB::select("select value  as jml from master_config where id = 3 and company_id=".Auth::user()->company_id);
       $tax_amount_config = \DB::select("select value  as jml from master_config where id = 8 and company_id=".Auth::user()->company_id);


       $premi_amount=str_replace(',', '.', $request->get('jml'));

       $fee_agent=($fee_agent_config[0]->jml * $premi_amount) / 100;
       $komisi_perusahaan=($fee_internal_config[0]->jml * $premi_amount) / 100;
       //$asuransi=$nett_amount - $fee_agent - $komisi_perusahaan;
       $tax_amount=($tax_amount_config[0]->jml * $fee_agent) / 100;


       return json_encode(['fee_agent'=>$fee_agent,'komisi_perusahaan'=>$komisi_perusahaan,'tax_amount'=>$tax_amount]);
    }



    public function set_end_of_day(){
      $date_now=date('Y-m-d');
      $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

      if($date_now==$systemDate->current_date){
        return json_encode(['rc'=>2,'msg'=>'End Of Day is already processed']);
      }else{
        $upd_date = date('Y-m-d', strtotime('+1 days', strtotime($systemDate->current_date)));

          DB::table('ref_system_date')
          ->where('id', 1)
          ->update(['current_date' => $upd_date,'last_date'=>$systemDate->current_date]);

        return json_encode(['rc'=>1,'msg'=>'End Of Day Process Finished']);
      }


    }

    public function endofyear(){
           $branch = \DB::select("select * from master_branch where company_id=".Auth::user()->company_id);

           if($branch){
              foreach ($branch as $value) {

                  $master_coa = \DB::select("select * from master_coa where coa_type_id in (3,4) 
                  and company_id=".Auth::user()->company_id." and branch_id=".$value->id." and last_os is not null 
                  and last_os not in (0) order by coa_no asc");

                  if($master_coa){
                    foreach ($master_coa as $item) {

                        $txcode='';
                        $tgl_tr=date('Y-m-d');

                        $no_urut = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd')."' and branch_id=".$value->id." and company_id=".Auth::user()->company_id);

                        $prx=date('Ymd');
                        $prx_branch=Auth::user()->branch_id;
                        $prx_company=Auth::user()->company_id;

                        if($no_urut){

                          $id_max= $no_urut[0]->max_id;
                          $sort_num = (int) substr($id_max, 1, 4);
                          $sort_num++;
                          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                          $txcode=$new_code;
                        }else{
                            $txcode=$prx.$prx_branch.$prx_company."0001";
                        }

                        $id='';
                        $id_urut = \DB::select("SELECT max(id) AS id from master_tx");
                        if($id_urut){
                            $id=$id_urut[0]->id + 1;
                        }else{
                            $id=1;
                        }


                        if($item->coa_type_id==3){

                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => 0,
                                    'coa_id' => $item->coa_no,
                                    'tx_notes' => 'end of year'.'-'.date('Y'),
                                    'tx_amount' => $item->last_os,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => 0,
                                    'id_workflow' => 9,
                                    'user_crt_id' =>'999',
                                    'user_app_id' =>'999',
                                    'branch_id' => $value->id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'coa_type_id' => 3,
                                    'type_tx' => 'end_of_year'.'-'.date('Y')
                                ]
                            );

                            $coa_trx = collect(\DB::select("select * from master_tx where id=".$id))->first();
                            if($coa_trx){

                                $id_='';
                                $id_urut = \DB::select("SELECT max(id) AS id from master_tx");
                                if($id_urut){
                                    $id_=$id_urut[0]->id + 1;
                                }else{
                                    $id_=1;
                                }


                                DB::table('master_tx')->insert(
                                  [
                                      'id' => $id_,
                                      'tx_code' => $txcode,
                                      'tx_date' => $tgl_tr,
                                      'tx_type_id' => 1,
                                      'coa_id' => '302',
                                      'tx_notes' => 'end of year'.'-'.date('Y'),
                                      'tx_amount' => $item->last_os,
                                      'acc_last' => 0,
                                      'acc_os' => 0,
                                      'tx_segment_id' => 0,
                                      'id_workflow' => 9,
                                      'user_crt_id' =>'999',
                                      'user_app_id' =>'999',
                                      'branch_id' => $value->id,
                                      'company_id' => Auth::user()->company_id,
                                      'created_at' => date('Y-m-d H:i:s'),
                                      'coa_type_id' => 3,
                                      'type_tx' => 'end_of_year'.'-'.date('Y')
                                  ]
                              );


                            }

                            //$this->jurnal_end_of_year($txcode);

                          }else{

                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => 0,
                                    'coa_id' => '302',
                                    'tx_notes' => 'end of year'.'-'.date('Y'),
                                    'tx_amount' => $item->last_os,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => 0,
                                    'id_workflow' => 9,
                                    'user_crt_id' =>'999',
                                    'user_app_id' =>'999',
                                    'branch_id' => $value->id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'coa_type_id' => 4,
                                    'type_tx' => 'end_of_year'.'-'.date('Y')
                                ]
                            );

                            $coa_trx = collect(\DB::select("select * from master_tx where id=".$id))->first();
                            if($coa_trx){

                                $id_='';
                                $id_urut = \DB::select("SELECT max(id) AS id from master_tx");
                                if($id_urut){
                                    $id_=$id_urut[0]->id + 1;
                                }else{
                                    $id_=1;
                                }


                                DB::table('master_tx')->insert(
                                  [
                                      'id' => $id_,
                                      'tx_code' => $txcode,
                                      'tx_date' => $tgl_tr,
                                      'tx_type_id' => 1,
                                      'coa_id' => $item->coa_no,
                                      'tx_notes' => 'end of year'.'-'.date('Y'),
                                      'tx_amount' => $item->last_os,
                                      'acc_last' => 0,
                                      'acc_os' => 0,
                                      'tx_segment_id' => 0,
                                      'id_workflow' => 9,
                                      'user_crt_id' =>'999',
                                      'user_app_id' =>'999',
                                      'branch_id' => $value->id,
                                      'company_id' => Auth::user()->company_id,
                                      'created_at' => date('Y-m-d H:i:s'),
                                      'coa_type_id' => 4,
                                      'type_tx' => 'end_of_year'.'-'.date('Y')
                                  ]
                              );


                            }






                          }

                          $this->jurnal_end_of_year($txcode);




                    }

                  }



              }

           }

          return json_encode(['rc'=>1,'msg'=>'End Of Year Process Finished']);


    }


    public function jurnal_end_of_year($txcode){
      $jurnal=\DB::select("select * from master_tx where tx_code='".$txcode."'");

                            if($jurnal){
                              foreach ($jurnal as $jurnals) {

                                  $coa1 = \DB::select("select last_os from master_coa where coa_no='".$jurnals->coa_id."' and company_id=".$jurnals->company_id." and branch_id=".$jurnals->branch_id);

                                  DB::table('master_tx')
                                  ->where('id', $jurnals->id)
                                  ->where('coa_id', $jurnals->coa_id)
                                  ->update(['acc_last' => $coa1[0]->last_os]);

                                  $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$jurnals->coa_type_id."' and tx_type_id=".$jurnals->tx_type_id);

                                  if($jurnals->coa_id=='302'){

                                        if($jurnals->tx_type_id==0){


                                            $acc_os=$coa1[0]->last_os - $jurnals->tx_amount;


                                        }else{

                                            if($ref_jurnal[0]->operator_sign=='-'){
                                                $acc_os=$coa1[0]->last_os - $jurnals->tx_amount;
                                            }else{
                                                $acc_os=$coa1[0]->last_os + $jurnals->tx_amount;
                                            }


                                        }

                                  }else{
                                    if($ref_jurnal[0]->operator_sign=='-'){
                                      $acc_os=$coa1[0]->last_os - $jurnals->tx_amount;
                                    }else{
                                        $acc_os=$coa1[0]->last_os + $jurnals->tx_amount;
                                    }
                                  }



                                  DB::table('master_tx')
                                  ->where('id', $jurnals->id)
                                  ->where('coa_id', $jurnals->coa_id)
                                  ->update(['acc_os' => $acc_os]);

                                  DB::table('master_coa')
                                  ->where('coa_no', $jurnals->coa_id)
                                  ->where('branch_id', $jurnals->branch_id)
                                  ->where('company_id', $jurnals->company_id)
                                  ->update(['last_os' => $acc_os]);


                              }

                            }
    }

    function changePassword(Request $request)
    {
        // $user = $this->getUserAdmin(Auth::user()->id);
        // $event = "Ubah Password User ".$user[0]->username;
        // $this->auditTrail($event,"User Admin");

        $data = User::find(Auth::user()->id);

        // $data = \DB::selectOne('SELECT * from master_user where id = ?',[Auth::user()->id]);


        if (Hash::check($request->input('oldPassword'), $data->password)) {
            $data->password = Hash::make($request->input('newPassword'));
            $data->save();
    
            return response()->json([
                'rc' => 1,
            ]);
        }
        else{
            return response()->json([
                'rc' => 99,
            ]);
        } 
      
    }


}
