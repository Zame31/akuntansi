<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;

class ApprovalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function list_app_baru(Request $request)
    {
        $results = \DB::select("select a.tx_code,a.tx_date,sum(a.tx_amount) as total,c.notes,b.fullname from
                            master_tx a
                            join master_user b on b.id=a.user_crt_id
                            join master_memo c on c.tx_code=a.tx_code
                            where a.id_workflow=2 and tx_type_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."
                            GROUP BY a.tx_code,a.tx_date,c.notes,b.fullname order by a.tx_date desc");
       $param['data']=$results;


            return view('master.master')->nest('child', 'approval.list',$param);


    }

    public function kirimAproval(Request $request){
        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_tx where id_workflow=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id."");

            $appAllowArr = array("t");
            $appAllowArrData = '';
            $appAllow = 'f';

            foreach ($results as $item) {

                $coa = \DB::select("select last_os,balance_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                $ref_jurnal = \DB::select("select operator_sign
                from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa[0]->last_os - $item->tx_amount;
                }else{
                    $acc_os=$coa[0]->last_os + $item->tx_amount;
                }

                 // POSITIF
                 if ($coa[0]->balance_type_id == 0) {
                    if ($acc_os >= 0) {
                        $appAllow = 't';
                    }else{
                        $appAllow = 'f';
                    }

                // NEGATIF
                }elseif($coa[0]->balance_type_id == 1){
                    if ($acc_os < 0) {
                        $appAllow = 't';
                    }else{
                        $appAllow = 'f';
                    }
                } else {
                        $appAllow = 't';
                }

                if ($appAllow == 'f') {
                    $appAllowArrData .= $item->coa_id.',';
                }

                array_push($appAllowArr, $appAllow);
            }


            if (in_array("f", $appAllowArr)) {

                return json_encode(['rc'=>99,'rm'=>'Approval tidak di izinkan, saldo '.$appAllowArrData.' abnormal']);

            }else{

                foreach ($results as $item) {

                    $coa = \DB::select("select last_os,balance_type_id from master_coa
                    where branch_id = ".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                    $ref_jurnal = \DB::select("select operator_sign
                    from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                    if($ref_jurnal[0]->operator_sign=='-'){
                        $acc_os=$coa[0]->last_os - $item->tx_amount;
                    }else{
                        $acc_os=$coa[0]->last_os + $item->tx_amount;
                    }

                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->update([
                        'id_workflow' => 9,
                        'seq_approval' => $set_last_seq_app]);

                    DB::table('master_memo')
                    ->where('tx_code', $item->tx_code)
                    ->update(['id_workflow' => 9]);

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('coa_id', $item->coa_id)
                    ->update(['acc_last' => $coa[0]->last_os]);

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('coa_id', $item->coa_id)
                    ->update(['acc_os' => $acc_os]);

                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', $item->coa_id)
                    ->update(['last_os' => $acc_os]);

                }

            }




            return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{

            foreach ($request->value as $item) {

                $results = \DB::select("select * from master_tx where tx_code='".$item."'");

                $appAllowArr = array("t");
                $appAllowArrData = '';
                $appAllow = 'f';

                foreach ($results as $item) {

                    $coa = \DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                    DB::table('master_tx')
                   ->where('id', $item->id)
                   ->where('coa_id', $item->coa_id)
                   ->update(['acc_last' => $coa[0]->last_os]);

                   $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                   if($ref_jurnal[0]->operator_sign=='-'){
                       $acc_os=$coa[0]->last_os - $item->tx_amount;
                   }else{
                       $acc_os=$coa[0]->last_os + $item->tx_amount;
                   }


                    // POSITIF
                    if ($coa[0]->balance_type_id == 0) {
                        if ($acc_os >= 0) {
                            $appAllow = 't';
                        }else{
                            $appAllow = 'f';
                        }

                    // NEGATIF
                    }elseif($coa[0]->balance_type_id == 1){
						if ($acc_os < 0) {
							$appAllow = 't';
						}else{
							$appAllow = 'f';
						}
					} else {
							$appAllow = 't';
                    }

                    if ($appAllow == 'f') {
                        $appAllowArrData .= $item->coa_id.',';
                    }

                    array_push($appAllowArr, $appAllow);
                }

                if (in_array("f", $appAllowArr)) {
                    return json_encode(['rc'=>99,'rm'=>'Approval tidak di izinkan, saldo '.$appAllowArrData.' tidak normal']);

                }else{




                    foreach ($results as $item) {

                        $coa = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                        $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                        $set_last_seq_app = $last_seq_app->max_id+1;

                        DB::table('master_tx')
                        ->where('tx_code', $item->tx_code)
                        ->update([
                            'id_workflow' => 9,'user_app_id'=>Auth::user()->id
                            ]);

                        DB::table('master_memo')
                        ->where('tx_code', $item->tx_code)
                        ->update(['id_workflow' => 9]);

                         DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->update(['acc_last' => $coa[0]->last_os,
                              'seq_approval' => $set_last_seq_app
                        ]);

                        $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                        if($ref_jurnal[0]->operator_sign=='-'){
                            $acc_os=$coa[0]->last_os - $item->tx_amount;
                        }else{
                            $acc_os=$coa[0]->last_os + $item->tx_amount;
                        }


                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->update(['acc_os' => $acc_os]);

                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', $item->coa_id)
                        ->update(['last_os' => $acc_os]);
                    }

                }





            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_tx 
            where id_workflow= (1,2) and branch_id=".Auth::user()->branch_id." 
            and company_id=".Auth::user()->company_id."");

            foreach ($results as $item) {

                DB::table('master_tx')
                ->where('tx_code', $item->tx_code)
                ->update(['id_workflow' => 0]);

                DB::table('master_memo')
                ->where('tx_code', $item->tx_code)
                ->update(['id_workflow' => 0]);

            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{

            foreach ($request->value as $item) {

                DB::table('master_tx')
                ->where('tx_code', $item)
                ->update(['id_workflow' => 0]);

                DB::table('master_memo')
                ->where('tx_code', $item)
                ->update(['id_workflow' => 0]);
            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

 public function detail_app(Request $request)
    {
        $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name from master_tx a
        left join master_coa b on a.coa_id=b.coa_no
        left join ref_segment c on c.id=a.tx_segment_id
        left join master_memo d on d.tx_code=a.tx_code
        left join master_user e on e.id=a.user_crt_id
        where a.tx_code='".$request->get('id')."' order by a.id asc");

        $param['data']=$data;


            return view('master.master')->nest('child', 'approval.detail',$param);


    }


}
