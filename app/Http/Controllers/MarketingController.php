<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\SalesScModel;
use App\Models\SalesModel;
use App\Models\SalesPolisModel;
use App\Models\SalesPolisScModel;

class MarketingController extends Controller
{
    public function create(Request $request,$type)
    {
        $param['ref_product'] = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $param['ref_bank'] = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $param['ref_quotation'] = \DB::select('SELECT * FROM ref_quotation where is_active=true  order by seq asc');
        $param['ref_proposal'] = \DB::select('SELECT * FROM ref_proposal where is_active=true  order by seq asc');
        $param['ref_cust'] = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $param['ref_agent'] = \DB::select('SELECT * FROM ref_agent order by seq asc');
        $param['ref_customer'] = \DB::select('SELECT * FROM master_customer where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');
        $param['ref_underwriter'] = \DB::select('SELECT * FROM ref_underwriter where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $param['ref_valuta'] = \DB::select('SELECT * FROM ref_valuta order by id asc');
        $param['data_comp'] = collect(\DB::select("SELECT * from master_company where id = 1"))->first();

        $param['master_cf_vehicle'] = \DB::select('SELECT * FROM master_cf_vehicle
        where is_active is true and id not in (select DISTINCT coc_id from master_sales_polis WHERE coc_id is not null)');

        if ( Auth::user()->user_role_id == 11 ) {
            // Role Client
            $param['polis_no'] = \DB::select("SELECT id,polis_no
                                                FROM master_sales_polis
                                                WHERE invoice_type_id = 0 AND
                                                      customer_id = ". Auth::user()->customer_id ." AND
                                                    (wf_status_id = 9 AND paid_status_id = 0) or
                                                    (wf_status_id = 13 AND paid_status_id = 1) or
                                                    (wf_status_id NOT IN (9, 16, 17, 18) AND paid_status_id = 1)
                                            ");
        } else {
            $param['polis_no'] = \DB::select("SELECT id,polis_no FROM master_sales_polis
                                                where invoice_type_id = 0 and
                                                (wf_status_id = 9 and paid_status_id = 0) or
                                                (wf_status_id = 13 and paid_status_id = 1) or
                                                (wf_status_id NOT IN (9, 16, 17, 18) and paid_status_id = 1)
                                            ");
        }


        $param['products'] = DB::TABLE('ref_product')->where('is_active', 't')->get();

        $getId = '';

        switch ($type) {
            case 'policy':
                $title = 'Form New Master Policy';
                $view = 'marketing.create.policy';
                $typeStore = 'new';
            break;

            case 'policy_endorsment':
                $title = 'Form New Master Policy Endorsment';
                $view = 'marketing.create.policy_endorsment';
                $typeStore = 'new';
            break;

            case 'edit_policy':
                $title = 'Edit Master Policy';
                $view = 'marketing.create.policy';
                $typeStore = 'edit';
                $getId = $request->get('id');
            break;

            case 'edit_policy_endorsment':
                $title = 'Edit Master Policy Endorsment';
                $view = 'marketing.create.policy_endorsment';
                $typeStore = 'edit';
                $getId = $request->get('id');
            break;

            // INSTALLMENT

            case 'policy_installment':
                $title = 'Form New Master Policy Installment';
                $view = 'marketing.create.policy_installment';
                $typeStore = 'new';
            break;

            case 'edit_policy_installment':
                $title = 'Edit Master Policy Installment';
                $view = 'marketing.create.policy_installment';
                $typeStore = 'edit';
                $getId = $request->get('id');
            break;

            case 'policy_installment_sc':
                $title = 'Form Master Policy Installment Schedule';
                $view = 'marketing.create.policy_installment_sc';
                $typeStore = 'new';
                $ms = collect(\DB::select("SELECT * FROM master_sales_polis where id = '".$request->get('id')."'"))->first();
                $mc = \DB::select("SELECT * FROM master_sales_schedule_polis where id_master_sales = ".$request->get('id')." order by outstanding desc");
                $mc_y_max = collect(\DB::select("SELECT max(year) as year_max FROM master_inventory_schedule where inventory_id = ".$request->get('id')))->first();
                $mc_y_min = collect(\DB::select("SELECT min(year) as year_min FROM master_inventory_schedule where inventory_id = ".$request->get('id')))->first();
                $param['premi_amount_config'] = collect(\DB::select("select value as jml from master_config where id = 13"))->first();
                $param['discount_amount_config'] = collect(\DB::select("select value as jml from master_config where id = 9"))->first();
                $param['fee_agent_config'] = collect(\DB::select("select value as jml from master_config where id = 2"))->first();
                $param['fee_internal_config'] = collect(\DB::select("select value  as jml from master_config where id = 3"))->first();
                $param['tax_amount_config'] = collect(\DB::select("select value  as jml from master_config where id = 8"))->first();

                $param['ms'] = $ms;
                $param['mc'] = $mc;
                $param['mc_y_max'] = $mc_y_max;
                $param['mc_y_min'] = $mc_y_min;
            break;


            case 'policy_installment_sc_detail':
                $title = 'Master Policy Installment Schedule Detail';
                $view = 'marketing.create.policy_installment_sc_detail';
                $typeStore = 'approval_policy_installment';
                $mi = collect(\DB::select("SELECT master_sales_polis.*,muc.fullname as membuat,mua.fullname as menyetujui
                FROM master_sales_polis
                left join master_user muc on muc.id = master_sales_polis.user_crt_id
                left join master_user mua on mua.id = master_sales_polis.user_upd_id
                where master_sales_polis.id = '".$request->get('id')."'"))->first();

                $mc = \DB::select("SELECT * FROM master_sales_schedule_polis
                where id_master_sales = ".$request->get('id')." order by outstanding desc");


                $param['mi'] = $mi;
                $param['mc'] = $mc;

            break;

            case 'policy_installment_sc_detail_success':
                $title = 'Master Policy Installment Schedule Detail';
                $view = 'marketing.create.policy_installment_sc_detail';
                $typeStore = 'success_policy_installment';
                $mi = collect(\DB::select("SELECT master_sales_polis.*,muc.fullname as membuat,mua.fullname as menyetujui
                FROM master_sales_polis
                left join master_user muc on muc.id = master_sales_polis.user_crt_id
                left join master_user mua on mua.id = master_sales_polis.user_upd_id
                where master_sales_polis.id = '".$request->get('id')."'"))->first();

                $mc = \DB::select("SELECT * FROM master_sales_schedule_polis
                where id_master_sales = ".$request->get('id')." order by outstanding desc");


                $param['mi'] = $mi;
                $param['mc'] = $mc;

            break;


        }

        $param['title'] = $title;
        $param['type'] = $type;
        $param['typeStore'] = $typeStore;
        $param['getId'] = $getId;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', $view, $param);
        }else {
            return view('master.master')->nest('child', $view, $param);
        }

    }

    public function list(Request $request,$type)
    {
        switch ($type) {

            // NEW
            case 'policy':
                $title = 'List New Master Policy';
                $view = 'marketing.list.list_policy';
                $typeList = 'new';
            break;

            case 'policy_endorsment':
                $title = 'List New Master Policy Endorsment';
                $view = 'marketing.list.list_policy_endorsment';
                $typeList = 'new';
            break;

            case 'policy_installment':
                $title = 'List New Master Policy Installment';
                $view = 'marketing.list.list_policy_installment';
                $typeList = 'new';
            break;


            // ACTIVE
            case 'success_policy':
                $title = 'List Master Policy Active';
                $view = 'marketing.list.list_policy';
                $typeList = 'success';
            break;

            case 'success_policy_installment':
                $title = 'List Master Policy Installment Active';
                $view = 'marketing.list.list_policy_installment';
                $typeList = 'success';
            break;

            // APPROVAL
            case 'approval_policy':
                $title = 'List Approval New Master Policy';
                $view = 'marketing.list.list_policy';
                $typeList = 'approve';
            break;

            case 'approval_policy_installment':
                $title = 'List Approval New Master Policy Installment';
                $view = 'marketing.list.list_policy_installment';
                $typeList = 'approve';
            break;



        }

        $param['title'] = $title;
        $param['type'] = $type;
        $param['typeList'] = $typeList;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', $view, $param);
        }else {
            return view('master.master')->nest('child', $view, $param);
        }

    }

    public function data(Request $request,$type,$id)
    {

        switch ($type) {


            case 'policy':
                $param = collect(\DB::select('SELECT mcv.*,mc.agent_id,mc.full_name,rp.type_id as type_product
                FROM master_cf_vehicle mcv
                join master_customer mc on mcv.customer_id = mc.id
				join ref_product rp on rp.id = mcv.product_id
                where mcv.id = ?',[$id]))->first();

                return response()->json([
                    'rc' => 0,
                    'rm' => $param
                ]);
            break;

            case 'list_template_excel':
                $column = [];
                $arr = [];
                $data = DB::TABLE('ref_insured_detail')
                            ->where('product_id', $id)
                            ->where('is_active', 't')
                            ->where('bit_id', '!=', 12)
                            ->orderBy('seq_list', 'ASC')
                            ->get();

                return response()->json([
                    'rc' => 1,
                    'rm' => 'Sukses',
                    'data' => $data
                ]);
            break;

            case 'master_sales_polis':
                if ($id == 'byget') {

                    $param = collect(\DB::select('SELECT mcv.*,mc.full_name
                    FROM master_sales_polis mcv
                    join master_customer mc on mcv.customer_id = mc.id
                    where mcv.polis_no = ?',[$request->get('polis_no')]))->first();

                    $this->setNewBitId($param->id);
                }else{
                    $param = collect(\DB::select('SELECT mcv.*,mc.full_name
                    FROM master_sales_polis mcv
                    join master_customer mc on mcv.customer_id = mc.id
                    where mcv.polis_no = ?',[$id]))->first();
                }


                return response()->json([
                    'rc' => 0,
                    'rm' => $param
                ]);
            break;

            case 'master_sales':
                $param = collect(\DB::select('SELECT mcv.*,mc.full_name
                    FROM master_sales mcv
                    join master_customer mc on mcv.customer_id = mc.id
                    where mcv.polis_no = ?',[$id]))->first();

                return response()->json([
                    'rc' => 0,
                    'rm' => $param
                ]);
            break;

            case 'master_sales_polis_byid':
                $param = collect(\DB::select('SELECT mcv.*,mc.full_name,mcc.cf_no, mcvv.id as cn_id,mqv.id as qs_id,rp.type_id as type_product
                FROM master_sales_polis mcv
                left join master_customer mc on mcv.customer_id = mc.id
                left join master_cf_vehicle mcc on mcc.id = mcv.coc_id
                left join master_cn_vehicle mcvv on mcvv.id = mcc.cn_id
                left join master_qs_vehicle mqv on mqv.id = mcvv.qs_id
                left join ref_product rp on rp.id = mcv.product_id
                where mcv.id = ?',[$id]))->first();

                return response()->json([
                    'rc' => 0,
                    'rm' => $param
                ]);
            break;

            case 'master_sales_polis_bycoc_id':
                $param = collect(\DB::select('SELECT mcv.*,mc.full_name, mcvv.id as cn_id,mqv.id as qs_id
                FROM master_sales_polis mcv
                left join master_customer mc on mcv.customer_id = mc.id
								left join master_cf_vehicle mcvvv on mcvvv.id = mcv.coc_id
								left join master_cn_vehicle mcvv on mcvv.id = mcvvv.cn_id
                left join master_qs_vehicle mqv on mqv.id = mcvv.qs_id
                where mcv.coc_id = ?',[$id]))->first();

                return response()->json([
                    'rc' => 0,
                    'rm' => $param
                ]);
            break;

            case 'detail_master_sales_polis':
                $param = collect(\DB::select('SELECT a.*,mcv.cf_no, b.full_name as customer,ru.definition as underwriter,mb.leader_name,mb.jabatan,b.full_name,b.address as c_address, c.definition as produk,d.full_name as agent,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create,rv.mata_uang,rv.deskripsi
                from master_sales_polis a
                left join master_customer b on b.id=a.customer_id
                left join ref_product c on c.id=a.product_id
                left join ref_agent d on d.id=a.agent_id
                left join ref_cust_segment g on g.id=a.segment_id
                left join ref_bank_account h on h.id=a.afi_acc_no
                left join ref_paid_status rw on rw.id = a.paid_status_id
                left join master_user mu on mu.id = a.user_approval_id
                left join master_user mus on mus.id = a.user_crt_id
                left join master_branch mb on mb.id = a.branch_id
                left join ref_underwriter ru on a.underwriter_id = ru.id
                left join ref_valuta rv on rv.id=a.valuta_id
                left join master_cf_vehicle mcv on mcv.id = a.coc_id where a.id= ?',[$id]))->first();


                return response()->json([
                    'rc' => 0,
                    'rm' => $param
                ]);
            break;

            case 'detail_master_sales_schedule_polis':
                $param = collect(\DB::select('SELECT a.*,ms.police_name,ms.start_date_polis,ms.end_date_polis,mcv.cf_no, b.full_name as customer,ms.kurs_today,mata_uang,deskripsi,ms.valuta_id,mb.leader_name,mb.jabatan,b.full_name,b.address as c_address,ru.definition as underwriter, c.definition as produk,d.full_name as agent,e.qs_no as quotation,f.qs_no as proposal,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create
                from master_sales_schedule_polis a
                left join master_sales_polis ms on ms.id = a.id_master_sales
                left join master_customer b on b.id=a.customer_id
                left join ref_product c on c.id=a.product_id
                left join ref_agent d on d.id=a.agent_id
                left join ref_quotation e on e.id=a.qs_no
                left join ref_proposal f on f.id=a.proposal_no
                left join ref_cust_segment g on g.id=a.segment_id
                left join ref_bank_account h on h.id=a.afi_acc_no
                left join ref_paid_status rw on rw.id = a.paid_status_id
                left join master_user mu on mu.id = a.user_approval_id
                left join master_user mus on mus.id = a.user_crt_id
                left join master_branch mb on mb.id = a.branch_id
                left join ref_valuta rv on rv.id = ms.valuta_id
                left join ref_underwriter ru on a.underwriter_id = ru.id
                left join master_cf_vehicle mcv on mcv.id = ms.coc_id  where a.id='.$id))->first();

                return response()->json([
                    'rc' => 0,
                    'rm' => $param
                ]);
            break;

            case 'list_policy_installment':
                switch ($id) {
                    case 'new':
                        $data = \DB::select("SELECT a.*,a.wf_status_id as stat, b.full_name,c.definition,
                        d.definition as paid,short_code,kurs_today,mata_uang,rv.id as id_uang
                        from master_sales_polis a
                        left join master_customer b on b.id=a.customer_id
                        left join ref_product c on c.id=a.product_id
                        left join ref_paid_status d on d.id=a.paid_status_id
                        left join master_branch mb on mb.id = a.branch_id
                        left join ref_valuta rv on rv.id = a.valuta_id where invoice_type_id = 2 and a.wf_status_id in (1) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

                    break;
                    case 'approve':
                        $data = \DB::select("SELECT a.*,a.wf_status_id as stat,b.full_name,c.definition,
                        d.definition as paid,short_code,kurs_today,mata_uang,rv.id as id_uang
                            from master_sales_polis a
                            left join master_customer b on b.id=a.customer_id
                            left join ref_product c on c.id=a.product_id
                            left join ref_paid_status d on d.id=a.paid_status_id
                            left join master_branch mb on mb.id = a.branch_id
                            left join ref_valuta rv on rv.id = a.valuta_id where invoice_type_id = 2 and a.wf_status_id in (2) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

                    break;
                    case 'success':
                        $data = \DB::select("SELECT a.*,a.wf_status_id as stat,b.full_name,c.definition,
                        d.definition as paid,short_code,kurs_today,mata_uang,rv.id as id_uang
                            from master_sales_polis a
                            left join master_customer b on b.id=a.customer_id
                            left join ref_product c on c.id=a.product_id
                            left join ref_paid_status d on d.id=a.paid_status_id
                            left join master_branch mb on mb.id = a.branch_id
                            left join ref_valuta rv on rv.id = a.valuta_id where invoice_type_id = 2 and a.wf_status_id in (9) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

                    break;
                }

                return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {

                    $get_memo = $this->getListMemo($data->id,'master_sales_polis');

                    switch ($id) {
                        case 'new':
                           $btn = '<a style="cursor:pointer;" class="dropdown-item" onclick="loadNewPageSPA(`'.route('marketing.create','edit_policy_installment').'?id='.$data->id.'`)" ><i class="la la-edit"></i>Ubah</a>
                                   <a style="cursor:pointer;" class="dropdown-item" onClick="loadNewPageSPA(`'.route('marketing.create','policy_installment_sc').'?id='.$data->id.'`)" ><i class="la la-clipboard"></i> Installment Schedule</a>
                           ';
                        break;
                        case 'approve':
                            $btn = '<a style="cursor:pointer;" class="dropdown-item" onclick="detail_invoice('.$data->id.',`installment`)" ><i
                            class="la la-clipboard"></i> Detail</a>
                            <a style="cursor:pointer;" class="dropdown-item" onClick="loadNewPageSPA(`'.route('marketing.create','policy_installment_sc_detail').'?id='.$data->id.'`)">
                                <i class="la la-clipboard"></i>
                                <span>Installment Schedule</span>
                            </a>
                            <a style="cursor:pointer;" class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                                <i class="la la-sticky-note"></i>
                                <span>Memo</span>
                            </a>';

                        break;
                        case 'success':
                        $btn = '<a style="cursor:pointer;" class="dropdown-item" onclick="detail_invoice('.$data->id.',`installment`)" ><i
                                class="la la-clipboard"></i> Detail</a>
                                <a style="cursor:pointer;" class="dropdown-item" onClick="loadNewPageSPA(`'.route('marketing.create','policy_installment_sc_detail_success').'?id='.$data->id.'`)">
                                        <i class="la la-clipboard"></i>
                                <span>Installment Schedule</span>
                            </a>
                            <a style="cursor:pointer;" class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                                <i class="la la-sticky-note"></i>
                                <span>Memo</span>
                            </a>';
                        break;

                    }

                    return '
                    <div class="dropdown dropdown-inline">
                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                           '.$btn.'
                        </div>
                    </div>
                ';


                })
                ->addColumn('cek', function ($data) {
                    return '
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                        <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                        <span></span>
                    </label>
                    ';
                    })
                ->editColumn('premi_amount',function($data) {
                    if ($data->valuta_id == 1) {
                        return number_format($data->premi_amount,2,',','.');
                    }else{
                        return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
                    }
                })
                ->editColumn('invoice_type_id',function($data) {
                    if($data->invoice_type_id==0){
                        return 'Original';
                    }
                    elseif($data->invoice_type_id==1){
                        return 'Endorsement';
                    }
                    else {
                        return 'Installment';
                    }
                })
                ->editColumn('stat',function($data) {
                    if($data->stat==1 || $data->stat==3){
                        return '<span class="btn btn-label-danger btn-sm zn-branch-label">Not Active</span>';
                    }
                    elseif($data->stat==2 ){
                        return '<span class="btn btn-label-warning btn-sm zn-branch-label">Waiting Approval</span>';
                    }
                    elseif($data->stat==9 ){
                        return '<span class="btn btn-label-success btn-sm zn-branch-label">Active</span>';
                    }
                })

                ->rawColumns(['cek', 'action','stat'])
                ->make(true);

            break;

            case 'search_master_sales_original_table' :

                if ($id == 'endorsment') {
                    $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                    d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name,mcv.cf_no
                        FROM master_sales_polis a
                        join master_customer mc on a.customer_id = mc.id
                        left join ref_product c on c.id=a.product_id
                        left join ref_agent d on d.id=mc.agent_id
                        left join ref_underwriter ru on a.underwriter_id = ru.id
                        left join ref_valuta rv on rv.id=a.valuta_id
                        left join master_cf_vehicle mcv on mcv.id = a.coc_id
                        where a.invoice_type_id = 1
                        and a.id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null) and
                        (a.wf_status_id = 9 and a.paid_status_id = 0) or
                        (a.wf_status_id = 13 and a.paid_status_id = 1) or
                        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');
                }else{
                    $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                    d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name,mcv.cf_no
                        FROM master_sales_polis a
                        join master_customer mc on a.customer_id = mc.id
                        left join ref_product c on c.id=a.product_id
                        left join ref_agent d on d.id=mc.agent_id
                        left join ref_underwriter ru on a.underwriter_id = ru.id
                        left join ref_valuta rv on rv.id=a.valuta_id
                        left join master_cf_vehicle mcv on mcv.id = a.coc_id
                        where a.invoice_type_id = 0
                        and a.id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null) and
                        (a.wf_status_id = 9 and a.paid_status_id = 0) or
                        (a.wf_status_id = 13 and a.paid_status_id = 1) or
                        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');
                }



                return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    $btn = '<button type="button" onclick="pilihCOC('.$data->id.')" class="btn btn-info btn-sm">Pilih</button>';
                    return $btn;

                })
                ->editColumn('ins_amount',function($data) {
                    return number_format($data->ins_amount,2,',','.');
                })
                ->editColumn('start_date_polis',function($data) {
                    return date('d M Y',strtotime($data->start_date_polis));
                })
                ->editColumn('end_date_polis',function($data) {
                    return date('d M Y',strtotime($data->end_date_polis));
                })
                ->rawColumns(['action'])
                ->make(true);
            break;

            case 'search_policy_table' :
                $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name
                    FROM master_cf_vehicle a
                    join master_customer mc on a.customer_id = mc.id
                    left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=mc.agent_id
                    left join ref_underwriter ru on a.underwriter_id = ru.id
                    left join ref_valuta rv on rv.id=a.valuta_id
					where a.is_active is true and a.id not in (select DISTINCT coc_id from master_sales_polis WHERE coc_id is not null)');

                return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {
                    $btn = '<button type="button" onclick="pilihCOC('.$data->id.')" class="btn btn-info btn-sm">Pilih</button>';
                    return $btn;

                })
                ->editColumn('total_sum_insured',function($data) {
                    return number_format($data->total_sum_insured,2,',','.');
                })
                ->editColumn('from_date',function($data) {
                    return date('d M Y',strtotime($data->from_date));
                })
                ->editColumn('to_date',function($data) {
                    return date('d M Y',strtotime($data->to_date));
                })
                ->rawColumns(['action'])
                ->make(true);
            break;

            case 'search_policy_number_table' :
                if ( Auth::user()->user_role_id == 11 ) {
                    // Role Client
                    $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                                                d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name
                                        FROM master_sales_polis a
                                        join master_customer mc on a.customer_id = mc.id
                                        left join ref_product c on c.id=a.product_id
                                        left join ref_agent d on d.id=mc.agent_id
                                        left join ref_underwriter ru on a.underwriter_id = ru.id
                                        left join ref_valuta rv on rv.id=a.valuta_id
                                        where a.invoice_type_id = 0 and
                                        a.customer_id = ' . Auth::user()->customer_id . ' and
                                        (a.wf_status_id = 9 and a.paid_status_id = 0) or
                                        (a.wf_status_id = 13 and a.paid_status_id = 1) or
                                        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');
                } else {
                    $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                                        d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name
                                            FROM master_sales_polis a
                                            join master_customer mc on a.customer_id = mc.id
                                            left join ref_product c on c.id=a.product_id
                                            left join ref_agent d on d.id=mc.agent_id
                                            left join ref_underwriter ru on a.underwriter_id = ru.id
                                            left join ref_valuta rv on rv.id=a.valuta_id
                                            where a.invoice_type_id = 0 and
                                            (a.wf_status_id = 9 and a.paid_status_id = 0) or
                                            (a.wf_status_id = 13 and a.paid_status_id = 1) or
                                            (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');
                }

                return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {
                    $btn = '<button type="button" onclick="pilihPolicyNumber(`'.$data->polis_no.'`)" class="btn btn-info btn-sm">Pilih</button>';
                    return $btn;

                })
                ->editColumn('ins_amount',function($data) {
                    return number_format($data->ins_amount,2,',','.');
                })
                ->editColumn('start_date_polis',function($data) {
                    return date('d M Y',strtotime($data->start_date_polis));
                })
                ->editColumn('end_date_polis',function($data) {
                    return date('d M Y',strtotime($data->end_date_polis));
                })
                ->rawColumns(['action'])
                ->make(true);
            break;

            case 'search_policy_number_ori_table' :
                $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name
                    FROM master_sales a
                    join master_customer mc on a.customer_id = mc.id
                    left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=mc.agent_id
                    left join ref_underwriter ru on a.underwriter_id = ru.id
                    left join ref_valuta rv on rv.id=a.valuta_id
                    where a.invoice_type_id = 0 and
                    (a.wf_status_id = 9 and a.paid_status_id = 0) or
                    (a.wf_status_id = 13 and a.paid_status_id = 1) or
                    (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');

                return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {
                    $btn = '<button type="button" onclick="pilihPolicyNumber(`'.$data->polis_no.'`)" class="btn btn-info btn-sm">Pilih</button>';
                    return $btn;

                })
                ->editColumn('ins_amount',function($data) {
                    return number_format($data->ins_amount,2,',','.');
                })
                ->editColumn('start_date_polis',function($data) {
                    return date('d M Y',strtotime($data->start_date_polis));
                })
                ->editColumn('end_date_polis',function($data) {
                    return date('d M Y',strtotime($data->end_date_polis));
                })
                ->rawColumns(['action'])
                ->make(true);
            break;

            case 'search_policy_endrosment_table' :
                $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name
                    FROM master_sales_polis a
                    join master_customer mc on a.customer_id = mc.id
                    left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=mc.agent_id
                    left join ref_underwriter ru on a.underwriter_id = ru.id
                    left join ref_valuta rv on rv.id=a.valuta_id
                    where a.invoice_type_id = 1 and
                    (a.wf_status_id = 9 and a.paid_status_id = 0) or
                    (a.wf_status_id = 13 and a.paid_status_id = 1) or
                    (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');

                return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {
                    $btn = '<button type="button" onclick="pilihPolicyNumber(`'.$data->polis_no.'`)" class="btn btn-info btn-sm">Pilih</button>';
                    return $btn;

                })
                ->editColumn('ins_amount',function($data) {
                    return number_format($data->ins_amount,2,',','.');
                })
                ->editColumn('start_date_polis',function($data) {
                    return date('d M Y',strtotime($data->start_date_polis));
                })
                ->editColumn('end_date_polis',function($data) {
                    return date('d M Y',strtotime($data->end_date_polis));
                })
                ->rawColumns(['action'])
                ->make(true);
            break;

            case 'search_master_sales_installment_table' :
                $data = \DB::select('SELECT a.*,ru.definition as underwriter, c.definition as produk,
                d.full_name as agent,rv.mata_uang,rv.deskripsi,mc.agent_id,mc.full_name,mcv.cf_no
                    FROM master_sales_polis a
                    join master_customer mc on a.customer_id = mc.id
                    left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=mc.agent_id
                    left join ref_underwriter ru on a.underwriter_id = ru.id
                    left join ref_valuta rv on rv.id=a.valuta_id
                    left join master_cf_vehicle mcv on mcv.id = a.coc_id
                    where a.invoice_type_id = 2
                    and a.coc_id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null and invoice_type_id = 2) and
                    (a.wf_status_id = 9 and a.paid_status_id = 0) or
                    (a.wf_status_id = 13 and a.paid_status_id = 1) or
                    (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');

                return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {
                    $btn = '<button type="button" onclick="pilihCOC('.$data->coc_id.')" class="btn btn-info btn-sm">Pilih</button>';
                    return $btn;

                })
                ->editColumn('ins_amount',function($data) {
                    return number_format($data->ins_amount,2,',','.');
                })
                ->editColumn('start_date_polis',function($data) {
                    return date('d M Y',strtotime($data->start_date_polis));
                })
                ->editColumn('end_date_polis',function($data) {
                    return date('d M Y',strtotime($data->end_date_polis));
                })
                ->rawColumns(['action'])
                ->make(true);
            break;

            case 'list_policy_customer':
                    $data = \DB::select("SELECT comp_fee_amount,agent_fee_amount,disc_amount,a.wf_status_id as stat,polis_amount,materai_amount,admin_amount,a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id
                    from master_sales_polis a
                    left join master_customer b on b.id=a.customer_id
                    left join ref_product c on c.id=a.product_id
                    left join ref_paid_status d on d.id=a.paid_status_id
                    left join master_branch e on e.id=a.branch_id
                    left join ref_valuta f on f.id=a.valuta_id where customer_id = ".$id." and invoice_type_id in (0,1) and a.wf_status_id=9 and paid_status_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

                    return DataTables::of($data)
                    ->addColumn('action', function ($data) use ($id) {

                        $get_memo = $this->getListMemo($data->id,'master_sales_polis');

                        $btn = '<a style="cursor:pointer;" class="dropdown-item" onclick="detail_invoice('.$data->id.','.$data->invoice_type_id.')"><i
                            class="la la-clipboard"></i> Detail</a>
                           ';

                        return '
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                            '.$btn.'
                            </div>
                        </div>
                    ';


                    })
                    ->addColumn('cek', function ($data) {
                        return '
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                            <span></span>
                        </label>
                        ';
                        })
                    ->editColumn('premi_amount',function($data) {
                        if ($data->valuta_id == 1) {
                            return number_format($data->premi_amount,2,',','.');
                        }else{
                            return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
                        }
                    })
                    ->editColumn('invoice_type_id',function($data) {
                        if($data->invoice_type_id==0){
                            return 'Original';
                        }
                        elseif($data->invoice_type_id==1){
                            return 'Endorsement';
                        }
                        else {
                            return 'Installment';
                        }
                    })
                    ->editColumn('stat',function($data) {
                        if($data->stat==1 || $data->stat==3){
                            return '<span class="btn btn-label-danger btn-sm zn-branch-label">Not Active</span>';
                        }
                        elseif($data->stat==2 ){
                            return '<span class="btn btn-label-warning btn-sm zn-branch-label">Waiting Approval</span>';
                        }
                        elseif($data->stat==9 ){
                            return '<span class="btn btn-label-success btn-sm zn-branch-label">Active</span>';
                        }
                    })

                    ->rawColumns(['cek', 'action','stat','url_dokumen'])
                    ->make(true);
            break;

            case 'list_policy':
                switch ($id) {
                    case 'new':
                        $data = \DB::select("SELECT c.type_id as type_product, a.id,b.full_name,c.definition,a.premi_amount,a.wf_status_id as stat,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id
                            from master_sales_polis a
                            left join master_customer b on b.id=a.customer_id
                            left join ref_product c on c.id=a.product_id
                            left join ref_paid_status d on d.id=a.paid_status_id
                            left join master_branch e on e.id=a.branch_id
                            left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1) and a.wf_status_id in (1,3) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." order by a.id desc");
                    break;
                    case 'approve':
                        $data = \DB::select("SELECT c.type_id as type_product, a.id,b.full_name,c.definition,a.premi_amount,a.wf_status_id as stat,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id
                        from master_sales_polis a
                        left join master_customer b on b.id=a.customer_id
                        left join ref_product c on c.id=a.product_id
                        left join ref_paid_status d on d.id=a.paid_status_id
                        left join master_branch e on e.id=a.branch_id
                        left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1) and a.wf_status_id=2 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

                    break;
                    case 'success':
                        if ( Auth::user()->user_role_id == 11 ) {
                            // Role Client
                            $data = \DB::select("SELECT c.type_id as type_product, comp_fee_amount,agent_fee_amount,disc_amount,a.wf_status_id as stat,polis_amount,materai_amount,admin_amount,a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id
                                                    from master_sales_polis a
                                                    left join master_customer b on b.id=a.customer_id
                                                    left join ref_product c on c.id=a.product_id
                                                    left join ref_paid_status d on d.id=a.paid_status_id
                                                    left join master_branch e on e.id=a.branch_id
                                                    left join ref_valuta f on f.id=a.valuta_id
                                                where invoice_type_id in (0,1) and
                                                        a.wf_status_id=9 and
                                                        paid_status_id=0 and
                                                        a.customer_id = " . Auth::user()->customer_id . " and
                                                        a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
                        } else {
                            $data = \DB::select("SELECT c.type_id as type_product, comp_fee_amount,agent_fee_amount,disc_amount,a.wf_status_id as stat,polis_amount,materai_amount,admin_amount,a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id
                                                    from master_sales_polis a
                                                    left join master_customer b on b.id=a.customer_id
                                                    left join ref_product c on c.id=a.product_id
                                                    left join ref_paid_status d on d.id=a.paid_status_id
                                                    left join master_branch e on e.id=a.branch_id
                                                    left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1) and a.wf_status_id=9 and paid_status_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
                        }

                    break;


                }

                return DataTables::of($data)
                ->addColumn('action', function ($data) use ($id) {

                    $get_memo = $this->getListMemo($data->id,'master_sales_polis');

                    if($data->type_product == 1 && $data->invoice_type_id==0){
                        $btn_realisasi = '<a style="cursor:pointer;" class="dropdown-item" onclick="realisasi_product('.$data->id.','.$data->invoice_type_id.')"><i
                        class="la la-clipboard"></i> Realisasi Produk</a>';
                    }else{
                        $btn_realisasi = '';
                    }

                   

                    switch ($id) {
                        case 'new':
                            $actEdit = ($data->invoice_type_id == 0) ? 'edit_policy':'edit_policy_endorsment';
                            $btn = '<a style="cursor:pointer;" class="dropdown-item" onclick="loadNewPageSPA(`'.route('marketing.create',$actEdit).'?id='.$data->id.'`)"><i class="la la-edit"></i>Ubah</a>';
                        break;
                        case 'approve':
                            $btn = '<a style="cursor:pointer;" class="dropdown-item" onclick="detail_invoice('.$data->id.','.$data->invoice_type_id.')"><i
                            class="la la-clipboard"></i> Detail</a>';
                         break;
                        case 'success':
                        $btn = '<a style="cursor:pointer;" class="dropdown-item" onclick="detail_invoice('.$data->id.','.$data->invoice_type_id.')"><i
                                    class="la la-clipboard"></i> Detail</a>
                                  
                                    <a style="cursor:pointer;" class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                                    <i class="la la-sticky-note"></i>
                                    <span>Memo</span>
                                </a>'.$btn_realisasi;
                        break;

                    }

                    return '
                    <div class="dropdown dropdown-inline">
                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                           '.$btn.'
                        </div>
                    </div>
                ';


                })
                ->addColumn('cek', function ($data) {
                    return '
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                        <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                        <span></span>
                    </label>
                    ';
                    })
                ->editColumn('premi_amount',function($data) {
                    if ($data->valuta_id == 1) {
                        return number_format($data->premi_amount,2,',','.');
                    }else{
                        return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
                    }
                })
                ->editColumn('invoice_type_id',function($data) {
                    if($data->invoice_type_id==0){
                        return 'Original';
                    }
                    elseif($data->invoice_type_id==1){
                        return 'Endorsement';
                    }
                    else {
                        return 'Installment';
                    }
                })
                ->editColumn('stat',function($data) {
                    if($data->stat==1 || $data->stat==3){
                        return '<span class="btn btn-label-danger btn-sm zn-branch-label">Not Active</span>';
                    }
                    elseif($data->stat==2 ){
                        return '<span class="btn btn-label-warning btn-sm zn-branch-label">Waiting Approval</span>';
                    }
                    elseif($data->stat==9 ){
                        return '<span class="btn btn-label-success btn-sm zn-branch-label">Active</span>';
                    }
                })

                ->rawColumns(['cek', 'action','stat','url_dokumen'])
                ->make(true);

                //END LIST
            break;

            case 'detail_product':
                $detailSort = DB::TABLE('master_qs_detail')
                ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                ->leftJoin('ref_insured_detail', function($join) {
                    $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                    $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                })
                ->where('master_qs_detail.doc_id', $id)
                ->where('master_qs_detail.doc_off_type', 3)
                ->where('master_qs_detail.bit_id', '!=', 12)
                ->where('ref_insured_detail.is_active', 't')
                ->orderBy('bit_id', 'ASC');

                $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                    ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id)
                    ->where('master_qs_detail.doc_off_type', 3)
                    ->where('master_qs_detail.bit_id', 12)
                    ->where('ref_insured_detail.is_active', 't')
                    ->unionAll($detailSort)
                    ->get();


                $detail = array_values($detail->groupBy('value_int')->toArray());


                return response()->json([
                    'rc' => 0,
                    'rm' => $detail
                ]);

            break;

            case 'detail_product_list':
                
                $this->setNewBitId($id);

                $detailSort = DB::TABLE('master_qs_detail')
                ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                ->leftJoin('master_sales_polis', 'master_sales_polis.id', '=', 'master_qs_detail.doc_id')
                ->leftJoin('ref_insured_detail', function($join) {
                    $join->on('ref_insured_detail.product_id', '=', 'master_sales_polis.product_id');
                    $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                })
                ->where('master_qs_detail.doc_id', $id)
                ->where('master_qs_detail.doc_off_type', 3)
                ->where('master_qs_detail.bit_id', '!=', 12)
                ->where('ref_insured_detail.is_active', 't')
                ->orderBy('bit_id', 'ASC');

                $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                    ->leftJoin('master_sales_polis', 'master_sales_polis.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_sales_polis.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id)
                    ->where('master_qs_detail.doc_off_type', 3)
                    ->where('master_qs_detail.bit_id', 12)
                    ->where('ref_insured_detail.is_active', 't')
                    ->unionAll($detailSort)
                    ->get();


                $detail = array_values($detail->groupBy('value_int')->toArray());


                return response()->json([
                    'rc' => 0,
                    'rm' => $detail
                ]);

            break;

            case 'detail_realisasi':
                $detailSort = DB::TABLE('master_qs_detail')
                ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                ->leftJoin('master_sales_polis', 'master_sales_polis.id', '=', 'master_qs_detail.doc_id')
                ->leftJoin('ref_insured_detail', function($join) {
                    $join->on('ref_insured_detail.product_id', '=', 'master_sales_polis.product_id');
                    $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                })
                ->where('master_qs_detail.doc_id', $id)
                ->where('master_qs_detail.doc_off_type', 3)
                ->where('master_qs_detail.bit_id', '!=', 12)
                ->where('ref_insured_detail.is_active', 't')
                ->orderBy('bit_id', 'ASC');

                $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                    ->leftJoin('master_sales_polis', 'master_sales_polis.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_sales_polis.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id)
                    ->where('master_qs_detail.doc_off_type', 3)
                    ->where('master_qs_detail.bit_id', 12)
                    ->where('ref_insured_detail.is_active', 't')
                    ->unionAll($detailSort)
                    ->get();


                $detail = array_values($detail->groupBy('value_int')->toArray());


                return response()->json([
                    'rc' => 0,
                    'rm' => $detail
                ]);

            break;

            case 'detail_product_cf':

                $detailSort = DB::TABLE('master_qs_detail')
                ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                ->leftJoin('ref_insured_detail', function($join) {
                    $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                    $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                })
                ->where('master_qs_detail.doc_id', $id)
                ->where('master_qs_detail.doc_off_type', 2)
                ->where('master_qs_detail.bit_id', '!=', 12)
                ->where('ref_insured_detail.is_active', 't')
                ->orderBy('bit_id', 'ASC');

                $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                    ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id)
                    ->where('master_qs_detail.doc_off_type', 2)
                    ->where('master_qs_detail.bit_id', 12)
                    ->where('ref_insured_detail.is_active', 't')
                    ->unionAll($detailSort)
                    ->get();


                $detail = array_values($detail->groupBy('value_int')->toArray());

                $head =  \DB::select("SELECT * from ref_insured_detail
                where product_id = ? and is_active = 't' and bit_id <> 12 order by bit_id asc", [$id]);

                return response()->json([
                    'rc' => 0,
                    'rm' => $detail
                ]);

            break;

            case 'detail_product_no_polis':

                $getPolis = collect(\DB::select('SELECT mcv.*,mc.full_name
                FROM master_sales_polis mcv
                join master_customer mc on mcv.customer_id = mc.id
                where mcv.polis_no = ?',[$request->get('polis_no')]))->first();

                $detailSort = DB::TABLE('master_qs_detail')
                ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                ->leftJoin('master_sales_polis', 'master_sales_polis.id', '=', 'master_qs_detail.doc_id')
                ->leftJoin('ref_insured_detail', function($join) {
                    $join->on('ref_insured_detail.product_id', '=', 'master_sales_polis.product_id');
                    $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                })
                ->where('master_qs_detail.doc_id', $getPolis->id)
                ->where('master_qs_detail.doc_off_type', 3)
                ->where('master_qs_detail.bit_id', '!=', 12)
                ->where('ref_insured_detail.is_active', 't')
                ->orderBy('bit_id', 'ASC');
                

                $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.is_currency')
                    ->leftJoin('master_sales_polis', 'master_sales_polis.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_sales_polis.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $getPolis->id)
                    ->where('master_qs_detail.doc_off_type', 3)
                    ->where('master_qs_detail.bit_id', 12)
                    ->where('ref_insured_detail.is_active', 't')
                    ->unionAll($detailSort)
                    ->get();


                $detail = array_values($detail->groupBy('value_int')->toArray());


                return response()->json([
                    'rc' => 0,
                    'rm' => $detail
                ]);

            break;


        }


    }

    public function store(Request $request,$type)
    {

        // dd($request->input('nett_amount'));
        // $this->clearSeparatorDouble($request->input('nett_amount'))*$nilai12

        if ($type == 'policy' || $type == 'policy_endorsment' || $type == 'policy_installment') {
            $detail = $request->input('detail');

            $systemDate = collect(\DB::select("select * from ref_system_date"))->first();
            $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();
            $ef_config = \DB::select("select value  as jml from master_config where id = 10");
            $ef_agent_config = \DB::select("select value  as jml from master_config where id = 11");
            $ef_company_config = \DB::select("select value  as jml from master_config where id = 12");

            $today = date('Y-m-d');

            $results = \DB::select("SELECT max(id) AS id from master_sales_polis");
            $id='';
            if($results){
                $id=$results[0]->id + 1;
            }else{
                $id=1;
            }

            $jml=str_replace('.', '', $request->input('premi'));
            $ef=$ef_config[0]->jml * str_replace(',', '.', $jml) / 100;
            $ef_agent=$ef_agent_config[0]->jml * $ef / 100;
            $ef_company=$ef_company_config[0]->jml * $ef / 100;

            $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();
            $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();

            $str=strlen($ref_invoice->next_no);

            if($str==3){
                $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }elseif($str==2){
                $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }else{
                $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }

            $startDatePolice = date('Y-m-d',strtotime($request->input('start_date_police')));
            $endDatePolice = date('Y-m-d',strtotime($request->input('end_date_police')));
            $valutaID = $request->input('valuta_id');
            $nilai = $this->clearSeparatorDouble($request->input('insurance'));

            if ( is_null($request->input('valas_amount')) ) {
                $valasAmount = 0;
            } else {
                $valasAmount = $this->clearSeparatorDouble($request->input('valas_amount'));
            }

            if ( is_null($request->input('today_kurs')) ) {
                $nilai12  = 1;
            } else {
                $nilai12 = $this->clearSeparatorDouble($request->input('today_kurs'));
            }

            $get_id = $request->input('get_id');
            $insuredName = $request->input('insured_name');

            // ENDORS
            $noEndors = DB::table('master_sales_polis')->max('endors_no');

            if ( empty($noEndors) ) {
                $noEndors = 1;
            } else {
                $noEndors += 1;
            }

            // Sequence endors
            $seqEndors = DB::table('master_sales_polis')->max('endors_seq');

            if ( empty($seqEndors) ) {
                $seqEndors = 1;
            } else {
                $seqEndors += 1;
            }
            // ENDORS

            if ( is_null($request->input('ef_pct')) ) {
                $ef_pct = 0;
            } else {
                $ef_pct = $this->clearSeparatorDouble($request->input('ef_pct'));
            }

            if ( is_null($request->input('ef')) ) {
                $ef = 0;
            } else {
                $ef = $this->clearSeparatorDouble($request->input('ef'));
            }

            if ( is_null($request->input('nett_amount')) ) {
                $nettPremium = 0;
            } else {
                $nettPremium = $this->clearSeparatorDouble($request->input('nett_amount'));
            }

            if ( is_null($request->input('disc_amount')) ) {
                $discAmount = 0;
            } else {
                $discAmount = $this->clearSeparatorDouble($request->input('disc_amount'));
            }

            if ( is_null($request->input('premi')) ) {
                $premi = 0;
            } else {
                $premi = $this->clearSeparatorDouble($request->input('premi'));
            }

            $setField = [
                'inv_date' => $systemDate->current_date,
                'polis_no' => $request->input('no_polis'),
                'police_name' => $request->input('policy_cust_name'),
                'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
                'product_id' => $request->input('product'),
                'agent_id' => $request->input('officer'),
                'paid_status_id' => 0,
                'customer_id' => $request->input('customer'),
                'underwriter_id' => $request->input('id_underwriter'),
                'is_active' => 1,
                'wf_status_id' => 1,
                'user_crt_id' => Auth::user()->id,
                'branch_id' => Auth::user()->branch_id,
                'company_id' => Auth::user()->company_id,
                'created_at' => date('Y-m-d H:i:s'),
                'ef' => $ef,
                'ef_agent' => $ef_agent,
                'ef_company' => $ef_company,
                'start_date_polis' => $startDatePolice,
                'end_date_polis' => $endDatePolice,
                'valuta_id' => $valutaID,
                'no_of_insured' => $request->input('no_of_insured'),
                'valuta_amount' => $valasAmount*$nilai12,
                'ins_amount' => $nilai*$nilai12,
                'disc_amount' => $discAmount*$nilai12,
                'net_amount' => $nettPremium*$nilai12,
                'premi_amount' => $premi*$nilai12,
                'kurs_today' =>  $nilai12,
            ];


        }

        switch ($type) {
            // invoice_type_id 0 = BIASA
            // invoice_type_id 1 = ENDORSMENT
            // invoice_type_id 2 = INSTALMENT
            case 'policy':

                if($systemDate->current_date == $today){

                    $setField['invoice_type_id'] = 0;
                    $setField['coc_id'] = $request->input('coc_no');
                    $setField['agent_fee_amount'] = $this->clearSeparatorDouble($request->input('fee_agent'))*$nilai12;
                    $setField['comp_fee_amount'] = $this->clearSeparatorDouble($request->input('fee_internal'))*$nilai12;
                    $setField['polis_amount'] = $this->clearSeparatorDouble($request->input('fee_polis'))*$nilai12;
                    $setField['materai_amount'] = $this->clearSeparatorDouble($request->input('fee_materai'))*$nilai12;
                    $setField['admin_amount'] = $this->clearSeparatorDouble($request->input('fee_admin'))*$nilai12;
                    $setField['tax_amount'] = $this->clearSeparatorDouble($request->input('tax_amount'))*$nilai12;
                    $setField['ins_fee'] = $this->clearSeparatorDouble($request->input('asuransi'))*$nilai12;
                    $setField['realisasi_sum_insured'] = $this->clearSeparatorDouble($request->input('realisasi_sum_insured'))*$nilai12;
                    $setField['realisasi_premium'] = $this->clearSeparatorDouble($request->input('realisasi_premium'))*$nilai12;
                    $setField['ef_pct'] = $ef_pct*$nilai12;
                    $setField['ef'] = $ef*$nilai12;

                    DB::table('ref_invoice_no')
                        ->where('company_id', Auth::user()->company_id)
                        ->where('year', date('Y'))
                        ->where('type',0)
                        ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

                    //IF EDIT DATA
                    if ($get_id) {

                        DB::table('master_sales_polis')->where('id', $get_id)->update($setField);
                        DB::table('master_qs_detail')->where('doc_id', (int)$get_id)->where('doc_off_type', 3)->delete();

                        $this->storeDetailProduct($detail,$get_id);
                    //IF ADD DATA
                    }else {

                        $seq = DB::table('master_sales_polis')->max('id');
                        $setField['id'] = $seq+1;
                        DB::table('master_sales_polis')->insert($setField);
                        $this->storeDetailProduct($detail,$setField['id']);
                    }

                    return json_encode(['rc'=>1,'rm'=>'Success']);
                }else{
                    return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
                }

            break;

            case 'policy_endorsment':
                if($systemDate->current_date == $today){

                    if ( is_null($request->input('asuransi')) ) {
                        $asuransi = 0;
                    } else {
                        $asuransi = $this->clearSeparatorDouble($request->input('asuransi'));
                    }
                    $setField['invoice_type_id'] = 1;
                    $setField['agent_fee_amount'] = $this->clearSeparatorDouble($request->input('fee_agent'))*$nilai12;
                    $setField['comp_fee_amount'] = $this->clearSeparatorDouble($request->input('fee_internal'))*$nilai12;
                    $setField['polis_amount'] = $this->clearSeparatorDouble($request->input('fee_polis'))*$nilai12;
                    $setField['materai_amount'] = $this->clearSeparatorDouble($request->input('fee_materai'))*$nilai12;
                    $setField['admin_amount'] = $this->clearSeparatorDouble($request->input('fee_admin'))*$nilai12;
                    $setField['tax_amount'] = $this->clearSeparatorDouble($request->input('tax_amount'))*$nilai12;
                    $setField['ins_fee'] = $asuransi*$nilai12;
                    $setField['endors_no'] = $noEndors;
                    $setField['insured_name'] = $insuredName;
                    $setField['endors_seq'] = $seqEndors;
                    $setField['endors_reason'] = $request->input('endorse_reason');
                    $setField['ef_pct'] = $this->clearSeparatorDouble($request->input('ef_pct'))*$nilai12;
                    $setField['ef_pct'] = $ef_pct*$nilai12;
                    $setField['coc_id'] = $request->input('coc_id');
                    $setField['realisasi_sum_insured'] = $this->clearSeparatorDouble($request->input('realisasi_sum_insured'))*$nilai12;
                    $setField['realisasi_premium'] = $this->clearSeparatorDouble($request->input('realisasi_premium'))*$nilai12;
                

                    DB::table('ref_invoice_no')
                        ->where('company_id', Auth::user()->company_id)
                        ->where('year', date('Y'))
                        ->where('type',0)
                        ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

                    //IF EDIT DATA
                    if ($get_id) {
                        DB::table('master_sales_polis')->where('id', $get_id)->update($setField);
                        DB::table('master_qs_detail')->where('doc_id', (int)$get_id)->where('doc_off_type', 3)->delete();

                        $this->storeDetailProduct($detail,$get_id);
                    //IF ADD DATA
                    }else {
                        $seq = DB::table('master_sales_polis')->max('id');
                        $setField['id'] = $seq+1;
                        DB::table('master_sales_polis')->insert($setField);
                        $this->storeDetailProduct($detail,$setField['id']);
                    }

                    return json_encode(['rc'=>1,'rm'=>'Success']);
                }else{
                    return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
                }

            break;

            case 'policy_installment':
                if($systemDate->current_date == $today){
                    $setField['invoice_type_id'] = 2;
                    $setField['coc_id'] = $request->input('coc_no');

                    DB::table('ref_invoice_no')
                        ->where('company_id', Auth::user()->company_id)
                        ->where('year', date('Y'))
                        ->where('type',0)
                        ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

                    //IF EDIT DATA
                    if ($get_id) {
                        DB::table('master_sales_polis')->where('id', $get_id)->update($setField);
                        DB::table('master_qs_detail')->where('doc_id', (int)$get_id)->where('doc_off_type', 3)->delete();

                        $this->storeDetailProduct($detail,$get_id);
                    //IF ADD DATA
                    }else {
                        $seq = DB::table('master_sales_polis')->max('id');
                        $setField['id'] = $seq+1;
                        DB::table('master_sales_polis')->insert($setField);
                        $this->storeDetailProduct($detail,$setField['id']);
                    }



                    return json_encode(['rc'=>1,'rm'=>'Success']);
                }else{
                    return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
                }

            break;

            case 'policy_installment_sc':

                try{

                    SalesPolisScModel::where('id_master_sales', $request->input('get_id'))->delete();
                    $get_ms = collect(\DB::select("SELECT * FROM master_sales_polis where id =".$request->input('get_id')))->first();
                    $outstanding = $request->input('outstanding');
                    $amor = $request->input('amor');
                    $dateamor = $request->input('dateamor');
                    $dateamordue = $request->input('dateamordue');
                    $days_overdue = $request->input('days_overdue');
                    $tenor = $request->input('tenor');
                    $comm_due = $request->input('comm_due');
                    $fee_agent = $request->input('fee_agent');
                    $agent_tax = $request->input('agent_tax');
                    $underwriter = $request->input('underwriter');
                    $n_premi = $request->input('n_premi');

                    for ($i=0; $i < $tenor; $i++) {
                        $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_sales_schedule_polis"))->first();

                        $dsc = new SalesPolisScModel();
                        $dsc->id = $get->max_id+1;
                        $dsc->id_master_sales = $request->input('get_id');
                        $dsc->interval = $request->input('interval');
                        $dsc->tenor = $tenor;
                        $dsc->days_overdue = $days_overdue;

                        $dsc->premi_amount = $this->clearSeparatorDouble($n_premi[$i]);
                        $dsc->comp_fee_amount = $this->clearSeparatorDouble($comm_due[$i]);
                        $dsc->agent_fee_amount = $this->clearSeparatorDouble($fee_agent[$i]);
                        $dsc->tax_amount = $this->clearSeparatorDouble($agent_tax[$i]);
                        $dsc->underwriter_amount = $this->clearSeparatorDouble($underwriter[$i]);
                        $dsc->net_amount = $this->clearSeparatorDouble($amor[$i]);
                        $dsc->outstanding = $this->clearSeparatorDouble($outstanding[$i]);
                        $dsc->date = $this->convertDate($dateamor[$i]);
                        $dsc->due_date_jrn = $this->convertDate($dateamordue[$i]);
                        $discount_amount_config = collect(\DB::select("select value as jml from master_config where id = 9"))->first();
                        $dsc->disc_amount = ($discount_amount_config->jml * $dsc->premi_amount) / 100;
                        $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

                        // INVOICE NO
                        $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();
                        $str=strlen($ref_invoice->next_no);
                        if($str==3){
                            $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                        }elseif($str==2){
                            $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                        }else{
                            $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                        }

                        DB::table('ref_invoice_no')
                        ->where('company_id', Auth::user()->company_id)
                        ->where('year', date('Y'))
                        ->where('type',0)
                        ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

                        if ($i == 0) {
                            $dsc->polis_amount = $get_ms->polis_amount;
                            $dsc->materai_amount = $get_ms->materai_amount;
                            $dsc->admin_amount = $get_ms->admin_amount;
                        }else {
                            $dsc->polis_amount = 0;
                            $dsc->materai_amount = 0;
                            $dsc->admin_amount = 0;
                        }


                        $dsc->inv_no = $no_invoice;
                        $dsc->inv_date = $this->convertDate($dateamor[$i]);
                        $dsc->afi_acc_no = $get_ms->afi_acc_no;
                        // $dsc->qs_no = $get_ms->qs_no;
                        // $dsc->qs_date = $get_ms->qs_date;
                        // $dsc->proposal_no = $get_ms->proposal_no;
                        // $dsc->proposal_date = $get_ms->proposal_date;
                        $dsc->is_active = $get_ms->is_active;
                        $dsc->branch_id = $get_ms->branch_id;
                        $dsc->company_id = $get_ms->company_id;
                        $dsc->notes = $get_ms->notes;

                        $dsc->is_tax_company = $get_ms->is_tax_company;
                        $dsc->ins_fee = $get_ms->ins_fee;
                        $dsc->ef = $get_ms->ef;
                        $dsc->ef_agent = $get_ms->ef_agent;
                        $dsc->ef_company = $get_ms->ef_company;
                        $dsc->polis_no = $get_ms->polis_no;
                        $dsc->underwriter_id = $get_ms->underwriter_id;
                        $dsc->invoice_type_id = $get_ms->invoice_type_id;
                        $dsc->no_of_insured = $get_ms->no_of_insured;
                        $dsc->url_dokumen = $get_ms->url_dokumen;
                        $dsc->customer_id = $get_ms->customer_id;
                        $dsc->product_id = $get_ms->product_id;
                        $dsc->agent_id = $get_ms->agent_id;
                        $dsc->ins_amount =  $get_ms->ins_amount;

                        $dsc->paid_status_id = 0;
                        $dsc->user_crt_id = Auth::user()->id;
                        $dsc->user_upd_id = Auth::user()->id;
                        $dsc->id_workflow = 1;
                        $dsc->save();
                    }

                    return response()->json([
                        'rc' => 0,
                        'rm' => "sukses"
                    ]);
                }
                catch (QueryException $e){

                    if($e->getCode() == '23505'){
                        $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
                    }else{
                        $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
                    }
                    return response()->json([
                        'rc' => 99,
                        'rm' => $response,
                        'msg' => $e->getMessage()
                    ]);
                }
            break;

            case 'list_template':
                $list = $request->input('list_template_excel');
                $col = $request->input('col_template_excel');


                foreach ($list as $key => $v) {
                    DB::TABLE('ref_insured_detail')
                    ->where('product_id', $request->input('product'))
                    ->where('input_name', $v)
                    ->update([
                        'seq_list' => $key,
                        'col_list' => $col[$key],
                    ]);
                }
                return json_encode(['rc'=>1,'rm'=>'Success']);
                // dd($request->input('col_template_excel'));

                // dd($request->input('list_template_excel'));
            break;

            case 'tambah_realisasi':

                $data_detail = $request->input('datas');
                 // STORE DETAIL
                $detail = json_decode($data_detail);

                //  dd($detail);
        
                if ( count($detail) > 0 ) {
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');
                    
                    $max_value_int = collect(\DB::select("SELECT max(value_int) 
                    from master_qs_detail 
                    where doc_id = ".$request->input('doc_id')." and doc_off_type = 3"))->first();

                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    if ( is_null($max_value_int->max) ) {
                        $max_value_int->max = 1;
                    } else {
                        $max_value_int->max += 1;
                    }

                    $get_realisasi_sum_insured = 0;
                    $get_realisasi_premium = 0;
        
                    foreach ( $detail as $data ) {
                        
                        if($data->bit_id == "80"){
                            $get_realisasi_sum_insured = $this->clearSeparatorDouble($data->value);
                        }

                        if($data->bit_id == "81"){
                            $get_realisasi_premium = $this->clearSeparatorDouble($data->value);
                        }

                            DB::TABLE('master_qs_detail')
                                ->insert([
                                    'id' => $id_qs_detail,
                                    'doc_id' => $request->input('doc_id'),
                                    'bit_id' => $data->bit_id,
                                    'value_text' => $data->value,
                                    'value_int' => $max_value_int->max,
                                    'doc_off_type' => 3,
                                ]);
                            $id_qs_detail++;
                    }
                }

                // UPDATE MASTER POLICY
                $master_policy = collect(\DB::select("SELECT realisasi_sum_insured,realisasi_premium 
                    from master_sales_polis 
                    where id = ".$request->input('doc_id')))->first();

                DB::table('master_sales_polis')
                ->where('id', $request->input('doc_id'))
                ->update([
                    'realisasi_sum_insured' =>$master_policy->realisasi_sum_insured + $get_realisasi_sum_insured,
                    'realisasi_premium'=>$master_policy->realisasi_premium + $get_realisasi_premium
                ]);

                $update_master_policy = collect(\DB::select("SELECT * 
                from master_sales_polis 
                where id = ".$request->input('doc_id')))->first();


                // STORE DETAIL
                return json_encode([
                    'rc'=>1,
                    'rm'=>'Success',
                    'realisasi_sum_insured'=> $master_policy->realisasi_sum_insured + $get_realisasi_sum_insured,
                    'realisasi_premium'=> $master_policy->realisasi_premium + $get_realisasi_premium,
                    'selisih_realisasi_sum_insured'=> $update_master_policy->ins_amount - $update_master_policy->realisasi_sum_insured,
                    'selisih_realisasi_premium'=> $update_master_policy->premi_amount - $update_master_policy->realisasi_premium
                    ]);
            break;

        }

    }

    public function storeDetailProduct($data_detail,$setId)
    {
         // STORE DETAIL
         $detail = json_decode($data_detail);

        //  dd($detail);

         if ( count($detail) > 0 ) {
             $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');
             if ( is_null($id_qs_detail) ) {
                 $id_qs_detail = 1;
             } else {
                 $id_qs_detail += 1;
             }

             $row = 1;
             foreach ( $detail as $item ) {
                 foreach ( $item->value as $data ) {
                     // INSERT MASTER QS DETAIL
                     DB::TABLE('master_qs_detail')
                         ->insert([
                             'id' => $id_qs_detail,
                             'doc_id' => $setId,
                             'bit_id' => $data->bit_id,
                             'value_text' => $data->value,
                             'value_int' => $row,
                             'doc_off_type' => 3,
                         ]);
                     $id_qs_detail++;
                 }
                 $row++;
             }
         }
         // STORE DETAIL
    }

    public function approval(Request $request,$type)
    {
        $datas = [];

        switch ($type) {
            case 'send':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(1,3) and invoice_type_id in(0,1) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $this->znSendApproval('master_sales_polis',$datas,'');

            break;

            case 'delete':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(1,3) and invoice_type_id in(0,1) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $this->znDeleteApproval('master_sales_polis',$datas);

            break;

            case 'give':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(2) and invoice_type_id in(0,1) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $this->znGiveApproval('master_sales_polis',$datas,$request->SetMemo);
            break;

            case 'reject':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(2) and invoice_type_id in(0,1) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $this->znRejectApproval('master_sales_polis',$datas,$request->SetMemo);
            break;

            case 'installment_send':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(1,3) and invoice_type_id in(2) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $notAllow = '';
                $notAllowCount = 0;

                foreach ($datas as $v) {
                    if ($v != null) {
                        $checkSc = \DB::select("select * from master_sales_schedule_polis where id_master_sales = ".$v);

                        if (count($checkSc) == 0) {
                            $notAllow .= $v.",";
                            $notAllowCount ++;
                        }
                    }

                }

                if ($notAllowCount > 0) {
                    return response()->json([
                        'rc' => 88,
                        'rm' => $notAllowCount." Data Installment Dengan ID ".$notAllow." Belum Memiliki Installment Schedule"
                    ]);
                }else {
                    $this->znSendApproval('master_sales_polis',$datas,'');
                }



            break;

            case 'installment_delete':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(1,3) and invoice_type_id in(2) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $this->znDeleteApproval('master_sales_polis',$datas);

            break;

            case 'installment_give':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(2) and invoice_type_id in(2) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $this->znGiveApproval('master_sales_polis',$datas,$request->SetMemo);
            break;

            case 'installment_reject':
                if ($request->get('type') == 'semua') {
                    $getMs = \DB::select("SELECT * from master_sales_polis where wf_status_id in(2) and invoice_type_id in(2) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                    foreach ($getMs as $key => $getId) {
                        $datas[$key] = $getId->id;
                    }
                }else {
                    $datas = $request->value;
                }

                $this->znRejectApproval('master_sales_polis',$datas,$request->SetMemo);
            break;

        }
    }

    public function setNewBitId($id)
    {

        // $id = doc ID
        $data = collect(\DB::select("SELECT product_id from master_sales_polis where id = ".$id))->first();

        // Cek apakah ada penambahan kolom detail baru
        $columnDetail = DB::TABLE('master_qs_detail')
                            ->where('master_qs_detail.doc_id', $id)
                            ->where('doc_off_type', 3)
                            ->get();

        $arrBitId = $columnDetail->pluck('bit_id')->toArray();

        $newBitId = DB::TABLE('ref_insured_detail')
                        ->where('product_id', $data->product_id)
                        ->whereNotIn('bit_id', $arrBitId)
                        ->get();


        if ( count($newBitId) > 0 ) {
            // Ada kolom baru, insert data ke master qs detail

            $rowData = array_values($columnDetail->groupBy('value_int')->toArray());
            foreach ( $newBitId as $item ) {

                foreach ( $rowData as $value ) {
                    // Insert Ke Master QS Detail detail value ""
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');

                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    $row = $value[0]->value_int;

                    // INSERT MASTER QS DETAIL
                    DB::TABLE('master_qs_detail')
                        ->insert([
                            'id' => $id_qs_detail,
                            'doc_id' => $id,
                            'bit_id' => $item->bit_id,
                            'value_text' => "",
                            'value_int' => $row,
                            'doc_off_type' => 3,
                        ]);
                }

            }
        }
    }



}
