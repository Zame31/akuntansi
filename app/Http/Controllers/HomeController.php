<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

        public function index(Request $request)
    {
      if(Auth::user()->user_role_id==5){
        $branch = DB::select("select * from master_branch");
       $company = DB::select("select * from master_company");
       $param['branch']=$branch;
       $param['company']=$company;
       $param['role']=Auth::user()->user_role_id;

      }else{
        $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
       $company = DB::select("select * from master_company");
       $param['branch']=$branch;
       $param['company']=$company;
       $param['role']=Auth::user()->user_role_id;
      }


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'home',$param);
        }else {
            return view('master.master')->nest('child', 'home',$param);
        }
    }



    public function coa(Request $request){
      if($request->value){
        $coa=DB::table('master_coa')
        ->select('coa_no','coa_name','balance_type_id')
        ->whereNotIn('coa_no', $request->value)
        ->where('is_parent',false)
        ->OrderBy('id','asc')
        ->get();
      }else{

       $coa = \DB::select('SELECT coa_no,coa_name,balance_type_id FROM master_coa where is_parent=false order by id asc');

//       $coa = \DB::select('SELECT coa_no,coa_name,balance_type_id FROM master_coa where is_parent=false');

      }
      return json_encode($coa);

    }
    public function cek_email(Request $request){

      if($request->get('idcust')){
        $query = \DB::select("SELECT * FROM master_customer where id=".$request->get('idcust'));
        if($query[0]->email!=$request->get('id')){
            $email = \DB::select("SELECT * FROM master_customer where email='".$request->get('id')."'");
            return json_encode($email);
        }else{
            $email = '';
            return json_encode($email);
        }


      }else{
        $email = \DB::select("SELECT * FROM master_customer where email='".$request->get('id')."'");
        return json_encode($email);
      }



    }

    public function ref_province($id){
      $city = \DB::select('SELECT * FROM ref_city where city_code='.$id);
      $prov = \DB::select('SELECT * FROM ref_province where province_code='.$city[0]->province_code);
      return json_encode($prov);

    }
    public function dok(Request $request){
        if($request->value){

        $dokumen =DB::table('ref_doc_type')
        ->whereNotIn('id', $request->value)
        ->where('is_active',true)
        ->OrderBy('seq','asc')
        ->get();
      }else{
        $dokumen = \DB::select('SELECT * FROM ref_doc_type where is_active=true order by seq asc');

      }

        return json_encode($dokumen);
    }

    public function dbcr(Request $request)
    {
       $dbcr = \DB::select("SELECT balance_type_id  FROM master_coa where coa_no='".$request->get('id')."'");
       return json_encode($dbcr);
    }


    public function fee(Request $request)
    {
       $premi_amount_config = \DB::select("select value as jml from master_config where id = 13");
       $discount_amount_config = \DB::select("select value as jml from master_config where id = 9");
       $fee_agent_config = \DB::select("select value as jml from master_config where id = 2");
       $fee_internal_config = \DB::select("select value  as jml from master_config where id = 3");
       $tax_amount_config = \DB::select("select value  as jml from master_config where id = 8");


       $premi_amount=($premi_amount_config[0]->jml * $request->get('jml')) / 100;
       $discount=($discount_amount_config[0]->jml * $premi_amount) / 100;
       $nett_amount=$premi_amount - $discount;
       $fee_agent=($fee_agent_config[0]->jml * $premi_amount) / 100;
       $komisi_perusahaan=($fee_internal_config[0]->jml * $premi_amount) / 100;
       $asuransi=$nett_amount - $fee_agent - $komisi_perusahaan;
       $tax_amount=($tax_amount_config[0]->jml * $fee_agent) / 100;


       return json_encode(['premi_amount'=>$premi_amount,'discount'=>$discount,'nett_amount'=>$nett_amount,'fee_agent'=>$fee_agent,'komisi_perusahaan'=>$komisi_perusahaan,'asuransi'=>$asuransi,'tax_amount'=>$tax_amount]);
    }


    public function set_end_of_day(){
      $date_now=date('Y-m-d');
      $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

      if($date_now==$systemDate->current_date){
        return json_encode(['rc'=>2,'msg'=>'End Of Day is already processed']);
      }else{
        $upd_date = date('Y-m-d', strtotime('+1 days', strtotime($systemDate->current_date)));

          DB::table('ref_system_date')
          ->where('id', 1)
          ->update(['current_date' => $upd_date,'last_date'=>$systemDate->current_date]);

        return json_encode(['rc'=>1,'msg'=>'End Of Day Process Finished']);
      }
    }







}
