<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use PDF;
use Excel;
use Request as Req;
use Illuminate\Support\Collection;

class TransaksiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index(Request $request)
    {
        return view('master.master')->nest('child', 'home');
    }

    public function transaksi_single(Request $request)
    {
        $segment = \DB::select('SELECT * FROM ref_segment where is_active=true order by seq asc');
        $dokumen = \DB::select('SELECT * FROM ref_doc_type where is_active=true order by seq asc');

        //$db = \DB::select('SELECT coa_no,coa_name from master_coa where balance_type_id=0 and is_active=true order by id asc');
        // $coa = \DB::select('SELECT coa_no,coa_name from master_coa where is_active=true and is_parent=false order by coa_no asc');
        $coa = \DB::select("SELECT * FROM master_coa where branch_id = ".Auth::user()->branch_id." and (is_parent = false or coa_no in ('302','303') ) order by coa_no");


        $param['segment']=$segment;
        $param['dokumen']=$dokumen;
        //$param['db']=$db;
        $param['coa']=$coa;

        return view('master.master')->nest('child', 'transaksi.create_single',$param);

    }

     public function transaksi_detail(Request $request)
    {

        if($request->get('type')=='det_reversal'){
           $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
           left join master_coa b on a.coa_id=b.coa_no
           left join ref_segment c on c.id=a.tx_segment_id
           left join master_memo d on d.tx_code=a.tx_code
           left join master_user e on e.id=a.user_crt_id
                           left join master_user ea on ea.id=a.user_app_id
                           left join ref_workflow_status rw on rw.id = a.id_workflow
                where a.tx_code='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." and a.id_workflow=3 and is_reversal=true order by a.id asc");

        }else if($request->get('type')=='reversal_sukses'){

            $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
           left join master_coa b on a.coa_id=b.coa_no
           left join ref_segment c on c.id=a.tx_segment_id
           left join master_memo d on d.tx_code=a.tx_code
           left join master_user e on e.id=a.user_crt_id
                           left join master_user ea on ea.id=a.user_app_id
                           left join ref_workflow_status rw on rw.id = a.id_workflow
                where a.tx_code='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." and a.id_workflow in (12,15) order by a.id asc");

        }else{
            $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
            left join master_coa b on a.coa_id=b.coa_no
            left join ref_segment c on c.id=a.tx_segment_id
            left join master_memo d on d.tx_code=a.tx_code
            left join master_user e on e.id=a.user_crt_id
                            left join master_user ea on ea.id=a.user_app_id
                            left join ref_workflow_status rw on rw.id = a.id_workflow
                where is_reversal is null and a.tx_code='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." order by a.id asc");
        }

        $dokumen = \DB::select("select a.*,b.definition from master_doc a
        left join ref_doc_type b on b.id=a.doc_type_id
        where tx_code='".$request->get('id')."'");

        $param['data']=$data;
        $param['dokumen']=$dokumen;


        $param['type']=$request->get('type');


        return view('master.master')->nest('child', 'transaksi.detail',$param);

    }

    public function export_pdf_transaksi_detail(Request $request)
    {
        if($request->get('type')=='det_reversal'){
            $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
            left join master_coa b on a.coa_id=b.coa_no
            left join ref_segment c on c.id=a.tx_segment_id
            left join master_memo d on d.tx_code=a.tx_code
            left join master_user e on e.id=a.user_crt_id
                            left join master_user ea on ea.id=a.user_app_id
                            left join ref_workflow_status rw on rw.id = a.id_workflow
                 where a.tx_code='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." and a.id_workflow=3 and is_reversal=true order by a.id asc");

         }else if($request->get('type')=='reversal_sukses'){

             $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
            left join master_coa b on a.coa_id=b.coa_no
            left join ref_segment c on c.id=a.tx_segment_id
            left join master_memo d on d.tx_code=a.tx_code
            left join master_user e on e.id=a.user_crt_id
                            left join master_user ea on ea.id=a.user_app_id
                            left join ref_workflow_status rw on rw.id = a.id_workflow
                 where a.tx_code='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." and a.id_workflow in (12,15) order by a.id asc");

         }else if($request->get('type')=='inventory'){

            $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
            left join master_coa b on a.coa_id=b.coa_no
            left join ref_segment c on c.id=a.tx_segment_id
            left join master_memo d on d.tx_code=a.tx_code
            left join master_user e on e.id=a.user_crt_id
                            left join master_user ea on ea.id=a.user_app_id
                            left join ref_workflow_status rw on rw.id = a.id_workflow
                where is_reversal is null and a.type_tx = 'master_inventory_schedule' and a.id_rel='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." order by a.id asc");


            if ($data) {
                # code...
            }else{
                $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
                left join master_coa b on a.coa_id=b.coa_no
                left join ref_segment c on c.id=a.tx_segment_id
                left join master_memo d on d.tx_code=a.tx_code
                left join master_user e on e.id=a.user_crt_id
                                left join master_user ea on ea.id=a.user_app_id
                                left join ref_workflow_status rw on rw.id = a.id_workflow
                    where is_reversal is null and a.id_rel='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." order by a.id asc");

            }

         }
         else if($request->get('type')=='bdd'){

            $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
            left join master_coa b on a.coa_id=b.coa_no
            left join ref_segment c on c.id=a.tx_segment_id
            left join master_memo d on d.tx_code=a.tx_code
            left join master_user e on e.id=a.user_crt_id
                            left join master_user ea on ea.id=a.user_app_id
                            left join ref_workflow_status rw on rw.id = a.id_workflow
                where is_reversal is null and a.type_tx = 'master_bdd' and a.id_rel='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." order by a.id asc");

            if ($data) {
                # code...
            }else{
                $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
                left join master_coa b on a.coa_id=b.coa_no
                left join ref_segment c on c.id=a.tx_segment_id
                left join master_memo d on d.tx_code=a.tx_code
                left join master_user e on e.id=a.user_crt_id
                                left join master_user ea on ea.id=a.user_app_id
                                left join ref_workflow_status rw on rw.id = a.id_workflow
                    where is_reversal is null and a.id_rel='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." order by a.id asc");

            }
         }
         else{
             $data = \DB::select("select a.*,c.definition,d.notes,e.fullname,b.coa_name,ea.fullname as user_approve,rw.definition as status_trx from master_tx a
             left join master_coa b on a.coa_id=b.coa_no
             left join ref_segment c on c.id=a.tx_segment_id
             left join master_memo d on d.tx_code=a.tx_code
             left join master_user e on e.id=a.user_crt_id
                             left join master_user ea on ea.id=a.user_app_id
                             left join ref_workflow_status rw on rw.id = a.id_workflow
                 where is_reversal is null and a.tx_code='".$request->get('id')."' and b.branch_id=".Auth::user()->branch_id." order by a.id asc");
         }

         $dokumen = \DB::select("select a.*,b.definition from master_doc a
         left join ref_doc_type b on b.id=a.doc_type_id
         where tx_code='".$request->get('id')."'");

         $param['data']=$data;
         $param['dokumen']=$dokumen;

         $param['type']=$request->get('type');

         $pdf = PDF::loadView('transaksi.pdf_detail_transaksi',$param)->setPaper('a4', 'potrait');
         return $pdf->stream('Detail Transaksi_' . date('dmY_His') . '.pdf');
    }

    public function export_pdf_inventory(Request $request)
    {
        $data = \DB::select("SELECT *, master_inventory.id as mid
        FROM master_inventory
              left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
              left join master_branch on master_branch.id = master_inventory.branch_id
        where id_workflow = 9 and master_inventory.branch_id=".Auth::user()->branch_id."
        and master_inventory.company_id=".Auth::user()->company_id." order by master_inventory.created_at desc");

         $param['data']=$data;

         $param['type']=$request->get('type');

         $pdf = PDF::loadView('cetak.pdf_inventory',$param)->setPaper('a4', 'landscape');
         return $pdf->stream('Inventory_' . date('dmY_His') . '.pdf');
    }

    public function export_excel_inventory(Request $request)
    {
        $data = \DB::select("SELECT *, master_inventory.id as mid
        FROM master_inventory
              left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
              left join master_branch on master_branch.id = master_inventory.branch_id
        where id_workflow = 9 and master_inventory.branch_id=".Auth::user()->branch_id."
        and master_inventory.company_id=".Auth::user()->company_id." order by master_inventory.created_at desc");

         $param['data']=$data;
         $param['type']=$request->get('type');

        Excel::create('Inventory_' . date('dmY_His') . '', function($excel) use ($param){
            $excel->sheet('Sheet 1',function($sheet) use ($param){
                $sheet->loadView('cetak.xlsx_inventory', $param);
            });
        })->export('xlsx');
    }

    public function export_pdf_bdd(Request $request)
    {
        $data = \DB::select("SELECT *, master_bdd.id as mid
        FROM master_bdd left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
        left join master_branch on master_branch.id = master_bdd.branch_id
        left join master_coa on master_coa.id = master_bdd.coa_acc_no
        where id_workflow = 9 and master_bdd.branch_id=".Auth::user()->branch_id." and master_bdd.company_id=".Auth::user()->company_id." order by master_bdd.created_at desc");

         $param['data']=$data;

         $param['type']=$request->get('type');

         $pdf = PDF::loadView('cetak.pdf_bdd',$param)->setPaper('a4', 'landscape');
         return $pdf->stream('BDD_' . date('dmY_His') . '.pdf');
    }

    public function export_excel_bdd(Request $request)
    {
        $data = \DB::select("SELECT *, master_bdd.id as mid
        FROM master_bdd left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
        left join master_branch on master_branch.id = master_bdd.branch_id
        left join master_coa on master_coa.id = master_bdd.coa_acc_no
        where id_workflow = 9 and master_bdd.branch_id=".Auth::user()->branch_id." and master_bdd.company_id=".Auth::user()->company_id." order by master_bdd.created_at desc");


         $param['data']=$data;
         $param['type']=$request->get('type');

        Excel::create('BDD_' . date('dmY_His') . '', function($excel) use ($param){
            $excel->sheet('Sheet 1',function($sheet) use ($param){
                $sheet->loadView('cetak.xlsx_bdd', $param);
            });
        })->export('xlsx');
    }

    



        public function transaksi_baru(Request $request)
    {
        $segment = \DB::select('SELECT * FROM ref_segment where is_active=true order by seq asc');
        $dokumen = \DB::select('SELECT * FROM ref_doc_type where is_active=true order by seq asc');
        $param['segment']=$segment;
        $param['dokumen']=$dokumen;


            return view('master.master')->nest('child', 'transaksi.create',$param);


    }


    public function insert_tr_baru(Request $request){

        // dd($request);
        $tgl_tr = date('Y-m-d', strtotime($request->input('tgl_tr')));
        $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();

        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

        $today=date('Y-m-d');
         if($systemDate->current_date==$today){

            $txcode='';
            // $tgl_tr=date('Y-m-d',strtotime($systemDate->current_date));

            // $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($systemDate->current_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where created_at::date='".date('Ymd',strtotime($systemDate->current_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            $prx=date('Ymd',strtotime($systemDate->current_date));
            $prx_branch=Auth::user()->branch_id;
            $prx_company=Auth::user()->company_id;

            if($results){

              $id_max= $results[0]->max_id;
              $sort_num = (int) substr($id_max, 1, 4);
              $sort_num++;
              $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
              $txcode=$new_code;
            }else{
                $txcode=$prx.$prx_branch.$prx_company."0001";
            }


            $catatan=$request->input('catatan');
            $idsegment=$request->input('idsegment');
            $tx_type=$request->input('type');


            $datacount= count($request->coa_no);
            for ($i=0; $i < $datacount; $i++) {


                $id='';
                $results = \DB::select("SELECT max(id) AS id from master_tx");
                if($results){
                    $id=$results[0]->id + 1;
                }else{
                    $id=1;
                }


                $coa=$request->coa_no[$i];
                $ket=$request->keterangan[$i];
                $debet=str_replace('.','', $request->debet[$i]);
                $debet1=str_replace(',','.', $debet);

                $kredit=str_replace('.','', $request->kredit[$i]);
                $kredit1=str_replace(',','.', $kredit);
                if($debet1==0){
                    $type=1;
                    $amount=$kredit1;

                }else{
                    $type=0;
                    $amount=$debet1;
                }


                //insert transaksi
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa."'");
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $type,
                        'coa_id' => $coa,
                        'tx_notes' => $ket,
                        'tx_amount' => $amount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 1,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => $tx_type,
                        'coa_type_id' => $ref_coa[0]->coa_type_id
                    ]
                );
            }

            //insert dokumen
                $jns_dok=count($request->jns_dok);
                for ($i=0; $i < $jns_dok ; $i++) {

                    $destination_path = public_path('upload_dokumen');
                    $files = $request->file[$i];
                    if($files){
                        $filename = $files->getClientOriginalName();
                        $upload_success = $files->move($destination_path, $txcode."_".$filename);

                        //$file=$this->request->getUploadedFiles()[$i];
                        //$file->moveTo('temp/' . $file->getName());
                        $jnsdok=$request->jns_dok[$i];
                        $iddoc='';
                        $results = \DB::select("SELECT max(id) AS id from master_doc");

                        if($results){
                            $iddoc=$results[0]->id + 1;
                        }else{
                            $iddoc=1;
                        }

                        DB::table('master_doc')->insert(
                            [
                                'id' => $iddoc,
                                'tx_code' => $txcode,
                                'url' => $filename,
                                'doc_type_id' => $jnsdok,
                                'created_at' => date('Y-m-d H:i:s'),
                                'user_crt_id' => Auth::user()->id
                            ]
                        );
                    }

                }




            //insert memo
            $idmemo='';
            $results = \DB::select("SELECT max(id) AS id from master_memo");
            if($results){
                $idmemo=$results[0]->id + 1;
            }else{
                $idmemo=1;
            }

            DB::table('master_memo')->insert(
                    [
                        'id' => $idmemo,
                        'tx_code' => $txcode,
                        'notes' => $catatan,
                        'id_workflow' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );

            return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
             return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
        }


    }

    public function list_tr_baru(Request $request)
    {
       $results = \DB::select("select a.tx_code,a.tx_date,sum(a.tx_amount) as total,c.notes,b.fullname,a.type_tx,d.short_code from
                            master_tx a
                            left join master_user b on b.id=a.user_crt_id
                            left join master_memo c on c.tx_code=a.tx_code
                            left join master_branch d on d.id=a.branch_id
                            where a.id_workflow in (1,0) and tx_type_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."
                            GROUP BY a.tx_code,a.tx_date,c.notes,b.fullname,a.type_tx,d.short_code order by a.tx_date desc");
       $param['data']=$results;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'transaksi.list',$param);
        }else {
            return view('master.master')->nest('child', 'transaksi.list',$param);
        }
    }

    public function list_reversal(Request $request)
    {
       $results = \DB::select("select a.tx_code,a.tx_date,sum(a.tx_amount) as total,c.notes,b.fullname,d.short_code from
                            master_tx a
                            left join master_user b on b.id=a.user_crt_id
                            left join master_memo c on c.tx_code=a.tx_code
                            left join master_branch d on d.id=a.branch_id
                            where a.id_workflow=3 and tx_type_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."
                            GROUP BY a.tx_code,a.tx_date,c.notes,b.fullname,d.short_code order by a.tx_date desc");
       $param['data']=$results;


            return view('master.master')->nest('child', 'transaksi.list_reversal',$param);

    }
    public function list_trx_success(Request $request)
    {
       $results = \DB::select("select a.tx_code,a.tx_date,sum(a.tx_amount) as total,c.notes,b.fullname,d.short_code from
                            master_tx a
                            left join master_user b on b.id=a.user_crt_id
                            left join master_memo c on c.tx_code=a.tx_code
                            left join master_branch d on d.id=a.branch_id
                            where a.id_workflow=9 and tx_type_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."
                            GROUP BY a.tx_code,a.tx_date,c.notes,b.fullname,d.short_code  order by a.tx_date desc");
       $param['data']=$results;


            return view('master.master')->nest('child', 'transaksi.list_trx_success',$param);

    }

    public function list_rev_success(Request $request)
    {
       $results = \DB::select("select a.tx_code,a.tx_date,sum(a.tx_amount) as total,c.notes,b.fullname,d.short_code from
                            master_tx a
                            left join master_user b on b.id=a.user_crt_id
                            left join master_memo c on c.tx_code=a.tx_code
                            left join master_branch d on d.id=a.branch_id
                            where a.id_workflow=12 and tx_type_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."
                            GROUP BY a.tx_code,a.tx_date,c.notes,b.fullname,d.short_code order by a.tx_date desc");
       $param['data']=$results;


            return view('master.master')->nest('child', 'transaksi.list_rev_success',$param);

    }

    public function list_overdue(Request $request)
    {
      $tgl_now=date('Y-m-d');
      $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
                        left join master_branch e on e.id=a.branch_id where a.wf_status_id=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and due_date_jrn <= '".$tgl_now."'");
      if($data){

            foreach ($data as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['is_overdue'=>true,'is_splitted'=>false]);
            }

      }

       $param['data']=$data;


            return view('master.master')->nest('child', 'transaksi.list_overdue',$param);

    }




    public function kirimAproval(Request $request){
        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_tx where id_workflow in (1,3)");

            foreach ($results as $item) {

                DB::table('master_tx')
                ->where('id', $item->id)
                ->update(['id_workflow' => 2]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_tx')
                ->where('tx_code', $item)
                ->update(['id_workflow' => 2]);




            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_tx where id_workflow in (1,0) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_memo')->where('tx_code', '=',$item->tx_code)->delete();
                DB::table('master_doc')->where('tx_code', '=',$item->tx_code)->delete();
                DB::table('master_tx')->where('tx_code', '=',$item->tx_code)->delete();

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_memo')->where('tx_code', '=',$item)->delete();
                DB::table('master_doc')->where('tx_code', '=',$item)->delete();
                DB::table('master_tx')->where('tx_code', '=',$item)->delete();

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function kirim_reversal(Request $request){

            foreach ($request->value as $item) {

               DB::table('master_tx')
                ->where('tx_code', $item)
                ->update(['id_workflow' => 3]);
            /*
            $data = \DB::select("select * from master_tx where tx_code='".$item."'");

            foreach ($data as $dt) {

                $results = \DB::select("SELECT max(id) AS id from master_tx");
                if($results){
                    $id=$results[0]->id + 1;
                }else{
                    $id=1;
                }

                if($dt->tx_type_id==0){
                    $type=1;
                }else{
                    $type=0;
                }

                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $item,
                        'tx_date' => $dt->tx_date,
                        'tx_type_id' => $type,
                        'coa_id' => $dt->coa_id,
                        'tx_notes' => $dt->tx_notes,
                        'tx_amount' => $dt->tx_amount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $dt->tx_segment_id,
                        'id_workflow' => 3,
                        'is_reversal' => true,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:s:i'),
                        'type_tx' => 'reversal',
                        'coa_type_id' => $dt->coa_type_id,
                    ]
                );

            }
            */
        }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }

    public function kirim_approve_reversal(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_tx where id_workflow=3 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_tx')
                ->where('id', $item->id)
                //->where('is_reversal', true)
                ->where('id_workflow', 3)
                ->update(['id_workflow' => 12]);

                 $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($item->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $item->tx_code,
                            'tx_date' => $item->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $item->coa_id,
                            'tx_notes' => $item->tx_notes,
                            'tx_amount' => $item->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $item->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_transaksi',
                            'coa_type_id' => $item->coa_type_id,
                            'seq_approval' => $set_last_seq_app
                        ]
                    );
                /*
                DB::table('master_tx')
                ->where('tx_code', $item->tx_code)
                //->where('id_workflow', 3)
                ->update(['is_reversal' => true,'id_workflow' => 15]);
                */

                DB::table('master_memo')
                ->where('tx_code', $item->tx_code)
                ->update(['id_workflow' => 10]);


            }
             $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true  and type_tx='reversal_transaksi' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $item) {

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 15]);

                    $coa = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");



                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('coa_id', $item->coa_id)
                    //->where('is_reversal', true)
                    //->where('id_workflow', 12)
                    ->update(['acc_last' => $coa[0]->last_os]);

                    $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                    if($ref_jurnal[0]->operator_sign=='-'){
                        $acc_os=$coa[0]->last_os - $item->tx_amount;
                    }else{
                        $acc_os=$coa[0]->last_os + $item->tx_amount;
                    }

                    //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $item->tx_amount;


                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('coa_id', $item->coa_id)
                    //->where('is_reversal', true)
                    //->where('id_workflow', 12)
                    ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$item->tx_notes]);

                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', $item->coa_id)
                    ->update(['last_os' => $acc_os]);
                }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{

            foreach ($request->value as $item) {
                DB::table('master_tx')
                ->where('tx_code', $item)
                //->where('is_reversal', true)
                ->where('id_workflow', 3)
                ->update(['id_workflow' => 12]);

                /*
                 DB::table('master_tx')
                ->where('tx_code', $item)
                ->where('id_workflow', 3)
                ->update(['is_reversal' => true,'id_workflow' => 12]);
                */

                DB::table('master_memo')
                ->where('tx_code', $item)
                ->update(['id_workflow' => 10]);

                $results = \DB::select("select * from master_tx where tx_code='".$item."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;


                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_transaksi',
                            'coa_type_id' => $dt->coa_type_id,
                            'seq_approval' => $set_last_seq_app
                        ]
                    );
                }
                $results = \DB::select("select * from master_tx where tx_code='".$item."' and id_workflow=3 and is_reversal=true  and type_tx='reversal_transaksi' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $dat) {
                    DB::table('master_tx')
                    ->where('id', $dat->id)
                    ->update(['id_workflow' => 15]);

                    $coa = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$dat->coa_id."'");



                    DB::table('master_tx')
                    ->where('id', $dat->id)
                    ->where('coa_id', $dat->coa_id)
                    //->where('is_reversal', true)
                    //->where('id_workflow', 12)
                    ->update(['acc_last' => $coa[0]->last_os]);

                    $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$dat->coa_type_id."' and tx_type_id=".$dat->tx_type_id);

                    if($ref_jurnal[0]->operator_sign=='-'){
                        $acc_os=$coa[0]->last_os - $dat->tx_amount;
                    }else{
                        $acc_os=$coa[0]->last_os + $dat->tx_amount;
                    }

                    //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $dat->tx_amount;


                    DB::table('master_tx')
                    ->where('id', $dat->id)
                    ->where('coa_id', $dat->coa_id)
                    //->where('is_reversal', true)
                    //->where('id_workflow', 12)
                    ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$dat->tx_notes]);

                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', $dat->coa_id)
                    ->update(['last_os' => $acc_os]);
                }
            }




        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapus_approve_reversal(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_tx where id_workflow in (3) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {
                if($item->is_reversal==true){

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->delete();


                }else{

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 9]);

                    DB::table('master_memo')
                    ->where('tx_code', $item->tx_code)
                    ->update(['id_workflow' => 3]);




                }


            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{

            foreach ($request->value as $item) {

                $results = \DB::select("select * from master_tx where id_workflow in (3) and branch_id=".Auth::user()->branch_id." and tx_code='".$item."' and company_id=".Auth::user()->company_id);

                foreach ($results as $value) {

                    if($value->is_reversal==true){
                        DB::table('master_tx')
                        ->where('id', $value->id)
                        ->delete();


                    }else{
                       DB::table('master_tx')
                        ->where('id', $value->id)
                        ->update(['id_workflow' => 9]);

                        DB::table('master_memo')
                        ->where('tx_code', $item)
                        ->update(['id_workflow' => 3]);


                    }

                }


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }
}
