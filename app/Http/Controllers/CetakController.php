<?php


namespace App\Http\Controllers;
include_once 'HTMLtoOpenXML.php';

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use PDF;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Element\TblWidth;
use PhpOffice\PhpWord\TemplateProcessor;
use HTMLtoOpenXML;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Excel;

class CetakController extends Controller
{
    public function balance_sheet()
    {
        // if (Req::ajax()) {
        //     return view('master.only_content')->nest('child', 'cetak.p_balance_sheet');
        // }else {
        //     return view('master.master')->nest('child', 'cetak.p_balance_sheet');
        // }

        $pdf = PDF::loadview('cetak.p_balance_sheet');
    	return $pdf->download('laporan-pegawai-pdf');
    }

    public function balance_sheet_view()
    {
        return view('cetak.p_balance_sheet');

        // if (Req::ajax()) {
        //     return view('cetak.p_balance_sheet')->nest('child', 'cetak.p_balance_sheet');
        // }else {
        //     return view('master.master')->nest('child', 'cetak.p_balance_sheet');
        // }

    }

    public function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}
		return $temp;
	}

    public function generateDocx($type,$id)
    {

        $data = collect(\DB::select('select a.*,mb.leader_name,mb.jabatan,b.full_name,b.address as c_address,ru.definition as underwriter, c.definition as produk,d.full_name as agent,e.qs_no as quotation,f.qs_no as proposal,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create,rv.mata_uang,rv.deskripsi
        from master_sales a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=a.agent_id
                    left join ref_quotation e on e.id=a.qs_no
                    left join ref_proposal f on f.id=a.proposal_no
                    left join ref_cust_segment g on g.id=a.segment_id
                    left join ref_bank_account h on h.id=a.afi_acc_no
                    left join ref_paid_status rw on rw.id = a.paid_status_id
                    left join master_user mu on mu.id = a.user_approval_id
                    left join master_user mus on mus.id = a.user_crt_id
                    left join master_branch mb on mb.id = a.branch_id
					left join ref_underwriter ru on a.underwriter_id = ru.id
                    left join ref_valuta rv on rv.id=a.valuta_id  where a.id='.$id))->first();

        // dd($data);

        // New Word Document

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $phpWord->setDefaultParagraphStyle(
            array(
            //'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::LEFT,
            'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0),
            'spacing' => 120,
            'lineHeight' => 1.15,
            )
            );

        $section = $phpWord->addSection([
            'paperSize' => 'A4',
            'marginLeft' => 600,
            'marginRight' => 600,
            'marginTop' => 600,
            'marginBottom' => 600,
            'spacing' => 120,
            'lineHeight' => 1,
            'spaceBefore' => 0, 'spaceAfter' => 0
          ]);

            $new_premi = $data->premi_amount;

            if (($new_premi % 1) > 0.5) {
                $new_premi = ceil($new_premi);

            }else {
                $new_premi = floor($new_premi);
            }



            $new_periode = date('d - m - Y',strtotime($data->date));


            $new_adm = $data->polis_amount + $data->materai_amount;

            $new_total =  $new_adm + $data->net_amount;



            if ($data->valuta_id != 1) {

                $new_adm = $new_adm/$data->kurs_today;
                $new_total = round($new_total/$data->kurs_today,2);
                $new_premi = $new_premi/$data->kurs_today;
            }



            $mata_uang = $data->mata_uang;
            $mata_uang_des = $data->deskripsi;

            if($data->valuta_id!=1){
                $premi_des = $this->numFormat($new_premi);
                $ins_amount=$data->ins_amount/$data->kurs_today;
            }else{
                $premi_des = $this->numFormat($new_premi);
                $ins_amount=$data->ins_amount;
            }

            // $tmp_new_total = explode('.', $new_total);

            $frac  = $new_total - (int) $new_total;
            // dd($frac);



            $sisa_desimal = 0;


            if ($frac > 0.5) {
                $sisa_desimal = (1.00 - $frac);
                $new_total = ceil($new_total);

            }else {
                $sisa_desimal = ($new_total - floor($new_total));

                $new_total = floor($new_total);

            }


            $data_imgz = collect(\DB::select("SELECT url_image from master_company where id = 1"))->first();


        if ($type == 'paid') {


            $html = '<div style="font-size:10pt;color:#000000;padding:40px;">
            <table style="width:100%">
                <tr>
                    <td><img style="margin-right:100px;padding-right:100px;" src="'.asset('img/'.$data_imgz->url_image).'" width="70" /></td>
                    <td style="color:#ffffff"> ----------------------------------------------------------------------</td>
                    <td><b>PT HD SOERYO INDONESIA GEMILANG</b><br/><div>Gedung RIFA Mega Kuningan Lt. 2</div><br/><div>Jl. Prof. Dr. Satrio Blok C4 Kav. 6-7</div><br/><div>Jakarta Selatan 12950</div><br/><br/><div>Tel :(021) 5260 781 | Fax :(021) 5260782</div><br/><div>e-mail : hds.indonesiagemilang@gmail.com</div></td>
                </tr>
            </table>

            <b style="font-size:11pt;display: block;text-align:center;margin-top: 10px;">OFFICIAL RECEIPT</b>
            <div style="text-align:right;">NO: '.$data->kwitansi_no.'</div>
            <table style="width:100%;">
                <tr>
                    <td><b><u>Customer Name (Nama pelanggan)</u></b></td>
                    <td><b>:</b></td>
                    <td><b>'.$data->polis_no.' - '.$data->full_name.'</b></td>
                </tr>
                <tr>
                    <td><b><u>Being Paymenf of (Untuk Pembayaran)</u></b></td>
                    <td><b>:</b></td>
                    <td><b>'.$data->produk.'</b></td>
                </tr>
                <tr>
                    <td><b><u>Amount (Jumlah)</u></b></td>
                    <td><b>:</b></td>
                    <td><b>'.$mata_uang.' '.$new_total.'</b></td>
                </tr>
                <tr>
                    <td><b><u>The Sum of (Uang Sejumlah) </u></b></td>
                    <td><b>:</b></td>
                    <td><b><u>'.$this->penyebut($new_total).' '.$mata_uang_des.'</u></b></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr >
                    <td style="margin-top:80px;"><b><div style="height: 10px;width: 10px;border: 2px solid #000000;display: inline-block;margin-right: 5px;"></div><span style="font-weight: bold;">Cheque/Giro</span>
                    <span style="margin-left:80px;font-weight: bold;">Cash</span></b></td>
                    <td> </td>
                    <td> <span style="font-weight: bold;">Transfer</span></td>
                </tr>

                <tr>
                    <td><b><u>Remarks (Catatan) </u></b></td>
                    <td><b>:</b></td>
                    <td><b></b></td>
                </tr>
            </table>

            <table style="width:100%;">
                <tr>
                    <td style="width:10%;"><div style="margin-right:100px;"><br/><br/>
                    <table style="width:70%; border: 6px #000000 solid;">
                        <tr>
                            <td><b>Pembayaran premi dianggap lunas setelah dana <br/> diterima di rekening PT. HD Soeryo Indonesia<br/> Gemilang</b></td>
                        </tr>
                    </table>
                     </div>
                    </td>
                    <td style="width:50%;text-align:center;"><b>Jakarta,'.date('d').' '.$this->MonthIndo(date('m')).' '.date('Y').'</b><br/><b>Authorized By </b><br/><br/><br/><br/><b><u>'.$data->leader_name.'</u></b><br/><b>'.$data->jabatan.'</b>
                    </td>
                </tr>
            </table>

            </div>
            ';

        }else {

            $html = '<div style="font-size:10pt;color:#000000;padding:40px;">
            <table style="width:100%">
                <tr>
                    <td><img style="margin-right:100px;padding-right:100px;" src="'.asset('img/'.$data_imgz->url_image).'" width="70" /></td>
                    <td style="color:#ffffff"> ----------------------------------------------------------------------</td>
                    <td><b>PT HD SOERYO INDONESIA GEMILANG</b><br/><div>Gedung RIFA Mega Kuningan Lt. 2</div><br/><div>Jl. Prof. Dr. Satrio Blok C4 Kav. 6-7</div><br/><div>Jakarta Selatan 12950</div><br/><br/><div>Tel :(021) 5260 781 | Fax :(021) 5260782</div><br/><div>e-mail : hds.indonesiagemilang@gmail.com</div></td>
                </tr>
            </table>

            <b style="font-size:11pt;display: block;text-align:center;margin-top: 10px;">DEBIT NOTE</b>
            <div style="text-align:right;">NO: '.$data->inv_no.'</div>

        <table style="width: 100%; border: 6px #000000 solid;">
            <tr>
                <td>
                    <table style="width:40%; border: 6px #ffffff solid;">
                        <tr>
                            <td><b>Company Name</b></td>
                            <td><b>:</b></td>
                            <td><b>'.$data->full_name.'</b></td>
                        </tr>
                        <tr>
                            <td><b>Received Name</b></td>
                            <td><b>:</b></td>
                            <td><b> - </b></td>
                        </tr>
                        <tr>
                            <td><b>Address</b></td>
                            <td><b>:</b></td>
                            <td><b>'.$data->c_address.'</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><table style="width: 100%; border: 6px #000000 solid;">'.
                        '<thead>'.
                            '<tr style="text-align: center; color: #000000; font-weight: bold; ">'.
                                '<th>No</th>'.
                                '<th>Description(s)</th>'.
                                '<th>Amount</th>'.
                            '</tr>'.
                        '</thead>'.
                        '<tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <table style="width:100%; border: 6px #ffffff solid;">
                                        <tr>
                                            <td><b>In Respect Of</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->produk.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Insured</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->full_name.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Policy No</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->polis_no.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Period From</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$new_periode.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Underwriter</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->underwriter.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Sum Insured</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$mata_uang.' '.$this->numFormat($ins_amount).'</b></td>
                                        </tr>
                                    </table>

                                    <table style="width:100%; border: 6px #ffffff solid;">
                                    <tr><td>Nett Premium</td></tr>
                                    <tr><td></td></tr>
                                    <tr><td>Polis and Stamp Duty Cost</td></tr>
                                    <tr><td style="text-align:right;">(+/-)</td></tr>
                                    </table>


                                </td>
                                <td>
                                    <br/><br/><br/><br/><br/><br/>
                                    <table style="width:100%; border: 6px #ffffff solid;">
                                    <tr><td style="text-align:right;">'.$mata_uang.' '.$premi_des.'</td></tr>
                                    <tr><td></td></tr>
                                    <tr><td style="text-align:right;">'.$mata_uang.' '.$this->numFormat($new_adm).'</td></tr>
                                    <tr><td style="text-align:right;">('.$this->numFormat($sisa_desimal).')</td></tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:right;font-weight:bold;">Grand Total</td>
                                <td style="text-align:right;font-weight:bold;">'.$mata_uang.' '.$this->numFormat($new_total).'</td>
                            </tr>
                            <tr>
                            <td style="font-weight:bold;" colspan="3">In Words (Indonesia) : # '.$this->penyebut($new_total).' '.$mata_uang_des.'</td>
                        </tr>
                        </tbody>'.
                    '</table></td>
            </tr>
        </table>

        <table style="width:100%;">
        <tr>
            <td style="width:10%;"><u>Notes:</u><ol><li>Payment should be made in the form oft the crossed cheque(Giro) payable <br/> to PT. HD Soeryo Indonesia Gemilang</li><li>Please attach the PAYMENT ADVICE SLIP  together with your <br/> payment and send to Finance Division of PT. HD Soeryo Indonesia <br/> Gemilang</li><li>Transfer to our bank account : <br/>Bank : BCA Slipi <br/>Account Number : <br/>084.23.233.29 (IDR Currency) <br/>084.23.238.68 (USD Currency)<br/><b>PT.HD Soeryo Indonesia Gemilang</b>
                   </li>
            </ol>
            </td>
            <td style="width:50%;text-align:center;"><b>Jakarta,'.date('d').' '.$this->MonthIndo(date('m')).' '.date('Y').'</b><br/><b>Authorized By </b><br/><br/><br/><br/><b><u>'.$data->leader_name.'</u></b><br/><b>'.$data->jabatan.'</b>
            </td>
        </tr>
    </table>
    </div>
        ';

    }

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="'.$data->inv_no.'.docx"');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');


    }

    public function generateDocxSc($invoice_type,$type,$id)
    {

        if ($invoice_type == 'normal') {
            $data = collect(\DB::select('select a.*,a.inv_date as date,mb.leader_name,mata_uang,deskripsi,mb.jabatan,b.full_name,b.address as c_address,ru.definition as underwriter, c.definition as produk,d.full_name as agent,e.qs_no as quotation,f.qs_no as proposal,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
                        left join ref_agent d on d.id=a.agent_id
                        left join ref_quotation e on e.id=a.qs_no
                        left join ref_proposal f on f.id=a.proposal_no
                        left join ref_cust_segment g on g.id=a.segment_id
                        left join ref_bank_account h on h.id=a.afi_acc_no
                        left join ref_paid_status rw on rw.id = a.paid_status_id
                        left join master_user mu on mu.id = a.user_approval_id
                        left join master_user mus on mus.id = a.user_crt_id
                        left join master_branch mb on mb.id = a.branch_id
                        left join ref_valuta rv on rv.id = a.valuta_id
                                            left join ref_underwriter ru on a.underwriter_id = ru.id  where a.id='.$id))->first();

        }else {
            $data = collect(\DB::select('select a.*,ms.kurs_today,mata_uang,deskripsi,ms.valuta_id,mb.leader_name,mb.jabatan,b.full_name,b.address as c_address,ru.definition as underwriter, c.definition as produk,d.full_name as agent,e.qs_no as quotation,f.qs_no as proposal,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create
            from master_sales_schedule a
            left join master_sales ms on ms.id = a.id_master_sales
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
                        left join ref_agent d on d.id=a.agent_id
                        left join ref_quotation e on e.id=a.qs_no
                        left join ref_proposal f on f.id=a.proposal_no
                        left join ref_cust_segment g on g.id=a.segment_id
                        left join ref_bank_account h on h.id=a.afi_acc_no
                        left join ref_paid_status rw on rw.id = a.paid_status_id
                        left join master_user mu on mu.id = a.user_approval_id
                        left join master_user mus on mus.id = a.user_crt_id
                        left join master_branch mb on mb.id = a.branch_id
                        left join ref_valuta rv on rv.id = ms.valuta_id
                                            left join ref_underwriter ru on a.underwriter_id = ru.id   where a.id='.$id))->first();

        }


        // dd($data);

        // New Word Document

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $phpWord->setDefaultParagraphStyle(
            array(
            //'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::LEFT,
            'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0),
            'spacing' => 120,
            'lineHeight' => 1.15,
            )
            );

        $section = $phpWord->addSection([
            'paperSize' => 'A4',
            'marginLeft' => 600,
            'marginRight' => 600,
            'marginTop' => 600,
            'marginBottom' => 600,
            'spacing' => 120,
            'lineHeight' => 1,
            'spaceBefore' => 0, 'spaceAfter' => 0
          ]);

          $new_premi = $data->premi_amount;

            if (($new_premi % 1) > 0.5) {
                $new_premi = ceil($new_premi);

            }else {
                $new_premi = floor($new_premi);
            }


            if ($data->start_date == '1970-01-01') {
                $new_periode = '';
            }else{
                $new_periode = date('d - m - Y',strtotime($data->start_date)).' to ';
            }


            $end_periode = date('d - m - Y',strtotime($data->end_date));


            $new_adm = $data->polis_amount + $data->materai_amount;

            $new_total =  $new_adm + $data->premi_amount;



            if ($data->valuta_id != 1) {

                $new_adm = $new_adm/$data->kurs_today;
                $new_total = round($new_total/$data->kurs_today,2);
                $new_premi = $new_premi/$data->kurs_today;
            }



            $mata_uang = $data->mata_uang;
            $mata_uang_des = $data->deskripsi;

            if($data->valuta_id!=1){
                $premi_des = $this->numFormat($new_premi);
                $ins_amount=$data->ins_amount/$data->kurs_today;
            }else{
                $premi_des = $this->numFormat($new_premi);
                $ins_amount=$data->ins_amount;
            }

            // $tmp_new_total = explode('.', $new_total);

            $frac  = $new_total - (int) $new_total;
            // dd($frac);



            $sisa_desimal = 0;


            if ($frac > 0.5) {
                $sisa_desimal = (1.00 - $frac);
                $new_total = ceil($new_total);

            }else {
                $sisa_desimal = ($new_total - floor($new_total));

                $new_total = floor($new_total);

            }


            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();


        if ($type == 'paid') {


            $html = '<div style="font-size:10pt;color:#000000;padding:40px;">
            <table style="width:100%">
                <tr>
                    <td><img style="margin-right:100px;padding-right:100px;" src="'.asset('img/'.$data_imgz->image_cetak).'" width="250" /></td>
                    <td style="color:#ffffff"> ----------------------</td>
                    <td>'.$data_imgz->address_cetak_word.'</td>
                </tr>
            </table>

            <b style="font-size:11pt;display: block;text-align:center;margin-top: 10px;">OFFICIAL RECEIPT</b>
            <div style="text-align:right;">NO: '.$data->kwitansi_no.'</div>
            <table style="width:100%;">
                <tr>
                    <td><b><u>Customer Name (Nama pelanggan)</u></b></td>
                    <td><b>:</b></td>
                    <td><b>'.$data->polis_no.' - '.$data->full_name.'</b></td>
                </tr>
                <tr>
                    <td><b><u>Being Paymenf of (Untuk Pembayaran)</u></b></td>
                    <td><b>:</b></td>
                    <td><b>'.$data->produk.'</b></td>
                </tr>
                <tr>
                    <td><b><u>Amount (Jumlah)</u></b></td>
                    <td><b>:</b></td>
                    <td><b>'.$mata_uang.' '.$this->numFormat($new_total).'</b></td>
                </tr>
                <tr>
                    <td><b><u>The Sum of (Uang Sejumlah) </u></b></td>
                    <td><b>:</b></td>
                    <td><b><u>'.$this->penyebut($new_total).' '.$mata_uang_des.'</u></b></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr >
                    <td style="margin-top:80px;"><b><div style="height: 10px;width: 10px;border: 2px solid #000000;display: inline-block;margin-right: 5px;"></div><span style="font-weight: bold;">Cheque/Giro</span>
                    <span style="margin-left:80px;font-weight: bold;">Cash</span></b></td>
                    <td> </td>
                    <td> <span style="font-weight: bold;">Transfer</span></td>
                </tr>

                <tr>
                    <td><b><u>Remarks (Catatan) </u></b></td>
                    <td><b>:</b></td>
                    <td><b></b></td>
                </tr>
            </table>

            <table style="width:100%;">
                <tr>
                    <td style="width:10%;"><div style="margin-right:100px;"><br/><br/>
                    <table style="width:70%; border: 6px #000000 solid;">
                        <tr>
                            <td><b>Pembayaran premi dianggap lunas setelah dana <br/> diterima di rekening PT. HD Soeryo Indonesia<br/> Gemilang</b></td>
                        </tr>
                    </table>
                     </div>
                    </td>
                    <td style="width:50%;text-align:center;"><b>Jakarta,'.date('d').' '.$this->MonthIndo(date('m')).' '.date('Y').'</b><br/><b>Authorized By </b><br/><br/><br/><br/><b><u>'.$data->leader_name.'</u></b><br/><b>'.$data->jabatan.'</b>
                    </td>
                </tr>
            </table>

            </div>
            ';

        }else {

            $html = '<div style="font-size:10pt;color:#000000;padding:40px;">
            <table style="width:100%">
                <tr>
                    <td><img style="margin-right:100px;padding-right:100px;" src="'.asset('img/'.$data_imgz->url_image).'" width="250" /></td>
                    <td style="color:#ffffff"> ----------------------</td>
                    <td>'.$data_imgz->address_cetak_word.'</td>
                </tr>
            </table>

            <b style="font-size:11pt;display: block;text-align:center;margin-top: 10px;">DEBIT NOTE</b>
            <div style="text-align:right;">NO: '.$data->inv_no.'</div>

        <table style="width: 100%; border: 6px #000000 solid;">
            <tr>
                <td>
                    <table style="width:40%; border: 6px #ffffff solid;">
                        <tr>
                            <td><b>Customer Name</b></td>
                            <td><b>:</b></td>
                            <td><b>'.$data->full_name.'</b></td>
                        </tr>
                        <tr>
                            <td><b>Received Name</b></td>
                            <td><b>:</b></td>
                            <td><b> - </b></td>
                        </tr>
                        <tr>
                            <td><b>Address</b></td>
                            <td><b>:</b></td>
                            <td><b>'.$data->c_address.'</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><table style="width: 100%; border: 6px #000000 solid;">'.
                        '<thead>'.
                            '<tr style="text-align: center; color: #000000; font-weight: bold; ">'.
                                '<th>No</th>'.
                                '<th>Description(s)</th>'.
                                '<th>Amount</th>'.
                            '</tr>'.
                        '</thead>'.
                        '<tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <table style="width:100%; border: 6px #ffffff solid;">
                                        <tr>
                                            <td><b>In Respect Of</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->produk.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Insured</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->full_name.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Policy No</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->polis_no.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Period From</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$new_periode.$end_periode.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Underwriter</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$data->underwriter.'</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>Sum Insured</b></td>
                                            <td><b>:</b></td>
                                            <td><b>'.$mata_uang.' '.$this->numFormat($ins_amount).'</b></td>
                                        </tr>
                                    </table>

                                    <table style="width:100%; border: 6px #ffffff solid;">
                                    <tr><td>Gross Premium</td></tr>
                                    <tr><td></td></tr>
                                    <tr><td>Polis and Stamp Duty Cost</td></tr>
                                    <tr><td style="text-align:right;">(+/-)</td></tr>
                                    </table>


                                </td>
                                <td>
                                    <br/><br/><br/><br/><br/><br/>
                                    <table style="width:100%; border: 6px #ffffff solid;">
                                    <tr><td style="text-align:right;">'.$mata_uang.' '.$premi_des.'</td></tr>
                                    <tr><td></td></tr>
                                    <tr><td style="text-align:right;">'.$mata_uang.' '.$this->numFormat($new_adm).'</td></tr>
                                    <tr><td style="text-align:right;">('.$this->numFormat($sisa_desimal).')</td></tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:right;font-weight:bold;">Grand Total</td>
                                <td style="text-align:right;font-weight:bold;">'.$mata_uang.' '.$this->numFormat($new_total).'</td>
                            </tr>
                            <tr>
                            <td style="font-weight:bold;" colspan="3">In Words (Indonesia) : # '.$this->penyebut($new_total).' '.$mata_uang_des.'</td>
                        </tr>
                        </tbody>'.
                    '</table></td>
            </tr>
        </table>

        <table style="width:100%;">
        <tr>
            <td style="width:10%;"><u>Notes:</u><ol><li>Payment should be made in the form oft the crossed cheque(Giro) payable <br/> to PT. HD Soeryo Indonesia Gemilang</li><li>Please attach the PAYMENT ADVICE SLIP  together with your <br/> payment and send to Finance Division of PT. HD Soeryo Indonesia <br/> Gemilang</li><li>Transfer to our bank account : <br/>Bank : BCA Slipi <br/>Account Number : <br/>084.23.233.29 (IDR Currency) <br/>084.23.238.68 (USD Currency)<br/><b>PT.HD Soeryo Indonesia Gemilang</b>
                   </li>
            </ol>
            </td>
            <td style="width:50%;text-align:center;"><b>Jakarta,'.date('d').' '.$this->MonthIndo(date('m')).' '.date('Y').'</b><br/><b>Authorized By </b><br/><br/><br/><br/><b><u>'.$data->leader_name.'</u></b><br/><b>'.$data->jabatan.'</b>
            </td>
        </tr>
    </table>
    </div>
        ';

    }

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="'.$data->inv_no.'.docx"');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');


    }

    public function xmlEntities($str)
    {
        $xml = array('&#34;','&#38;','&#38;','&#60;','&#62;','&#160;','&#161;','&#162;','&#163;','&#164;','&#165;','&#166;','&#167;','&#168;','&#169;','&#170;','&#171;','&#172;','&#173;','&#174;','&#175;','&#176;','&#177;','&#178;','&#179;','&#180;','&#181;','&#182;','&#183;','&#184;','&#185;','&#186;','&#187;','&#188;','&#189;','&#190;','&#191;','&#192;','&#193;','&#194;','&#195;','&#196;','&#197;','&#198;','&#199;','&#200;','&#201;','&#202;','&#203;','&#204;','&#205;','&#206;','&#207;','&#208;','&#209;','&#210;','&#211;','&#212;','&#213;','&#214;','&#215;','&#216;','&#217;','&#218;','&#219;','&#220;','&#221;','&#222;','&#223;','&#224;','&#225;','&#226;','&#227;','&#228;','&#229;','&#230;','&#231;','&#232;','&#233;','&#234;','&#235;','&#236;','&#237;','&#238;','&#239;','&#240;','&#241;','&#242;','&#243;','&#244;','&#245;','&#246;','&#247;','&#248;','&#249;','&#250;','&#251;','&#252;','&#253;','&#254;','&#255;');
        $html = array('&quot;','&amp;','&amp;','&lt;','&gt;','&nbsp;','&iexcl;','&cent;','&pound;','&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;','&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;','&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;','&frac34;','&iquest;','&Agrave;','&Aacute;','&Acirc;','&Atilde;','&Auml;','&Aring;','&AElig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;','&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;','&Ocirc;','&Otilde;','&Ouml;','&times;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;','&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;','&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;','&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;','&otilde;','&ouml;','&divide;','&oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;','&thorn;','&yuml;');
        $str = str_replace($html,$xml,$str);
        $str = str_ireplace($html,$xml,$str);
        return $str;
    }

    public function cetakInsurace(Request $request, $type, $id) {

        if ( $type == 'qs' ) {
            $data = DB::TABLE('master_qs_vehicle')
                    ->select(
                                'master_qs_vehicle.*',
                                'master_customer.address',
                                'ref_product.definition',
                                'master_customer.full_name',
                                'ref_underwriter.definition as security',
                                'ref_business_source.business_def',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_qs_vehicle.product_id')
                    ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_qs_vehicle.underwriter_id')
                    ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_qs_vehicle.buss_source_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_qs_vehicle.valuta_id')
                    ->where('master_qs_vehicle.id', $id)
                    ->first();

            $colsName = DB::TABLE('ref_insured_detail')
                    ->where('product_id', $data->product_id)
                    ->where('is_active', 't')
                    ->where('bit_id', '!=', 12)
                    ->orderBy('id', 'ASC')
                    ->get();

            $parent = DB::TABLE('master_qs_detail')
                            ->select('value_int')
                            ->distinct()
                            ->where('doc_id', $id)
                            ->where(function($query) {
                                $query->where('bit_id', 12);
                                $query->where('value_text', 'f');
                            })
                            ->get();

            $parent = $parent->pluck('value_int')->toArray();

            $detail = DB::TABLE('master_qs_detail')
                        ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number')
                        ->leftJoin('master_qs_vehicle', 'master_qs_vehicle.id', '=', 'master_qs_detail.doc_id')
                        ->leftJoin('ref_insured_detail', function($join) {
                            $join->on('ref_insured_detail.product_id', '=', 'master_qs_vehicle.product_id');
                            $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                        })
                        ->where('master_qs_detail.doc_id', $id)
                        ->where('master_qs_detail.bit_id', '!=', 12)
                        ->where('master_qs_detail.doc_off_type', 0)
                        ->whereNotIn('master_qs_detail.value_int', $parent)
                        ->get();

            $tittle = 'QUOTATION SLIP';
            $type_doc = 'QS';
            $no_docs = $data->qs_no;
        } else if ( $type == 'cn' ) {
            $data = DB::TABLE('master_cn_vehicle')
                        ->select(
                                    'master_cn_vehicle.*',
                                    'master_customer.address',
                                    'ref_product.definition',
                                    'master_customer.full_name',
                                    'ref_underwriter.definition as security',
                                    'ref_business_source.business_def',
                                    'ref_valuta.mata_uang'
                                )
                        ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cn_vehicle.customer_id')
                        ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cn_vehicle.product_id')
                        ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_cn_vehicle.underwriter_id')
                        ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_cn_vehicle.buss_source_id')
                        ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_cn_vehicle.valuta_id')
                        ->where('master_cn_vehicle.id', $id)
                        ->first();

            $colsName = DB::TABLE('ref_insured_detail')
                        ->where('product_id', $data->product_id)
                        ->where('is_active', 't')
                        ->where('bit_id', '!=', 12)
                        ->orderBy('id', 'ASC')
                        ->get();

            $parent = DB::TABLE('master_qs_detail')
                            ->select('value_int')
                            ->distinct()
                            ->where('doc_id', $id)
                            ->where(function($query) {
                                $query->where('bit_id', 12);
                                $query->where('value_text', 'f');
                            })
                            ->get();

            $parent = $parent->pluck('value_int')->toArray();

            $detail = DB::TABLE('master_qs_detail')
                        ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number')
                        ->leftJoin('master_cn_vehicle', 'master_cn_vehicle.id', '=', 'master_qs_detail.doc_id')
                        ->leftJoin('ref_insured_detail', function($join) {
                            $join->on('ref_insured_detail.product_id', '=', 'master_cn_vehicle.product_id');
                            $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                        })
                        ->where('master_qs_detail.doc_id', $id)
                        ->where('master_qs_detail.bit_id', '!=', 12)
                        ->where('master_qs_detail.doc_off_type', 1)
                        ->whereNotIn('master_qs_detail.value_int', $parent)
                        ->get();

            $tittle = 'PROPOSAL OF INSURANCE';
            $type_doc = 'CN';
            $no_docs = $data->cn_no;
        } else {
            $data = DB::TABLE('master_cf_vehicle')
                    ->select(
                                'master_cf_vehicle.*',
                                'master_customer.address',
                                'ref_product.definition',
                                'master_customer.full_name',
                                'ref_underwriter.definition as security',
                                'ref_business_source.business_def',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cf_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cf_vehicle.product_id')
                    ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_cf_vehicle.underwriter_id')
                    ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_cf_vehicle.buss_source_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_cf_vehicle.valuta_id')
                    ->where('master_cf_vehicle.id', $id)
                    ->first();

            $colsName = DB::TABLE('ref_insured_detail')
                            ->where('product_id', $data->product_id)
                            ->where('is_active', 't')
                            ->where('bit_id', '!=', 12)
                            ->orderBy('id', 'ASC')
                            ->get();

            $parent = DB::TABLE('master_qs_detail')
                            ->select('value_int')
                            ->distinct()
                            ->where('doc_id', $id)
                            ->where(function($query) {
                                $query->where('bit_id', 12);
                                $query->where('value_text', 'f');
                            })
                            ->get();

            $parent = $parent->pluck('value_int')->toArray();

            $detail = DB::TABLE('master_qs_detail')
                        ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number')
                        ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                        ->leftJoin('ref_insured_detail', function($join) {
                            $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                            $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                        })
                        ->where('master_qs_detail.doc_id', $id)
                        ->where('master_qs_detail.bit_id', '!=', 12)
                        ->where('master_qs_detail.doc_off_type', 2)// COC
                        ->whereNotIn('master_qs_detail.value_int', $parent)
                        ->get();

            $tittle = 'CONFIRMATION OF COVER';
            $type_doc = 'COC';
            $no_docs = $data->cf_no;
        }

        $detail = array_values($detail->groupBy('value_int')->toArray());


        $premium = 0;

        foreach ( $detail as $values ) {
            foreach ( $values as $item ) {
                switch ( $item->bit_id ) {
                    case 21 :
                        // Premium
                        $premium += (float)str_replace('.', '', $item->value_text);
                    break;
                }
            }
        }


        $the_business = str_replace('&', 'and', $data->the_business);
        $form_wording = str_replace('&', 'and', $data->form_wording);
        // $coverage = str_replace('&', 'and', $data->coverage);


        if ( $data->product_id == 4 ) {
            // Vehicle Product
            if ( $type != 'qs' ) {
                $file = public_path('insurance_docs_vehicle_coc_cn.docx');
            } else {
                $file = public_path('insurance_docs_vehicle.docx');
            }

        } else {
            // Non Vehicle
            if ( $type != 'qs' ) {
                $file = public_path('insurance_docs_non_vehicle_coc_cn.docx');
            } else {
                $file = public_path('insurance_docs_non_vehicle.docx');
            }

        }

        $phpword = new \PhpOffice\PhpWord\TemplateProcessor($file);

        if ( $type == 'cn' || $type == 'coc' ) {
            $phpword->setValue('acquisition_cost', $data->acquitition_cost . '%');
            $phpword->setValue('security_info', $data->security_note);
            $phpword->setValue('info_underwriting', $data->info_underwriting);
        }



        // if ( $data->no_of_insured == 1) {
        //     $values = [];
        //     foreach ( $detail as $item ) {
        //         $values[] = [
        //                             'headerDetail' => $item->label,
        //                             'spt' => ':',
        //                             'content' => $item->value_text
        //                         ];
        //     }
        //     $phpword->cloneRowAndSetValues('headerDetail', $values);
        // } else {
        //     $phpword->setValue('headerDetail', '');
        //     $phpword->setValue('spt', '');
        //     $phpword->setValue('content', '');
        // }

        $phpword->setValue('tittle', $tittle);
        $phpword->setValue('no_docs', $no_docs);
        $phpword->setValue('form_wording', $form_wording);
        $phpword->setValue('type_of_insurance', $data->definition);
        $phpword->setValue('customer_name', strtoupper(strtolower($data->full_name)));
        $phpword->setValue('address', $data->address);
        $phpword->setValue('the_business', $the_business);
        if ( is_null($data->from_date) && is_null($data->to_date) ) {
            $dt = DB::TABLE('ref_qs_content')
                        ->where('product_id', $data->product_id)
                        ->first();
            if ( !is_null($dt) ) {
                $poi = str_replace('&', 'and', $dt->period_default);
            }
        } else {
            $poi = '<b> From </b> '. date('d/m/Y', strtotime($data->from_date)) .' <b> To </b> '.date('d/m/Y', strtotime($data->to_date));
            $poi = strip_tags($poi);
        }

        $TSI = strip_tags('<b> ' . $data->mata_uang . ' ' . number_format($data->total_sum_insured, 0, ',', '.') . ' </b>');
        $phpword->setValue('period_of_insurance', $poi);
        $phpword->setValue('no_of_insured', $data->no_of_insured);
        $phpword->setValue('total_sum_insured', $TSI);

        // $coverage = str_replace('&amp;', 'and', $data->coverage);
        // $coverage = str_replace('&nbsp;', ' ', $coverage);
        // $coverage = preg_replace('/\n/', '', $coverage);
        // $coverage = preg_replace('/\r/', '', $coverage);
        // $coverage = strip_tags($coverage);

        // $main_exclusions = str_replace('&amp;', 'and', $data->main_exclusions);
        // $main_exclusions = str_replace('&nbsp;', ' ', $main_exclusions);
        // $main_exclusions = preg_replace('/\n/', '', $main_exclusions);
        // $main_exclusions = preg_replace('/\r/', '', $main_exclusions);
        // $main_exclusions = strip_tags($main_exclusions);
        // $toOpenXML = HTMLtoOpenXML::getInstance()->fromHTML("<p>te<b>s</b>t</p>");
        // $main_exclusions = htmlentities($data->main_exclusions);


        // $deductible = str_replace('&amp;', 'and', $data->deductible);
        // $deductible = str_replace('&nbsp;', ' ', $deductible);
        // $deductible = str_replace('<div>-', '    <div> -', $deductible);
        // $deductible = strip_tags($deductible);

        // $clauses = str_replace('&amp;', 'and', $data->clauses);
        // $clauses = str_replace('&nbsp;', ' ', $clauses);
        // $clauses = str_replace('<div>-', '    <div> -', $clauses);
        // $clauses = strip_tags($clauses);



        // $coverage = $this->xmlEntities($data->coverage);
        // $main_exclusions = $this->xmlEntities($data->main_exclusions);
        // $deductible = $this->xmlEntities($data->deductible);
        // $clauses = $this->xmlEntities($data->clauses);

        // $coverage = $data->coverage;
        // $main_exclusions = $data->main_exclusions;
        // $deductible = $data->deductible;
        // $clauses = $data->clauses;


        $parser = new \HTMLtoOpenXML\Parser();
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(false);
        $coverage = $parser->fromHTML(htmlspecialchars_decode($data->coverage));
        $phpword->setValue('coverage', $coverage);

        $parser2 = new HTMLtoOpenXML\Parser();
        $main_exclusions = $parser2->fromHTML(htmlspecialchars_decode($data->main_exclusions));
        $phpword->setValue('main_exclusions', $main_exclusions);

        $parser3 = new HTMLtoOpenXML\Parser();
        $deductible = $parser3->fromHTML(htmlspecialchars_decode($data->deductible));
        $phpword->setValue('deductible', $deductible);

        $parser4 = new \HTMLtoOpenXML\Parser();
        $clauses = $parser4->fromHTML(htmlspecialchars_decode($data->clauses));
        $phpword->setValue('clauses', $clauses);
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);


        // $coverage = $data->coverage;
        // $main_exclusions = $data->main_exclusions;
        // $deductible = $data->deductible;
        // $clauses = $data->clauses;

        $phpword->setValue('clauses', $clauses);
        // $phpword->setValue('rate', $data->rate);
        // $phpword->setValue('add_rate', $data->add_rate);
        // $ttl_rate = $data->rate + $data->add_rate;
        // $phpword->setValue('ttl_rate', $ttl_rate);
        // $phpword->setValue('tsi', number_format($data->total_sum_insured, 0, '.', ','));
        // $phpword->setValue('comp_amt', number_format($data->comprehensive_amount, 2, '.', ','));
        // $phpword->setValue('flood_amt', number_format($data->flood_amount, 2, '.', ','));
        // $ttl1 = $data->comprehensive_amount + $data->flood_amount;
        // $phpword->setValue('ttl1', number_format($ttl1, 2, '.', ','));
        // $phpword->setValue('valuta', $data->mata_uang);
        // $phpword->setValue('pa', number_format($data->personal_accident, 0, '.', ','));
        // $phpword->setValue('tpl_limit', number_format($data->tpl_limit, 0, '.', ','));
        // $phpword->setValue('pa_driver', number_format($data->pa_driver, 0, '.', ','));
        // $phpword->setValue('pa_passenger', number_format($data->pa_passenger, 0, '.', ','));
        // $subTotal = $ttl1 + $data->tpl_limit + $data->pa_driver + $data->pa_passenger;


        $phpword->setValue('premium', number_format($data->comprehensive_amount, 2, ',', '.'));
        $phpword->setValue('add_premi', number_format($data->add_premi, 2, ',', '.'));
        $phpword->setValue('gross_premium', number_format($data->gross_premium, 2, ',', '.'));
        $phpword->setValue('discount_amount', number_format($data->discount_amount, 2, ',', '.'));
        $phpword->setValue('nett_premium', number_format($data->nett_premium, 2, ',', '.'));
        $phpword->setValue('add_disc', number_format($data->add_disc, 2, ',', '.'));
        $phpword->setValue('premium_final', number_format($data->premium_final, 2, ',', '.'));
        $phpword->setValue('security', strtoupper(strtolower($data->security)));
        $phpword->setValue('tgl', date('d F Y'));

        $temp_file = tempnam(sys_get_temp_dir(), 'PhpWord');
        $phpword->saveAs($temp_file);
        $fileName = $type_doc . '_' . strtoupper(strtolower($data->full_name)) . '_' . date('d/m/Y') . '.docx';
        header('Content-Disposition: attachment; filename=' . $fileName);
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file

    }

    public function cetakLampiranInsurace(Request $request, $type, $id) {

        if ( $type == 'qs' ) {
            $data = DB::TABLE('master_qs_vehicle')
                    ->select(
                                'master_qs_vehicle.*',
                                'master_customer.address',
                                'ref_product.definition',
                                'master_customer.full_name',
                                'ref_underwriter.definition as security',
                                'ref_business_source.business_def',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_qs_vehicle.product_id')
                    ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_qs_vehicle.underwriter_id')
                    ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_qs_vehicle.buss_source_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_qs_vehicle.valuta_id')
                    ->where('master_qs_vehicle.id', $id)
                    ->first();

            $colsName = DB::TABLE('ref_insured_detail')
                    ->where('product_id', $data->product_id)
                    ->where('is_active', 't')
                    ->where('bit_id', '!=', 12)
                    ->orderBy('bit_id', 'ASC')
                    ->get();

            $parent = DB::TABLE('master_qs_detail')
                            ->select('value_int')
                            ->distinct()
                            ->where('doc_id', $id)
                            ->where(function($query) {
                                $query->where('bit_id', 12);
                                $query->where('value_text', 'f');
                            })
                            ->get();

            $parent = $parent->pluck('value_int')->toArray();

            $detail = DB::TABLE('master_qs_detail')
                        ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number', 'ref_insured_detail.is_currency')
                        ->leftJoin('master_qs_vehicle', 'master_qs_vehicle.id', '=', 'master_qs_detail.doc_id')
                        ->leftJoin('ref_insured_detail', function($join) {
                            $join->on('ref_insured_detail.product_id', '=', 'master_qs_vehicle.product_id');
                            $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                        })
                        ->where('master_qs_detail.doc_id', $id)
                        ->where('master_qs_detail.bit_id', '!=', 12)
                        ->where('master_qs_detail.doc_off_type', 0)
                        ->whereNotIn('master_qs_detail.value_int', $parent)
                        ->where('ref_insured_detail.is_active', 't')
                        ->orderBy('ref_insured_detail.bit_id', 'ASC')
                        ->get();

            $tittle = 'QUOTATION SLIP';
            $type_doc = 'QS';
            $no_docs = $data->qs_no;
        } else if ( $type == 'cn' ) {
            $data = DB::TABLE('master_cn_vehicle')
                        ->select(
                                    'master_cn_vehicle.*',
                                    'master_customer.address',
                                    'ref_product.definition',
                                    'master_customer.full_name',
                                    'ref_underwriter.definition as security',
                                    'ref_business_source.business_def',
                                    'ref_valuta.mata_uang'
                                )
                        ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cn_vehicle.customer_id')
                        ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cn_vehicle.product_id')
                        ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_cn_vehicle.underwriter_id')
                        ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_cn_vehicle.buss_source_id')
                        ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_cn_vehicle.valuta_id')
                        ->where('master_cn_vehicle.id', $id)
                        ->first();

            $colsName = DB::TABLE('ref_insured_detail')
                        ->where('product_id', $data->product_id)
                        ->where('is_active', 't')
                        ->where('bit_id', '!=', 12)
                        ->orderBy('bit_id', 'ASC')
                        ->get();

            $parent = DB::TABLE('master_qs_detail')
                            ->select('value_int')
                            ->distinct()
                            ->where('doc_id', $id)
                            ->where(function($query) {
                                $query->where('bit_id', 12);
                                $query->where('value_text', 'f');
                            })
                            ->get();

            $parent = $parent->pluck('value_int')->toArray();

            $detail = DB::TABLE('master_qs_detail')
                        ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number', 'ref_insured_detail.is_currency')
                        ->leftJoin('master_cn_vehicle', 'master_cn_vehicle.id', '=', 'master_qs_detail.doc_id')
                        ->leftJoin('ref_insured_detail', function($join) {
                            $join->on('ref_insured_detail.product_id', '=', 'master_cn_vehicle.product_id');
                            $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                        })
                        ->where('master_qs_detail.doc_id', $id)
                        ->where('master_qs_detail.bit_id', '!=', 12)
                        ->where('master_qs_detail.doc_off_type', 1)
                        ->whereNotIn('master_qs_detail.value_int', $parent)
                        ->where('ref_insured_detail.is_active', 't')
                        ->orderBy('ref_insured_detail.bit_id', 'ASC')
                        ->get();

            $tittle = 'PROPOSAL OF INSURANCE';
            $type_doc = 'CN';
            $no_docs = $data->cn_no;
        } else {
            $data = DB::TABLE('master_cf_vehicle')
                    ->select(
                                'master_cf_vehicle.*',
                                'master_customer.address',
                                'ref_product.definition',
                                'master_customer.full_name',
                                'ref_underwriter.definition as security',
                                'ref_business_source.business_def',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cf_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cf_vehicle.product_id')
                    ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_cf_vehicle.underwriter_id')
                    ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_cf_vehicle.buss_source_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_cf_vehicle.valuta_id')
                    ->where('master_cf_vehicle.id', $id)
                    ->first();

            $colsName = DB::TABLE('ref_insured_detail')
                            ->where('product_id', $data->product_id)
                            ->where('is_active', 't')
                            ->where('bit_id', '!=', 12)
                            ->orderBy('bit_id', 'ASC')
                            ->get();

            $parent = DB::TABLE('master_qs_detail')
                            ->select('value_int')
                            ->distinct()
                            ->where('doc_id', $id)
                            ->where(function($query) {
                                $query->where('bit_id', 12);
                                $query->where('value_text', 'f');
                            })
                            ->get();

            $parent = $parent->pluck('value_int')->toArray();

            $detail = DB::TABLE('master_qs_detail')
                        ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number', 'ref_insured_detail.is_currency')
                        ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                        ->leftJoin('ref_insured_detail', function($join) {
                            $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                            $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                        })
                        ->where('master_qs_detail.doc_id', $id)
                        ->where('master_qs_detail.bit_id', '!=', 12)
                        ->where('master_qs_detail.doc_off_type', 2)// COC
                        ->whereNotIn('master_qs_detail.value_int', $parent)
                        ->where('ref_insured_detail.is_active', 't')
                        ->orderBy('ref_insured_detail.bit_id', 'ASC')
                        ->get();

            $tittle = 'CONFIRMATION OF COVER';
            $type_doc = 'COC';
            $no_docs = $data->cf_no;
        }

        $detail = array_values($detail->groupBy('value_int')->toArray());

        $fileName = 'LAMPIRAN_' . $type_doc . '_' . strtoupper(strtolower($data->full_name)) . '_' . date('d/m/Y');
        $param['colsName'] = $colsName;
        $param['doc_type'] = $type_doc;
        $param['data'] = $data;
        $param['detail'] = $detail;
        Excel::create($fileName, function($excel) use ($param){
            $excel->sheet('Sheet 1',function($sheet) use ($param){
                $sheet->loadView('cetak.xlsx_lampiran_insurance', $param);
            });
        })->export('xlsx');

    }


}
