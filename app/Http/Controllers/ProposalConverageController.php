<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use PDF;
use iio\libmergepdf\Merger;
use Illuminate\Support\Collection;
use Datetime;

class ProposalConverageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {


        $data = DB::TABLE('master_qs_vehicle')
                    ->select(
                                'master_customer.full_name',
                                'master_qs_vehicle.*',
                                'ref_client_products.definition',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                    ->leftJoin('ref_client_products', 'ref_client_products.id', '=', 'master_qs_vehicle.product_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_qs_vehicle.valuta_id')
                    ->where('is_client_create', 't')
                    ->where('master_qs_vehicle.customer_id', Auth::user()->customer_id)
                    ->orderBy('master_qs_vehicle.id', 'DESC')
                    ->get();

        $param['data'] = $data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'proposal_coverage.index',$param);
        }else {
            return view('master.master')->nest('child', 'proposal_coverage.index',$param);
        }

    }

    public function getRomawi($bln){
        switch ($bln){
            case 1:
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            case 9:
                return "IX";
                break;
            case 10:
                return "X";
                break;
            case 11:
                return "XI";
                break;
            case 12:
                return "XII";
                break;
        }
    }

    public function create(Request $request)
    {

        $param['products'] = DB::TABLE('ref_client_products')->where('is_active', 't')->get();

        $param['refUnderwriter'] = DB::TABLE('ref_underwriter')
                                        ->where('is_active', 't')
                                        ->get();

        $param['sourceBusiness'] = DB::TABLE('ref_business_source')
                                        ->where('is_active', 't')
                                        ->get();

        $param['ref_valuta'] = DB::TABLE('ref_valuta')->get();


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'proposal_coverage.create',$param);
        }else {
            return view('master.master')->nest('child', 'proposal_coverage.create',$param);
        }

    }

    public function show(Request $request, $id_vehicle) {

        $data = DB::TABLE('master_qs_vehicle')
                    ->select(
                                'master_qs_vehicle.*',
                                'master_customer.address',
                                'ref_client_products.definition as product',
                                'ref_agent.full_name'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                    ->leftJoin('ref_client_products', 'ref_client_products.id', '=', 'master_qs_vehicle.product_id')
                    ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_customer.agent_id')
                    ->where('master_qs_vehicle.id', $id_vehicle)
                    ->first();


        // Cek apakah ada penambahan kolom detail baru
        $columnDetail = DB::TABLE('master_qs_detail')
                            ->where('master_qs_detail.doc_id', $id_vehicle)
                            ->where('doc_off_type', 0)
                            ->get();

        $arrBitId = $columnDetail->pluck('bit_id')->toArray();

        $newBitId = DB::TABLE('ref_insured_detail')
                        ->where('product_id', $data->product_id)
                        ->whereNotIn('bit_id', $arrBitId)
                        ->get();

        if ( count($newBitId) > 0 ) {
            // Ada kolom baru, insert data ke master qs detail

            $rowData = array_values($columnDetail->groupBy('value_int')->toArray());
            foreach ( $newBitId as $item ) {


                foreach ( $rowData as $value ) {
                    // Insert Ke Master QS Detail detail value ""
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');

                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    $row = $value[0]->value_int;

                    // INSERT MASTER QS DETAIL
                    DB::TABLE('master_qs_detail')
                        ->insert([
                            'id' => $id_qs_detail,
                            'doc_id' => $id_vehicle,
                            'bit_id' => $item->bit_id,
                            'value_text' => "",
                            'value_int' => $row,
                            'doc_off_type' => 0,
                        ]);
                }

            }
        }


        $param['data'] = $data;
        $param['products'] = DB::TABLE('ref_client_products')->where('is_active', 't')->get();
        $param['customer'] = DB::TABLE('master_customer')
                                ->where('is_active', 't')
                                ->where('branch_id', Auth::user()->branch_id)
                                ->orderBy('full_name', 'ASC')
                                ->get();

        $param['refUnderwriter'] = DB::TABLE('ref_underwriter')
                                        ->where('is_active', 't')
                                        ->get();

        $param['sourceBusiness'] = DB::TABLE('ref_business_source')
                                    ->where('is_active', 't')
                                    ->get();

        $param['headerTable'] =  DB::TABLE('ref_insured_detail')
                                    ->where('product_id', $data->product_id)
                                    ->where('is_active', 't')
                                    ->where('bit_id', '!=', 12)
                                    ->orderBy('bit_id', 'ASC')
                                    ->get();

        $detail1 = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name')
                    ->leftJoin('master_qs_vehicle', 'master_qs_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_qs_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id_vehicle)
                    ->where('master_qs_detail.bit_id', '!=', 12)
                    ->where('master_qs_detail.doc_off_type', 0)
                    ->where('ref_insured_detail.is_active', 't')
                    ->orderBy('bit_id', 'ASC');
                    // ->get();

        $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name')
                    ->leftJoin('master_qs_vehicle', 'master_qs_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_qs_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id_vehicle)
                    ->where('master_qs_detail.bit_id', 12)
                    ->where('master_qs_detail.doc_off_type', 0)
                    ->unionAll($detail1)
                    ->get();

        $detail = array_values($detail->groupBy('value_int')->toArray());

        $param['detail'] = $detail;
        $param['ref_valuta'] = DB::TABLE('ref_valuta')->get();
        $param['status'] = DB::TABLE('status_qs_client')->where('qs_id', $id_vehicle)->first();

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'proposal_coverage.edit',$param);
        }else {
            return view('master.master')->nest('child', 'proposal_coverage.edit',$param);
        }
    }

}
