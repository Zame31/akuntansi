<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use PDF;
use Illuminate\Support\Collection;

class COCController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $data = DB::TABLE('master_cf_vehicle')
                    ->select(
                                'master_customer.full_name',
                                'master_cf_vehicle.*',
                                'ref_product.definition',
                                'ref_valuta.mata_uang'
                            )
                    ->join('master_customer', 'master_customer.id', '=', 'master_cf_vehicle.customer_id')
                    ->join('ref_product', 'ref_product.id', '=', 'master_cf_vehicle.product_id')
                    ->join('ref_valuta', 'ref_valuta.id', '=', 'master_cf_vehicle.valuta_id')
                    ->orderBy('master_cf_vehicle.id', 'DESC')
                    ->get();

        $param['data'] = $data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'coc.index',$param);
        }else {
            return view('master.master')->nest('child', 'coc.index',$param);
        }

    }

    public function getRomawi($bln){
        switch ($bln){
            case 1:
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            case 9:
                return "IX";
                break;
            case 10:
                return "X";
                break;
            case 11:
                return "XI";
                break;
            case 12:
                return "XII";
                break;
        }
    }

    public function create(Request $request)
    {

        $param['products'] = DB::TABLE('ref_product')->where('is_active', 't')->get();
        $param['customer'] = DB::TABLE('master_customer')
                                ->where('is_active', 't')
                                ->where('branch_id', Auth::user()->branch_id)
                                ->orderBy('full_name', 'ASC')
                                ->get();
        $param['refUnderwriter'] = DB::TABLE('ref_underwriter')
                                        ->where('is_active', 't')
                                        ->get();

        $param['sourceBusiness'] = DB::TABLE('ref_business_source')
                                    ->where('is_active', 't')
                                    ->get();

        $param['ref_valuta'] = DB::TABLE('ref_valuta')->get();

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'coc.create',$param);
        }else {
            return view('master.master')->nest('child', 'coc.create',$param);
        }

    }

    public function getCustomerAddress(Request $request) {
        $data = DB::TABLE('master_customer')
                    ->select('master_customer.*', 'ref_agent.spv_code', 'ref_agent.full_name')
                    ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_customer.agent_id')
                    ->where('master_customer.id', $request->id)
                    ->first();

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data
        ]);
    }



    public function store(Request $request) {
        DB::beginTransaction();

        try {
            $arrData = $request->all();


            // CEK data yang insert client
            $qsClient = DB::TABLE('status_qs_client')
                            ->select('status_qs_client.*')
                            ->leftJoin('master_cn_vehicle', 'master_cn_vehicle.qs_id', '=', 'status_qs_client.qs_id')
                            ->where('master_cn_vehicle.id', $arrData['cn_id'])
                            ->first();

            if ( !is_null($qsClient) ) {
                if ( $qsClient->wf_status_client_id == 4 ) {
                    // Data sudah proposal
                    DB::TABLE('status_qs_client')
                        ->where('qs_id', $qsClient->qs_id)
                        ->update([
                            'wf_status_client_id' => 5
                        ]);
                }
            }

            if ( $arrData['underwriter_id'] == '19' ) {
                // PRODUK KREDIT MULTIGUNA REGULER (KMR)
                $detailKMR = json_decode($arrData['detail']);
                $detailKMR = $this->validateProductKMR($detailKMR);

                $detail = $detailKMR['data'];
                $jmlPassed = $detailKMR['jmlPassed'];

                if ( count($detail) != $jmlPassed ) {
                    return response()->json([
                        'rc' => 1,
                        'rm' => 'Invalid',
                        'data' => $detail
                    ]);
                }

            } else {
                $detail = json_decode($arrData['detail']);
            }

            $company = DB::TABLE('master_company')->where('id', 1)->first();
            if ( is_null($arrData['id']) ) {
                if ( $company->app_name == 'ATA HDIC' ) {
                    $dataProduct = DB::TABLE('ref_product')->where('id', $arrData['underwriter_id'])->first();
                    $codeProduct = "";
                    if ( !is_null($dataProduct)) {
                        $codeProduct = $dataProduct->prod_code;
                    }

                    $id = explode('/', $arrData['cn_no_value'])[1];

                    $year = date('y');
                    $bulan = date('n');
                    $romawi = $this->getRomawi($bulan);
                    $qsNo = 'COC/' . $id . '/' . $codeProduct . '/HSIK-BM/' . $romawi . '/'. $year;
                    $arrData['cf_no_value'] = $qsNo;
                }
            }


            if ( is_null($arrData['rate']) ) {
                $arrData['rate'] = 0;
            }

            if ( is_null($arrData['acquisition_cost']) ) {
                $arrData['acquisition_cost'] = 0;
            }

            if ( is_null($arrData['additional_rate']) ) {
                $arrData['additional_rate'] = 0;
            }

            if ( is_null($arrData['discount']) ) {
                $arrData['discount'] = 0;
            }

            if ( !is_null($arrData['total_sum_insured']) ) {
                $totalSumInsured = str_replace('.', '', $arrData['total_sum_insured']);
                $totalSumInsured = str_replace(',', '.', $totalSumInsured);
                $totalSumInsured = (float)$totalSumInsured;
            } else {
                $totalSumInsured = 0;
            }

            if ( !is_null($arrData['third_party_limit'])) {
                $thirdPartyLimit = str_replace('.', '', $arrData['third_party_limit']);
                $thirdPartyLimit = str_replace(',', '.', $thirdPartyLimit);
                $thirdPartyLimit = (float)$thirdPartyLimit;
            } else {
                $thirdPartyLimit = 0;
            }

            if ( !is_null($arrData['personal_accident']) ) {
                $personalAccident = str_replace('.', '', $arrData['personal_accident']);
                $personalAccident = str_replace(',', '.', $personalAccident);
                $personalAccident = (float)$personalAccident;
            } else {
                $personalAccident = 0;
            }

            $lastId = DB::TABLE('master_cf_vehicle')->max('id');
            if ( is_null($lastId) ) {
                $lastId = 1;
            } else {
                $lastId += 1;
            }

            $valutaId = explode('_', $arrData['valuta_id'])[0];

            if ( is_null($request->from) ) {
                $from = NULL;
            } else {
                $from = date('Y-m-d', strtotime($request->from));
            }

            if ( is_null($request->to) ) {
                $to = NULL;
            } else {
                $to = date('Y-m-d', strtotime($request->to));
            }

            $ttlSI = 0;
            $ttlPremi = 0;
            $ttlAddPremi = 0;
            $ttlGrossPremi = 0;
            $ttlDiscFeet = 0;
            $ttlPremiAfterFeet = 0;
            $ttlAddDisc = 0;
            $ttlPremiFinal = 0;
            // $detail = json_decode($arrData['detail']);
            foreach ( $detail as $dt ) {

                if ( $dt->value[0]->value == 't' && $dt->value[0]->bit_id == 12) {
                    foreach ( $dt->value as $item ) {
                        // Yang Dihitung hanya yang aktif
                        switch ( $item->bit_id ) {
                            case '8' :
                                // Sum Insured
                                $SI = str_replace('.', '', $item->value);
                                $SI = str_replace(',', '.', $SI);
                                $ttlSI += (float)$SI;
                            break;
                            case '21' :
                                // Premium
                                $Premium = str_replace('.', '', $item->value);
                                $Premium = str_replace(',', '.', $Premium);
                                $ttlPremi += (float)$Premium;
                            break;
                            case '28' :
                                // Total Add Premi
                                $addPremi = str_replace('.', '', $item->value);
                                $addPremi = str_replace(',', '.', $addPremi);
                                $ttlAddPremi += (float)$addPremi;
                            break;
                            case '29' :
                                // Gross Premi
                                $grossPremi = str_replace('.', '', $item->value);
                                $grossPremi = str_replace(',', '.', $grossPremi);
                                $ttlGrossPremi += (float)$grossPremi;
                            break;
                            case '30' :
                                // Discount Feet
                                $discFeet = str_replace('.', '', $item->value);
                                $discFeet = str_replace(',', '.', $discFeet);
                                $ttlDiscFeet += (float)$discFeet;
                            break;
                            case '31' :
                                // Premi After Feet
                                $premiAfterFeet = str_replace('.', '', $item->value);
                                $premiAfterFeet = str_replace(',', '.', $premiAfterFeet);
                                $ttlPremiAfterFeet += (float)$premiAfterFeet;
                            break;
                            case '32' :
                            // case '1' :
                                // Additional Discount
                                $addDiscount = str_replace('.', '', $item->value);
                                $addDiscount = str_replace(',', '.', $addDiscount);
                                $ttlAddDisc += (float)$addDiscount;
                            break;
                            case '33' :
                                // Premium Final
                                $premiFinal = str_replace('.', '', $item->value);
                                $premiFinal = str_replace(',', '.', $premiFinal);
                                $ttlPremiFinal += (float)$premiFinal;
                            break;
                        }
                    }
                }

            }

            $totalSumInsured = $ttlSI;


            $arrData['comprehensive_amount'] = $ttlPremi; // Premiun
            $arrData['gross_premium_value'] = $ttlGrossPremi;
            $arrData['discount_amount_value'] = $ttlDiscFeet;
            $arrData['nett_premium_value'] = $ttlPremiAfterFeet;

            if ( is_null($arrData['id']) ) {
                // INSERT

                // $detail = json_decode($arrData['detail']);

                DB::TABLE('master_cf_vehicle')
                    ->insert([
                        'id' => $lastId,
                        'customer_id' => $arrData['customer'],
                        'form_wording' => $arrData['form_wording'],
                        'the_business' => $arrData['the_business'],
                        'no_of_insured' => count($detail),
                        'total_sum_insured' => $totalSumInsured,
                        'add_premi' => $ttlAddPremi,
                        'add_disc' => $ttlAddDisc,
                        'premium_final' => $ttlPremiFinal,
                        'coverage' => $arrData['coverage_content'],
                        'main_exclusions' => $arrData['main_exclusions_content'],
                        'deductible' => $arrData['deductible_content'],
                        'clauses' => $arrData['clauses_content'],
                        'rate' => $arrData['rate'],
                        'add_rate' => $arrData['additional_rate'],
                        'third_party_limit' => $thirdPartyLimit,
                        'personal_accident' => $personalAccident,
                        'discount' => $arrData['discount'],
                        'comprehensive_amount' => $arrData['comprehensive_amount'],
                        'flood_amount' => $arrData['flood_amount'],
                        'tpl_limit' => $arrData['tpl_limit_amount'],
                        'pa_driver' => $arrData['pa_driver_amount'],
                        'pa_passenger' => $arrData['pa_passenger_amount'],
                        'discount_amount' => $arrData['discount_amount_value'],
                        'security' => NULL,
                        'user_crt_id' => Auth::user()->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'is_active' => 't',
                        'cf_status' =>  0,
                        'cn_no' => $arrData['cn_no_value'],
                        'gross_premium' => $arrData['gross_premium_value'],
                        'nett_premium' => $arrData['nett_premium_value'],
                        'underwriter_id' => $arrData['security'],
                        'valuta_id' => $valutaId,
                        'from_date' => $from,
                        'to_date' => $to,
                        'buss_source_id' => $arrData['source_business'],
                        'product_id' => $arrData['underwriter_id'],
                        'cn_id' => $arrData['cn_id'],
                        'cf_no' => $arrData['cf_no_value'],
                        'policy_name' => $arrData['policy_name'],
                        'acquitition_cost' => $arrData['acquisition_cost'],
                        'security_note' => $arrData['security_note'],
                        'info_underwriting' => $arrData['info_underwriting']
                    ]);

                    if ( count($detail) > 0 ) {
                        $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');
                        if ( is_null($id_qs_detail) ) {
                            $id_qs_detail = 1;
                        } else {
                            $id_qs_detail += 1;
                        }

                        $row = 1;
                        foreach ( $detail as $item ) {
                            foreach ( $item->value as $data ) {
                                // INSERT MASTER QS DETAIL
                                DB::TABLE('master_qs_detail')
                                    ->insert([
                                        'id' => $id_qs_detail,
                                        'doc_id' => $lastId,
                                        'bit_id' => $data->bit_id,
                                        'value_text' => $data->value,
                                        'value_int' => $row,
                                        'doc_off_type' => 2,
                                    ]);
                                $id_qs_detail++;
                            }
                            $row++;
                        }
                    } // end if
            } else {

                // Insert Detail
                // $detail = json_decode($arrData['detail']);

                $lastCountEdit = DB::TABLE('master_cf_vehicle')
                                    ->where('id', $arrData['id'])
                                    ->max('count_edit');

                if ( is_null($lastCountEdit) ) {
                    $lastCountEdit = 1;
                } else {
                    $lastCountEdit += 1;
                }

                // UPDATE
                DB::TABLE('master_cf_vehicle')
                    ->where('id', $arrData['id'])
                    ->update([
                        'customer_id' => $arrData['customer'],
                        'form_wording' => $arrData['form_wording'],
                        'the_business' => $arrData['the_business'],
                        'no_of_insured' => count($detail),
                        'total_sum_insured' => $totalSumInsured,
                        'add_premi' => $ttlAddPremi,
                        'add_disc' => $ttlAddDisc,
                        'premium_final' => $ttlPremiFinal,
                        'coverage' => $arrData['coverage_content'],
                        'main_exclusions' => $arrData['main_exclusions_content'],
                        'deductible' => $arrData['deductible_content'],
                        'clauses' => $arrData['clauses_content'],
                        'rate' => $arrData['rate'],
                        'add_rate' => $arrData['additional_rate'],
                        'third_party_limit' => $thirdPartyLimit,
                        'personal_accident' => $personalAccident,
                        'discount' => $arrData['discount'],
                        'comprehensive_amount' => $arrData['comprehensive_amount'],
                        'flood_amount' => $arrData['flood_amount'],
                        'tpl_limit' => $arrData['tpl_limit_amount'],
                        'pa_driver' => $arrData['pa_driver_amount'],
                        'pa_passenger' => $arrData['pa_passenger_amount'],
                        'discount_amount' => $arrData['discount_amount_value'],
                        'security' => NULL,
                        'user_upd_id' => Auth::user()->id,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'is_active' => 't',
                        'cf_status' =>  0,
                        'cn_no' => $arrData['cn_no_value'],
                        'gross_premium' => $arrData['gross_premium_value'],
                        'nett_premium' => $arrData['nett_premium_value'],
                        'underwriter_id' => $arrData['security'],
                        'valuta_id' => $valutaId,
                        'from_date' => $from,
                        'to_date' => $to,
                        'buss_source_id' => $arrData['source_business'],
                        'product_id' => $arrData['underwriter_id'],
                        'cn_id' => $arrData['cn_id'],
                        'cf_no' => $arrData['cf_no_value'],
                        'count_edit' => $lastCountEdit,
                        'policy_name' => $arrData['policy_name']
                    ]);

                    // Delete Data Detail Sebelumnya
                DB::TABLE('master_qs_detail')
                    ->where('doc_id', $arrData['id'])
                    ->delete();

                if ( count($detail) > 0 ) {
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');
                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    $row = 1;
                    foreach ( $detail as $item ) {
                        foreach ( $item->value as $data ) {
                            // INSERT MASTER QS DETAIL
                            DB::TABLE('master_qs_detail')
                                ->insert([
                                    'id' => $id_qs_detail,
                                    'doc_id' => $arrData['id'],
                                    'bit_id' => $data->bit_id,
                                    'value_text' => $data->value,
                                    'value_int' => $row,
                                    'doc_off_type' => 2,
                                ]);
                            $id_qs_detail++;
                        }
                        $row++;
                    }
                } // end if
            }

            DB::commit();
            return response()->json([
                'rc' => 0,
                'rm' => 'Sukses',
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }

    }

    public function show(Request $request, $id_vehicle) {
        $data = DB::TABLE('master_cf_vehicle')
                ->select(
                            'master_cf_vehicle.*',
                            'master_customer.address',
                            'ref_product.definition as product',
                            'ref_agent.full_name'
                        )
                ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cf_vehicle.customer_id')
                ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cf_vehicle.product_id')
                ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_customer.agent_id')
                ->where('master_cf_vehicle.id', $id_vehicle)
                ->first();

        // Cek apakah ada penambahan kolom detail baru
        $columnDetail = DB::TABLE('master_qs_detail')
                            ->where('master_qs_detail.doc_id', $id_vehicle)
                            ->where('doc_off_type', 2)
                            ->get();

        $arrBitId = $columnDetail->pluck('bit_id')->toArray();

        $newBitId = DB::TABLE('ref_insured_detail')
                        ->where('product_id', $data->product_id)
                        ->whereNotIn('bit_id', $arrBitId)
                        ->get();


        if ( count($newBitId) > 0 ) {
            // Ada kolom baru, insert data ke master qs detail

            $rowData = array_values($columnDetail->groupBy('value_int')->toArray());
            foreach ( $newBitId as $item ) {


                foreach ( $rowData as $value ) {
                    // Insert Ke Master QS Detail detail value ""
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');

                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    $row = $value[0]->value_int;

                    // INSERT MASTER QS DETAIL
                    DB::TABLE('master_qs_detail')
                        ->insert([
                            'id' => $id_qs_detail,
                            'doc_id' => $id_vehicle,
                            'bit_id' => $item->bit_id,
                            'value_text' => "",
                            'value_int' => $row,
                            'doc_off_type' => 2,
                        ]);
                }

            }
        }

        $param['data'] = $data;

        $param['products'] = DB::TABLE('ref_product')->where('is_active', 't')->get();
        $param['customer'] = DB::TABLE('master_customer')
                            ->where('is_active', 't')
                            ->where('branch_id', Auth::user()->branch_id)
                            ->orderBy('full_name', 'ASC')
                            ->get();

        $param['refUnderwriter'] = DB::TABLE('ref_underwriter')
                                    ->where('is_active', 't')
                                    ->get();

        $param['sourceBusiness'] = DB::TABLE('ref_business_source')
                                ->where('is_active', 't')
                                ->get();

        $param['headerTable'] =  DB::TABLE('ref_insured_detail')
                                    ->where('product_id', $data->product_id)
                                    ->where('is_active', 't')
                                    ->where('bit_id', '!=', 12)
                                    ->orderBy('bit_id', 'ASC')
                                    ->get();

        $detail1 = DB::TABLE('master_qs_detail')
                ->select('master_qs_detail.*', 'ref_insured_detail.input_name')
                ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                ->leftJoin('ref_insured_detail', function($join) {
                    $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                    $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                })
                ->where('master_qs_detail.doc_id', $id_vehicle)
                ->where('master_qs_detail.bit_id', '!=', 12)
                ->where('master_qs_detail.doc_off_type', 2)
                ->where('ref_insured_detail.is_active', 't')
                ->orderBy('bit_id', 'ASC');

        $detail = DB::TABLE('master_qs_detail')
                ->select('master_qs_detail.*', 'ref_insured_detail.input_name')
                ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                ->leftJoin('ref_insured_detail', function($join) {
                    $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                    $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                })
                ->where('master_qs_detail.doc_id', $id_vehicle)
                ->where('master_qs_detail.bit_id', 12)
                ->where('master_qs_detail.doc_off_type', 2)
                ->unionAll($detail1)
                ->get();


        $detail = array_values($detail->groupBy('value_int')->toArray());


        $param['detail'] = $detail;

        $param['ref_valuta'] = DB::TABLE('ref_valuta')->get();

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'coc.edit',$param);
        }else {
            return view('master.master')->nest('child', 'coc.edit',$param);
        }
    }

    public function setCancel(Request $request) {
        $isCancel = $request->cancel;
        $id = $request->id;

        $rm;
        $data = DB::TABLE('master_cf_vehicle')
                    ->where('id', $id);

        if ( $isCancel == 't' ) {
            $data->update(['is_active' => 'f']);
            $rm = 'Berhasil dibatalkan';
        } else {
            $data->update(['is_active' => 't']);
            $rm = 'Berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $rm
        ]);

    }

    public function export(Request $request, $id) {
        $data = DB::TABLE('master_cf_vehicle')
                    ->select(
                                'master_cf_vehicle.*',
                                'master_customer.address',
                                'ref_product.definition',
                                'master_customer.full_name',
                                'ref_underwriter.definition as security',
                                'ref_business_source.business_def',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cf_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cf_vehicle.product_id')
                    ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_cf_vehicle.underwriter_id')
                    ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_cf_vehicle.buss_source_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_cf_vehicle.valuta_id')
                    ->where('master_cf_vehicle.id', $id)
                    ->first();

                    $colsName = DB::TABLE('ref_insured_detail')
                    ->where('product_id', $data->product_id)
                    ->where('is_active', 't')
                    ->where('bit_id', '!=', 12)
                    ->orderBy('id', 'ASC')
                    ->get();

        $parent = DB::TABLE('master_qs_detail')
                        ->select('value_int')
                        ->distinct()
                        ->where('doc_id', $id)
                        ->where(function($query) {
                            $query->where('bit_id', 12);
                            $query->where('value_text', 'f');
                        })
                        ->get();

        // dd($parent);

        $parent = $parent->pluck('value_int')->toArray();

        $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number')
                    ->leftJoin('master_cf_vehicle', 'master_cf_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_cf_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id)
                    ->where('master_qs_detail.bit_id', '!=', 12)
                    ->where('master_qs_detail.doc_off_type', 2)// COC
                    ->whereNotIn('master_qs_detail.value_int', $parent)
                    ->get();


        $detail = array_values($detail->groupBy('value_int')->toArray());

        $data->no_of_insured = count($detail);
        $param['detail'] = $detail;

        $param['data'] = $data;


        //  return view('quotation_slip.pdf_quotation_slip',$param);

        $pdf = PDF::loadView('coc.pdf_coc',$param)->setPaper('a4', 'potrait');
        $pdf->getDomPDF()->set_option("enable_php", true);
         return $pdf->stream('CF ' . $data->full_name . ' - ' . date('d F Y') . '.pdf');
    } // end function

    public function getDataCN(Request $request)
    {
        $rc = "";
        $rm = "";
        $data = "";

        $cnNo = $request->cnno;
        $cn = DB::TABLE('master_cf_vehicle')->where('cn_no', $cnNo)->first();

        if ( !is_null($cn) ) {
            return response()->json([
                'rc' => 2,
                'rm' => 'Proposal No Sudah Terdaftar'
            ]);
        }

        $cn = DB::TABLE('master_cn_vehicle')
                        ->select(
                                    'master_cn_vehicle.*',
                                    'ref_valuta.mata_uang',
                                    'ref_agent.spv_code',
                                    'ref_product.definition as product'
                                )
                        ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_cn_vehicle.valuta_id')
                        ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cn_vehicle.customer_id')
                        ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_customer.agent_id')
                        ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cn_vehicle.product_id')
                        ->where('cn_no', $cnNo)
                        ->first();

        $id = DB::TABLE('master_cf_vehicle')->max('id');
        if ( is_null($id) ) {
            $id = 1;
        } else {
            $id += 1;
        }

        if ( is_null($cn) ) {

            return response()->json([
                'rc' => 2,
                'rm' => 'Proposal No Tidak Ditemukan'
            ]);

        } else {

            if ( !$cn->is_active ) {
                return response()->json([
                    'rc' => 2,
                    'rm' => 'Proposal No Tidak Aktif'
                ]);
            }

            $year = date('y');
            $bulan = date('n');
            $romawi = $this->getRomawi($bulan);
            $cfNo = $id . '/MKT/HD/' . $cn->spv_code . '/COC/' . $romawi . '/'. $year;
            $cn->cf_no = $cfNo;

            if ( !is_null($cn->from_date) ) {
                $cn->from_date = date('d F Y', strtotime($cn->from_date));
            }

            if ( !is_null($cn->to_date) ) {
                $cn->to_date = date('d F Y', strtotime($cn->to_date));
            }

            return response()->json([
                'rc' => 1,
                'rm' => 'Sukses',
                'data' => $cn
            ]);
        }

    } // end function

    public function getListCNNo(Request $request)
    {
        $data = DB::SELECT("select cn_no
                            from (
                                select
                                    distinct cn_no
                                from master_cn_vehicle
                                union
                                select
                                    distinct cn_no
                                from master_cf_vehicle
                            ) x order by cn_no asc
        ");

        return json_encode($data);
    } // end function

    public function getListFilterCN(Request $request) {
        $arrCN = DB::TABLE('master_cf_vehicle')->select('cn_no')->get();
        $arrCN = $arrCN->pluck('cn_no');

        $data = DB::TABLE('master_cn_vehicle')
                    ->select(
                                'master_customer.full_name',
                                'master_cn_vehicle.*',
                                'ref_product.definition',
                                'ref_valuta.mata_uang as valuta'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_cn_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_cn_vehicle.product_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_cn_vehicle.valuta_id')
                    ->whereNotIn('master_cn_vehicle.cn_no', $arrCN)
                    ->where('master_cn_vehicle.product_id', $request->product_id)
                    ->where('master_cn_vehicle.is_active', 't')
                    ->orderBy('master_cn_vehicle.id', 'DESC')
                    ->get();

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data
        ]);
    }

    public function getDetailInsured(Request $request, $id) {
        $column = [];
        $arr = [];
        $data = DB::TABLE('ref_insured_detail')
                    ->where('product_id', $id)
                    ->where('is_active', 't')
                    ->where('bit_id', '!=', 12)
                    ->orderBy('bit_id', 'ASC')
                    ->get();

        $qs_id = $request->qs_id;
        $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name')
                    ->leftJoin('master_cn_vehicle', 'master_cn_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_cn_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_off_type', 1) // CN
                    ->where('ref_insured_detail.is_active', 't')
                    ->where('master_qs_detail.doc_id', $qs_id)
                    ->orderBy('master_qs_detail.bit_id', 'ASC')
                    ->get();

        $detail = array_values($detail->groupBy('value_int')->toArray());

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data,
            'detail' => $detail
        ]);
    }








}
