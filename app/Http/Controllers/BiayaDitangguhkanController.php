<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\BDDModel;
use App\Models\BDDScModel;
use App\Models\BDDTxModel;
use App\Models\MasterTxModel;


class BiayaDitangguhkanController extends Controller
{
    public function index(Request $request)
    {

        if($request->get('type')=='new'){
            $tittle = 'New Prepaid Expense List';
        }
        else if($request->get('type')=='approve') {
            $tittle = 'Approval New Prepaid Expense';
        }
        else if($request->get('type')=='success') {
            $tittle = 'Success Prepaid Expense List';
        }
        else if($request->get('type')=='approve_reversal') {
            $tittle = 'Approval Delete Prepaid Expense';
        }
        else if($request->get('type')=='deleted') {
            $tittle = 'Deleted Prepaid Expense';
        }

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'biaya_ditangguhkan.list',$param);
        }else {
            return view('master.master')->nest('child', 'biaya_ditangguhkan.list',$param);
        }

    }

    public function scheduleList(Request $request)
    {

        $tittle = 'Approval Payment (Prepaid Expense Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'biaya_ditangguhkan.schedule', $param);
        }else {
            return view('master.master')->nest('child', 'biaya_ditangguhkan.schedule', $param);
        }

    }

    public function scheduleListCancel(Request $request)
    {

        $tittle = 'Approval Cancel Payment (Prepaid Expense Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'biaya_ditangguhkan.schedule_cancel', $param);
        }else {
            return view('master.master')->nest('child', 'biaya_ditangguhkan.schedule_cancel', $param);
        }

    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');
            if ($get_id) {
              BDDModel::destroy($get_id);
              BDDScModel::where('bdd_id', $get_id)->delete();
              BDDTxModel::where('bdd_id', $get_id)->delete();

            }

            $tenor = $request->input('tenor');
            $year = $request->input('year');
            $month = $request->input('month');
            $amor = $request->input('amor');
            $outstanding = $request->input('outstanding');

            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_bdd"))->first();

            $data = new BDDModel();
            $data->id = $get->max_id+1;
            $data->bdd_id = $request->input('inventory_type_id');
            $data->bdd_note = $request->input('inventory_desc');
            $data->bdd_amount = $this->clearSeparator($request->input('purchase_amount'));
            $data->amor_amount = $this->clearSeparator($amor[0]);
            $data->user_crt_id = Auth::user()->id;
            $data->user_upd_id = Auth::user()->id;
            $data->company_id = Auth::user()->company_id;
            $data->branch_id = Auth::user()->branch_id;
            $data->tenor = $tenor;
            $data->id_workflow = 1;
            $data->is_active = 't';
            $data->afi_acc_no = $request->input('afi_acc_no');
            $data->coa_acc_no = $request->input('coa_acc_no');
            $data->tambahan = $request->input('tambahan');

            $data->tax_amount = $this->clearSeparator($request->input('tax_amount'));
            $data->is_tax_company = $request->input('is_tax_company');

            $data->save();

            for ($i=0; $i < $tenor; $i++) {
               $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_bdd_schedule"))->first();

                $dsc = new BDDScModel();
                $dsc->id = $get->max_id+1;
                $dsc->bdd_id = $data->id;
                $dsc->year = $year[$i];
                $dsc->month = $month[$i];
                $dsc->amor_amount = $this->clearSeparator($amor[$i]);
                $dsc->last_os = $this->clearSeparator($outstanding[$i]);
                $dsc->paid_status_id = 0;
                $dsc->user_crt_id = Auth::user()->id;
                $dsc->user_upd_id = Auth::user()->id;
                $dsc->id_workflow = 1;
                $dsc->save();
            }

            // MULTI OVERBOOKING
            $multi_coa_no = $request->input('multi_coa_no');
            $multi_ket = $request->input('multi_ket');
            $multi_debet = $request->input('multi_debet');
            $multi_kredit = $request->input('multi_kredit');

            if ($multi_coa_no) {

                // DEBET
                foreach ($multi_coa_no as $key => $vtx) {

                    $note = '';
                    if ($multi_ket[$key]) {
                        $note = $multi_ket[$key];
                    }

                    // if ($key > 1) {

                        if ($multi_debet[$key] != '') {

                            $get_bdd_tx = collect(\DB::select("SELECT max(id::int) as max_id FROM master_bdd_txadd"))->first();
                            $dtx = new BDDTxModel();
                            $dtx->id = $get_bdd_tx->max_id+1;
                            $dtx->bdd_id = $data->id;
                            $dtx->acc_no = $multi_coa_no[$key];
                            $dtx->amount = ($multi_debet[$key] == '') ? 0 : $this->clearSeparatorDouble($multi_debet[$key]);
                            $dtx->tx_type_id = '0';
                            $dtx->note = $note;
                            $dtx->user_crt_id = Auth::user()->id;
                            $dtx->created_at = date('Y-m-d H:i:s');
                            $dtx->tx_date = date('Y-m-d H:i:s');
                            $dtx->save();

                        }
                    // }
                }

                // KREDIT
                foreach ($multi_coa_no as $key => $vtx) {

                    $note = '';
                    if ($multi_ket[$key]) {
                        $note = $multi_ket[$key];
                    }

                   

                        if ($multi_kredit[$key] != '') {
                            $get_bdd_tx = collect(\DB::select("SELECT max(id::int) as max_id FROM master_bdd_txadd"))->first();
                            $dtx = new BDDTxModel();
                            $dtx->id = $get_bdd_tx->max_id+1;
                            $dtx->bdd_id = $data->id;
                            $dtx->acc_no = $multi_coa_no[$key];
                            $dtx->amount = ($multi_kredit[$key] == '') ? 0 : $this->clearSeparatorDouble($multi_kredit[$key]);
                            $dtx->tx_type_id = '1';
                            $dtx->note = $note;
                            $dtx->user_crt_id = Auth::user()->id;
                            $dtx->created_at = date('Y-m-d H:i:s');
                            $dtx->tx_date = date('Y-m-d H:i:s');
        
                            $dtx->save();
                        }

                    

                
                }
            }



            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function refinventory(Request $request)
    {

        $id = $request->input('id');
        $data = \DB::select("SELECT * FROM ref_inventory where id = '".$id."'");
        return json_encode($data);
    }

    public function form_biaya_ditangguhkan($id)
    {
        if ($id == 'create') {
            $param['tittle'] = 'Add Prepaid Expense';
            $param['type'] = 'create';
            $param['get_id'] = '';

        }else {
            $param['get_id'] = $id;
            $param['type'] = 'edit';
            $param['tittle'] = 'Edit Prepaid Expense';
            $mi = collect(\DB::select("SELECT * FROM master_bdd
            left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
            where master_bdd.id = '".$id."'"))->first();
            $mc = \DB::select("SELECT * FROM master_bdd_schedule where bdd_id = ".$id." order by last_os desc");
            $mc_y_max = collect(\DB::select("SELECT max(year) as year_max FROM master_bdd_schedule where bdd_id = ".$id))->first();
            $mc_y_min = collect(\DB::select("SELECT min(year) as year_min FROM master_bdd_schedule where bdd_id = ".$id))->first();

            $mtx = \DB::select("SELECT
            acc_no , 
            note ,
            sum(case when tx_type_id = 0 then amount else 0 end) as debet,
            sum(case when tx_type_id = 1 then amount else 0 end) as kredit
          from
            master_bdd_txadd mbt
          where
            bdd_id = ".$id."
            group by acc_no , bdd_id , note");

            
            $param['mi'] = $mi;
            $param['mc'] = $mc;
            $param['mtx'] = $mtx;

            $param['mc_y_max'] = $mc_y_max;
            $param['mc_y_min'] = $mc_y_min;


        }


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'biaya_ditangguhkan.form',$param);
        }else {
            return view('master.master')->nest('child', 'biaya_ditangguhkan.form',$param);
        }
    }

    public function biaya_ditangguhkan_detail(Request $request,$id)
    {
        $mi = collect(\DB::select("SELECT master_bdd.*,ref_bdd.*,muc.fullname as membuat,mua.fullname as menyetujui FROM master_bdd
        left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
        left join master_user muc on muc.id = master_bdd.user_crt_id
        left join master_user mua on mua.id = master_bdd.user_upd_id

        where master_bdd.id = '".$id."'"))->first();
        $mc = \DB::select("SELECT * FROM master_bdd_schedule where bdd_id = ".$id." order by last_os desc");

        $mtx = \DB::select("SELECT * FROM master_bdd_txadd
        join master_coa on master_coa.coa_no = master_bdd_txadd.acc_no where bdd_id = ".$id." order by master_bdd_txadd.id");

        $param['mi'] = $mi;
        $param['mc'] = $mc;
        $param['mtx'] = $mtx;

        $param['type'] = $request->get('type');
        $param['getId'] = $id;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'biaya_ditangguhkan.detail',$param);
        }else {
            return view('master.master')->nest('child', 'biaya_ditangguhkan.detail',$param);
        }
    }


    public function data()
    {
       $data = \DB::select("SELECT *, master_bdd.id as mid
       FROM master_bdd left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
       left join master_branch on master_branch.id = master_bdd.branch_id
       where id_workflow = 1 and master_bdd.branch_id=".Auth::user()->branch_id." and master_bdd.company_id=".Auth::user()->company_id." order by master_bdd.created_at desc");
       return DataTables::of($data)
       ->addColumn('other_amount', function ($data) {

            $other_amount = 0;
            $txadd = \DB::select("SELECT * FROM master_bdd_txadd where tx_type_id = 0 and bdd_id = ".$data->mid);
            foreach ($txadd as $key => $v) {
                $other_amount += $v->amount;
            }

            return $this->numFormat($other_amount);
        })
       ->addColumn('action', function ($data) {

        $get_memo = $this->getListMemo($data->mid,'master_bdd');

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" onclick="loadNewPage(`'.route('biaya_ditangguhkan.form',$data->mid).'?type=new`)">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>
              <a class="dropdown-item" onClick="loadNewPage(`'.route('biaya_ditangguhkan.detail',$data->mid).'?type=new`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
              <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
              <i class="la la-sticky-note"></i>
              <span>Memo</span>
          </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('bdd_amount',function($data) {
            return number_format($data->bdd_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_approve()
    {
       $data = \DB::select("SELECT *, master_bdd.id as mid
       FROM master_bdd left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
       left join master_branch on master_branch.id = master_bdd.branch_id
       where id_workflow = 2 and master_bdd.branch_id=".Auth::user()->branch_id." and master_bdd.company_id=".Auth::user()->company_id." order by master_bdd.created_at desc");
       return DataTables::of($data)
       ->addColumn('other_amount', function ($data) {

            $other_amount = 0;
            $txadd = \DB::select("SELECT * FROM master_bdd_txadd where tx_type_id = 0 and bdd_id = ".$data->mid);
            foreach ($txadd as $key => $v) {
                $other_amount += $v->amount;
            }

            return $this->numFormat($other_amount);
        })

       ->addColumn('action', function ($data) {

        $get_memo = $this->getListMemo($data->mid,'master_bdd');

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">

              <a class="dropdown-item" onClick="loadNewPage(`'.route('biaya_ditangguhkan.detail',$data->mid).'?type=approve`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
              <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
              <i class="la la-sticky-note"></i>
              <span>Memo</span>
          </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('bdd_amount',function($data) {
            return number_format($data->bdd_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_success()
    {
       $data = \DB::select("SELECT *, master_bdd.id as mid
       FROM master_bdd left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
       left join master_branch on master_branch.id = master_bdd.branch_id
       left join master_coa on master_coa.id = master_bdd.coa_acc_no
       where id_workflow = 9 and master_bdd.branch_id=".Auth::user()->branch_id." and master_bdd.company_id=".Auth::user()->company_id." order by master_bdd.created_at desc");
       return DataTables::of($data)
       ->addColumn('other_amount', function ($data) {

            $other_amount = 0;
            $txadd = \DB::select("SELECT * FROM master_bdd_txadd where tx_type_id = 0 and bdd_id = ".$data->mid);
            foreach ($txadd as $key => $v) {
                $other_amount += $v->amount;
            }

            return $this->numFormat($other_amount);
        })

       ->addColumn('action', function ($data) {
        $get_memo = $this->getListMemo($data->mid,'master_bdd');

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">

              <a class="dropdown-item" onClick="loadNewPage(`'.route('biaya_ditangguhkan.detail',$data->mid).'?type=success`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
              <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                    <i class="la la-sticky-note"></i>
                    <span>Memo</span>
                </a>
                <a class="dropdown-item" onClick="cetakTransaksi('.$data->mid.',`bdd`);" >
                <i class="la la-sticky-note"></i>
                <span>Cetak Bukti Transaksi</span>
            </a>
            </div>
        </div>
        ';
        })
        ->addColumn('paid', function ($data) {
            $count_sc = collect(\DB::select("select count(id) as c from master_bdd_schedule where bdd_id = ".$data->mid))->first();
            $count_sc_paid = collect(\DB::select("select count(id) as c from master_bdd_schedule where paid_status_id = 1 and bdd_id = ".$data->mid))->first();

             return ' <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                            '.$count_sc_paid->c." / ".$count_sc->c.'
                        </span>';
            })
        ->addColumn('paid_amount', function ($data) {
            $count_sc = collect(\DB::select("select count(id) as c from master_bdd_schedule where bdd_id = ".$data->mid))->first();
            $count_sc_paid = collect(\DB::select("select count(id) as c from master_bdd_schedule where paid_status_id = 1 and bdd_id = ".$data->mid))->first();

            $count_paid = \DB::selectOne("select sum(amor_amount) as jumlah from master_bdd_schedule where paid_status_id = 1 and bdd_id = ".$data->mid);

            // return number_format($count_sc_paid->c * $data->amor_amount,0,',','.');
            return number_format($count_paid->jumlah,0,',','.');

            })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('bdd_amount',function($data) {
            return number_format($data->bdd_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek','action','paid'])
        ->make(true);

    }


    public function data_reversal()
    {
        $data = \DB::select("SELECT *, master_bdd.id as mid
        FROM master_bdd left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
        left join master_branch on master_branch.id = master_bdd.branch_id
        where id_workflow = 3 and master_bdd.branch_id=".Auth::user()->branch_id." and master_bdd.company_id=".Auth::user()->company_id." order by master_bdd.created_at desc");
        return DataTables::of($data)
        ->addColumn('other_amount', function ($data) {

            $other_amount = 0;
            $txadd = \DB::select("SELECT * FROM master_bdd_txadd where tx_type_id = 0 and bdd_id = ".$data->mid);
            foreach ($txadd as $key => $v) {
                $other_amount += $v->amount;
            }

            return $this->numFormat($other_amount);
        })

        ->addColumn('action', function ($data) {

            $get_memo = $this->getListMemo($data->mid,'master_bdd');

         return '
         <div class="dropdown dropdown-inline">
             <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="flaticon-more"></i>
             </button>
             <div class="dropdown-menu dropdown-menu-right">

               <a class="dropdown-item" onClick="loadNewPage(`'.route('biaya_ditangguhkan.detail',$data->mid).'?type=success`)">
                   <i class="la la-clipboard"></i>
                   <span>Detail</span>
               </a>
               <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
               <i class="la la-sticky-note"></i>
               <span>Memo</span>
           </a>
             </div>
         </div>
         ';
         })
         ->addColumn('cek', function ($data) {
             return '
             <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                 <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                 <span></span>
             </label>
             ';
             })
         ->editColumn('bdd_amount',function($data) {
             return number_format($data->bdd_amount,0,',','.');
         })
         ->editColumn('amor_amount',function($data) {
             return number_format($data->amor_amount,0,',','.');
         })
         ->editColumn('is_active',function($data) {
             return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
         })
         ->rawColumns(['cek', 'action'])
         ->make(true);

    }

    public function data_schedule()
    {
        $data = \DB::select("SELECT mi.id as mid,coa_name, mi.amor_amount, mi.last_os,mi.month,mi.year, master_bdd.bdd_note
        FROM master_bdd_schedule mi
        left join master_bdd on master_bdd.id = mi.bdd_id
        left join master_coa on master_coa.id = master_bdd.coa_acc_no
        where mi.id_workflow = 2 order by bdd_note,mi.last_os desc");
       return DataTables::of($data)
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('month',function($data) {
            return $this->MonthIndoNew($data->month);
        })
        ->editColumn('last_os',function($data) {
            return number_format($data->last_os,0,',','.');
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_schedule_cancel()
    {
        $data = \DB::select("SELECT mi.id as mid,coa_name, mi.amor_amount, mi.last_os,mi.month,mi.year, master_bdd.bdd_note
       FROM master_bdd_schedule mi
       left join master_bdd on master_bdd.id = mi.bdd_id
       left join master_coa on master_coa.id = master_bdd.coa_acc_no
       where mi.id_workflow = 14 order by bdd_note,mi.last_os desc");
       return DataTables::of($data)
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('month',function($data) {
            return $this->MonthIndoNew($data->month);
        })
        ->editColumn('last_os',function($data) {
            return number_format($data->last_os,0,',','.');
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);


    }

    public function data_deleted()
    {
        $data = \DB::select("SELECT *, master_bdd.id as mid
        FROM master_bdd left join ref_bdd on ref_bdd.id = master_bdd.bdd_id
        left join master_branch on master_branch.id = master_bdd.branch_id
        where id_workflow = 12 and master_bdd.branch_id=".Auth::user()->branch_id." and master_bdd.company_id=".Auth::user()->company_id." order by master_bdd.created_at desc");
        return DataTables::of($data)
        ->addColumn('other_amount', function ($data) {

            $other_amount = 0;
            $txadd = \DB::select("SELECT * FROM master_bdd_txadd where tx_type_id = 1 and bdd_id = ".$data->mid);
            foreach ($txadd as $key => $v) {
                $other_amount += $v->amount;
            }

            return $this->numFormat($other_amount);
        })

        ->addColumn('action', function ($data) {

            $get_memo = $this->getListMemo($data->mid,'master_bdd');

         return '
         <div class="dropdown dropdown-inline">
             <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="flaticon-more"></i>
             </button>
             <div class="dropdown-menu dropdown-menu-right">

               <a class="dropdown-item" onClick="loadNewPage(`'.route('biaya_ditangguhkan.detail',$data->mid).'?type=success`)">
                   <i class="la la-clipboard"></i>
                   <span>Detail</span>
               </a>
               <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
               <i class="la la-sticky-note"></i>
               <span>Memo</span>
           </a>
             </div>
         </div>
         ';
         })
         ->addColumn('cek', function ($data) {
             return '
             <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                 <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                 <span></span>
             </label>
             ';
             })
         ->editColumn('bdd_amount',function($data) {
             return number_format($data->bdd_amount,0,',','.');
         })
         ->editColumn('amor_amount',function($data) {
             return number_format($data->amor_amount,0,',','.');
         })
         ->editColumn('is_active',function($data) {
             return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
         })
         ->rawColumns(['cek', 'action'])
         ->make(true);

    }




    public function kirimAproval(Request $request){
        $this->actSendApproval('master_bdd',$request->get('type'),$request->value,'');
    }

    public function giveAproval(Request $request){
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
            $this->actGiveApproval('master_bdd',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function hapusAproval(Request $request){
        $this->actDeleteApproval('master_bdd',$request->get('type'),$request->value);
    }

    public function rejectAproval(Request $request){
        $this->actRejectApproval('master_bdd',$request->get('type'),$request->value);
    }

    // SCHEDULE
    public function kirimAprovalSc(Request $request){
        $this->actSendApproval('master_bdd_schedule',$request->get('type'),$request->value,$request->get('idtype'));
    }

    public function giveAprovalSc(Request $request){
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
        $this->actGiveApproval('master_bdd_schedule',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function rejectAprovalSc(Request $request){
        $this->actRejectApproval('master_bdd_schedule',$request->get('type'),$request->value,$request->SetMemo);
    }

    public function CancelPaySc(Request $request)
    {
        $this->actCancelPaySc('master_bdd_schedule',$request->get('type'),$request->value);
    }

    public function GiveApprovalCancelPaySc(Request $request)
    {
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
        $this->actGiveApprovalCancelPaySc('master_bdd_schedule',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function RejectCancelPayment(Request $request)
    {
        $this->actRejectCancelPayment('master_bdd_schedule',$request->get('type'),$request->value);
    }

    // DELETE REVERSAL
    public function hapusAprovalRev(Request $request){

        if ($request->get('type') == 'semua') {
            $get_sc = \DB::select("SELECT bdd_note FROM master_bdd_schedule
            left join master_bdd on master_bdd.id = master_bdd_schedule.bdd_id where paid_status_id = 1
            and master_bdd.id_workflow = 9
            GROUP BY bdd_note");
        }else {
            $getId = $request->value;
            $getWhere = '';
            $i = 0;
            $len = count($getId);

            foreach ($getId as $item) {

                // dd($item);

                // if ($key > 0) {
                    if ($i == $len - 1) {
                        $getWhere .= $item;
                    }else{
                        if($item != null){
                            $getWhere .= $item.",";
                        }

                    }

                // }
                $i++;
            }

            $get_sc = \DB::select("SELECT bdd_note FROM master_bdd_schedule
            left join master_bdd on master_bdd.id = master_bdd_schedule.bdd_id where paid_status_id = 1
            and master_bdd.id_workflow = 9 and master_bdd.id in (".$getWhere.")
            GROUP BY bdd_note ");

        }

        if ($get_sc) {
            $text_warning = 'Tidak Bisa Melakukan Aksi Delete, Terdapat Status Paid Pada Prepaid Expense Schedule : ';

            foreach ($get_sc as $key => $value) {
                $text_warning .= '<br> '.($key+1).'. '.$value->bdd_note;
            }
            return response()->json([
                'rc' => 88,
                'rm' => $text_warning
            ]);
        }else{
            $this->actDeleteApprovalRev('master_bdd',$request->get('type'),$request->value);
        }

    }
    public function giveHapusAprovalRev(Request $request){
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
         $this->actGiveDeleteApprovalRev('master_bdd',$request->get('type'),$request->value,$request->SetMemo);
        }
    }
    public function rejectHapusAprovalRev(Request $request){
        $this->actRejectDeleteApprovalRev('master_bdd',$request->get('type'),$request->value);
    }


}
