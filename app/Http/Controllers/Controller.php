<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\Models\MasterTxModel;
use Auth;
use App\Models\InventoryModel;
use App\Models\InventoryScModel;
use App\Models\BDDModel;
use App\Models\BDDScModel;
use App\Models\SalesModel;
use App\Models\SalesScModel;
use App\Models\SalesPolisModel;
use App\Models\SalesPolisScModel;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Datetime;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function sendMail($view,$data)
    {

        Mail::send($view, $data, function ($message) use ($data) {
            $message->from('official@ata.co.id', 'ATA');
            $message->to($data['sendto']);
            $message->subject($data['subject']);
            $message->attach($data['path']);
        });
    }

    public function numFormat($nominal)
    {
        return number_format($nominal,2,",",".");
    }

    public function clearSeparator($nominal)
    {
        return str_replace('.','',$nominal);
    }

    public function clearSeparatorDouble($nominal)
    {
        if($nominal){
            $nom = str_replace('.','',$nominal);
            $nom = str_replace(',','.',$nom);
        }else{
            $nom = 0;
        }

        return $nom;
    }

    public function convertDate($date)
    {
        return date('Y-m-d',strtotime($date));
    }


    public function MonthIndo($data)
    {
        $bulan = array (
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );

        return $bulan[$data];
    }

    public function MonthIndoNew($data)
    {
        $bulan = array (
            '1' => 'Januari',
            '2' => 'Februari',
            '3' => 'Maret',
            '4' => 'April',
            '5' => 'Mei',
            '6' => 'Juni',
            '7' => 'Juli',
            '8' => 'Agustus',
            '9' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );
        return $bulan[$data];
    }

    public function endSession()
    {
        Auth::logout();
        // return redirect()->route('login');
        return response()->json([
            'rc' => 0,
            'rm' => "sukses"
        ]);
    }


    public function actSendApproval($setTable,$type,$datas,$idtype){

        if($type=='semua'){

            if($setTable == 'master_bdd_schedule'){
                $results = \DB::select("select * from ".$setTable." where bdd_id = ".$idtype." and id_workflow in (1,3)");
            }else {
                $results = \DB::select("select * from ".$setTable." where id_workflow in (1,3)");
            }

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 2]);
            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 2]);

            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }



    public function actGiveApproval($setTable,$type,$datas,$memo){

            if($type=='semua'){
                if ($setTable == 'master_inventory_schedule') {
                    $results = \DB::select("select master_inventory_schedule.id from ".$setTable."
                    join master_inventory mi on mi.id = master_inventory_schedule.inventory_id
                    where mi.branch_id =".Auth::user()->branch_id." and master_inventory_schedule.id_workflow in (2)");
                }else{
                    $results = \DB::select("select * from ".$setTable." where id_workflow in (2)");
                }


                foreach ($results as $item) {

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 9,]);

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['user_upd_id' => Auth::user()->id]);

                    if ($setTable == 'master_sales') {
                        $getTxCode = "";
                    }else {
                        $getTxCode = $this->setJurnalData($item->id,$setTable);
                    }

                    // SET MEMO
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                    DB::table('master_memo')->insert(
                        [
                            'id' => $get->max_id+1,
                            'tx_code' => $getTxCode,
                            'notes' => $memo,
                            'ref_code' => $item->id,
                            'ref_code_type' => $setTable,
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:i:s'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );

                    // UDPATE STATUS PAID INVENTORY
                    if ($setTable == 'master_inventory_schedule') {
                        DB::table('master_inventory_schedule')
                        ->where('id', $item->id)
                        ->update(['paid_status_id' => 1]);
                    }
                    // UDPATE STATUS PAID BDD
                    elseif ($setTable == 'master_bdd_schedule') {
                        DB::table('master_bdd_schedule')
                        ->where('id', $item->id)
                        ->update(['paid_status_id' => 1]);
                    }
                     // UDPATE STATUS PAID INSTALLMENT
                     elseif ($setTable == 'master_sales_schedule') {
                        DB::table('master_sales_schedule')
                        ->where('id', $item->id)
                        ->update(['paid_status_id' => 1]);

                        $getKwitansi = $this->generateKwitansi();

                        DB::table('master_sales_schedule')
                        ->where('id', $item->id)
                        ->update(['kwitansi_no' => $getKwitansi]);
                    }

                }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);

            }else{
                foreach ($datas as $item) {

                    if($item != null){
                        DB::table($setTable)
                        ->where('id', $item)
                        ->update(['id_workflow' => 9]);

                        DB::table($setTable)
                        ->where('id', $item)
                        ->update(['user_upd_id' => Auth::user()->id]);

                        if ($setTable == 'master_sales') {
                            $getTxCode = "";
                        }else {
                            $getTxCode = $this->setJurnalData($item,$setTable);
                        }



                        // SET MEMO
                        $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                        DB::table('master_memo')->insert(
                            [
                                'id' => $get->max_id+1,
                                'tx_code' => $getTxCode,
                                'notes' => $memo,
                                'ref_code' => $item,
                                'ref_code_type' => $setTable,
                                'id_workflow' => 9,
                                'created_at' => date('Y-m-d H:i:s'),
                                'user_crt_id' => Auth::user()->id
                            ]
                        );

                        // UDPATE STATUS PAID
                        if ($setTable == 'master_inventory_schedule') {
                            DB::table('master_inventory_schedule')
                            ->where('id', $item)
                            ->update(['paid_status_id' => 1]);
                        }
                        // UDPATE STATUS PAID BDD
                        elseif ($setTable == 'master_bdd_schedule') {
                            DB::table('master_bdd_schedule')
                            ->where('id', $item)
                            ->update(['paid_status_id' => 1]);
                        }
                        // UDPATE STATUS PAID INSTALLMENT
                        elseif ($setTable == 'master_sales_schedule') {
                            DB::table('master_sales_schedule')
                            ->where('id', $item)
                            ->update(['paid_status_id' => 1]);

                            $getKwitansi = $this->generateKwitansi();
                            DB::table('master_sales_schedule')
                            ->where('id', $item)
                            ->update(['kwitansi_no' => $getKwitansi]);
                        }
                    }



                }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);
            }

    }

    public function generateKwitansi()
    {
        $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

        // INVOICE NO
        $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();
        $str=strlen($ref_invoice->next_no);
        if($str==3){
        $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
        }elseif($str==2){
        $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
        }else{
            $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
        }

        DB::table('ref_invoice_no')
        ->where('company_id', Auth::user()->company_id)
        ->where('year', date('Y'))
        ->where('type',1)
        ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

        return $no_invoice;
    }

    public function actGiveApprovalSplit($setTable,$type,$datas,$memo){

        if($type=='semua'){

            $setStatus = ['is_agent_paid','is_company_paid','is_premi_paid'];

            for ($i=0; $i <= 2; $i++) {

                $results = \DB::select("select * from ".$setTable." where ".$setStatus[$i]."=2");

                foreach ($results as $item) {
                    DB::table('master_sales_schedule')
                    ->where('id',  $item->id)
                    ->update([$setStatus[$i] => 9]);

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['user_upd_id' => Auth::user()->id]);


                    $getTxCode = $this->setJurnalDataSplit($item->id,$setStatus[$i]);

                    $getKwitansi = $this->generateKwitansi();

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['kwitansi_no' => $getKwitansi]);


                    // $getTxCode = '';
                    // SET MEMO
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                    DB::table('master_memo')->insert(
                        [
                            'id' => $get->max_id+1,
                            'tx_code' => $getTxCode,
                            'notes' => $memo,
                            'ref_code' => $item->id,
                            'ref_code_type' => $setTable,
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:i:s'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );

                }

            }



        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

                $split_data = explode("|",$item);

                if ($split_data[1] == 'agent') {
                    $type_split = 'is_agent_paid';
                }elseif ($split_data[1] == 'commision') {
                    $type_split = 'is_company_paid';
                }elseif ($split_data[1] == 'premium') {
                    $type_split = 'is_premi_paid';
                }

            DB::table($setTable)
                ->where('id', $split_data[0])
                ->update([$type_split => 9]);

                DB::table($setTable)
                ->where('id', $split_data[0])
                ->update(['user_upd_id' => Auth::user()->id]);

                $getTxCode = $this->setJurnalDataSplit($split_data[0],$type_split);

                $getKwitansi = $this->generateKwitansi();

                DB::table($setTable)
                ->where('id', $split_data[0])
                ->update(['kwitansi_no' => $getKwitansi]);

                // $getTxCode = '';
                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'ref_code' => $split_data[0],
                        'ref_code_type' => $setTable,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }

    }

    public function actGiveApprovalSplitRev($setTable,$type,$datas,$memo){

        if($type=='semua'){

            $setStatus = ['is_agent_paid','is_company_paid','is_premi_paid'];

            for ($i=0; $i <= 2; $i++) {

                $results = \DB::select("select * from ".$setTable." where ".$setStatus[$i]."=3");

                foreach ($results as $item) {
                    DB::table('master_sales_schedule')
                    ->where('id',  $item->id)
                    ->update([$setStatus[$i] => null]);

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['user_upd_id' => Auth::user()->id]);


                    $getTxCode = $this->setJurnalDataSplitRev($item->id,$setStatus[$i]);

                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                    DB::table('master_memo')->insert(
                        [
                            'id' => $get->max_id+1,
                            'tx_code' => $getTxCode,
                            'notes' => $memo,
                            'ref_code' => $item->id,
                            'ref_code_type' => $setTable,
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:i:s'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );

                }

            }



        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

                $split_data = explode("|",$item);

                if ($split_data[1] == 'agent') {
                    $type_split = 'is_agent_paid';
                }elseif ($split_data[1] == 'commision') {
                    $type_split = 'is_company_paid';
                }elseif ($split_data[1] == 'premium') {
                    $type_split = 'is_premi_paid';
                }

            DB::table($setTable)
                ->where('id', $split_data[0])
                ->update([$type_split => null]);

                DB::table($setTable)
                ->where('id', $split_data[0])
                ->update(['user_upd_id' => Auth::user()->id]);

                $getTxCode = $this->setJurnalDataSplitRev($split_data[0],$type_split);

                // $getTxCode = '';
                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'ref_code' => $split_data[0],
                        'ref_code_type' => $setTable,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }

    }

    public function actDeleteApproval($setTable,$type,$datas){
        if($type=='semua'){

            $results = \DB::select("select * from ".$setTable." where id_workflow in (1,3) ");

            foreach ($results as $item) {

                if ($setTable == 'master_inventory') {
                    InventoryModel::destroy($item->id);
                    InventoryScModel::where('inventory_id', $item->id)->delete();
                }elseif ($setTable == 'master_sales' || $setTable == 'master_sales_schedule') {
                    SalesModel::destroy($item->id);
                    SalesScModel::where('id_master_sales', $item->id)->delete();
                } else {
                    BDDModel::destroy($item->id);
                    BDDScModel::where('bdd_id', $item->id)->delete();
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($datas as $item) {
                if ($setTable == 'master_inventory') {
                    InventoryModel::destroy($item);
                    InventoryScModel::where('inventory_id', $item)->delete();
                }elseif ($setTable == 'master_sales') {
                    SalesModel::destroy($item);
                    SalesScModel::where('id_master_sales', $item)->delete();
                }else {
                    BDDModel::destroy($item);
                    BDDScModel::where('bdd_id', $item)->delete();
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function actDeleteApprovalRev($setTable,$type,$datas){
        if($type=='semua'){

            $results = \DB::select("select * from ".$setTable." where id_workflow in (9) ");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 3]);
            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {
                DB::table($setTable)
                 ->where('id', $item)
                 ->update(['id_workflow' => 3]);
             }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actGiveDeleteApprovalRev($setTable,$type,$datas,$memo){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (3)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 12]);

                // if ($setTable == 'master_sales' || $setTable == 'master_sales_schedule') {
                //     $getTxCode = 0;
                // }else {
                    $getTxCode = $this->setJurnalDataReversal($item->id,$setTable);
                // }

                // $getTxCode = $this->setJurnalDataReversal($item->id,$setTable);

                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'ref_code' => $item->id,
                        'ref_code_type' => $setTable,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );

                // UDPATE STATUS PAID
                if ($setTable == 'master_inventory_schedule') {
                    DB::table('master_inventory_schedule')
                    ->where('id',  $item->id)
                    ->update(['paid_status_id' => 0]);
                }

                 // UDPATE STATUS PAID BDD
                 if ($setTable == 'master_bdd_schedule') {
                    DB::table('master_bdd_schedule')
                    ->where('id',  $item->id)
                    ->update(['paid_status_id' => 0]);
                }

                 // UDPATE STATUS PAID BDD
                 if ($setTable == 'master_sales_schedule') {
                    DB::table('master_sales_schedule')
                    ->where('id',  $item->id)
                    ->update(['paid_status_id' => 0]);
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 12]);

                // if ($setTable == 'master_sales' || $setTable == 'master_sales_schedule') {
                //     $getTxCode = 0;
                // }else {
                    $getTxCode = $this->setJurnalDataReversal($item,$setTable);
                // }

                // $this->setJurnalDataReversal($item,$setTable);

                 // SET MEMO
                 $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                 DB::table('master_memo')->insert(
                     [
                         'id' => $get->max_id+1,
                         'tx_code' => $getTxCode,
                         'notes' => $memo,
                         'ref_code' => $item,
                         'ref_code_type' => $setTable,
                         'id_workflow' => 9,
                         'created_at' => date('Y-m-d H:i:s'),
                         'user_crt_id' => Auth::user()->id
                     ]
                 );


                 // UDPATE STATUS PAID
                 if ($setTable == 'master_inventory_schedule') {
                    DB::table('master_inventory_schedule')
                    ->where('id', $item)
                    ->update(['paid_status_id' => 0]);
                }

                 // UDPATE STATUS PAID BDD
                 if ($setTable == 'master_bdd_schedule') {
                    DB::table('master_bdd_schedule')
                    ->where('id', $item)
                    ->update(['paid_status_id' => 0]);
                }
                if ($setTable == 'master_sales_schedule') {
                    DB::table('master_sales_schedule')
                    ->where('id',  $item)
                    ->update(['paid_status_id' => 0]);
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actRejectDeleteApprovalRev($setTable,$type,$datas){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (3)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 9]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 9]);

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actRejectApproval($setTable,$type,$datas,$memo){



        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (2)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 1]);

                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => "",
                        'notes' => $memo,
                        'ref_code' => $item->id,
                        'ref_code_type' => $setTable,
                        'id_workflow' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 1]);


                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => "",
                        'notes' => $memo,
                        'ref_code' => $item,
                        'ref_code_type' => $setTable,
                        'id_workflow' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actRejectApprovalSplit($setTable,$type,$datas,$memo){

        if($type=='semua'){

            $setStatus = ['is_agent_paid','is_company_paid','is_premi_paid'];

            for ($i=0; $i <= 2; $i++) {

                $results = \DB::select("select * from ".$setTable." where ".$setStatus[$i]."=2");

                foreach ($results as $item) {
                    DB::table('master_sales_schedule')
                    ->where('id',  $item->id)
                    ->update([$setStatus[$i] => null]);

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['user_upd_id' => Auth::user()->id]);

                    $getTxCode = '';
                    // SET MEMO
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                    DB::table('master_memo')->insert(
                        [
                            'id' => $get->max_id+1,
                            'tx_code' => $getTxCode,
                            'notes' => $memo,
                            'ref_code' => $item->id,
                            'ref_code_type' => $setTable,
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:i:s'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );

                }

            }



        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

                $split_data = explode("|",$item);

                if ($split_data[1] == 'agent') {
                    $type_split = 'is_agent_paid';
                }elseif ($split_data[1] == 'commision') {
                    $type_split = 'is_company_paid';
                }elseif ($split_data[1] == 'premium') {
                    $type_split = 'is_premi_paid';
                }

            DB::table($setTable)
                ->where('id', $split_data[0])
                ->update([$type_split => null]);

                DB::table($setTable)
                ->where('id', $split_data[0])
                ->update(['user_upd_id' => Auth::user()->id]);

                $getTxCode = '';
                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'ref_code' => $split_data[0],
                        'ref_code_type' => $setTable,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }

    }


    public function actRejectApprovalSplitRev($setTable,$type,$datas,$memo){

        if($type=='semua'){

            $setStatus = ['is_agent_paid','is_company_paid','is_premi_paid'];

            for ($i=0; $i <= 2; $i++) {

                $results = \DB::select("select * from ".$setTable." where ".$setStatus[$i]."=3");

                foreach ($results as $item) {
                    DB::table('master_sales_schedule')
                    ->where('id',  $item->id)
                    ->update([$setStatus[$i] => 9]);

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['user_upd_id' => Auth::user()->id]);

                    $getTxCode = '';
                    // SET MEMO
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                    DB::table('master_memo')->insert(
                        [
                            'id' => $get->max_id+1,
                            'tx_code' => $getTxCode,
                            'notes' => $memo,
                            'ref_code' => $item->id,
                            'ref_code_type' => $setTable,
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:i:s'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );

                }

            }



        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

                $split_data = explode("|",$item);

                if ($split_data[1] == 'agent') {
                    $type_split = 'is_agent_paid';
                }elseif ($split_data[1] == 'commision') {
                    $type_split = 'is_company_paid';
                }elseif ($split_data[1] == 'premium') {
                    $type_split = 'is_premi_paid';
                }

            DB::table($setTable)
                ->where('id', $split_data[0])
                ->update([$type_split => 9]);

                DB::table($setTable)
                ->where('id', $split_data[0])
                ->update(['user_upd_id' => Auth::user()->id]);

                $getTxCode = '';
                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'ref_code' => $split_data[0],
                        'ref_code_type' => $setTable,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }

    }


    public function actRejectCancelPayment($setTable,$type,$datas){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (14)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 9]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 9]);

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function getListMemo($id,$type)
    {
        $data_memo = \DB::select("select * from master_memo where ref_code = ".$id." and ref_code_type = '".$type."' order by created_at desc");

        $list_memo = '';
        foreach ($data_memo as $key => $value) {
            $list_memo .= " <div class='kt-list-timeline__item'>
                <span class='kt-list-timeline__badge kt-list-timeline__badge--success'></span>
                <span class='kt-list-timeline__text'>".$value->notes."</span>
                <span class='kt-list-timeline__time'>".date("d M Y H:i:s", strtotime($value->created_at))."</span>
            </div>";
        }

        $text = "<div class='kt-list-timeline'>
                    <div class='kt-list-timeline__items'>
                        ".$list_memo."
                    </div>
                </div>";

        $action = "<button onclick='znIconboxClose()' type='button' class='btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm'>Close</button>";

        return array($text,$action);
    }


    public function setJurnalData($item,$setTable)
    {
        $set_month = '';
        $set_year = '';


        if ($setTable == 'master_inventory_schedule') {
            $gData = collect(\DB::select("select * from master_inventory_schedule where id = ".$item))->first();
            $mInventory = collect(\DB::select("select * from master_inventory where id = ".$gData->inventory_id))->first();
            $invoice_type_id = 5;
            $set_invoice_id = $mInventory->inventory_type_id;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";
            $set_month = $this->MonthIndoNew($gData->month);
            $set_year = $gData->year;
            $id_rel = $mInventory->id;
            $user_create = $gData->user_crt_id;

        }elseif ($setTable == 'master_inventory') {
            $gData = collect(\DB::select("select * from master_inventory where id = ".$item))->first();
            $invoice_type_id = 3;
            $set_invoice_id = $gData->inventory_type_id;
            $set_tx_amount = $gData->purchase_amount;
            $set_tx_notes = $gData->inventory_desc;
            $id_rel = $gData->id;
            $user_create = $gData->user_crt_id;

        }elseif ($setTable == 'master_bdd') {
            $gData = collect(\DB::select("select * from master_bdd where id = ".$item))->first();
            $invoice_type_id = 4;
            $set_tx_amount = $gData->bdd_amount;
            $set_tx_notes = $gData->bdd_note;
            $afi_acc_no = $gData->afi_acc_no;
            $coa_acc_no = $gData->coa_acc_no;
            $set_tax = $gData->tax_amount;
            $id_rel = $gData->id;
            $user_create = $gData->user_crt_id;

        }elseif ($setTable == 'master_bdd_schedule') {
            $gData = collect(\DB::select("select * from master_bdd_schedule where id = ".$item))->first();
            $mBdd = collect(\DB::select("select * from master_bdd where id = ".$gData->bdd_id))->first();

            $invoice_type_id = 6;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";
            $afi_acc_no = $mBdd->afi_acc_no;
            $coa_acc_no = $mBdd->coa_acc_no;

            $set_month = $this->MonthIndoNew($gData->month);
            $set_year = $gData->year;
            $id_rel = $mBdd->id;
            $user_create = $gData->user_crt_id;
        }elseif ($setTable == 'master_sales') {
            $gData = collect(\DB::select("select * from master_sales where id = ".$item))->first();
            $invoice_type_id = 1;
            $set_invoice_id = 0;
            $set_tx_amount = $gData->net_amount+$gData->polis_amount+$gData->materai_amount+$gData->admin_amount;
            $set_tx_notes = $gData->inv_no;
            $id_rel = $gData->id;
            $user_create = $gData->user_crt_id;
        }
        elseif ($setTable == 'master_sales_schedule') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 1;
            $set_invoice_id = 0;
            $set_tx_amount = $gData->net_amount+$gData->polis_amount+$gData->materai_amount+$gData->admin_amount;
            $set_tx_notes = $gData->inv_no;

            $set_month = $this->MonthIndoNew($gData->month);
            $set_year = $gData->year;
            $id_rel = $gData->id;
            $user_create = $gData->user_crt_id;
        }


        if ($setTable == 'master_inventory_schedule' || $setTable == 'master_inventory')
        {


            if ($setTable == 'master_inventory') {
                $ref_debet = collect(\DB::select("select * from ref_bank_account
                where id = ".$gData->afi_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->definition;
            } else {
                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            }


            $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();
        }
        elseif($setTable == 'master_bdd_schedule' || $setTable == 'master_bdd')
        {

            if ($setTable == 'master_bdd') {

                $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 "))->first();
                // $ref_kredit->tx_notes = $ref_kredit->coa_name;

                $ref_debet = collect(\DB::select("select * from ref_bank_account
                where id = ".$afi_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->definition;

            }else {
                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                    where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 "))->first();

                $ref_kredit = collect(\DB::select("select * from master_coa
                    where id = ".$coa_acc_no))->first();
                $ref_kredit->tx_notes = $ref_kredit->coa_name;
            }


            // $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            // where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 "))->first();

            // $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
            // where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 "))->first();

        }elseif ($setTable == 'master_sales_schedule' || $setTable == 'master_sales')
        {


            // if ($setTable == 'master_sales') {
                $ref_kredit = collect(\DB::select("select * from ref_bank_account
                where id = ".$gData->afi_acc_no))->first();
                $ref_kredit->tx_notes = $ref_kredit->definition;
                $ref_kredit->tx_notes = 'Payment Invoice No,';
            // } else {
            //     $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            //     where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            // }


            $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            $ref_debet->tx_notes = 'Payment Invoice No,';

        }


        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

        // SET TX CODE
        $txcode='';
        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".$systemDate->current_date."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
        $prx=date("ymd", strtotime($systemDate->current_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){
          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
        }else{
            $txcode=$prx.$prx_branch.$prx_company."0001";
        }

        $tx_code = $txcode;
        // $tx_code =date('Ymd').Auth::user()->company_id.Auth::user()->branch_id;

        // tahun/bulan/tanggal/id_company/id_branch/empat digit terakhir master tx

        // JIKA BDD TAX
        if ($setTable == 'master_bdd') {

            if ($gData->is_tax_company == 't') {
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();

                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;

                $dsc = new MasterTxModel();
                $dsc->id = $get->max_id+1;
                $dsc->tx_code = $tx_code;
                $dsc->id_rel = $id_rel;
                $dsc->type_tx = $setTable;
                $dsc->seq_approval = $set_last_seq_app;

                $dsc->tx_date = $systemDate->current_date;
                $dsc->tx_type_id = 1;

                $dsc->tx_amount = $set_tax;
                // DEBET

                    $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='200001'"))->first();
                    $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                    $dsc->coa_type_id = $coa->coa_type_id;
                    $dsc->acc_last = $coa->last_os;
                    $dsc->coa_id = '200001';

                    $dsc->tx_notes = $coa->coa_name.", ".$set_tx_notes;

                    if ($jurnalOp->operator_sign == '+') {
                        $dsc->acc_os = $coa->last_os + $set_tax;
                    }else {
                        $dsc->acc_os = $coa->last_os - $set_tax;
                    }

                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', '200001')
                    ->update(['last_os' => $dsc->acc_os]);

                    $dsc->tx_segment_id = 1;
                    $dsc->created_at = date('Y-m-d H:i:s');
                    $dsc->user_crt_id = $user_create;
                    $dsc->user_app_id = Auth::user()->id;
                    $dsc->id_workflow = 9;
                    $dsc->branch_id = Auth::user()->branch_id;
                    $dsc->company_id = 1;
                    $dsc->save();

            }

        }

        // JIKA BDD MULTI OVERBOOKING
        if ($setTable == 'master_bdd' || $setTable == 'master_inventory') {

            if ($gData->tambahan == '1') {

                if ($setTable == 'master_bdd') {

                    $txadd = \DB::select("SELECT * FROM master_bdd_txadd where bdd_id = ".$gData->id);
                }else{
                    $txadd = \DB::select("SELECT * FROM master_inventory_txadd where inventory_id = ".$gData->id);

                }



                $total_txadd = 0;


                foreach ($txadd as $ktx => $vtx) {
                    $total_txadd += $vtx->amount;

                     // DEBET
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;


                    $multitx = new MasterTxModel();
                    $multitx->id = $get->max_id+1;
                    $multitx->tx_code = $tx_code;
                    $multitx->id_rel = $id_rel;
                    $multitx->type_tx = $setTable;
                    $multitx->tx_date = $systemDate->current_date;
                    $multitx->tx_type_id = $vtx->tx_type_id;
                    $multitx->tx_amount = $vtx->amount;
                    $multitx->seq_approval = $set_last_seq_app;

                    $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$vtx->acc_no."'"))->first();
                    $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = ".$vtx->tx_type_id))->first();

                    $multitx->coa_type_id = $coa->coa_type_id;
                    $multitx->acc_last = $coa->last_os;
                    $multitx->coa_id = $vtx->acc_no;

                    $multitx->tx_notes = $coa->coa_name.", ".$set_tx_notes;

                    if ($jurnalOp->operator_sign == '+') {
                        $multitx->acc_os = $coa->last_os + $vtx->amount;
                    }else {
                        $multitx->acc_os = $coa->last_os - $vtx->amount;
                    }

                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', $vtx->acc_no)
                    ->update(['last_os' => $multitx->acc_os]);

                    $multitx->tx_segment_id = 1;
                    $multitx->created_at = date('Y-m-d H:i:s');
                    $multitx->user_crt_id = $user_create;
                    $multitx->user_app_id = Auth::user()->id;
                    $multitx->id_workflow = 9;
                    $multitx->branch_id = Auth::user()->branch_id;
                    $multitx->company_id = Auth::user()->company_id;
                    $multitx->save();
                }



            }

        }

        for ($i=0; $i <= 1; $i++) {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
            $set_last_seq_app = $last_seq_app->max_id+1;

            $dsc = new MasterTxModel();
            $dsc->id = $get->max_id+1;
            $dsc->tx_code = $tx_code;
            $dsc->id_rel = $id_rel;

            $dsc->type_tx = $setTable;

            $dsc->tx_date = $systemDate->current_date;
            $dsc->tx_type_id = $i;
            $dsc->seq_approval = $set_last_seq_app;


            // DEBET
            if ($i == 1) {

                if ($setTable == 'master_bdd') {

                    if ($gData->is_tax_company == 't') {
                        $set_tx_amount = $set_tx_amount - $set_tax;
                    }else{
                        $set_tx_amount = $set_tx_amount;
                    }

                    if ($gData->tambahan == '1') {
                        $set_tx_amount = $set_tx_amount;
                    }else{
                        $set_tx_amount = $set_tx_amount;
                    }

                    $dsc->tx_amount = $set_tx_amount;

                }else{
                    $dsc->tx_amount = $set_tx_amount;
                }

                // if ($setTable == 'master_bdd') {

                //     if ($gData->tambahan == '1') {
                //         $dsc->tx_amount = $set_tx_amount + $total_txadd;
                //     }else{
                //         $dsc->tx_amount = $set_tx_amount;
                //     }

                // }else{
                //     $dsc->tx_amount = $set_tx_amount;
                // }



                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_debet->coa_no."'"))->first();

                // if ($setTable == 'master_inventory_schedule'){
                //     $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();
                // }else {
                    $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();
                // }

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_debet->coa_no;


                $dsc->tx_notes = $ref_debet->tx_notes.", ".$set_tx_notes.$set_month." ".$set_year;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $dsc->tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $dsc->tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_debet->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            // KREDIT
            else{



                $dsc->tx_amount = $set_tx_amount;

                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_kredit->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_kredit->coa_no;

                $dsc->tx_notes = $ref_kredit->tx_notes.", ".$set_tx_notes.$set_month." ".$set_year;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_kredit->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }

            $dsc->tx_segment_id = 1;
            $dsc->created_at = date('Y-m-d H:i:s');
            $dsc->user_crt_id = $user_create;
            $dsc->user_app_id = Auth::user()->id;
            $dsc->id_workflow = 9;
            $dsc->branch_id = Auth::user()->branch_id;
            $dsc->company_id = 1;
            $dsc->save();
        }

        return $tx_code;
    }

    public function setJurnalDataReversal($item,$setTable)
    {


        if ($setTable == 'master_inventory_schedule') {
            $gData = collect(\DB::select("select * from master_inventory_schedule where id = ".$item))->first();
            $mInventory = collect(\DB::select("select * from master_inventory where id = ".$gData->inventory_id))->first();
            $invoice_type_id = 5;
            $set_invoice_id = $mInventory->inventory_type_id;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";

        }elseif ($setTable == 'master_inventory') {
            $gData = collect(\DB::select("select * from master_inventory where id = ".$item))->first();
            $invoice_type_id = 3;
            $set_invoice_id = $gData->inventory_type_id;
            $set_tx_amount = $gData->purchase_amount;
            $set_tx_notes = $gData->inventory_desc;
            $id_rel = $gData->id;
            $user_create = $gData->user_crt_id;

        }elseif ($setTable == 'master_bdd') {
            $gData = collect(\DB::select("select * from master_bdd where id = ".$item))->first();
            $invoice_type_id = 4;
            $set_tx_amount = $gData->bdd_amount;
            $set_tx_notes = $gData->bdd_note;
            $afi_acc_no = $gData->afi_acc_no;
            $coa_acc_no = $gData->coa_acc_no;
            $set_tax = $gData->tax_amount;
            $id_rel = $gData->id;
            $user_create = $gData->user_crt_id;

        }elseif ($setTable == 'master_bdd_schedule') {
            $gData = collect(\DB::select("select * from master_bdd_schedule where id = ".$item))->first();
            $mBdd = collect(\DB::select("select * from master_bdd where id = ".$gData->bdd_id))->first();
            $invoice_type_id = 4;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";
            $afi_acc_no = $mBdd->afi_acc_no;
            $coa_acc_no = $mBdd->coa_acc_no;

        }elseif ($setTable == 'master_sales') {
            $gData = collect(\DB::select("select * from master_sales where id = ".$item))->first();
            $invoice_type_id = 1;
            $set_invoice_id = 0;
            $set_tx_amount = $gData->net_amount+$gData->polis_amount+$gData->materai_amount+$gData->admin_amount;
            $set_tx_notes = $gData->inv_no;
        }
        elseif ($setTable == 'master_sales_schedule') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 1;
            $set_invoice_id = 0;
            $set_tx_amount = $gData->net_amount+$gData->polis_amount+$gData->materai_amount+$gData->admin_amount;
            $set_tx_notes = $gData->inv_no;
        }





        if ($setTable == 'master_inventory_schedule' || $setTable == 'master_inventory')
        {
            if ($setTable == 'master_inventory') {
                $ref_kredit = collect(\DB::select("select * from ref_bank_account
                where id = ".$gData->afi_acc_no))->first();
                $ref_kredit->tx_notes = $ref_kredit->definition;
            } else {
                $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            }


            $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();


            // if ($setTable == 'master_inventory') {
            //     $ref_debet = collect(\DB::select("select * from ref_bank_account
            //     where id = ".$gData->afi_acc_no))->first();
            //     $ref_debet->tx_notes = $ref_debet->definition;
            // } else {
            //     $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            //     where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            // }


            // $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
            // where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();
        }
        elseif($setTable == 'master_bdd_schedule' || $setTable == 'master_bdd')
        {

            if ($setTable == 'master_bdd') {

                // COA 103004
                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 "))->first();
                // $ref_kredit->tx_notes = $ref_kredit->coa_name;

                // dd($ref_debet);

                // COA 100003
                $ref_kredit = collect(\DB::select("select * from ref_bank_account
                where id = ".$afi_acc_no))->first();
                $ref_kredit->tx_notes = $ref_kredit->definition;

            }else {
                $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
                    where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 "))->first();

                $ref_debet = collect(\DB::select("select * from master_coa
                    where id = ".$coa_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->coa_name;
            }


        }elseif ($setTable == 'master_sales_schedule' || $setTable == 'master_sales')
        {


            // if ($setTable == 'master_sales') {
                $ref_debet = collect(\DB::select("select * from ref_bank_account
                where id = ".$gData->afi_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->definition;
            // } else {
            //     $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            //     where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            // }


            $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();

        }

        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();


        // SET TX CODE
        $txcode='';
        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".$systemDate->current_date."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
        $prx=date("ymd", strtotime($systemDate->current_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){
          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
        }else{
            $txcode=$prx.$prx_branch.$prx_company."0001";
        }

        $tx_code = $txcode;

          // JIKA BDD MULTI OVERBOOKING
          if ($setTable == 'master_bdd'  || $setTable == 'master_inventory') {

            if ($gData->tambahan == '1') {

                if ($setTable == 'master_bdd') {

                    $txadd = \DB::select("SELECT * FROM master_bdd_txadd where bdd_id = ".$gData->id);
                }else{
                    $txadd = \DB::select("SELECT * FROM master_inventory_txadd where inventory_id = ".$gData->id);

                }

                // $txadd = \DB::select("SELECT * FROM master_bdd_txadd where bdd_id = ".$gData->id);
                $total_txadd = 0;


                foreach ($txadd as $ktx => $vtx) {
                    $total_txadd += $vtx->amount;

                     // DEBET
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;

                    $tx_type_id_rev = 0;
                    if ($vtx->tx_type_id == 0) {
                        $tx_type_id_rev = 1;
                    }else{
                        $tx_type_id_rev = 0;
                    }

                    $multitx = new MasterTxModel();
                    $multitx->id = $get->max_id+1;
                    $multitx->tx_code = $tx_code;
                    $multitx->id_rel = $id_rel;
                    $multitx->type_tx = $setTable;
                    $multitx->tx_date = $systemDate->current_date;
                    $multitx->tx_type_id = $tx_type_id_rev;

                    $multitx->tx_amount = $vtx->amount;
                    $multitx->seq_approval = $set_last_seq_app;


                    $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$vtx->acc_no."'"))->first();
                    $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = ".$tx_type_id_rev))->first();

                    $multitx->coa_type_id = $coa->coa_type_id;
                    $multitx->acc_last = $coa->last_os;
                    $multitx->coa_id = $vtx->acc_no;

                    $multitx->tx_notes = 'rev-'.$coa->coa_name.", ".$set_tx_notes;

                    if ($jurnalOp->operator_sign == '+') {
                        $multitx->acc_os = $coa->last_os + $vtx->amount;
                    }else {
                        $multitx->acc_os = $coa->last_os - $vtx->amount;
                    }

                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', $vtx->acc_no)
                    ->update(['last_os' => $multitx->acc_os]);

                    $multitx->tx_segment_id = 1;
                    $multitx->created_at = date('Y-m-d H:i:s');
                    $multitx->user_crt_id = $user_create;
                    $multitx->user_app_id = Auth::user()->id;
                    $multitx->id_workflow = 9;
                    $multitx->branch_id = Auth::user()->branch_id;
                    $multitx->company_id = Auth::user()->company_id;
                    $multitx->save();
                }



            }

        }

        for ($i=0; $i <= 1; $i++) {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
            $set_last_seq_app = $last_seq_app->max_id+1;

            $dsc = new MasterTxModel();
            $dsc->id = $get->max_id+1;
            $dsc->tx_code = $tx_code;
            $dsc->id_rel = $gData->id;
            $dsc->tx_date = $systemDate->current_date;
            $dsc->tx_type_id = $i;
            $dsc->seq_approval = $set_last_seq_app;
            // DEBET



            if ($i == 1) {


                $dsc->tx_amount = $set_tx_amount;



                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_debet->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_debet->coa_no;
                $dsc->tx_notes = "rev-".$ref_debet->tx_notes.", ".$set_tx_notes;


                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_debet->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            // KREDIT
            else{

                if ($setTable == 'master_bdd') {

                    if ($gData->tambahan == '1') {
                        $txkredit = $set_tx_amount + $total_txadd;
                    }else{
                        $txkredit = $set_tx_amount;
                    }

                    $dsc->tx_amount = $txkredit;

                }else{
                    $dsc->tx_amount = $set_tx_amount;
                }




                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_kredit->coa_no."'"))->first();


                if ($setTable == 'master_inventory_schedule'){
                    $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();
                }else{
                    $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();
                }

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_kredit->coa_no;
                // $dsc->tx_type_id = 1;

                $dsc->tx_notes = "rev-".$ref_kredit->tx_notes.", ".$set_tx_notes;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $dsc->tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $dsc->tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_kredit->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }

            $dsc->tx_segment_id = 1;
            $dsc->created_at = date('Y-m-d H:i:s');
            $dsc->user_crt_id = Auth::user()->id;
            $dsc->user_app_id = Auth::user()->id;
            $dsc->id_workflow = 9;
            $dsc->branch_id = Auth::user()->branch_id;
            $dsc->company_id = Auth::user()->company_id;
            $dsc->save();
        }

        return $tx_code;


    }

    public function setJurnalDataSplit($item,$setTable)
    {

        if ($setTable == 'is_agent_paid') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 2;
            $set_invoice_id = 0;

            if ($gData->is_tax_company == true) {
                $set_tx_amount = $gData->agent_fee_amount;
            }else{
                $set_tx_amount = $gData->agent_fee_amount-$gData->tax_amount;
            }

            $set_tx_notes = $gData->inv_no;

        }
        elseif ($setTable == 'is_company_paid') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 2;
            $set_invoice_id = 1;
            $set_tx_amount = $gData->comp_fee_amount+$gData->admin_amount;
            $set_tx_notes = $gData->inv_no;
        }
        elseif ($setTable == 'is_premi_paid') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 2;
            $set_invoice_id = 2;
            $set_tx_amount = $gData->underwriter_amount+$gData->polis_amount+$gData->materai_amount;
            $set_tx_notes = $gData->inv_no;
        }

            if ($setTable == 'is_agent_paid') {

                $ref_debet = collect(\DB::select("select * from ref_bank_account where id = ".$gData->afi_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->definition;

                $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();

            }elseif ($setTable == 'is_company_paid') {

                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();

                $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();

            }elseif ($setTable == 'is_premi_paid') {

                $ref_debet = collect(\DB::select("select * from ref_bank_account where id = ".$gData->afi_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->definition;

                $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();

            }





        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

        // SET TX CODE
        $txcode='';
        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".$systemDate->current_date."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
        $prx=date("ymd", strtotime($systemDate->current_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){
          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
        }else{
            $txcode=$prx.$prx_branch.$prx_company."0001";
        }

        $tx_code = $txcode;

        if ($setTable == 'is_agent_paid') {

            // dd($gData->is_tax_company);
            // die();
            if ($gData->is_tax_company == false) {



                for ($i=0; $i <= 1; $i++) {
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;

                $dsc = new MasterTxModel();
                $dsc->id = $get->max_id+1;
                $dsc->tx_code = $tx_code;
                $dsc->id_rel = $gData->id;

                $dsc->tx_date = $systemDate->current_date;

                $dsc->tx_type_id = $i;

                $dsc->tx_amount = $gData->tax_amount;
                $dsc->seq_approval = $set_last_seq_app;

                 // DEBET
                    if ($i == 1) {

                        $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='200001'"))->first();
                        $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                        $dsc->coa_type_id = $coa->coa_type_id;
                        $dsc->acc_last = $coa->last_os;
                        $dsc->coa_id = '200001';


                        $dsc->tx_notes = $coa->coa_name.", ".$set_tx_notes;

                        if ($jurnalOp->operator_sign == '+') {
                            $dsc->acc_os = $coa->last_os + $gData->tax_amount;
                        }else {
                            $dsc->acc_os = $coa->last_os - $gData->tax_amount;
                        }

                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', '200001')
                        ->update(['last_os' => $dsc->acc_os]);
                    }
                    // KREDIT
                    else{
                        $dsc->tx_amount = $gData->tax_amount;

                        $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_kredit->coa_no."'"))->first();
                        $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();

                        $dsc->coa_type_id = $coa->coa_type_id;
                        $dsc->acc_last = $coa->last_os;
                        $dsc->coa_id = $ref_kredit->coa_no;

                        $dsc->tx_notes = $ref_kredit->tx_notes.", ".$set_tx_notes;

                        if ($jurnalOp->operator_sign == '+') {
                            $dsc->acc_os = $coa->last_os + $gData->tax_amount;
                        }else {
                            $dsc->acc_os = $coa->last_os - $gData->tax_amount;
                        }

                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', $ref_kredit->coa_no)
                        ->update(['last_os' => $dsc->acc_os]);
                    }

                $dsc->tx_segment_id = 1;
                $dsc->created_at = date('Y-m-d H:i:s');
                $dsc->user_crt_id = Auth::user()->id;
                $dsc->user_app_id = Auth::user()->id;
                $dsc->id_workflow = 9;
                $dsc->branch_id = 1;
                $dsc->company_id = 1;
                $dsc->save();
                }

            }

        }

        for ($i=0; $i <= 1; $i++) {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
            $set_last_seq_app = $last_seq_app->max_id+1;

            $dsc = new MasterTxModel();
            $dsc->id = $get->max_id+1;
            $dsc->tx_code = $tx_code;
            $dsc->id_rel = $gData->id;
            $dsc->tx_date = $systemDate->current_date;
            $dsc->tx_type_id = $i;
            $dsc->seq_approval = $set_last_seq_app;


            // DEBET
            if ($i == 1) {


                $dsc->tx_amount = $set_tx_amount;




                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_debet->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_debet->coa_no;

                $dsc->tx_notes = $ref_debet->tx_notes.", ".$set_tx_notes;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $dsc->tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $dsc->tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_debet->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            // KREDIT
            else{
                $dsc->tx_amount = $set_tx_amount;

                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_kredit->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_kredit->coa_no;

                $dsc->tx_notes = $ref_kredit->tx_notes.", ".$set_tx_notes;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_kredit->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            $dsc->tx_segment_id = 1;
            $dsc->created_at = date('Y-m-d H:i:s');
            $dsc->user_crt_id = Auth::user()->id;
            $dsc->user_app_id = Auth::user()->id;
            $dsc->id_workflow = 9;
            $dsc->branch_id = 1;
            $dsc->company_id = 1;
            $dsc->save();
        }

        return $tx_code;


    }

    public function setJurnalDataSplitRev($item,$setTable)
    {

        if ($setTable == 'is_agent_paid') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 2;
            $set_invoice_id = 0;

            if ($gData->is_tax_company == true) {
                $set_tx_amount = $gData->agent_fee_amount;
            }else{
                $set_tx_amount = $gData->agent_fee_amount-$gData->tax_amount;
            }


            $set_tx_notes = $gData->inv_no;
        }
        elseif ($setTable == 'is_company_paid') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 2;
            $set_invoice_id = 1;
            $set_tx_amount = $gData->comp_fee_amount+$gData->admin_amount;
            $set_tx_notes = $gData->inv_no;
        }
        elseif ($setTable == 'is_premi_paid') {
            $gData = collect(\DB::select("select * from master_sales_schedule where id = ".$item))->first();
            $invoice_type_id = 2;
            $set_invoice_id = 2;
            $set_tx_amount = $gData->underwriter_amount+$gData->polis_amount+$gData->materai_amount;
            $set_tx_notes = $gData->inv_no;
        }


            if ($setTable == 'is_agent_paid') {

                $ref_kredit = collect(\DB::select("select * from ref_bank_account where id = ".$gData->afi_acc_no))->first();
                $ref_kredit->tx_notes = $ref_kredit->definition;

                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();

            }elseif ($setTable == 'is_company_paid') {

                $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();

                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();

            }elseif ($setTable == 'is_premi_paid') {

                $ref_kredit = collect(\DB::select("select * from ref_bank_account where id = ".$gData->afi_acc_no))->first();
                $ref_kredit->tx_notes = $ref_kredit->definition;

                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();

            }





        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

        // SET TX CODE
        $txcode='';
        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".$systemDate->current_date."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
        $prx=date("ymd", strtotime($systemDate->current_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){
          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
        }else{
            $txcode=$prx.$prx_branch.$prx_company."0001";
        }

        $tx_code = $txcode;

        if ($setTable == 'is_agent_paid') {

            // dd($gData->is_tax_company);
            // die();
            if ($gData->is_tax_company == false) {



                for ($i=0; $i <= 1; $i++) {
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;

                $dsc = new MasterTxModel();
                $dsc->id = $get->max_id+1;
                $dsc->tx_code = $tx_code;
                $dsc->id_rel = $gData->id;

                $dsc->tx_date = $systemDate->current_date;
                $dsc->tx_type_id = $i;

                $dsc->tx_amount = $gData->tax_amount;
                $dsc->seq_approval = $set_last_seq_app;

                 // KREDIT
                    if ($i == 0) {

                        $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='200001'"))->first();
                        $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();

                        $dsc->coa_type_id = $coa->coa_type_id;
                        $dsc->acc_last = $coa->last_os;
                        $dsc->coa_id = '200001';

                        $dsc->tx_notes = 'rev-'.$coa->coa_name.", ".$set_tx_notes;

                        if ($jurnalOp->operator_sign == '+') {
                            $dsc->acc_os = $coa->last_os + $gData->tax_amount;
                        }else {
                            $dsc->acc_os = $coa->last_os - $gData->tax_amount;
                        }

                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', '200001')
                        ->update(['last_os' => $dsc->acc_os]);
                    }
                    // DEBET
                    else{
                        $dsc->tx_amount = $gData->tax_amount;

                        $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_debet->coa_no."'"))->first();
                        $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                        $dsc->coa_type_id = $coa->coa_type_id;
                        $dsc->acc_last = $coa->last_os;
                        $dsc->coa_id = $ref_debet->coa_no;

                        $dsc->tx_notes = 'rev-'.$ref_debet->tx_notes.", ".$set_tx_notes;

                        if ($jurnalOp->operator_sign == '+') {
                            $dsc->acc_os = $coa->last_os + $gData->tax_amount;
                        }else {
                            $dsc->acc_os = $coa->last_os - $gData->tax_amount;
                        }

                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', $ref_debet->coa_no)
                        ->update(['last_os' => $dsc->acc_os]);
                    }

                $dsc->tx_segment_id = 1;
                $dsc->created_at = date('Y-m-d H:i:s');
                $dsc->user_crt_id = Auth::user()->id;
                $dsc->user_app_id = Auth::user()->id;
                $dsc->id_workflow = 9;
                $dsc->branch_id = 1;
                $dsc->company_id = 1;
                $dsc->save();
                }

            }

        }

        for ($i=0; $i <= 1; $i++) {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();
            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
            $set_last_seq_app = $last_seq_app->max_id+1;

            $dsc = new MasterTxModel();
            $dsc->id = $get->max_id+1;
            $dsc->tx_code = $tx_code;
            $dsc->id_rel = $gData->id;

            $dsc->tx_date = $systemDate->current_date;
            $dsc->tx_type_id = $i;
            $dsc->seq_approval = $set_last_seq_app;


            // DEBET
            if ($i == 1) {


                $dsc->tx_amount = $set_tx_amount;




                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_debet->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_debet->coa_no;

                $dsc->tx_notes = 'rev-'.$ref_debet->tx_notes.", ".$set_tx_notes;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $dsc->tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $dsc->tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_debet->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            // KREDIT
            else{
                $dsc->tx_amount = $set_tx_amount;

                $coa = collect(\DB::select("select * from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$ref_kredit->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_kredit->coa_no;

                $dsc->tx_notes = 'rev-'.$ref_kredit->tx_notes.", ".$set_tx_notes;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $ref_kredit->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            $dsc->tx_segment_id = 1;
            $dsc->created_at = date('Y-m-d H:i:s');
            $dsc->user_crt_id = Auth::user()->id;
            $dsc->user_app_id = Auth::user()->id;
            $dsc->id_workflow = 9;
            $dsc->branch_id = 1;
            $dsc->company_id = 1;
            $dsc->save();
        }

        return $tx_code;


    }

    public function actCancelPaySc($setTable,$type,$datas)
    {
        DB::table($setTable)
        ->where('id', $datas)
        ->update(['id_workflow' => 14]);
    }

    public function getMenu()
    {
        $data = \DB::select("SELECT * FROM ref_rel_menu
        left join ref_menu on ref_menu.code = ref_rel_menu.code_menu where id_role = ".Auth::user()->user_role_id);
        return json_encode($data);
    }

    public function actGiveApprovalCancelPaySc($setTable,$type,$datas,$memo){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (14)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 12]);

                 // SEMENTARA BELUM SET JURNAL
                //  if ($setTable == 'master_sales_schedule') {
                //     $getTxCode = 0;
                // }else {
                    $getTxCode = $this->setJurnalDataReversal($item->id,$setTable);
                // }



                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'ref_code' => $item->id,
                        'ref_code_type' => $setTable,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );

                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['paid_status_id' => 0]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 12]);

                 // SEMENTARA BELUM SET JURNAL
                //  if ($setTable == 'master_sales_schedule') {
                //     $getTxCode = 0;
                // }else {
                    $getTxCode = $this->setJurnalDataReversal($item,$setTable);
                // }


                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'ref_code' => $item,
                        'ref_code_type' => $setTable,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


                DB::table('master_inventory_schedule')
                ->where('id', $item)
                ->update(['paid_status_id' => 0]);

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function znSendApproval($setTable,$datas,$idtype)
    {
        foreach ($datas as $item) {
            DB::table($setTable)
             ->where('id', $item)
             ->update(['wf_status_id' => 2]);
        }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }

    public function znDeleteApproval($setTable,$datas){
        if ($setTable == 'master_sales_polis') {
            foreach ($datas as $item) {
                SalesPolisModel::destroy($item);
                SalesPolisScModel::where('id_master_sales', $item)->delete();
            }
        }

         return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }


    public function znGiveApproval($setTable,$datas,$memo){

        foreach ($datas as $item) {
            DB::table($setTable)
             ->where('id', $item)
             ->update(['wf_status_id' => 9]);

            DB::table($setTable)
            ->where('id', $item)
            ->update(['user_upd_id' => Auth::user()->id]);

            // SET MEMO
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
            DB::table('master_memo')->insert(
                [
                    'id' => $get->max_id+1,
                    'tx_code' => '',
                    'notes' => $memo,
                    'ref_code' => $item,
                    'ref_code_type' => $setTable,
                    'id_workflow' => 9,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_crt_id' => Auth::user()->id
                ]
            );
        }
    }

    public function znRejectApproval($setTable,$datas,$memo){

        foreach ($datas as $item) {
            DB::table($setTable)
             ->where('id', $item)
             ->update(['wf_status_id' => 1]);

            DB::table($setTable)
            ->where('id', $item)
            ->update(['user_upd_id' => Auth::user()->id]);

            // SET MEMO
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
            DB::table('master_memo')->insert(
                [
                    'id' => $get->max_id+1,
                    'tx_code' => '',
                    'notes' => $memo,
                    'ref_code' => $item,
                    'ref_code_type' => $setTable,
                    'id_workflow' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_crt_id' => Auth::user()->id
                ]
            );
        }

    }

    public function validateProductKMR($detailKMR)
    {
        $jmlPassed = 0;
        $tenor = 0;
        foreach ( $detailKMR as $item ) {
            foreach ( $item->value as $dt ) {
                if ( $dt->bit_id == 6 ) {
                    // Field Tanggal Lahir
                    $mt = join('-', array_reverse(explode('/', $dt->value)));
                    // $mt = join('-', array_reverse(explode('-', $dt->value)));
                    $mt = new Datetime($mt); // Your date of birth
                } else if ( $dt->bit_id == 5 ) {
                    // Field Tanggal Lahir
                    $brtdt = join('-', array_reverse(explode('/', $dt->value)));
                    // $brtdt = join('-', array_reverse(explode('-', $dt->value)));
                    $bday = new Datetime($brtdt); // Your date of birth
                    $today = new Datetime(date('Y-m-d'));
                    $usia = $today->diff($bday)->y;
                } else if ( $dt->bit_id == 87 ) {
                    // Field Masa Asuransi (Dalam Bulan)
                    $masaAsuransi = (int)$dt->value;
                    $tenor = ceil($masaAsuransi / 12); // Pembualatan ke atas
                } else if ( $dt->bit_id == 89 ) {
                    // Field Premi Rate
                    $premiRate = (float)str_replace(',', '.', $dt->value);
                }

            } // end foreach

            $interval = $bday->diff($mt);
            $bulanLahir = (int) $interval->format('%m');

            if ( $bulanLahir >= 6 ) {
                $usia += 1;
            }

            $rateUsia = DB::TABLE('ref_rate_usia')
                            ->where('usia', $usia)
                            ->where('tenor', $tenor)
                            ->first();

            if ( !is_null($rateUsia) ) {
                $rate = (float)$rateUsia->rate;
                if ( $premiRate == $rate) {
                    $this->setValueStatusCheck($item->value, 'PASSED');
                    $jmlPassed++;
                } else {
                    $this->setValueStatusCheck($item->value, 'FAILED');
                }
            } else {
                $this->setValueStatusCheck($item->value, 'ERROR');
            }

        } // end foreach

        $param['data'] = $detailKMR;
        $param['jmlPassed'] = $jmlPassed;
        return $param;
    } // end function

    public function setValueStatusCheck($data, $value)
    {
        foreach ( $data as $dt ) {
            if ( $dt->bit_id == 91 ) {
                // Status Check
                $dt->value = $value;
            }
        }
    }  // end function


}
