<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use App\Models\MasterTxModel;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\InventoryModel;
use App\Models\InventoryScModel;

class BatchController extends Controller
{
    public function index(Request $request)
    {

        $tittle = 'Batch Proses';

        $idbranch='konsol';
        if($request->get('id')){
          $idbranch=$request->get('id');

            if($request->get('id')=='konsol'){

               $aktiva = \DB::select("select
                  id,
                  coa_no,
                  coa_name,
                  case when coa_no in ('301') then last_os +
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                  FROM   cte
                  where coa_no in ('400','500')) else last_os end as last_os
                  from
                  (select
                  a.id,
                  a.coa_no,
                  coa_name,
                  case when a.coa_no = b.coa_parent_id then b.nom
                     when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                  from master_coa a left join
                  (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                  from master_coa
                  where coa_parent_id is  not null and length(coa_parent_id) = 6
                  group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT coa_no, sum(last_os) AS last_os
                  FROM   cte
                  GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

          $pasiva = \DB::select("select
                  id,
                  coa_no,
                  coa_name,
                  case when coa_no in ('301') then last_os +
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=1
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                  )
                  SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                  FROM   cte
                  where coa_no in ('400','500')) else last_os end as last_os
                  from
                  (select
                  a.id,
                  a.coa_no,
                  coa_name,
                  case when a.coa_no = b.coa_parent_id then b.nom
                     when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                  from master_coa a left join
                  (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                  from master_coa
                  where coa_parent_id is  not null and length(coa_parent_id) = 6
                  group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT coa_no, sum(last_os) AS last_os
                  FROM   cte
                  GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");
            }else{

               $aktiva = \DB::select("select
                  id,
                  coa_no,
                  coa_name,
                  case when coa_no in ('301') then last_os +
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                  FROM   cte
                  where coa_no in ('400','500')) else last_os end as last_os
                  from
                  (select
                  a.id,
                  a.coa_no,
                  coa_name,
                  case when a.coa_no = b.coa_parent_id then b.nom
                     when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                  from master_coa a left join
                  (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                  from master_coa
                  where coa_parent_id is  not null and length(coa_parent_id) = 6
                  group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT coa_no, sum(last_os) AS last_os
                  FROM   cte
                  GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.branch_id=".$idbranch." and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

              $pasiva = \DB::select("select
                  id,
                  coa_no,
                  coa_name,
                  case when coa_no in ('301') then last_os +
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=1
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                  )
                  SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                  FROM   cte
                  where coa_no in ('400','500')) else last_os end as last_os
                  from
                  (select
                  a.id,
                  a.coa_no,
                  coa_name,
                  case when a.coa_no = b.coa_parent_id then b.nom
                     when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                  from master_coa a left join
                  (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                  from master_coa
                  where coa_parent_id is  not null and length(coa_parent_id) = 6
                  group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT coa_no, sum(last_os) AS last_os
                  FROM   cte
                  GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");


            }


        }else{
          $aktiva = \DB::select("select
                  id,
                  coa_no,
                  coa_name,
                  case when coa_no in ('301') then last_os +
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                  FROM   cte
                  where coa_no in ('400','500')) else last_os end as last_os
                  from
                  (select
                  a.id,
                  a.coa_no,
                  coa_name,
                  case when a.coa_no = b.coa_parent_id then b.nom
                     when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                  from master_coa a left join
                  (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                  from master_coa
                  where coa_parent_id is  not null and length(coa_parent_id) = 6
                  group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT coa_no, sum(last_os) AS last_os
                  FROM   cte
                  GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

          $pasiva = \DB::select("select
                  id,
                  coa_no,
                  coa_name,
                  case when coa_no in ('301') then last_os +
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=1
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                  )
                  SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                  FROM   cte
                  where coa_no in ('400','500')) else last_os end as last_os
                  from
                  (select
                  a.id,
                  a.coa_no,
                  coa_name,
                  case when a.coa_no = b.coa_parent_id then b.nom
                     when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                  from master_coa a left join
                  (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                  from master_coa
                  where coa_parent_id is  not null and length(coa_parent_id) = 6
                  group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                  (WITH RECURSIVE cte AS (
                     SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                     FROM   master_coa
                     WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                     UNION  ALL
                     SELECT c.coa_no, p.coa_no, p.last_os
                     FROM   cte c
                     JOIN   master_coa p USING (coa_parent_id)
                  )
                  SELECT coa_no, sum(last_os) AS last_os
                  FROM   cte
                  GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");
        }

        if($request->get('id')){
            $idbranch=$request->get('id');
              if($request->get('id')=='konsol'){
                $laba_rugi = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    balance_type_id,
                    coa_type_id,
                    case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and company_id=".Auth::user()->company_id."
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                    where coa_type_id in (3,4) and a.company_id=".Auth::user()->company_id."
                    ORDER BY coa_no asc");

              }else{
                $laba_rugi = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    balance_type_id,
                    coa_type_id,
                    case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and branch_id=".$idbranch." and company_id=".Auth::user()->company_id."
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                    where coa_type_id in (3,4) and a.branch_id=".$idbranch." and a.company_id=".Auth::user()->company_id."
                    ORDER BY coa_no asc");
              }

          }else{
             $laba_rugi = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    balance_type_id,
                    coa_type_id,
                    case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and company_id=".Auth::user()->company_id."
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                    where coa_type_id in (3,4) and a.company_id=".Auth::user()->company_id."
                    ORDER BY coa_no asc");
              }




        $param['aktiva']=$aktiva;
        $param['pasiva']=$pasiva;
        $param['laba_rugi']=$laba_rugi;

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');
        $param['type_batch'] = $request->get('type_batch');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'batch.list', $param);
        }else {
            return view('master.master')->nest('child', 'batch.list', $param);
        }

    }

    public function batch_inventory (Request $request){

        $type_batch = $request->get('type_batch');
        $system_date = collect(\DB::select("select * from ref_system_date"))->first();

        if ($type_batch == 'day') {
            $month=date('m',strtotime($system_date->current_date));
            $year=date('Y',strtotime($system_date->current_date));
        }else {
            $month=date('m',strtotime($request->get('tgl')));
            $year=date('Y',strtotime($request->get('tgl')));
        }



        $batch = collect(\DB::select("select * from ref_batch where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." and is_done=true and processing_date = '".date('Y-m-d',strtotime($request->get('tgl')))."'"))->first();

        if($batch){
            return json_encode(['rc'=>2,'rm'=>'Batch Process is already processed']);
        }else{

            $gData = DB::select("SELECT master_inventory_schedule.id as id from master_inventory_schedule
            left join master_inventory on master_inventory.id = master_inventory_schedule.inventory_id
            where paid_status_id not in (1) and master_inventory_schedule.id_workflow not in (9)
            and month = ".$month." and year=".$year."
            and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            if($gData){
                foreach ($gData as $value) {

                    $this->setJurnalData($value->id,'master_inventory_schedule');

                    DB::table('master_inventory_schedule')
                    ->where('id', $value->id)
                    ->update(['paid_status_id' => 1,'id_workflow'=>9]);
                }

                return json_encode(['rc'=>1,'rm'=>'Batch Process Finished']);
            }

            else{
                return json_encode(['rc'=>3,'rm'=>'Data Not Found  ']);
            }

        }

    }

    public function batch_bdd (Request $request){

        $type_batch = $request->get('type_batch');
        $system_date = collect(\DB::select("select * from ref_system_date"))->first();

        if ($type_batch == 'day') {
            $month=date('m',strtotime($system_date->current_date));
            $year=date('Y',strtotime($system_date->current_date));
        }else {
            $month=date('m',strtotime($request->get('tgl')));
            $year=date('Y',strtotime($request->get('tgl')));
        }


        $batch = collect(\DB::select("select * from ref_batch where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." and is_done=true and processing_date = '".date('Y-m-d',strtotime($request->get('tgl')))."'"))->first();

        if($batch){
            return json_encode(['rc'=>2,'rm'=>'Batch Process is already processed']);
        }else{

            $gData = DB::select("SELECT master_bdd_schedule.id from master_bdd_schedule
            left join master_bdd on master_bdd.id = master_bdd_schedule.bdd_id
            where paid_status_id not in (1) and master_bdd_schedule.id_workflow not in (9) and  month = ".$month." and year=".$year." and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            if($gData){

                foreach ($gData as $value) {
                    $this->setJurnalData($value->id,'master_bdd_schedule');

                    DB::table('master_bdd_schedule')
                    ->where('id', $value->id)
                    ->update(['paid_status_id' => 1,'id_workflow'=>9]);
                }

                return json_encode(['rc'=>1,'rm'=>'Batch Process Finished']);
            }

            else{
                return json_encode(['rc'=>3,'rm'=>'Data Not Found  ']);
            }

        }

    }

    public function batch_jurnal (Request $request){

        $getBranch = \DB::SELECT("SELECT * FROM master_branch where is_ho = 'f'");

        foreach ($getBranch as $keyBranch => $vBranch) {

            $aktiva = $this->getBalanceSheet('aktiva',$vBranch->id);
            $pasiva = $this->getBalanceSheet('pasiva',$vBranch->id);

            $total=0;
            $total_pasiva=0;
                if($aktiva){
                    foreach ($aktiva as $item) {
                        $awal=strlen($item->coa_no);
                        if($awal==6){
                        $total+=$item->last_os;
                        }
                    }
                }

                if ($pasiva) {
                    foreach ($pasiva as $item) {
                        $awal=strlen($item->coa_no);
                        if($awal==3){
                            $total_pasiva+=$item->last_os;
                        }
                    }
                }

            $selisih = $total - $total_pasiva;
          

            $type_batch = $request->get('type_batch');
            $system_date = collect(\DB::select("select * from ref_system_date"))->first();
    
            if ($type_batch == 'day') {
                $month=date('m',strtotime($system_date->current_date));
                $year=date('Y',strtotime($system_date->current_date));
            }else {
                $month=date('m',strtotime($request->get('tgl')));
                $year=date('Y',strtotime($request->get('tgl')));
            }
    
            $batch = collect(\DB::select("select * from master_financial where branch_id=".$vBranch->id." and company_id=".Auth::user()->company_id." and report_date = '".date('Y-m-d',strtotime($request->get('tgl')))."'"))->first();
    
            if($batch){
              DB::table('master_financial')->where('id', $batch->id)->delete();
            }
    
            $posisi_keuangan_inti = collect(\DB::select("select
                sum(case when coa_no in ('100','101','102','103') then last_os end) as total_aset,
                sum(case when coa_no in ('301') then last_os end) as profit_loss,
                sum(case when coa_no in ('400') then last_os end) as total_income,
                sum(case when coa_no in ('500') then last_os end) as total_outcome,
                sum(case when coa_no in ('500001') then last_os end) as marketing_outcome,
                sum(case when coa_no in ('500002') then last_os end) as building_outcome,
                sum(case when coa_no in ('500003') then last_os end) as general_outcome,
                sum(case when coa_no in ('500004') then last_os end) as resources_outcome
                from
                (select
                coa_no,
                coa_name,
                case when coa_no in ('301') then last_os +
                (WITH RECURSIVE cte AS (
                   SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                   FROM   master_coa
                   WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id." and branch_id=".$vBranch->id." and is_active=true
                   UNION  ALL
                   SELECT c.coa_no, p.coa_no, p.last_os
                   FROM   cte c
                   JOIN   (select * from master_coa where company_id=".Auth::user()->company_id." and branch_id=".$vBranch->id." and is_active=true) p USING (coa_parent_id)
                )
                SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                FROM   cte
                where coa_no in ('400','500')) else last_os end as last_os
                from
                (select
                a.coa_no,
                coa_name,
                case when a.coa_no = b.coa_parent_id then b.nom
                   when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                from master_coa a left join
                (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                from master_coa
                where coa_parent_id is  not null and length(coa_parent_id) = 6
                group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                (WITH RECURSIVE cte AS (
                   SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                   FROM   master_coa
                   WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id." and branch_id=".$vBranch->id." and is_active=true
                   UNION  ALL
                   SELECT c.coa_no, p.coa_no, p.last_os
                   FROM   cte c
                   JOIN   (select * from master_coa where company_id=".Auth::user()->company_id." and branch_id=".$vBranch->id." and is_active=true) p USING (coa_parent_id)
                )
                SELECT coa_no, sum(last_os) AS last_os
                FROM   cte
                GROUP  BY 1) c on a.coa_no = c.coa_no where a.company_id=".Auth::user()->company_id." and a.branch_id=".$vBranch->id." and a.is_active=true) x) x;"))->first();
    
    
    
                $results = \DB::select("SELECT max(id) AS id from master_financial");
                if($results){
                    $id=$results[0]->id + 1;
                }else{
                    $id=1;
                }
                $systemDate = collect(\DB::select("select * from ref_system_date"))->first();
    
                DB::table('master_financial')->insert(
                    [
                        'id' => $id,
                        'report_date' => $systemDate->current_date,
                        'total_aset' => $posisi_keuangan_inti->total_aset,
                        'profit_loss' =>$posisi_keuangan_inti->profit_loss,
                        'total_income' => $posisi_keuangan_inti->total_income,
                        'total_outcome' => $posisi_keuangan_inti->total_outcome,
                        'marketing_outcome' => $posisi_keuangan_inti->marketing_outcome,
                        'building_outcome' => $posisi_keuangan_inti->building_outcome,
                        'general_outcome' => $posisi_keuangan_inti->general_outcome,
                        'resources_outcome' => $posisi_keuangan_inti->resources_outcome,
                        'date_dim_id' => date('Ymd',strtotime($systemDate->current_date)),
                        'company_id' =>Auth::user()->company_id,
                        'branch_id' => $vBranch->id,
                        'selisih' => $selisih
                    ]
                );
    
                

        }

        $upd_date = date('Y-m-d', strtotime('+1 days', strtotime($systemDate->current_date)));
        $last_datez = date('Y-m-d', strtotime('+1 days', strtotime($systemDate->last_date)));

        if ($last_datez > date('Y-m-d')) {

        }else {

            if ($type_batch == 'day') {
                DB::table('ref_system_date')
                ->where('id', 1)
                ->update(['current_date' => $upd_date,'last_date'=>$systemDate->current_date]);
            }
            
        }




        $results = \DB::select("SELECT max(id) AS id from ref_batch");
        if($results){
            $id=$results[0]->id + 1;
        }else{
            $id=1;
        }
        DB::table('ref_batch')->insert(
            [
                'id' => $id,
                'processing_date' => date('Y-m-d',strtotime($request->get('tgl'))),
                'processing_time' => date('Y-m-d H:i:s',strtotime($request->get('tgl'))),
                'is_done' =>true,
                'company_id' =>Auth::user()->company_id,
                'branch_id' => Auth::user()->branch_id
            ]
        );

      

        return json_encode(['rc'=>1,'rm'=>'Batch Process Finished']);








    }

    public function getBalanceSheet($type,$branch)
    {
       // $idbranch = 'konsol';
       $idbranch = $branch;

        if($branch=='konsol'){

          $arrBranch = DB::table('master_branch')
                            ->select('id')
                            ->where('is_ho', 'f')
                            ->where('company_id', Auth::user()->company_id)
                            ->pluck('id')
                            ->toArray();

          $arrBranch = join(',', $arrBranch);

          // Aktiva Paling Baru
          $aktiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . $arrBranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . $arrBranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 1
                                        and company_id = " . Auth::user()->company_id . "
                                        and branch_id in (" . $arrBranch . ")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (select coa_no, sum(last_os) as last_os from master_coa where
                                        coa_parent_id is null
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . $arrBranch . ")
                                        and is_active = true
                                        group by coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no, sum(last_os)as last_os, coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                          group by coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

          // Pasiva Paling Baru
          $pasiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os +
                                  (with recursive cte as (
                                select
                                  a.coa_no,
                                  a.coa_no as coa_parent_id,
                                  coalesce(a.last_os, 0) as last_os
                                from
                                  (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                union all
                                select
                                  c.coa_no,
                                  p.coa_no,
                                  p.last_os
                                from
                                  cte c
                                join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                    using (coa_parent_id) )
                                select
                                  sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                from
                                  cte
                                where
                                  coa_no in ('400','500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 2
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . $arrBranch .")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

        }else{
          // BUKAN KONSOL

          // Aktiva Paling Baru
          $aktiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . $idbranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . $idbranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 1
                                        and company_id = " . Auth::user()->company_id . "
                                        and branch_id in (" . $idbranch . ")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (select coa_no, sum(last_os) as last_os from master_coa where
                                        coa_parent_id is null
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . $idbranch . ")
                                        and is_active = true
                                        group by coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no, sum(last_os)as last_os, coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $idbranch . ")
                                          and is_active = true
                                          group by coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

          // Pasiva Paling Baru
          $pasiva = \DB::SELECT("select
                                    coa_no,
                                    coa_name,
                                    sum(case when coa_no in ('301') then last_os +
                                    (with recursive cte as (
                                  select
                                    a.coa_no,
                                    a.coa_no as coa_parent_id,
                                    coalesce(a.last_os, 0) as last_os
                                  from
                                    (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                  union all
                                  select
                                    c.coa_no,
                                    p.coa_no,
                                    p.last_os
                                  from
                                    cte c
                                  join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                      using (coa_parent_id) )
                                  select
                                    sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                  from
                                    cte
                                  where
                                    coa_no in ('400','500', '600')) else last_os end )as last_os
                                  from
                                    (
                                    select
                                      a.coa_no,
                                      coa_name,
                                      d.coa_parent_id,
                                      case
                                        when a.coa_no = b.coa_parent_id then b.nom
                                        when a.coa_no = c.coa_no then c.last_os
                                        else coalesce(a.last_os, 0) end as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          coa_name,
                                          sum(last_os) as last_os,
                                          coa_type_id
                                        from
                                          master_coa
                                        where
                                          coa_type_id = 2
                                          and company_id = " . Auth::user()->company_id ."
                                          and branch_id in (" . $idbranch .")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_name,
                                          coa_type_id ) a
                                      left join (
                                        select
                                          coa_parent_id,
                                          sum(coalesce(last_os, 0)) as nom
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is not null
                                          and length(coa_parent_id) = 6
                                        group by
                                          coa_parent_id ) b on
                                        a.coa_no = b.coa_parent_id
                                      left join (with recursive cte as (
                                        select
                                          a.coa_no,
                                          a.coa_no as coa_parent_id,
                                          coalesce(a.last_os, 0) as last_os
                                        from
                                          (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                      union all
                                        select
                                          c.coa_no,
                                          p.coa_no,
                                          p.last_os
                                        from
                                          cte c
                                        join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                            using (coa_parent_id) )
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          cte
                                        group by
                                          1) c on
                                        a.coa_no = c.coa_no
                                      left join (
                                        select
                                          distinct coa_no,
                                          case
                                            when coa_parent_id isnull then coa_no
                                            else coa_parent_id end
                                          from
                                            master_coa) d on
                                        a.coa_no = d.coa_no
                                      order by
                                        d.coa_parent_id,
                                        a.coa_no asc)x
                                  group by
                                    coa_no,
                                    coa_name ,
                                    coa_parent_id
                                  order by
                                    coa_parent_id,
                                    coa_no");


        } // end if


        if ($type == 'aktiva') {
          return $aktiva;
        }
        elseif ($type == 'pasiva') {
          return $pasiva;
        }

    }
}
