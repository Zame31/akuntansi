<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;

class OpexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $segment_beban = \DB::select('SELECT * FROM ref_outflow_type where is_active=true order by id asc');
        $bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by id asc');
        $dokumen = \DB::select('SELECT * FROM ref_doc_type where is_active=true order by seq asc');
        $segment = \DB::select('SELECT * FROM ref_segment where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');

        $param['segment_beban']=$segment_beban;
        $param['bank']=$bank;
        $param['dokumen']=$dokumen;
        $param['segment']=$segment;
       
        return view('master.master')->nest('child', 'opex.index',$param);
        
    }
    public function update_opex(Request $request){
        $segment_beban = \DB::select('SELECT * FROM ref_outflow_type where is_active=true order by id asc');
        $bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true order by id asc');
        $dokumen = \DB::select('SELECT * FROM ref_doc_type where is_active=true order by seq asc');
        $segment = \DB::select('SELECT * FROM ref_segment where is_active=true order by seq asc');

        $data = \DB::select("SELECT * FROM master_tx where tx_code='".$request->get('id')."' order by id asc");

        $param['segment_beban']=$segment_beban;
        $param['bank']=$bank;
        $param['dokumen']=$dokumen;
        $param['segment']=$segment;
        $param['data']=$data;

        return view('master.master')->nest('child', 'opex.form_edit',$param);

    }
    public function insert_opex(Request $request){

        $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();
         if($cek_branch->is_open=='true'){
            $systemDate = collect(\DB::select("select * from ref_system_date"))->first();
            $txcode='';
            $tgl_tr=date('Y-m-d',strtotime($systemDate->current_date));

            // $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($systemDate->current_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where created_at::date='".date('Ymd',strtotime($systemDate->current_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            $prx=date('Ymd',strtotime($systemDate->current_date));
            $prx_branch=Auth::user()->branch_id;
            $prx_company=Auth::user()->branch_id;

            if($results){

              $id_max= $results[0]->max_id;
              $sort_num = (int) substr($id_max, 1, 4);
              $sort_num++;
              $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
              $txcode=$new_code;
            }else{
                $txcode=$prx.$prx_branch.$prx_company."0001";
            }

            
            $catatan=$request->input('catatan');
            $idsegment=$request->input('idsegment');
            $keterangan=$request->input('keterangan');
            $amount=str_replace('.','', $request->input('amount'));    
            $amount1=str_replace(',','.', $amount);        

            $datacount= count($request->coa_no);
            for ($i=0; $i < $datacount; $i++) {


                $id='';
                $results = \DB::select("SELECT max(id) AS id from master_tx");

                if($results){
                    $id=$results[0]->id + 1;
                }else{
                    $id=1;
                }
                $coa=$request->coa_no[$i];
                //insert transaksi
                if($i==0){
                    $type=0;
                }else{
                    $type=1;
                }
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$coa."'");
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $type,
                        'coa_id' => $coa,
                        'tx_notes' => $keterangan,
                        'tx_amount' => $amount1,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => 4,
                        'id_workflow' => 1,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'opex',
                        'coa_type_id' => $ref_coa[0]->coa_type_id
                    ]
                );
            }

            //insert dokumen
            
               $jns_dok=count($request->jns_dok);
                for ($i=0; $i < $jns_dok ; $i++) {

                    $destination_path = public_path('upload_dokumen');
                    $files = $request->file[$i];
                    if($files){
                        $filename = $files->getClientOriginalName();
                        $upload_success = $files->move($destination_path, $txcode."_".$filename);

                        //$file=$this->request->getUploadedFiles()[$i];
                        //$file->moveTo('temp/' . $file->getName());
                        $jnsdok=$request->jns_dok[$i];
                        $iddoc='';
                        $results = \DB::select("SELECT max(id) AS id from master_doc");

                        if($results){
                            $iddoc=$results[0]->id + 1;
                        }else{
                            $iddoc=1;
                        }

                        DB::table('master_doc')->insert(
                            [
                                'id' => $iddoc,
                                'tx_code' => $txcode,
                                'url' => $filename,
                                'doc_type_id' => $jnsdok,
                                'created_at' => date('Y-m-d H:i:s'),
                                'user_crt_id' => Auth::user()->id
                            ]
                        );    
                    }
                }     
            
            


            //insert memo
            $idmemo='';
            $results = \DB::select("SELECT max(id) AS id from master_memo");
            if($results){
                $idmemo=$results[0]->id + 1;
            }else{
                $idmemo=1;
            }

            DB::table('master_memo')->insert(
                    [
                        'id' => $idmemo,
                        'tx_code' => $txcode,
                        'notes' => $catatan,
                        'id_workflow' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );

            return json_encode(['rc'=>1,'rm'=>'berhasil']);
    
         }else{

            return json_encode(['rc'=>2,'rm'=>'Set End Of day']);

         }

        
    }
}



