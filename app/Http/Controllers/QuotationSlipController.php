<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use PDF;
use iio\libmergepdf\Merger;
use Illuminate\Support\Collection;
use Datetime;

class QuotationSlipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {


        $data = DB::TABLE('master_qs_vehicle')
                    ->select(
                                'master_customer.full_name',
                                'master_qs_vehicle.*',
                                'ref_product.definition',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_qs_vehicle.product_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_qs_vehicle.valuta_id')
                    ->where(function ($query) {
                        $query->whereNull('is_client_create')
                             ->orWhere('is_client_create', 'f');
                    })
                    ->orderBy('master_qs_vehicle.id', 'DESC')


                    ->get();

        $param['data'] = $data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'quotation_slip.index',$param);
        }else {
            return view('master.master')->nest('child', 'quotation_slip.index',$param);
        }

    }

    public function indexClient(Request $request)
    {
        $data = DB::TABLE('master_qs_vehicle')
                ->select(
                            'master_customer.full_name',
                            'master_qs_vehicle.*',
                            'ref_product.definition',
                            'ref_valuta.mata_uang',
                            'status_qs_client.wf_status_client_id'
                        )
                ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                ->leftJoin('ref_product', 'ref_product.id', '=', 'master_qs_vehicle.product_id')
                ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_qs_vehicle.valuta_id')
                ->leftJoin('status_qs_client', 'status_qs_client.qs_id', '=', 'master_qs_vehicle.id')
                ->where('status_qs_client.wf_status_client_id', '>', '1')
                ->where('master_qs_vehicle.is_client_create', 't')
                ->orderBy('master_qs_vehicle.id', 'DESC')
                ->get();

        $param['data'] = $data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'quotation_slip.index_client',$param);
        }else {
            return view('master.master')->nest('child', 'quotation_slip.index_client',$param);
        }
    }

    public function getRomawi($bln){
        switch ($bln){
            case 1:
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            case 9:
                return "IX";
                break;
            case 10:
                return "X";
                break;
            case 11:
                return "XI";
                break;
            case 12:
                return "XII";
                break;
        }
    }

    public function create(Request $request)
    {

        $param['type'] = $request->type;

        $param['products'] = DB::TABLE('ref_product')->where('is_active', 't')->get();
        $param['customer'] = DB::TABLE('master_customer')
                                ->where('is_active', 't')
                                ->where('branch_id', Auth::user()->branch_id)
                                ->orderBy('full_name', 'ASC')
                                ->get();
        $param['refUnderwriter'] = DB::TABLE('ref_underwriter')
                                        ->where('is_active', 't')
                                        ->get();

        $param['sourceBusiness'] = DB::TABLE('ref_business_source')
                                        ->where('is_active', 't')
                                        ->get();

        $param['ref_valuta'] = DB::TABLE('ref_valuta')->get();


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'quotation_slip.create',$param);
        }else {
            return view('master.master')->nest('child', 'quotation_slip.create',$param);
        }

    }

    public function getCustomerAddress(Request $request) {


        $data = DB::TABLE('master_customer')
                    ->select('master_customer.*', 'ref_agent.spv_code', 'ref_agent.full_name')
                    ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_customer.agent_id')
                    ->where('master_customer.id', $request->id)
                    ->first();

        // LAMA
        $action = $request->action;
        $id = DB::TABLE('master_qs_vehicle')->max('id');
        if ( $action != 'edit' ) {
            if ( is_null($id) ) {
                $id = 1;
            } else {
                $id += 1;
            }
        }


        // $monthRomawi
        $year = date('y');
        $bulan = date('n');
        $romawi = $this->getRomawi($bulan);
        $qsNo = $id . '/MKT/HD/' . $data->spv_code . '/QS/' . $romawi . '/'. $year;

        $data->qs_no = $qsNo;

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data
        ]);
    }



    public function store(Request $request) {

        DB::beginTransaction();
        try {
            $arrData = $request->all();

            if ( isset($arrData['menu']) ) {
                // Create Menu Proposal Coverage

                if ( $arrData['menu'] == 'proposal_coverage' ) {
                    $dataProduct = DB::TABLE('ref_qs_content')
                        ->where('product_id', $arrData['underwriter_id'])
                        ->where('is_active', 't')
                        ->first();

                    if ( !is_null($dataProduct) ) {
                        $arrData['main_exclusions'] = $dataProduct->main_exclusions;
                        $arrData['deductible'] = $dataProduct->deductible;
                        $arrData['clauses'] = $dataProduct->clauses;
                        $arrData['coverage'] = $dataProduct->coverage;
                        $arrData['form_wording'] = $dataProduct->form_wording;
                        $arrData['the_business'] = $dataProduct->the_business;
                    } //endif

                    // Generate QS No
                    if ( is_null($arrData['id']) ) {
                        // Insert data QS Baru
                        $idQS = DB::TABLE('master_qs_vehicle')->max('id');

                        if ( is_null($idQS) ) {
                            $idQS = 1;
                        } else {
                            $idQS += 1;
                        }

                        $dataCustomer = DB::TABLE('master_customer')
                                ->select('master_customer.*', 'ref_agent.spv_code', 'ref_agent.full_name')
                                ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_customer.agent_id')
                                ->where('master_customer.id', Auth::user()->customer_id)
                                ->first();


                        // $monthRomawi
                        $year = date('y');
                        $bulan = date('n');
                        $romawi = $this->getRomawi($bulan);
                        $qsNo = $idQS . '/MKT/HD/' . $dataCustomer->spv_code . '/QS/' . $romawi . '/'. $year;
                        $arrData['qs_no_value'] = $qsNo;

                    }


                } //endif

            } // end if


            if ( $arrData['underwriter_id'] == '19' ) {
                // PRODUK KREDIT MULTIGUNA REGULER (KMR)
                $detailKMR = json_decode($arrData['detail']);
                $detailKMR = $this->validateProductKMR($detailKMR);

                $detail = $detailKMR['data'];
                $jmlPassed = $detailKMR['jmlPassed'];

                if ( count($detail) != $jmlPassed ) {
                    return response()->json([
                        'rc' => 1,
                        'rm' => 'Invalid',
                        'data' => $detail
                    ]);
                }

            } else {
                $detail = json_decode($arrData['detail']);
            }

            $company = DB::TABLE('master_company')->where('id', 1)->first();
            if ( is_null($arrData['id']) ) {
                if ( $company->app_name == 'ATA HDIC' ) {
                    $dataProduct = DB::TABLE('ref_product')->where('id', $arrData['underwriter_id'])->first();
                    $codeProduct = "";
                    if ( !is_null($dataProduct)) {
                        $codeProduct = $dataProduct->prod_code;
                    }

                    $refDoc = DB::TABLE('ref_doc_no')
                                ->where('year', date('Y'))
                                ->first();

                    if ( is_null($refDoc) ) {
                        $id = 1;
                        $maxId = DB::TABLE('ref_doc_no')->max('id');
                        if ( is_null($maxId) ) {
                            $maxId = 1;
                        } else {
                            $maxId += 1;
                        }

                        DB::TABLE('ref_doc_no')->insert([
                            'id' => $maxId,
                            'year' => date('Y'),
                            'current_no' => 1,
                            'next_no' => 2
                        ]);

                    } else {
                        $id = $refDoc->next_no;

                        DB::TABLE('ref_doc_no')
                                ->where('id', $refDoc->id)
                                ->update([
                                    'current_no' => $refDoc->next_no,
                                    'next_no' => $refDoc->next_no + 1
                                ]);
                    }

                    $year = date('y');
                    $bulan = date('n');
                    $romawi = $this->getRomawi($bulan);
                    $qsNo = 'QS/' . $id . '/' . $codeProduct . '/HSIK-BM/' . $romawi . '/'. $year;
                    $arrData['qs_no_value'] = $qsNo;
                }
            }


            if ( is_null($arrData['rate']) ) {
                $arrData['rate'] = 0;
            }

            if ( is_null($arrData['additional_rate']) ) {
                $arrData['additional_rate'] = 0;
            }

            if ( is_null($arrData['discount']) ) {
                $arrData['discount'] = 0;
            }

            if ( !is_null($arrData['total_sum_insured']) ) {
                $totalSumInsured = str_replace('.', '', $arrData['total_sum_insured']);
                $totalSumInsured = str_replace(',', '.', $totalSumInsured);
                $totalSumInsured = (float)$totalSumInsured;
            } else {
                $totalSumInsured = 0;
            }

            if ( !is_null($arrData['third_party_limit'])) {
                $thirdPartyLimit = str_replace('.', '', $arrData['third_party_limit']);
                $thirdPartyLimit = str_replace(',', '.', $thirdPartyLimit);
                $thirdPartyLimit = (float)$thirdPartyLimit;
            } else {
                $thirdPartyLimit = 0;
            }

            if ( !is_null($arrData['personal_accident']) ) {
                $personalAccident = str_replace('.', '', $arrData['personal_accident']);
                $personalAccident = str_replace(',', '.', $personalAccident);
                $personalAccident = (float)$personalAccident;
            } else {
                $personalAccident = 0;
            }

            $lastId = DB::TABLE('master_qs_vehicle')->max('id');
            if ( is_null($lastId) ) {
                $lastId = 1;
            } else {
                $lastId += 1;
            }

            $valutaId = explode('_', $arrData['valuta_id'])[0];

            if ( is_null($request->from) ) {
                $from = NULL;
            } else {
                $from = date('Y-m-d', strtotime($request->from));
            }

            if ( is_null($request->to) ) {
                $to = NULL;
            } else {
                $to = date('Y-m-d', strtotime($request->to));
            }

            $ttlSI = 0;
            $ttlPremi = 0;
            $ttlAddPremi = 0;
            $ttlGrossPremi = 0;
            $ttlGrossPremi2 = 0; //yang dipake
            $ttlDiscFeet = 0;
            $ttlPremiAfterFeet = 0;
            $ttlAddDisc = 0;
            $ttlPremiFinal = 0;
            $ratePremi = 0;
            $sumInsured = 0;
            $arrGrossPremi = []; // untuk menampung hasil perhitungan gross premi (PRODUCT KMR)


            foreach ( $detail as $dt ) {

                if ( $dt->value[0]->value == 't' && $dt->value[0]->bit_id == 12) {

                    $jmlLoop = count($dt->value)-1;

                    foreach ( $dt->value as $i => $item ) {
                        // Yang Dihitung hanya yang aktif
                        switch ( $item->bit_id ) {
                            case '89':
                                $ratePremi = str_replace('.', '', $item->value);
                                $ratePremi = str_replace(',', '.', $ratePremi);
                                $ratePremi = (float)$ratePremi;

                                // $ratePremi = (int)$item->value;
                            break;

                            case '8' :
                                // Sum Insured
                                $SI = str_replace('.', '', $item->value);
                                $SI = str_replace(',', '.', $SI);
                                $sumInsured = (float)$SI;
                                $ttlSI += (float)$SI;
                            break;
                            case '21' :
                                // Premium
                                $Premium = str_replace('.', '', $item->value);
                                $Premium = str_replace(',', '.', $Premium);
                                $ttlPremi += (float)$Premium;
                            break;
                            case '28' :
                                // Total Add Premi
                                $addPremi = str_replace('.', '', $item->value);
                                $addPremi = str_replace(',', '.', $addPremi);
                                $ttlAddPremi += (float)$addPremi;
                            break;
                            case '29' :
                                // Gross Premi
                                $grossPremi = str_replace('.', '', $item->value);
                                $grossPremi = str_replace(',', '.', $grossPremi);
                                $ttlGrossPremi += (float)$grossPremi;
                            break;
                            case '30' :
                                // Discount Feet
                                $discFeet = str_replace('.', '', $item->value);
                                $discFeet = str_replace(',', '.', $discFeet);
                                $ttlDiscFeet += (float)$discFeet;
                            break;
                            case '31' :
                                // Premi After Feet
                                $premiAfterFeet = str_replace('.', '', $item->value);
                                $premiAfterFeet = str_replace(',', '.', $premiAfterFeet);
                                $ttlPremiAfterFeet += (float)$premiAfterFeet;
                            break;
                            case '32' :
                            // case '1' :
                                // Additional Discount
                                $addDiscount = str_replace('.', '', $item->value);
                                $addDiscount = str_replace(',', '.', $addDiscount);
                                $ttlAddDisc += (float)$addDiscount;
                            break;
                            case '33' :
                                // Premium Final
                                $premiFinal = str_replace('.', '', $item->value);
                                $premiFinal = str_replace(',', '.', $premiFinal);
                                $ttlPremiFinal += (float)$premiFinal;
                            break;
                        }

                        if ( $jmlLoop == $i && $arrData['underwriter_id'] == '19' ) {
                            // Khusus Product KMR
                            // Hitung Gross Premium di Index Terakhir
                            $grossPremi = (($sumInsured * $ratePremi) / 100);
                            $arrGrossPremi[] = number_format($grossPremi, 2, ',', '.');
                            $ttlGrossPremi2 += $grossPremi;
                        } // end if


                    }
                }

            } // end foreach


            $totalSumInsured = $ttlSI;

            $arrData['comprehensive_amount'] = $ttlPremi; // Premiun
            if ( $arrData['underwriter_id'] == '19' ) {
                // PRODUK KMR
                $arrData['gross_premium_value'] = $ttlGrossPremi2;
            } else {
                $arrData['gross_premium_value'] = $ttlGrossPremi;
            }

            $arrData['discount_amount_value'] = $ttlDiscFeet;
            $arrData['nett_premium_value'] = $ttlPremiAfterFeet;


            if ( Auth::user()->user_role_id == 11 ) {
                // Input oleh user client
                $url = 'quotation_slip.client';

                $dataCustomer = DB::TABLE('master_customer')
                                    ->where('id', Auth::user()->customer_id)
                                    ->first();

                $arrData['customer'] = Auth::user()->customer_id;
                $arrData['security'] = NULL;
                $arrData['is_client_create'] = 't';


                if ( is_null($arrData['id']) ) {

                    // INSERT DATA status_qs_client

                    DB::TABLE('status_qs_client')->where('qs_id', $lastId)->delete();

                    DB::TABLE('status_qs_client')->insert([
                        'customer_id' => $arrData['customer'],
                        'qs_id' => $lastId,
                        'wf_status_client_id' => 1 // NEW
                    ]);

                } else {
                    // Perbaikan Reject (update status jadi baru lagi)
                    DB::TABLE('status_qs_client')
                        ->where('qs_id', $arrData['id'])
                        ->update([
                            'wf_status_client_id' => 1 // NEW
                        ]);
                }

            } else {
                // Role Marketing
                if ( is_null($arrData['id']) ) {
                    // Insert Data
                    $arrData['is_client_create'] = 'f';
                    $url = 'quotation_slip';

                } else {
                    // Edit Data
                    $dataQS = DB::TABLE('master_qs_vehicle')->where('id', $arrData['id'])->first();

                    if ( $dataQS->is_client_create ) {
                        $arrData['is_client_create'] = 't';
                        $url = 'quotation_slip.client';

                    } else {
                        $arrData['is_client_create'] = 'f';
                        $url = 'quotation_slip';
                    }


                }


            } // end if


            if ( is_null($arrData['id']) ) {
                // INSERT
                // Insert Detail

                DB::TABLE('master_qs_vehicle')
                    ->insert([
                        'id' => $lastId,
                        'customer_id' => $arrData['customer'],
                        'form_wording' => $arrData['form_wording'],
                        'the_business' => $arrData['the_business'],
                        // 'period_of_insurance' => $arrData['periode_of_insurance'],
                        // 'no_of_insured' => $arrData['number_of_insured'],
                        'no_of_insured' => count($detail),
                        // 'make_type' => $arrData['type'],
                        // 'colour' => $arrData['colour'],
                        // 'year_built' => $arrData['year_built'],
                        // 'police_reg_no' => $arrData['police_reg_no'],
                        // 'chassis_no' => $arrData['chassis_no'],
                        // 'engine_no' => $arrData['engine_no'],
                        // 'accessories' => $arrData['accessories'],
                        'total_sum_insured' => $totalSumInsured,
                        'add_premi' => $ttlAddPremi,
                        'add_disc' => $ttlAddDisc,
                        'premium_final' => $ttlPremiFinal,
                        'coverage' => $arrData['coverage_content'],
                        'main_exclusions' => $arrData['main_exclusions_content'],
                        'deductible' => $arrData['deductible_content'],
                        'clauses' => $arrData['clauses_content'],
                        'rate' => $arrData['rate'],
                        'add_rate' => $arrData['additional_rate'],
                        'third_party_limit' => $thirdPartyLimit,
                        'personal_accident' => $personalAccident,
                        'discount' => $arrData['discount'],
                        'comprehensive_amount' => $arrData['comprehensive_amount'],
                        'flood_amount' => $arrData['flood_amount'],
                        'tpl_limit' => $arrData['tpl_limit_amount'],
                        'pa_driver' => $arrData['pa_driver_amount'],
                        'pa_passenger' => $arrData['pa_passenger_amount'],
                        'discount_amount' => $arrData['discount_amount_value'],
                        'security' => NULL,
                        'user_crt_id' => Auth::user()->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'is_active' => 't',
                        'qs_status' =>  0,
                        'qs_status' =>  NULL,
                        'qs_no' => $arrData['qs_no_value'],
                        'gross_premium' => $arrData['gross_premium_value'],
                        'nett_premium' => $arrData['nett_premium_value'],
                        'underwriter_id' => $arrData['security'],
                        'product_id' => $arrData['underwriter_id'],
                        'valuta_id' => $valutaId,
                        'from_date' => $from,
                        'to_date' => $to,
                        'buss_source_id' => $arrData['source_business'],
                        'policy_name' => $arrData['policy_name'],
                        'is_client_create' => $arrData['is_client_create']
                    ]);

                if ( count($detail) > 0 ) {
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');
                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    $row = 1;
                    foreach ( $detail as $idx => $item ) {
                        foreach ( $item->value as $data ) {

                            if ( $data->bit_id == '29' && $arrData['underwriter_id'] == '19') {
                                // Khusus untuk Produk KMR (GROSS PREMI)
                                $data->value = $arrGrossPremi[$idx];
                            }

                            // INSERT MASTER QS DETAIL
                            DB::TABLE('master_qs_detail')
                                ->insert([
                                    'id' => $id_qs_detail,
                                    'doc_id' => $lastId,
                                    'bit_id' => $data->bit_id,
                                    'value_text' => $data->value,
                                    'value_int' => $row,
                                    'doc_off_type' => 0,
                                ]);
                            $id_qs_detail++;
                        }
                        $row++;
                    }
                } // end if

            } else {

                // Update Detail
                // $detail = json_decode($arrData['detail']);

                if ( Auth::user()->user_role_id != 11 ) {
                    // Hanya oleh user marketing
                    $lastCountEdit = DB::TABLE('master_qs_vehicle')
                                    ->where('id', $arrData['id'])
                                    ->max('count_edit');

                    if ( is_null($lastCountEdit) ) {
                        $lastCountEdit = 1;
                    } else {
                        $lastCountEdit += 1;
                    }

                } else {
                    $lastCountEdit = NULL;
                }

                // UPDATE
                DB::TABLE('master_qs_vehicle')
                    ->where('id', $arrData['id'])
                    ->update([
                        'customer_id' => $arrData['customer'],
                        'form_wording' => $arrData['form_wording'],
                        'the_business' => $arrData['the_business'],
                        'no_of_insured' => count($detail),
                        'total_sum_insured' => $totalSumInsured,
                        'add_premi' => $ttlAddPremi,
                        'add_disc' => $ttlAddDisc,
                        'premium_final' => $ttlPremiFinal,
                        'coverage' => $arrData['coverage_content'],
                        'main_exclusions' => $arrData['main_exclusions_content'],
                        'deductible' => $arrData['deductible_content'],
                        'clauses' => $arrData['clauses_content'],
                        'rate' => $arrData['rate'],
                        'add_rate' => $arrData['additional_rate'],
                        'third_party_limit' => $thirdPartyLimit,
                        'personal_accident' => $personalAccident,
                        'discount' => $arrData['discount'],
                        'comprehensive_amount' => $arrData['comprehensive_amount'],
                        'flood_amount' => $arrData['flood_amount'],
                        'tpl_limit' => $arrData['tpl_limit_amount'],
                        'pa_driver' => $arrData['pa_driver_amount'],
                        'pa_passenger' => $arrData['pa_passenger_amount'],
                        'discount_amount' => $arrData['discount_amount_value'],
                        'security' => NULL,
                        'user_upd_id' => Auth::user()->id,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'is_active' => 't',
                        'qs_status' =>  0,
                        'qs_status' =>  NULL,
                        'qs_no' => $arrData['qs_no_value'],
                        'gross_premium' => $arrData['gross_premium_value'],
                        'nett_premium' => $arrData['nett_premium_value'],
                        'underwriter_id' => $arrData['security'],
                        'product_id' => $arrData['underwriter_id'],
                        'valuta_id' => $valutaId,
                        'from_date' => $from,
                        'to_date' => $to,
                        'buss_source_id' => $arrData['source_business'],
                        'count_edit' => $lastCountEdit,
                        'policy_name' => $arrData['policy_name']
                    ]);

                // Delete Data Detail Sebelumnya
                DB::TABLE('master_qs_detail')
                        ->where('doc_id', $arrData['id'])
                        ->delete();

                if ( count($detail) > 0 ) {
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');
                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    $row = 1;
                    foreach ( $detail as $idx => $item ) {
                        foreach ( $item->value as $data ) {

                            if ( $data->bit_id == '29' && $arrData['underwriter_id'] == '19') {
                                // Khusus untuk Produk KMR (GROSS PREMI)
                                $data->value = $arrGrossPremi[$idx];
                            }

                            // INSERT MASTER QS DETAIL
                            DB::TABLE('master_qs_detail')
                                ->insert([
                                    'id' => $id_qs_detail,
                                    'doc_id' => $arrData['id'],
                                    'bit_id' => $data->bit_id,
                                    'value_text' => $data->value,
                                    'value_int' => $row,
                                    'doc_off_type' => 0,
                                ]);
                            $id_qs_detail++;
                        }
                        $row++;
                    }
                } // end if
            } // end if


            DB::commit();

            return response()->json([
                'rc' => 0,
                'rm' => 'Sukses',
                'url' => $url
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }


    }

    public function show(Request $request, $id_vehicle, $type) {

        $data = DB::TABLE('master_qs_vehicle')
                    ->select(
                                'master_qs_vehicle.*',
                                'master_customer.address',
                                'ref_product.definition as product',
                                'ref_agent.full_name'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_qs_vehicle.product_id')
                    ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_customer.agent_id')
                    ->where('master_qs_vehicle.id', $id_vehicle)
                    ->first();

        // Cek apakah ada penambahan kolom detail baru
        $columnDetail = DB::TABLE('master_qs_detail')
                            ->where('master_qs_detail.doc_id', $id_vehicle)
                            ->where('doc_off_type', 0)
                            ->get();

        $arrBitId = $columnDetail->pluck('bit_id')->toArray();

        $newBitId = DB::TABLE('ref_insured_detail')
                        ->where('product_id', $data->product_id)
                        ->whereNotIn('bit_id', $arrBitId)
                        ->get();


        if ( count($newBitId) > 0 ) {
            // Ada kolom baru, insert data ke master qs detail

            $rowData = array_values($columnDetail->groupBy('value_int')->toArray());
            foreach ( $newBitId as $item ) {


                foreach ( $rowData as $value ) {
                    // Insert Ke Master QS Detail detail value ""
                    $id_qs_detail = DB::TABLE('master_qs_detail')->max('id');

                    if ( is_null($id_qs_detail) ) {
                        $id_qs_detail = 1;
                    } else {
                        $id_qs_detail += 1;
                    }

                    $row = $value[0]->value_int;

                    // INSERT MASTER QS DETAIL
                    DB::TABLE('master_qs_detail')
                        ->insert([
                            'id' => $id_qs_detail,
                            'doc_id' => $id_vehicle,
                            'bit_id' => $item->bit_id,
                            'value_text' => "",
                            'value_int' => $row,
                            'doc_off_type' => 0,
                        ]);
                }

            }
        }


        $param['data'] = $data;
        $param['type'] = $type;
        $param['products'] = DB::TABLE('ref_product')->where('is_active', 't')->get();
        $param['customer'] = DB::TABLE('master_customer')
                                ->where('is_active', 't')
                                ->where('branch_id', Auth::user()->branch_id)
                                ->orderBy('full_name', 'ASC')
                                ->get();

        $param['refUnderwriter'] = DB::TABLE('ref_underwriter')
                                        ->where('is_active', 't')
                                        ->get();

        $param['sourceBusiness'] = DB::TABLE('ref_business_source')
                                    ->where('is_active', 't')
                                    ->get();

        $param['headerTable'] =  DB::TABLE('ref_insured_detail')
                                    ->where('product_id', $data->product_id)
                                    ->where('is_active', 't')
                                    ->where('bit_id', '!=', 12)
                                    ->orderBy('bit_id', 'ASC')
                                    ->get();

        $detail1 = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name')
                    ->leftJoin('master_qs_vehicle', 'master_qs_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_qs_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id_vehicle)
                    ->where('master_qs_detail.bit_id', '!=', 12)
                    ->where('master_qs_detail.doc_off_type', 0)
                    ->where('ref_insured_detail.is_active', 't')
                    ->orderBy('bit_id', 'ASC');

        $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name')
                    ->leftJoin('master_qs_vehicle', 'master_qs_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_qs_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id_vehicle)
                    ->where('master_qs_detail.bit_id', 12)
                    ->where('master_qs_detail.doc_off_type', 0)
                    ->unionAll($detail1)
                    ->get();

        $detail = array_values($detail->groupBy('value_int')->toArray());

        $param['detail'] = $detail;
        $param['ref_valuta'] = DB::TABLE('ref_valuta')->get();

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'quotation_slip.edit',$param);
        }else {
            return view('master.master')->nest('child', 'quotation_slip.edit',$param);
        }
    }

    public function setCancel(Request $request) {
        $isCancel = $request->cancel;
        $id = $request->id;

        $rm;
        $data = DB::TABLE('master_qs_vehicle')
                    ->where('id', $id);

        if ( $isCancel == 't' ) {
            $data->update(['is_active' => 'f']);
            $rm = 'Berhasil dibatalkan';
        } else {
            $data->update(['is_active' => 't']);
            $rm = 'Berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $rm
        ]);

    }

    public function export(Request $request, $id) {
        $data = DB::TABLE('master_qs_vehicle')
                    ->select(
                                'master_qs_vehicle.*',
                                'master_customer.address',
                                'ref_product.definition',
                                'master_customer.full_name',
                                'ref_underwriter.definition as security',
                                'ref_business_source.business_def',
                                'ref_valuta.mata_uang'
                            )
                    ->leftJoin('master_customer', 'master_customer.id', '=', 'master_qs_vehicle.customer_id')
                    ->leftJoin('ref_product', 'ref_product.id', '=', 'master_qs_vehicle.product_id')
                    ->leftJoin('ref_underwriter', 'ref_underwriter.id', '=', 'master_qs_vehicle.underwriter_id')
                    ->leftJoin('ref_business_source', 'ref_business_source.id', '=', 'master_qs_vehicle.buss_source_id')
                    ->leftJoin('ref_valuta', 'ref_valuta.id', '=', 'master_qs_vehicle.valuta_id')
                    ->where('master_qs_vehicle.id', $id)
                    ->first();

        $colsName = DB::TABLE('ref_insured_detail')
                        ->where('product_id', $data->product_id)
                        ->where('is_active', 't')
                        ->where('bit_id', '!=', 12)
                        ->orderBy('id', 'ASC')
                        ->get();

        $parent = DB::TABLE('master_qs_detail')
                        ->select('value_int')
                        ->distinct()
                        ->where('doc_id', $id)
                        ->where(function($query) {
                            $query->where('bit_id', 12);
                            $query->where('value_text', 'f');
                        })
                        ->get();


        $parent = $parent->pluck('value_int')->toArray();

        $detail = DB::TABLE('master_qs_detail')
                    ->select('master_qs_detail.*', 'ref_insured_detail.input_name', 'ref_insured_detail.label', 'ref_insured_detail.is_number')
                    ->distinct()
                    ->leftJoin('master_qs_vehicle', 'master_qs_vehicle.id', '=', 'master_qs_detail.doc_id')
                    ->leftJoin('ref_insured_detail', function($join) {
                        $join->on('ref_insured_detail.product_id', '=', 'master_qs_vehicle.product_id');
                        $join->on('master_qs_detail.bit_id', '=', 'ref_insured_detail.bit_id');
                    })
                    ->where('master_qs_detail.doc_id', $id)
                    ->where('master_qs_detail.bit_id', '!=', 12)
                    ->where('master_qs_detail.doc_off_type', 0)
                    ->whereNotIn('master_qs_detail.value_int', $parent)
                    ->get();


        $detail = array_values($detail->groupBy('value_int')->toArray());
        $data->no_of_insured = count($detail);
        $param['detail'] = $detail;
        $param['data'] = $data;

        // $m = new Merger();
        // $pdf = PDF::loadView('quotation_slip.pdf_quotation_slip',$param)->setPaper('a4', 'potrait');
        // $m->addRaw($pdf->output());

        // $dt = $detail;
        // $detail['colsname'] = $colsName->sortBy('bit_id');
        // $detail['detail'] = $dt;
        // $pdf = PDF::loadView('quotation_slip.pdf_lampiran_insured',$detail)->setPaper('a4', 'landscape');
        // $m->addRaw($pdf->output());

        // file_put_contents(public_path('test_1.pdf'), $m->merge());

        $pdf = PDF::loadView('quotation_slip.pdf_quotation_slip',$param)->setPaper('a4', 'potrait');
        $pdf->getDomPDF()->set_option("enable_php", true);
         return $pdf->stream('QS ' . $data->full_name . ' - ' . date('d F Y') . '.pdf');
    } // end function

    public function getQsContent(Request $request) {

        $data = DB::TABLE('ref_qs_content')
                        ->where('product_id', $request->id)
                        ->where('is_active', 't')
                        ->first();

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data
        ]);
    }

    public function get_insured_detail(Request $request) {

        $data = DB::TABLE('ref_insured_detail')
                    ->where('product_id', $request->product_id)
                    ->where('is_active', 't')
                    ->where('bit_id', '!=', 12)
                    ->orderBy('bit_id', 'ASC')
                    ->get();

        $form = "";
        $input = "";
        $rowCount = 0;
        $numOfCols = 3;
        $bootstrapColWidth = 12 / $numOfCols;

        $numberOfColumns = 3;
        $bootstrapColWidth = 12 / $numberOfColumns ;
        $arrayChunks = array_chunk($data->toArray(), $numberOfColumns);
        $form_elm = "";
        $form .= '<div class="row">';
        foreach ( $arrayChunks as $items ) {
            // $form = "";
            foreach ( $items as $i => $item ) {
                $input = "";

                // JIKA STATUS CHECK
                if($item->bit_id == 91 || $item->is_hidden){
                    $input .= view('form_render.input_hidden', [
                        'name' => $item->input_name . '-' . $item->bit_id
                    ]);
                }else{
                    if ( $item->is_date ) {
                        // Format Tanggal
                        $input .= view('form_render.input_date', [
                            'label' => $item->label,
                            'name' => $item->input_name . '-' . $item->bit_id,
                            'is_required' => $item->is_required,
                        ]);
                    } else if ( $item->is_number && $item->is_rate ) {
                        // Format Presentase
                        $input .= view('form_render.input_rate', [
                            'label' => $item->label,
                            'name' => $item->input_name . '-' . $item->bit_id,
                            'is_required' => $item->is_required,
                        ]);
                    } else {
                        $input .= view('form_render.input_text', [
                            'label' => $item->label,
                            'name' => $item->input_name . '-' . $item->bit_id,
                            'is_number' => $item->is_number,
                            'is_required' => $item->is_required,
                            'is_currency' => $item->is_currency
                        ]);
                    }
                }




                $form .= '<div class="col-md-4" id="v-'.$item->input_name . '-' . $item->bit_id.'">
                            '.$input.'
                          </div>';
            }

            // if ( !$item->is_number ) {
            //     $form .= view('form_render.input_text', [
            //         'label' => $item->label,
            //         'name' => $item->input_name . '-' . $item->bit_id
            //     ]);
            // } else {
            //     $form .= view('form_render.input_number', [
            //         'label' => $item->label,
            //         'name' => $item->input_name . '-' . $item->bit_id
            //     ]);
            // }

        }
        $form .= '</div">';
        $form_elm .= $form;

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data,
            'view' => $form_elm
        ]);

    }

    public function getDetailInsured(Request $request, $id) {
        $column = [];
        $arr = [];
        // $data = DB::TABLE('ref_insured_detail')
        //             ->where('product_id', $id)
        //             ->where('is_active', 't')
        //             ->where('bit_id', '!=', 12)
        //             ->orderBy('bit_id', 'ASC')
        //             ->get();

        $data =  \DB::select("SELECT * from ref_insured_detail
        where product_id = ? and is_active = 't' and bit_id <> 12 order by bit_id asc", [$id]);

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data
        ]);
    }

    public function sendApprove(Request $request)
    {

        DB::beginTransaction();
        try {

            $arrData = $request->all();
            $value = $arrData['value'];

            foreach ( $value as $id ) {

                $cek = DB::TABLE('status_qs_client')
                            ->where('qs_id', $id)
                            ->first();

                if ( $cek->wf_status_client_id == 1 || $cek->wf_status_client_id == 7) {
                    // Status New Atau Reject
                    DB::TABLE('status_qs_client')
                            ->where('qs_id', $id)
                            ->update([
                                'wf_status_client_id' => 2,
                            ]);
                }

            }

            DB::commit();

            return response()->json([
                'rc' => 0,
                'rm' => 'Sukses',
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }

    } // end function

    public function approve(Request $request)
    {
        DB::beginTransaction();
        try {

            $arrData = $request->all();
            $value = $arrData['value'];

            foreach ( $value as $id ) {

                $cek = DB::TABLE('status_qs_client')
                            ->where('qs_id', $id)
                            ->first();

                if ( $cek->wf_status_client_id == 2 ) {
                    DB::TABLE('status_qs_client')
                            ->where('qs_id', $id)
                            ->update([
                                'wf_status_client_id' => 3,
                                'notes' => NULL
                            ]);
                }

            }

            DB::commit();

            return response()->json([
                'rc' => 0,
                'rm' => 'Sukses',
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }

    } // end function

    public function reject(Request $request)
    {
        DB::beginTransaction();
        try {

            $arrData = $request->all();
            $value = $arrData['value'];

            foreach ( $value as $id ) {

                $cek = DB::TABLE('status_qs_client')
                            ->where('qs_id', $id)
                            ->first();

                // if ( $cek->wf_status_client_id == 2 || $cek->wf_status_client_id == 3) {
                if ( $cek->wf_status_client_id == 2 ) {
                    // 2 : Need Approve
                    // 3 : Sudah di approve (QS)

                    DB::TABLE('status_qs_client')
                            ->where('qs_id', $id)
                            ->update([
                                'wf_status_client_id' => 7,
                                'notes' => $arrData['SetMemo']
                            ]);
                }

            }

            DB::commit();

            return response()->json([
                'rc' => 0,
                'rm' => 'Sukses',
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    } // end function








}
