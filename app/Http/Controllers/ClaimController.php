<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;

class ClaimController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $ref_claim = \DB::select('SELECT * FROM ref_claim_status where is_active=true and company_id='.Auth::user()->company_id);
        $ref_clause = \DB::select('SELECT * FROM ref_cause_loss where is_active=true');
        $product = DB::TABLE('ref_product')->where('is_active', 't')->get();

        $param['products']=$product;
        $param['ref_claim']=$ref_claim;
        $param['id']=$request->get('id');
        $param['ref_clause']=$ref_clause;

        return view('master.master')->nest('child', 'claim.index',$param);

    }
     public function insert(Request $request){
        $results = \DB::select("SELECT max(id) AS id from master_claim");
        $id='';
        if($results){
            $id=$results[0]->id + 1;
        }else{
            $id=1;
        }

        $filename='';
        $filename1='';
        $filename2='';
        $filename3='';
        if($request->file('file_pla')){

            $destination_path = public_path('upload_claim');
            $files = $request->file('file_pla');
            $filename = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $id."_pla_".$filename);

        }

        if($request->file('file_dla')){
            $destination_path1 = public_path('upload_claim');
            $files1 = $request->file('file_dla');
            $filename1 = $files1->getClientOriginalName();
            $upload_success1 = $files1->move($destination_path1, $id."_dla_".$filename1);

        }

        if($request->file('file_lod')){

            $destination_path2 = public_path('upload_claim');
            $files2 = $request->file('file_lod');
            $filename2 = $files2->getClientOriginalName();
            $upload_success2 = $files2->move($destination_path2, $id."_lod_".$filename2);

        }

        if($request->file('file_claim')){

            $destination_path3 = public_path('upload_claim');
            $files3 = $request->file('file_claim');
            $filename3 = $files3->getClientOriginalName();
            $upload_success3 = $files3->move($destination_path3, $id."_claim_".$filename3);

        }





        if ( is_null($request->input('estimation')) ) {
            $nominal1 = 0;
        } else {
            $nominal=str_replace('.', '', $request->input('estimation'));
            $nominal1=str_replace(',','.', $nominal);
        }


        if($request->input('claim_amount')){
            $nominal2=str_replace('.', '', $request->input('claim_amount'));
            $nominal3=str_replace(',','.', $nominal2);
        } else {
            $nominal3=0;
        }

        $date1 = date('Y-m-d',strtotime($request->input('tgl_claim')));
        if ( is_null($request->input('tgl_claim_end'))) {
            $date2 = null;
        } else {
            $date2 = date('Y-m-d',strtotime($request->input('tgl_claim_end')));
        }


        $diff= abs(strtotime($date2) - strtotime($date1));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        $durasi=$days;


        if($request->input('id')){

            DB::table('master_claim')
                ->where('id', $request->input('id'))
                ->update([
                    'claim_status_id' => $request->input('claim_id'),
                    'polis_no' => $request->input('police_number'),
                    'claim_notes' => $request->input('notes'),
                    'loss_date' => date('Y-m-d',strtotime($request->input('tgl_loss'))),
                    'claim_end_date' => date('Y-m-d',strtotime($request->input('tgl_claim_end'))),
                    'claim_start_date' => date('Y-m-d',strtotime($request->input('tgl_claim'))),
                    'loss_amount' => $nominal1,
                    'claim_amount' => $nominal3,
                    'kronologi_kejadian' => $request->input('kronologi'),
                    'cause_loss_id' => $request->input('loss_id'),
                    'url_doc_pla' => $filename ? $filename : '',
                    'url_doc_dla' => $filename1 ? $filename1 : '' ,
                    'url_doc_lod' => $filename2 ? $filename2 : '',
                    'url_doc' => $filename3 ? $filename3 : '',
                    'duration' => $durasi,
                    'user_crt_id' => Auth::user()->id,
                    'workflow_status_id'=>5

                ]);

        }else{

            DB::table('master_claim')->insert(
                [
                    'id' => $id,
                    'claim_status_id' => $request->input('claim_id'),
                    'polis_no' => $request->input('police_number'),
                    'claim_notes' => $request->input('notes'),
                    'loss_date' => date('Y-m-d',strtotime($request->input('tgl_loss'))),
                    'claim_end_date' => date('Y-m-d',strtotime($request->input('tgl_claim_end'))),
                    'claim_start_date' => date('Y-m-d',strtotime($request->input('tgl_claim'))),
                    'loss_amount' => $nominal1,
                    'claim_amount' => $nominal3,
                    'kronologi_kejadian' => $request->input('kronologi'),
                    'cause_loss_id' => $request->input('loss_id'),
                    'url_doc_pla' => $filename ? $filename : '',
                    'url_doc_dla' => $filename1 ? $filename1 : '' ,
                    'url_doc_lod' => $filename2 ? $filename2 : '',
                    'url_doc' => $filename3 ? $filename3 : '',
                    'duration' => $durasi,
                    'user_crt_id' => Auth::user()->id,
                    'workflow_status_id'=>1,
                    'qs_doc_id' => $request->input('qs_doc_id')
                ]
            );

        }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }
    public function byClaim(Request $request){
      $data = \DB::select('SELECT * FROM master_claim where id='.$request->get('id'));
      return json_encode($data);
    }

    public function detail_claim(Request $request){

      $data1 = \DB::select('SELECT a.*,b.cause_definition,c.status_definition FROM master_claim a
        left join ref_cause_loss b on a.cause_loss_id=b.id
        left join ref_claim_status c on a.claim_status_id=c.id where a.id='.$request->get('id'));

      $data = \DB::select('SELECT a.*,b.cause_definition,c.status_definition FROM master_claim_time a
        left join ref_cause_loss b on a.cause_loss_id=b.id
        left join ref_claim_status c on a.claim_status_id=c.id where a.id='.$request->get('id'));

      return json_encode(['data'=>$data,'data1'=>$data1]);
    }

    public function delete_claim(Request $request){
      $data = DB::table('master_claim')->where('id', $request->get('id'))->delete();
      return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }

    public function send_claim(Request $request){
         foreach ($request->value as $item) {
            DB::table('master_claim')
            ->where('id', $item)
            ->update(['workflow_status_id' => 2]);
         }

      return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }

    public function approve_claim(Request $request){
        if($request->get('type')=='semua'){
            $data = \DB::select('SELECT * FROM master_claim where workflow_status_id=2');

            foreach ($data as $item) {
                $data_time = \DB::select('SELECT * FROM master_claim_time where id='.$item->id);

                if($data_time){
                    DB::table('master_claim_time')
                    ->where('id', $item->id)
                    ->update([

                        'claim_status_id' => $item->claim_status_id,
                        'polis_no' => $item->polis_no,
                        'claim_notes' => $item->claim_notes,
                        'loss_date' => date('Y-m-d',strtotime($item->loss_date)),
                        'claim_end_date' => date('Y-m-d',strtotime($item->claim_end_date)),
                        'claim_start_date' => date('Y-m-d',strtotime($item->claim_start_date)),
                        'loss_amount' => $item->loss_amount,
                        'claim_amount' => $item->claim_amount,
                        'kronologi_kejadian' => $item->kronologi_kejadian,
                        'cause_loss_id' => $item->cause_loss_id,
                        'url_doc_pla' => $item->url_doc_pla,
                        'url_doc_dla' => $item->url_doc_dla,
                        'url_doc_lod' => $item->url_doc_lod,
                        'url_doc' => $item->url_doc,
                        'duration' => $item->duration,
                        'user_crt_id' => Auth::user()->id,
                        'workflow_status_id'=>3

                    ]);

                }else{
                    DB::table('master_claim_time')->insert(
                        [
                            'id' => $item->id,
                            'claim_status_id' => $item->claim_status_id,
                            'polis_no' => $item->polis_no,
                            'claim_notes' => $item->claim_notes,
                            'loss_date' => date('Y-m-d',strtotime($item->loss_date)),
                            'claim_end_date' => date('Y-m-d',strtotime($item->claim_end_date)),
                            'claim_start_date' => date('Y-m-d',strtotime($item->claim_start_date)),
                            'loss_amount' => $item->loss_amount,
                            'claim_amount' => $item->claim_amount,
                            'kronologi_kejadian' => $item->kronologi_kejadian,
                            'cause_loss_id' => $item->cause_loss_id,
                            'url_doc_pla' => $item->url_doc_pla,
                            'url_doc_dla' => $item->url_doc_dla,
                            'url_doc_lod' => $item->url_doc_lod,
                            'url_doc' => $item->url_doc,
                            'duration' => $item->duration,
                            'user_crt_id' => Auth::user()->id,
                            'workflow_status_id'=>3
                        ]
                    );

                }

                DB::table('master_claim')
                ->where('id', $item->id)
                ->update(['workflow_status_id' => 3]);


            }

        }else{

            foreach ($request->value as $value) {
                $data = \DB::select('SELECT * FROM master_claim where id='.$value);

                foreach ($data as $item) {

                    $data_time = \DB::select('SELECT * FROM master_claim_time where id='.$item->id);

                    if($data_time){
                        DB::table('master_claim_time')
                        ->where('id', $item->id)
                        ->update([

                            'claim_status_id' => $item->claim_status_id,
                            'polis_no' => $item->polis_no,
                            'claim_notes' => $item->claim_notes,
                            'loss_date' => date('Y-m-d',strtotime($item->loss_date)),
                            'claim_end_date' => date('Y-m-d',strtotime($item->claim_end_date)),
                            'claim_start_date' => date('Y-m-d',strtotime($item->claim_start_date)),
                            'loss_amount' => $item->loss_amount,
                            'claim_amount' => $item->claim_amount,
                            'kronologi_kejadian' => $item->kronologi_kejadian,
                            'cause_loss_id' => $item->cause_loss_id,
                            'url_doc_pla' => $item->url_doc_pla,
                            'url_doc_dla' => $item->url_doc_dla,
                            'url_doc_lod' => $item->url_doc_lod,
                            'url_doc' => $item->url_doc,
                            'duration' => $item->duration,
                            'user_crt_id' => Auth::user()->id,
                            'workflow_status_id'=>3

                        ]);

                    }else{
                        DB::table('master_claim_time')->insert(
                            [
                                'id' => $item->id,
                                'claim_status_id' => $item->claim_status_id,
                                'polis_no' => $item->polis_no,
                                'claim_notes' => $item->claim_notes,
                                'loss_date' => date('Y-m-d',strtotime($item->loss_date)),
                                'claim_end_date' => date('Y-m-d',strtotime($item->claim_end_date)),
                                'claim_start_date' => date('Y-m-d',strtotime($item->claim_start_date)),
                                'loss_amount' => $item->loss_amount,
                                'claim_amount' => $item->claim_amount,
                                'kronologi_kejadian' => $item->kronologi_kejadian,
                                'cause_loss_id' => $item->cause_loss_id,
                                'url_doc_pla' => $item->url_doc_pla,
                                'url_doc_dla' => $item->url_doc_dla,
                                'url_doc_lod' => $item->url_doc_lod,
                                'url_doc' => $item->url_doc,
                                'duration' => $item->duration,
                                'user_crt_id' => Auth::user()->id,
                                'workflow_status_id'=>3
                            ]
                        );

                    }



                }




                DB::table('master_claim')
                ->where('id', $value)
                ->update(['workflow_status_id' => 3]);
            }
        }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }

    public function reject_claim(Request $request){
        if($request->get('type')=='semua'){
            $data = \DB::select('SELECT * FROM master_claim where workflow_status_id=2');

            foreach ($data as $item) {

                 DB::table('master_claim')
                ->where('id', $item->id)
                ->update(['workflow_status_id' => 4]);
            }

        }else{

            foreach ($request->value as $item) {
                DB::table('master_claim')
                ->where('id', $item)
                ->update(['workflow_status_id' => 4]);
            }
        }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }


     public function list_approval(Request $request)
    {
        $data = \DB::select('SELECT a.*,b.cause_definition FROM master_claim a left join ref_cause_loss b on b.id=a.cause_loss_id where workflow_status_id=2');

        $param['data']=$data;

        return view('master.master')->nest('child', 'claim.list_approval',$param);

    }

    public function getDetailProduct(Request $request) {
        $data = DB::TABLE('ref_insured_detail')
                ->where('product_id', $request->id)
                ->where('is_filterable', 't')
                ->get();

        return response()->json([
            'rc' => 1,
            'data' => $data
        ]);
    }

    public function getListKeyword(Request $request)
    {
        $data = DB::TABLE('master_qs_detail')
                    ->where('bit_id', $request->bit_id)
                    ->get();

        return response()->json([
            'rc' => 1,
            'data' => $data
        ]);
    }

    public function getListDataFilter(Request $request)
    {



        $arrData = $request->all();

        if ( $request->bit_id == '9999' ) {
            // Cari Data Berdasarkan No Polis
            $noPolis = $request->keyword;
            $arrIdQS = NULL;
            $customerName = NULL;
        } else if ( $request->bit_id == '8888' ) {
            $customerName = strtolower(trim($request->keyword));
            $arrIdQS = NULL;
            $noPolis = NULL;
        } else {
            $data = DB::TABLE('master_qs_detail')
                    ->where('bit_id', $request->bit_id)
                    ->whereRAW("LOWER(value_text) LIKE '" . strtolower($request->keyword) . "%'")
                    // ->where('value_text', 'LIKE', $request->keyword . '%')
                    ->where('doc_off_type', 3) // Master Sales Polis
                    ->get();

            $arrIdQS = $data->pluck('doc_id');
            $noPolis = NULL;
            $customerName = NULL;
        }


        // dd($data);


        $data = DB::TABLE('master_sales_polis')
                        ->select(
                            'master_customer.full_name',
                            'master_sales_polis.*',
                            'ref_product.definition',
                            'ref_agent.full_name as agent',
                            'ref_bank_account.definition as bank_account'
                        )
                        ->leftJoin('master_customer', 'master_customer.id', '=', 'master_sales_polis.customer_id')
                        ->leftJoin('ref_product', 'ref_product.id', '=', 'master_sales_polis.product_id')
                        ->leftJoin('ref_agent', 'ref_agent.id', '=', 'master_sales_polis.agent_id')
                        ->leftJoin('ref_bank_account', 'ref_bank_account.id', '=', 'master_sales_polis.afi_acc_no')
                        ->when(!is_null($customerName), function ($query) use($customerName, $request) {
                            return $query->whereRAW("LOWER(master_customer.full_name) = '" . $customerName . "'")
                                        ->where('product_id', $request->product_id);
                        })
                        ->when(!is_null($arrIdQS), function ($query) use($arrIdQS){
                            return $query->whereIn('master_sales_polis.id', $arrIdQS);
                        })
                        ->when(!is_null($noPolis), function ($query) use($noPolis, $request){
                            return $query->where('polis_no', trim($noPolis))
                                        ->where('product_id', $request->product_id);
                        })
                        ->where('master_sales_polis.is_active', 't')
                        ->orderBy('master_sales_polis.id', 'DESC')
                        ->get();


                        // select
                        //         a.id,
                        //         b.full_name,
                        //         c.definition,
                        //         e.full_name as agent,
                        //         f.qs_no,g.definition as bank
                        //     from master_sales a
                        //     left join master_customer b on b.id=a.customer_id
                        //     left join ref_product c on c.id=a.product_id
                        //     left join ref_paid_status d on d.id=a.paid_status_id
                        //     left join ref_agent e on e.id=a.agent_id
                        //     left join ref_quotation f on f.id=a.qs_no
                        //     left join ref_bank_account g on g.id=a.afi_acc_no where a.polis_no
        return response()->json([
            'rc' => 1,
            'data' => $data
        ]);
    }

    public function getAllDetailProduct(Request $request) {

        $data = DB::TABLE('ref_insured_detail')
                ->where('product_id', $request->product_id)
                ->where('is_active', 't')
                ->where('bit_id', '!=', 12)
                ->orderBy('id', 'ASC')
                ->get();

        $form = "";
        $input = "";
        $rowCount = 0;
        $numOfCols = 3;
        $bootstrapColWidth = 12 / $numOfCols;

        $numberOfColumns = 3;
        $bootstrapColWidth = 12 / $numberOfColumns ;
        $arrayChunks = array_chunk($data->toArray(), $numberOfColumns);
        $form_elm = "";
        $form .= '<div class="row">';
        foreach ( $arrayChunks as $items ) {
            foreach ( $items as $i => $item ) {
                $input = "";
                if ( $item->is_number && $item->is_rate ) {
                    // Format Presentase
                    $input .= view('form_render.input_rate', [
                        'label' => $item->label,
                        'name' => $item->input_name . '-' . $item->bit_id,
                        'is_required' => $item->is_required,
                    ]);
                } else {
                    $input .= view('form_render.input_text', [
                        'label' => $item->label,
                        'name' => $item->input_name . '-' . $item->bit_id,
                        'is_number' => $item->is_number,
                        'is_required' => $item->is_required,
                        'is_currency' => $item->is_currency
                    ]);
                }


                $form .= '<div class="col-md-4">
                            '.$input.'
                        </div>';
            }
        }
        $form .= '</div">';
        $form_elm .= $form;

        $data_qs_detail = DB::TABLE('master_qs_detail')
                            ->select(
                                        'master_qs_detail.*',
                                        'ref_insured_detail.input_name'
                                    )
                            ->leftJoin('ref_insured_detail', 'ref_insured_detail.bit_id', '=', 'master_qs_detail.bit_id')
                            ->where('master_qs_detail.doc_id', $request->doc_id)
                            ->where('master_qs_detail.doc_off_type', 3)
                            ->where('master_qs_detail.bit_id', '!=', 12)
                            ->get();

        return response()->json([
            'rc' => 1,
            'rm' => 'Sukses',
            'data' => $data,
            'view' => $form_elm,
            'data_qs_detail' => $data_qs_detail
        ]);
    }

}



