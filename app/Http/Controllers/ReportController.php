<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use PDF;
use Hash;
use Auth;
use Excel;
use File;
use Request as Req;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{

    public function getShortCodeBranch($idBranch) {
        $dt = DB::TABLE('master_branch')->select('short_code')->where('id', $idBranch)->first();
        return $dt->short_code;
    }

    public function balance_sheet(Request $request)
    {

      // $idbranch = 'konsol';
      $idbranch = Auth::user()->branch_id;

      if( !is_null($request->get('id')) ){
        $idbranch = $request->get('id');

          if($request->get('id')=='konsol'){
            $idbranch = 'konsol';

            $arrBranch = DB::table('master_branch')
                              ->select('id')
                              ->where('is_ho', 'f')
                              ->where('company_id', Auth::user()->company_id)
                              ->pluck('id')
                              ->toArray();

            $arrBranch = join(',', $arrBranch);

            // Aktiva Paling Baru
            $aktiva = \DB::SELECT("select
                                    coa_no,
                                    coa_name,
                                    sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . $arrBranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . $arrBranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                  from
                                    (
                                    select
                                      a.coa_no,
                                      coa_name,
                                      d.coa_parent_id,
                                      case
                                        when a.coa_no = b.coa_parent_id then b.nom
                                        when a.coa_no = c.coa_no then c.last_os
                                        else coalesce(a.last_os, 0) end as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          coa_name,
                                          sum(last_os) as last_os,
                                          coa_type_id
                                        from
                                          master_coa
                                        where
                                          coa_type_id = 1
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_name,
                                          coa_type_id ) a
                                      left join (
                                        select
                                          coa_parent_id,
                                          sum(coalesce(last_os, 0)) as nom
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is not null
                                          and length(coa_parent_id) = 6
                                        group by
                                          coa_parent_id ) b on
                                        a.coa_no = b.coa_parent_id
                                      left join (with recursive cte as (
                                        select
                                          a.coa_no,
                                          a.coa_no as coa_parent_id,
                                          coalesce(a.last_os, 0) as last_os
                                        from
                                          (select coa_no, sum(last_os) as last_os from master_coa where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id ."
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                          group by coa_no) a
                                      union all
                                        select
                                          c.coa_no,
                                          p.coa_no,
                                          p.last_os
                                        from
                                          cte c
                                        join (
                                          select
                                            coa_no, sum(last_os)as last_os, coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $arrBranch . ")
                                            and is_active = true
                                            group by coa_no,
                                            coa_parent_id ) p
                                            using (coa_parent_id) )
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          cte
                                        group by
                                          1) c on
                                        a.coa_no = c.coa_no
                                      left join (
                                        select
                                          distinct coa_no,
                                          case
                                            when coa_parent_id isnull then coa_no
                                            else coa_parent_id end
                                          from
                                            master_coa) d on
                                        a.coa_no = d.coa_no
                                      order by
                                        d.coa_parent_id,
                                        a.coa_no asc)x
                                  group by
                                    coa_no,
                                    coa_name ,
                                    coa_parent_id
                                  order by
                                    coa_parent_id,
                                    coa_no");

            // Pasiva Paling Baru
            $pasiva = \DB::SELECT("select
                                    coa_no,
                                    coa_name,
                                    sum(case when coa_no in ('301') then last_os +
                                    (with recursive cte as (
                                  select
                                    a.coa_no,
                                    a.coa_no as coa_parent_id,
                                    coalesce(a.last_os, 0) as last_os
                                  from
                                    (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $arrBranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                  union all
                                  select
                                    c.coa_no,
                                    p.coa_no,
                                    p.last_os
                                  from
                                    cte c
                                  join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $arrBranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                      using (coa_parent_id) )
                                  select
                                    sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                  from
                                    cte
                                  where
                                    coa_no in ('400','500', '600')) else last_os end )as last_os
                                  from
                                    (
                                    select
                                      a.coa_no,
                                      coa_name,
                                      d.coa_parent_id,
                                      case
                                        when a.coa_no = b.coa_parent_id then b.nom
                                        when a.coa_no = c.coa_no then c.last_os
                                        else coalesce(a.last_os, 0) end as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          coa_name,
                                          sum(last_os) as last_os,
                                          coa_type_id
                                        from
                                          master_coa
                                        where
                                          coa_type_id = 2
                                          and company_id = " . Auth::user()->company_id ."
                                          and branch_id in (" . $arrBranch .")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_name,
                                          coa_type_id ) a
                                      left join (
                                        select
                                          coa_parent_id,
                                          sum(coalesce(last_os, 0)) as nom
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is not null
                                          and length(coa_parent_id) = 6
                                        group by
                                          coa_parent_id ) b on
                                        a.coa_no = b.coa_parent_id
                                      left join (with recursive cte as (
                                        select
                                          a.coa_no,
                                          a.coa_no as coa_parent_id,
                                          coalesce(a.last_os, 0) as last_os
                                        from
                                          (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $arrBranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                      union all
                                        select
                                          c.coa_no,
                                          p.coa_no,
                                          p.last_os
                                        from
                                          cte c
                                        join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $arrBranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                            using (coa_parent_id) )
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          cte
                                        group by
                                          1) c on
                                        a.coa_no = c.coa_no
                                      left join (
                                        select
                                          distinct coa_no,
                                          case
                                            when coa_parent_id isnull then coa_no
                                            else coa_parent_id end
                                          from
                                            master_coa) d on
                                        a.coa_no = d.coa_no
                                      order by
                                        d.coa_parent_id,
                                        a.coa_no asc)x
                                  group by
                                    coa_no,
                                    coa_name ,
                                    coa_parent_id
                                  order by
                                    coa_parent_id,
                                    coa_no");

          }else{
            // BUKAN KONSOL

            // Aktiva Paling Baru
            $aktiva = \DB::SELECT("select
                                    coa_no,
                                    coa_name,
                                    sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . $idbranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . $idbranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                  from
                                    (
                                    select
                                      a.coa_no,
                                      coa_name,
                                      d.coa_parent_id,
                                      case
                                        when a.coa_no = b.coa_parent_id then b.nom
                                        when a.coa_no = c.coa_no then c.last_os
                                        else coalesce(a.last_os, 0) end as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          coa_name,
                                          sum(last_os) as last_os,
                                          coa_type_id
                                        from
                                          master_coa
                                        where
                                          coa_type_id = 1
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $idbranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_name,
                                          coa_type_id ) a
                                      left join (
                                        select
                                          coa_parent_id,
                                          sum(coalesce(last_os, 0)) as nom
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is not null
                                          and length(coa_parent_id) = 6
                                        group by
                                          coa_parent_id ) b on
                                        a.coa_no = b.coa_parent_id
                                      left join (with recursive cte as (
                                        select
                                          a.coa_no,
                                          a.coa_no as coa_parent_id,
                                          coalesce(a.last_os, 0) as last_os
                                        from
                                          (select coa_no, sum(last_os) as last_os from master_coa where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id ."
                                          and branch_id in (" . $idbranch . ")
                                          and is_active = true
                                          group by coa_no) a
                                      union all
                                        select
                                          c.coa_no,
                                          p.coa_no,
                                          p.last_os
                                        from
                                          cte c
                                        join (
                                          select
                                            coa_no, sum(last_os)as last_os, coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                            group by coa_no,
                                            coa_parent_id ) p
                                            using (coa_parent_id) )
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          cte
                                        group by
                                          1) c on
                                        a.coa_no = c.coa_no
                                      left join (
                                        select
                                          distinct coa_no,
                                          case
                                            when coa_parent_id isnull then coa_no
                                            else coa_parent_id end
                                          from
                                            master_coa) d on
                                        a.coa_no = d.coa_no
                                      order by
                                        d.coa_parent_id,
                                        a.coa_no asc)x
                                  group by
                                    coa_no,
                                    coa_name ,
                                    coa_parent_id
                                  order by
                                    coa_parent_id,
                                    coa_no");

            // Pasiva Paling Baru
            $pasiva = \DB::SELECT("select
                                      coa_no,
                                      coa_name,
                                      sum(case when coa_no in ('301') then last_os +
                                      (with recursive cte as (
                                    select
                                      a.coa_no,
                                      a.coa_no as coa_parent_id,
                                      coalesce(a.last_os, 0) as last_os
                                    from
                                      (
                                            select
                                              coa_no,
                                              sum(last_os) as last_os
                                            from
                                              master_coa
                                            where
                                              coa_parent_id is null
                                              and company_id = " . Auth::user()->company_id . "
                                              and branch_id in (" . $idbranch . ")
                                              and is_active = true
                                            group by
                                              coa_no) a
                                    union all
                                    select
                                      c.coa_no,
                                      p.coa_no,
                                      p.last_os
                                    from
                                      cte c
                                    join (
                                            select
                                              coa_no,
                                              sum(last_os)as last_os,
                                              coa_parent_id
                                            from
                                              master_coa
                                            where
                                              company_id = " . Auth::user()->company_id . "
                                              and branch_id in (" . $idbranch . ")
                                              and is_active = true
                                            group by
                                              coa_no,
                                              coa_parent_id ) p
                                        using (coa_parent_id) )
                                    select
                                      sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                    from
                                      cte
                                    where
                                      coa_no in ('400','500', '600')) else last_os end )as last_os
                                    from
                                      (
                                      select
                                        a.coa_no,
                                        coa_name,
                                        d.coa_parent_id,
                                        case
                                          when a.coa_no = b.coa_parent_id then b.nom
                                          when a.coa_no = c.coa_no then c.last_os
                                          else coalesce(a.last_os, 0) end as last_os
                                        from
                                          (
                                          select
                                            coa_no,
                                            coa_name,
                                            sum(last_os) as last_os,
                                            coa_type_id
                                          from
                                            master_coa
                                          where
                                            coa_type_id = 2
                                            and company_id = " . Auth::user()->company_id ."
                                            and branch_id in (" . $idbranch .")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_name,
                                            coa_type_id ) a
                                        left join (
                                          select
                                            coa_parent_id,
                                            sum(coalesce(last_os, 0)) as nom
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is not null
                                            and length(coa_parent_id) = 6
                                          group by
                                            coa_parent_id ) b on
                                          a.coa_no = b.coa_parent_id
                                        left join (with recursive cte as (
                                          select
                                            a.coa_no,
                                            a.coa_no as coa_parent_id,
                                            coalesce(a.last_os, 0) as last_os
                                          from
                                            (
                                            select
                                              coa_no,
                                              sum(last_os) as last_os
                                            from
                                              master_coa
                                            where
                                              coa_parent_id is null
                                              and company_id = " . Auth::user()->company_id . "
                                              and branch_id in (" . $idbranch . ")
                                              and is_active = true
                                            group by
                                              coa_no) a
                                        union all
                                          select
                                            c.coa_no,
                                            p.coa_no,
                                            p.last_os
                                          from
                                            cte c
                                          join (
                                            select
                                              coa_no,
                                              sum(last_os)as last_os,
                                              coa_parent_id
                                            from
                                              master_coa
                                            where
                                              company_id = " . Auth::user()->company_id . "
                                              and branch_id in (" . $idbranch . ")
                                              and is_active = true
                                            group by
                                              coa_no,
                                              coa_parent_id ) p
                                              using (coa_parent_id) )
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            cte
                                          group by
                                            1) c on
                                          a.coa_no = c.coa_no
                                        left join (
                                          select
                                            distinct coa_no,
                                            case
                                              when coa_parent_id isnull then coa_no
                                              else coa_parent_id end
                                            from
                                              master_coa) d on
                                          a.coa_no = d.coa_no
                                        order by
                                          d.coa_parent_id,
                                          a.coa_no asc)x
                                    group by
                                      coa_no,
                                      coa_name ,
                                      coa_parent_id
                                    order by
                                      coa_parent_id,
                                      coa_no");


          } // end if


      }else{

        // Aktiva Paling Baru
        $aktiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . Auth::user()->branch_id . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . Auth::user()->branch_id . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 1
                                        and company_id = " . Auth::user()->company_id . "
                                        and branch_id in (" . Auth::user()->branch_id . ")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (select coa_no, sum(last_os) as last_os from master_coa where
                                        coa_parent_id is null
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . Auth::user()->branch_id . ")
                                        and is_active = true
                                        group by coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no, sum(last_os)as last_os, coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                          group by coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");


            // $pasiva = \DB::select("select
            //     coa_no,
            //     coa_name,
            //     sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = ".Auth::user()->company_id."  and is_active=true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = ".Auth::user()->company_id."  and is_active=true) p using (coa_parent_id) ) select sum(case when coa_no = '500' then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500')) else last_os end )as last_os
            //   from
            //     (
            //     select
            //       a.id,
            //       a.coa_no,
            //       coa_name,
            //       d.coa_parent_id,
            //       case
            //         when a.coa_no = b.coa_parent_id then b.nom
            //         when a.coa_no = c.coa_no then c.last_os
            //         else coalesce(a.last_os, 0)
            //       end as last_os
            //     from
            //       master_coa a
            //     left join (
            //       select
            //         coa_parent_id,
            //         sum(coalesce(last_os, 0)) as nom
            //       from
            //         master_coa
            //       where
            //         coa_parent_id is not null
            //         and length(coa_parent_id) = 6
            //       group by
            //         coa_parent_id ) b on
            //       a.coa_no = b.coa_parent_id
            //     left join (with recursive cte as (
            //       select
            //         coa_no,
            //         coa_no as coa_parent_id,
            //         coalesce(last_os, 0) as last_os
            //       from
            //         master_coa
            //       where
            //         coa_parent_id is null
            //         and company_id =".Auth::user()->company_id." and is_active=true
            //     union all
            //       select
            //         c.coa_no,
            //         p.coa_no,
            //         p.last_os
            //       from
            //         cte c
            //       join (
            //         select
            //           *
            //         from
            //           master_coa
            //         where
            //           company_id = ".Auth::user()->company_id." and is_active=true) p
            //           using (coa_parent_id) )
            //       select
            //         coa_no,
            //         sum(last_os) as last_os
            //       from
            //         cte
            //       group by
            //         1) c on
            //       a.coa_no = c.coa_no
            //     left join (
            //       select
            //         distinct coa_no,
            //         case
            //           when coa_parent_id isnull then coa_no
            //           else coa_parent_id
            //         end
            //       from
            //         master_coa) d on
            //       a.coa_no = d.coa_no
            //     where
            //       coa_type_id = 2
            //       and a.company_id = ".Auth::user()->company_id." and a.is_active=true
            //     order by
            //       d.coa_parent_id,
            //       a.coa_no asc)x
            //   group by
            //     coa_no,
            //     coa_name ,
            //     coa_parent_id
            //   order by
            //     coa_parent_id,
            //     coa_no;");

            // $pasiva = \DB::select("select
            //     coa_no,
            //     coa_name,
            //     sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = ".Auth::user()->company_id."  and is_active=true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = ".Auth::user()->company_id."  and is_active=true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
            //   from
            //     (
            //     select
            //       a.id,
            //       a.coa_no,
            //       coa_name,
            //       d.coa_parent_id,
            //       case
            //         when a.coa_no = b.coa_parent_id then b.nom
            //         when a.coa_no = c.coa_no then c.last_os
            //         else coalesce(a.last_os, 0)
            //       end as last_os
            //     from
            //       master_coa a
            //     left join (
            //       select
            //         coa_parent_id,
            //         sum(coalesce(last_os, 0)) as nom
            //       from
            //         master_coa
            //       where
            //         coa_parent_id is not null
            //         and length(coa_parent_id) = 6
            //       group by
            //         coa_parent_id ) b on
            //       a.coa_no = b.coa_parent_id
            //     left join (with recursive cte as (
            //       select
            //         coa_no,
            //         coa_no as coa_parent_id,
            //         coalesce(last_os, 0) as last_os
            //       from
            //         master_coa
            //       where
            //         coa_parent_id is null
            //         and company_id =".Auth::user()->company_id." and is_active=true
            //     union all
            //       select
            //         c.coa_no,
            //         p.coa_no,
            //         p.last_os
            //       from
            //         cte c
            //       join (
            //         select
            //           *
            //         from
            //           master_coa
            //         where
            //           company_id = ".Auth::user()->company_id." and is_active=true) p
            //           using (coa_parent_id) )
            //       select
            //         coa_no,
            //         sum(last_os) as last_os
            //       from
            //         cte
            //       group by
            //         1) c on
            //       a.coa_no = c.coa_no
            //     left join (
            //       select
            //         distinct coa_no,
            //         case
            //           when coa_parent_id isnull then coa_no
            //           else coa_parent_id
            //         end
            //       from
            //         master_coa) d on
            //       a.coa_no = d.coa_no
            //     where
            //       coa_type_id = 2
            //       and a.company_id = ".Auth::user()->company_id." and a.is_active=true
            //     order by
            //       d.coa_parent_id,
            //       a.coa_no asc)x
            //   group by
            //     coa_no,
            //     coa_name ,
            //     coa_parent_id
            //   order by
            //     coa_parent_id,
            //     coa_no;");

        // Pasiva Paling Baru
        $pasiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os +
                                  (with recursive cte as (
                                select
                                  a.coa_no,
                                  a.coa_no as coa_parent_id,
                                  coalesce(a.last_os, 0) as last_os
                                from
                                  (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                union all
                                select
                                  c.coa_no,
                                  p.coa_no,
                                  p.last_os
                                from
                                  cte c
                                join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                    using (coa_parent_id) )
                                select
                                  sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                from
                                  cte
                                where
                                  coa_no in ('400','500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 2
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . Auth::user()->branch_id .")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");
      } // end if

      // dd($pasiva);


      if(Auth::user()->user_role_id==6 || Auth::user()->user_role_id==5){
      }else{

        // BUKAN ADMIN PUSAT ATAU ADMIN CABANG

        // Aktiva Paling Baru
        $aktiva = \DB::SELECT("select
                                coa_no,
                                coa_name,
                                sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . Auth::user()->branch_id . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . Auth::user()->branch_id . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                              from
                                (
                                select
                                  a.coa_no,
                                  coa_name,
                                  d.coa_parent_id,
                                  case
                                    when a.coa_no = b.coa_parent_id then b.nom
                                    when a.coa_no = c.coa_no then c.last_os
                                    else coalesce(a.last_os, 0) end as last_os
                                  from
                                    (
                                    select
                                      coa_no,
                                      coa_name,
                                      sum(last_os) as last_os,
                                      coa_type_id
                                    from
                                      master_coa
                                    where
                                      coa_type_id = 1
                                      and company_id = " . Auth::user()->company_id . "
                                      and branch_id in (" . Auth::user()->branch_id . ")
                                      and is_active = true
                                    group by
                                      coa_no,
                                      coa_name,
                                      coa_type_id ) a
                                  left join (
                                    select
                                      coa_parent_id,
                                      sum(coalesce(last_os, 0)) as nom
                                    from
                                      master_coa
                                    where
                                      coa_parent_id is not null
                                      and length(coa_parent_id) = 6
                                    group by
                                      coa_parent_id ) b on
                                    a.coa_no = b.coa_parent_id
                                  left join (with recursive cte as (
                                    select
                                      a.coa_no,
                                      a.coa_no as coa_parent_id,
                                      coalesce(a.last_os, 0) as last_os
                                    from
                                      (select coa_no, sum(last_os) as last_os from master_coa where
                                      coa_parent_id is null
                                      and company_id = " . Auth::user()->company_id ."
                                      and branch_id in (" . Auth::user()->branch_id . ")
                                      and is_active = true
                                      group by coa_no) a
                                  union all
                                    select
                                      c.coa_no,
                                      p.coa_no,
                                      p.last_os
                                    from
                                      cte c
                                    join (
                                      select
                                        coa_no, sum(last_os)as last_os, coa_parent_id
                                      from
                                        master_coa
                                      where
                                        company_id = " . Auth::user()->company_id . "
                                        and branch_id in (" . Auth::user()->branch_id . ")
                                        and is_active = true
                                        group by coa_no,
                                        coa_parent_id ) p
                                        using (coa_parent_id) )
                                    select
                                      coa_no,
                                      sum(last_os) as last_os
                                    from
                                      cte
                                    group by
                                      1) c on
                                    a.coa_no = c.coa_no
                                  left join (
                                    select
                                      distinct coa_no,
                                      case
                                        when coa_parent_id isnull then coa_no
                                        else coa_parent_id end
                                      from
                                        master_coa) d on
                                    a.coa_no = d.coa_no
                                  order by
                                    d.coa_parent_id,
                                    a.coa_no asc)x
                              group by
                                coa_no,
                                coa_name ,
                                coa_parent_id
                              order by
                                coa_parent_id,
                                coa_no");

          $pasiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os +
                                  (with recursive cte as (
                                select
                                  a.coa_no,
                                  a.coa_no as coa_parent_id,
                                  coalesce(a.last_os, 0) as last_os
                                from
                                  (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                union all
                                select
                                  c.coa_no,
                                  p.coa_no,
                                  p.last_os
                                from
                                  cte c
                                join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                    using (coa_parent_id) )
                                select
                                  sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                from
                                  cte
                                where
                                  coa_no in ('400','500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 2
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . Auth::user()->branch_id .")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . Auth::user()->branch_id . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

      } // end if




       $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);


       $param['idbranch'] = $idbranch;
       $param['branch'] = $branch;
       $param['aktiva'] = $aktiva;
       $param['pasiva'] = $pasiva;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.balance_sheet',$param);
        }else {
            return view('master.master')->nest('child', 'report.balance_sheet',$param);
        }
    }

    public function laba_rugi(Request $request)
    {

      // $idbranch='konsol';
      $idbranch = Auth::user()->branch_id;
      if( !is_null($request->get('id')) ){
        $idbranch=$request->get('id');
          if($request->get('id')=='konsol') {

            $arrBranch = DB::table('master_branch')
                              ->select('id')
                              ->where('is_ho', 'f')
                              ->where('company_id', Auth::user()->company_id)
                              ->pluck('id')
                              ->toArray();

            $arrBranch = join(',', $arrBranch);

            // $laba_rugi = \DB::select("select
            //     id,
            //     coa_no,
            //     coa_name,
            //     balance_type_id,
            //     coa_type_id,
            //     case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
            //     from master_coa a left join
            //     (select coa_parent_id, sum(coalesce(last_os,0)) as nom
            //     from master_coa
            //     where coa_parent_id is  not null and is_active=true and company_id=".Auth::user()->company_id."
            //     group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
            //     where coa_type_id in (3,4) and a.is_active=true and a.company_id=".Auth::user()->company_id." and left(a.coa_no, 3) != '600'
            //     ORDER BY coa_no asc");

            $laba_rugi = \DB::select("SELECT
                                            coa_no,
                                            coa_name,
                                            balance_type_id,
                                            coa_type_id,
                                            CASE
                                              WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                            END AS last_os
                                      FROM (
                                              SELECT coa_no,
                                                      coa_name,
                                                      balance_type_id,
                                                      coa_type_id,
                                                      sum(last_os) AS last_os
                                              FROM
                                              master_coa
                                              WHERE coa_type_id IN (3,4) AND
                                                    is_active = true AND
                                                    company_id = " . Auth::user()->company_id . "   AND
                                                    branch_id IN (" . $arrBranch . ") AND
                                                    LEFT(coa_no, 3) != '600'
                                              GROUP BY coa_no,
                                                  coa_name,
                                                  balance_type_id,
                                                  coa_type_id
                                              ORDER BY coa_no ASC
                                            ) a LEFT JOIN (
                                                              SELECT
                                                                coa_parent_id,
                                                                sum(coalesce(last_os, 0)) AS nom
                                                              FROM
                                                                master_coa
                                                              WHERE
                                                                coa_parent_id IS NOT NULL AND
                                                                is_active = true AND
                                                                company_id = " . Auth::user()->company_id . " AND
                                                                branch_id IN (" . $arrBranch . ")
                                                              GROUP BY coa_parent_id
                                                          ) b ON a.coa_no = b.coa_parent_id
                                      ORDER BY coa_no ASC");

          }else{

            // $laba_rugi = \DB::select("select
            //     id,
            //     coa_no,
            //     coa_name,
            //     balance_type_id,
            //     coa_type_id,
            //     case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
            //     from master_coa a left join
            //     (select coa_parent_id, sum(coalesce(last_os,0)) as nom
            //     from master_coa
            //     where coa_parent_id is  not null and is_active=true and branch_id=".$idbranch." and company_id=".Auth::user()->company_id."
            //     group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
            //     where coa_type_id in (3,4) and a.is_active=true and a.branch_id=".$idbranch." and a.company_id=".Auth::user()->company_id." and left(a.coa_no, 3) != '600'
            //     ORDER BY coa_no asc");

            $laba_rugi = \DB::select("SELECT
                                            coa_no,
                                            coa_name,
                                            balance_type_id,
                                            coa_type_id,
                                            CASE
                                              WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                            END AS last_os
                                      FROM (
                                              SELECT coa_no,
                                                      coa_name,
                                                      balance_type_id,
                                                      coa_type_id,
                                                      sum(last_os) AS last_os
                                              FROM
                                              master_coa
                                              WHERE coa_type_id IN (3,4) AND
                                                    is_active = true AND
                                                    company_id = " . Auth::user()->company_id . "   AND
                                                    branch_id IN (" . $idbranch . ") AND
                                                    LEFT(coa_no, 3) != '600'
                                              GROUP BY coa_no,
                                                  coa_name,
                                                  balance_type_id,
                                                  coa_type_id
                                              ORDER BY coa_no ASC
                                            ) a LEFT JOIN (
                                                              SELECT
                                                                coa_parent_id,
                                                                sum(coalesce(last_os, 0)) AS nom
                                                              FROM
                                                                master_coa
                                                              WHERE
                                                                coa_parent_id IS NOT NULL AND
                                                                is_active = true AND
                                                                company_id = " . Auth::user()->company_id . " AND
                                                                branch_id IN (" . $idbranch . ")
                                                              GROUP BY coa_parent_id
                                                          ) b ON a.coa_no = b.coa_parent_id
                                      ORDER BY coa_no ASC");
          }

      }else{

        //  $laba_rugi = \DB::select("select
        //         id,
        //         coa_no,
        //         coa_name,
        //         balance_type_id,
        //         coa_type_id,
        //         case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
        //         from master_coa a left join
        //         (select coa_parent_id, sum(coalesce(last_os,0)) as nom
        //         from master_coa
        //         where coa_parent_id is  not null and is_active=true and company_id=".Auth::user()->company_id."
        //         group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
        //         where coa_type_id in (3,4) and a.is_active=true and a.company_id=".Auth::user()->company_id." and left(a.coa_no, 3) != '600'
        //         ORDER BY coa_no asc");

         $laba_rugi = \DB::select("SELECT
                                          coa_no,
                                          coa_name,
                                          balance_type_id,
                                          coa_type_id,
                                          CASE
                                            WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                          END AS last_os
                                    FROM (
                                            SELECT coa_no,
                                                    coa_name,
                                                    balance_type_id,
                                                    coa_type_id,
                                                    sum(last_os) AS last_os
                                            FROM
                                            master_coa
                                            WHERE coa_type_id IN (3,4) AND
                                                  is_active = true AND
                                                  company_id = " . Auth::user()->company_id . "   AND
                                                  branch_id IN (" . Auth::user()->branch_id . ") AND
                                                  LEFT(coa_no, 3) != '600'
                                            GROUP BY coa_no,
                                                coa_name,
                                                balance_type_id,
                                                coa_type_id
                                            ORDER BY coa_no ASC
                                          ) a LEFT JOIN (
                                                            SELECT
                                                              coa_parent_id,
                                                              sum(coalesce(last_os, 0)) AS nom
                                                            FROM
                                                              master_coa
                                                            WHERE
                                                              coa_parent_id IS NOT NULL AND
                                                              is_active = true AND
                                                              company_id = " . Auth::user()->company_id . " AND
                                                              branch_id IN (" . Auth::user()->branch_id . ")
                                                            GROUP BY coa_parent_id
                                                        ) b ON a.coa_no = b.coa_parent_id
                                    ORDER BY coa_no ASC");
      }

      if(Auth::user()->user_role_id==6 || Auth::user()->user_role_id==5){
      }else{

        // BUKAN ADMIN PUSAT DAN SUPER ADMIN

        // $laba_rugi = \DB::select("select
        //         id,
        //         coa_no,
        //         coa_name,
        //         balance_type_id,
        //         coa_type_id,
        //         case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
        //         from master_coa a left join
        //         (select coa_parent_id, sum(coalesce(last_os,0)) as nom
        //         from master_coa
        //         where coa_parent_id is  not null and is_active=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id."
        //         group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
        //         where coa_type_id in (3,4) and a.is_active=true and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and left(a.coa_no, 3) != '600'
        //         ORDER BY coa_no asc");

        $laba_rugi = \DB::select("SELECT
                                        coa_no,
                                        coa_name,
                                        balance_type_id,
                                        coa_type_id,
                                        CASE
                                          WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                        END AS last_os
                                  FROM (
                                          SELECT coa_no,
                                                  coa_name,
                                                  balance_type_id,
                                                  coa_type_id,
                                                  sum(last_os) AS last_os
                                          FROM
                                          master_coa
                                          WHERE coa_type_id IN (3,4) AND
                                                is_active = true AND
                                                company_id = " . Auth::user()->company_id . "   AND
                                                branch_id IN (" . Auth::user()->branch_id . ") AND
                                                LEFT(coa_no, 3) != '600'
                                          GROUP BY coa_no,
                                              coa_name,
                                              balance_type_id,
                                              coa_type_id
                                          ORDER BY coa_no ASC
                                        ) a LEFT JOIN (
                                                          SELECT
                                                            coa_parent_id,
                                                            sum(coalesce(last_os, 0)) AS nom
                                                          FROM
                                                            master_coa
                                                          WHERE
                                                            coa_parent_id IS NOT NULL AND
                                                            is_active = true AND
                                                            company_id = " . Auth::user()->company_id . " AND
                                                            branch_id IN (" . Auth::user()->branch_id . ")
                                                          GROUP BY coa_parent_id
                                                      ) b ON a.coa_no = b.coa_parent_id
                                  ORDER BY coa_no ASC");


      }

       $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);

       $param['idbranch']=$idbranch;
       $param['branch']=$branch;
       $param['laba_rugi']=$laba_rugi;



        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.laba_rugi',$param);
        }else {
            return view('master.master')->nest('child', 'report.laba_rugi',$param);
        }
    }

    public function cashflow(Request $request)
    {
      $cash='';
      $start='';
      $end='';
      $idbranch='konsol';

      if ($request->has('cari')) {
        $start=$request->input('start');
        $end=$request->input('end');

        if($request->get('id')){
            $idbranch=$request->get('id');
            if($request->get('id')=='konsol'){

              $cash = \DB::select("select
              coalesce((select
              coalesce(sum(acc_os),0) as caschflow1
              from master_tx where id in
              (select min(id) from master_tx where tx_date = '".date('Y-m-d',strtotime($request->input('end')))."' and left(coa_id,3) = '100' group by coa_id)),0) as caschflow1,
              coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) as caschflow2,
              coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0) as caschflow3,
              coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) -
              coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0) as caschflow4,
              coalesce((select
              coalesce(sum(acc_os),0) as caschflow1
              from master_tx where id in
              (select min(id) from master_tx where tx_date = '2019-10-16' and left(coa_id,3) = '100' group by coa_id)),0) +
              (coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) -
              coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0)) as caschflow5
              from master_tx where tx_date between '".date('Y-m-d',strtotime($request->input('start')))."' and '".date('Y-m-d',strtotime($request->input('end')))."' and company_id=".Auth::user()->company_id);


            }else{

              $cash = \DB::select("select
                coalesce((select
                coalesce(sum(acc_os),0) as caschflow1
                from master_tx where id in
                (select min(id) from master_tx where tx_date = '".date('Y-m-d',strtotime($request->input('end')))."' and left(coa_id,3) = '100' group by coa_id)),0) as caschflow1,
                coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) as caschflow2,
                coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0) as caschflow3,
                coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) -
                coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0) as caschflow4,
                coalesce((select
                coalesce(sum(acc_os),0) as caschflow1
                from master_tx where id in
                (select min(id) from master_tx where tx_date = '2019-10-16' and left(coa_id,3) = '100' group by coa_id)),0) +
                (coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) -
                coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0)) as caschflow5
                from master_tx where tx_date between '".date('Y-m-d',strtotime($request->input('start')))."' and '".date('Y-m-d',strtotime($request->input('end')))."' and branch_id=".$idbranch." and company_id=".Auth::user()->company_id);


            }

        }else{
          $cash = \DB::select("select
              coalesce((select
              coalesce(sum(acc_os),0) as caschflow1
              from master_tx where id in
              (select min(id) from master_tx where tx_date = '".date('Y-m-d',strtotime($request->input('end')))."' and left(coa_id,3) = '100' group by coa_id)),0) as caschflow1,
              coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) as caschflow2,
              coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0) as caschflow3,
              coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) -
              coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0) as caschflow4,
              coalesce((select
              coalesce(sum(acc_os),0) as caschflow1
              from master_tx where id in
              (select min(id) from master_tx where tx_date = '2019-10-16' and left(coa_id,3) = '100' group by coa_id)),0) +
              (coalesce(sum(case when left(coa_id,3) in ('300','200') and coa_id in ('400001','400003','400002')  then tx_amount end),0) -
              coalesce(sum(case when left(coa_id,6) in ('500001','500002','500003','500004') then tx_amount end),0)) as caschflow5
              from master_tx where tx_date between '".date('Y-m-d',strtotime($request->input('start')))."' and '".date('Y-m-d',strtotime($request->input('end')))."' and company_id=".Auth::user()->company_id);
        }




       }

       $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);


       $param['branch']=$branch;
       $param['idbranch']=$idbranch;
       $param['cash']=$cash;
       $param['start']=$start;
       $param['end']=$end;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.cashflow',$param);
        }else {
            return view('master.master')->nest('child', 'report.cashflow',$param);
        }
    }
    public function cashflow_detail(Request $request)
    {
        $data = collect(\DB::select("select
          coalesce(sum(case when coa_id in ('400001','400003') then tx_amount end),0) as inflow1,
          coalesce(sum(case when left(coa_id,6) in ('500001') then tx_amount end),0) as outflow1,
          coalesce(sum(case when left(coa_id,6) in ('500002') then tx_amount end),0) as outflow2,
          coalesce(sum(case when left(coa_id,6) in ('500003') then tx_amount end),0) as outflow3,
          coalesce(sum(case when left(coa_id,6) in ('500004') then tx_amount end),0) as outflow4,
          coalesce(sum(case when coa_id in ('400002') then tx_amount end),0) as inflow2,
          0 as outflow5,
          0 as inflow3,
          coalesce(sum(case when left(coa_id,3) = '300' then tx_amount end),0) as inflow4,
          coalesce(sum(case when left(coa_id,3) = '200' then tx_amount end),0) as inflow5
          from master_tx"))->first();
        $param['data']=$data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.cashflow_detail',$param);
        }else {
            return view('master.master')->nest('child', 'report.cashflow_detail' ,$param);
        }
    }
    public function sales_product(Request $request)
    {
        $data='';
        $start_date='';
        $end_date='';
        $idbranch='konsol';

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if( $request->get('search') ) {

            $start_date = date('Y-m-d', strtotime($request->get('start_date')));
            $end_date = date('Y-m-d', strtotime($request->get('end_date')));

            if ( $start_date == "1970-01-01") {
                abort(403, 'Invalid date format "' . $request->get('start_date') . '"');
            }

            if ( $end_date == "1970-01-01") {
                abort(403, 'Invalid date format "' . $request->get('end_date') . '"');
            }

            if( !is_null($request->get('id')) ) {

                $idbranch = $request->get('id');

                if( $request->get('id') == 'konsol' ){

                  $data = \DB::SELECT("SELECT
                                            RP.id,
                                            RP.definition,
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM master_sales MS LEFT JOIN ref_product RP ON RP.id = MS.product_id
                                        WHERE
                                            inv_date between '". $start_date ."' AND '". $end_date ."' AND
                                            MS.company_id = " . Auth::user()->company_id . "
                                        GROUP BY RP.id, RP.definition");

                } else {

                 if ( !is_numeric($idbranch) ) {
                    abort(403, 'Invalid branch id "' . $idbranch . '". Must be numeric');
                 }

                  $param['short_code'] = $this->getShortCodeBranch($idbranch);

                  $data = \DB::SELECT("SELECT
                                            RP.id,
                                            RP.definition,
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM master_sales MS LEFT JOIN ref_product RP ON RP.id = MS.product_id
                                        WHERE
                                            inv_date between '". $start_date ."' AND '". $end_date ."' AND
                                            MS.branch_id = " . $idbranch . " AND
                                            MS.company_id = " . Auth::user()->company_id . "
                                        GROUP BY RP.id, RP.definition");

                }
            } else {
              $data = \DB::SELECT("SELECT
                                        RP.id,
                                        RP.definition,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM master_sales MS LEFT JOIN ref_product RP ON RP.id = MS.product_id
                                    WHERE
                                        inv_date between '". $start_date ."' AND '". $end_date ."' AND
                                        MS.company_id = " . Auth::user()->company_id . "
                                    GROUP BY RP.id, RP.definition");

            }



        }


      $master_branch = DB::SELECT("SELECT is_ho FROM master_branch where id = " . Auth::user()->branch_id);

      if ($master_branch[0]->is_ho == false) {
         $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id." AND id = " . Auth::user()->branch_id);
      } else {
          // head office show all branch
          $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
      }

       $param['branch']=$branch;
       $param['idbranch']=$idbranch;
       $param['data']=$data;
       $param['start_date'] = $request->get('start_date');
       $param['end_date'] = $request->get('end_date');



        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_product',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_product',$param);
        }
    }

    public function sales_branch(Request $request)
    {
        $data='';
        $start_date='';
        $end_date='';
        $idbranch='konsol';

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);


        if( $request->get('search') ){

            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');

            $startDateFormat = date('Y-m-d', strtotime($request->get('start_date')));
            $endDateFormat = date('Y-m-d', strtotime($request->get('end_date')));

            if ( $startDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $start_date . '"');
            }

            if ( $endDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $end_date . '"');
            }

            if ( $request->get('id') ) {
              $idbranch = $request->get('id');

              if( $request->get('id') == 'konsol') {

                $data = \DB::SELECT("SELECT
                                        MB.id,
                                        MB.branch_name,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM master_sales MS LEFT JOIN master_branch MB ON MB.id = MS.branch_id
                                    WHERE inv_date BETWEEN '". $startDateFormat ."' AND '". $endDateFormat ."' AND
                                          MS.company_id = "  . Auth::user()->company_id . "
                                    GROUP BY MB.id, MB.branch_name");
              }else{

                if ( !is_numeric($idbranch) ) {
                    abort(403, 'Invalid branch id "' . $idbranch . '". Must be numeric');
                 }

                $param['short_code'] = $this->getShortCodeBranch($idbranch);
                $data = \DB::SELECT("SELECT
                                        MB.id,
                                        MB.branch_name,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM master_sales MS LEFT JOIN master_branch MB ON MB.id = MS.branch_id
                                    WHERE inv_date BETWEEN '". $startDateFormat ."' AND '". $endDateFormat ."' AND
                                        MS.company_id = "  . Auth::user()->company_id . " AND
                                        MS.branch_id = " . $idbranch . "
                                    GROUP BY MB.id, MB.branch_name");

              }

            }else{
              $data = \DB::SELECT("SELECT
                                        MB.id,
                                        MB.branch_name,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM master_sales MS LEFT JOIN master_branch MB ON MB.id = MS.branch_id
                                    WHERE inv_date BETWEEN '". $startDateFormat ."' AND '". $endDateFormat ."' AND
                                            MS.company_id = "  . Auth::user()->company_id . "
                                    GROUP BY MB.id, MB.branch_name");
            }



        }





      $master_branch = DB::SELECT("SELECT is_ho FROM master_branch where id = " . Auth::user()->branch_id);

      if ($master_branch[0]->is_ho == false) {
         $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id." AND id = " . Auth::user()->branch_id);
      } else {
          // head office show all branch
          $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
      }

       $param['branch']=$branch;
       $param['idbranch']=$idbranch;
       $param['data']=$data;
       $param['start_date']=$start_date;
       $param['end_date']=$end_date;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_branch',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_branch',$param);
        }
    }

    // is parent false
    public function sales_officer(Request $request)
    {
        $data='';
        $start_date='';
        $end_date='';
        $idbranch='konsol';

        $branch_id_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($branch_id_user);

        if( $request->get('search') ){

            $start_date = date('Y-m-d', strtotime($request->get('start_date')));
            $end_date = date('Y-m-d', strtotime($request->get('end_date')));

            if ( $start_date == "1970-01-01") {
                abort(403, 'Invalid date format "' . $request->get('start_date') . '"');
            }

            if ( $end_date == "1970-01-01") {
                abort(403, 'Invalid date format "' . $request->get('end_date') . '"');
            }

            if( $request->get('id') ) {

                $idbranch = $request->get('id');


                if( $request->get('id')=='konsol' ) {

                   $data = \DB::SELECT("SELECT
                                            RAT.id,
                                            (SELECT full_name as definition FROM ref_agent WHERE id = RAT.id),
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM master_sales MS LEFT JOIN ref_agent RAT ON RAT.id = MS.agent_id
                                        WHERE
                                            inv_date BETWEEN '". $start_date ."' AND '". $end_date ."' AND
                                            MS.company_id = " . Auth::user()->company_id . "
                                        GROUP BY RAT.id");
                } else {

                    if ( !is_numeric($idbranch) ) {
                        abort(403, 'Invalid branch id "' . $idbranch . '". Must be numeric');
                     }

                    $param['short_code'] = $this->getShortCodeBranch($idbranch);

                    $data = \DB::SELECT("SELECT
                                            RAT.id,
                                            (SELECT full_name as definition FROM ref_agent WHERE id = RAT.id),
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM master_sales MS LEFT JOIN ref_agent RAT ON RAT.id = MS.agent_id
                                        WHERE
                                            inv_date BETWEEN '". $start_date ."' AND '". $end_date ."' AND
                                            MS.company_id = " . Auth::user()->company_id . " AND
                                            MS.branch_id = " . $idbranch . "
                                        GROUP BY RAT.id");

                }

            }else{

              $data = \DB::SELECT("SELECT
                                        RAT.id,
                                        (SELECT full_name as definition FROM ref_agent WHERE id = RAT.id),
                                        sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM master_sales MS LEFT JOIN ref_agent RAT ON RAT.id = MS.agent_id
                                    WHERE
                                        inv_date BETWEEN '". $start_date ."' AND '". $end_date ."' AND
                                        MS.company_id = " . Auth::user()->company_id . "
                                    GROUP BY RAT.id");

              // QUERY SEMULA
            //   $data = \DB::SELECT("SELECT
            //                             RAT.id,
            //                             RAT.definition,
            //                             SUM(MS.premi_amount) AS premi_amount,
            //                             SUM(MS.agent_fee_amount) AS agent_fee_amount,
            //                             SUM(MS.comp_fee_amount) AS comp_fee_amount,
            //                             SUM(MS.net_amount) AS net_amount
            //                         FROM master_sales MS LEFT JOIN ref_agent_type RAT ON RAT.id = MS.agent_id
            //                         WHERE
            //                             inv_date BETWEEN '". $start_date ."' AND '". $end_date ."' AND
            //                             MS.company_id = " . Auth::user()->company_id . "
            //                         GROUP BY RAT.id, RAT.definition");

            }




        }


      $master_branch = DB::SELECT("SELECT is_ho FROM master_branch where id = " . Auth::user()->branch_id);

      if ($master_branch[0]->is_ho == false) {
         $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id." AND id = " . Auth::user()->branch_id);
      } else {
          // head office show all branch
          $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
      }


       $param['branch']=$branch;
       $param['idbranch']=$idbranch;
       $param['data']=$data;
       $param['start_date'] = $request->get('start_date');
       $param['end_date'] = $request->get('end_date');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_officer',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_officer',$param);
        }
    }


    public function sales_detail(Request $request)
    {

        $start_date = date('Y-m-d', strtotime($request->input('start_date')));
        $end_date = date('Y-m-d', strtotime($request->input('end_date')));

        if ( $start_date == "1970-01-01") {
            abort(403, 'Invalid date format "' . $request->get('start_date') . '"');
        }

        if ( $end_date == "1970-01-01") {
            abort(403, 'Invalid date format "' . $request->get('end_date') . '"');
        }

        if ( $request->get('type') == 'product' ){
            $data = \DB::select("select
                                    a.id,
                                    b.full_name,
                                    a.premi_amount,
                                    a.agent_fee_amount,
                                    a.comp_fee_amount + a.admin_amount AS comp_fee_amount,
                                    a.net_amount,
                                    a.ins_fee + a.materai_amount + a.polis_amount as nett_to_underwriter
                                from
                                    master_sales a
                                left join master_customer b on
                                    b.id = a.customer_id
                                where
                                    product_id = " . $request->get('id') . " and
                                    inv_date BETWEEN '". $start_date ."' AND '". $end_date ."'
                                ");
        } else if( $request->get('type') == 'office' ){
            $data = \DB::select("select
                                    a.id,
                                    b.full_name,
                                    a.premi_amount,
                                    a.agent_fee_amount,
                                    a.comp_fee_amount + a.admin_amount AS comp_fee_amount,
                                    a.net_amount,
                                    a.ins_fee + a.materai_amount + a.polis_amount as nett_to_underwriter
                                from
                                    master_sales a
                                left join master_customer b on
                                    b.id = a.customer_id
                                where
                                    a.agent_id = " . $request->get('id') . " and
                                    inv_date BETWEEN '". $start_date ."' AND '". $end_date ."'
                                ");
        } else if( $request->get('type') == 'customer_segment' ){
            $data = \DB::select("SELECT
                                    MS.id,
                                    MC.full_name,
                                    MS.premi_amount,
                                    MS.agent_fee_amount,
                                    MS.comp_fee_amount + MS.admin_amount AS comp_fee_amount,
                                    MS.net_amount,
                                    MS.ins_fee + MS.materai_amount + MS.polis_amount as nett_to_underwriter
                                FROM
                                    master_sales MS LEFT JOIN ref_cust_segment RCS ON RCS.id = MS.segment_id
                                                    LEFT JOIN master_customer MC ON MC.id = MS.customer_id
                                WHERE
                                    MS.segment_id = " . $request->get('id') . " AND
                                    MS.inv_date BETWEEN '". $start_date ."' AND '". $end_date ."'
                                ");
        } else if( $request->get('type') == 'company' ) {
            $data = \DB::select("SELECT
                                    MS.id,
                                    MC.full_name,
                                    MS.premi_amount,
                                    MS.agent_fee_amount,
                                    MS.comp_fee_amount + MS.admin_amount AS comp_fee_amount,
                                    MS.net_amount,
                                    MS.ins_fee + MS.materai_amount + MS.polis_amount as nett_to_underwriter
                                FROM
                                    master_sales MS LEFT JOIN master_customer MC ON MC.id = MS.customer_id
                                                    LEFT JOIN ref_cust_type RCT ON RCT.id = MC.cust_type_id
                                WHERE
                                    MC.cust_type_id = " . $request->get('id') . " AND
                                    MS.inv_date BETWEEN '". $start_date ."' AND '". $end_date ."'
                                ");
        } else {
            // branch
            $data = \DB::select("select
                                    a.id,
                                    b.full_name,
                                    a.premi_amount,
                                    a.agent_fee_amount,
                                    a.comp_fee_amount + a.admin_amount AS comp_fee_amount,
                                    a.net_amount,
                                    a.ins_fee + a.materai_amount + a.polis_amount as nett_to_underwriter
                                from
                                    master_sales a
                                left join master_customer b on
                                    b.id = a.customer_id
                                where
                                    a.branch_id = " . $request->get('id') . " and
                                    inv_date BETWEEN '". $start_date ."' AND '". $end_date ."'
                                ");
        }

        $param['data']=$data;
        $param['type']=$request->get('type');

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_detail',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_detail',$param);
        }
    }

    // Tambahan
    public function nominativeReport(Request $request)
    {
        $curDate = date('Y-m-d');
        $filterStatus = $request->input('status');

        if ( $filterStatus ) {

            $arrSts = ['All', 'Aktif', 'Paid', 'Completed'];

            if ( in_array($filterStatus, $arrSts) ) {
                $param['data'] = $this->getQueryNominativeReport($filterStatus);
            } else {
                $param['data'] = NULL;
                abort(403, 'Filter "' . $filterStatus . '" doesn\'t exist');
            }

            switch ( $filterStatus ) {
                case "All" : $param['sts'] = '1000'; break;
                case "Aktif" : $param['sts'] = '1'; break;
                case "Paid" : $param['sts'] = '2'; break;
                case "Completed" : $param['sts'] = '3'; break;
                default: $param['sts'] = '1000';
            }

        } else {
            $param['data'] = $this->getQueryNominativeReport('All');
            $param['sts'] = '1000';
        }

        $branch_id_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($branch_id_user);

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.nominative_report',$param);
        }else {
            return view('master.master')->nest('child', 'report.nominative_report',$param);
        }
    }

    public function download_laporan(Request $request)
    {
        $param['directory'] = File::directories(public_path('batch_file'));

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.download_laporan',$param);
        }else {
            return view('master.master')->nest('child', 'report.download_laporan',$param);
        }
    }



    public function getQueryNominativeReport($filter)
    {
        switch ( $filter ) {
            case "All" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id
                            WHERE
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 0) OR
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 1) OR
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 1 AND is_splitted = true) AND
                                MS.branch_id = " . Auth::user()->branch_id . " AND
                                MS.company_id = " . Auth::user()->company_id . "
                            ORDER BY MS.id ASC");
            break;
            case "Aktif" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id

                            WHERE
                                MS.wf_status_id = 9 AND MS.paid_status_id = 0 AND
                                MS.branch_id = " . Auth::user()->branch_id . " AND
                                MS.company_id = " . Auth::user()->company_id . "
                            ORDER BY MS.id ASC");
            break;
            case "Paid" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id
                            WHERE
                                MS.wf_status_id = 9 AND MS.paid_status_id = 1 AND
                                MS.branch_id = " . Auth::user()->branch_id . " AND
                                MS.company_id = " . Auth::user()->company_id . "
                            ORDER BY MS.id ASC");
            break;
            case "Completed" :
                $data = DB::SELECT("SELECT
                                MS.polis_no,
                                MC.full_name AS nama_tertanggung,
                                RP.definition AS produk,
                                MS.ins_amount AS sum_insured,
                                MS.premi_amount AS premium,
                                MS.disc_amount AS disc_amount,
                                MS.net_amount AS nett_premium,
                                MS.comp_fee_amount AS commision,
                                MS.agent_fee_amount AS agent_fee,
                                MS.ins_fee AS nett_to_underwriter,
                                RIT.definition AS inv_type,
                                MS.start_date,
                                MS.end_date,
                                RA.full_name AS agent_name,
                                RS.definition AS segment
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                                LEFT JOIN ref_cust_segment RS ON MS.segment_id = RS.id
                                                LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                LEFT JOIN ref_invoice_type RIT ON MS.invoice_type_id = RIT.id
                            WHERE
                                (MS.wf_status_id = 9 AND MS.paid_status_id = 1 AND is_splitted = true) AND
                                MS.branch_id = " . Auth::user()->branch_id . " AND
                                MS.company_id = " . Auth::user()->company_id . "
                            ORDER BY MS.id ASC");
            break;


        }

        return $data;
    }

    public function exportNominativeReport(Request $request)
    {
        // dd($request);
        $ext = $request->input('file');
        $sts = $request->input('status');

        if ( $sts ) {
            $param['data'] = $this->getQueryNominativeReport($sts);
            $param['status'] = $sts;
        } else {
            $param['data'] = $this->getQueryNominativeReport('All');
            $param['status'] = 'All';
        }

        $branch_id_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($branch_id_user);

        if ( $ext == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_nominative_report', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Nominatif Customer Aktif - ' . date('d/m/Y') . '.pdf');

        } else {
            // export to excel
            Excel::create('Laporan Nominatif Customer Aktif - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_nominative_report', $param);
                });
            })->export('xlsx');
        }
    }

    public function salesCustomerSegment(Request $request)
    {
        $data = '';
        $start_date = '';
        $end_date = '';
        $idbranch = 'konsol';

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if( $request->get('search') ) {

            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');

            $startDateFormat = date('Y-m-d', strtotime($request->get('start_date')));
            $endDateFormat = date('Y-m-d', strtotime($request->get('end_date')));

            if ( $startDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $start_date . '"');
            }

            if ( $endDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $end_date . '"');
            }

            // branch changed
            if( $request->get('id') ) {
                $idbranch = $request->get('id');

                if($request->get('id')=='konsol') {
                    $data = \DB::select("SELECT
                                            RCS.id,
                                            RCS.definition,
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM
                                            master_sales MS JOIN ref_cust_segment RCS ON MS.segment_id = RCS.id
                                        WHERE inv_date BETWEEN '". $startDateFormat ."' AND
                                                               '". $endDateFormat ."' AND
                                                               MS.company_id=" . Auth::user()->company_id . "
                                        GROUP BY RCS.id, RCS.definition");
                } else {

                    if ( !is_numeric($idbranch) ) {
                        abort(403, 'Invalid branch id "' . $idbranch . '". Must be numeric');
                    }

                    $param['short_code'] = $this->getShortCodeBranch($idbranch);
                    $data = \DB::select("SELECT
                                            RCS.id,
                                            RCS.definition,
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM
                                            master_sales MS JOIN ref_cust_segment RCS ON MS.segment_id = RCS.id
                                        WHERE inv_date BETWEEN '". $startDateFormat ."' AND
                                                            '". $endDateFormat ."' AND
                                                            MS.company_id = " . Auth::user()->company_id . " AND
                                                            MS.branch_id = " . $idbranch . "
                                        GROUP BY RCS.id, RCS.definition");
                }

            } else {
              $data = \DB::select("SELECT
                                        RCS.id,
                                        RCS.definition,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM
                                        master_sales MS JOIN ref_cust_segment RCS ON MS.segment_id = RCS.id
                                    WHERE inv_date BETWEEN '". $startDateFormat ."' AND
                                                           '". $endDateFormat ."' AND
                                                           MS.company_id=" . Auth::user()->company_id . "
                                    GROUP BY RCS.id, RCS.definition");
            }

        }


        $master_branch = DB::SELECT("SELECT is_ho FROM master_branch where id = " . Auth::user()->branch_id);

        if ($master_branch[0]->is_ho == false) {
           $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id." AND id = " . Auth::user()->branch_id);
        } else {
            // head office show all branch
            $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
        }

        $param['branch'] = $branch;
        $param['idbranch'] = $idbranch;
        $param['data'] = $data;
        $param['start_date'] = $start_date;
        $param['end_date'] = $end_date;



        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_customer_segment',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_customer_segment',$param);
        }

    }

    public function salesCompany(Request $request)
    {
        $data = '';
        $start_date = '';
        $end_date = '';
        $idbranch = 'konsol';

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if( $request->get('search') ) {

            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');


            $startDateFormat = date('Y-m-d', strtotime($request->get('start_date')));
            $endDateFormat = date('Y-m-d', strtotime($request->get('end_date')));

            if ( $startDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $start_date . '"');
            }

            if ( $endDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $end_date . '"');
            }

            // branch changed
            if( $request->get('id') ) {
                $idbranch = $request->get('id');

                if($request->get('id')=='konsol') {
                    $data = \DB::select("SELECT
                                        RCT.id,
                                        RCT.definition,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM
                                        master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                        LEFT JOIN ref_cust_type RCT ON RCT.id = MC.cust_type_id
                                    WHERE inv_date BETWEEN '". $startDateFormat ."' AND
                                                           '". $endDateFormat ."' AND
                                                           MS.company_id=" . Auth::user()->company_id . "
                                    GROUP BY RCT.id, RCT.definition");
                } else {

                    if ( !is_numeric($idbranch) ) {
                        abort(403, 'Invalid branch id "' . $idbranch . '". Must be numeric');
                    }

                    $param['short_code'] = $this->getShortCodeBranch($idbranch);
                    $data = \DB::select("SELECT
                                        RCT.id,
                                        RCT.definition,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM
                                        master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                        LEFT JOIN ref_cust_type RCT ON RCT.id = MC.cust_type_id
                                    WHERE inv_date BETWEEN '". $startDateFormat ."' AND
                                                           '". $endDateFormat ."' AND
                                                           MS.company_id =" . Auth::user()->company_id . " AND
                                                           MS.branch_id = " . $idbranch . "
                                    GROUP BY RCT.id, RCT.definition");
                }

            } else {
              $data = \DB::select("SELECT
                                        RCT.id,
                                        RCT.definition,
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM
                                        master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                        LEFT JOIN ref_cust_type RCT ON RCT.id = MC.cust_type_id
                                    WHERE inv_date BETWEEN '". $startDateFormat ."' AND
                                                           '". $endDateFormat ."' AND
                                                           MS.company_id=" . Auth::user()->company_id . "
                                    GROUP BY RCT.id, RCT.definition");
            }

        }


        $master_branch = DB::SELECT("SELECT is_ho FROM master_branch where id = " . Auth::user()->branch_id);

        if ($master_branch[0]->is_ho == false) {
           $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id." AND id = " . Auth::user()->branch_id);
        } else {
            // head office show all branch
            $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
        }

        $param['branch'] = $branch;
        $param['idbranch'] = $idbranch;
        $param['data'] = $data;
        $param['start_date'] = $start_date;
        $param['end_date'] = $end_date;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_company',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_company',$param);
        }

    }

    // is parent true
    public function salesOfficer(Request $request)
    {
        $data = '';
        $start_date = '';
        $end_date = '';
        $idbranch = 'konsol';

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if ( $request->get('search' )) {

            $start_date=$request->get('start_date');
            $end_date=$request->get('end_date');

            $startDateFormat = date('Y-m-d', strtotime($request->get('start_date')));
            $endDateFormat = date('Y-m-d', strtotime($request->get('end_date')));

            if ( $startDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $start_date . '"');
            }

            if ( $endDateFormat == "1970-01-01") {
                abort(403, 'Invalid date format "' . $end_date . '"');
            }

            if( $request->get('id') ) {
                $idbranch = $request->get('id');

                if ( !is_numeric($idbranch) ) {
                    abort(403, 'Invalid branch id "' . $idbranch . '". Must be numeric');
                }

                if($request->get('id')=='konsol'){

                   $data = \DB::select("SELECT
                                            RA.parent_id as id,
                                            (SELECT full_name FROM ref_agent WHERE id = RA.parent_id),
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM
                                            master_sales MS LEFT JOIN ref_agent RA on RA.id = MS.agent_id
                                        WHERE inv_date BETWEEN '". $request->get('start_date') ."' AND
                                                                '". $request->get('end_date') ."' AND
                                                                MS.company_id=" . Auth::user()->company_id . " AND
                                                                RA.is_parent = false
                                        GROUP BY RA.parent_id");
                }else{

                  $param['short_code'] = $this->getShortCodeBranch($idbranch);
                  $data = \DB::select("SELECT
                                            RA.parent_id as id,
                                            (SELECT full_name FROM ref_agent WHERE id = RA.parent_id),
                                            sum(MS.premi_amount) AS premi_amount,
                                            sum(MS.agent_fee_amount) AS agent_fee_amount,
                                            sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                            sum(MS.net_amount) AS net_amount,
                                            sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                        FROM
                                            master_sales MS LEFT JOIN ref_agent RA on RA.id = MS.agent_id
                                        WHERE inv_date BETWEEN '". $request->get('start_date') ."' AND
                                                                '". $request->get('end_date') ."' AND
                                                                MS.company_id = " . Auth::user()->company_id . " AND
                                                                MS.branch_id  = " . $idbranch . " AND
                                                                RA.is_parent = false
                                        GROUP BY RA.parent_id");

                }

            }else{
              $data = \DB::SELECT("SELECT
                                        RA.parent_id as id,
                                        (SELECT full_name FROM ref_agent WHERE id = RA.parent_id),
                                        sum(MS.premi_amount) AS premi_amount,
                                        sum(MS.agent_fee_amount) AS agent_fee_amount,
                                        sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                        sum(MS.net_amount) AS net_amount,
                                        sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                                    FROM
                                        master_sales MS LEFT JOIN ref_agent RA on RA.id = MS.agent_id
                                    WHERE inv_date BETWEEN '". $request->get('start_date') ."' AND
                                                           '". $request->get('end_date') ."' AND
                                                           MS.company_id=" . Auth::user()->company_id . " AND
                                                           RA.is_parent = false
                                    GROUP BY RA.parent_id");

            }

        }

      $master_branch = DB::SELECT("SELECT is_ho FROM master_branch where id = " . Auth::user()->branch_id);

      if ($master_branch[0]->is_ho == false) {
         $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id." AND id = " . Auth::user()->branch_id);
      } else {
          // head office show all branch
          $branch = DB::select("select * from master_branch where company_id=".Auth::user()->company_id);
      }




       $param['branch']=$branch;
       $param['idbranch']=$idbranch;
       $param['data']=$data;
       $param['start_date']=$start_date;
       $param['end_date']=$end_date;




        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_officer_parent',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_officer_parent',$param);
        }
    }

    public function salesOfficerDetail(Request $request) {

        $start_date = date('Y-m-d', strtotime($request->get('start_date')));
        $end_date = date('Y-m-d', strtotime($request->get('end_date')));

        if ( $start_date == "1970-01-01") {
            abort(403, 'Invalid date format "' . $request->get('start_date') . '"');
        }

        if ( $end_date == "1970-01-01") {
            abort(403, 'Invalid date format "' . $request->get('end_date') . '"');
        }

        $data = \DB::SELECT("SELECT
                                RA.id,
                                RA.full_name,
                                sum(MS.premi_amount) AS premi_amount,
                                sum(MS.agent_fee_amount) AS agent_fee_amount,
                                sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                sum(MS.net_amount) AS net_amount,
                                sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                            FROM
                                master_sales MS LEFT JOIN ref_agent RA on RA.id = MS.agent_id
                            WHERE MS.company_id= " . Auth::user()->company_id . " AND
                                  RA.is_parent = false AND
                                  RA.parent_id = " . $request->get('id') . " AND
                                  MS.inv_date BETWEEN '". $start_date ."' AND '". $end_date ."'
                            GROUP BY RA.id, RA.full_name
                            ");


        $param['data']=$data;
        $param['type']=$request->get('type');
        $param['start_date'] = $request->get('start_date');
        $param['end_date'] = $request->get('end_date');

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_officer_detail',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_officer_detail',$param);
        }


    }

    public function salesOfficerDetailCustomer(Request $request) {

        $start_date = date('Y-m-d', strtotime($request->get('start_date')));
        $end_date = date('Y-m-d', strtotime($request->get('end_date')));

        if ( $start_date == "1970-01-01") {
            abort(403, 'Invalid date format "' . $request->get('start_date') . '"');
        }

        if ( $end_date == "1970-01-01") {
            abort(403, 'Invalid date format "' . $request->get('end_date') . '"');
        }

        $data = \DB::SELECT("SELECT
                                MS.id,
                                MC.full_name,
                                sum(MS.premi_amount) AS premi_amount,
                                sum(MS.agent_fee_amount) AS agent_fee_amount,
                                sum(MS.comp_fee_amount) + sum(MS.admin_amount) + sum(MS.comp_fee_amount) AS comp_fee_amount,
                                sum(MS.net_amount) AS net_amount,
                                sum(MS.ins_fee) + sum(MS.materai_amount) + sum(MS.polis_amount) AS nett_to_underwriter
                            FROM
                                master_sales MS LEFT JOIN master_customer MC ON MC.id = MS.customer_id
                            WHERE
                                MS.agent_id = " . $request->get('id') . " AND
                                MS.inv_date BETWEEN '". $start_date ."' AND '". $end_date ."'
                            GROUP BY MS.id, MC.full_name
                            ");



        $param['data']=$data;
        $param['type']=$request->get('type');

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.sales_officer_detail_customer',$param);
        }else {
            return view('master.master')->nest('child', 'report.sales_officer_detail_customer',$param);
        }
    }

    public function invoicePendingSplitPayment(Request $request)
    {
        $filterStatus = $request->input('filter');

        if ( $filterStatus ) {

            $arrSts = ['All', 'Current Month', 'Next Month'];

            if ( in_array($filterStatus, $arrSts) ) {
                $param['data'] = $this->getQuerySplitPayment($filterStatus);
            } else {
                $param['data'] = NULL;
                abort(403, 'Filter "' . $filterStatus . '" doesn\'t exist');
            }

            switch ( $filterStatus ) {
                case "All" : $param['sts'] = '1000'; break;
                case "Current Month" : $param['sts'] = '1'; break;
                case "Next Month" : $param['sts'] = '2'; break;
                default: $param['sts'] = '1000';
            }

        } else {
            $param['data'] = $this->getQuerySplitPayment('All');
            $param['sts'] = '1000';
        }

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.invoice_pending',$param);
        }else {
            return view('master.master')->nest('child', 'report.invoice_pending',$param);
        }
    }

    public function getQuerySplitPayment($filter)
    {
        $currentDate = date('Y-m-d');
        switch ( $filter ) {
            case "All" :
                    // no filter
                    $data = DB::SELECT("SELECT
                                            MC.full_name,
                                            RP.definition,
                                            RA.full_name AS agent_name,
                                            CASE WHEN MS.is_agent_paid = 1 THEN 0 ELSE MS.agent_fee_amount END AS agent_fee,
                                            CASE WHEN MS.is_company_paid = 1 THEN 0 ELSE MS.comp_fee_amount END AS commision,
                                            CASE WHEN MS.is_premi_paid = 1 THEN 0 ELSE COALESCE(MS.ins_fee,0) + COALESCE(MS.polis_amount,0) + COALESCE(MS.materai_amount,0) END AS ins_fee
                                        FROM master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                            LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                            LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                        WHERE
                                            ((MS.is_agent_paid = 0 OR is_company_paid = 0 OR is_premi_paid = 0) OR
                                            (MS.is_agent_paid IS NULL OR is_company_paid IS NULL OR is_premi_paid IS NULL)) AND
                                             MS.paid_status_id = 1
                                        ");
            break;
            case "Current Month" :
                    $data = DB::SELECT("SELECT
                                            MC.full_name,
                                            RP.definition,
                                            RA.full_name AS agent_name,
                                            CASE WHEN MS.is_agent_paid = 1 THEN 0 ELSE MS.agent_fee_amount END AS agent_fee,
                                            CASE WHEN MS.is_company_paid = 1 THEN 0 ELSE MS.comp_fee_amount END AS commision,
                                            CASE WHEN MS.is_premi_paid = 1 THEN 0 ELSE COALESCE(MS.ins_fee,0) + COALESCE(MS.polis_amount,0) + COALESCE(MS.materai_amount,0) END AS ins_fee
                                        FROM master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                             LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                             LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                        WHERE
                                            DATE_PART('MONTH', end_date) = DATE_PART('MONTH', current_date) AND
                                            DATE_PART('YEAR', end_date) =  DATE_PART('YEAR', current_date) AND
                                            ((MS.is_agent_paid = 0 OR is_company_paid = 0 OR is_premi_paid = 0) OR
                                            (MS.is_agent_paid IS NULL OR is_company_paid IS NULL OR is_premi_paid IS NULL)) AND
                                            MS.paid_status_id = 1
                                        ");
            break;
            case "Next Month" :
                $data = DB::SELECT("SELECT
                                        MC.full_name,
                                        RP.definition,
                                        RA.full_name AS agent_name,
                                        CASE WHEN MS.is_agent_paid = 1 THEN 0 ELSE MS.agent_fee_amount END AS agent_fee,
                                        CASE WHEN MS.is_company_paid = 1 THEN 0 ELSE MS.comp_fee_amount END AS commision,
                                        CASE WHEN MS.is_premi_paid = 1 THEN 0 ELSE COALESCE(MS.ins_fee,0) + COALESCE(MS.polis_amount,0) + COALESCE(MS.materai_amount,0) END AS ins_fee
                                    FROM master_sales MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                         LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                         LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                    WHERE
                                        DATE_PART('MONTH', end_date) = DATE_PART('MONTH', current_date) + 1 AND
                                        DATE_PART('YEAR', end_date) = CASE
                                                                        WHEN
                                                                            DATE_PART('MONTH', current_date) = 12 THEN DATE_PART('YEAR', current_date) + 1
                                                                            ELSE DATE_PART('YEAR', current_date)
                                                                        END AND
                                        ((MS.is_agent_paid = 0 OR is_company_paid = 0 OR is_premi_paid = 0) OR
                                        (MS.is_agent_paid IS NULL OR is_company_paid IS NULL OR is_premi_paid IS NULL)) AND
                                        MS.paid_status_id = 1
                                        ");
            break;
        }

        return $data;
    }

    public function exportInvoicePendingSplitPayment(Request $request)
    {
        $ext = $request->input('file');
        $sts = $request->input('filter');

        if ( $sts ) {
            $param['data'] = $this->getQuerySplitPayment($sts);
            $param['status'] = $sts;
        } else {
            $param['data'] = $this->getQuerySplitPayment('All');
            $param['status'] = 'All';
        }

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if ( $ext == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_invoice_pending', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Debit Note Pending Split Payment - ' . date('d/m/Y') . '.pdf');
        } else {
            // export to excel
            Excel::create('Laporan Debit Note Pending Split Payment - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_invoice_pending', $param);
                });
            })->export('xlsx');
        }
    }

    public function saveToFolder(Request $request)
    {

            $sy = collect(\DB::SELECT('SELECT * FROM ref_system_date'))->first();
            $system_date = date('Ymd',strtotime($sy->last_date));

            // $system_date = date('Ymd', strtotime('-1 days', strtotime($sy->current_date)));
            // $system_date_n = date('Y-m-d', strtotime('-1 days', strtotime($sy->current_date)));


            $destination_path = public_path('batch_file/'.$sy->last_date);
            if(!File::isDirectory($destination_path)){
                File::makeDirectory($destination_path, 0777, true, true);
            }

            // KONSOL
            $destination_path_konsol = public_path('batch_file/'.$sy->last_date.'/KONSOL');
            if(!File::isDirectory($destination_path_konsol)){
                File::makeDirectory($destination_path_konsol, 0777, true, true);
            }


            $b_name = 'KONSOL_';

            if ($request->type == 'bs') {

              $param["aktiva"] = $this->getBalanceSheet('aktiva','konsol');
              $param["pasiva"] = $this->getBalanceSheet('pasiva','konsol');
              $param["laba_rugi"] = $this->getLabarugi('laba_rugi','konsol');


              $fname = $b_name."BS".$system_date;

              $pdf = PDF::loadView('cetak.pdf_balance_sheet', $param)->setPaper('a4', 'potrait');

              $data = $pdf->output();
              $file = fopen($destination_path_konsol."/".$fname.".pdf", 'w');
              fwrite($file, $data);
              fclose($file);


            }elseif ($request->type == 'pl') {
              $param["aktiva"] = $this->getBalanceSheet('aktiva','konsol');
              $param["pasiva"] = $this->getBalanceSheet('pasiva','konsol');
              $param["laba_rugi"] = $this->getLabarugi('laba_rugi','konsol');

              $fname = $b_name."PL".$system_date;

              $pdf = PDF::loadView('cetak.pdf_profit_loss', $param)->setPaper('a4', 'potrait');

              $data = $pdf->output();
              $file = fopen($destination_path_konsol."/".$fname.".pdf", 'w');
              fwrite($file, $data);
              fclose($file);


            }elseif ($request->type == 'cn') {

                $ext = $request->input('file');
                $sts = $request->input('status');

                for ($i=1; $i <= 3; $i++) {

                    if ($i == 1) {
                        $sts = 'Aktif';
                        $fname = $b_name."CN".$system_date."A";
                    }elseif ($i == 2) {
                        $sts = 'Paid';
                        $fname = $b_name."CN".$system_date."P";
                    }elseif ($i == 3) {
                        $sts = 'Completed';
                        $fname = $b_name."CN".$system_date."C";
                    }

                    $param['data'] = $this->getQueryNominativeReport($sts);
                    $param['status'] = $sts;
                    $pdf = PDF::loadView('cetak.pdf_nominative_report', $param)->setPaper('a4', 'landscape');

                    $data = $pdf->output();
                    $file = fopen($destination_path_konsol."/".$fname.".pdf", 'w');
                    fwrite($file, $data);
                    fclose($file);

                    Excel::create($fname, function($excel) use ($param){
                        $excel->sheet('Sheet 1',function($sheet) use ($param){
                            $sheet->loadView('cetak.xlsx_nominative_report', $param);
                        });
                    })->store('xlsx', $destination_path_konsol);
                    #Store and export

                }

              // MASTER COA
            }elseif ($request->type == 'coa') {
                $param['data'] = 'waw';

                $fname = $b_name."COA".$system_date;
                Excel::create($fname, function($excel) use ($param){
                    $excel->sheet('Sheet 1',function($sheet) use ($param){
                        $sheet->loadView('cetak.xlsx_coa', $param);
                    });
                })->store('xlsx', $destination_path_konsol);

            }elseif ($request->type == 'due_date') {

              $param['data'] = json_decode($this->data_renewal_list_due_date($request));     

              $fname = $b_name."RENEWAL_LIST_DUE_DATE".$system_date;

              Excel::create($fname, function($excel) use ($param){
                  $excel->sheet('Sheet 1',function($sheet) use ($param){
                      $sheet->loadView('cetak.xlsx_renewal_list_due_date', $param);
                  });
              })->store('xlsx', $destination_path_konsol);

              Excel::create("RENEWAL_LIST_DUE_DATE".$system_date, function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_renewal_list_due_date', $param);
                });
            })->store('xlsx', public_path('batch_file'));

          }

            // JIKA BRANCH
            $getBranch = \DB::SELECT("SELECT * FROM master_branch where is_ho = 'f'");

            foreach ($getBranch as $keyBranch => $vBranch) {

              $destination_path_branch = public_path('batch_file/'.$sy->last_date.'/'.$vBranch->short_code);
              if(!File::isDirectory($destination_path_branch)){
                  File::makeDirectory($destination_path_branch, 0777, true, true);
              }

              if ($request->type == 'bs') {

                $param["aktiva"] = $this->getBalanceSheet('aktiva',$vBranch->id);
                $param["pasiva"] = $this->getBalanceSheet('pasiva',$vBranch->id);
                $param["laba_rugi"] = $this->getLabarugi('laba_rugi',$vBranch->id);

                $fname = $vBranch->short_code."_BS".$system_date;

                $pdf = PDF::loadView('cetak.pdf_balance_sheet', $param)->setPaper('a4', 'potrait');

                $data = $pdf->output();
                $file = fopen($destination_path_branch."/".$fname.".pdf", 'w');
                fwrite($file, $data);
                fclose($file);


              }elseif ($request->type == 'pl') {
                $param["aktiva"] = $this->getBalanceSheet('aktiva',$vBranch->id);
                $param["pasiva"] = $this->getBalanceSheet('pasiva',$vBranch->id);
                $param["laba_rugi"] = $this->getLabarugi('laba_rugi',$vBranch->id);

                $fname = $vBranch->short_code."_PL".$system_date;

                $pdf = PDF::loadView('cetak.pdf_profit_loss', $param)->setPaper('a4', 'potrait');

                $data = $pdf->output();
                $file = fopen($destination_path_branch."/".$fname.".pdf", 'w');
                fwrite($file, $data);
                fclose($file);


              }elseif ($request->type == 'cn') {

                  $ext = $request->input('file');
                  $sts = $request->input('status');

                  for ($i=1; $i <= 3; $i++) {

                      if ($i == 1) {
                          $sts = 'Aktif';
                          $fname = $vBranch->short_code."_CN".$system_date."A";
                      }elseif ($i == 2) {
                          $sts = 'Paid';
                          $fname = $vBranch->short_code."_CN".$system_date."P";
                      }elseif ($i == 3) {
                          $sts = 'Completed';
                          $fname = $vBranch->short_code."_CN".$system_date."C";
                      }

                      $param['data'] = $this->getQueryNominativeReport($sts);
                      $param['status'] = $sts;
                      $pdf = PDF::loadView('cetak.pdf_nominative_report', $param)->setPaper('a4', 'landscape');

                      $data = $pdf->output();
                      $file = fopen($destination_path_branch."/".$fname.".pdf", 'w');
                      fwrite($file, $data);
                      fclose($file);

                      Excel::create($fname, function($excel) use ($param){
                          $excel->sheet('Sheet 1',function($sheet) use ($param){
                              $sheet->loadView('cetak.xlsx_nominative_report', $param);
                          });
                      })->store('xlsx', $destination_path_branch);
                      #Store and export

                  }

                // MASTER COA
              }elseif ($request->type == 'coa') {
                  $param['data'] = 'waw';

                  $fname = $vBranch->short_code."_COA".$system_date;
                  Excel::create($fname, function($excel) use ($param){
                      $excel->sheet('Sheet 1',function($sheet) use ($param){
                          $sheet->loadView('cetak.xlsx_coa', $param);
                      });
                  })->store('xlsx', $destination_path_branch);

              }

              elseif ($request->type == 'due_date') {
              
                $param['data'] = json_decode($this->data_renewal_list_due_date($request));     
  
                $fname = $vBranch->short_code."_RENEWAL_LIST_DUE_DATE".$system_date;
                Excel::create($fname, function($excel) use ($param){
                    $excel->sheet('Sheet 1',function($sheet) use ($param){
                        $sheet->loadView('cetak.xlsx_renewal_list_due_date', $param);
                    });
                })->store('xlsx', $destination_path_branch);
  
            }

            }

            // dd($param);

            return json_encode(['rc'=>3,'rm'=>'success',
            'param'=>$param,
            'path'=>"RENEWAL_LIST_DUE_DATE".$system_date]);

    }

    public function sendMailAny(Request $request)
    {

      $get_mail = collect(\DB::select("SELECT * from master_company"))->first();
      
      $toEmail[0] = $get_mail->email_1;
      $toEmail[1] = $get_mail->email_2;
      $toEmail[2] = 'znurzamanz@gmail.com';

      $data_renewal = json_decode($this->data_renewal_list_due_date($request));
      // $count_data = count($data_renewal);

      $data = array(
        'sendto'=> $toEmail,
        'company'=> $get_mail,
        'count_data_renewal'=> count($data_renewal->data),
        'subject' => 'LAPORAN RENEWAL LIST DUE DATE',
        'path' => public_path('batch_file').'/'.$request->input('path').'.xlsx');

      $this->sendMail('email.due_date',$data);

      return json_encode(['rc'=>0,'rm'=>'success','email'=>$toEmail]);
    }

    public function getQueryDueDateInstallment()
    {
        $data = \DB::SELECT("SELECT
                                MS.id,
                                MC.full_name,
                                RC.definition,
                                MS.premi_amount,
                                RPS.definition AS paid,
                                MS.polis_no,
                                MS.is_agent_paid,
                                MS.is_company_paid,
                                MS.is_premi_paid,
                                MS.invoice_type_id,
                                MS.url_dokumen,
                                MS.invoice_type_id,
                                MS.due_date_jrn as due_date
                            FROM master_sales_schedule MS
                                 JOIN master_customer MC on MC.id = MS.customer_id
                                 JOIN ref_product RC on RC.id = MS.product_id
                                 JOIN ref_paid_status RPS on RPS.id = MS.paid_status_id
                            WHERE MS.invoice_type_id = 2 AND
                                  MS.paid_status_id = 0 AND
                                  MS.due_date_jrn <= current_date AND
                                  MS.branch_id = " . Auth::user()->branch_id . " AND
                                  MS.company_id = ".Auth::user()->company_id);
            return $data;
    } // end function

    public function dueDateInstallment(Request $request)
    {
        $param['data'] = $this->getQueryDueDateInstallment();

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.due_date_installment', $param);
        }else {
            return view('master.master')->nest('child', 'report.due_date_installment', $param);
        }
    } // end function

    public function exportDueDateInstallment(Request $request)
    {
        $typeFile = $request->input('file');
        $param['data'] = $this->getQueryDueDateInstallment();

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if ( $typeFile == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_due_date_installment', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Due Date Installment - ' . date('d/m/Y') . '.pdf');
        } else {
            // export to excel
            Excel::create('Laporan Due Date Installment - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_due_date_installment', $param);
                });
            })->export('xlsx');
        }
    } // end function

    
    public function QueryQSReport($type)
    {
      if ($type == 'aktif') {
          $data = \DB::SELECT("SELECT
            mc.full_name ,
            mqv.qs_no ,
            msp.polis_no ,
            msp.start_date_polis ,
            msp.end_date_polis ,
            msp.ins_amount ,
            msp.net_amount
          from
            master_qs_vehicle mqv
          left join master_customer mc on
            mqv.customer_id = mc.id
          left join master_cn_vehicle mcv on
            mqv.id = mcv.qs_id
          left join master_cf_vehicle mcv2 on
            mcv.id = mcv2.cn_id
          left join master_sales_polis msp on
            mcv2.id = msp.coc_id
          where msp.polis_no is not null");
      }else{
          $data = \DB::SELECT("SELECT
            mc.full_name ,
            mqv.qs_no ,
            msp.polis_no ,
            msp.start_date_polis ,
            msp.end_date_polis ,
            msp.ins_amount ,
            msp.net_amount
          from
            master_qs_vehicle mqv
          left join master_customer mc on
            mqv.customer_id = mc.id
          left join master_cn_vehicle mcv on
            mqv.id = mcv.qs_id
          left join master_cf_vehicle mcv2 on
            mcv.id = mcv2.cn_id
          left join master_sales_polis msp on
            mcv2.id = msp.coc_id
            where msp.polis_no is null");
      }
       
          
        return $data;
    } // end function

    public function qsReport(Request $request,$type)
    {
        $param['data'] = $this->QueryQSReport($type);
        $param['type'] = $type;

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.qs_report', $param);
        }else {
            return view('master.master')->nest('child', 'report.qs_report', $param);
        }
    } // end function

    public function qsReport_export(Request $request)
    {
        $typeFile = $request->input('file');
        $type = $request->input('jenis');

        $param['type'] = $type;
        $param['data'] = $this->QueryQSReport($type);

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if ( $typeFile == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_qs_report', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Nominatif QS Status Polis Aktif - ' . date('d/m/Y') . '.pdf');
        } else {
            // export to excel
            Excel::create('Laporan Nominatif QS Status Polis Aktif - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_qs_report', $param);
                });
            })->export('xlsx');
        }
    } // end function

    public function detail_product_export(Request $request)
    {
        $typeFile = $request->input('file');
       
        $param['product_id'] = $request->input('product_id');
        $param['product_name'] = $request->input('product_name');
        $param['typeFile'] = $request->input('file');
        $param['table_data'] = [];

        // export to excel
        $param['ck_excel_label'] = $request->input('ck_excel_label');
        $param['ck_excel_input_name'] = $request->input('ck_excel_input_name');

        Excel::create('template_detail_product - ' . date('d/m/Y') . '', function($excel) use ($param){
            $excel->sheet('Sheet 1',function($sheet) use ($param){
                $sheet->loadView('cetak.xlsx_detail_product', $param);
            });
        })->export('xlsx');
        
    }

    public function table_detail_product_export(Request $request)
    {
        $typeFile = $request->input('file');
       
        $param['product_id'] = $request->input('product_id');
        $param['product_name'] = $request->input('product_name');
        $param['typeFile'] = $request->input('file');

        // if ( $typeFile == 'xls_table' ) {
        $headerDetail = \DB::select("SELECT * from ref_insured_detail 
        where product_id = ? and is_active = true and input_name <> 'sts' order by bit_id",[(string)$request->input('product_id')]);
        
        $arr_excel_label = [];
        $arr_excel_input_name = [];

        foreach ($headerDetail as $key => $hd) {
          $arr_excel_label[$key] = $hd->label;
          $arr_excel_input_name[$key] = $hd->input_name;

        }

        $param['ck_excel_label'] = $arr_excel_label;
        $param['ck_excel_input_name'] = $arr_excel_input_name;
        $param['table_data'] = $request->input('table_data');

        // dd($param);

            Excel::create('table_detail_product', function($excel) use ($param){
                  $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_detail_product', $param);
                });
              })->store('xlsx', public_path('excel'));
              
              return "excel/table_detail_product.xlsx";

            // return $output;

        // } 
    }

    public function detail_product_import(Request $request)
    {
      if($request->hasFile('file_import')){
          $files = $request->file('file_import');
            $getData =  Excel::load($files, function($reader) {})->get()->toArray();
            // $getHeader =  Excel::load($files, function($reader) {})->toObject();
            
            $product_id = $getData[0]['zn_product_id'];
            $zn_type_import = $getData[0]['zn_type_import'];

            if ($product_id) {
              $headerDetail = \DB::select("SELECT * from ref_insured_detail where product_id = ? and is_active = true and input_name <> 'sts' order by bit_id",[(string)$product_id]);
            }else{
              $headerDetail = [];
              $product_id = '';
            }


      }else{
        $getData = [];
        $headerDetail = [];
        $product_id = '';

      }

      return response()->json([
        'rc' => 0,
        'rm' => $getData,
        'detail' => $headerDetail,
        'product_id' => $product_id,
        'zn_type_import' => $zn_type_import,


    ]);

      // $file_import = $request->input('file_import');

      // dd($file_import);
    //   Excel::load($file_import, function($reader) {

    //     dd($results = $reader->all());
    //     // reader methods
    
    // });
    }


    public function claimReport(Request $request)
    {
        $filterStatus = $request->input('filter');

        if ( $filterStatus ) {

            $arrSts = ['All', 'Claim On Process', 'Claim Accepted', 'Claim Finished'];

            if ( in_array($filterStatus, $arrSts) ) {
                $param['data'] = $this->getQueryClaimReport($filterStatus);
            } else {
                $param['data'] = NULL;
                abort(403, 'Filter "' . $filterStatus . '" doesn\'t exist');
            }

            switch ( $filterStatus ) {
                case "All" : $param['sts'] = '1000'; break;
                case "Claim On Process" : $param['sts'] = '1'; break;
                case "Claim Accepted" : $param['sts'] = '2'; break;
                case "Claim Finished" : $param['sts'] = '3'; break;
                default: $param['sts'] = '1000'; break;
            }

        } else {
            $param['data'] = $this->getQueryClaimReport('All');
            $param['sts'] = '1000';
        }

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.claim_report',$param);
        }else {
            return view('master.master')->nest('child', 'report.claim_report',$param);
        }
    }

    public function getQueryClaimReport($filter)
    {
        $currentDate = date('Y-m-d');
        switch ( $filter ) {
            case "All" :
                    // no filter
                    $data = \DB::SELECT("SELECT
                                            MC.*,
                                            RCS.status_definition
                                        FROM master_claim MC LEFT JOIN ref_claim_status RCS ON RCS.id = MC.claim_status_id
                                        WHERE MC.workflow_status_id IN (1,3,4)
                                        ORDER BY MC.id ASC");
            break;
            case "Claim On Process" :
                $data = \DB::SELECT("SELECT
                                        MC.*,
                                        RCS.status_definition
                                    FROM master_claim MC LEFT JOIN ref_claim_status RCS ON RCS.id = MC.claim_status_id
                                    WHERE MC.workflow_status_id IN (1,3,4) AND MC.claim_status_id = 1
                                    ORDER BY MC.id ASC");
            break;
            case "Claim Accepted" :
                $data = \DB::SELECT("SELECT
                                        MC.*,
                                        RCS.status_definition
                                    FROM master_claim MC LEFT JOIN ref_claim_status RCS ON RCS.id = MC.claim_status_id
                                    WHERE MC.workflow_status_id IN (1,3,4) AND MC.claim_status_id = 2
                                    ORDER BY MC.id ASC");
            break;
            case "Claim Finished" :
                $data = \DB::SELECT("SELECT
                                        MC.*,
                                        RCS.status_definition
                                    FROM master_claim MC LEFT JOIN ref_claim_status RCS ON RCS.id = MC.claim_status_id
                                    WHERE MC.workflow_status_id IN (1,3,4) AND MC.claim_status_id = 3
                                    ORDER BY MC.id ASC");
            break;
        }

        return $data;
    }

    public function exportClaimReport(Request $request)
    {
        $ext = $request->input('file');
        $sts = $request->input('filter');

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if ( $sts ) {
            $param['data'] = $this->getQueryClaimReport($sts);
            $param['status'] = $sts;
        } else {
            $param['data'] = $this->getQueryClaimReport('All');
            $param['status'] = 'All';
        }

        if ( $ext == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_claim_report', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Claim Report - ' . date('d/m/Y') . '.pdf');
        } else {
            // export to excel
            Excel::create('Laporan Claim Report - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_claim_report', $param);
                });
            })->export('xlsx');
        }
    } // end function

    public function installmentPendingSplitPayment(Request $request)
    {
        $filterStatus = $request->input('filter');


        if ( $filterStatus ) {

            $arrSts = ['All', 'Current Month', 'Next Month'];

            if ( in_array($filterStatus, $arrSts) ) {
                $param['data'] = $this->getQueryInstallmentSplitPayment($filterStatus);
            } else {
                $param['data'] = NULL;
                abort(403, 'Filter "' . $filterStatus . '" doesn\'t exist');
            }

            switch ( $filterStatus ) {
                case "All" : $param['sts'] = '1000'; break;
                case "Current Month" : $param['sts'] = '1'; break;
                case "Next Month" : $param['sts'] = '2'; break;
                default: $param['sts'] = '1000';
            }

        } else {
            $param['data'] = $this->getQueryInstallmentSplitPayment('All');
            $param['sts'] = '1000';
        }

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'report.installment_pending',$param);
        }else {
            return view('master.master')->nest('child', 'report.installment_pending',$param);
        }
    }

    public function getQueryInstallmentSplitPayment($filter)
    {
        $currentDate = date('Y-m-d');
        switch ( $filter ) {
            case "All" :
                    // no filter
                    $data = DB::SELECT("SELECT
                                            MC.full_name,
                                            RP.definition,
                                            RA.full_name AS agent_name,
                                            CASE WHEN MS.is_agent_paid = 1 THEN 0 ELSE MS.agent_fee_amount END AS agent_fee,
                                            CASE WHEN MS.is_company_paid = 1 THEN 0 ELSE MS.comp_fee_amount END AS commision,
                                            CASE WHEN MS.is_premi_paid = 1 THEN 0 ELSE COALESCE(MS.ins_fee,0) + COALESCE(MS.polis_amount,0) + COALESCE(MS.materai_amount,0) END AS ins_fee
                                        FROM master_sales_schedule MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                            LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                            LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                        WHERE
                                            ((MS.is_agent_paid = 0 OR is_company_paid = 0 OR is_premi_paid = 0) OR
                                            (MS.is_agent_paid IS NULL OR is_company_paid IS NULL OR is_premi_paid IS NULL)) AND
                                             MS.paid_status_id = 1
                                        ");
            break;
            case "Current Month" :
                    $data = DB::SELECT("SELECT
                                            MC.full_name,
                                            RP.definition,
                                            RA.full_name AS agent_name,
                                            CASE WHEN MS.is_agent_paid = 1 THEN 0 ELSE MS.agent_fee_amount END AS agent_fee,
                                            CASE WHEN MS.is_company_paid = 1 THEN 0 ELSE MS.comp_fee_amount END AS commision,
                                            CASE WHEN MS.is_premi_paid = 1 THEN 0 ELSE COALESCE(MS.ins_fee,0) + COALESCE(MS.polis_amount,0) + COALESCE(MS.materai_amount,0) END AS ins_fee
                                        FROM master_sales_schedule MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                             LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                             LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                        WHERE
                                            DATE_PART('MONTH', due_date_jrn) = DATE_PART('MONTH', current_date) AND
                                            DATE_PART('YEAR', due_date_jrn) =  DATE_PART('YEAR', current_date) AND
                                            ((MS.is_agent_paid = 0 OR is_company_paid = 0 OR is_premi_paid = 0) OR
                                            (MS.is_agent_paid IS NULL OR is_company_paid IS NULL OR is_premi_paid IS NULL)) AND
                                            MS.paid_status_id = 1
                                        ");
            break;
            case "Next Month" :
                $data = DB::SELECT("SELECT
                                        MC.full_name,
                                        RP.definition,
                                        RA.full_name AS agent_name,
                                        CASE WHEN MS.is_agent_paid = 1 THEN 0 ELSE MS.agent_fee_amount END AS agent_fee,
                                        CASE WHEN MS.is_company_paid = 1 THEN 0 ELSE MS.comp_fee_amount END AS commision,
                                        CASE WHEN MS.is_premi_paid = 1 THEN 0 ELSE COALESCE(MS.ins_fee,0) + COALESCE(MS.polis_amount,0) + COALESCE(MS.materai_amount,0) END AS ins_fee
                                    FROM master_sales_schedule MS LEFT JOIN master_customer MC ON MS.customer_id = MC.id
                                                         LEFT JOIN ref_product RP ON MS.product_id = RP.id
                                                         LEFT JOIN ref_agent RA ON MS.agent_id = RA.id
                                    WHERE
                                        DATE_PART('MONTH', due_date_jrn) = DATE_PART('MONTH', current_date) + 1 AND
                                        DATE_PART('YEAR', due_date_jrn) = CASE
                                                                        WHEN
                                                                            DATE_PART('MONTH', current_date) = 12 THEN DATE_PART('YEAR', current_date) + 1
                                                                            ELSE DATE_PART('YEAR', current_date)
                                                                        END AND
                                        ((MS.is_agent_paid = 0 OR is_company_paid = 0 OR is_premi_paid = 0) OR
                                        (MS.is_agent_paid IS NULL OR is_company_paid IS NULL OR is_premi_paid IS NULL)) AND
                                        MS.paid_status_id = 1
                                        ");

            break;
        }

        return $data;
    }

    public function exportInstallmentPendingSplitPayment(Request $request)
    {
        $ext = $request->input('file');
        $sts = $request->input('filter');

        $id_branch_user = Auth::user()->branch_id;
        $param['short_code'] = $this->getShortCodeBranch($id_branch_user);

        if ( $sts ) {
            $param['data'] = $this->getQueryInstallmentSplitPayment($sts);
            $param['status'] = $sts;
        } else {
            $param['data'] = $this->getQueryInstallmentSplitPayment('All');
            $param['status'] = 'All';
        }

        if ( $ext == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_installment_pending', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Installment Pending Split Payment - ' . date('d/m/Y') . '.pdf');
        } else {
            // export to excel
            Excel::create('Laporan Installment Pending Split Payment - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_installment_pending', $param);
                });
            })->export('xlsx');
        }
    }

    public function getAktiva($id)
    {

      $tittle = 'Batch Proses';

      $idbranch='konsol';
          if($id){
            $idbranch=$id;

              if($id=='konsol'){

                $aktiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

            $pasiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=1
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");
              }else{

                $aktiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.branch_id=".$idbranch." and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

                $pasiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=1
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");


              }


          }else{
            $aktiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

            $pasiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=1
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");
          }

          if($id){
              $idbranch=$id;
                if($id=='konsol'){
                  $laba_rugi = \DB::select("select
                      id,
                      coa_no,
                      coa_name,
                      balance_type_id,
                      coa_type_id,
                      case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                      from master_coa a left join
                      (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                      from master_coa
                      where coa_parent_id is  not null and company_id=".Auth::user()->company_id."
                      group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                      where coa_type_id in (3,4) and a.company_id=".Auth::user()->company_id."
                      ORDER BY coa_no asc");

                }else{
                  $laba_rugi = \DB::select("select
                      id,
                      coa_no,
                      coa_name,
                      balance_type_id,
                      coa_type_id,
                      case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                      from master_coa a left join
                      (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                      from master_coa
                      where coa_parent_id is  not null and branch_id=".$idbranch." and company_id=".Auth::user()->company_id."
                      group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                      where coa_type_id in (3,4) and a.branch_id=".$idbranch." and a.company_id=".Auth::user()->company_id."
                      ORDER BY coa_no asc");
                }

            }else{
              $laba_rugi = \DB::select("select
                      id,
                      coa_no,
                      coa_name,
                      balance_type_id,
                      coa_type_id,
                      case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                      from master_coa a left join
                      (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                      from master_coa
                      where coa_parent_id is  not null and company_id=".Auth::user()->company_id."
                      group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                      where coa_type_id in (3,4) and a.company_id=".Auth::user()->company_id."
                      ORDER BY coa_no asc");
                }




          return $aktiva;
    }
    public function getPasiva($id)
    {

      $tittle = 'Batch Proses';

      $idbranch='konsol';
          if($id){
            $idbranch=$id;

              if($id=='konsol'){

                $aktiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

            $pasiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=1
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");
              }else{

                $aktiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.branch_id=".$idbranch." and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

                $pasiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=1
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");


              }


          }else{
            $aktiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no  where coa_type_id =1 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");

            $pasiva = \DB::select("select
                    id,
                    coa_no,
                    coa_name,
                    case when coa_no in ('301') then last_os +
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=1
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   (select * from master_coa where company_id=1) p USING (coa_parent_id)
                    )
                    SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
                    FROM   cte
                    where coa_no in ('400','500')) else last_os end as last_os
                    from
                    (select
                    a.id,
                    a.coa_no,
                    coa_name,
                    case when a.coa_no = b.coa_parent_id then b.nom
                      when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
                    from master_coa a left join
                    (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                    from master_coa
                    where coa_parent_id is  not null and length(coa_parent_id) = 6
                    group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
                    (WITH RECURSIVE cte AS (
                      SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
                      FROM   master_coa
                      WHERE  coa_parent_id IS NULL and company_id=".Auth::user()->company_id."
                      UNION  ALL
                      SELECT c.coa_no, p.coa_no, p.last_os
                      FROM   cte c
                      JOIN   master_coa p USING (coa_parent_id)
                    )
                    SELECT coa_no, sum(last_os) AS last_os
                    FROM   cte
                    GROUP  BY 1) c on a.coa_no = c.coa_no where coa_type_id =2 and a.company_id=".Auth::user()->company_id." order by coa_no asc) x;");
          }

          if($id){
              $idbranch=$id;
                if($id=='konsol'){
                  $laba_rugi = \DB::select("select
                      id,
                      coa_no,
                      coa_name,
                      balance_type_id,
                      coa_type_id,
                      case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                      from master_coa a left join
                      (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                      from master_coa
                      where coa_parent_id is  not null and company_id=".Auth::user()->company_id."
                      group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                      where coa_type_id in (3,4) and a.company_id=".Auth::user()->company_id."
                      ORDER BY coa_no asc");

                }else{
                  $laba_rugi = \DB::select("select
                      id,
                      coa_no,
                      coa_name,
                      balance_type_id,
                      coa_type_id,
                      case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                      from master_coa a left join
                      (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                      from master_coa
                      where coa_parent_id is  not null and branch_id=".$idbranch." and company_id=".Auth::user()->company_id."
                      group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                      where coa_type_id in (3,4) and a.branch_id=".$idbranch." and a.company_id=".Auth::user()->company_id."
                      ORDER BY coa_no asc");
                }

            }else{
              $laba_rugi = \DB::select("select
                      id,
                      coa_no,
                      coa_name,
                      balance_type_id,
                      coa_type_id,
                      case when a.coa_no = b.coa_parent_id then b.nom else coalesce(a.last_os,0) end as last_os
                      from master_coa a left join
                      (select coa_parent_id, sum(coalesce(last_os,0)) as nom
                      from master_coa
                      where coa_parent_id is  not null and company_id=".Auth::user()->company_id."
                      group by coa_parent_id ) b on a.coa_no = b.coa_parent_id
                      where coa_type_id in (3,4) and a.company_id=".Auth::user()->company_id."
                      ORDER BY coa_no asc");
                }




          return $pasiva;
    }


    public function getLabarugi($type,$branch)
    {
      $idbranch = $branch;

          if($branch=='konsol') {

            $arrBranch = DB::table('master_branch')
                              ->select('id')
                              ->where('is_ho', 'f')
                              ->where('company_id', Auth::user()->company_id)
                              ->pluck('id')
                              ->toArray();

            $arrBranch = join(',', $arrBranch);

            $laba_rugi = \DB::select("SELECT
                                            coa_no,
                                            coa_name,
                                            balance_type_id,
                                            coa_type_id,
                                            CASE
                                              WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                            END AS last_os
                                      FROM (
                                              SELECT coa_no,
                                                      coa_name,
                                                      balance_type_id,
                                                      coa_type_id,
                                                      sum(last_os) AS last_os
                                              FROM
                                              master_coa
                                              WHERE coa_type_id IN (3,4) AND
                                                    is_active = true AND
                                                    company_id = " . Auth::user()->company_id . "   AND
                                                    branch_id IN (" . $arrBranch . ") AND
                                                    LEFT(coa_no, 3) != '600'
                                              GROUP BY coa_no,
                                                  coa_name,
                                                  balance_type_id,
                                                  coa_type_id
                                              ORDER BY coa_no ASC
                                            ) a LEFT JOIN (
                                                              SELECT
                                                                coa_parent_id,
                                                                sum(coalesce(last_os, 0)) AS nom
                                                              FROM
                                                                master_coa
                                                              WHERE
                                                                coa_parent_id IS NOT NULL AND
                                                                is_active = true AND
                                                                company_id = " . Auth::user()->company_id . " AND
                                                                branch_id IN (" . $arrBranch . ")
                                                              GROUP BY coa_parent_id
                                                          ) b ON a.coa_no = b.coa_parent_id
                                      ORDER BY coa_no ASC");

          }else{

            $laba_rugi = \DB::select("SELECT
                                            coa_no,
                                            coa_name,
                                            balance_type_id,
                                            coa_type_id,
                                            CASE
                                              WHEN a.coa_no = b.coa_parent_id THEN b.nom ELSE coalesce(a.last_os, 0)
                                            END AS last_os
                                      FROM (
                                              SELECT coa_no,
                                                      coa_name,
                                                      balance_type_id,
                                                      coa_type_id,
                                                      sum(last_os) AS last_os
                                              FROM
                                              master_coa
                                              WHERE coa_type_id IN (3,4) AND
                                                    is_active = true AND
                                                    company_id = " . Auth::user()->company_id . "   AND
                                                    branch_id IN (" . $idbranch . ") AND
                                                    LEFT(coa_no, 3) != '600'
                                              GROUP BY coa_no,
                                                  coa_name,
                                                  balance_type_id,
                                                  coa_type_id
                                              ORDER BY coa_no ASC
                                            ) a LEFT JOIN (
                                                              SELECT
                                                                coa_parent_id,
                                                                sum(coalesce(last_os, 0)) AS nom
                                                              FROM
                                                                master_coa
                                                              WHERE
                                                                coa_parent_id IS NOT NULL AND
                                                                is_active = true AND
                                                                company_id = " . Auth::user()->company_id . " AND
                                                                branch_id IN (" . $idbranch . ")
                                                              GROUP BY coa_parent_id
                                                          ) b ON a.coa_no = b.coa_parent_id
                                      ORDER BY coa_no ASC");
          }

          return $laba_rugi;
    }

    public function getBalanceSheet($type,$branch)
    {
       // $idbranch = 'konsol';
       $idbranch = $branch;

        if($branch=='konsol'){

          $arrBranch = DB::table('master_branch')
                            ->select('id')
                            ->where('is_ho', 'f')
                            ->where('company_id', Auth::user()->company_id)
                            ->pluck('id')
                            ->toArray();

          $arrBranch = join(',', $arrBranch);

          // Aktiva Paling Baru
          $aktiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . $arrBranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . $arrBranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 1
                                        and company_id = " . Auth::user()->company_id . "
                                        and branch_id in (" . $arrBranch . ")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (select coa_no, sum(last_os) as last_os from master_coa where
                                        coa_parent_id is null
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . $arrBranch . ")
                                        and is_active = true
                                        group by coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no, sum(last_os)as last_os, coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                          group by coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

          // Pasiva Paling Baru
          $pasiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os +
                                  (with recursive cte as (
                                select
                                  a.coa_no,
                                  a.coa_no as coa_parent_id,
                                  coalesce(a.last_os, 0) as last_os
                                from
                                  (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                union all
                                select
                                  c.coa_no,
                                  p.coa_no,
                                  p.last_os
                                from
                                  cte c
                                join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                    using (coa_parent_id) )
                                select
                                  sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                from
                                  cte
                                where
                                  coa_no in ('400','500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 2
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . $arrBranch .")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is null
                                          and company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no,
                                          sum(last_os)as last_os,
                                          coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $arrBranch . ")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

        }else{
          // BUKAN KONSOL

          // Aktiva Paling Baru
          $aktiva = \DB::SELECT("select
                                  coa_no,
                                  coa_name,
                                  sum(case when coa_no in ('301') then last_os + (with recursive cte as ( select coa_no, coa_no as coa_parent_id, coalesce(last_os, 0) as last_os from master_coa where coa_parent_id is null and company_id = " . Auth::user()->company_id . " and branch_id in (" . $idbranch . ") and is_active = true union all select c.coa_no, p.coa_no, p.last_os from cte c join (select * from master_coa where company_id = " . Auth::user()->company_id . " and branch_id in (" . $idbranch . ") and is_active = true) p using (coa_parent_id) ) select sum(case when coa_no in ('500', '600') then last_os*-1 else last_os end) as last_os from cte where coa_no in ('400', '500', '600')) else last_os end )as last_os
                                from
                                  (
                                  select
                                    a.coa_no,
                                    coa_name,
                                    d.coa_parent_id,
                                    case
                                      when a.coa_no = b.coa_parent_id then b.nom
                                      when a.coa_no = c.coa_no then c.last_os
                                      else coalesce(a.last_os, 0) end as last_os
                                    from
                                      (
                                      select
                                        coa_no,
                                        coa_name,
                                        sum(last_os) as last_os,
                                        coa_type_id
                                      from
                                        master_coa
                                      where
                                        coa_type_id = 1
                                        and company_id = " . Auth::user()->company_id . "
                                        and branch_id in (" . $idbranch . ")
                                        and is_active = true
                                      group by
                                        coa_no,
                                        coa_name,
                                        coa_type_id ) a
                                    left join (
                                      select
                                        coa_parent_id,
                                        sum(coalesce(last_os, 0)) as nom
                                      from
                                        master_coa
                                      where
                                        coa_parent_id is not null
                                        and length(coa_parent_id) = 6
                                      group by
                                        coa_parent_id ) b on
                                      a.coa_no = b.coa_parent_id
                                    left join (with recursive cte as (
                                      select
                                        a.coa_no,
                                        a.coa_no as coa_parent_id,
                                        coalesce(a.last_os, 0) as last_os
                                      from
                                        (select coa_no, sum(last_os) as last_os from master_coa where
                                        coa_parent_id is null
                                        and company_id = " . Auth::user()->company_id ."
                                        and branch_id in (" . $idbranch . ")
                                        and is_active = true
                                        group by coa_no) a
                                    union all
                                      select
                                        c.coa_no,
                                        p.coa_no,
                                        p.last_os
                                      from
                                        cte c
                                      join (
                                        select
                                          coa_no, sum(last_os)as last_os, coa_parent_id
                                        from
                                          master_coa
                                        where
                                          company_id = " . Auth::user()->company_id . "
                                          and branch_id in (" . $idbranch . ")
                                          and is_active = true
                                          group by coa_no,
                                          coa_parent_id ) p
                                          using (coa_parent_id) )
                                      select
                                        coa_no,
                                        sum(last_os) as last_os
                                      from
                                        cte
                                      group by
                                        1) c on
                                      a.coa_no = c.coa_no
                                    left join (
                                      select
                                        distinct coa_no,
                                        case
                                          when coa_parent_id isnull then coa_no
                                          else coa_parent_id end
                                        from
                                          master_coa) d on
                                      a.coa_no = d.coa_no
                                    order by
                                      d.coa_parent_id,
                                      a.coa_no asc)x
                                group by
                                  coa_no,
                                  coa_name ,
                                  coa_parent_id
                                order by
                                  coa_parent_id,
                                  coa_no");

          // Pasiva Paling Baru
          $pasiva = \DB::SELECT("select
                                    coa_no,
                                    coa_name,
                                    sum(case when coa_no in ('301') then last_os +
                                    (with recursive cte as (
                                  select
                                    a.coa_no,
                                    a.coa_no as coa_parent_id,
                                    coalesce(a.last_os, 0) as last_os
                                  from
                                    (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                  union all
                                  select
                                    c.coa_no,
                                    p.coa_no,
                                    p.last_os
                                  from
                                    cte c
                                  join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                      using (coa_parent_id) )
                                  select
                                    sum(case when coa_no in ('500','600') then last_os*-1 else last_os end) as last_os
                                  from
                                    cte
                                  where
                                    coa_no in ('400','500', '600')) else last_os end )as last_os
                                  from
                                    (
                                    select
                                      a.coa_no,
                                      coa_name,
                                      d.coa_parent_id,
                                      case
                                        when a.coa_no = b.coa_parent_id then b.nom
                                        when a.coa_no = c.coa_no then c.last_os
                                        else coalesce(a.last_os, 0) end as last_os
                                      from
                                        (
                                        select
                                          coa_no,
                                          coa_name,
                                          sum(last_os) as last_os,
                                          coa_type_id
                                        from
                                          master_coa
                                        where
                                          coa_type_id = 2
                                          and company_id = " . Auth::user()->company_id ."
                                          and branch_id in (" . $idbranch .")
                                          and is_active = true
                                        group by
                                          coa_no,
                                          coa_name,
                                          coa_type_id ) a
                                      left join (
                                        select
                                          coa_parent_id,
                                          sum(coalesce(last_os, 0)) as nom
                                        from
                                          master_coa
                                        where
                                          coa_parent_id is not null
                                          and length(coa_parent_id) = 6
                                        group by
                                          coa_parent_id ) b on
                                        a.coa_no = b.coa_parent_id
                                      left join (with recursive cte as (
                                        select
                                          a.coa_no,
                                          a.coa_no as coa_parent_id,
                                          coalesce(a.last_os, 0) as last_os
                                        from
                                          (
                                          select
                                            coa_no,
                                            sum(last_os) as last_os
                                          from
                                            master_coa
                                          where
                                            coa_parent_id is null
                                            and company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no) a
                                      union all
                                        select
                                          c.coa_no,
                                          p.coa_no,
                                          p.last_os
                                        from
                                          cte c
                                        join (
                                          select
                                            coa_no,
                                            sum(last_os)as last_os,
                                            coa_parent_id
                                          from
                                            master_coa
                                          where
                                            company_id = " . Auth::user()->company_id . "
                                            and branch_id in (" . $idbranch . ")
                                            and is_active = true
                                          group by
                                            coa_no,
                                            coa_parent_id ) p
                                            using (coa_parent_id) )
                                        select
                                          coa_no,
                                          sum(last_os) as last_os
                                        from
                                          cte
                                        group by
                                          1) c on
                                        a.coa_no = c.coa_no
                                      left join (
                                        select
                                          distinct coa_no,
                                          case
                                            when coa_parent_id isnull then coa_no
                                            else coa_parent_id end
                                          from
                                            master_coa) d on
                                        a.coa_no = d.coa_no
                                      order by
                                        d.coa_parent_id,
                                        a.coa_no asc)x
                                  group by
                                    coa_no,
                                    coa_name ,
                                    coa_parent_id
                                  order by
                                    coa_parent_id,
                                    coa_no");


        } // end if


        if ($type == 'aktiva') {
          return $aktiva;
        }
        elseif ($type == 'pasiva') {
          return $pasiva;
        }

    }

    public function renewal_list_report(Request $request)
    {
      $param['start_date'] = $request->get('start_date');
      $param['end_date'] = $request->get('end_date');
      if (Req::ajax()) {
          return view('master.only_content')->nest('child', 'report.renewal_list',$param);
      }else {
          return view('master.master')->nest('child', 'report.renewal_list',$param);
      }
    }

    
    public function renewal_list_due_date(Request $request)
    {

      $param['data'] = 'a'; 

      if (Req::ajax()) {
          return view('master.only_content')->nest('child', 'report.renewal_due_date',$param);
      }else {
          return view('master.master')->nest('child', 'report.renewal_due_date',$param);
      }
    }

    public function data_renewal_list_report(Request $request)
    {
      $date_start = date('Y-m-d', strtotime($request->input('start_date')));
      $date_end = date('Y-m-d', strtotime($request->input('end_date')));
      
      $data = DB::select("select ra.full_name, mc.full_name as nama_client, msp.police_name, 
          rp.definition, msp.start_date_polis, msp.end_date_polis, ru.definition as uw_def, msp.polis_no,
          rv.deskripsi, msp.ins_amount, msp.net_amount, '' as netto_brokerage, msp.invoice_type_id, msp.id as id_msp
        from master_sales_polis msp
        left join ref_agent ra on ra.id = msp.agent_id 
        left join master_customer mc on mc.id = msp.customer_id 
        left join ref_product rp on rp.id = msp.product_id
        left join ref_underwriter ru on ru.id = msp.underwriter_id 
        left join ref_valuta rv on rv.id = msp.valuta_id 
        where end_date_polis>=:date_start AND end_date_polis<=:date_end",['date_start' => $date_start, 'date_end' => $date_end]);      
      
      return json_encode(['data' => $data]);
    }

    public function data_renewal_list_due_date(Request $request)
    {
      $mc = collect(DB::select("SELECT due_date from master_company"))->first();     
    
      $data = DB::select("SELECT ra.full_name, mc.full_name as nama_client, msp.police_name, 
        rp.definition, msp.start_date_polis, msp.end_date_polis, ru.definition as uw_def, msp.polis_no,
        rv.deskripsi, msp.ins_amount, msp.net_amount, '' as netto_brokerage, msp.invoice_type_id, msp.id as id_msp,
      CURRENT_DATE + INTERVAL '".$mc->due_date." day' as hari_cuy	
      from master_sales_polis msp
      left join ref_agent ra on ra.id = msp.agent_id 
      left join master_customer mc on mc.id = msp.customer_id 
      left join ref_product rp on rp.id = msp.product_id
      left join ref_underwriter ru on ru.id = msp.underwriter_id 
      left join ref_valuta rv on rv.id = msp.valuta_id
      where msp.wf_status_id = 9 and msp.end_date_polis < CURRENT_DATE + INTERVAL '".$mc->due_date." day'");      
      
      return json_encode(['data' => $data]);
    }

    public function export_renewal_list_report(Request $request)
    {
        $ext = $request->get('type');        
        $param['date_start'] = date('Y-m-d', strtotime($request->get('start_date')));
        $param['date_end'] = date('Y-m-d', strtotime($request->get('end_date'))); 

        $param['data'] = json_decode($this->data_renewal_list_report($request));        

        if ( $ext == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_renewal_list_report', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Renewal List Report - ' . date('d/m/Y') . '.pdf');
        } else {
            // export to excel
            Excel::create('Laporan Renewal List Report - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                  $sheet->loadView('cetak.xlsx_renewal_list_report', $param);
                  $sheet->setColumnFormat(array('I' => '#0'));
                });
            })->export('xlsx');
        }
    }

    public function export_renewal_list_due_date(Request $request)
    {
        $ext = $request->get('type');        
       
        $param['data'] = json_decode($this->data_renewal_list_due_date($request));        

        if ( $ext == 'pdf' ) {
            $pdf = PDF::loadView('cetak.pdf_renewal_list_due_date', $param)->setPaper('a4', 'landscape');
            return $pdf->stream('Laporan Renewal List Due Date - ' . date('d/m/Y') . '.pdf');
        } else {
            // export to excel
            Excel::create('Laporan Renewal List Due Date - ' . date('d/m/Y') . '', function($excel) use ($param){
                $excel->sheet('Sheet 1',function($sheet) use ($param){
                    $sheet->loadView('cetak.xlsx_renewal_list_due_date', $param);
                });
            })->export('xlsx');
        }
    }
}
