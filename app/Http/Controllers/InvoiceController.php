<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;
use DateTime;

class InvoiceController extends Controller
{
    public function invoice_baru(Request $request)
    {
        $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where is_active=true');
        $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where is_active=true');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');

        $ref_underwriter = \DB::select('SELECT * FROM ref_underwriter where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_valuta = \DB::select('SELECT * FROM ref_valuta order by id asc');
        
        $param['master_sales_polis'] = \DB::select('SELECT a.id as id,mcv.cf_no 
        FROM master_sales_polis a
        join master_cf_vehicle mcv on mcv.id = a.coc_id  where invoice_type_id = 0  
        and a.id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null) and
        (a.wf_status_id = 9 and a.paid_status_id = 0) or 
        (a.wf_status_id = 13 and a.paid_status_id = 1) or
        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1) ');



        $param['ref_valuta']=$ref_valuta;
        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;
        $param['ref_underwriter']=$ref_underwriter;

        $param['type'] ='New';

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'invoice.form',$param);
        }else {
            return view('master.master')->nest('child', 'invoice.form',$param);
        }
    }

    public function edit_invoice(Request $request)
    {
        $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where is_active=true');
        $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where is_active=true');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');

        $ref_underwriter = \DB::select('SELECT * FROM ref_underwriter where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');

        //$data = \DB::select('SELECT * FROM master_sales where id='.$request->get('id'));
        $data = collect(\DB::select("SELECT master_sales.*,mcvvv.cf_no FROM master_sales 
        left join master_sales_polis msp on msp.id = master_sales.coc_id 
        left join master_cf_vehicle mcvvv on mcvvv.id = msp.coc_id 
        where master_sales.id=".$request->get('id')))->first();

        $ref_valuta = \DB::select('SELECT * FROM ref_valuta order by id asc');

        $param['master_sales_polis'] = \DB::select('SELECT a.coc_id as id,mcv.cf_no 
        FROM master_sales_polis a
        join master_cf_vehicle mcv on mcv.id = a.coc_id  where invoice_type_id = 0  
        and a.coc_id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null) and
        (a.wf_status_id = 9 and a.paid_status_id = 0) or 
        (a.wf_status_id = 13 and a.paid_status_id = 1) or
        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1) ');

        $param['ref_valuta']=$ref_valuta;

        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;
        $param['ref_underwriter']=$ref_underwriter;
        $param['data']=$data;

        $param['type'] ='Edit';

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'invoice.form',$param);
        }else {
            return view('master.master')->nest('child', 'invoice.form',$param);
        }
    }

    public function edit_endorse(Request $request)
    {
        $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where is_active=true');
        $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where is_active=true');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');

        $ref_underwriter = \DB::select('SELECT * FROM ref_underwriter where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');

        //$data = \DB::select('SELECT * FROM master_sales where id='.$request->get('id'));
        $data = collect(\DB::select("SELECT master_sales.*,mcvvv.cf_no FROM master_sales 
        left join master_cf_vehicle mcvvv on mcvvv.id = master_sales.coc_id where master_sales.id=".$request->get('id')))->first();

        $ref_valuta = \DB::select('SELECT * FROM ref_valuta order by id asc');

        $param['master_sales_polis'] = \DB::select('SELECT a.coc_id as id,mcv.cf_no 
        FROM master_sales_polis a
        join master_cf_vehicle mcv on mcv.id = a.coc_id  where invoice_type_id = 0  
        and a.coc_id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null) and
        (a.wf_status_id = 9 and a.paid_status_id = 0) or 
        (a.wf_status_id = 13 and a.paid_status_id = 1) or
        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1) ');

        $param['ref_valuta']=$ref_valuta;

        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;
        $param['ref_underwriter']=$ref_underwriter;
        $param['data']=$data;

        $param['type'] ='Edit';

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'invoice.endorse',$param);
        }else {
            return view('master.master')->nest('child', 'invoice.endorse',$param);
        }
    }

    public function invoice_list(Request $request)
    {
        $data = \DB::select("SELECT a.product_id as coc_product_id,a.coc_id,a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
            left join master_sales_polis msp on msp.id = a.coc_id
                        left join ref_valuta f on f.id=a.valuta_id where a.invoice_type_id in (0,1) and a.wf_status_id in (1,3) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." order by a.id desc");

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.list',$param);

    }

    public function invoice_approval(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
                        left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1) and a.wf_status_id=2 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;
        return view('master.master')->nest('child', 'invoice.approval',$param);

    }
    public function invoice_approval_payment(Request $request)
    {

        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
                        left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1) and a.wf_status_id=5 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.approval_payment',$param);

    }
    public function invoice_paid(Request $request)
    {
        $data = \DB::select("select d1,d2,d3,materai_amount,polis_amount,disc_amount,agent_fee_amount,comp_fee_amount,admin_amount,ins_fee,a.id,b.full_name,c.definition,a.premi_amount,
        d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,
        a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,
        a.valuta_id from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
                        left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1) and a.wf_status_id in (9,16,17,18) and paid_status_id=1 and is_overdue=false and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.invoice_paid',$param);

    }
    public function invoice_dropped(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
                        left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1) and a.wf_status_id=10 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.invoice_dropped',$param);

    }


    public function get_data_payment(Request $request){

        $getid = $request->input('id');
        $getKurs = $request->input('kurs');


        $f = '';

        if($request->input('type') == 'selected'){
            if ($getid) {
                $datas = implode(",", $getid);
                $f = 'and a.id in ('.$datas.')';
            }
        }

        if($getKurs == ''){
            // $data['valuta_id'] =

            $data = \DB::select("select
            sum(agent_fee_amount) as agent_fee_amount,
            sum(comp_fee_amount) as comp_fee_amount,
            sum(a.premi_amount) as premi_amount,
            sum(polis_amount) as polis_amount,
            sum(materai_amount) as materai_amount,
            sum(admin_amount) as admin_amount,
            sum(disc_amount) as disc_amount,
            sum(net_amount) as net_amount,
            valuta_id,mata_uang,kurs_today
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
            left join ref_valuta f on f.id=a.valuta_id
            where invoice_type_id in (0,1) and a.wf_status_id=9
            and paid_status_id=0 ".$f." and a.branch_id=".Auth::user()->branch_id."
            and a.company_id=".Auth::user()->company_id." GROUP BY valuta_id,mata_uang,kurs_today");

        }else{
            $data = \DB::select("
            select
                sum(case when valuta_id in ('1') then agent_fee_amount else (agent_fee_amount/kurs_today)*".$getKurs." end) as agent_fee_amount,
                sum(case when valuta_id in ('1') then comp_fee_amount else (comp_fee_amount/kurs_today)*".$getKurs." end) as comp_fee_amount,
				sum(case when valuta_id in ('1') then a.premi_amount else (a.premi_amount/kurs_today)*".$getKurs." end) as premi_amount,
                sum(case when valuta_id in ('1') then polis_amount else (polis_amount/kurs_today)*".$getKurs." end) as polis_amount,
                sum(case when valuta_id in ('1') then materai_amount else (materai_amount/kurs_today)*".$getKurs." end ) as materai_amount,
                sum(case when valuta_id in ('1') then admin_amount else (admin_amount/kurs_today)*".$getKurs." end) as admin_amount,
                sum(case when valuta_id in ('1') then disc_amount else (disc_amount/kurs_today)*".$getKurs." end) as disc_amount,
                sum(case when valuta_id in ('1') then net_amount else (net_amount/kurs_today)*".$getKurs." end) as net_amount,
                valuta_id,mata_uang,kurs_today
                from master_sales a
                left join master_customer b on b.id=a.customer_id
                left join ref_product c on c.id=a.product_id
                left join ref_paid_status d on d.id=a.paid_status_id
                left join master_branch e on e.id=a.branch_id
                left join ref_valuta f on f.id=a.valuta_id where invoice_type_id in (0,1)
                and a.wf_status_id=9 and paid_status_id=0 ".$f." and a.branch_id=".Auth::user()->branch_id."
                 and a.company_id=".Auth::user()->company_id." GROUP BY valuta_id,mata_uang,kurs_today");
        }





        return json_encode($data);

    }

    public function invoice_active(Request $request)
    {
        $data = \DB::select("SELECT msp.product_id as coc_product_id,a.coc_id,a.comp_fee_amount,a.agent_fee_amount,a.disc_amount,a.polis_amount,a.materai_amount,a.admin_amount,a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id 
        from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
            left join master_sales_polis msp on msp.id = a.coc_id
                        left join ref_valuta f on f.id=a.valuta_id where a.invoice_type_id in (0,1) and a.wf_status_id=9 and a.paid_status_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;


        return view('master.master')->nest('child', 'invoice.invoice_active',$param);

    }

    public function insert_invoice_baru(Request $request){

        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();
        $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();

        $today=date('Y-m-d');
        if($systemDate->current_date==$today){

            $results = \DB::select("SELECT max(id) AS id from master_sales");
            $id='';
            if($results){
                $id=$results[0]->id + 1;
            }else{
                $id=1;
            }

            $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where id='.$request->input('quotation'));
            $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where id='.$request->input('proposal'));
            $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

            $ef_config = \DB::select("select value  as jml from master_config where id = 10");
            $ef_agent_config = \DB::select("select value  as jml from master_config where id = 11");
            $ef_company_config = \DB::select("select value  as jml from master_config where id = 12");

            $jml=str_replace('.', '', $request->input('premi'));


            $ef=$ef_config[0]->jml * str_replace(',', '.', $jml) / 100;
            $ef_agent=$ef_agent_config[0]->jml * $ef / 100;
            $ef_company=$ef_company_config[0]->jml * $ef / 100;

            $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

            $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();


            $str=strlen($ref_invoice->next_no);
            if($str==3){

            $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;

            }elseif($str==2){
            $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }else{
                $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }

            DB::table('ref_invoice_no')
                ->where('company_id', Auth::user()->company_id)
                ->where('year', date('Y'))
                ->where('type',0)
                ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

            $tglmulai=date('Y-m-d',strtotime($request->input('tgl_mulai')));
            $tglakhir = date('Y-m-d', strtotime('+'.$request->input('tgl_akhir').' days', strtotime($request->input('tgl_mulai'))));

            $startDatePolice = date('Y-m-d',strtotime($request->input('start_date_police')));
            $endDatePolice = date('Y-m-d',strtotime($request->input('end_date_police')));
            $valutaID = $request->input('valuta_id');

            // if ( is_null($request->input('insurance')) ) {
            //     $nilai = $this->clearSeparatorDouble($request->input('total_sum_insured'));
            // } else {
                $nilai = $this->clearSeparatorDouble($request->input('insurance'));
            // }


            if ( is_null($request->input('valas_amount')) ) {
                $valasAmount = 0;
            } else {
                $valasAmount = $this->clearSeparatorDouble($request->input('valas_amount'));
            }

         if ( is_null($request->input('today_kurs')) ) {
                $nilai12  = 1;
            } else {
                $nilai12 = $this->clearSeparatorDouble($request->input('today_kurs'));
                //$nilai12 = str_replace(',', '.', $nilai12);
            }

            if ( is_null($request->input('ef_pct')) ) {
                $ef_pct = 0;
            } else {
                $ef_pct = $this->clearSeparatorDouble($request->input('ef_pct'));
            }

        $filename = '';

        if($request->file('file')){
            $destination_path = public_path('upload_invoice');
            $files = $request->file('file');
            $filename = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $id."_".$filename);
        }

        $get_id = $request->input('get_id');


        $setField = [
            'inv_date' => $systemDate->current_date,
            'polis_no' => $request->input('no_polis'),
            'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
            'product_id' => $request->input('product'),
            'agent_id' => $request->input('officer'),
            'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
            'end_date' => $tglakhir,
            'afi_acc_no' => $request->input('bank'),
            'segment_id' => $request->input('segment'),
            'is_tax_company' => $request->input('is_tax_company'),
            'paid_status_id' => 0,
            'qs_no' => $request->input('quotation'),
            'customer_id' => $request->input('customer'),
            'qs_date' => $ref_quotation[0]->from_date,
            'proposal_no' => $request->input('proposal'),
            'underwriter_id' => $request->input('id_underwriter'),
            'invoice_type_id' => $request->input('invoice_type_id'),
            'proposal_date' => $ref_proposal[0]->from_date,
            'is_active' => 1,
            'wf_status_id' => 1,
            'user_crt_id' => Auth::user()->id,
            'branch_id' => Auth::user()->branch_id,
            'company_id' => Auth::user()->company_id,
            'created_at' => date('Y-m-d H:i:s'),
            'ef' => $ef,
            'ef_agent' => $ef_agent,
            'ef_company' => $ef_company,
            'url_dokumen' => $filename,
            'start_date_polis' => $startDatePolice,
            'end_date_polis' => $endDatePolice,
            'valuta_id' => $valutaID,
            'no_of_insured' => $request->input('no_of_insured'),
            'valuta_amount' => $valasAmount*$nilai12,
            'ins_amount' => $nilai*$nilai12,
            'disc_amount' => $this->clearSeparatorDouble($request->input('disc_amount'))*$nilai12,
            'net_amount' => $this->clearSeparatorDouble($request->input('nett_amount'))*$nilai12,
            'agent_fee_amount' => $this->clearSeparatorDouble($request->input('fee_agent'))*$nilai12,
            'comp_fee_amount' => $this->clearSeparatorDouble($request->input('fee_internal'))*$nilai12,
            'premi_amount' => $this->clearSeparatorDouble($request->input('premi'))*$nilai12,
            'polis_amount' => $this->clearSeparatorDouble($request->input('fee_polis'))*$nilai12,
            'materai_amount' => $this->clearSeparatorDouble($request->input('fee_materai'))*$nilai12,
            'admin_amount' => $this->clearSeparatorDouble($request->input('fee_admin'))*$nilai12,
            'tax_amount' => $this->clearSeparatorDouble($request->input('tax_amount'))*$nilai12,
            'ins_fee' => $this->clearSeparatorDouble($request->input('asuransi'))*$nilai12,
            'ef' => $this->clearSeparatorDouble($request->input('ef'))*$nilai12,
            'ef_pct' => $ef_pct*$nilai12,
            'kurs_today' =>  $nilai12,
            'pph' => $this->clearSeparatorDouble($request->input('pph'))*$nilai12
        ];

        $setField['coc_id'] = $request->input('coc_no');
        $setField['police_name'] = $request->input('policy_cust_name');

         //IF EDIT DATA
         if ($get_id) {
            DB::table('master_sales')->where('id', $get_id)->update($setField);
        //IF ADD DATA
        }else {
            $seq = DB::table('master_sales')->max('id');
            $setField['id'] = $seq+1;
            DB::table('master_sales')->insert($setField);
        }

          return json_encode(['rc'=>1,'rm'=>'Success']);
        }else{

            return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
        }



    }

    public function insert_invoice_baru_endorsement(Request $request)
    {
        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();
        $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();

        $today=date('Y-m-d');
        if($systemDate->current_date==$today){

            $results = \DB::select("SELECT max(id) AS id from master_sales");
            $id='';
            if($results){
                $id=$results[0]->id + 1;
            }else{
                $id=1;
            }

            $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where id='.$request->input('quotation'));
            $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where id='.$request->input('proposal'));
            $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

            $ef_config = \DB::select("select value  as jml from master_config where id = 10");
            $ef_agent_config = \DB::select("select value  as jml from master_config where id = 11");
            $ef_company_config = \DB::select("select value  as jml from master_config where id = 12");

            $jml=str_replace('.', '', $request->input('premi'));


            $ef=$ef_config[0]->jml * str_replace(',', '.', $jml) / 100;
            $ef_agent=$ef_agent_config[0]->jml * $ef / 100;
            $ef_company=$ef_company_config[0]->jml * $ef / 100;

            $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

            $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();


            $str=strlen($ref_invoice->next_no);
            if($str==3){

            $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;

            }elseif($str==2){
            $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }else{
                $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }

            DB::table('ref_invoice_no')
                ->where('company_id', Auth::user()->company_id)
                ->where('year', date('Y'))
                ->where('type',0)
                ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

            $tglmulai=date('Y-m-d',strtotime($request->input('tgl_mulai')));
            $tglakhir = date('Y-m-d', strtotime('+'.$request->input('tgl_akhir').' days', strtotime($request->input('tgl_mulai'))));

            $startDatePolice = date('Y-m-d',strtotime($request->input('start_date_Policy')));
            $endDatePolice = date('Y-m-d',strtotime($request->input('end_date_Policy')));
            $valutaID = $request->input('valuta_id');

            // if ( is_null($request->input('insurance')) ) {
            //     $nilai = $this->clearSeparatorDouble($request->input('total_sum_insured'));
            // } else {
                $nilai = $this->clearSeparatorDouble($request->input('insurance'));
            // }


            if ( is_null($request->input('valas_amount')) ) {
                $valasAmount = 0;
            } else {
                $valasAmount = $this->clearSeparatorDouble($request->input('valas_amount'));
            }

         if ( is_null($request->input('today_kurs')) ) {
                $nilai12  = 1;
            } else {
                $nilai12 = $this->clearSeparatorDouble($request->input('today_kurs'));
                //$nilai12 = str_replace(',', '.', $nilai12);
            }

        $filename = '';

        if($request->file('file')){
            $destination_path = public_path('upload_invoice');
            $files = $request->file('file');
            $filename = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path, $id."_".$filename);
        }

        $get_id = $request->input('get_id');


        $setField = [
            'inv_date' => $systemDate->current_date,
            'polis_no' => $request->input('no_polis'),
            'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
            'product_id' => $request->input('product'),
            'agent_id' => $request->input('officer'),
            'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
            'end_date' => $tglakhir,
            'afi_acc_no' => $request->input('bank'),
            'segment_id' => $request->input('segment'),
            'is_tax_company' => $request->input('is_tax_company'),
            'paid_status_id' => 0,
            'qs_no' => $request->input('quotation'),
            'customer_id' => $request->input('customer'),
            'qs_date' => $ref_quotation[0]->from_date,
            'proposal_no' => $request->input('proposal'),
            'underwriter_id' => $request->input('id_underwriter'),
            'invoice_type_id' => $request->input('invoice_type_id'),
            'proposal_date' => $ref_proposal[0]->from_date,
            'is_active' => 1,
            'wf_status_id' => 1,
            'user_crt_id' => Auth::user()->id,
            'branch_id' => Auth::user()->branch_id,
            'company_id' => Auth::user()->company_id,
            'created_at' => date('Y-m-d H:i:s'),
            'ef' => $ef,
            'ef_agent' => $ef_agent,
            'ef_company' => $ef_company,
            'url_dokumen' => $filename,
            'start_date_polis' => $startDatePolice,
            'end_date_polis' => $endDatePolice,
            'valuta_id' => $valutaID,
            'no_of_insured' => $request->input('no_of_insured'),
            'valuta_amount' => $valasAmount*$nilai12,
            'ins_amount' => $nilai*$nilai12,
            'disc_amount' => $this->clearSeparatorDouble($request->input('disc_amount'))*$nilai12,
            'net_amount' => $this->clearSeparatorDouble($request->input('nett_amount'))*$nilai12,
            'agent_fee_amount' => $this->clearSeparatorDouble($request->input('fee_agent'))*$nilai12,
            'comp_fee_amount' => $this->clearSeparatorDouble($request->input('fee_internal'))*$nilai12,
            'premi_amount' => $this->clearSeparatorDouble($request->input('premi'))*$nilai12,
            'polis_amount' => $this->clearSeparatorDouble($request->input('fee_polis'))*$nilai12,
            'materai_amount' => $this->clearSeparatorDouble($request->input('fee_materai'))*$nilai12,
            'admin_amount' => $this->clearSeparatorDouble($request->input('fee_admin'))*$nilai12,
            'tax_amount' => $this->clearSeparatorDouble($request->input('tax_amount'))*$nilai12,
            'ins_fee' => $this->clearSeparatorDouble($request->input('asuransi'))*$nilai12,

            'kurs_today' =>  $nilai12,
        ];

        $setField['coc_id'] = $request->input('coc_no');
        $setField['police_name'] = $request->input('policy_cust_name');
        $setField['insured_name'] = $request->input('insured_name');
        $setField['endors_reason'] = $request->input('endorse_reason');

         //IF EDIT DATA
         if ($get_id) {
            DB::table('master_sales')->where('id', $get_id)->update($setField);
        //IF ADD DATA
        }else {
            $seq = DB::table('master_sales')->max('id');
            $setField['id'] = $seq+1;
            DB::table('master_sales')->insert($setField);
        }

            return json_encode(['rc'=>1,'rm'=>'Success']);

        }else{

            return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
        }

    }


    public function insert_invoice_baru_installment(Request $request)
    {
        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();
        $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();
        $today = date('Y-m-d');

        if ( $systemDate->current_date == $today ) {
            $results = \DB::select("SELECT max(id) AS id from master_sales");
            $id = '';

            if( $results ) {
                $id = $results[0]->id + 1;
            }else{
                $id = 1;
            }

            $ref_quotation = \DB::select('SELECT * FROM ref_quotation where id='.$request->input('quotation'));
            $ref_proposal = \DB::select('SELECT * FROM ref_proposal where id='.$request->input('proposal'));
            $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

            $ef_config = \DB::select("select value  as jml from master_config where id = 10");
            $ef_agent_config = \DB::select("select value  as jml from master_config where id = 11");
            $ef_company_config = \DB::select("select value  as jml from master_config where id = 12");

            $jml = str_replace('.', '', $request->input('premi'));


            $ef=$ef_config[0]->jml * str_replace(',', '.', $jml) / 100;
            $ef_agent=$ef_agent_config[0]->jml * $ef / 100;
            $ef_company=$ef_company_config[0]->jml * $ef / 100;

            $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

            $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();

            $str=strlen($ref_invoice->next_no);
            // if ( $str == 3 ){

            //     $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;

            // } elseif ( $str == 2 ) {
            //     $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            // }else{
            //     $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            // }

            // DB::table('ref_invoice_no')
            //     ->where('company_id', Auth::user()->company_id)
            //     ->where('year', date('Y'))
            //     ->where('type',0)
            //     ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

            $refInvoiceNo = DB::table('ref_invoice_no')->select('last_no', 'next_no')
                                                        ->where('company_id', Auth::user()->company_id)
                                                       ->where('year', date('Y'))
                                                       ->where('type',0)->first();

             DB::table('ref_invoice_no')
                ->where('company_id', Auth::user()->company_id)
                ->where('year', date('Y'))
                ->where('type',0)
                ->update(['last_no' =>$refInvoiceNo->next_no, 'next_no'=>$refInvoiceNo->next_no + 1]);

            if ( $str == 3 ){
                $no_invoice='MKT/II/'. $refInvoiceNo->next_no .'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            } elseif ( $str == 2 ) {
                $no_invoice='MKT/II/0'. $refInvoiceNo->next_no .'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }else{
                $no_invoice='MKT/II/00'. $refInvoiceNo->next_no .'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
            }

            $tglmulai=date('Y-m-d',strtotime($request->input('tgl_mulai')));
            $tglakhir = date('Y-m-d', strtotime('+'.$request->input('tgl_akhir').' days', strtotime($request->input('tgl_mulai'))));

            $noEndors = DB::table('master_sales')->max('endors_no');

            if ( empty($noEndors) ) {
                $noEndors = 1;
            } else {
                $noEndors += 1;
            }

            $nilai=str_replace('.', '', $request->input('insurance'));
            $nilai1=str_replace('.', '', $request->input('disc_amount'));
            $nilai2=str_replace('.', '', $request->input('nett_amount'));
            $nilai3=str_replace('.', '', $request->input('fee_agent'));
            $nilai4=str_replace('.', '', $request->input('fee_internal'));
            $nilai5=str_replace('.', '', $request->input('premi'));
            $nilai6=str_replace('.', '', $request->input('fee_polis'));
            $nilai7=str_replace('.', '', $request->input('fee_materai'));
            $nilai8=str_replace('.', '', $request->input('fee_admin'));
            $nilai9=str_replace('.', '', $request->input('tax_amount'));
            $nilai10=str_replace('.', '', $request->input('asuransi'));
            $nilai11=str_replace('.', '', $request->input('no_of_insured'));

            if ( $request->file('file') ) {
                // insert with file
                $destination_path = public_path('upload_invoice');
                $files = $request->file('file');
                $filename = $files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $id."_".$filename);

                DB::table('master_sales')->insert(
                    [
                        'id' => $id,
                        'inv_date' => $systemDate->current_date,
                        'polis_no' => $request->input('no_polis'),
                        // 'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
                        'inv_no' => $no_invoice,
                        'product_id' => $request->input('product'),
                        'ins_amount' => str_replace(',', '.', $nilai),
                        'disc_amount' => str_replace(',', '.', $nilai1),
                        'invoice_type_id' => 2,
                        'endors_no' => $noEndors,
                        'net_amount' => str_replace(',', '.', $nilai2),
                        'agent_id' => $request->input('officer'),
                        'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                        'end_date' => $tglakhir,
                        'afi_acc_no' => $request->input('bank'),
                        'segment_id' => $request->input('segment'),
                        'agent_fee_amount' => str_replace(',', '.', $nilai3),
                        'comp_fee_amount' => str_replace(',', '.', $nilai4),
                        'premi_amount' => str_replace(',', '.', $nilai5),
                        'polis_amount' => str_replace(',', '.', $nilai6),
                        'materai_amount' => str_replace(',', '.', $nilai7),
                        'admin_amount' => str_replace(',', '.', $nilai8),
                        'tax_amount' => str_replace(',', '.', $nilai9),
                        'is_tax_company' => $request->input('is_tax_company'),
                        //'due_date_jrn' => $request->input('email'),
                        'paid_status_id' => 0,
                        'qs_no' => $request->input('quotation'),
                        'customer_id' => $request->input('customer'),
                        'qs_date' => $ref_quotation[0]->qs_date,
                        'proposal_no' => $request->input('proposal'),
                        'underwriter_id' => $request->input('id_underwriter'),
                        'invoice_type_id' => $request->input('invoice_type_id'),
                        'proposal_date' => $ref_proposal[0]->qs_date,
                        'is_active' => 1,
                        'wf_status_id' => 1,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        //'coverage_amount' => Auth::user()->branch_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'ins_fee' => str_replace(',', '.', $nilai10),
                        'no_of_insured' => str_replace(',', '.', $nilai11),
                        'ef' => $ef,
                        'ef_agent' => $ef_agent,
                        'ef_company' => $ef_company,
                        'url_dokumen' => $filename,
                    ]
                );
            } else {
                // insert without file
                DB::table('master_sales')->insert(
                    [
                        'id' => $id,
                        'inv_date' => $systemDate->current_date,
                        'polis_no' => $request->input('no_polis'),
                        // 'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
                        'inv_no' => $no_invoice,
                        'product_id' => $request->input('product'),
                        'ins_amount' => str_replace(',', '.', $nilai),
                        'disc_amount' => str_replace(',', '.', $nilai1),
                        'invoice_type_id' => 2,
                        'endors_no' => $noEndors,
                        'net_amount' => str_replace(',', '.', $nilai2),
                        'agent_id' => $request->input('officer'),
                        'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                        'end_date' => $tglakhir,
                        'afi_acc_no' => $request->input('bank'),
                        'segment_id' => $request->input('segment'),
                        'agent_fee_amount' => str_replace(',', '.', $nilai3),
                        'comp_fee_amount' => str_replace(',', '.', $nilai4),
                        'premi_amount' => str_replace(',', '.', $nilai5),
                        'polis_amount' => str_replace(',', '.', $nilai6),
                        'materai_amount' => str_replace(',', '.', $nilai7),
                        'admin_amount' => str_replace(',', '.', $nilai8),
                        'tax_amount' => str_replace(',', '.', $nilai9),
                        'is_tax_company' => $request->input('is_tax_company'),
                        //'due_date_jrn' => $request->input('email'),
                        'paid_status_id' => 0,
                        'qs_no' => $request->input('quotation'),
                        'customer_id' => $request->input('customer'),
                        'qs_date' => $ref_quotation[0]->qs_date,
                        'proposal_no' => $request->input('proposal'),
                        'underwriter_id' => $request->input('id_underwriter'),
                        'invoice_type_id' => $request->input('invoice_type_id'),
                        'proposal_date' => $ref_proposal[0]->qs_date,
                        'is_active' => 1,
                        'wf_status_id' => 1,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        //'coverage_amount' => Auth::user()->branch_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'ins_fee' => str_replace(',', '.', $nilai10),
                        'no_of_insured' => str_replace(',', '.', $nilai11),
                        'ef' => $ef,
                        'ef_agent' => $ef_agent,
                        'ef_company' => $ef_company,
                    ]
                );
            } // end if

            return json_encode(['rc'=>1,'rm'=>'Success']);

        }else{

            return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
        }

        // $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();

        // if($cek_branch->is_open=='true'){
        // $results = \DB::select("SELECT max(id) AS id from master_sales");
        // $id='';
        // if($results){
        //     $id=$results[0]->id + 1;
        // }else{
        //     $id=1;
        // }

        // $ref_quotation = \DB::select('SELECT * FROM ref_quotation where id='.$request->input('quotation'));
        // $ref_proposal = \DB::select('SELECT * FROM ref_proposal where id='.$request->input('proposal'));
        // $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

        // $ef_config = \DB::select("select value  as jml from master_config where id = 10");
        // $ef_agent_config = \DB::select("select value  as jml from master_config where id = 11");
        // $ef_company_config = \DB::select("select value  as jml from master_config where id = 12");

        // $ef=$ef_config[0]->jml * str_replace('.', '', $request->input('premi')) / 100;
        // $ef_agent=$ef_agent_config[0]->jml * $ef / 100;
        // $ef_company=$ef_company_config[0]->jml * $ef / 100;

        // $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

        // $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();


        // $str=strlen($ref_invoice->next_no);
        // if($str==3){

        //    $no_invoice='MKT/'.$d_date->romawi_no.'/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;

        // }elseif($str==2){
        //    $no_invoice='MKT/'.$d_date->romawi_no.'/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
        // }else{
        //     $no_invoice='MKT/'.$d_date->romawi_no.'/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
        // }

        // DB::table('ref_invoice_no')
        //     ->where('company_id', Auth::user()->company_id)
        //     ->where('year', date('Y'))
        //     ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

        // $tglmulai=date('Y-m-d',strtotime($request->input('tgl_mulai')));
        // $tglakhir = date('Y-m-d', strtotime('+'.$request->input('tgl_akhir').' days', strtotime($request->input('tgl_mulai'))));

        // $noEndors = DB::table('master_sales')->max('endors_no');

        // if ( empty($noEndors) ) {
        //     $noEndors = 1;
        // } else {
        //     $noEndors += 1;
        // }

        // if ( is_null($request->input('no_of_insured')) ) {
        //     $noOfInsured = null;
        // } else {
        //     $noOfInsured = str_replace('.', '', $request->input('no_of_insured'));
        // }


        // DB::table('master_sales')->insert(
        //         [
        //             'id' => $id,
        //             'inv_date' => $systemDate->current_date,
        //             'polis_no' => $request->input('no_polis'),
        //             'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
        //             'product_id' => $request->input('product'),
        //             'ins_amount' => str_replace('.', '', $request->input('insurance')),
        //             'disc_amount' => str_replace('.', '', $request->input('disc_amount')),
        //             'invoice_type_id' => 2,
        //             'endors_no' => $noEndors,

        //             'net_amount' => str_replace('.', '', $request->input('nett_amount')),
        //             'agent_id' => $request->input('officer'),

        //             'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
        //             'end_date' => $tglakhir,
        //             'afi_acc_no' => $request->input('bank'),
        //             'segment_id' => $request->input('segment'),
        //             'agent_fee_amount' => str_replace('.', '', $request->input('fee_agent')),

        //             'comp_fee_amount' => str_replace('.', '', $request->input('fee_internal')),
        //             'premi_amount' => str_replace('.', '', $request->input('premi')),

        //             'polis_amount' => str_replace('.', '', $request->input('fee_polis')),
        //             'materai_amount' => str_replace('.', '', $request->input('fee_materai')),
        //             'admin_amount' => str_replace('.', '', $request->input('fee_admin')),
        //             'tax_amount' => str_replace('.', '', $request->input('tax_amount')),
        //             'is_tax_company' => $request->input('is_tax_company'),
        //             //'due_date_jrn' => $request->input('email'),
        //             'paid_status_id' => 0,
        //             'qs_no' => $request->input('quotation'),
        //             'customer_id' => $request->input('customer'),
        //             'qs_date' => $ref_quotation[0]->qs_date,
        //             'proposal_no' => $request->input('proposal'),

        //             'underwriter_id' => $request->input('id_underwriter'),

        //             'invoice_type_id' => $request->input('invoice_type_id'),

        //             'proposal_date' => $ref_proposal[0]->qs_date,
        //             'is_active' => 1,
        //             'wf_status_id' => 1,
        //             'user_crt_id' => Auth::user()->id,
        //             'branch_id' => Auth::user()->branch_id,
        //             'company_id' => Auth::user()->company_id,
        //             //'coverage_amount' => Auth::user()->branch_id,
        //             'created_at' => date('Y-m-d H:i:s'),
        //             'ins_fee' => str_replace('.', '', $request->input('asuransi')),
        //             'no_of_insured' => $noOfInsured,
        //             'ef' => $ef,
        //             'ef_agent' => $ef_agent,
        //             'ef_company' => $ef_company,
        //         ]
        //     );
        //     return json_encode(['rc'=>1,'rm'=>'Success']);
        // }else{
        //     return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
        // }

    }

    public function update_invoice_baru(Request $request){
        $inv_type = $request->input('invoice_type_id');
        $ref_quotation = \DB::select('SELECT * FROM ref_quotation where id='.$request->input('quotation'));
        $ref_proposal = \DB::select('SELECT * FROM ref_proposal where id='.$request->input('proposal'));

        $startDatePolice = date('Y-m-d',strtotime($request->input('start_date_police')));
            $endDatePolice = date('Y-m-d',strtotime($request->input('end_date_police')));
            $valutaID = $request->input('valuta_id');

            if ( is_null($request->input('insurance')) ) {
                $nilai = str_replace('.', '', $request->input('total_sum_insured'));
                $nilai = str_replace(',', '.', $nilai);
            } else {
                $nilai = str_replace('.', '', $request->input('insurance'));
                $nilai = str_replace(',', '.', $nilai);
            }


            if ( is_null($request->input('valas_amount')) ) {
                $valasAmount = NULL;
            } else {
                $valasAmount = str_replace('.', '', $request->input('valas_amount'));
            }



        //'inv_date' => date('Y-m-d',strtotime($request->input('tgl_tr'))),
       // $nilai=str_replace('.', '', $request->input('insurance'));
        $nilai1=str_replace('.', '', $request->input('disc_amount'));
        $nilai2=str_replace('.', '', $request->input('nett_amount'));
        $nilai3=str_replace('.', '', $request->input('fee_agent'));
        $nilai4=str_replace('.', '', $request->input('fee_internal'));
        $nilai5=str_replace('.', '', $request->input('premi'));
        $nilai6=str_replace('.', '', $request->input('fee_polis'));
        $nilai7=str_replace('.', '', $request->input('fee_materai'));
        $nilai8=str_replace('.', '', $request->input('fee_admin'));
        $nilai9=str_replace('.', '', $request->input('tax_amount'));
        $nilai10=str_replace('.', '', $request->input('asuransi'));
        $nilai11=str_replace('.', '', $request->input('no_of_insured'));

        /*
        if ($nilai == "") {
            $nilai = str_replace('.', '', $request->input('total_sum_insured'));
        }
        */
        if ( is_null($request->input('today_kurs')) ) {
                $nilai12  = NULL;
            } else {
                $nilai12 = str_replace('.', '', $request->input('today_kurs'));
                //$nilai12 = str_replace(',', '.', $nilai12);
            }

         $tglmulai=date('Y-m-d',strtotime($request->input('tgl_mulai')));
        $tglakhir = date('Y-m-d', strtotime('+'.$request->input('tgl_akhir').' days', strtotime($request->input('tgl_mulai'))));
        if($request->file('file')){
            $destination_path = public_path('upload_invoice');
            $files = $request->file('file');
            $filename = $files->getClientOriginalName();
            $upload_success = $files->move($destination_path,$request->input('id')."_".$filename);

            if ( $inv_type == 1) {

                // update endorsment
                DB::table('master_sales')
                ->where('id', $request->input('id'))
                ->update([
                    'polis_no' => $request->input('no_polis'),
                    'inv_no' => $request->input('invoice'),
                    'product_id' => $request->input('product'),
                    'ins_amount' => str_replace(',', '.', $nilai),
                    'disc_amount' => str_replace(',', '.', $nilai1),

                    'net_amount' => str_replace(',', '.', $nilai2),
                    'agent_id' => $request->input('officer'),

                    'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                    'end_date' => $tglakhir,
                    'afi_acc_no' => $request->input('bank'),
                    'segment_id' => $request->input('segment'),
                    'agent_fee_amount' => str_replace(',', '.', $nilai3),

                    'comp_fee_amount' => str_replace(',', '.', $nilai4),
                    'premi_amount' => str_replace(',', '.', $nilai5),

                    'polis_amount' => str_replace(',', '.', $nilai6),
                    'materai_amount' => str_replace(',', '.', $nilai7),
                    'admin_amount' => str_replace(',', '.', $nilai8),
                    'tax_amount' => str_replace(',', '.', $nilai9),
                    'ins_fee' => str_replace(',', '.', $nilai10),
                    'no_of_insured' => str_replace(',', '.', $nilai11),
                    'is_tax_company' => $request->input('is_tax_company'),

                    //'due_date_jrn' => $request->input('email'),
                    'paid_status_id' => 0,
                    'qs_no' => $request->input('quotation'),
                    'customer_id' => $request->input('customer'),
                    'qs_date' => $ref_quotation[0]->qs_date,
                    'proposal_no' => $request->input('proposal'),
                    'proposal_date' => $ref_proposal[0]->qs_date,
                    'is_active' => 1,
                    'wf_status_id' => 1,
                    'user_crt_id' => Auth::user()->id,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    'url_dokumen' => $filename,
                    'endors_reason' => $request->input('endorse_reason'),
                    'insured_name' => $request->input('insured_name'),
                    'start_date_polis' => $startDatePolice,
                    'end_date_polis' => $endDatePolice,
                    'valuta_id' => $valutaID,
                    'valuta_amount' => $valasAmount ? str_replace(',', '.', $valasAmount) : 0,
                    'kurs_today' =>  $nilai12 ?  str_replace(',', '.', $nilai12) : 0,
                ]);
            } else {

                DB::table('master_sales')
                ->where('id', $request->input('id'))
                ->update([
                    'polis_no' => $request->input('no_polis'),
                    'product_id' => $request->input('product'),
                    'ins_amount' => str_replace(',', '.', $nilai),
                    'disc_amount' => str_replace(',', '.', $nilai1),

                    'net_amount' => str_replace(',', '.', $nilai2),
                    'agent_id' => $request->input('officer'),

                    'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                    'end_date' => $tglakhir,
                    'afi_acc_no' => $request->input('bank'),
                    'segment_id' => $request->input('segment'),
                    'agent_fee_amount' => str_replace(',', '.', $nilai3),

                    'comp_fee_amount' => str_replace(',', '.', $nilai4),
                    'premi_amount' => str_replace(',', '.', $nilai5),

                    'polis_amount' => str_replace(',', '.', $nilai6),
                    'materai_amount' => str_replace(',', '.', $nilai7),
                    'admin_amount' => str_replace(',', '.', $nilai8),
                    'tax_amount' => str_replace(',', '.', $nilai9),
                    'ins_fee' => str_replace(',', '.', $nilai10),
                    'no_of_insured' => str_replace(',', '.', $nilai11),
                    'is_tax_company' => $request->input('is_tax_company'),

                    //'due_date_jrn' => $request->input('email'),
                    'paid_status_id' => 0,
                    'qs_no' => $request->input('quotation'),
                    'customer_id' => $request->input('customer'),
                    'qs_date' => $ref_quotation[0]->qs_date,
                    'proposal_no' => $request->input('proposal'),
                    'proposal_date' => $ref_proposal[0]->qs_date,
                    'is_active' => 1,
                    'wf_status_id' => 1,
                    'user_crt_id' => Auth::user()->id,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    'start_date_polis' => $startDatePolice,
                    'end_date_polis' => $endDatePolice,
                    'valuta_id' => $valutaID,
                    'valuta_amount' => $valasAmount ? str_replace(',', '.', $valasAmount) : 0,
                    'kurs_today' =>  $nilai12 ?  str_replace(',', '.', $nilai12) : 0
                ]);
            }


        }else{

            if ( $inv_type == 1) {
                 // Endorsment
                 DB::table('master_sales')
                ->where('id', $request->input('id'))
                ->update([
                    'inv_no' => $request->input('invoice'),
                    'product_id' => $request->input('product'),
                    'ins_amount' => str_replace(',', '.', $nilai),
                    'disc_amount' => str_replace(',', '.', $nilai1),

                    'net_amount' => str_replace(',', '.', $nilai2),
                    'agent_id' => $request->input('officer'),

                    'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                    'end_date' => $tglakhir,
                    'afi_acc_no' => $request->input('bank'),
                    'segment_id' => $request->input('segment'),
                    'agent_fee_amount' => str_replace(',', '.', $nilai3),

                    'comp_fee_amount' => str_replace(',', '.', $nilai4),
                    'premi_amount' => str_replace(',', '.', $nilai5),

                    'polis_amount' => str_replace(',', '.', $nilai6),
                    'materai_amount' => str_replace(',', '.', $nilai7),
                    'admin_amount' => str_replace(',', '.', $nilai8),
                    'tax_amount' => str_replace(',', '.', $nilai9),
                    'ins_fee' => str_replace(',', '.', $nilai10),
                    'no_of_insured' => str_replace(',', '.', $nilai11),
                    'is_tax_company' => $request->input('is_tax_company'),

                    //'due_date_jrn' => $request->input('email'),
                    'paid_status_id' => 0,
                    'qs_no' => $request->input('quotation'),
                    'customer_id' => $request->input('customer'),
                    'qs_date' => $ref_quotation[0]->qs_date,
                    'proposal_no' => $request->input('proposal'),
                    'proposal_date' => $ref_proposal[0]->qs_date,
                    'is_active' => 1,
                    'wf_status_id' => 1,
                    'user_crt_id' => Auth::user()->id,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    'endors_reason' => $request->input('endorse_reason'),
                    'insured_name' => $request->input('insured_name')
                ]);
            } else {
                DB::table('master_sales')
                ->where('id', $request->input('id'))
                ->update([
                    'polis_no' => $request->input('no_polis'),
                    'product_id' => $request->input('product'),
                    'ins_amount' => str_replace(',', '.', $nilai),
                    'disc_amount' => str_replace(',', '.', $nilai1),

                    'net_amount' => str_replace(',', '.', $nilai2),
                    'agent_id' => $request->input('officer'),

                    'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                    'end_date' => $tglakhir,
                    'afi_acc_no' => $request->input('bank'),
                    'segment_id' => $request->input('segment'),
                    'agent_fee_amount' => str_replace(',', '.', $nilai3),

                    'comp_fee_amount' => str_replace(',', '.', $nilai4),
                    'premi_amount' => str_replace(',', '.', $nilai5),

                    'polis_amount' => str_replace(',', '.', $nilai6),
                    'materai_amount' => str_replace(',', '.', $nilai7),
                    'admin_amount' => str_replace(',', '.', $nilai8),
                    'tax_amount' => str_replace(',', '.', $nilai9),
                    'ins_fee' => str_replace(',', '.', $nilai10),
                    'no_of_insured' => str_replace(',', '.', $nilai11),
                    'is_tax_company' => $request->input('is_tax_company'),

                    //'due_date_jrn' => $request->input('email'),
                    'paid_status_id' => 0,
                    'qs_no' => $request->input('quotation'),
                    'customer_id' => $request->input('customer'),
                    'qs_date' => $ref_quotation[0]->qs_date,
                    'proposal_no' => $request->input('proposal'),
                    'proposal_date' => $ref_proposal[0]->qs_date,
                    'is_active' => 1,
                    'wf_status_id' => 1,
                    'user_crt_id' => Auth::user()->id,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                     'start_date_polis' => $startDatePolice,
                        'end_date_polis' => $endDatePolice,
                        'valuta_id' => $valutaID,
                        'valuta_amount' => $valasAmount ? str_replace(',', '.', $valasAmount) : 0,
                        'kurs_today' =>  $nilai12 ?  str_replace(',', '.', $nilai12) : 0,
                ]);
            }
        }



        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }

     public function kirimAproval_invoice(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id in(1,3) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 2]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 2]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')->where('id', '=',$item->id)->delete();

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

              DB::table('master_sales')->where('id', '=',$item)->delete();
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function kirimAproval_invoice1(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($no){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $item->id,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Approve new invoice - '.$request->get('cct'),
                        ]
                    );
            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($no){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $item,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Approve new invoice - '.$request->get('cct'),
                        ]
                    );

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice1(Request $request){
        $results = \DB::select("select * from master_sales where wf_status_id=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

        if($request->get('type')=='semua'){


            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 3,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($results){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $item->id,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Reject new invoice - '.$request->get('cct'),
                        ]
                    );

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 3,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($results){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $item,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Reject new invoice - '.$request->get('cct'),
                        ]
                    );

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function detail_invoice(Request $request){

        $data = \DB::select('select a.*,mb.leader_name,mb.jabatan,b.full_name,b.address as c_address,ru.definition as underwriter, c.definition as produk,d.full_name as agent,e.qs_no as quotation,f.qs_no as proposal,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create,rv.mata_uang,rv.deskripsi
        from master_sales a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=a.agent_id
                    left join ref_quotation e on e.id=a.qs_no
                    left join ref_proposal f on f.id=a.proposal_no
                    left join ref_cust_segment g on g.id=a.segment_id
                    left join ref_bank_account h on h.id=a.afi_acc_no
                    left join ref_paid_status rw on rw.id = a.paid_status_id
                    left join master_user mu on mu.id = a.user_approval_id
                    left join master_user mus on mus.id = a.user_crt_id
                    left join master_branch mb on mb.id = a.branch_id
                                        left join ref_underwriter ru on a.underwriter_id = ru.id
                                        left join ref_valuta rv on rv.id=a.valuta_id where a.id='.$request->get('id'));

        return json_encode($data);
    }

    public function detail_invoice_schedule(Request $request){

        $data = \DB::select('select a.*,ms.kurs_today,mata_uang,deskripsi,ms.valuta_id,mb.leader_name,mb.jabatan,b.full_name,b.address as c_address,ru.definition as underwriter, c.definition as produk,d.full_name as agent,e.qs_no as quotation,f.qs_no as proposal,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create
        from master_sales_schedule a
        left join master_sales ms on ms.id = a.id_master_sales
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=a.agent_id
                    left join ref_quotation e on e.id=a.qs_no
                    left join ref_proposal f on f.id=a.proposal_no
                    left join ref_cust_segment g on g.id=a.segment_id
                    left join ref_bank_account h on h.id=a.afi_acc_no
                    left join ref_paid_status rw on rw.id = a.paid_status_id
                    left join master_user mu on mu.id = a.user_approval_id
                    left join master_user mus on mus.id = a.user_crt_id
                    left join master_branch mb on mb.id = a.branch_id
                    left join ref_valuta rv on rv.id = ms.valuta_id
										left join ref_underwriter ru on a.underwriter_id = ru.id  where a.id='.$request->get('id'));

        return json_encode($data);
    }


     public function kirimAproval_invoice_active(Request $request){

        $kurs = $this->clearSeparatorDouble($request->input('t_kurs'));


       if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=9 and paid_status_id=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);


            foreach ($results as $item) {
                $is_premi_paid = null;
                $is_agent_paid = null;
                $is_company_paid = null;


                if ($request->input('d1') == 'n') {
                    $is_premi_paid = 1;
                }
                if ($request->input('d2') == 'n') {
                    $is_agent_paid = 1;
                }
                if ($request->input('d3') == 'n') {
                    $is_company_paid = 1;
                }


                DB::table('master_sales')
                ->where('id', $item->id)
                ->update([
                    'wf_status_id' => 5,
                    'd1' => $request->input('d1'),
                    'd2' => $request->input('d2'),
                    'd3' => $request->input('d3'),
                    'is_premi_paid' => $is_premi_paid,
                    'is_agent_paid' => $is_agent_paid,
                    'is_company_paid' => $is_company_paid,
                    'd_type' => $request->input('jenis_bayar'),
                    'kurs_today' => $kurs,
                    // 'valuta_amount' => ($item->valuta_amount/$item->kurs_today)*$kurs,
                    'ins_amount' => ($item->valuta_id == 1) ? $item->ins_amount : ($item->ins_amount/$item->kurs_today)*$kurs,
                    'disc_amount' => ($item->valuta_id == 1) ? $item->disc_amount : ($item->disc_amount/$item->kurs_today)*$kurs,
                    'net_amount' => ($item->valuta_id == 1) ? $item->net_amount : ($item->net_amount/$item->kurs_today)*$kurs,
                    'agent_fee_amount' => ($item->valuta_id == 1) ? $item->agent_fee_amount : ($item->agent_fee_amount/$item->kurs_today)*$kurs,
                    'comp_fee_amount' => ($item->valuta_id == 1) ? $item->comp_fee_amount : ($item->comp_fee_amount/$item->kurs_today)*$kurs,
                    'premi_amount' => ($item->valuta_id == 1) ? $item->premi_amount : ($item->premi_amount/$item->kurs_today)*$kurs,
                    'polis_amount' => ($item->valuta_id == 1) ? $item->polis_amount : ($item->polis_amount/$item->kurs_today)*$kurs,
                    'materai_amount' => ($item->valuta_id == 1) ? $item->materai_amount : ($item->materai_amount/$item->kurs_today)*$kurs,
                    'admin_amount' => ($item->valuta_id == 1) ? $item->admin_amount : ($item->admin_amount/$item->kurs_today)*$kurs,
                    'tax_amount' => ($item->valuta_id == 1) ? $item->tax_amount : ($item->tax_amount/$item->kurs_today)*$kurs,
                    'ins_fee' => ($item->valuta_id == 1) ? $item->ins_fee : ($item->ins_fee/$item->kurs_today)*$kurs

                ]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                $getKurs = collect(\DB::select("SELECT * from master_sales
                where id = ".$item." and wf_status_id=9 and paid_status_id=0
                and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id))->first();


                $is_premi_paid = null;
                $is_agent_paid = null;
                $is_company_paid = null;


                if ($request->input('d1') == 'n') {
                    $is_premi_paid = 1;
                }
                if ($request->input('d2') == 'n') {
                    $is_agent_paid = 1;
                }
                if ($request->input('d3') == 'n') {
                    $is_company_paid = 1;
                }

               DB::table('master_sales')
                ->where('id', $item)
                ->update([
                    'wf_status_id' => 5,
                    'd1' => $request->input('d1'),
                    'd2' => $request->input('d2'),
                    'd3' => $request->input('d3'),
                    'is_premi_paid' => $is_premi_paid,
                    'is_agent_paid' => $is_agent_paid,
                    'is_company_paid' => $is_company_paid,
                    'd_type' => $request->input('jenis_bayar'),
                    'kurs_today' => $kurs,
                    // 'valuta_amount' => ($getKurs->valuta_amount/$getKurs->kurs_today)*$kurs,
                    'ins_amount' => ($getKurs->valuta_id == 1) ? $getKurs->ins_amount : ($getKurs->ins_amount/$getKurs->kurs_today)*$kurs,
                    'disc_amount' => ($getKurs->valuta_id == 1) ? $getKurs->disc_amount : ($getKurs->disc_amount/$getKurs->kurs_today)*$kurs,
                    'net_amount' => ($getKurs->valuta_id == 1) ? $getKurs->net_amount : ($getKurs->net_amount/$getKurs->kurs_today)*$kurs,
                    'agent_fee_amount' => ($getKurs->valuta_id == 1) ? $getKurs->agent_fee_amount : ($getKurs->agent_fee_amount/$getKurs->kurs_today)*$kurs,
                    'comp_fee_amount' => ($getKurs->valuta_id == 1) ? $getKurs->comp_fee_amount : ($getKurs->comp_fee_amount/$getKurs->kurs_today)*$kurs,
                    'premi_amount' => ($getKurs->valuta_id == 1) ? $getKurs->premi_amount : ($getKurs->premi_amount/$getKurs->kurs_today)*$kurs,
                    'polis_amount' => ($getKurs->valuta_id == 1) ? $getKurs->polis_amount : ($getKurs->polis_amount/$getKurs->kurs_today)*$kurs,
                    'materai_amount' => ($getKurs->valuta_id == 1) ? $getKurs->materai_amount : ($getKurs->materai_amount/$getKurs->kurs_today)*$kurs,
                    'admin_amount' => ($getKurs->valuta_id == 1) ? $getKurs->admin_amount : ($getKurs->admin_amount/$getKurs->kurs_today)*$kurs,
                    'tax_amount' => ($getKurs->valuta_id == 1) ? $getKurs->tax_amount : ($getKurs->tax_amount/$getKurs->kurs_today)*$kurs,
                    'ins_fee' => ($getKurs->valuta_id == 1) ? $getKurs->ins_fee : ($getKurs->ins_fee/$getKurs->kurs_today)*$kurs

                ]);


            }

        // return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice_active(Request $request){
        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=9 and paid_status_id=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 10]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 10]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

     public function split_paid(Request $request){
        /*
         foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 4,'ket_jurnal'=>$request->get('type')]);
        }
        */
        if($request->get('type')=='agent'){

           DB::table('master_sales')
                ->where('id', $request->get('id'))
                ->update(['is_agent_paid' => 0,'ket_jurnal'=>$request->get('type')]);


        }elseif($request->get('type')=='commision'){

            DB::table('master_sales')
                ->where('id', $request->get('id'))
                ->update(['is_company_paid' => 0,'ket_jurnal'=>$request->get('type')]);


        }elseif($request->get('type')=='premium'){
            DB::table('master_sales')
                ->where('id', $request->get('id'))
                ->update(['is_premi_paid' => 0,'ket_jurnal'=>$request->get('type')]);


        }else{

            $item1 = \DB::select("select * from master_sales where id=".$request->get('id'));

            foreach ($item1 as $key) {
                if(!isset($key->is_agent_paid) && $key->d2 =='t'){
                    DB::table('master_sales')
                    ->where('id', $request->get('id'))
                    ->update([
                        'is_agent_paid' => false,
                        'ket_jurnal'=>$request->get('type')]);
                }

                if(!isset($key->is_company_paid) && $key->d3 =='t'){
                    DB::table('master_sales')
                    ->where('id', $request->get('id'))
                    ->update([
                        'is_company_paid' => false,
                        'ket_jurnal'=>$request->get('type')]);
                }

                if(!isset($key->is_premi_paid) && $key->d1 =='t'){
                    DB::table('master_sales')
                    ->where('id', $request->get('id'))
                    ->update([
                        'is_premi_paid' => false,
                        'ket_jurnal'=>$request->get('type')]);
                }

            }





        }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }

     public function drop_selected(Request $request){

         foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 10]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }


    public function kirimAproval_app_split(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=4 and paid_status_id=1 and is_overdue=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 13,'notes'=>$request->get('cct'),'is_splitted'=>true,'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 13,'notes'=>$request->get('cct'),'is_splitted'=>true,'user_approval_id'=>Auth::user()->id]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_app_split(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=4 and paid_status_id=1 and is_overdue=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function ini_untuk_overdue_split(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=9 and paid_status_id=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            $config = \DB::select("select value from master_config where id=1");
            foreach ($results as $item) {
                $plus="'+".$config[0]->value." days'";
                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($item->inv_date)));




                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));


                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'paid_status_id'=>1,'is_overdue'=>true,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_splitted'=>false]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {
                $results = \DB::select("select * from master_sales where id=".$item);
                $config = \DB::select("select value from master_config where id=1");


                $plus="'+".$config[0]->value." days'";
                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($results[0]->inv_date)));

                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));



               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'paid_status_id'=>1,'is_overdue'=>true,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_splitted'=>false]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function kirimAproval_invoice_paid(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=5 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($item->inv_date)));
                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($item->inv_date)));




                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));

                $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

                $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=1 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();


                $str=strlen($ref_invoice->next_no);
                if($str==3){

                   $no_invoice='MKT/III/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;

                }elseif($str==2){
                   $no_invoice='MKT/III/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }else{
                    $no_invoice='MKT/III/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }

                DB::table('ref_invoice_no')
            ->where('company_id', Auth::user()->company_id)
            ->where('year', date('Y'))
            ->where('type',1)
            ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);


                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'paid_status_id'=>1,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_overdue'=>false,'user_approval_id'=>Auth::user()->id,'kwitansi_no'=>$no_invoice]);


                $txcode='';
                $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

                $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                $prx=date('Ymd',strtotime($item->inv_date));
                $prx_branch=Auth::user()->branch_id;
                $prx_company=Auth::user()->branch_id;

                if($results){

                  $id_max= $results[0]->max_id;
                  $sort_num = (int) substr($id_max, 1, 4);
                  $sort_num++;
                  $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                  $txcode=$new_code;
                }else{
                    $txcode=$prx.$prx_branch.$prx_company."0001";
                }

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($no){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $txcode,
                            'ref_code' => $item->id,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Approve payment - '.$request->get('cct'),
                        ]
                    );




                $idsegment=$item->segment_id;

                $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=1 and company_id=".Auth::user()->company_id." order by id asc ");

                foreach ($config as $value) {
                        $id='';
                        $results = \DB::select("SELECT max(id) AS id from master_tx");
                        if($results){
                            $id=$results[0]->id + 1;
                        }else{
                            $id=1;
                        }



                        if($value->coa_no){
                            $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");


                            $d1 = 0;
                            $d2 = 0;
                            $d3 = 0;

                            // RUMUS SPLIT

                            if( $item->d1 == 't'){
                                $d1 = $item->premi_amount + $item->polis_amount + $item->materai_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_premi_paid' => 1]);
                            }

                            if( $item->d2 == 't'){
                                $d2 = $item->agent_fee_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_agent_paid' => 1]);
                            }

                            if( $item->d3 == 't'){
                                $d3 = $item->comp_fee_amount + $item->admin_amount;
                            }else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_company_paid' => 1]);
                            }

                            if ($item->d_type == 'penuh') {
                                $txamount= $item->premi_amount + $item->polis_amount + $item->materai_amount + $item->agent_fee_amount + $item->comp_fee_amount + $item->admin_amount;
                            }else {
                                $txamount = $d1 + $d2 + $d3;
                            }

                            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                            $set_last_seq_app = $last_seq_app->max_id+1;


                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $value->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $txamount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                    'seq_approval' => $set_last_seq_app
                                ]
                            );

                            $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                            if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $txamount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $txamount;
                                }

                            $acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->net_amount;

                            /*
                            if($value->tx_type_id==0){
                                $acc_os=$coa1[0]->last_os - $item->net_amount;
                            }else{
                                $acc_os=$coa1[0]->last_os + $item->net_amount;
                            }*/
                             

                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('coa_no', $value->coa_no)
                             ->where('branch_id', Auth::user()->branch_id)
                            ->where('company_id', Auth::user()->company_id)
                            ->update(['last_os' => $acc_os]);

                          }else{

                            $coa = \DB::select("select b.coa_no from master_sales a
                            join ref_bank_account b on a.afi_acc_no=b.id
                            where a.id=".$item->id);

                            $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");


                            $d1 = 0;
                            $d2 = 0;
                            $d3 = 0;

                            // RUMUS SPLIT

                            if( $item->d1 == 't'){
                                $d1 = $item->premi_amount + $item->polis_amount + $item->materai_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_premi_paid' => 1]);
                            }

                            if( $item->d2 == 't'){
                                $d2 = $item->agent_fee_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_agent_paid' => 1]);
                            }

                            if( $item->d3 == 't'){
                                $d3 = $item->comp_fee_amount + $item->admin_amount;
                            }else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_company_paid' => 1]);
                            }

                            if ($item->d_type == 'penuh') {
                                $txamount= $item->premi_amount + $item->polis_amount + $item->materai_amount + $item->agent_fee_amount + $item->comp_fee_amount + $item->admin_amount;
                            }else {
                                $txamount = $d1 + $d2 + $d3;
                            }

                            // $txamount=$item->net_amount+$item->polis_amount+$item->materai_amount+$item->admin_amount;
                            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                            $set_last_seq_app = $last_seq_app->max_id+1;

                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $coa[0]->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $txamount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                    'seq_approval' => $set_last_seq_app,
                                ]
                            );
                            $coa1 = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and  coa_no='".$coa[0]->coa_no."'");
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                            if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $txamount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $txamount;
                                }

                            $acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->net_amount;


                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('branch_id', Auth::user()->branch_id)
                            ->where('coa_no', $coa[0]->coa_no)
                            ->update(['last_os' => $acc_os]);


                    }
                }

                          }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{

            foreach ($request->value as $value) {
                $results = \DB::select("select * from master_sales where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." and id=".$value);

                $setMsId = $value;

                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($results[0]->inv_date)));




                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));

                $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

                $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=1 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();


                $str=strlen($ref_invoice->next_no);
                if($str==3){

                   $no_invoice='MKT/III/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;

                }elseif($str==2){
                   $no_invoice='MKT/III/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }else{
                    $no_invoice='MKT/III/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }

                DB::table('ref_invoice_no')
            ->where('company_id', Auth::user()->company_id)
            ->where('year', date('Y'))
            ->where('type',1)
            ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

               DB::table('master_sales')
                ->where('id', $value)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'paid_status_id'=>1,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_overdue'=>false,'user_approval_id'=>Auth::user()->id,'kwitansi_no'=>$no_invoice]);



                foreach ($results as $item) {

                    $txcode='';
                    $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

                    $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                    $prx=date('Ymd',strtotime($item->inv_date));
                    $prx_branch=Auth::user()->branch_id;
                    $prx_company=Auth::user()->company_id;

                    if($results){

                      $id_max= $results[0]->max_id;
                      $sort_num = (int) substr($id_max, 1, 4);
                      $sort_num++;
                      $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                      $txcode=$new_code;
                    }else{
                        $txcode=$prx.$prx_branch.$prx_company."0001";
                    }

                    $no = \DB::select("SELECT max(id) AS id from master_memo");
                    $id='';
                    if($no){
                        $id=$no[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    DB::table('master_memo')->insert(
                            [
                                'id' => $id,
                                'tx_code' => $txcode,
                                'ref_code' => $item->id,
                                'ref_code_type' => 'invoice',
                                'id_workflow' => 9,
                                'user_crt_id' => Auth::user()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'notes' =>'Approve payment - '.$request->get('cct'),
                            ]
                        );



                    $idsegment=$item->segment_id;

                    $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=1 and company_id=".Auth::user()->company_id." order by id asc ");
                      foreach ($config as $value) {
                        $id='';
                        $results = \DB::select("SELECT max(id) AS id from master_tx");
                        if($results){
                            $id=$results[0]->id + 1;
                        }else{
                            $id=1;
                        }

                        if($value->coa_no){
                            $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and  coa_no='".$value->coa_no."'");
                            // $txamount=$item->ins_fee;

                            $d1 = 0;
                            $d2 = 0;
                            $d3 = 0;

                            // RUMUS SPLIT

                            if( $item->d1 == 't'){
                                $d1 = $item->premi_amount + $item->polis_amount + $item->materai_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_premi_paid' => 1]);
                            }

                            if( $item->d2 == 't'){
                                $d2 = $item->agent_fee_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_agent_paid' => 1]);
                            }

                            if( $item->d3 == 't'){
                                $d3 = $item->comp_fee_amount + $item->admin_amount;
                            }else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_company_paid' => 1]);
                            }

                            if ($item->d_type == 'penuh') {
                                $txamount= $item->premi_amount + $item->polis_amount + $item->materai_amount + $item->agent_fee_amount + $item->comp_fee_amount + $item->admin_amount;
                            }else {
                                $txamount = $d1 + $d2 + $d3;
                            }

                            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                            $set_last_seq_app = $last_seq_app->max_id+1;

                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $value->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $txamount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                    'seq_approval' => $set_last_seq_app
                                ]
                            );

                            $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                            if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $txamount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $txamount;
                                }

                            /*

                            if($value->tx_type_id==0){
                                $acc_os=$coa1[0]->last_os - $item->net_amount;
                            }else{
                                $acc_os=$coa1[0]->last_os + $item->net_amount;

                            }
                            */

                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('coa_no', $value->coa_no)
                             ->where('branch_id', Auth::user()->branch_id)
                            ->where('company_id', Auth::user()->company_id)
                            ->update(['last_os' => $acc_os]);

                        }else{
                             $coa = \DB::select("select b.coa_no from master_sales a
                            join ref_bank_account b on a.afi_acc_no=b.id
                            where a.id=".$item->id);

                             $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                            //  $txamount=$item->ins_fee;

                            $d1 = 0;
                            $d2 = 0;
                            $d3 = 0;

                            // RUMUS SPLIT

                            if( $item->d1 == 't'){
                                $d1 = $item->premi_amount + $item->polis_amount + $item->materai_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_premi_paid' => 1]);
                            }

                            if( $item->d2 == 't'){
                                $d2 = $item->agent_fee_amount;
                            }
                            else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_agent_paid' => 1]);
                            }

                            if( $item->d3 == 't'){
                                $d3 = $item->comp_fee_amount + $item->admin_amount;
                            }else{
                                DB::table('master_sales')
                                ->where('id', $item->id)
                                ->update(['is_company_paid' => 1]);
                            }

                            if ($item->d_type == 'penuh') {
                                $txamount= $item->premi_amount + $item->polis_amount + $item->materai_amount + $item->agent_fee_amount + $item->comp_fee_amount + $item->admin_amount;
                            }else {
                                $txamount = $d1 + $d2 + $d3;
                            }

                            $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                            $set_last_seq_app = $last_seq_app->max_id+1;

                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $coa[0]->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $txamount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                    'seq_approval' => $set_last_seq_app
                                ]
                            );

                            $coa1 = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                            if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $txamount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $txamount;
                                }
                            /*
                            if($value->tx_type_id==0){
                                $acc_os=$coa1[0]->last_os - $item->net_amount;
                            }else{
                                $acc_os=$coa1[0]->last_os + $item->net_amount;
                            }
                            */

                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('branch_id', Auth::user()->branch_id)
                            ->where('coa_no', $coa[0]->coa_no)
                            ->update(['last_os' => $acc_os]);

                        }
                    }

                    }
                }
            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }




    public function hapusAproval_invoice_paid(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=5 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($no){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $item->id,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Reject payment - '.$request->get('cct'),
                        ]
                    );


            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($no){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $item,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Reject payment - '.$request->get('cct'),
                        ]
                    );

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function kirimAproval_invoice_paid1(Request $request){
        if($request->get('type')=='semua'){
            // is_overdue=true
            //$results = \DB::select("select * from master_sales where wf_status_id=4 and paid_status_id=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            $results = \DB::select("select id,polis_no,full_name,definition,keterangan,total,is_pilih,inv_date,is_tax_company from(

            select a.id,polis_no,b.full_name,c.definition,'agent' as keterangan,is_agent_paid as is_pilih,inv_date,is_tax_company,
            case
            when is_agent_paid=0 then agent_fee_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."


            union all

            select a.id,polis_no,b.full_name,c.definition,'commision' as keterangan,is_company_paid as is_pilih,inv_date,is_tax_company,
            case
            when is_company_paid=0 then comp_fee_amount + admin_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."

            union all

            select a.id,polis_no,b.full_name,c.definition,'premium' as keterangan,is_premi_paid as is_pilih,inv_date,is_tax_company,
            case
            when is_premi_paid=0 then premi_amount+polis_amount+materai_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."

            ) as y ");


            foreach ($results as $item) {
                /*
                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 13,'is_splitted'=>true,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

                */
                if(isset($item->is_pilih)){
                     if($item->is_pilih==false){

                    $txcode='';
                    $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

                    $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                    $prx=date('Ymd',strtotime($item->inv_date));
                    $prx_branch=Auth::user()->branch_id;
                    $prx_company=Auth::user()->branch_id;

                    if($results){

                      $id_max= $results[0]->max_id;
                      $sort_num = (int) substr($id_max, 1, 4);
                      $sort_num++;
                      $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                      $txcode=$new_code;
                      }else{
                        $txcode=$prx.$prx_branch.$prx_company."0001";
                    }

                    $no = \DB::select("SELECT max(id) AS id from master_memo");
                    $id='';
                    if($no){
                        $id=$no[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    DB::table('master_memo')->insert(
                            [
                                'id' => $id,
                                'ref_code' => $item->id,
                                'ref_code_type' => 'invoice',
                                'id_workflow' => 9,
                                'user_crt_id' => Auth::user()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'notes' =>'Approve split payment - '.$request->get('cct'),
                            ]
                        );



                    //$idsegment=$item->segment_id;

                    if($item->is_tax_company==true){
                        $this->jurnal_split_payment_true($item->id,$item->keterangan);
                    }else{
                        $this->jurnal_split_payment_false($item->id,$item->keterangan);
                    }



                }
                }



            }
        //}

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{

            foreach ($request->value as $item) {

                if($item){

                $exp=explode('_',$item);
                $id_itme=$exp[0];
                $ket=$exp[1];

                $query = \DB::select("select * from master_sales where id=".$id_itme." and paid_status_id=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($no){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $id_itme,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Approve split payment - '.$request->get('cct'),
                        ]
                    );



                if($query[0]->is_tax_company==true){
                    $this->jurnal_split_payment_true($id_itme,$ket);
                }else{
                    $this->jurnal_split_payment_false($id_itme,$ket);
                }

                }


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice_paid1(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id in (1,9) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'is_agent_paid' => null,'is_company_paid' => null,'is_premi_paid' => null,'user_approval_id'=>Auth::user()->id]);

                $no = \DB::select("SELECT max(id) AS id from master_memo");
                $id='';
                if($results){
                    $id=$no[0]->id + 1;
                }else{
                    $id=1;
                }

                DB::table('master_memo')->insert(
                        [
                            'id' => $id,
                            'ref_code' => $item->id,
                            'ref_code_type' => 'invoice',
                            'id_workflow' => 9,
                            'user_crt_id' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'notes' =>'Reject split payment - '.$request->get('cct'),
                        ]
                    );


            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {
                if($item){
                    $exp=explode('_',$item);
                    $id_itme=$exp[0];
                    $ket=$exp[1];

                    if($ket=='agent'){
                        DB::table('master_sales')
                        ->where('id', $id_itme)
                        ->update(['is_agent_paid' => null,'wf_status_id' => 9 ]);

                    }elseif($ket=='commision'){
                         DB::table('master_sales')
                        ->where('id', $id_itme)
                        ->update(['is_company_paid' => null,'wf_status_id' => 9]);

                    }else{
                         DB::table('master_sales')
                        ->where('id', $id_itme)
                        ->update(['is_premi_paid' => null,'wf_status_id' => 9]);

                    }
                    /*
                    DB::table('master_sales')
                    ->where('id', $id_itme)
                    ->update(['wf_status_id' => 13,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);
                    */

                    $no = \DB::select("SELECT max(id) AS id from master_memo");
                    $id='';
                    if($no){
                        $id=$no[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    DB::table('master_memo')->insert(
                            [
                                'id' => $id,
                                'ref_code' => $id_itme,
                                'ref_code_type' => 'invoice',
                                'id_workflow' => 9,
                                'user_crt_id' => Auth::user()->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'notes' =>'Reject split payment - '.$request->get('cct'),
                            ]
                    );

                }


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function approval_split_payment(Request $request)
    {
      /*
      $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=4 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
       */

    //    RUMUS SPLIT

        $data = \DB::select("select id,polis_no,full_name,definition,keterangan,total,is_pilih from(

            select a.id,polis_no,b.full_name,c.definition,'agent' as keterangan,is_agent_paid as is_pilih,
            case
            when is_agent_paid=0 then agent_fee_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."


            union all

            select a.id,polis_no,b.full_name,c.definition,'commision' as keterangan,is_company_paid as is_pilih,
            case
            when is_company_paid=0 then comp_fee_amount + admin_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."

            union all

            select a.id,polis_no,b.full_name,c.definition,'premium' as keterangan,is_premi_paid as is_pilih,
            case
            when is_premi_paid=0 then premi_amount+polis_amount+materai_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."

            ) as y



            ");


       $param['data']=$data;
     return view('master.master')->nest('child', 'invoice.approval_split_payment',$param);

    }


    public function complete_payment(Request $request)
    {

    //   $data = \DB::select("select d1,d2,d3,a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id from master_sales a
    //         left join master_customer b on b.id=a.customer_id
    //         left join ref_product c on c.id=a.product_id
    //         left join ref_paid_status d on d.id=a.paid_status_id
    //         left join master_branch e on e.id=a.branch_id
    //                     left join ref_valuta f on f.id=a.valuta_id where a.wf_status_id in (13) and paid_status_id=1 and is_splitted=true and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

    $data = \DB::select("select d1,d2,d3,a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id from master_sales a
    left join master_customer b on b.id=a.customer_id
    left join ref_product c on c.id=a.product_id
    left join ref_paid_status d on d.id=a.paid_status_id
    left join master_branch e on e.id=a.branch_id
    left join ref_valuta f on f.id=a.valuta_id
                where a.is_agent_paid = 1 and a.is_company_paid = 1 and a.is_premi_paid = 1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);


       $param['data']=$data;
     return view('master.master')->nest('child', 'invoice.complete_split',$param);

    }

    public function reversal_payment(Request $request){

            foreach ($request->value as $item) {
               if($request->type=='payment'){
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 14]);
               }else{
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 11]);
               }

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }
    public function list_reversal_payment(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,a.is_premi_paid,a.invoice_type_id,a.url_dokumen,e.short_code ,f.mata_uang,a.kurs_today,a.valuta_id from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id
            left join master_branch e on e.id=a.branch_id
                        left join ref_valuta f on f.id=a.valuta_id where a.wf_status_id=14 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.list_reversal_payment',$param);

    }

    public function list_reversal_split(Request $request)
    {
        /*
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=11 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
        */
        $data = \DB::select("select id,polis_no,full_name,definition,keterangan,total,is_pilih from(

            select a.id,polis_no,b.full_name,c.definition,'agent' as keterangan,is_agent_paid as is_pilih,
            case
            when is_agent_paid=2 then agent_fee_amount + disc_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id in (9,11,13) and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."


            union all

            select a.id,polis_no,b.full_name,c.definition,'commision' as keterangan,is_company_paid as is_pilih,
            case
            when is_company_paid=2 then comp_fee_amount
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id in (9,11,13) and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."

            union all

            select a.id,polis_no,b.full_name,c.definition,'premium' as keterangan,is_premi_paid as is_pilih,
            case
            when is_premi_paid=2 then ins_fee
            else
            0
            end as total
            from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            where a.wf_status_id in (9,11,13) and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id."

            ) as y



            ");

        $param['data']=$data;
        return view('master.master')->nest('child', 'invoice.list_reversal_split',$param);

    }

    public function kirimAproval_reversal(Request $request){

        if($request->get('type')=='semua'){
            $query = \DB::select("select * from master_sales where wf_status_id=14 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            foreach ($query as $value) {
                 DB::table('master_sales')
                        ->where('id', $value->id)
                        ->update(['wf_status_id' => 9,'paid_status_id'=>0 ,'is_agent_paid'=>null,'is_premi_paid'=>null,'is_company_paid'=>null]);

                // DB::table('master_tx')
                // ->where('id_rel', $value->id)
                // ->update(['id_workflow' => 3]);

                $data = \DB::select("select * from master_tx where id_rel='".$value->id."'  and type_tx='invoice_payment'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;


                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                            'seq_approval' => $set_last_seq_app
                        ]
                    );

                }


                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true  and type_tx='reversal_invoice' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);



                foreach ($results as $item) {

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 3)
                    ->update(['id_workflow' => 12]);

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('id_workflow', 3)
                    ->update(['is_reversal' => true,'id_workflow' => 15]);

                    DB::table('master_memo')
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 10]);

                    $coa = \DB::select("select last_os from master_coa where branch_id=".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        //->where('is_reversal', true)
                        //->where('id_workflow', 12)
                        ->update(['acc_last' => $coa[0]->last_os]);

                        $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                        if($ref_jurnal[0]->operator_sign=='-'){
                            $acc_os=$coa[0]->last_os - $item->tx_amount;
                        }else{
                            $acc_os=$coa[0]->last_os + $item->tx_amount;
                        }

                        //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $item->tx_amount;


                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ///->where('is_reversal', true)
                        ///->where('id_workflow', 12)
                        ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$item->tx_notes]);

                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', $item->coa_id)
                        ->update(['last_os' => $acc_os]);


                }


            }

        }else{


            foreach ($request->value as $item) {



                 DB::table('master_sales')
                        ->where('id', $item)
                        ->update(['wf_status_id' => 9,'paid_status_id'=>0,'is_agent_paid'=>null,'is_premi_paid'=>null,'is_company_paid'=>null]);

                // DB::table('master_tx')
                // ->where('id_rel', $item)
                // ->update(['id_workflow' => 3]);

                $get_tx_code = collect(\DB::select("select * from master_tx where id_rel='".$item."' and type_tx='invoice_payment' order by id desc"))->first();

                $data = \DB::select("select * from master_tx where id_rel='".$item."' and type_tx='invoice_payment' and tx_code = '".$get_tx_code->tx_code."'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                            'seq_approval' => $set_last_seq_app
                        ]
                    );
                }
                /*
                DB::table('master_tx')
                ->where('tx_code', $data[0]->tx_code)
                ->where('is_reversal', true)
                ->where('id_workflow', 3)
                ->update(['id_workflow' => 12]);


                 DB::table('master_tx')
                ->where('tx_code', $data[0]->tx_code)
                ->where('id_workflow', 3)
                ->update(['is_reversal' => true,'id_workflow' => 15]);


                DB::table('master_memo')
                ->where('tx_code', $data[0]->tx_code)
                ->update(['id_workflow' => 10]);
                */

                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $dt) {
                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 3)
                    ->update(['id_workflow' => 12]);

                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('id_workflow', 3)
                    ->update(['is_reversal' => true,'id_workflow' => 15]);

                    DB::table('master_memo')
                    ->where('id', $dt->id)
                    ->update(['id_workflow' => 10]);

                    $coa = \DB::select("select last_os from master_coa where branch_id=".Auth::user()->branch_id." and coa_no='".$dt->coa_id."'");

                     DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                   // ->where('is_reversal', true)
                 //   ->where('id_workflow', 12)
                    ->update(['acc_last' => $coa[0]->last_os]);

                    $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$dt->coa_type_id."' and tx_type_id=".$dt->tx_type_id);

                    if($ref_jurnal[0]->operator_sign=='-'){
                        $acc_os=$coa[0]->last_os - $dt->tx_amount;
                    }else{
                        $acc_os=$coa[0]->last_os + $dt->tx_amount;
                    }

                    //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $dt->tx_amount;


                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                    //->where('is_reversal', true)
                    //->where('id_workflow', 12)
                    ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$dt->tx_notes]);

                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', $dt->coa_id)
                    ->update(['last_os' => $acc_os]);


                 }
                    /*
                    DB::table('master_sales')
                    ->where('id', $item)
                    ->update(['wf_status_id' => 9,'paid_status_id'=>0]);
                    */
                 }

            }

        }


//    }


    public function kirimAproval_reversal1(Request $request){

        if($request->get('type')=='semua'){

            $query = \DB::select("select * from master_sales where wf_status_id in (9,11)");
            foreach ($query as $value) {


                DB::table('master_sales')
                ->where('id', $value->id)
                ->update(['wf_status_id' => 9,'is_premi_paid'=>null,'is_company_paid'=>null,'is_agent_paid'=>null]);

                DB::table('master_tx')
                ->where('id_rel', $value->id)
                ->update(['id_workflow' => 3]);

                $data = \DB::select("select * from master_tx where id_rel='".$value->id."' and type_tx='invoice_split'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'id_rel' => $value->id,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                            'seq_approval' => $set_last_seq_app
                        ]
                    );

                }

                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true and  type_tx='reversal_invoice' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $item) {

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    //->where('is_reversal', true)
                   // ->where('id_workflow', 3)
                    ->update(['id_workflow' => 12]);

/*
                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('id_workflow', 3)
                    ->update(['is_reversal' => true,'id_workflow' => 15]);
*/
                    DB::table('master_memo')
                    ->where('tx_code', $item->tx_code)
                    ->update(['id_workflow' => 10]);

                    $coa = \DB::select("select last_os from master_coa where branch_id=".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        //->where('is_reversal', true)
                       // ->where('id_workflow', 12)
                        ->update(['acc_last' => $coa[0]->last_os]);

                        $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                        if($ref_jurnal[0]->operator_sign=='-'){
                            $acc_os=$coa[0]->last_os - $item->tx_amount;
                        }else{
                            $acc_os=$coa[0]->last_os + $item->tx_amount;
                        }

                        //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $item->tx_amount;


                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        //->where('is_reversal', true)
                       // ->where('id_workflow', 12)
                        ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$item->tx_notes]);


                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', $item->coa_id)
                        ->update(['last_os' => $acc_os]);
                }


            }

        }else{
            foreach ($request->value as $item) {


                DB::table('master_tx')
                ->where('id_rel', $item)
                ->update(['id_workflow' => 3]);



                $data = \DB::select("select * from master_tx where id_rel='".$item."'  and type_tx='invoice_split'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                    $set_last_seq_app = $last_seq_app->max_id+1;


                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' =>$dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'id_rel' => $item,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_invoice',
                            'id_rel'=>$dt->id_rel,
                            'coa_type_id' => $dt->coa_type_id,
                            'seq_approval' => $set_last_seq_app
                        ]
                    );


                }


                /*

                DB::table('master_tx')
                ->where('id', $data[0]->id)
                ->where('is_reversal', true)
                ->where('id_workflow', 3)
                ->update(['id_workflow' => 12]);

                 DB::table('master_tx')
                ->where('tx_code', $data[0]->tx_code)
                ->where('id_workflow', 3)
                ->update(['is_reversal' => true,'id_workflow' => 15]);
                */

                DB::table('master_memo')
                ->where('tx_code', $data[0]->tx_code)
                ->update(['id_workflow' => 10]);

                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true and type_tx='reversal_invoice' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $dt) {

                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    //->where('is_reversal', true)
                    //->where('id_workflow', 3)
                    ->update(['id_workflow' => 12]);

                    $coa = \DB::select("select last_os from master_coa where branch_id=".Auth::user()->branch_id." and coa_no='".$dt->coa_id."'");

                     DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                    //->where('is_reversal', true)
                    //->where('id_workflow', 12)
                    ->update(['acc_last' => $coa[0]->last_os]);

                    $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$dt->coa_type_id."' and tx_type_id=".$dt->tx_type_id);

                    if($ref_jurnal[0]->operator_sign=='-'){
                        $acc_os=$coa[0]->last_os - $dt->tx_amount;
                    }else{
                        $acc_os=$coa[0]->last_os + $dt->tx_amount;
                    }

                    //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $dt->tx_amount;


                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                    //->where('is_reversal', true)
                   // ->where('id_workflow', 12)
                    ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$dt->tx_notes]);


                    DB::table('master_coa')
                    ->where('branch_id', Auth::user()->branch_id)
                    ->where('coa_no', $dt->coa_id)
                    ->update(['last_os' => $acc_os]);

                 }
                 DB::table('master_sales')
                    ->where('id', $item)
                    ->update(['wf_status_id' => 9,'is_premi_paid'=>null,'is_company_paid'=>null,'is_agent_paid'=>null]);
            }

        }

    }

    public function hapusAproval_reversal(Request $request){

        if($request->get('type')=='semua'){
           $query = \DB::select("select * from master_sales where wf_status_id=14 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            foreach ($query as $value) {
                DB::table('master_sales')
                ->where('id', $value->id)
                ->update(['wf_status_id' => 9]);

            }

        }else{
            foreach ($request->value as $item) {
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9]);
            }

        }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }

    public function hapusAproval_reversal1(Request $request){

        if($request->get('type')=='semua'){
            $query = \DB::select("select * from master_sales where wf_status_id=11");
            foreach ($query as $value) {
                DB::table('master_sales')
                ->where('id', $value->id)
                ->update(['wf_status_id' => 9]);

            }

        }else{
            foreach ($request->value as $item) {
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9]);
            }

        }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }
    function getRomawi($bln){
        switch ($bln){
            case 1:
            return "I";
            break;
            case 2:
            return "II";
            break;
            case 3:
            return "III";
            break;
            case 4:
            return "IV";
            break;
            case 5:
            return "V";
            break;
            case 6:
            return "VI";
            break;
            case 7:
            return "VII";
            break;
            case 8:
            return "VIII";
            break;
            case 9:
            return "IX";
            break;
            case 10:
            return "X";
            break;
            case 11:
            return "XI";
            break;
            case 12:
            return "XII";
            break;
        }
    }


    function jurnal_split_payment_true($id,$ket){


        $item = collect(\DB::select("select * from master_sales where id=".$id))->first();

        $ref_id_invoice='';

        if($ket=='agent'){
             $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and invoice_id=0 and is_tax_company=true and company_id=".Auth::user()->company_id." order by id asc ");

             DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_agent_paid' => 1,'user_approval_id'=>Auth::user()->id]);
            $ref_id_invoice=0;
        }elseif($ket=='commision'){
            $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and invoice_id=1 and is_tax_company=true and company_id=".Auth::user()->company_id." order by id asc ");

            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_company_paid' => 1,'user_approval_id'=>Auth::user()->id]);

            $ref_id_invoice=1;
        }elseif ($ket=='premium') {
            // 7 DAN 10
            $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and invoice_id=2 and is_tax_company=true and company_id=".Auth::user()->company_id." order by id asc ");

            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_premi_paid' => 1,'user_approval_id'=>Auth::user()->id]);
            $ref_id_invoice=2;
        }else{
            $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and is_tax_company=true and company_id=".Auth::user()->company_id." order by id asc ");
             DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_agent_paid' => 1,'is_company_paid' => 1,'is_premi_paid' => 1,'user_approval_id'=>Auth::user()->id]);
        }


        $item1 = collect(\DB::select("select * from master_sales where id=".$id))->first();

        if($item1->is_agent_paid==1 && $item1->is_company_paid==1 && $item1->is_premi_paid==1){

        DB::table('master_sales')
            ->where('id', $id)
            ->update(['wf_status_id' => 13,'is_splitted'=>true,'user_approval_id'=>Auth::user()->id]);

        }

        $txcode='';
        $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

        $prx=date('Ymd',strtotime($item->inv_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){

          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
      }else{
        $txcode=$prx.$prx_branch.$prx_company."0001";
        }

      foreach ($config as $value) {
        $idsegment=$item->segment_id;

        $id='';

        $results = \DB::select("SELECT max(id) AS id from master_tx");
        if($results){
            $id=$results[0]->id + 1;
        }else{
            $id=1;
        }


        if($value->coa_no){
            if($value->id==5){
                $txamount=$item->agent_fee_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice' => $ref_id_invoice,
                        'seq_approval' => $set_last_seq_app,

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where  coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }

            if($value->id==6){
                $txamount=$item->comp_fee_amount + $item->admin_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app


                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }
            if($value->id==7){
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $txamount=$item->ins_fee;
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->premi_amount;


                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }
            if($value->id==9){
                $txamount=$item->comp_fee_amount+$item->admin_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app,
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }

        }else{

            $coa = \DB::select("select b.coa_no from master_sales a
                join ref_bank_account b on a.afi_acc_no=b.id
                where a.id=".$item->id);

            if($value->id==8){
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $coa[0]->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $item->agent_fee_amount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);



                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $item->agent_fee_amount;
                }else{
                    $acc_os=$coa1[0]->last_os + $item->agent_fee_amount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->agent_fee_amount;

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $coa[0]->coa_no)
                ->update(['last_os' => $acc_os]);
            }


            if($value->id==10){
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                $txamount=$item->ins_fee;
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $coa[0]->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app,
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);



                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->agent_fee_amount;

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $coa[0]->coa_no)
                ->update(['last_os' => $acc_os]);
            }
        }
    }

}

function jurnal_split_payment_false($id,$ket){
    $item = collect(\DB::select("select * from master_sales where id=".$id))->first();
    $ref_id_invoice='';
    if($ket=='agent'){
             $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and invoice_id=0 and is_tax_company=false and company_id=".Auth::user()->company_id." order by id asc ");
             DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_agent_paid' => 1,'user_approval_id'=>Auth::user()->id]);
        $ref_id_invoice=0;
        }elseif($ket=='commision'){
            $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and invoice_id=1 and is_tax_company=false and company_id=".Auth::user()->company_id." order by id asc ");
            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_company_paid' => 1,'user_approval_id'=>Auth::user()->id]);
            $ref_id_invoice=1;
        }elseif ($ket=='premium') {
            // 27 DAN 31
            $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and invoice_id=2 and is_tax_company=false and company_id=".Auth::user()->company_id." order by id asc ");
            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_premi_paid' => 1,'user_approval_id'=>Auth::user()->id]);
        $ref_id_invoice=2;
        }else{
            $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and is_tax_company=false and company_id=".Auth::user()->company_id." order by id asc ");
        }

        $item1 = collect(\DB::select("select * from master_sales where id=".$id))->first();

        if($item1->is_agent_paid==1 && $item1->is_company_paid==1 && $item1->is_premi_paid==1){

        DB::table('master_sales')
            ->where('id', $id)
            ->update(['wf_status_id' => 13,'is_splitted'=>true,'user_approval_id'=>Auth::user()->id]);

        }

    $txcode='';
        $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

        $prx=date('Ymd',strtotime($item->inv_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){

          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
      }else{
        $txcode=$prx.$prx_branch.$prx_company."0001";
        }

    foreach ($config as $value) {
        $id='';
        $results = \DB::select("SELECT max(id) AS id from master_tx");
        if($results){
            $id=$results[0]->id + 1;
        }else{
            $id=1;
        }

        if($value->coa_no){
            if($value->id==25){
                $txamount=$item->agent_fee_amount - $item->tax_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }

            if($value->id==26){
                $txamount=$item->comp_fee_amount+$item->admin_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;

                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }

            if($value->id==27){
                $txamount=$item->ins_fee;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }
            if($value->id==28){
                $txamount=$item->tax_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;

                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }

            if($value->id==30){
                $txamount=$item->comp_fee_amount+$item->admin_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }

            if($value->id==32){
                $txamount=$item->tax_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$value->coa_no."'");
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."' and company_id=".Auth::user()->company_id." and branch_id=".Auth::user()->branch_id);
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                 ->where('branch_id', Auth::user()->branch_id)
                ->where('company_id', Auth::user()->company_id)
                ->update(['last_os' => $acc_os]);
            }


        }else{

            $coa = \DB::select("select b.coa_no from master_sales a
                join ref_bank_account b on a.afi_acc_no=b.id
                where a.id=".$item->id);

            if($value->id==29){
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                $txamount=$item->agent_fee_amount - $item->tax_amount;
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $coa[0]->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);



                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->agent_fee_amount;

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $coa[0]->coa_no)
                ->update(['last_os' => $acc_os]);
            }


            if($value->id==31){
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                $txamount=$item->ins_fee;
                $last_seq_app = collect(\DB::select("SELECT max(seq_approval::int) as max_id FROM master_tx"))->first();
                $set_last_seq_app = $last_seq_app->max_id+1;

                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $coa[0]->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $item->segment_id,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id,
                        'ref_id_invoice'=>$ref_id_invoice,
                        'seq_approval' => $set_last_seq_app
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where branch_id = ".Auth::user()->branch_id." and coa_no='".$coa[0]->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);



                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->agent_fee_amount;

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('coa_no', $coa[0]->coa_no)
                ->update(['last_os' => $acc_os]);
            }
        }
    }

}

public function endorse_baru(Request $request)
    {
        $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where is_active=true');
        $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where is_active=true');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');

        $ref_underwriter = \DB::select('SELECT * FROM ref_underwriter where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');

        // Tambahan
        $ref_valuta = \DB::select('SELECT * FROM ref_valuta order by id asc');
        $param['polis_no'] = \DB::select("SELECT polis_no FROM master_sales
        where invoice_type_id = 0 and
        (wf_status_id = 9 and paid_status_id = 0) or 
        (wf_status_id = 13 and paid_status_id = 1) or
        (wf_status_id NOT IN (9, 16, 17, 18) and paid_status_id = 1)
        ");

        $param['master_sales_polis'] = \DB::select('SELECT a.id as id,mcv.cf_no 
        FROM master_sales_polis a
        join master_cf_vehicle mcv on mcv.id = a.coc_id  where invoice_type_id = 1  
        and a.id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null) and
        (a.wf_status_id = 9 and a.paid_status_id = 0) or 
        (a.wf_status_id = 13 and a.paid_status_id = 1) or
        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');

        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;
        $param['ref_underwriter']=$ref_underwriter;
        $param['ref_valuta']=$ref_valuta;

        $param['type'] ='New';

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'invoice.endorse',$param);
        }else {
            return view('master.master')->nest('child', 'invoice.endorse',$param);
        }
    }

    public function installment_baru(Request $request)
    {
        $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM ref_quotation where is_active=true  order by seq asc');
        $ref_proposal = \DB::select('SELECT * FROM ref_proposal where is_active=true  order by seq asc');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true and is_parent=false order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');

        $ref_underwriter = \DB::select('SELECT * FROM ref_underwriter where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');

        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;
        $param['ref_underwriter']=$ref_underwriter;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'invoice.installment',$param);
        }else {
            return view('master.master')->nest('child', 'invoice.installment',$param);
        }
    }

     public function get_polis(Request $request){
        $dbcr = \DB::select("SELECT *  FROM master_sales where polis_no='".$request->get('id')."' and invoice_type_id = 0");
        return json_encode($dbcr);
      }

    public function rev_split(Request $request){
        $id=$request->get('id');
        $ket=$request->get('type');
        //$item = collect(\DB::select("select * from master_sales where id=".$id))->first();

        if($ket=='agent'){
            DB::table('master_sales')
            ->where('id', $id)
            ->update(['is_agent_paid' => 2]);

        }elseif($ket=='commision'){
             DB::table('master_sales')
            ->where('id', $id)
            ->update(['is_company_paid' => 2]);

        }else{
             DB::table('master_sales')
            ->where('id', $id)
            ->update(['is_premi_paid' => 2]);

        }
        /*
        if($item->is_tax_company==true){
            $this->rev_split_true($id,$ket);

        }else{
            $this->rev_split_false($id,$ket);
        }
        */

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }

    public function rev_split_new(Request $request){

        foreach ($request->value as $item) {
           if($item){
                $exp=explode('_',$item);
                $id=$exp[0];
                $ket=$exp[1];
                $data = collect(\DB::select("select * from master_sales where id=".$id))->first();

                if($data->is_tax_company==true){
                  $this->rev_split_true($id,$ket);

                }else{
                 $this->rev_split_false($id,$ket);
                }
                $data = collect(\DB::select("select * from master_sales where id=".$id))->first();
                if($data->is_agent_paid==null && $data->is_company_paid==null && $data->is_premi_paid==null){

                DB::table('master_sales')
                    ->where('id', $id)
                    ->update(['wf_status_id' => 9,'user_approval_id'=>Auth::user()->id]);

                }
            }

        }


        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }

    public function rev_split_reject_new(Request $request){

        foreach ($request->value as $item) {
            if($item){
                $exp=explode('_',$item);
                $id=$exp[0];
                $ket=$exp[1];

                    if($ket=='agent'){
                        DB::table('master_sales')
                        ->where('id', $id)
                        ->update(['is_agent_paid' => 1,'wf_status_id' => 9 ]);

                    }elseif($ket=='commision'){
                         DB::table('master_sales')
                        ->where('id', $id)
                        ->update(['is_company_paid' => 1,'wf_status_id' => 9]);

                    }else{
                         DB::table('master_sales')
                        ->where('id', $id)
                        ->update(['is_premi_paid' => 1,'wf_status_id' => 9]);

                    }

            }

        }


        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }


    public function rev_split_true($id,$ket){


        // $data = \DB::select("select * from master_tx where id_rel='".$item."' and type_tx='invoice_payment' and tx_code = '".$get_tx_code->tx_code."'");

        if($ket=='agent'){
            $get_tx_code = collect(\DB::select("select * from master_tx where id_rel='".$id."' and type_tx='invoice_split' and ref_id_invoice=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id desc"))->first();

             $config = \DB::select("SELECT * from master_tx where id_rel=".$id." and tx_code = '".$get_tx_code->tx_code."' and ref_id_invoice=0 and type_tx='invoice_split' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id asc ");

             DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_agent_paid' => null]);

        }elseif($ket=='commision'){
            $get_tx_code = collect(\DB::select("select * from master_tx where id_rel='".$id."' and type_tx='invoice_split' and ref_id_invoice=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id desc"))->first();

            $config = \DB::select("SELECT * from master_tx where id_rel=".$id." and tx_code = '".$get_tx_code->tx_code."' and ref_id_invoice=1 and type_tx='invoice_split' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id asc ");

            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_company_paid' => null]);

        }else{
            $get_tx_code = collect(\DB::select("select * from master_tx where id_rel='".$id."' and type_tx='invoice_split' and ref_id_invoice=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id desc"))->first();

           $config = \DB::select("SELECT * from master_tx where id_rel=".$id." and tx_code = '".$get_tx_code->tx_code."' and ref_id_invoice=2 and type_tx='invoice_split' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id asc ");

            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_premi_paid' => null]);

        }


        foreach ($config as $dt) {
                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->update(['id_workflow' => 3]);


                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id__=$results[0]->id + 1;
                    }else{
                        $id__=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id__,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                        ]
                    );

                }
                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true and  type_tx='reversal_invoice' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $item) {

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 12]);

                   DB::table('master_memo')
                    ->where('tx_code', $item->tx_code)
                    ->update(['id_workflow' => 10]);

                    $coa = \DB::select("select last_os from master_coa where branch_id=".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->update(['acc_last' => $coa[0]->last_os]);

                        $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                        if($ref_jurnal[0]->operator_sign=='-'){
                            $acc_os=$coa[0]->last_os - $item->tx_amount;
                        }else{
                            $acc_os=$coa[0]->last_os + $item->tx_amount;
                        }


                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$item->tx_notes]);


                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', $item->coa_id)
                        ->update(['last_os' => $acc_os]);
                }

    }




    public function rev_split_false($id,$ket){


        if($ket=='agent'){
            $get_tx_code = collect(\DB::select("select * from master_tx where id_rel='".$id."' and type_tx='invoice_split' and ref_id_invoice=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id desc"))->first();

             $config = \DB::select("SELECT * from master_tx where id_rel=".$id." and tx_code = '".$get_tx_code->tx_code."' and ref_id_invoice=0 and type_tx='invoice_split' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id asc ");

             DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_agent_paid' => null]);

        }elseif($ket=='commision'){
            $get_tx_code = collect(\DB::select("select * from master_tx where id_rel='".$id."' and type_tx='invoice_split' and ref_id_invoice=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id desc"))->first();

            $config = \DB::select("SELECT * from master_tx where id_rel=".$id." and tx_code = '".$get_tx_code->tx_code."' and ref_id_invoice=1 and type_tx='invoice_split' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id asc ");

            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_company_paid' => null]);

        }else{
            $get_tx_code = collect(\DB::select("select * from master_tx where id_rel='".$id."' and type_tx='invoice_split' and ref_id_invoice=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id desc"))->first();

            $config = \DB::select("SELECT * from master_tx where id_rel=".$id." and tx_code = '".$get_tx_code->tx_code."' and ref_id_invoice=2 and type_tx='invoice_split' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." order by id asc ");

            DB::table('master_sales')
                ->where('id', $id)
                ->update(['is_premi_paid' => null]);

        }

                foreach ($config as $dt) {
                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->update(['id_workflow' => 3]);


                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id__=$results[0]->id + 1;
                    }else{
                        $id__=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id__,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                        ]
                    );

                }
                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true and  type_tx='reversal_invoice' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $item) {

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 12]);

                   DB::table('master_memo')
                    ->where('tx_code', $item->tx_code)
                    ->update(['id_workflow' => 10]);

                    $coa = \DB::select("select last_os from master_coa where branch_id=".Auth::user()->branch_id." and coa_no='".$item->coa_id."'");

                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->update(['acc_last' => $coa[0]->last_os]);

                        $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                        if($ref_jurnal[0]->operator_sign=='-'){
                            $acc_os=$coa[0]->last_os - $item->tx_amount;
                        }else{
                            $acc_os=$coa[0]->last_os + $item->tx_amount;
                        }


                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$item->tx_notes]);


                        DB::table('master_coa')
                        ->where('branch_id', Auth::user()->branch_id)
                        ->where('coa_no', $item->coa_id)
                        ->update(['last_os' => $acc_os]);
                }




    }



}
