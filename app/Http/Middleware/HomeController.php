<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index(Request $request)
    {
        return view('master.master')->nest('child', 'home');
    }

    public function coa(){
      $coa = \DB::select('SELECT coa_no,coa_name,balance_type_id FROM master_coa where is_parent=false'); 
      return json_encode($coa);

    }

    public function dbcr(Request $request)
    {
       $dbcr = \DB::select("SELECT balance_type_id  FROM master_coa where coa_no='".$request->get('id')."'"); 
       return json_encode($dbcr);
    }

    
}
