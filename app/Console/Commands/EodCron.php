<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
class EodCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eod:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");

        $this->info('Start Batch Day!');
        \Log::info("Start Batch Day!!");

        $proses = app('App\Http\Controllers\CronController')->batch_jurnal('day',date('Y-m-d H:i:s'));
        $this->info('Start Cetak BS!');
        \Log::info('Start Cetak BS!');
        $cetak_bs = app('App\Http\Controllers\CronController')->saveToFolder('bs');
        $this->info('Start Cetak PL!');
        \Log::info('Start Cetak PL!');
        $cetak_pl = app('App\Http\Controllers\CronController')->saveToFolder('pl');
        $this->info('Start Cetak CN!');
        \Log::info('Start Cetak CN!');
        $cetak_cn = app('App\Http\Controllers\CronController')->saveToFolder('cn');
        $this->info('Start Cetak COA!');
        \Log::info('Start Cetak COA!');
        $cetak_coa = app('App\Http\Controllers\CronController')->saveToFolder('coa');
    //   dD($proses);
        $this->info('eod:cron Cummand Run successfully!');
    }
}
