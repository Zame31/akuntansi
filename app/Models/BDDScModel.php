<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class BDDScModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'master_bdd_schedule';
    // public $timestamps = false;

}
