<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class BDDTxModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'master_bdd_txadd';
    public $timestamps = false;

}
