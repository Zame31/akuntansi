@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Accounting </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    {{$tittle}}</a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form {{$tittle}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">

                        <button onclick="saveData()" class="btn btn-success">Simpan</button>

                        @if ($type == 'new')
                            <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=new')" class="btn btn-success">Kembali</button>
                        @elseif($type == 'approve')
                            <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=approve')" class="btn btn-success">Kembali</button>
                        @elseif($type == 'success')
                            <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=success')" class="btn btn-success">Kembali</button>
                        @else
                            <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=new')" class="btn btn-secondary">Kembali</button>

                        @endif


                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form id="form-data" enctype="multipart/form-data">
              <input type="hidden" name="get_id" id="get_id" value="{{$get_id}}">
                <div class="row">
                    <div class="col-3">
                        <div class="row">
                            <div class="col-8">
                                <div class="form-group">
                                    <label>Prepaid Expense Name</label>
                                    <select readonly class="form-control" name="inventory_type_id" id="inventory_type_id">

                                        @php
                                        $inven_type = \DB::select("SELECT * FROM ref_bdd where coa_no = '103004' and is_active = 't' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
                                        @endphp
                                        @foreach($inven_type as $item)
                                        <option  value="{{$item->id}}">{{$item->bdd_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Tenor</label>
                                    <div class="kt-spinner kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                        <input onkeyup="convertToRupiah(this)" maxlength="2" type="text" class="form-control" id="tenor" name="tenor">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Rekening Bank</label>
                            <select class="form-control" name="afi_acc_no" id="afi_acc_no">
                            <option></option>
                                @php
                                $inven_type = \DB::select("SELECT * FROM ref_bank_account where is_active = 't' and branch_id = ".Auth::user()->branch_id);
                                @endphp
                                @foreach($inven_type as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Pilih COA Accrual </label>
                            <select class="form-control" name="coa_acc_no" id="coa_acc_no">
                            <option></option>
                                @php
                                // $inven_type = \DB::select("SELECT * FROM master_coa where coa_no in ('103004','103005','103007')");
                                $inven_type = \DB::select("SELECT * FROM master_coa where branch_id = ".Auth::user()->branch_id." and coa_type_id = 4 and is_parent = false and coa_parent_id is not null order by coa_no");

                                @endphp
                                @foreach($inven_type as $item)
                                <option value="{{$item->id}}">{{$item->coa_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-8">

                            <div class="form-group">
                                <label>Tax Amount</label>
                                <input type="text" class="form-control" maxlength="15" onkeyup="convertToRupiah(this)" value="0" id="tax_amount" name="tax_amount">
                            </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Tax Status</label>
                                    <select class="form-control" onchange="fieldTax(this.value)" name="is_tax_company" id="is_tax_company">
                                        <option value="t">Ya</option>
                                        <option value="f">Tidak</option>
                                    </select>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <label>Prepaid Expense Note</label>
                            <textarea name="inventory_desc" id="inventory_desc" class="form-control" rows="4" cols="80"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Prepaid Expense Amount</label>
                            <input type="text" class="form-control" maxlength="15" 
                            oninput="convertToRupiah(this);setMultiTotal(); $('#form-data').bootstrapValidator('revalidateField', 'purchase_amount');" id="purchase_amount" name="purchase_amount">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Amortisasi Month</label>
                                    <select class="form-control zn-date" name="month" id="month">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Amortisasi Year</label>
                                    <select class="form-control zn-date" name="year" id="year">
                                        @for ($i = date('Y'); $i >= 2015; $i--)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <button onclick="generateAmortisasi();" class="btn btn-success">Generate Schedule</button>

                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Apakah ada transaksi tambahan ?</label>
                                    <select onchange="setMulti(this.value)" class="form-control" name="tambahan" id="tambahan">
                                        <option value="0">Tidak</option>
                                        <option value="1">Ya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-9">


                        <div class="kt-portlet scrollStyle" style="height: 500px; overflow: auto;">
                            <div class="kt-portlet__body">
                                <h5 class="zn-head-line-success" style="margin-bottom:40px;">Biaya Ditangguhkan Schedule</h5>
                                <div class="row"
                                    style="margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                    <div class="col-3">
                                        <h6>Month</h6>
                                    </div>
                                    <div class="col-2">
                                        <h6>Year</h6>
                                    </div>
                                    <div class="col-3 text-right">
                                        <h6>Amortisasi Amount </h6>
                                    </div>
                                    <div class="col-4 text-right">
                                        <h6>Outstanding</h6>
                                    </div>
                                </div>
                                <div class="row" id="dataAmotisasi">
                                    @if ($type == 'edit')
                                        @foreach ($mc as $key => $v)

                                        <div class="col-3">
                                                <div class="form-group">
                                                    <select class="form-control zn-date" name="month[]">
                                                        <option {{($v->month == '1' ? "selected":"")}} value="1">January</option>
                                                        <option {{($v->month == '2' ? "selected":"")}} value="2">February</option>
                                                        <option {{($v->month == '3' ? "selected":"")}} value="3">March</option>
                                                        <option {{($v->month == '4' ? "selected":"")}} value="4">April</option>
                                                        <option {{($v->month == '5' ? "selected":"")}} value="5">Mey</option>
                                                        <option {{($v->month == '6' ? "selected":"")}} value="6">June</option>
                                                        <option {{($v->month == '7' ? "selected":"")}} value="7">July</option>
                                                        <option {{($v->month == '8' ? "selected":"")}} value="8">August</option>
                                                        <option {{($v->month == '9' ? "selected":"")}} value="9">September</option>
                                                        <option {{($v->month == '10' ? "selected":"")}} value="10">October</option>
                                                        <option {{($v->month == '11' ? "selected":"")}} value="11">November</option>
                                                        <option {{($v->month == '12' ? "selected":"")}} value="12">Desember</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <select class="form-control zn-date" name="year[]">
                                                      @for ($i=$mc_y_min->year_min; $i <= $mc_y_max->year_max; $i++)
                                                        <option {{($v->year == $i ? "selected":"")}} value="{{$i}}">{{$i}}</option>
                                                      @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                            <input style="text-align:right;" value="{{number_format($v->amor_amount,0,",",".")}}" id="amor{{$key+1}}" name="amor[]" placeholder="0"
                                                    class="form-control txt" onkeyup="convertToRupiah(this); reGenerateAmortisasi();" type="text">
                                            </div>
                                            <div class="col-4">
                                                <input readonly style="text-align:right;" value="{{number_format($v->last_os,0,",",".")}}" id="outstanding{{$key+1}}" name="outstanding[]" placeholder="0"
                                                    class="form-control txt1" onkeyup="convertToRupiah(this)" type="text">
                                            </div>
                                        @endforeach
                                    @endif



                                </div>

                            </div>
                        </div>


                        <div id="view_multi" class="kt-portlet scrollStyle" style="height: 500px; overflow: auto;">
                            <div class="kt-portlet__body">
                                <h5 class="zn-head-line-success" style="margin-bottom:40px;">Form Jurnal Tambahan</h5>
                                <div class="row" style="margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                    <div class="col-3">
                                        <h6>Akun</h6>
                                    </div>
                                    <div class="col-4">
                                        <h6>Keterangan Transaksi</h6>
                                    </div>
                                    <div class="col-2">
                                        <h6>Debet</h6>
                                    </div>
                                    <div class="col-2">
                                        <h6>Kredit</h6>
                                    </div>
                                    <div class="col-1">
                                        <button type="button" onclick="addTambahan()" class="btn btn-success btn-elevate btn-icon"><i class="la la-plus"></i></button>
                                    </div>
                                </div>
                                @if ($type == 'edit')
                                @else
                                <div class="row mt-2">
                                    <div class="col-3">
                                        <select onchange="revalidateM(this);" class="form-control kt-select2 coa_no zn-date" name="multi_coa_no[]" id="multi_coa_no0">
                                        </select>
                                        <div class="invalid-feedback">Pilih No Akun</div>
                                    </div>
                                    <div class="col-4">
                                        <textarea  name="multi_ket[]" id="multi_ket0" class="form-control" style="height: 40px;" rows="4" cols="80"></textarea>
                                    </div>
                                    <div class="col-2">
                                        <input  style="text-align:right;" id="multi_debet0" name="multi_debet[]" onkeyup="convertToRupiahDec(this);revalidateM(this);" placeholder="0" class="form-control money "  type="text">
                                        <div class="invalid-feedback">Nominal Kredit Tidak Sesuai</div>
                                    </div>
                                    <div class="col-2">
                                        <input  style="text-align:right;" id="multi_kredit0" name="multi_kredit[]" onkeyup="convertToRupiahDec(this);revalidateM(this);" placeholder="0" class="form-control money "  type="text">
                                        <div class="invalid-feedback">Nominal Kredit Tidak Sesuai</div>
                                    </div>

                                </div>
                                @endif


                                <div id="multi_row">
                                    @php
                                        $coa = \DB::select("SELECT coa_no,coa_name,balance_type_id FROM master_coa where branch_id = ".Auth::user()->branch_id." and ((is_parent=false and coa_parent_id is not null) or coa_no in ('302','303')) order by id asc");
                                        $total_debet = 0;
                                        $total_kredit = 0;

                                        $amount_debet = 0;
                                        $amount_kredit = 0;

                                    @endphp

                                    @if ($type == 'edit')
                                        @php
                                             $count_mtx = count($mtx);
                                        @endphp

                                        @foreach ($mtx as $key => $v)
                                        @php
                                            // if ($v->tx_type_id == 0) {
                                                $total_debet += $v->debet;
                                            // }else{
                                                $total_kredit += $v->kredit;
                                            // }
                                        @endphp
                                        {{-- @if ($v->tx_type_id == 0) --}}
                                        <div id="fmulti{{ $key }}">

                                            <div class="row mt-2">
                                                <div class="col-3">
                                                    <select onchange="revalidateM(this);" class="form-control kt-select2 coa_no zn-date" name="multi_coa_no[]" id="multi_coa_no{{ $key }}">
                                                        @foreach ($coa as $c)
                                                            <option {{ ($c->coa_no == $v->acc_no) ? "selected":"" }} value="{{ $c->coa_no }}">{{ $c->coa_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Pilih No Akun</div>
                                                </div>
                                                <div class="col-4">
                                                    <textarea name="multi_ket[]" id="multi_ket{{ $key }}" class="form-control" style="height: 40px;" rows="4" cols="80">{{ $v->note }}</textarea>
                                                </div>
                                                <div class="col-2">
                                                    <input value="{{number_format($v->debet,2,",",".")}}" style="text-align:right;" id="multi_debet{{ $key }}" name="multi_debet[]" onkeyup="convertToRupiahDec(this);revalidateM(this);" placeholder="0" class="form-control money"  type="text">
                                                    <div class="invalid-feedback">Nominal Kredit Tidak Sesuai</div>
                                                </div>
                                                <div class="col-2">
                                                    <input value="{{number_format($v->kredit,2,",",".")}}" style="text-align:right;" id="multi_kredit{{ $key }}" name="multi_kredit[]" onkeyup="convertToRupiahDec(this);revalidateM(this);" placeholder="0" class="form-control money"  type="text">
                                                    <div class="invalid-feedback">Nominal Kredit Tidak Sesuai</div>
                                                </div>
                                                @if ($key > 0)
                                                <div class="col-1">
                                                    <button type="button" class="btn btn-danger btn-elevate btn-icon" onclick="removeMulti({{ $key }})"><i class="la la-trash"></i></button>
                                                </div>
                                                @endif

                                            </div>
                                        </div>
                                        {{-- @else

                                        @endif --}}
                                        
                                        @endforeach
                                    @endif
                                </div>

                                <div class="row" style="margin-top: 15px;padding-top: 15px;border-top: 1px dashed #ebedf2;">
                                    <div class="col-7">
                                        <h6>Total</h6>
                                    </div>
                                    <div class="col-2">
                                        <h6 style="text-align:right;margin-right: 15px;">
                                            <div id="multi_total" class="money">{{number_format($total_debet,2,",",".")}}</div>
                                        </h6>
                                    </div>
                                    <div class="col-2">
                                        <h6 style="text-align:right;margin-right: 15px;">
                                            <div id="multi_total_kredit" class="money">{{number_format($total_kredit,2,",",".")}}</div>
                                        </h6>
                                    </div>

                                    <div class="col-1">

                                    </div>
                                </div>

                            </div>
                        </div>




                    </div>
                </div>
            </form>

        </div>
        <div class="kt-portlet__head" style="border-top: 1px solid #e2e5ec;">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">

                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">

                        <button onclick="saveData()" class="btn btn-success">Simpan</button>

                        @if ($type == 'new')
                        <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=new')" class="btn btn-success">Kembali</button>
                    @elseif($type == 'approve')
                        <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=approve')" class="btn btn-success">Kembali</button>
                    @elseif($type == 'success')
                        <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=success')" class="btn btn-success">Kembali</button>
                    @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#view_multi').hide();

    function fieldTax(type) {
        if (type == 'f') {
            $('#tax_amount').attr('readonly', true);
            $('#tax_amount').val(0);
        } else {
            $('#tax_amount').attr('readonly', false);

        }

        $('#form-data').bootstrapValidator('revalidateField', 'tax_amount');

    }

    var countMulti = 1;
    var type = '{{$type}}';
    @if ($type == 'edit')

    if (type == 'edit') {
        $('#inventory_type_id').val('{{$mi->bdd_id}}');
        $('#tenor').val('{{$mi->tenor}}');
        $('#inventory_desc').val('{{$mi->bdd_note}}');
        $('#purchase_amount').val(numFormat('{{$mi->bdd_amount}}'));
        $('#afi_acc_no').val('{{$mi->afi_acc_no}}');
        $('#coa_acc_no').val('{{$mi->coa_acc_no}}');
        $('#tax_amount').val('{{$mi->tax_amount}}');
        $('#tambahan').val('{{$mi->tambahan}}');

        if ('{{$mi->is_tax_company}}' == '1') {
            $('#is_tax_company').val('t');
            fieldTax('t');
        }else{
            $('#is_tax_company').val('f');
            fieldTax('f');
        }

        if ('{{$mi->tambahan}}' == '1') {
            $('#view_multi').show();
        } else {
            $('#view_multi').hide();
        }
        var countMulti = '{{ $count_mtx }}';
    }
    @endif
</script>
@include('biaya_ditangguhkan.action')
@stop
