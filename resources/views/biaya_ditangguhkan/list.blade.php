@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Prepaid Expense </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{$tittle}} </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            {{$tittle}}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">


                            @if ($type == 'new')
                                <a href="#" onclick="gActAll('sendApproval','/kirimAprovalBdd?type=semua','{{ route('data.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a>
                                <a href="#" onclick="gActSelected('sendApproval','/kirimAprovalBdd?type=satu','{{ route('data.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a>
                                <a href="#" onclick="gActAll('delete','/hapusAprovalBdd?type=semua','{{ route('data.biaya_ditangguhkan') }}');" class="btn btn-pill btn-danger" style="color: white;">Delete All</a>
                                <a href="#" onclick="gActSelected('delete','/hapusAprovalBdd?type=satu','{{ route('data.biaya_ditangguhkan') }}');" class="btn btn-pill btn-danger" style="color: white;">Delete Selected</a>
                                <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.form','create') }}')" class="btn btn-pill btn-success">Tambah Data</button>
                            @elseif($type == 'approve')
                                <a href="#" onclick="gActAll('giveApproval','/giveAprovalBdd?type=semua','{{ route('data_approve.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval All</a>
                                <a href="#" onclick="gActSelected('giveApproval','/giveAprovalBdd?type=satu','{{ route('data_approve.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Selected</a>
                                <a href="#" onclick="gActAll('reject','/rejectAprovalBdd?type=semua','{{ route('data_approve.biaya_ditangguhkan') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject All</a>
                                <a href="#" onclick="gActSelected('reject','/rejectAprovalBdd?type=satu','{{ route('data_approve.biaya_ditangguhkan') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Selected</a>
                            @elseif($type == 'success')
                            
                            <a href="#" onclick="cetakData('all',`bdd`);" class="btn btn-pill btn-danger" style="color: white;">Cetak PDF</a>
                            <a href="#" onclick="cetakDataExcel('all',`bdd`);" class="btn btn-pill btn-success" style="color: white;">Cetak Excel</a>
                            
                            @if (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 9)
                            
                                <a href="#" onclick="gActAll('delete','/hapusAprovalBddRev?type=semua','{{ route('data_success.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete All</a>
                                <a href="#" onclick="gActSelected('delete','/hapusAprovalBddRev?type=satu','{{ route('data_success.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete Selected</a>
                            @endif
                            @elseif($type == 'approve_reversal')
                                <a href="#" onclick="gActAll('deleteRev','/giveHapusAprovalBddRev?type=semua','{{ route('data_reversal.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete All</a>
                                <a href="#" onclick="gActSelected('deleteRev','/giveHapusAprovalBddRev?type=satu','{{ route('data_reversal.biaya_ditangguhkan') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete Selected</a>
                                <a href="#" onclick="gActAll('reject','/rejectHapusAprovalBddRev?type=semua','{{ route('data_reversal.biaya_ditangguhkan') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete All</a>
                                <a href="#" onclick="gActSelected('reject','/rejectHapusAprovalBddRev?type=satu','{{ route('data_reversal.biaya_ditangguhkan') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete Selected</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label></th>
                                    <th width="30px">Action</th>
                                    <th>Prepaid Expense Name</th>
                                    <th>Prepaid Expense Note</th>
                                    <th>Prepaid Expense Amount</th>
                                    <th>Amor Amount</th>
                                    @if ($type == 'success')
                                    <th>Paid Amount</th>
                                    <th>Status Paid</th>
                                    <th>Coa Name</th>
                                    @endif
                                    <th>Other Amount</th>
                                    <th>Branch Code</th>
                                    {{-- <th>last OS</th> --}}
                                    {{-- <th>Status</th> --}}
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                @if ($type == 'success')
                                <th></th>
                                <th></th>
                                <th></th>
                                @endif
                            </tfoot>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('biaya_ditangguhkan.action')
@stop
