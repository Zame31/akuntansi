@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Quotation Slip </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Create </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-product">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Quotation Form
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <a onclick="loadNewPage('{{ route('quotation_slip') }}');" class="btn btn-clean kt-margin-r-10" style="cursor: pointer;">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body pb-0">
                <div class="form-group row mb-2">
                    <label class="col-2 col-form-label">Select Type of Insurance </label>
                    <div class="col-8">
                        <select class="form-control kt-select2 init-select2" name="product" id="product">
                            <option value="1000" disabled selected>Select Type Of Insurace</option>
                            @forelse ($products as $item)
                                <option value="{{ $item->id }}">{{ $item->definition }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label"></label>
                    <div class="col-8">
                        <button type="button" class="btn btn-md btn-outline-primary kt-margin-t-5 kt-margin-b-5" onclick="show_form()">Submit</button>
                    </div>
                </div>
              </div>
        </div>

    </div>

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-form" style="display: none;">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Quotation Form</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a onclick="show_form_product()" class="btn btn-clean kt-margin-r-10" style="cursor: pointer;">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success" onclick="store();">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <form class="kt-form" id="form-data">
                            <div class="row">
                                <div class="col-xl-2"></div>
                                <div class="col-xl-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg" id="product_name"></h3>
                                            <div class="row">
                                                <label class="col-3 col-form-label control-label">Form / Wording</label>
                                                <div class="form-group col-9">
                                                    <input class="form-control" type="text" name="form_wording" id="form_wording" value="Polis Standard Asuransi Kendaraan Bermotor Indonesia">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Select Customer</label>
                                                <div class="form-group col-9">
                                                    <select class="form-control kt-select2 init-select2" name="customer" id="customer" onchange="get_address_customer(this);">
                                                        <option value="1000" disabled selected>Select Customer</option>
                                                        @forelse ($customer as $item)
                                                            <option value="{{ $item->id }}" @if ( $item->id == 1) selected @endif >{{ $item->full_name }}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Customer Address</label>
                                                <div class="form-group col-9">
                                                    <div id="container-cust-addr">
                                                        <textarea class="form-control" name="customer_address" id="customer_address" rows="3">Principally consisting of  Business & private   and all other activities related or incidental thereto and any other occupation in which the Insured may become engaged.</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Period of Insurance</label>
                                                <div class="form-group col-9">
                                                    <textarea class="form-control" name="periode_of_insurance" id="periode_of_insurance" rows="3">TBA (Both dates are inclusive until 12:00 noon local time) </textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Number of Insured</label>
                                                <div class="form-group col-9">
                                                    <input class="form-control" type="text" name="number_of_insured" id="number_of_insured" value="520,550,000.00" onkeypress="return hanyaAngka(event)" maxlength="80">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                                    <div class="kt-section">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg">Vehicle Details</h3>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Make / Type</label>
                                                <div class="form-group col-9">
                                                    <input class="form-control" type="text" name="type" id="type" value="All New Fortuner 2.4 VRZ A/T 4X2 22">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Colour</label>
                                                <div class="form-group col-9">
                                                    <input class="form-control" type="text" name="colour" id="colour" value="Attitude Black Mica">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Year Built</label>
                                                <div class="form-group col-9">
                                                    <input class="form-control" type="text" onkeydown="event.preventDefault()" name="year_built" id="year_built" value="Brand New 2020">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Police Reg No</label>
                                                <div class="form-group  col-9">
                                                    <input class="form-control" type="text" name="police_reg_no" id="police_reg_no" value="TBA">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Chassis No</label>
                                                <div class="form-group  col-9">
                                                    <input class="form-control" type="text" name="chassis_no" id="chassis_no" value="MHFGB8GS0L0908164">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Engine No</label>
                                                <div class="form-group  col-9">
                                                    <input class="form-control" type="text" name="engine_no" id="engine_no" value="2GDC671410">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Accessories</label>
                                                <div class="form-group  col-9">
                                                    <input class="form-control" type="text" name="accessories" id="accessories" value="Kaca Film VKOOL">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Total Sum Insured</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" onkeyup="calculate('comprehensive')" type="text" name="total_sum_insured" id="total_sum_insured" placeholder="0" value="520,550,000.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Coverage</label>
                                                <div class="form-group col-9">
                                                    <div class="container-texteditor">
                                                        <h5 class="tittle"> Konten </h5>
                                                        <textarea class="form-control" id="coverage_data" rows="10">
                                                            Comprehensive Cover:
                                                                Loss of or damage to the insured vehicle(s) caused by collision, impact, overturning, running off the road, including loss or damage caused by malicious act, fire, theft, riot, strike, civil commotion, terrorism, sabotage and Act of God (earthquake, volcanic eruption and flood) and other geological and meteorological phenomenon.

                                                                Including:
                                                                -Third Party Liability (TPL)  maximum limit:  IDR 10,000,000.00 for motor vehicle
                                                                -Personal Accident (death and total disablement) to unnamed passenger & driver   IDR. 10,000,000.00/person including its Driver for motor vehicle
                                                                -Medical Expenses to unnamed passenger & driver:  10% of PA Limit for motor vehicle
                                                                -Act of God (Earthquake, Volcanic Eruption, Typhoon, Windstorm and Flood)
                                                                -Strike, Riot, Civil Commotion, Terrorism and Sabotage(SRCCTS)
                                                                -Towing Fee : 0.5% Of Sum Insured
                                                        </textarea>
                                                    </div>
                                                    <input type="hidden" name="coverage_content" id="coverage_content" value="">
                                                    <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Coverage', 'coverage');"><i class="fa fa-pencil-alt"></i> Fill Coverage</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Main Exclusions</label>
                                                <div class="form-group col-9">
                                                    <div class="container-texteditor">
                                                        <h5 class="tittle"> Konten </h5>
                                                        <textarea class="form-control" id="main_exclusions_data" rows="10">
                                                            This policy does not cover any loss, damage, expense incurred in Motor Vehicle and or third party legal liability, caused by:
                                                                1	the motor Vehicle used for:
                                                                -	towing or pushing other vehicle or objects, giving driving lesson;
                                                                -	participating in contests, trainings, channelling of skill or speed hobby, carnivals, campaigns, demonstrations;
                                                                -	committing crimes;
                                                                2	embezzlement, fraud, hypnotic and the like;
                                                                3	malicious acts committed by: the Insured; spouses, children, parents or siblings of the Insured; people instructed by the Insured, working for the Insured; people with the knowledge or with the consent of the Insured; people living with the Insured; management, share holders, commissioners or employees if the Insured is a legal entity;
                                                                4	overload of vehicle capacity set out by the manufacturer.
                                                                5	Deliberate action of the Insured and or driver;
                                                                6	Driven by a person who has no Driving License (SIM);
                                                                7	Driven under the influence of alcohol, drugs
                                                                8	Driven forcibly when the Vehicle is technically out of order or not roadworthy;
                                                                9	Non standards equipments which are not mentioned in the Policy.
                                                                10	nuclear reaction, nuclear radiation, ionization, fusion, fission or radioactive contamination, irrespective of whether or not it occurs inside or outside Motor Vehicle and or the Interest Insured.

                                                        </textarea>
                                                    </div>
                                                    <input type="hidden" name="main_exclusions_content" id="main_exclusions_content" value="">
                                                    <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Main Exclusions', 'main_exclusions');"><i class="fa fa-pencil-alt"></i> Fill Main Exclusions</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Deductible</label>
                                                <div class="form-group col-9">
                                                    <div class="container-texteditor">
                                                        <h5 class="tittle"> Konten </h5>
                                                        <textarea class="form-control" id="deductible_data" rows="10">
                                                            Material Damage
                                                            IDR. 300,000,- each and every accident
                                                            AOG & SRCCTS
                                                            10% of claim, min IDR. 500.000,- each and every accident for motor vehicle
                                                            TLO by Theft
                                                            5% of SI
                                                            Water Hammer:
                                                            25% of claim, min IDR 25,000,000
                                                        </textarea>
                                                    </div>
                                                    <input type="hidden" name="deductible_content" id="deductible_content" value="">
                                                    <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Deductible', 'deductible');"><i class="fa fa-pencil-alt"></i> Fill Deductible </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Clauses</label>
                                                <div class="form-group col-9">
                                                    <div class="container-texteditor">
                                                        <h5 class="tittle"> Konten </h5>
                                                        <textarea class="form-control" id="clauses_data" rows="10">
                                                            1.   Automatic Addition and Deletion Clause (30 days)
                                                            2.	Act of God Clause including water hammer
                                                            3.	Cancellation Clause (30 days)
                                                            4.	Constructive Total Loss (75%)
                                                            5.	Loss Notification Clause (14 days)
                                                            6.	Sister Car Clause
                                                            7.	Strike, Riot, Civil Commotion, Terrorism & Sabotage Clause
                                                            8.	Transit Extension (inter island) in Republic of Indonesia Clause
                                                            9.	85 % Condition of Average Clause
                                                            10.	Non Standard Accessories (10% Of Sum Insured)
                                                        </textarea>
                                                    </div>
                                                    <input type="hidden" name="clauses_content" id="clauses_content" value="">
                                                    <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Clauses', 'clauses');"><i class="fa fa-pencil-alt"></i> Fill Clauses </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Rate</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <input class="form-control text-right" type="text" onkeyup="calculate('comprehensive');" name="rate" id="rate" aria-describedby="basic-addon2" value="1.20">
                                                        <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Additional Rate</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <input class="form-control text-right" type="text" name="additional_rate" onkeyup="calculate('flood')" id="additional_rate" aria-describedby="basic-addon2" value="0.3">
                                                        <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Third Party Limit</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control text-right money" onkeyup="calculate('tpl_limit')" type="text" name="third_party_limit" id="third_party_limit" value="10,000,000.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Personal Accident</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control text-right money" type="text" onkeyup="calculate('pa_driver')" name="personal_accident" id="personal_accident" value="10,000,000.00">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Discount</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <input class="form-control text-right" type="text" onkeyup="calculate('discount')" name="discount" id="discount" aria-describedby="basic-addon2" value="10%">
                                                        <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                                    <div class="kt-section">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg">Premium Calculation</h3>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Comprehensive</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="comprehensive" id="comprehensive" disabled placeholder="0" value="6,246,600.00">
                                                    </div>
                                                    <span class="form-text text-muted text-right">Total Sum Insured x Rate</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Flood, EQ,SRCC, TS</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="flood" id="flood" disabled placeholder="0" value="1,561,650.00">
                                                    </div>
                                                    <span class="form-text text-muted text-right">Total Sum Insured x Additional Rate</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">TPL Limit</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="tpl_limit" id="tpl_limit" disabled placeholder="0" value="100,000.00">
                                                    </div>
                                                    <span class="form-text text-muted text-right">Third Party Limit x 1%</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">PA Driver</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="pa_driver" id="pa_driver" disabled placeholder="0" value="50,000.00 ">
                                                    </div>
                                                    <span class="form-text text-muted text-right">Personal Accident x 0,5%</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">PA Passenger</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="pa_passenger" id="pa_passenger" disabled placeholder="0" value="40,000.00">
                                                    </div>
                                                    <span class="form-text text-muted text-right">Personal Accident x 0,1% x 4</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Sub Total</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="sub_total" id="sub_total" disabled placeholder="0" value="7,998,250.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Discount Amount</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="discount_amount" id="discount_amount" disabled placeholder="0" value="799,825.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Grand Total</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="grand_total" id="grand_total" disabled placeholder="0" value="7,198,425.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Security</label>
                                                <div class="form-group col-9">
                                                    <select class="form-control kt-select2 init-select2" name="security" id="security">
                                                        <option value="1000" disabled selected>Select Security</option>
                                                        @forelse ($refUnderwriter as $item)
                                                            <option value="{{ $item->id }}" @if($item->id == 1) selected @endif>{{ $item->definition }}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                                    <div class="kt-section kt-section--last">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg">Others:</h3>
                                            <div class="row">
                                                <label class="col-3 col-form-label">QS No</label>
                                                <div class="form-group col-9">
                                                    <input class="form-control" type="text" name="qs_no" id="qs_no" value="{{ $qsNo }}" disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Gross Premium</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="gross_premium" id="gross_premium" disabled placeholder="0" value="7,998,250.00">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Nett Premium</label>
                                                <div class="form-group col-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                                        <input class="form-control money text-right" type="text" name="nett_premium" id="nett_premium" disabled placeholder="0" value="7,198,425.00">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->


    <!-- end:: Subheader -->

</div>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>

@include('quotation_slip.action')
@include('quotation_slip.modal.clauses')
@include('quotation_slip.modal.coverage')
@include('quotation_slip.modal.deductible')
@include('quotation_slip.modal.main_exclusions')
<script type="text/javascript">

    function showModalTextEditor(tittle_modal, class_name) {
        $("#" + class_name + ' .modal-title').html(tittle_modal);
        $("#" + class_name).modal('show');
        $('.' + class_name).summernote({
            focus: true,
            height: 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['view', ['fullscreen', 'help']],
            ],
        });
    }

    function save(class_name) {
        var markup = $('.' + class_name).summernote('code');
        $('.' + class_name).summernote('destroy');
        console.log(markup);
        $("#" + class_name).modal('hide');
        $("#" + class_name + '_data').val(markup);
        // $("#" + class_name + '_data').html(markup);
        $("#" + class_name + '_content').val(markup);
        $("#form-data").bootstrapValidator('revalidateField', $("#" + class_name + '_content').prop('name'));
    }

    function calculate(type) {
        var comprehensive, flood, tpl_limit, pa_driver, pa_passenger, sub_total,
            sub_disc_10, grand_total;

        var total_sum_insured = $("#total_sum_insured").val();
        var additional_rate = $("#additional_rate").val();
        var rate = $("#rate").val();
        var third_party_limit = $("#third_party_limit").val();
        var personal_accident = $("#personal_accident").val();
        var discount = $("#discount").val();

        // Calculate Comprehensive
        if ( total_sum_insured ) {
            // Contoh memakai dua digit angka
            total_sum_insured = total_sum_insured.split('.');
            total_sum_insured = parseFloat(total_sum_insured[0].replace(/\,/g, ''));

            // total_sum_insured = parseFloat(total_sum_insured.replace(/\,/g, ''));
        } else {
            total_sum_insured = 0;
        }

        if ( rate ) {
            rate = parseFloat($("#rate").val()) / 100;
        } else {
            rate = 0;
        }


        comprehensive = total_sum_insured * rate;
        $("#comprehensive").val(comprehensive).trigger('mask.maskMoney');

        if ( additional_rate ) {
            rate = parseFloat($("#additional_rate").val()) / 100;
        } else {
            additional_rate = 0;
        }

        flood = total_sum_insured * additional_rate;
        $("#flood").val(flood).trigger('mask.maskMoney');

        // Calculate Flood
        total_sum_insured = $("#total_sum_insured").val();
        if ( total_sum_insured ) {
            total_sum_insured = total_sum_insured.split('.');
            total_sum_insured = parseFloat(total_sum_insured[0].replace(/\,/g, ''));
        } else {
            total_sum_insured = 0;
        }

        if ( additional_rate ) {
            additional_rate = parseFloat($("#additional_rate").val()) / 100;
        } else {
            additional_rate = 0;
        }

        flood = total_sum_insured * additional_rate;
        $("#flood").val(flood).trigger('mask.maskMoney');

        // Calculate Tpl Limit
        if ( third_party_limit ) {
            third_party_limit = third_party_limit.split('.');
            third_party_limit = parseFloat(third_party_limit[0].replace(/\,/g, ''));
        } else {
            third_party_limit = 0;
        }

        tpl_limit = third_party_limit * 0.01; // third_party * 1%
        $("#tpl_limit").val(tpl_limit).trigger('mask.maskMoney');

        // Calculate PA Driver
        if ( personal_accident ) {
            personal_accident = personal_accident.split('.');
            personal_accident = parseFloat(personal_accident[0].replace(/\,/g, ''));
        } else {
            personal_accident = 0;
        }

        pa_driver = personal_accident * 0.005; // personal_accident * 0.5%
        $("#pa_driver").val(pa_driver).trigger('mask.maskMoney');

        pa_passenger = personal_accident * 0.001 * 4; // personal_accident * 0.5%
        $("#pa_passenger").val(pa_passenger).trigger('mask.maskMoney');

        // Calculate Discount
        if ( discount ) {
            discount = parseFloat($("#discount").val()) / 100;
        } else {
            discount = 0;
        }

        // Calculate Total
        sub_total = comprehensive + flood + tpl_limit + pa_driver + pa_passenger;
        sub_disc_10 = sub_total * discount;
        grand_total = sub_total - sub_disc_10;

        $("#sub_total").val(sub_total).trigger('mask.maskMoney');
        $("#discount_amount").val(sub_disc_10).trigger('mask.maskMoney');
        $("#grand_total").val(grand_total).trigger('mask.maskMoney');

        
    } // end function

    function closeModal() {
        $("#kt_summernote_modal").modal('hide');
    }
</script>
@stop
