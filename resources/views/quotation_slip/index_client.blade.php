@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Marketing </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Quotation Slip Client</a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Quotation Slip Client List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <a style="cursor: pointer;color:white;" onclick="approve('all','/quotation_slip/approve?type=all')" class="btn btn-pill btn-success mr-2" >Approval All</a>
                            <a style="cursor: pointer;color:white;" onclick="approve('selected','/quotation_slip/approve?type=selected')" class="btn btn-pill btn-info">Approval Selected</a>
                            <a style="cursor: pointer;color:white;" onclick="reject('all','/quotation_slip/reject?type=all');" class="btn btn-pill btn-danger" >Reject All</a>
                            <a style="cursor: pointer;color:white;" onclick="reject('selected','/quotation_slip/reject?type=selected');" class="btn btn-pill btn-danger" >Reject Selected</a>
                            <button onclick="loadNewPage('{{ route('quotation_slip.create', ['type' => 'client']) }}');" class="btn btn-pill btn-primary">Add New QS </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="table-responsive">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                        <thead>
                            <tr>
                                <th width="30px" class="text-center">
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                        <span></span>
                                    </label>
                                </th>
                                <th width="30px" class="text-center">Action</th>
                                <th class="text-center">No</th>
                                <th class="text-center">ID</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">QS No</th>
                                <th class="text-center">Change Counter</th>
                                <th class="text-center">Customer Name</th>
                                <th class="text-center">Type of Insurance</th>
                                <th class="text-center">Valuta</th>
                                <th class="text-center">TSI</th>
                                <th class="text-center">Gross Premium</th>
                                <th class="text-center">Premium After Fleet</th>
                                <th class="text-center">Premium Final</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data)
                                @foreach($data as $i => $item)
                                    <tr>
                                        <td class="text-center">
                                            @if ( $item->wf_status_client_id == 2 )
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input type="checkbox" value="{{ $item->id }}" class="kt-group-checkable">
                                                    <span></span>
                                                </label>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td align="center">
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">

                                                    @if ( $item->wf_status_client_id != 7 && $item->wf_status_client_id != 2)
                                                        <a class="dropdown-item" style="cursor:pointer;" onclick="loadNewPage('{{ route('quotation_slip.show', ['id' => $item->id, 'type' => 'client']) }}')">
                                                            <i class="la la-edit"></i> Edit
                                                        </a>
                                                    @endif

                                                    @if ( $item->wf_status_client_id == 2 )
                                                        <a class="dropdown-item" style="cursor:pointer;" onclick="approve('one','/quotation_slip/approve?type=one', '{{ $item->id }}')">
                                                            <i class="la la-check"></i> Approve
                                                        </a>
                                                        <a class="dropdown-item" style="cursor:pointer;" onclick="reject('one','/quotation_slip/reject?type=selected', '{{ $item->id }}');">
                                                            <i class="la la-close"></i> Reject
                                                        </a>
                                                    @endif

                                                    @if ( $item->wf_status_client_id == 3 )
                                                        {{-- Hanya posisi data yang sudah di approve (QS),  bisa di reject --}}
                                                        {{-- <a class="dropdown-item" style="cursor:pointer;" onclick="reject('one','/quotation_slip/reject?type=selected', '{{ $item->id }}');">
                                                            <i class="la la-close"></i> Reject
                                                        </a> --}}
                                                    @endif


                                                    {{-- @if ( $item->is_active )
                                                        <a class="dropdown-item" style="cursor:pointer;" onclick="setCancel('{{ $item->id }}', 't')">
                                                            <i class="la la-close"></i> Batalkan
                                                        </a>
                                                    @else
                                                        <a class="cursor-pointer" style="cursor:pointer;" onclick="setCancel('{{ $item->id }}', 'f')">
                                                            <i class="la la-check"></i> Aktifkan
                                                        </a>
                                                    @endif --}}


                                                    <a class="dropdown-item" style="cursor:pointer;" onclick="print('{{ route('quotation_slip.export', ['id' => $item->id]) }}')">
                                                        <i class="la la-file-pdf-o"></i> Cetak Surat (PDF)
                                                    </a>
                                                    @php
                                                        $urlWord = 'insurance/printword/qs/' . $item->id;
                                                        $urlExcel = 'insurance/printexcel/qs/' . $item->id;
                                                    @endphp
                                                    <a class="dropdown-item text-dark" href="{{ url($urlWord) }}">
                                                        <i class="la la-file-word-o"></i> Cetak Surat (Word)
                                                    </a>
                                                    <a class="dropdown-item text-dark" href="{{ url($urlExcel) }}">
                                                        <i class="la la-file-excel-o"></i> Cetak Lampiran (Excel)
                                                    </a>

                                                    {{-- <a class="dropdown-item text-dark" href="insurance/printword/qs/{{$item->id}}">
                                                        <i class="la la-file-word-o"></i> Cetak Surat (Word)
                                                    </a>
                                                    <a class="dropdown-item text-dark" href="insurance/printexcel/qs/{{$item->id}}">
                                                        <i class="la la-file-excel-o"></i> Cetak Lampiran (Excel)
                                                    </a> --}}
                                                </div>
                                            </div>
                                        </td>
                                        <td align="center"> {{ ++$i }} </td>
                                        <td align="center"> {{ $item->id }} </td>
                                        <td align="center">

                                            @if ( $item->wf_status_client_id == 2 )
                                                <span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill">Waiting Approve</span>
                                            @elseif ( $item->wf_status_client_id == 7)
                                                <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Reject</span>
                                            @else
                                                <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                            @endif

                                        </td>
                                        <td align="center"> {{ $item->qs_no }} </td>
                                        <td align="right">

                                            @if ( is_null($item->count_edit) )
                                                0
                                            @else
                                                {{ $item->count_edit }}
                                            @endif
                                        </td>
                                        <td> {{ $item->full_name }} </td>
                                        <td> {{ $item->definition }}</td>
                                        <td> {{ $item->mata_uang }}</td>
                                        <td align="right"> {{ number_format($item->total_sum_insured,2,',','.')}} </td>
                                        <td align="right"> {{ number_format($item->gross_premium,2,',','.')}} </td>
                                        <td align="right"> {{ number_format($item->nett_premium,2,',','.')}} </td>
                                        <td align="right"> {{ number_format($item->premium_final,2,',','.')}} </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('quotation_slip.action')
<script type="text/javascript">
    $('#example-select-all').click(function (e) {
        $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
    });

    function approve(type, action, id_apporve)
    {
        var setTittle = '';
        var setText = '';
        let value = [];


        if ( type == 'one' ) {

            setTittle = 'Approval';
            setText = 'Yakin akan menyetujui data ini?';
            value = [id_apporve];

        }else if ( type == 'all' ) {
            setTittle = 'Approval All';
            setText = 'Yakin akan menyetujui semua data ?';

            $('td input[type="checkbox"]').each(function(){
                value.push(this.value);
            });

            if ( value.length == 0 ){
                toastr.warning("Tidak dapat approve. Data tidak tersedia");
                return false;
            }

        } else {
            // Selected
            setTittle = 'Approval Selected';
            setText = 'Yakin akan menyetujui data yang dipilih ?';

            $('td input[type="checkbox"]:checked').each(function(){
                if(this.checked){
                    value.push(this.value);
                }
            });

        }

        if ( type == 'selected' ) {
            if ( value.length == 0 ) {
                toastr.warning("Minimal Pilih 1 data untuk melakukan '"+setTittle+"' ");
                return false;
            }
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        swal.fire({
            title: setTittle,
            text: setText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F8BD89",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + action,
                    data: {
                        value: value
                    },
                    dataType: 'JSON',
                    success: function (res) {
                        endLoadingPage();

                        // CEK SCHEDULE
                        if (res.rc == 0) {
                            toastr.success(setTittle+" Success");
                            location.reload();
                        }else {
                            toastr.error("Terjadi Kesalahan");
                        }
                    }
                }).done(function( res ) {
                }).fail(function(res) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });
    } // end function

    function reject(type, action, id_reject) {
        var setTittle = '';
        var setText = '';
        let value = [];

        if ( type == 'one' ) {
            setTittle = 'Reject';
            setText = 'Yakin akan menolak data ini ?';
            value = [id_reject];

        } else if ( type == 'all' ) {
            setTittle = 'Reject All';
            setText = 'Yakin akan menolak semua data ?';

            $('td input[type="checkbox"]').each(function(){
                value.push(this.value);
            });

            if ( value.length == 0 ){
                toastr.warning("Tidak dapat reject. Data tidak tersedia");
                return false;
            }

        } else {
            // Selected
            setTittle = 'Reject Selected';
            setText = 'Yakin akan menolak data yang dipilih ?';

            $('td input[type="checkbox"]:checked').each(function(){
                if(this.checked){
                    value.push(this.value);
                }
            });

        }

        if ( type == 'selected' ) {
            if ( value.length == 0 ) {
                toastr.warning("Minimal Pilih 1 data untuk melakukan '"+setTittle+"' ");
                return false;
            }
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        swal.fire({
            title: setTittle,
            text: setText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F8BD89",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                var SetText = `isi memo atau catatan dibawah ini untuk respon yang anda berikan. <br><br> <textarea name="inputMemoApprove" id="inputMemoApprove" class="form-control" rows="4" cols="80"></textarea>`;
                var SetAction = `
                    <button onclick="prosesReject('`+setTittle+`','`+type+`','`+action+`', '`+id_reject+`')" type="button"
                        class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Kirim</button>
                        <button onclick="znClose()" type="button"
                    class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Kembali</button>`;

                znIconbox("Memo / Catatan",SetText,SetAction,'warning');

                // loadingPage();
                // $.ajax({
                //     type: 'POST',
                //     url: base_url + action,
                //     data: {
                //         value: value
                //     },
                //     dataType: 'JSON',
                //     success: function (res) {
                //         endLoadingPage();

                //         // CEK SCHEDULE
                //         if (res.rc == 0) {
                //             toastr.success(setTittle+" Success");
                //             location.reload();
                //         }else {
                //             toastr.error("Terjadi Kesalahan");
                //         }
                //     }
                // }).done(function( res ) {
                // }).fail(function(res) {
                //     endLoadingPage();
                //     toastr.error("Terjadi Kesalahan");
                // });
            }
        });
    } // end function

    function prosesReject(setTittle,type, action, id_reject) {
        loadingModal();
        var SetMemo = $('#inputMemoApprove').val();
        let value = [];

        if ( type == 'one' ) {
            value = [id_reject];
        } else if ( type == 'all' ) {
            $('td input[type="checkbox"]').each(function(){
                value.push(this.value);
            });
        } else {
            // Selected
            $('td input[type="checkbox"]:checked').each(function(){
                if(this.checked){
                    value.push(this.value);
                }
            });

        }


        // if(type == 'semua'){
        //     var setData = {SetMemo: SetMemo};
        // }else {
        //     let value = [];
        //     $('input[type="checkbox"]').each(function(){
        //         if(this.checked){
        //             value.push(this.value);
        //         }
        //     });
        //     var setData = {value: value,SetMemo: SetMemo};
        // }

        var setData = {
            value: value,
            SetMemo: SetMemo
        };

        $.ajax({
            type: 'POST',
            url: base_url + action,
            data: setData,
            dataType: 'JSON',
            success: function (res) {

                KTApp.unblockPage("#modal .modal-content");

                if (res.rc == 0) {
                    toastr.success(setTittle+" Success");
                    location.reload();
                    location.reload();
                }else {
                    toastr.error("Terjadi Kesalahan");
                }
            }
        }).done(function( res ) {
        }).fail(function(res) {
            // znIconboxClose();
            // endloadingModal();
            toastr.warning("Terjadi Kesalahan, Gagal Melakukan "+setTittle);
        });
    }
</script>
@stop
