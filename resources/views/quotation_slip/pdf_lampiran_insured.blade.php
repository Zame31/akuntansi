<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>

            header {
                position: fixed;
                top: 0px;
                left: 0px;
                right: 0px;
                height: 100px;
                color: white;
                line-height: 35px;
            }

            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 0.5cm;

                color: white;
                text-align: right;
                line-height: 1.5cm;
            }

            footer .pagenum:before {
                content: counter(page);
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2cm;
                margin-left: 0cm;
                margin-right: 1cm;
                margin-bottom: 1cm;
            }

            * {
                font-family: sans-serif !important;
            }

            table.table-data,
            table.table-data tr,
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
            }

            table.table-data th {
                text-align: center;
            }

            h1.tittle {
                font-size: 18px;
                text-decoration: underline;
                margin-bottom: 0;
            }

            .container-tittle {
                text-align: center;
            }

            .container-tittle h4 {
                margin-top: 0;
                font-size: 13px;
            }

            .container-table {
                font-size: 8px !important;
            }

            .container-table table {
                border-collapse: collapse;
                border-spacing: 0;
            }

            .container-table tr,
            .container-table td,
            .container-table th {
                border: 1px solid #000;
            }

            .container-table tr td:last-child {
                text-align: justify;
            }

            .container-table tr td:first-child {
                width: 25%;
            }

            .container-table tr td:nth-child(2) {
                width: 5%;
                text-align: center;
            }

            .container-table tr td:last-child {
                width: 75%;
            }

            .calculate_premium tr td:first-child{
                width: 45%;
            }

            /* .calculate_premium tr td:nth-child(2){
                width: 25%;
                text-align: center;
            } */

            .calculate_premium tr td:nth-child(3){
                width: 40%;
            }

            /* .calculate_premium tr td:nth-child(4){
                width: 5%;
            } */

            table.bottomtable {
                page-break-before: always;
                page-break-after:auto
            }

            table.bottomtable tr {
                page-break-inside:avoid !important;
                page-break-after:auto !important;
            }

            table.bottomtable tr td {
                page-break-inside:avoid !important;
                page-break-after:auto !important;
            }

            caption {
                page-break-inside:avoid !important;
                page-break-after:auto !important;
            }


            table.ttd {
                width: 100%;
            }

            table.ttd tr td:first-child,
            table.ttd tr td:last-child{
                width: 50%;
            }

            table.ttd tr td:last-child {
                text-align: right;
            }

            .break-page {
                page-break-before: always !important;
                page-break-after:auto;
            }

            .break-page table {
                font-size: 11px !important;
            }

        </style>
    </head>
    <title> Export PDF Quotation List </title>
    <body>

        <header>
            @php
                $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
            @endphp
            <img src="{{asset('img/'.$data_imgz->image_cetak)}}" width="300">
        </header>

        <footer>

            <script type="text/php">
                if (isset($pdf)) {
                    $text = "{PAGE_NUM}";
                    $size = 10;
                    $font = $fontMetrics->getFont("Verdana");
                    $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                    $x = ($pdf->get_width() - $width);
                    $y = $pdf->get_height() - 35;
                    $pdf->page_text($x, $y, $text, $font, $size);
                }
            </script>
        </footer>

        <main>

            <div class="container-tittle">
                <h1 class="tittle"> LAMPIRAN </h1>
                <h4> Detail Insured </h4>
            </div>

            <div class="container-table">
                <table width="100%">
                    <tr>
                        @foreach ( $colsname as $value )
                            <th style="text-align: center"> {{ $value->label }} </th>
                        @endforeach
                    </tr>
                    @foreach ( $detail as $dt )
                        <tr>
                            @foreach ( $dt as $item )
                                <td> {{ $item->value_text }} </td>
                            @endforeach
                        </tr>
                    @endforeach
                </table>

            </div>

        </main>

    </body>
</html>
