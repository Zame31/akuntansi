<script type="text/javascript">
    var DataTable;
    var id_array_data = 0;
    var data = [];

    $(".money").maskMoney({
        prefix:'',
        allowNegative: false,
        thousands:'.',
        decimal:',',
        affixesStay: false,
        allowZero: true
        // precision: 0 // Tidak ada 2 digit dibelakang koma
    });

    $(".percentage").inputmask({
        alias:"numeric",
        integerDigits:3,
        groupSeparator: ".",
        digits: 5,
        max:100,
        allowMinus:false,
        digitsOptional: true,
        placeholder: "0"
    });

    $(document).ready(function () {

         // Tambahan
        $('#from, #to').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            clearBtn: true
            // startDate: date
        });


        $('#from').change(function () {
            $('#to').val('');
            $('#to').datepicker('destroy');
            $('#to').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                startDate: this.value,
                clearBtn: true
            });
            // $('#form-data').bootstrapValidator('revalidateField', 'from');
        });

        $('#to').change(function () {
            // $('#form-data').bootstrapValidator('revalidateField', 'to');
        });

        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                source_business: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih source of business'
                        },
                    }
                },
                form_wording: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            max: 255,
                            message: 'Silahkan isi maksimal 255 karakter'
                        },
                    }
                },
                customer: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih customer'
                        },
                    }
                },
                valuta_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih valuta id'
                        },
                    }
                },
                customer_address: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            max: 255,
                            message: 'Silahkan isi maksimal 255 karakter'
                        },
                    }
                },
                policy_name: {
                    validators: {
                        stringLength: {
                            max: 100,
                            message: 'Silahkan isi maksimal 100 karakter'
                        },
                        // regexp: {
                        //     regexp: /^[a-zA-Z\s]*$/,
                        //     message: 'Hanya boleh diisi huruf'
                        // },
                    }
                },
                // from: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih tanggal'
                //         },
                //     }
                // },
                // to: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih tanggal'
                //         },
                //     }
                // },
                the_business: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi'
                        // },
                        // stringLength: {
                        //     max: 255,
                        //     message: 'Silahkan isi maksimal 255 karakter'
                        // },
                    }
                },
                periode_of_insurance: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            max: 255,
                            message: 'Silahkan isi maksimal 255 karakter'
                        },
                    }
                },
                // number_of_insured: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan isi'
                //         },
                //         stringLength: {
                //             max: 5,
                //             message: 'Silahkan isi maksimal 5 digit angka'
                //         },
                //         regexp: {
                //             regexp: /^\d+$/,
                //             message: 'Wajib diisi angka'
                //         },
                //     }
                // },
                // type: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan isi'
                //         },
                //         stringLength: {
                //             max: 255,
                //             message: 'Silahkan isi maksimal 255 karakter'
                //         },
                //     }
                // },
                // colour: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan isi'
                //         },
                //         stringLength: {
                //             max: 255,
                //             message: 'Silahkan isi maksimal 255 karakter'
                //         },
                //     }
                // },
                // year_built: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan isi'
                //         },
                //     }
                // },
                // police_reg_no: {
                //     validators: {
                //         stringLength: {
                //             max: 100,
                //             message: 'Silahkan isi maksimal 100 karakter'
                //         },
                //     }
                // },
                // chassis_no: {
                //     validators: {
                //         stringLength: {
                //             max: 100,
                //             message: 'Silahkan isi maksimal 100 karakter'
                //         },
                //     }
                // },
                // engine_no: {
                //     validators: {
                //         stringLength: {
                //             max: 100,
                //             message: 'Silahkan isi maksimal 100 karakter'
                //         },
                //     }
                // },
                // accessories: {
                //     validators: {
                //         stringLength: {
                //             max: 100,
                //             message: 'Silahkan isi maksimal 100 karakter'
                //         },
                //     }
                // },
                valuta_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih valuta id'
                        },
                    }
                },
                total_sum_insured: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi'
                        // },
                        callback: {
                            // message: 'Silahkan isi',
                            callback: function(value, validator, $field) {
                                if (value === '0.00') {
                                    // return {
                                    //     valid: false,
                                    //     message: 'Silahkan isi'
                                    // }
                                    return true;
                                }
                                return true;
                            }
                        }
                    }
                },
                rate: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi'
                        // },
                        stringLength: {
                            max: 8,
                            message: 'Silahkan isi maksimal 8 karakter'
                        },
                        regexp: {
                            regexp: /^100$|^[0-9]{1,2}$|^[0-9]{1,2}\.[0-9]{1,5}$/i,
                            message: 'Format persentase tidak valid'
                        }
                    }
                },
                additional_rate: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi'
                        // },
                        stringLength: {
                            max: 8,
                            message: 'Silahkan isi maksimal 8 karakter'
                        },
                        regexp: {
                            regexp: /^100$|^[0-9]{1,2}$|^[0-9]{1,2}\.[0-9]{1,5}$/i,
                            message: 'Format persentase tidak valid'
                        },
                        callback: {
                            // message: 'Silahkan isi',
                            callback: function(value, validator, $field) {
                                if (value === '0') {
                                    return true;
                                }
                                return true;
                            }
                        }
                    }
                },
                third_party_limit: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi'
                        // },
                        stringLength: {
                            max: 30,
                            message: 'Silahkan isi maksimal 30 digit angka'
                        },
                        callback: {
                            // message: 'Silahkan isi',
                            callback: function(value, validator, $field) {
                                if (value === '0.00') {
                                    // return {
                                    //     valid: true,
                                    //     message: 'Silahkan isi'
                                    // }
                                    return true;
                                }
                                return true;
                            }
                        }
                    }
                },
                personal_accident: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi'
                        // },
                        stringLength: {
                            max: 30,
                            message: 'Silahkan isi maksimal 30 digit angka'
                        },
                        callback: {
                            // message: 'Silahkan isi',
                            callback: function(value, validator, $field) {
                                if (value === '0.00') {
                                    // return {
                                    //     valid: false,
                                    //     message: 'Silahkan isi'
                                    // }
                                    return true;
                                }
                                return true;
                            }
                        }
                    }
                },
                discount: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi'
                        // },
                        stringLength: {
                            max: 8,
                            message: 'Silahkan isi maksimal 8 karakter'
                        },
                        regexp: {
                            regexp: /^100$|^[0-9]{1,2}$|^[0-9]{1,2}\.[0-9]{1,5}$/i,
                            message: 'Format persentase tidak valid'
                        },
                        callback: {
                            // message: 'Silahkan isi',
                            callback: function(value, validator, $field) {
                                if (value === '0') {
                                    return {
                                        valid: false,
                                        message: 'Silahkan isi'
                                    }
                                }
                                return true;
                            }
                        }
                    }
                },
                security: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih security'
                        },
                    }
                },
                coverage_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi coverage'
                        },
                    }
                },
                main_exclusions_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi main exlusions'
                        },
                    }
                },
                deductible_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi deductible'
                        },
                    }
                },
                clauses_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi clauses'
                        },
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

        $('.year').datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true,
            orientation: 'bottom'
        }).on('changeDate', function(){
            // Revalidate the date when user change it
            $('#form-data').bootstrapValidator('revalidateField', 'year_built');
        });
    });

    function initMaskMoney() {
        $(".money").maskMoney({
            prefix:'',
            allowNegative: false,
            thousands:'.',
            decimal:',',
            affixesStay: false,
            // precision: 0 // Tidak ada 2 digit dibelakang koma
        });
    }



    function getValuta(elm) {
        var value = $(elm).val();
        var mata_uang = value.split('_')[1];
        $(".mata-uang").html(mata_uang);
    } // end function

    function showModalTextEditor(tittle_modal, class_name) {

        $("#" + class_name + ' .modal-title').html(tittle_modal);
        $("#" + class_name).modal('show');
        $('.' + class_name).summernote({
            lineHeights: ['0.2', '0.3', '0.4', '0.5', '0.6', '0.8', '1.0', '1.2', '1.4', '1.5', '2.0', '3.0'],
            focus: true,
            height: 300,
            toolbar: [
                ['style', ['style']],
                ['fontsize', ['fontsize']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['view', ['fullscreen', 'help']],
                ['height', ['height']]
            ],

        });

        // Set Content
        $("." + class_name).summernote("code", $("#" + class_name + '_content').val());
    }

    function save(class_name) {
        var markup = $('.' + class_name).summernote('code');
        $('.' + class_name).summernote('destroy');
        console.log(markup);
        $("#" + class_name).modal('hide');
        $("#" + class_name + '_data').html(markup);
        $("#" + class_name + '_content').val(markup);
        $("#form-data").bootstrapValidator('revalidateField', $("#" + class_name + '_content').prop('name'));
    }

    function calculate(type) {
        if ( type ) {
            $("#form-data").bootstrapValidator('revalidateField', $("#" + type).prop('name'));
        }
        var comprehensive, flood, tpl_limit, pa_driver, pa_passenger, sub_total,
            sub_disc_10, grand_total, today_curs;

        var total_sum_insured = $("#total_sum_insured").val();
        var additional_rate = $("#additional_rate").val();
        var rate = $("#rate").val();
        var third_party_limit = $("#third_party_limit").val();
        var personal_accident = $("#personal_accident").val();
        var discount = $("#discount").val();

        // Calculate Comprehensive
        if ( total_sum_insured ) {
            // Contoh memakai dua digit angka
            total_sum_insured = total_sum_insured.split('.');
            total_sum_insured = parseFloat(total_sum_insured[0].replace(/\,/g, ''));

            // total_sum_insured = parseFloat(total_sum_insured.replace(/\,/g, ''));
        } else {
            total_sum_insured = 0;
        }

        if ( rate ) {
            rate = $("#rate").val() / 100;
        } else {
            rate = 0;
        }


        comprehensive = total_sum_insured * rate;

        $("#comprehensive").val(comprehensive.toFixed(2)).trigger('mask.maskMoney');

        if ( additional_rate ) {
            rate = parseFloat($("#additional_rate").val()) / 100;
        } else {
            additional_rate = 0;
        }

        flood = total_sum_insured * additional_rate;
        $("#flood").val(flood.toFixed(2)).trigger('mask.maskMoney');

        // Calculate Flood
        total_sum_insured = $("#total_sum_insured").val();
        if ( total_sum_insured ) {
            total_sum_insured = total_sum_insured.split('.');
            total_sum_insured = parseFloat(total_sum_insured[0].replace(/\,/g, ''));
        } else {
            total_sum_insured = 0;
        }

        if ( additional_rate ) {
            additional_rate = parseFloat($("#additional_rate").val()) / 100;
        } else {
            additional_rate = 0;
        }

        flood = total_sum_insured * additional_rate;
        $("#flood").val(parseFloat(flood.toFixed(2))).trigger('mask.maskMoney');

        // Calculate Tpl Limit
        if ( third_party_limit ) {
            third_party_limit = third_party_limit.split('.');
            third_party_limit = parseFloat(third_party_limit[0].replace(/\,/g, ''));
        } else {
            third_party_limit = 0;
        }

        tpl_limit = third_party_limit * 0.01; // third_party * 1%
        $("#tpl_limit").val(tpl_limit.toFixed(2)).trigger('mask.maskMoney');

        // Calculate PA Driver
        if ( personal_accident ) {
            personal_accident = personal_accident.split('.');
            personal_accident = parseFloat(personal_accident[0].replace(/\,/g, ''));
        } else {
            personal_accident = 0;
        }

        pa_driver = personal_accident * 0.005; // personal_accident * 0.5%
        $("#pa_driver").val(pa_driver.toFixed(2)).trigger('mask.maskMoney');

        pa_passenger = personal_accident * 0.001 * 4; // personal_accident * 0.5%
        $("#pa_passenger").val(pa_passenger.toFixed(2)).trigger('mask.maskMoney');

        // Calculate Discount
        if ( discount ) {
            discount = parseFloat($("#discount").val()) / 100;
        } else {
            discount = 0;
        }


        $("#comprehensive_amount").val(comprehensive.toFixed(2));
        $("#flood_amount").val(flood.toFixed(2));
        $("#tpl_limit_amount").val(tpl_limit.toFixed(2));
        $("#pa_driver_amount").val(pa_driver.toFixed(2));
        $("#pa_passenger_amount").val(pa_passenger.toFixed(2));

        // Calculate Total
        sub_total = comprehensive + flood + tpl_limit + pa_driver + pa_passenger;
        sub_disc_10 = sub_total * discount;
        grand_total = sub_total - sub_disc_10;

        $("#sub_total").val(sub_total.toFixed(2)).trigger('mask.maskMoney');
        $("#sub_total_value").val(sub_total.toFixed(2));

        $("#discount_amount").val(sub_disc_10.toFixed(2)).trigger('mask.maskMoney');
        $("#discount_amount_value").val(sub_disc_10.toFixed(2));

        $("#grand_total").val(grand_total.toFixed(2)).trigger('mask.maskMoney');
        $("#grand_total_value").val(grand_total.toFixed(2));

        $("#gross_premium").val(sub_total.toFixed(2)).trigger('mask.maskMoney');
        $("#gross_premium_value").val(sub_total.toFixed(2));

        $("#nett_premium").val(grand_total.toFixed(2)).trigger('mask.maskMoney');
        $("#nett_premium_value").val(grand_total.toFixed(2));


    } // end function

    function closeModal() {
        $("#kt_summernote_modal").modal('hide');
    }


    function show_form() {

        var product_id = $("#product").val();

        if ( !product_id ) {
            toastr.error("Please select type of insurance");
            return false;
        }

        // if ( product_id != 4 ) {
        //     toastr.info("<b> Can't Process </b> <br> Please choose another type of insurance");
        //     return false;
        // }

        if ( product_id ) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: base_url + '/quotation_slip/get_qs_content?id=' + product_id,
                data: {},
                beforeSend: function () {
                    $("#container-product").fadeOut(500);
                    loadingPage();
                },

                success: function (response) {
                    console.log(response);
                    if (response.rc == 1) {
                        var data = response.data;
                        $("#form-data")[0].reset();
                        $("#source_business").val("1000").trigger('change');
                        $("#customer").val("1000").trigger('change');
                        $("#security").val("1000").trigger('change');
                        $('#form-data').bootstrapValidator("resetForm", true);
                        $("#valuta_id").val("1_IDR").trigger('change');

                        $("#container-form").delay(1000).slideDown(1000);
                        $("#product_name").html($("#product option:selected").text());
                        $("#underwriter_id").val(product_id);

                        if ( data ) {
                            $("#form_wording").val(data.form_wording);
                            $("#the_business").val(data.the_business);

                            $("#coverage_data").html(data.coverage);
                            $("#coverage_content").val(data.coverage);

                            $("#main_exclusions_data").html(data.main_exclusions);
                            $("#main_exclusions_content").val(data.main_exclusions);

                            $("#deductible_data").html(data.deductible);
                            $("#deductible_content").val(data.deductible);

                            $("#clauses_data").html(data.clauses);
                            $("#clauses_content").val(data.clauses);
                        }

                        getDetailInsured(product_id);
                        appendCalculation(product_id);

                        var page = '?id=' + product_id;
                        var url = window.location.pathname;
                        page = url + page;
                        var state = { name: "name", page: 'History', url: page };
                        window.history.replaceState(state, "History", page);


                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }

                }
            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        } // endif

    }

    function show_form_product(){
        var url = window.location.href.split('?')[0];
        page = url;
        var state = { name: "name", page: 'History', url: page };
        window.history.replaceState(state, "History", page);

        $("#product").val("1000").trigger('change');

        $("#container-product").slideDown('slow');
        $("#container-form").slideUp('slow');

    }

    function get_address_customer(elm, action) {
        var value = $(elm).val();

        if ( value ) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var product_id = $("#product").val();

            $.ajax({
                type: "GET",
                url: base_url + '/quotation_slip/get_cust_addr?id=' + value + '&action=' + action + '&prod_id=' + product_id,
                data: {},
                beforeSend: function () {
                    KTApp.block('#container-cust-addr', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'danger',
                        message: 'Please wait...'
                    });
                },

                success: function (response) {
                    console.log(response);
                    if (response.rc == 1) {
                        var data = response.data;
                        $("#customer_address").val(data.address);
                        $("#officer").val(data.full_name);
                        $("#qs_no").val(data.qs_no);
                        $("#qs_no_value").val(data.qs_no);
                        $('#form-data').bootstrapValidator('revalidateField', 'customer_address');
                    }

                }
            }).done(function (msg) {
                KTApp.unblock('#container-cust-addr');
            }).fail(function (msg) {
                KTApp.unblock('#container-cust-addr');
                toastr.error("Terjadi Kesalahan");
            });
        } // endif
    }


    function store() {
        var validate = $('#form-data').data('bootstrapValidator').validate();
        if (validate.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);
            objData.append('detail', JSON.stringify(data));

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('quotation_slip.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    console.log(response);
                    endLoadingPage();
                    if ( response.rc == 0 ) {

                        if ( !$("#id").val() ) {
                            var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
                            var action = `<button onclick="znView('${response.url}')" type="button"
                                                class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                                                Data</button>
                                            <button onclick="znClose()" type="button"
                                                class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                                                Data</button>`;
                        } else {
                            var text = `Data berhasil disimpan`;
                            var action = `<button onclick="znView('${response.url}')" type="button"
                                                class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                                                Data</button>`;
                        }

                        znIconbox("Sukses",text,action);
                    } else if ( response.rc == 1 ) {
                        var data_fail = response.data;
                        swal.fire({
                            title: 'Warning',
                            text: 'Terdapat Rate Premi yang tidak sesuai dengan sistem. \n Silakan cek kembali',
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonText: 'Ok',
                            reverseButtons: true
                        }).then(function (result) {
                        });
                        console.log('invalid');
                        console.log(data_fail);

                        // Remove all data in datatable
                        DataTable.clear().draw(false);
                        data = [];
                        data = data_fail;
                        console.log('data baru');
                        console.log(data);
                        for ( i = 0; i < data.length; i++) {

                            console.log(data[i]);
                            action = "";
                            rows = [];

                            action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" onclick="modal_add_detail('edit', '${data[i].id}')">
                                                <i class="la la-edit"></i> Edit
                                            </a>
                                            <a class="dropdown-item" onclick="hapus_detail('${data[i].id}', this)">
                                                <i class="la la-trash"></i> Hapus
                                            </a>
                                            <a class="dropdown-item" onclick="set_active('${data[i].id}', this, 'f')">
                                                <i class="la la-close"></i> Non Aktif
                                            </a>`;

                            var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';

                            data[i].value.forEach( function (value, index){
                                console.log(value);
                                if ( value.name != 'sts' ) {
                                    rows.push(value.value);
                                }
                            });

                            // Insert acton at the begining index array
                            rows.unshift(sts);
                            rows.unshift(action);
                            DataTable.row.add(rows).draw();
                            id_array_data = data[i].id;

                        } // end for


                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif
    }

    function znClose() {
        znIconboxClose();
        $("#form-data")[0].reset();
        document.getElementById("form-data").reset();
        $('#form-data').bootstrapValidator("resetForm", true);
        loadNewPage('{{ route('quotation_slip.create') }}');
    }

    function znView(url) {
        znIconboxClose();
        console.log('url lihat data : ' + url);

        if ( url == 'quotation_slip' ) {
            loadNewPage('{{ route('quotation_slip') }}');
        } else {
            loadNewPage('{{ route('quotation_slip.client') }}');
        }


    }

    function setCancel(id, isCancel) {
        if (isCancel == 't') {
            var titleSwal = 'Batalkan Quotation Slip';
            var textSwal = 'Anda yakin akan membatalkan quotation slip ini?';
        } else {
            var titleSwal = 'Aktif Quotation Slip';
            var textSwal = 'Anda yakin akan mengaktifkan quotation slip ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('quotation_slip.set_cancel') }}',
                    data: {
                        id: id,
                        cancel: isCancel,
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                            location.reload();
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function print(url) {
        window.open(url, '_blank');
    }

    function getDetailInsured(prod_id) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: base_url + '/quotation_slip/get_detail_insured/' + prod_id,
            data: {},

            beforeSend: function () {
                // loadingPage();
            },

            success: function (res) {
                var data = res.data;
                var class_dt = "";
                if (res.rc == 1) {
                    var tableHeaders = "";
                    tableHeaders = `<th width="30px" class="text-center">Action</th>
                                    <th class="text-center"> Status </th>`;
                    data.forEach(function ( value, index) {
                        if ( value.is_showing ) {
                            class_dt = 'all';
                        } else {
                            class_dt = 'none';
                        }
                        tableHeaders += `<th class="${class_dt} text-center">${value.label}</th>`;
                    })
                    $("#tableDiv").empty();
                    $("#tableDiv").append('<table id="table_detail" class="table table-striped- table-hover table-checkable"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                    DataTable = $('#table_detail').DataTable({
                        responsive: true,
                    });
                } else {
                    toastr.error(data.rm);
                }
            }
        }).done(function (msg) {
            // endLoadingPage();
        }).fail(function (msg) {
            // endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function save_detail(act, id_detail) {
        if ( $("#form-detail")[0].checkValidity() ) {
            event.preventDefault();
            var temp_id;
            var data_detail = $("#form-detail").serializeArray();
            var array_input = data_detail.filter(function(item){
                return item.name != '_token';
                // return item.name != '_token';
            });

            temp_id = id_array_data+1;
            var temp_data = [];
            var rows = [];

            var item = {
                "name" : 'sts',
                "value" : 't',
                "bit_id" : 12,
            }

            temp_data.push(item);

            var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" onclick="modal_add_detail('edit', '${temp_id}')">
                                <i class="la la-edit"></i> Edit
                            </a>
                            <a class="dropdown-item" onclick="hapus_detail('${temp_id}', this)">
                                <i class="la la-trash"></i> Hapus
                            </a>
                            <a class="dropdown-item" onclick="set_active('${temp_id}', this, 'f')">
                                <i class="la la-close"></i> Non Aktif
                            </a>`;

            var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';
            array_input.forEach( function (value, index){
                rows.push(value.value);
                var name = value.name.split('-')[0];
                var bit_id = value.name.split('-')[1];
                item = {
                    "name": name,
                    "value": value.value,
                    "bit_id" : bit_id
                };
                temp_data.push(item);
            });

            // Insert acton at the begining index array
            rows.unshift(sts);
            rows.unshift(action);

            if ( act == 'insert' ) {

                data.push({
                    id : temp_id,
                    value: temp_data
                });

                console.log('data setelah insert');
                console.log(data);

                DataTable.row.add(rows).draw();
                id_array_data++;
            } else {
                // Remove All Data
                DataTable.clear().draw(false);
                id_detail = parseInt(id_detail);

                console.log('id data edit');
                console.log(id_detail);
                // Remove From Array
                data = data.filter(function(value){
                    console.log(value);
                    return value.id != id_detail;
                });

                console.log('data setelah dihapus');
                console.log(data);


                console.log('data seteleh ditambkan kembali');
                // Insert to Array
                data.push({
                    id : temp_id,
                    value: temp_data
                });

                console.log(data);

                for ( i = 0; i < data.length; i++) {

                    console.log(data[i]);
                    action = "";
                    rows = [];

                    action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" onclick="modal_add_detail('edit', '${data[i].id}')">
                                        <i class="la la-edit"></i> Edit
                                    </a>
                                    <a class="dropdown-item" onclick="hapus_detail('${data[i].id}', this)">
                                        <i class="la la-trash"></i> Hapus
                                    </a>
                                    <a class="dropdown-item" onclick="set_active('${data[i].id}', this, 'f')">
                                        <i class="la la-close"></i> Non Aktif
                                    </a>`;

                    var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';

                    data[i].value.forEach( function (value, index){
                        console.log(value);
                        if ( value.name != 'sts' ) {
                            rows.push(value.value);
                        }
                    });

                    // Insert acton at the begining index array
                    rows.unshift(sts);
                    rows.unshift(action);
                    DataTable.row.add(rows).draw();
                    id_array_data = data[i].id;

                }

                console.log('id index terakhir : ' + id_array_data);
                console.log('posisi data setelah edit');
                console.log(data);
            }

            $("#detail").modal('hide');
        }
    }

    function hapus_detail(id, elm) {
        var elmt = $(elm).parents('tr');
        data = data.filter(function(value){
            return value.id != id;
        });

        console.log('posisi data setelah dihapus');
        console.log(data);

        DataTable.row(elmt).remove().draw(false);
    }

    function modal_add_detail(action, id_arr) {
        event.preventDefault();
        var url = new URL(window.location.href);
        var id = url.searchParams.get("id");
        var prod_id = id;
        $("#form-detail")[0].reset();

        if ( action == 'insert' || action == 'insert_edit') {

            if ( action == 'insert_edit' ) {
                prod_id = id_arr;
            }

            //AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{ route('get_insured_detail') }}',
                data: {
                    product_id : prod_id
                },

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    console.log(response);
                    if ( response.rc == 1 ) {
                        var view = response.view;
                        $(".form_content").html(view);
                        initMaskMoney();
                        $("#title-detail").html("Detail");
                        $("#btn-detail").attr('onclick', 'save_detail(`insert`)');
                        $('.tanggal').datepicker({
                            format: 'dd/mm/yyyy',
                            autoclose: true,
                        });
                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        } else {

            var prod_id_edit =  $("#underwriter_id").val();

            //AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{ route('get_insured_detail') }}',
                data: {
                    product_id : prod_id_edit
                },

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    console.log(response);
                    if ( response.rc == 1 ) {
                        var view = response.view;
                        $(".form_content").html(view);
                        initMaskMoney();

                        console.log(data);

                        console.log('id : ' + id_arr);

                        var data_edit = data.filter(function(value, index) {
                            return value.id == id_arr;
                        });

                        console.log(data_edit);
                        data_edit = data_edit[0].value;
                        data_edit.forEach(function (value, index) {
                            $("#" + value.name + '-' + value.bit_id).val(value.value);
                        });

                        $("#btn-detail").attr('onclick', 'save_detail(`edit`, `'+ id_arr +'`)');
                        $("#title-detail").html("Edit Detail");

                        $('.tanggal').datepicker({
                            format: 'dd/mm/yyyy',
                            autoclose: true,
                        });
                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // end if

        $("#detail").modal('show');
    } // end function

    function set_active(id_detail, elm, is_active) {

        data.forEach(function (value, index) {
            console.log(value);
            if( value.id == id_detail ) {
                value.value.forEach(function (dt, index) {
                    if ( dt.bit_id == 12 && is_active == 'f') {
                        dt.value = 'f';
                    } else if ( dt.bit_id == 12 && is_active == 't') {
                        dt.value = 't';
                    }
                });
            }
        });

        var data_pilih = data.filter(function (value, index) {
            return value.id == id_detail;
        });

        var elmt = $(elm).parents('tr');
        DataTable.row(elmt).remove().draw(false);

        var sts, act;
        var rows = [];
        if (is_active == 'f' ) {
            // Set Non Aktif
            sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Non Aktif</span>';
            act = `<a class="dropdown-item" onclick="set_active('${data_pilih[0].id}', this, 't')">
                    <i class="la la-check"></i> Aktif
                </a>`;

        } else {
            // Set Aktif
            sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';
            act = `<a class="dropdown-item" onclick="set_active('${data_pilih[0].id}', this, 'f')">
                    <i class="la la-close"></i> Non Aktif
                </a>`;
        }

        var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" onclick="modal_add_detail('edit', '${data_pilih[0].id}')">
                                <i class="la la-edit"></i> Edit
                            </a>
                            <a class="dropdown-item" onclick="hapus_detail('${data_pilih[0].id}', this)">
                                <i class="la la-trash"></i> Hapus
                            </a>` + act;

        data_pilih[0].value.forEach( function (value, index){
            if ( value.name != 'sts' ) {
                rows.push(value.value);
            }
        });

        rows.unshift(sts);
        rows.unshift(action);

        console.log('row');
        console.log(rows);

        DataTable.row.add(rows).draw();

        console.log(data);

    }

    function appendCalculation(product_id) {
        if ( product_id != 4 ) {
            // Selain Vehicle
            $(".prod-vehicle").addClass('d-none');
            $(".label-dynamic").text('Premium');
            $(".row-change").removeClass('col-6');
            $(".row-change").addClass('col-12');
        } else {
            $(".prod-vehicle").removeClass('d-none');
            $(".label-dynamic").text('Comprehensive');
            $(".row-change").removeClass('col-12');
            $(".row-change").addClass('col-6');
        }
    }

// DOWNLOAD TEMPLATE EXCEL

    function modal_download_template() {

        $('#ex-select-all').prop('selected',true);

        var product_id = $("#underwriter_id").val();

            $.ajax({
            type: "GET",
            url: base_url + '/quotation_slip/get_detail_insured/' + product_id,
            data: {},

            beforeSend: function () {
                loadingPage();
            },

            success: function (res) {
                var data = res.data;
                console.log(data);

                if (res.rc == 1) {
                    var tableHeaders = "";
                    data.forEach(function ( value, index) {
                        tableHeaders += `<div class="col-md-4">
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                                <input value="${value.label}" data-idname="${value.input_name}" class="ck-excel" name="ck-excel-label[]" checked type="checkbox"> ${value.label}
                                                <span></span>
                                            </label>
                                        </div>`;
                    })
                    $("#list_check_template").empty();
                    $("#list_check_template").append(tableHeaders);
                } else {
                    toastr.error(data.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });

        $('#template_excel').modal('show');
    }

    function modal_import_template() {
        $("#excel-import")[0].reset();
        $('#excel-import').bootstrapValidator("resetForm", true);
        $('#import_template_excel').modal('show');
    }

    function exportData(ext) {

        let arr_ck_excel_label = [];
        let arr_ck_excel_input_name = [];

        $('input[name="ck-excel-label[]"]:checked').each(function() {
            arr_ck_excel_label.push($(this).val());
            arr_ck_excel_input_name.push($(this).data('idname'));
            console.log($(this).val());
            console.log($(this).data('idname'));
        });
        // $('#type_size').find(':selected').data('harga');


        if (arr_ck_excel_label.length == 0) {
            toastr.warning('Minimal Pilih 1 Kolom Untuk Melakukan Download Template');
        }else{
            var query = {
                file: ext,
                product_id: $('#underwriter_id').val(),
                product_name: $('#product_name').html(),
                ck_excel_label: arr_ck_excel_label,
                ck_excel_input_name: arr_ck_excel_input_name,
            }

            console.log(query);
            window.open(base_url + 'report/detail_product_export?'+$.param(query), "_blank");
        }
    }

    function importDataExcel() {

        var validateDebit = $('#excel-import').data('bootstrapValidator').validate();
        if (validateDebit.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("excel-import");
            var objData = new FormData(formData);

            var rows = [];

            $.ajax({
                type: 'POST',
                url: '{{ route('import.detail_product_import') }}',
                data: objData,
                contentType: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },
                success: function (msg) {
                    endLoadingPage();
                }

            }).done(function (msg) {
                console.log('done',msg);

                let dataExcel = msg.rm;
                let detailHeader = msg.detail;
                let typeImport = msg.zn_type_import;

                if (typeImport == 'xls_table') {
                    data = [];
                    DataTable.clear().draw();
                }

                var arrError = [];

                if (msg.product_id == '') {
                    toastr.info('Template Tidak Sesuai, ID pada template tidak ditemukan !');
                    return false;
                }

                if (typeImport == '') {
                    toastr.info('Template Tidak Sesuai, Tipe Import tidak ditemukan !');
                    return false;
                }

                if(msg.product_id != $('#underwriter_id').val()) {
                    toastr.info('Template Tidak Sesuai !');
                    return false;
                }

                let tmp_dataExcel = [];
                dataExcel.forEach((v,key) => {
                    if (key > 0) {
                        id_array_data += key;
                        rows = [];
                        let tmp_data_fk = [];
                        temp_data = [];

                        var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" onclick="modal_add_detail('edit', '${id_array_data}')">
                                                <i class="la la-edit"></i> Edit
                                            </a>
                                            <a class="dropdown-item" onclick="hapus_detail('${id_array_data}', this)">
                                                <i class="la la-trash"></i> Hapus
                                            </a>`;

                        var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';

                        if (typeImport == 'xls_table') {
                            if (v['sts'] == 0) {
                                sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Non Aktif</span>';
                            }else {
                                sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';
                            }
                        }
                        else{

                        }

                        rows.unshift(sts);
                        rows.unshift(action);



                        // VALIDASI
                        detailHeader.forEach((v_fk,key_fk) => {

                            let headerAllow = Object.keys(v);
                            if (headerAllow.includes(v_fk.input_name)) {


                                // if (v[v_fk.input_name] == '' || v[v_fk.input_name] == null) {
                                //     arrError.push(`<div class="kt-list-timeline__item">
                                //                         <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                //                         <span class="kt-list-timeline__text">Kolom `+v_fk.label+` Baris ke `+(key+2)+`</span>
                                //                         <span class="kt-list-timeline__time" style="width: 130px;"><span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="background: #fff5fe;color: #fd3162;border: 1px solid #fde0e7;">Tidak Boleh Kosong</span> </span>

                                //                     </div>`);
                                // }else{
                                    if (v_fk.is_number || v_fk.is_currency) {

                                            if (typeof v[v_fk.input_name] != 'number') {
                                                arrError.push(`<div class="kt-list-timeline__item">
                                                            <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                            <span class="kt-list-timeline__text">
                                                                Kolom `+v_fk.label+` Baris ke `+(key+2)+`
                                                                </span>
                                                                <span class="kt-list-timeline__time" style="width: 130px;"><span class="mb-1 kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="background: #fff5fe;color: #fd3162;border: 1px solid #fde0e7;">Harus Angka</span></span>
                                                        </div>`);

                                            }
                                    }
                                // }


                            }
                        });
                        // VALIDASI




                       if (arrError.length == 0) {

                            var setStatus = 't';

                            if (typeImport == 'xls_table') {
                                if (v['sts'] == 0) {
                                    setStatus = 'f';
                                }
                            }

                            var sts_item = {
                                "name" : 'sts',
                                "value" : setStatus,
                                "bit_id" : 12,
                            }

                            temp_data.push(sts_item);


                            detailHeader.forEach((v_fk,key_fk) => {

                            if (v[v_fk.input_name]) {

                                console.log(v_fk.input_name,v_fk.is_currency);

                                let setValue = '';

                                if(typeof v[v_fk.input_name] == 'number'){
                                    if(v_fk.is_currency){
                                        setValue = formatMoney(v[v_fk.input_name]);
                                    }else{
                                        setValue = v[v_fk.input_name];
                                    }

                                }else if(typeof v[v_fk.input_name] == 'object'){
                                   console.log('ini date',v[v_fk.input_name].date);
                                   setValue = znFormatDateNumSlash(v[v_fk.input_name].date);
                                }else{
                                    setValue = v[v_fk.input_name]
                                }

                                // let setValue = (typeof v[v_fk.input_name] == 'number') ?
                                // ((v_fk.is_currency) ? formatMoney(v[v_fk.input_name]):v[v_fk.input_name]) :v[v_fk.input_name];

                                var item = {
                                    "name" : v_fk.input_name,
                                    "value": setValue,
                                    "bit_id" : v_fk.bit_id,
                                };
                                rows.push(setValue);
                            }else{
                                var item = {
                                    "name" : v_fk.input_name,
                                    "value": '',
                                    "bit_id" : v_fk.bit_id,
                                };
                                rows.push('');
                            }

                            temp_data.push(item);

                            });

                            data.push({
                            id : id_array_data,
                            value : temp_data
                            });

                            DataTable.row.add(rows).draw();
                       }


                    }

                });

                $('#import_template_excel').modal('hide');

                if (arrError.length > 0) {
                    $('#kt-iconbox__icon').hide();
                    var textEx = `<div class="kt-list-timeline mt-4 mb-4 scrollStyleDanger px-2" style="max-height: 350px;overflow-y: scroll;">
                                    <div class="kt-list-timeline__items">
                                        `+arrError.join("")+`
                                    </div>
                                </div>`
                    var actionEx = `  <button onclick="znIconboxClose();modal_import_template();" type="button"
                                        class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Import Ulang</button>
                                    <button onclick="znIconboxClose()" type="button"
                                        class="btn btn-outline-danger btn-elevate btn-pill btn-elevate-air btn-sm">Tutup</button>`;
                    znIconbox("Import Excel", textEx, actionEx,'danger');
                }

                console.log('tmp_dataExcel',tmp_dataExcel);


                console.log(data);

            }).fail(function (msg) {
                console.log('error',msg);
                endLoadingPage();
                // toastr.error("Terjadi Kesalahan");
            });

        }

    }

    function export_excel_detail() {

        console.log(data);

        $.ajax({
            type : 'POST',
            url: '{{ route('export.table_detail_product_export') }}',
            data:{
                file: 'xls_table',
                product_id: $('#underwriter_id').val(),
                product_name: $('#product_name').html(),
                table_data: data
            },
            beforeSend: function () {
                loadingPage();
            },
        }).then((response) => {
            endLoadingPage();
            console.log(response);
            window.open(base_url + response, "_blank");
        });


        // /////////////////////////////////////
        // console.log(data);

        // var query = {
        //         file: 'xls_table',
        //         product_id: $('#underwriter_id').val(),
        //         product_name: $('#product_name').html(),
        //         table_data: data
        //     }

        //     console.log(query);
        //     window.open(base_url + 'report/detail_product_export?'+$.param(query), "_blank");
    }

    $(document).ready(function () {
        $('#ex-select-all').click(function (e) {
        console.log('wwwwww');
            $('.ck-excel').prop('checked', this.checked);
        });

        $("#excel-import").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                file_import: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        file: {
                            extension: 'xlsx,xls',
                            type: 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            message: 'File yang akan di import tidak sesuai'
                        }
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    })


// END DOWNLOAD TEMPLATE EXCEL

function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {

    }
}

function check_data_product(){

    var data_detail_product = data;
    console.log(data_detail_product);

    data_detail_product.forEach(v => {
        let dp = v.value;

        dp.forEach(v => {

            switch (parseInt(v.bit_id)) {

                // CHECK USIA
                case 84:
                    console.log(v.value);
                    var date = new Date('22-12-2020');
                    console.log(date);

                    console.log(znFormatDateNum(v.value));

                    // if(v.value)
                    // // Sum Insured
                    // var si = (v.value) ? parseFloat(clearNumFormatDec(v.value)):0;
                    // ttlSI += si;
                break;

            }
        });

    });

}

</script>
