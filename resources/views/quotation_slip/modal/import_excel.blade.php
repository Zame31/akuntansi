<!--begin::Modal-->
<div class="modal fade" id="import_template_excel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
                <div class="modal-body">
                    <form id="excel-import" enctype="multipart/form-data">
                        <div>
                            <label class="col-form-label">Upload File Template Excel (xls,xlsx) : </label>
                            <div class="form-group mb-2">
                                <input class="form-control" type="file" name="file_import" id="file_import">
                            </div>
                        </div>
                    </form>
                  
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success " onclick="importDataExcel();"><i class="la la-save"></i>Import Excel</button>
                </div>
        </div>
    </div>
</div>
<!--end::Modal-->
