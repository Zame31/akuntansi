<!--begin::Modal-->
<div class="modal fade" id="clauses" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form class="kt-form kt-form--fit kt-form--label-right">
                <div class="modal-body">
                    <div class="form-group row kt-margin-t-10">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="clauses"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger btn-elevate btn-icon" data-dismiss="modal"><i class="la la-close"></i></button>
                    <button type="button" class="btn btn-outline-success btn-elevate btn-icon" onclick="save('clauses')"><i class="la la-save"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
