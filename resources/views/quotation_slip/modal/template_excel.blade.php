<!--begin::Modal-->
<div class="modal fade" id="template_excel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Template Excel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
                <div class="modal-body">
                    <h6 class="mb-4">Pilih Kolom yang akan di gunakan untuk template excel :</h6>
                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--solid kt-checkbox--success">
                        <input checked type="checkbox" class="kt-group-checkable" id="ex-select-all"> Select All / Remove All
                        <span></span>
                    </label>
                    <div style="border-bottom: 1px dashed #ebedf2;margin-bottom: 15px;margin-top: 15px;"></div>
                    <form id="excel-template-form" enctype="multipart/form-data">
                        <div class="row" id="list_check_template">
                            <div id="list_co_1" class="col-md-4 sortable connectedSortable">
        
                            </div>
                            <div id="list_co_2" class="col-md-4 sortable connectedSortable">

                            </div>
                            <div id="list_co_3" class="col-md-4 sortable connectedSortable">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success " onclick="exportData('xls');"><i class="la la-save"></i>Download Template Excel</button>
                </div>
        </div>
    </div>
</div>
<!--end::Modal-->
