@section('content')
<div class="app-content">
    <div class="section">


        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        {{$tittle}} {{$type_batch}} </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="form-group col-3">
                    <label class="text-center">Batch Proses Date</label>
                    <div class="input-group date">
                        @php
                            $system_date = collect(\DB::select("select * from ref_system_date"))->first();
                        @endphp
                        <input type="hidden" name="type" id="type" value="multi">
                    <input type="text" value="{{date('d F Y',strtotime($system_date->current_date))}}" class="form-control {{($type_batch=='day') ? "zn-readonly":""}}" id="tgl_tr" name="tgl_tr" readonly
                            placeholder="Batch Proses Date" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar-check-o"></i>
                            </span>
                        </div>
                        <div class="invalid-feedback">Silahkan pilih tanggal</div>
                    </div>
                </div>
                <div class="col-3">
                    <label class="text-center"> </label>
                    <div class="input-group">
                        <button type="button" onclick="return search()"
                            class="btn btn-info btn-elevate btn-elevate-air mt-2">Start
                            Proses</button>
                    </div>
                </div>
            </div>
            <div id="berhasil" class="kt-portlet kt-iconbox kt-iconbox--success kt-iconbox--animate-fast">
                <div class="kt-portlet__body">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path
                                        d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z M10.875,15.75 C11.1145833,15.75 11.3541667,15.6541667 11.5458333,15.4625 L15.3791667,11.6291667 C15.7625,11.2458333 15.7625,10.6708333 15.3791667,10.2875 C14.9958333,9.90416667 14.4208333,9.90416667 14.0375,10.2875 L10.875,13.45 L9.62916667,12.2041667 C9.29375,11.8208333 8.67083333,11.8208333 8.2875,12.2041667 C7.90416667,12.5875 7.90416667,13.1625 8.2875,13.5458333 L10.2041667,15.4625 C10.3958333,15.6541667 10.6354167,15.75 10.875,15.75 Z"
                                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    <path
                                        d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z"
                                        fill="#000000" />
                                </g>
                            </svg> </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <a class="kt-link" href="#">Success</a>
                            </h3>
                            <div class="kt-iconbox__content">
                                Batch Process Finished
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="gagal" class="kt-portlet kt-iconbox kt-iconbox--danger kt-iconbox--animate-fast">
                <div class="kt-portlet__body">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path
                                        d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z"
                                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    <path
                                        d="M10.5857864,13 L9.17157288,11.5857864 C8.78104858,11.1952621 8.78104858,10.5620972 9.17157288,10.1715729 C9.56209717,9.78104858 10.1952621,9.78104858 10.5857864,10.1715729 L12,11.5857864 L13.4142136,10.1715729 C13.8047379,9.78104858 14.4379028,9.78104858 14.8284271,10.1715729 C15.2189514,10.5620972 15.2189514,11.1952621 14.8284271,11.5857864 L13.4142136,13 L14.8284271,14.4142136 C15.2189514,14.8047379 15.2189514,15.4379028 14.8284271,15.8284271 C14.4379028,16.2189514 13.8047379,16.2189514 13.4142136,15.8284271 L12,14.4142136 L10.5857864,15.8284271 C10.1952621,16.2189514 9.56209717,16.2189514 9.17157288,15.8284271 C8.78104858,15.4379028 8.78104858,14.8047379 9.17157288,14.4142136 L10.5857864,13 Z"
                                        fill="#000000" />
                                </g>
                            </svg> </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <a class="kt-link" href="#">Fail</a>
                            </h3>
                            <div class="kt-iconbox__content">
                                Batch Process is already processed
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="gagal2" class="kt-portlet kt-iconbox kt-iconbox--danger kt-iconbox--animate-fast">
                <div class="kt-portlet__body">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path
                                        d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z"
                                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    <path
                                        d="M10.5857864,13 L9.17157288,11.5857864 C8.78104858,11.1952621 8.78104858,10.5620972 9.17157288,10.1715729 C9.56209717,9.78104858 10.1952621,9.78104858 10.5857864,10.1715729 L12,11.5857864 L13.4142136,10.1715729 C13.8047379,9.78104858 14.4379028,9.78104858 14.8284271,10.1715729 C15.2189514,10.5620972 15.2189514,11.1952621 14.8284271,11.5857864 L13.4142136,13 L14.8284271,14.4142136 C15.2189514,14.8047379 15.2189514,15.4379028 14.8284271,15.8284271 C14.4379028,16.2189514 13.8047379,16.2189514 13.4142136,15.8284271 L12,14.4142136 L10.5857864,15.8284271 C10.1952621,16.2189514 9.56209717,16.2189514 9.17157288,15.8284271 C8.78104858,15.4379028 8.78104858,14.8047379 9.17157288,14.4142136 L10.5857864,13 Z"
                                        fill="#000000" />
                                </g>
                            </svg> </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <a class="kt-link" href="#">Fail</a>
                            </h3>
                            <div class="kt-iconbox__content">
                                Data Not Found
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            {{-- SEND FILE --}}

        </div>


        <!-- end:: Subheader -->

    </div>
</div>

<script async type="text/javascript">
    // $('#svg_pl').hide();
    // $("#loading2").css('display', 'block');


    let type_batch = '{{$type_batch}}';

    function cetakBS() {

        var objData = new FormData();
        objData.append("type", "bs");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('report.saveToFolder') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (response) {}
        }).done(function (msg) {
        }).fail(function (msg) {
        });
    }

    function cetakPL() {

        var objData = new FormData();
        objData.append("type", "pl");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('report.saveToFolder') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (response) {}
        }).done(function (msg) {
            $("#loading2").css('display', 'none');
        }).fail(function (msg) {
        });
    }

    function cetakCN() {
        var objData = new FormData();
        objData.append("data", "zz");
        objData.append("type", "cn");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('report.saveToFolder') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {},
            success: function (response) {}
        }).done(function (msg) {

        }).fail(function (msg) {
        });

    }

    function cetakCOA() {
        var objData = new FormData();
        objData.append("data", "zz");
        objData.append("type", "coa");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('report.saveToFolder') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {},
            success: function (response) {}
        }).done(function (msg) {

        }).fail(function (msg) {
        });

    }

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    console.log(date);
    console.log(firstDay);
    console.log(lastDay);

    $('#tgl_tr').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,


    });

    $("#berhasil").hide();
    $("#gagal").hide();
    $("#gagal2").hide();


function batch_inventory() {

        $.ajax({
            type: 'GET',
            url: base_url + '/batch_inventory?type_batch='+type_batch+'&tgl=' + $("#tgl_tr").val(),
            beforeSend: function () {
                $('#stat_batch_inv').html('On Processing');
            },
            success: function (res) {
                var obj = JSON.parse(res);
            }
        }).done(function (res) {
            var obj = JSON.parse(res);

            $('#stat_batch_inv').html(obj.rm);

            batch_bdd();

        }).fail(function (res) {
            $("#gagal").show();
            $("#berhasil").hide();
            swal.fire("Error", "Terjadi Kesalahan!", "error");
        });

    }

    function batch_bdd() {
        $.ajax({
            type: 'GET',
            url: base_url + '/batch_bdd?type_batch='+type_batch+'&tgl=' + $("#tgl_tr").val(),
            beforeSend: function () {
                $('#stat_batch_bdd').html('On Processing');
  
            },
            success: function (res) {
                var obj = JSON.parse(res);
            }
        }).done(function (res) {
            var obj = JSON.parse(res);

            $('#stat_batch_bdd').html(obj.rm);
            batch_finance();

        }).fail(function (res) {
            $("#gagal").show();
            $("#berhasil").hide();
            swal.fire("Error", "Terjadi Kesalahan!", "error");
        });

    }

    function batch_finance() {
      

        $.ajax({
            
            type: 'GET',
            url: base_url + '/batch_jurnal?type_batch='+type_batch+'&tgl=' + $("#tgl_tr").val(),
            beforeSend: function () {
                $('#stat_batch_fin').html('On Processing');
            },
            success: function (res) {
                var obj = JSON.parse(res);
                // endLoadingPage();

                cetakBS();
                cetakPL();
                cetakCN();
                cetakCOA();
               

                console.log(res);
                if (obj.rc == 1) {
                    $("#gagal2").hide();
                    $("#gagal").hide();
                    $("#berhasil").show();
                } else if (obj.rc == 2) {
                    $("#gagal2").hide();
                    $("#gagal").show();
                    $("#berhasil").hide();
                } else {


                    $("#gagal2").show();
                    $("#gagal").hide();
                    $("#berhasil").hide();

                }
            }
        }).done(function (res) {
            var obj = JSON.parse(res);
            // endLoadingPage();

            // $("#loading2").css('display', 'none');
            console.log(res);
            if (obj.rc == 1) {
                $("#gagal2").hide();
                $("#gagal").hide();
                $("#berhasil").show();
            } else if (obj.rc == 2) {
                $("#gagal").show();
                $("#berhasil").hide();
                $("#gagal2").hide();
            } else {

                $("#gagal2").show();
                $("#gagal").hide();
                $("#berhasil").hide();

            }

        }).fail(function (res) {
            // endLoadingPage();
            $("#loading2").css('display', 'none');
            $("#gagal").show();
            $("#berhasil").hide();
            swal.fire("Error", "Terjadi Kesalahan!", "error");
        });
    }

    function search() {
        $("#loading2").css('display', 'block');
        $('#stat_batch_inv').html('Waiting');
        $('#stat_batch_bdd').html('Waiting');
        $('#stat_batch_fin').html('Waiting');

        // loadingPage();
        if ($('#tgl_tr').val() === '') {
            endLoadingPage();
            $("#tgl_tr").addClass("is-invalid");
            return false;
        } else {
            $("#tgl_tr").removeClass("is-invalid");

        }

        if (type_batch == 'day') {
            batch_finance();
            $('#view_batch_inventory').hide();
            $('#view_batch_bdd').hide();

        }else {
            batch_inventory();
            $('#view_batch_inventory').show();
            $('#view_batch_bdd').show();
            cetakDueDate();
            

        }

      


    }

    function cetakDueDate() {

        var objData = new FormData();
        objData.append("type", "due_date");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('report.saveToFolder') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (res) {
                console.log(res);
                sendMailDueDate(res.path);

            }
        }).done(function (msg) {
        }).fail(function (msg) {
        });
    }

    function sendMailDueDate(path){
        var objData = new FormData();
        objData.append("path", path);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('report.sendMailAny') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (res) {
                console.log(res);


            }
        }).done(function (msg) {
        }).fail(function (msg) {
        });
    }


    function exportToPDF(sts) {
        console.log('status cetak: ' + sts);
        switch (sts) {
            case '1000' :
                 window.open(base_url + 'report/nominative_report/export?file=pdf&status=All', "_blank");
            break;
            case '1' :
                window.open(base_url + 'report/nominative_report/export?file=pdf&status=Aktif', "_blank");
            break;
            case '2' :
                window.open(base_url + 'report/nominative_report/export?file=pdf&status=Paid', "_blank");
            break;
            case '3' :
                window.open(base_url + 'report/nominative_report/export?file=pdf&status=Completed', "_blank");
            break;
        }
    }

    function exportToXls(sts) {
        console.log('status cetak: ' + sts);
        switch (sts) {
            case '1000' :
                 window.open(base_url + 'report/nominative_report/export?file=xls&status=All', "_blank");
            break;
            case '1' :
                window.open(base_url + 'report/nominative_report/export?file=xls&status=Aktif', "_blank");
            break;
            case '2' :
                window.open(base_url + 'report/nominative_report/export?file=xls&status=Paid', "_blank");
            break;
            case '3' :
                window.open(base_url + 'report/nominative_report/export?file=xls&status=Completed', "_blank");
            break;
        }
    }

</script>
@stop
