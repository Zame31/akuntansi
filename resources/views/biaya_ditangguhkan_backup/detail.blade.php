@section('content')
@php
function convertMonth($data)
{
$bulan = array (
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);

echo $bulan[$data];
}
@endphp
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Accounting </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Prepaid Expense Detail </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Prepaid Expense Detail
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        @if ($type == 'new')
                            <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=new')" class="btn btn-success">Kembali</button>
                        @elseif($type == 'approve')
                            <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=approve')" class="btn btn-success">Kembali</button>
                        @elseif($type == 'success')
                            <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=success')" class="btn btn-success">Kembali</button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <div class="row">
                <div class="col-3">


                    <div class="form-group col-12">
                        <h6>Biaya Ditangguhkan Name</h6>
                        <div class="input-group">
                            {{$mi->bdd_name}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Tenor</h6>
                        <div class="input-group">
                            {{$mi->tenor}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Biaya Ditangguhkan note</h6>
                        <div class="input-group">
                            {{$mi->bdd_note}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Biaya Ditangguhkan Amount</h6>
                        <div class="input-group">
                            {{number_format($mi->bdd_amount,0,",",".")}}
                        </div>
                    </div>

                </div>
                <div class="col-9">
                    <div class="kt-portlet " id="scrollStyle" style="height: 500px; overflow: auto;">
                        <div class="kt-portlet__body">
                            <h5 class="zn-head-line-success" style="margin-bottom:40px;">Biaya Ditangguhkan Schedule</h5>
                            <div class="row" style="margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                <div class="col-2">
                                    <h6>Status</h6>
                                </div>
                                <div class="col-2">
                                    <h6>Month</h6>
                                </div>
                                <div class="col-2">
                                    <h6>Year</h6>
                                </div>
                                <div class="col-3 text-right">
                                    <h6>Amortisasi Amount </h6>
                                </div>
                                <div class="col-3 text-right">
                                    <h6>Outstanding</h6>
                                </div>


                            </div>
                            @foreach ($mc as $v)
                            <div class="row mt-3">
                                <div class="col-2">
                                    @if ($v->paid_status_id == 1)
                                    <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Not Paid
                                    </span>

                                    @elseif($v->paid_status_id == 2)
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            Paid
                                        </span>

                                        @endif
                                </div>
                                <div class="col-2" style="font-size: 14px;">
                                    <h6>{{convertMonth($v->month)}}</h6>
                                </div>
                                <div class="col-2" style="font-size: 14px;">
                                    {{$v->year}}
                                </div>
                                <div class="col-3 text-right" style="font-size: 14px;">
                                    {{number_format($v->amor_amount,0,",",".")}}
                                </div>
                                <div class="col-3 text-right" style="font-size: 14px;">
                                    {{number_format($v->last_os,0,",",".")}}
                                </div>

                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

@stop
