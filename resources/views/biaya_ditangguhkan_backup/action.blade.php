<script>


$('#month').val({{date('m')}});
$('#year').val({{date('Y')}});

var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  <button onclick="znClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                    Data</button>
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;

$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            inventory_type_id: {
                validators: {
                    notEmpty: {
                        message: 'Isi Biaya Ditangguhkan Name'
                    },
                }
            },
            inventory_desc: {
                validators: {
                    notEmpty: {
                        message: 'Isi Biaya Ditangguhkan Note'
                    },
                    stringLength: {
                          min:10,
                          max:200,
                          message: 'Minimal 10 Karakter Maksimal 200 Karakter'
                      }
                }
            },
            tenor: {
                validators: {
                    notEmpty: {
                        message: 'Tenor Belum Terisi'
                    },
                    stringLength: {
                            max:40,
                            message: 'Maksimal 40 Karakter'
                        }
                }
            },
            purchase_amount: {
                validators: {
                    notEmpty: {
                        message: 'Isi Biaya Ditangguhkan Amount'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

    $('#inventory_type_id').select2({
        placeholder:"Silahkan Pilih"
    });

    $('.zn-date').select2({
        placeholder:"Silahkan Pilih"
    });

    $('#month').select2({
        placeholder:"Silahkan Pilih"
    });

    $('#year').select2({
        placeholder:"Silahkan Pilih"
    });

var typeList = '{{$type}}';

if (typeList == 'new') {
    var act_url = '{{ route('data.biaya_ditangguhkan') }}';
}
else if(typeList == 'approve'){
    var act_url = '{{ route('data_approve.biaya_ditangguhkan') }}';
}
else if(typeList == 'success'){
    var act_url = '{{ route('data_success.biaya_ditangguhkan') }}';
}

var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { targets: [3,4,5], className: 'text-right' },
        { "orderable": false, "targets": 0 }
    ],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'cek', name: 'cek' },
        { data: 'action', name: 'action' },
        { data: 'bdd_name', name: 'bdd_name' },
        { data: 'bdd_note', name: 'bdd_note' },
        { data: 'bdd_amount', name: 'bdd_amount' },
        { data: 'amor_amount', name: 'amor_amount' },
        { data: 'tenor', name: 'tenor' },

        // { data: 'last_os', name: 'last_os' },
        // { data: 'is_active', name: 'is_active' }
    ]
});


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});



function znClose() {
    znIconboxClose();
    $("#form-data").data('bootstrapValidator').resetForm();
    $("#form-data")[0].reset();
    $("#dataAmotisasi").html('');
}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('biaya_ditangguhkan.index') }}?type=new');
}

// INVENTORY CHANGE
function setTenor(id) {

    $.ajax({
            type: "POST",
            url: '{{ route('refinventory.get') }}',
            data: {
                id:id
               },
           beforeSend: function() {
                $('.kt-spinner').addClass("zn-aktif");
           },
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {
              $('#tenor').val(v.amor_month);
            });
        }
    }).done(function( msg ) {
        $('.kt-spinner').removeClass("zn-aktif");
        $('#form-data').bootstrapValidator('revalidateField', 'tenor');
        // $('#form-data').bootstrapValidator("resetForm",true);
    });

}

function generateAmortisasi() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {
        loadingPage();
    $('#dataAmotisasi').html('');

    var tenor = $('#tenor').val();
    var purchase_amount = $('#purchase_amount').val();
    var month = $('#month').val();
    var year = $('#year').val();

    month = parseInt(month);
    year = parseInt(year);
    purchase_amount = parseInt(clearNumFormat(purchase_amount));
    tenor = parseInt(tenor);

    var n_amor = Math.round(purchase_amount/tenor);
    var endYear = (tenor/12);
    var outstanding = purchase_amount - n_amor;
    var yearlist = '';

    for (let i = 1; i <= tenor; i++) {
        yearlist = '';
        // MONTH YEAR
        var m = [];
        for (let im = 1; im <= 12; im++) {
            m[im] = (month == im) ? "selected" : "";
        }

        var y = [];
        for (let iyy = year; iyy <= year+endYear; iyy++) {
            m[iyy] = (year == iyy) ? "selected" : "";
        }

        for (let iy = 0; iy < endYear; iy++) {
            yearlist += '<option '+m[year+iy]+' value="'+(year+iy)+'">'+(year+iy)+'</option>';
        }
        // MONTH YEAR
        $('#dataAmotisasi').append(`
            <div class="col-3">
                <div class="form-group">
                    <select class="form-control zn-date" name="month[]">
                        <option `+m[1]+` value="1">January</option>
                        <option `+m[2]+` value="2">February</option>
                        <option `+m[3]+` value="3">March</option>
                        <option `+m[4]+` value="4">April</option>
                        <option `+m[5]+` value="5">Mey</option>
                        <option `+m[6]+` value="6">June</option>
                        <option `+m[7]+` value="7">July</option>
                        <option `+m[8]+` value="8">August</option>
                        <option `+m[9]+` value="9">September</option>
                        <option `+m[10]+` value="10">October</option>
                        <option `+m[11]+` value="11">November</option>
                        <option `+m[12]+` value="12">Desember</option>
                    </select>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <select class="form-control zn-date" name="year[]">
                        `+yearlist+`
                    </select>
                </div>
            </div>
            <div class="col-3">
                <input style="text-align:right;" value="`+numFormat(n_amor)+`" id="amor`+i+`" name="amor[]" placeholder="0"
                    class="form-control txt" onkeyup="convertToRupiah(this); reGenerateAmortisasi();" type="text">
            </div>
            <div class="col-4">
                <input readonly style="text-align:right;" value="`+numFormat(outstanding)+`" id="outstanding`+i+`" name="outstanding[]" placeholder="0"
                    class="form-control txt1" onkeyup="convertToRupiah(this)" type="text">
            </div>

        `);

        outstanding -= n_amor;

        month += 1;
        if (month > 12) {
            month = 1;
            year += 1;
        }


    }

    $('.zn-date').select2({
        placeholder:"Silahkan Pilih"
    });

    }

    endLoadingPage();
}

function reGenerateAmortisasi() {

   console.log($("#amor1").val());

    var newTenor = $('#tenor').val();

    var budget = $('#purchase_amount').val();
    budget = budget.split('.').join("");

    for (let z = 1; z <= newTenor; z++) {
        if (z == 1) {

            var first_n_amo = $("#amor"+z).val();

            console.log(first_n_amo);


            first_n_amo = first_n_amo.split('.').join("");

            $("#outstanding1").val(numFormat(budget-first_n_amo));

        }else{

            var out_amo_temp = $("#outstanding"+(z-1)).val();
            var nt_amo_temp = $("#amor"+z).val();

            out_amo_temp = out_amo_temp.split('.').join("");
            nt_amo_temp = nt_amo_temp.split('.').join("");

            out_amo_temp -= nt_amo_temp;
            $("#outstanding"+z).val(numFormat(out_amo_temp));
        }
    }
}

function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

        var tenor = $('#tenor').val();

        var outstanding = $('#outstanding'+tenor).val();

        if (outstanding != 0) {
            toastr.warning('Nilai Outstanding harus 0');
        }else {

            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('biaya_ditangguhkan.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },
                success: function (response) {
                    endLoadingPage();
                    if (response.rc == 0) {
                        znIconbox("Data Berhasil Disimpan",text,action);

                    }else {
                        toastr.warning(response.rm);

                    }
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        }
    } // endif

} // end function

function kirimsemua() {
    swal.fire({
        title: "Send Approval All",
        text: "Yakin akan mengirimkan semua data untuk dimintai persetujuan ?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#46C5EF",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then(function(result){
        if (result.value) {
            loadingPage();
            $.ajax({
                type: 'POST',
                url: base_url + '/kirimAprovalBdd?type=semua',
                success: function (res) {
                    endLoadingPage();
                    toastr.success("Send Approval All Success");
                }
            }).done(function( res ) {
                    table.ajax.url( '{{ route('data.biaya_ditangguhkan') }}' ).load();

            }).fail(function(res) {
                endLoadingPage();
                toastr.warning("Terjadi Kesalahan, Gagal Melakukan Send Approval All");
            });
        }
    });

}


function approveAll() {
    swal.fire({
        title: "Approval All",
        text: "Yakin akan Menyetujui semua data ?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#46C5EF",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then(function(result){
        if (result.value) {
            loadingPage();
            $.ajax({
                type: 'POST',
                url: base_url + '/giveAprovalBdd?type=semua',
                success: function (res) {
                    endLoadingPage();
                    toastr.success("Approval All Success");
                }
            }).done(function( res ) {
                    table.ajax.url( '{{ route('data_approve.biaya_ditangguhkan') }}' ).load();

            }).fail(function(res) {
                endLoadingPage();
                toastr.warning("Terjadi Kesalahan, Gagal Melakukan Approval All");
            });
        }
    });

}


function approveSelected() {

    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        toastr.warning("Minimal Pilih 1 data untuk melakukan 'Approval Selected' ");
    }else{

        swal.fire({
        title: "Approval Selected",
        text: "Yakin akan Menyetujui data yang dipilih ?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#46C5EF",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then(function(result){
        if (result.value) {
            loadingPage();
            $.ajax({
                type: 'POST',
                url: base_url + '/giveAprovalBdd?type=satu',
                data: {value: value},
                async: false,
                success: function (res) {
                    endLoadingPage();
                    toastr.success("Approval Selected Success");
                }
            }).done(function( res ) {
                    table.ajax.url( '{{ route('data_approve.biaya_ditangguhkan') }}' ).load();

            }).fail(function(res) {
                endLoadingPage();
                toastr.warning("Terjadi Kesalahan, Gagal Melakukan Approval Selected");
            });
        }
    });

    }

}

function kirim() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        toastr.warning("Minimal Pilih 1 data untuk melakukan 'Send Approval Selected' ");
    }else{
        swal.fire({
            title: "Send Approval Selected",
            text: "Yakin akan mengirimkan data yang dipilih untuk dimintai persetujuan ?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#46C5EF",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAprovalBdd?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {
                        endLoadingPage();
                        toastr.success("Send Approval Selected Success");
                    }
                }).done(function( res ) {
                    table.ajax.url( '{{ route('data.biaya_ditangguhkan') }}' ).load();

                }).fail(function(res) {
                    endLoadingPage();
                    toastr.warning("Terjadi Kesalahan, Gagal Melakukan Send Approval Selected");
                });
            }
        });
    }
}

function hapus() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        toastr.warning("Minimal Pilih 1 data untuk melakukan 'Delete Selected' ");
    }else{
        swal.fire({
            title: "Deleted selected",
            text: "Yakin akan menghapus data yang dipilih ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F8BD89",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/hapusAprovalBdd?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        toastr.success("Delete Selected Success");
                    }
                }).done(function( res ) {
                    table.ajax.url( '{{ route('data.biaya_ditangguhkan') }}' ).load();

                }).fail(function(res) {
                    endLoadingPage();
                    toastr.warning("Terjadi Kesalahan, Gagal Melakukan Delete Selected");
                });
            }
        });
    }
}

function hapussemua() {
    swal.fire({
        title: "Delete All",
        text: "Yakin akan menghapus Semua Data ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F8BD89",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then(function(result){
        if (result.value) {
            loadingPage();
            $.ajax({
                type: 'POST',
                url: base_url + '/hapusAprovalBdd?type=semua',
                success: function (res) {
                    endLoadingPage();
                    toastr.success("Delete All Success");
                }
            }).done(function( res ) {
                table.ajax.url( '{{ route('data.biaya_ditangguhkan') }}' ).load();

            }).fail(function(res) {
                endLoadingPage();
                toastr.warning("Terjadi Kesalahan, Gagal Melakukan Delete All");
            });
        }
    });
}


function rejectAll() {
    swal.fire({
        title: "Reject All",
        text: "Yakin Akan Menolak Semua Data ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F8BD89",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then(function(result){
        if (result.value) {
            loadingPage();
            $.ajax({
                type: 'POST',
                url: base_url + '/rejectAprovalBdd?type=semua',
                success: function (res) {
                    endLoadingPage();
                    toastr.success("Reject All Success");
                }
            }).done(function( res ) {
                table.ajax.url( '{{ route('data_approve.biaya_ditangguhkan') }}' ).load();

            }).fail(function(res) {
                endLoadingPage();
                toastr.warning("Terjadi Kesalahan, Gagal Melakukan Reject All");
            });
        }
    });
}


function rejectSelected() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        toastr.warning("Minimal Pilih 1 data untuk melakukan 'Send Approval Selected' ");
    }else{
        swal.fire({
        title: "Reject Selected",
        text: "Yakin Akan Menolak Data yang Dipilih ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F8BD89",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then(function(result){
        if (result.value) {
            loadingPage();
            $.ajax({
                type: 'POST',
                url: base_url + '/rejectAprovalBdd?type=satu',
                data: {value: value},
                async: false,
                success: function (res) {
                    endLoadingPage();
                    toastr.success("Reject Selected Success");
                }
            }).done(function( res ) {
                table.ajax.url( '{{ route('data_approve.biaya_ditangguhkan') }}' ).load();

            }).fail(function(res) {
                endLoadingPage();
                toastr.warning("Terjadi Kesalahan, Gagal Melakukan Reject Selected");
            });
        }
    });
    }

}
</script>
