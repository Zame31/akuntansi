@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Accounting </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    {{$tittle}}</a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form {{$tittle}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">

                        <button onclick="saveData()" class="btn btn-success">Simpan</button>

                          <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.index') }}')" class="btn btn-danger">Kembali</button>


                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form id="form-data" enctype="multipart/form-data">
              <input type="hidden" name="get_id" id="get_id" value="{{$get_id}}">
                <div class="row">
                    <div class="col-4">
                        <div class="row">
                            <div class="col-8">
                                <div class="form-group">
                                    <label>Prepaid Expense Name</label>
                                    <select class="form-control" name="inventory_type_id" id="inventory_type_id">
                                    <option></option>
                                        @php
                                        $inven_type = \DB::select("SELECT * FROM ref_bdd where is_active = 't'");
                                        @endphp
                                        @foreach($inven_type as $item)
                                        <option value="{{$item->id}}">{{$item->bdd_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>Tenor</label>
                                    <div class="kt-spinner kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                        <input onkeyup="convertToRupiah(this)" maxlength="2" type="text" class="form-control" id="tenor" name="tenor">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Prepaid Expense Note</label>
                            <textarea name="inventory_desc" id="inventory_desc" class="form-control" rows="4" cols="80"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Prepaid Expense Amount</label>
                            <input type="text" class="form-control" maxlength="15" onkeyup="convertToRupiah(this)" id="purchase_amount" name="purchase_amount">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Amortisasi Month</label>
                                    <select class="form-control zn-date" name="month" id="month">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">Mey</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Amortisasi Year</label>
                                    <select class="form-control zn-date" name="year" id="year">
                                        <option value="2019">2019</option>
                                        <option value="2018">2018</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label>Last OS</label>
                            <input type="text" class="form-control" id="last_os" name="last_os">
                        </div> --}}

                        <button onclick="generateAmortisasi();" class="btn btn-success">Generate Schedule</button>
                    </div>

                    <div class="col-8">


                        <div class="kt-portlet" id="scrollStyle" style="height: 500px; overflow: auto;">
                            <div class="kt-portlet__body">
                                <h5 class="zn-head-line-success" style="margin-bottom:40px;">Biaya Ditangguhkan Schedule</h5>
                                <div class="row"
                                    style="margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                    <div class="col-3">
                                        <h6>Month</h6>
                                    </div>
                                    <div class="col-2">
                                        <h6>Year</h6>
                                    </div>
                                    <div class="col-3 text-right">
                                        <h6>Amortisasi Amount </h6>
                                    </div>
                                    <div class="col-4 text-right">
                                        <h6>Outstanding</h6>
                                    </div>
                                </div>
                                <div class="row" id="dataAmotisasi">
                                    @if ($type == 'edit')
                                        @foreach ($mc as $key => $v)

                                        <div class="col-3">
                                                <div class="form-group">
                                                    <select class="form-control zn-date" name="month[]">
                                                        <option {{($v->month == '1' ? "selected":"")}} value="1">January</option>
                                                        <option {{($v->month == '2' ? "selected":"")}} value="2">February</option>
                                                        <option {{($v->month == '3' ? "selected":"")}} value="3">March</option>
                                                        <option {{($v->month == '4' ? "selected":"")}} value="4">April</option>
                                                        <option {{($v->month == '5' ? "selected":"")}} value="5">Mey</option>
                                                        <option {{($v->month == '6' ? "selected":"")}} value="6">June</option>
                                                        <option {{($v->month == '7' ? "selected":"")}} value="7">July</option>
                                                        <option {{($v->month == '8' ? "selected":"")}} value="8">August</option>
                                                        <option {{($v->month == '9' ? "selected":"")}} value="9">September</option>
                                                        <option {{($v->month == '10' ? "selected":"")}} value="10">October</option>
                                                        <option {{($v->month == '11' ? "selected":"")}} value="11">November</option>
                                                        <option {{($v->month == '12' ? "selected":"")}} value="12">Desember</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <select class="form-control zn-date" name="year[]">
                                                      @for ($i=$mc_y_min->year_min; $i <= $mc_y_max->year_max; $i++)
                                                        <option {{($v->year == $i ? "selected":"")}} value="{{$i}}">{{$i}}</option>
                                                      @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                            <input style="text-align:right;" value="{{number_format($v->amor_amount,0,",",".")}}" id="amor{{$key+1}}" name="amor[]" placeholder="0"
                                                    class="form-control txt" onkeyup="convertToRupiah(this); reGenerateAmortisasi();" type="text">
                                            </div>
                                            <div class="col-4">
                                                <input readonly style="text-align:right;" value="{{number_format($v->last_os,0,",",".")}}" id="outstanding{{$key+1}}" name="outstanding[]" placeholder="0"
                                                    class="form-control txt1" onkeyup="convertToRupiah(this)" type="text">
                                            </div>
                                        @endforeach
                                    @endif



                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var type = '{{$type}}';
    @if ($type == 'edit')

    if (type == 'edit') {
      console.log(type);
        $('#inventory_type_id').val('{{$mi->bdd_id}}');
        $('#tenor').val('{{$mi->tenor}}');
        $('#inventory_desc').val('{{$mi->bdd_note}}');
        $('#purchase_amount').val(numFormat('{{$mi->bdd_amount}}'));
    }
    @endif
</script>
@include('biaya_ditangguhkan.action')
@stop
