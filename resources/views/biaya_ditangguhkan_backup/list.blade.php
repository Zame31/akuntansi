@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Accounting </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{$tittle}} </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            {{$tittle}}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">

                            @if ($type == 'new')
                                <a href="#" onclick="kirimsemua();" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a>
                                <a href="#" onclick="kirim();" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a>
                                <a href="#" onclick="hapussemua();" class="btn btn-pill btn-danger" style="color: white;">Delete All</a>
                                <a href="#" onclick="hapus();" class="btn btn-pill btn-danger" style="color: white;">Delete Selected</a>
                                <button onclick="loadNewPage('{{ route('biaya_ditangguhkan.form','create') }}')" class="btn btn-pill btn-success">Tambah Data</button>
                            @elseif($type == 'approve')
                                <a href="#" onclick="approveAll();" class="btn btn-pill btn-primary" style="color: white;">Approval All</a>
                                <a href="#" onclick="approveSelected();" class="btn btn-pill btn-primary" style="color: white;">Approval Selected</a>
                                <a href="#" onclick="rejectAll();" class="btn btn-pill btn-danger" style="color: white;">Reject All</a>
                                <a href="#" onclick="rejectSelected();" class="btn btn-pill btn-danger" style="color: white;">Reject Selected</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label></th>
                                    <th width="30px">Action</th>
                                    <th>Prepaid Expense Name</th>
                                    <th>Prepaid Expense Note</th>
                                    <th>Prepaid Expense Amount</th>
                                    <th>Amor Amount</th>
                                    <th>Tenor</th>
                                    {{-- <th>last OS</th> --}}
                                    {{-- <th>Status</th> --}}
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('biaya_ditangguhkan.action')
@stop
