<!--begin::Modal-->
<div class="modal fade" id="modal_list_filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped- table-hover table-checkable" id="table_filter">
                        <thead>
                            <tr>
                                <th width="30px" class="text-center">Action</th>
                                <th class="text-center">Police Number</th>
                                <th class="text-center">Customer Name</th>
                                <th class="text-center">Type of Insurance</th>
                                <th class="text-center">TSI</th>
                                <th class="text-center">Gross Premium</th>
                                <th class="text-center">Discount</th>
                                <th class="text-center">Nett Premium</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
