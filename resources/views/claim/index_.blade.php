@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Sales</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Claim </a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form Claim
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="return simpan()" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                {{-- <div class="dropdown dropdown-inline">
                    <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flaticon-more-1"></i>
                    </a>
                </div> --}}
            </div>
        </div>
        <div class="kt-portlet__body">
           <form id="form-new-customer" enctype="multipart/form-data">
            <div class="row">
                <div class="col-4">
                        <div class="form-group">
                                <label for="exampleTextarea">Claim Status</label>
                                <select class="form-control kt-select2" name="claim_id" id="claim_id">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach($ref_claim as $item)
                                    <option value="{{$item->id}}">{{$item->status_definition}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">Silahkan pilih claim status</div>
                            </div>
                            <div class="form-group">
                                    <label for="exampleTextarea">Claim Notes</label>
                                    <textarea class="form-control" id="notes" maxlength="200" rows="6" style="height: 130px;" name="notes"></textarea>
                                    <div class="invalid-feedback">Silahkan isi claim notes</div>
                                </div>
                    <div class="form-group">
                        <label>PLA Document</label>
                        <input type="file" class="form-control" name="file_pla" id="file_pla">
                        <div class="invalid-feedback" id="ss1">Silahkan Upload Dokumen</div>
                        <div class="invalid-feedback" id="ff1">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                        <div class="invalid-feedback" id="mm1">Max Size Dokumen 2 Mb</div>
                    </div>
                    <div class="form-group">
                                <label for="exampleTextarea">Claim Start Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" value="{{date('d M Y')}}" id="tgl_claim" name="tgl_claim" readonly
                                        placeholder="Pilih Tanggal" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                </div>
                            </div>


                </div>
                <div class="col-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Police Number</label>
                                <input type="text" class="form-control" id="police_number" onkeypress="return huruf(event)" name="police_number">
                                <div class="invalid-feedback" id="mm3">Silahkan isi short name</div>
                            </div>

                            <div class="form-group">
                                    <label for="exampleTextarea">Chronology</label>
                                    <textarea class="form-control" id="kronologi" maxlength="200" rows="6" style="height: 130px;" name="kronologi"></textarea>
                                    <div class="invalid-feedback">Silahkan isi address</div>
                            </div>
                    <div class="form-group">
                        <label>DLA Document</label>
                        <input type="file" class="form-control" name="file_dla" id="file_dla">
                        <div class="invalid-feedback" id="ss2">Silahkan Upload Dokumen</div>
                        <div class="invalid-feedback" id="ff2">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                        <div class="invalid-feedback" id="mm2">Max Size Dokumen 2 Mb</div>
                    </div>
                    <div class="form-group">
                                <label for="exampleTextarea">Claim End Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" value="{{date('d M Y')}}" id="tgl_claim_end" name="tgl_claim_end" readonly
                                        placeholder="Pilih Tanggal" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                </div>
                            </div>

                </div>
                <div class="col-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Date Of Loss</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" value="{{date('d M Y')}}" id="tgl_loss" name="tgl_loss" readonly
                                        placeholder="Pilih Tanggal" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleTextarea">Estimation of Loss</label>
                                <input type="text" class="form-control money" style="text-align: right;" id="estimation"  onkeypress="return huruf(event)" name="estimation" maxlength="50">
                                <div class="invalid-feedback" id="mm6">Silahkan isi Estimation of Loss</div>
                            </div>
                            <div class="form-group">
                                <label for="exampleTextarea">Cause of Loss</label>
                                <select class="form-control kt-select2" name="loss_id" id="loss_id">
                                    <option value="">Silahkan Pilih</option>
                                </select>
                                <div class="invalid-feedback">Silahkan pilih Cause of Loss</div>
                            </div>
                    <div class="form-group">
                        <label>LOD Document</label>
                        <input type="file" class="form-control" name="file_lod" id="file_lod">
                        <div class="invalid-feedback" id="ss3">Silahkan Upload Dokumen</div>
                        <div class="invalid-feedback" id="ff3">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                        <div class="invalid-feedback" id="mm3">Max Size Dokumen 2 Mb</div>
                    </div>
                    <div class="form-group">
                        <label>Claim Document</label>
                        <input type="file" class="form-control" name="file_claim" id="file_claim">
                        <div class="invalid-feedback" id="ss4">Silahkan Upload Dokumen</div>
                        <div class="invalid-feedback" id="ff4">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                        <div class="invalid-feedback" id="mm4">Max Size Dokumen 2 Mb</div>
                    </div>

                </div>
            </div>
        </form>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(".money").maskMoney({
        prefix:'',
        allowNegative: false,
        thousands:'.',
        decimal:',',
        affixesStay: false
    });
    
$(document).ready(function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();


    var max="'"+ dd +" "+mm+" "+yyyy+"'";
    console.log(max);
    $('#tgl_claim').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

    $('#tgl_claim_end').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

    $('#tgl_loss').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

});

$('#segment_beban').select2({
    placeholder: "Silahkan Pilih"
});
$('#bank').select2({
    placeholder: "Silahkan Pilih"
});

 $('#catatan').keyup(function() {
        var panjang = this.value.length;

        if(panjang==80){
            $('#invoice').addClass( "is-invalid" );
            $('#co').hide();
            $('#ck').show();
        }else{
            $('#invoice').removeClass( "is-invalid" );
            $('#co').hide();
            $('#ck').hide();
        }
    });
var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  <button onclick="znClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                    Data</button>
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;
function znClose() {
    znIconboxClose();
    //$("#form-new").data('bootstrapValidator').resetForm();
    $("#form-new")[0].reset();
    location.reload();
    //$("#dataAmotisasi").html('');
}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('list_tr_baru') }}');
}

var _create_form = $("#form-new");

function simpan() {
    loadingPage();


    if($('#claim_id').val()===''){
        endLoadingPage();
        $( "#claim_id" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#claim_id").removeClass( "is-invalid" );
    }



    if($('#notes').val()===''){
        endLoadingPage();
        $( "#notes" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#notes").removeClass( "is-invalid" );

    }

    if($('#file_pla').val()===''){
        endLoadingPage();
        $('#ss1').show();
        $('#ff1').hide();
        $('#mm1').hide();
        return false;

    }else{
        var size=$('#file_pla')[0].files[0].size;
        console.log(size);
        var extension=$('#file_pla').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            endLoadingPage();
            $( "#file_pla" ).addClass("is-invalid");
            $('#ss1').hide();
            $('#ff1').hide();
            $('#mm1').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
            endLoadingPage();
            $( "#file_pla" ).addClass("is-invalid");
            $('#ss1').hide();
            $('#ff1').show();
            $('#mm1').hide();
            return false;
        }

        $( "#file_pla" ).removeClass( "is-invalid" );

    }

    if($('#tgl_claim').val()===''){
        endLoadingPage();
        $( "#tgl_claim" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_claim").removeClass( "is-invalid" );

    }

    if($('#kronologi').val()===''){
        endLoadingPage();
        $( "#kronologi" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#kronologi").removeClass( "is-invalid" );

    }

    if($('#file_dla').val()===''){
        endLoadingPage();
        $('#ss2').show();
        $('#ff2').hide();
        $('#mm3').hide();
        return false;
    }else{
        var size=$('#file_dla')[0].files[0].size;
        console.log(size);
        var extension=$('#file_dla').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            endLoadingPage();
            $( "#file_dla" ).addClass("is-invalid");
            $('#ss2').hide();
            $('#ff2').hide();
            $('#mm2').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
            endLoadingPage();
            $( "#file_dla" ).addClass("is-invalid");
            $('#ss2').hide();
            $('#ff2').show();
            $('#mm2').hide();
            return false;
        }

        $( "#file_dla" ).removeClass( "is-invalid" );

    }

    if($('#tgl_claim_end').val()===''){
        endLoadingPage();
        $( "#tgl_claim_end" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_claim_end").removeClass( "is-invalid" );

    }
    if($('#tgl_loss').val()===''){
        endLoadingPage();
        $( "#tgl_loss" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_loss").removeClass( "is-invalid" );

    }

    if($('#estimation').val()===''){
        endLoadingPage();
        $( "#estimation" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#estimation").removeClass( "is-invalid" );

    }

     if($('#loss_id').val()===''){
        endLoadingPage();
        $( "#loss_id" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#loss_id").removeClass( "is-invalid" );

    }

     if($('#file_lod').val()===''){
        endLoadingPage();
        $('#ss3').show();
        $('#ff3').hide();
        $('#mm3').hide();
        return false;
    }else{
        var size=$('#file_lod')[0].files[0].size;
        console.log(size);
        var extension=$('#file_lod').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            endLoadingPage();
            $( "#file_lod" ).addClass("is-invalid");
            $('#ss3').hide();
            $('#ff3').hide();
            $('#mm3').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
            endLoadingPage();
            $( "#file_lod" ).addClass("is-invalid");
            $('#ss3').hide();
            $('#ff3').show();
            $('#mm3').hide();
            return false;
        }

        $( "#file_lod" ).removeClass( "is-invalid" );

    }

    if($('#file_claim').val()===''){
        endLoadingPage();
        $('#ss4').show();
        $('#ff4').hide();
        $('#mm4').hide();
        return false;
    }else{
        var size=$('#file_claim')[0].files[0].size;
        console.log(size);
        var extension=$('#file_claim').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            endLoadingPage();
            $( "#file_claim" ).addClass("is-invalid");
            $('#ss4').hide();
            $('#ff4').hide();
            $('#mm4').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
            endLoadingPage();
            $( "#file_claim" ).addClass("is-invalid");
            $('#ss4').hide();
            $('#ff4').show();
            $('#mm4').hide();
            return false;
        }

        $( "#file_claim" ).removeClass( "is-invalid" );

    }

    var _form_data = new FormData(_create_form[0]);
    $.ajax({
        type: 'POST',
        url: base_url + '/insert_claim',
        data: _form_data,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (res) {
            //var parse = $.parseJSON(res);
            if(res.rc==1){
                endLoadingPage();
                znIconbox("Data Berhasil Disimpan",text,action);
                /*

                swal.fire({
                    title: 'Info',
                    text: "Berhasil",
                    type: 'success',
                    confirmButtonText: 'Tutup',
                    reverseButtons: true
                }).then(function(result){
                    if (result.value) {

                        //_create_form[0].reset();
                        //location.reload();
                    }
                });
                */
            }else{
                swal.fire("Info",obj.rm,"info");
            }

        }
    }).done(function( res ) {
        var obj = JSON.parse(res);
        console.log(res['data']);
        endLoadingPage();

        if(obj.rc==1){
            znIconbox("Data Berhasil Disimpan",text,action);
        }else{
            swal.fire("Info",obj.rm,"info");
        }

       }).fail(function(res) {
        endLoadingPage();
        swal.fire("Error","Terjadi Kesalahan!","error");
    });


}

var counter1_ = 1;
var def_ = 'new';
function add_row_new_dok(){
    let value = [];
    $(".dokno > option:selected").each(function() {

    value.push(this.value);
    //Add operations here

    });


    if(def_=='new'){
        var file="#file" + counter1_;

    }else{
        var aa =counter1_ -1;
        var file="#file" + aa;
        var ss="#ss" + aa;
        var ff="#ff" + aa;
        var mm="#mm" + aa;
        var jns_dok="#jns_dok" + aa;

    }
    //document.getElementById("videoUploadFile").files.length == 0


    if($('#file').val()===''){
        $("#file").addClass("is-invalid");
        $('#ss').show();
        $('#ff').hide();
        $('#mm').hide();
        return false;
    }else{
        var size=$('#file')[0].files[0].size;
        console.log(size);
        var extension=$('#file').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').hide();
            $('#mm').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').show();
            $('#mm').hide();
            return false;
        }

        $( "#file" ).removeClass( "is-invalid" );

    }
    if($(jns_dok).val() ===''){
        $(jns_dok).addClass( "is-invalid" );
        return false;
    }else{
        $( jns_dok ).removeClass( "is-invalid" );

    }
    if($(file).val() ===''){
        $(file).addClass( "is-invalid" );
        $(ss).show();
        $(ff).hide();
        $(mm).hide();
        return false;
    }else{
        if(typeof  $(file).val() === 'undefined'){
            console.log('eweuh');
        }else{
            console.log($(file).val());
            var size=$(file)[0].files[0].size;
            console.log(size);
            var extension=$(file).val().replace(/^.*\./, '');
            console.log(extension);
            if(size >= 2000002){
                $( file ).addClass("is-invalid");
                $(ss).hide();
                $(ff).hide();
                $(mm).show();
                return false;

            }
            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
                $( file ).addClass("is-invalid");
                $(ss).hide();
                $(ff).show();
                $(mm).hide();
                return false;
                }
            $( file ).removeClass( "is-invalid" );
        }


    }


    var newRow = $("<tr>");
    var cols = "";
    cols +='<div class="row mt-2">';
    cols += '<div class="col-5"><select class="form-control kt-select2 init-select2 dokno" name="jns_dok[]" id="jns_dok'+counter1_+'"></select><div class="invalid-feedback">Silahkan Pilih Jenis Dokumen</div></div>';
    cols += ' <div class="offset-1 col-5"><input type="file"  class="form-control" name="file[]" id="file'+counter1_+'"><div class="invalid-feedback" id="ss'+counter1_+'">Silahkan Upload Dokumen</div><div class="invalid-feedback" id="ff'+counter1_+'">Format Dokumen pdf,jpeg,jpg,png,xls,xlxs</div><div class="invalid-feedback" id="mm'+counter1_+'">Max Size Dokumen 2 Mb</div></div>';
    cols += '<div class="col-1"><button type="button" class="btn btn-danger btn-elevate btn-icon ibtnDel_"><i class="la la-trash"></i></button></div>';
    cols +='</div>';

    newRow.append(cols);
    $("table.table-dok").append(newRow);
    //$(".table-dok").append(cols);
    var jns_dok="#jns_dok" + counter1_;
    var _items='';
        $.ajax({
            type: 'GET',
            url: base_url + '/ref_dokumen',
            data: {value: value},
            async: false,
            success: function (res) {
                var data = $.parseJSON(res);
                if(data.length!=0){
                    _items='<option value="">Silahkan pilih</option>';
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
                    });
                    console.log(jns_dok);
                    $(jns_dok).html(_items);
                }else{
                    console.log('ga ada data');
                    $(jns_dok).closest("tr").remove();
                }
            }
        });
    $(jns_dok).select2();
    def_='old';
    counter1_ ++;
    console.log('asup');
}




$("table.table-dok").on("click", ".ibtnDel_", function (event) {
        $(this).closest("tr").remove();
        //$( "ph" ).eq( 1 ).removeClass();
       // counter_ -= 1
      //  calculateSum();
    });

</script>
<!-- end:: Content -->
@stop
