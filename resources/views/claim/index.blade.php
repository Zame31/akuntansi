@section('content')
<style>
* {
  box-sizing: border-box;
}

/* body {
  font: 16px Arial;
} */

/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}

input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}

.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff;
  border-bottom: 1px solid #d4d4d4;
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9;
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important;
  color: #ffffff;
}
</style>
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Sales</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Claim </a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-form-claim">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form Claim
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar d-none" id="container-search-ulang">
                <div class="row">
                    <div class="col-12">
                        <a href="#" onclick="search_ulang();" class="btn btn-pill btn-primary"
                            style="color: white;">Search Ulang</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
           <form id="form-new-customer" enctype="multipart/form-data">

            <div id="container-filter">
                <div class="form-group row mb-3">
                    <label class="col-2 col-form-label">Select Type of Insurance </label>
                    <div class="col-6">
                        <select class="form-control kt-select2 init-select2" name="product_insurance" id="product_insurance" onchange="get_detail_proudct_list(this);">
                            <option value="1000" disabled selected>Select Type Of Insurace</option>
                            @forelse ($products as $item)
                                <option value="{{ $item->id }}">{{ $item->definition }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label class="col-2 col-form-label">Cari Polis Berdasarkan </label>
                    <div class="col-6" id="container-filter-polis">
                        <select class="form-control kt-select2 init-select2" name="filter_polis" id="filter_polis">
                            <option value="1000" disabled selected>Select Filter Polis</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label class="col-2 col-form-label">Masukkan Keyword </label>
                    <div class="col-6" id="container-keyword">
                        <input type="text" class="form-control" id="keyword" name="keyword">
                    </div>
                </div>

                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-6">
                        <button class="btn btn-outline-primary" onclick="search_polis()"> Submit </button>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-md-4">

                    <div class="form-group" style="display: none;" id="container-police">
                        <label for="exampleTextarea">Police Number</label>
                        @if($id)
                        <input type="text" class="form-control autocomplete" readonly id="police_number2" name="police_number" value="{{$id}}">
                        @else
                        <input type="text" class="form-control autocomplete" placeholder="Search Police Number ..." id="police_number" name="police_number">
                        @endif
                        <div class="invalid-feedback" id="mm3">Silahkan isi short name</div>
                    </div>

                    <div class="form-group flat" style="display: none;">
                            <label>Customer</label>
                            <input type="text" readonly class="form-control" id="customer">
                    </div>
                </div>


                    <div class="col-md-4 flat" style="display: none;">
                        <div class="form-group">
                            <label>Product Name</label>
                            <input type="text" readonly class="form-control" id="product">
                        </div>
                        <div class="form-group">
                            <label>Agent</label>
                            <input type="text" readonly class="form-control" id="agent">
                        </div>



                    </div>
                    <div class="col-md-4" style="display: none;">
                        <div class="form-group">
                            <label>Quotation Number</label>
                            <input type="text" readonly class="form-control" id="quotation">
                        </div>
                        <div class="form-group">
                            <label>Bank Account No</label>
                            <input type="text" readonly class="form-control" id="bank">
                        </div>
                    </div>



            </div>

            <div class="row">
                <div class="col-md-4 flat" style="display: none;">
                    <button class="btn btn-pill btn-success" onclick="detailProduct()"> Lihat Detail </button>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>
       <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="history" style="display: none;">
            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                             List History Claim
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <a href="#" onclick="showModalAdd();" class="btn btn-pill btn-primary"
                                    style="color: white;">Add New Claim</a>
                                <a href="#" onclick="kirim_claim();" class="btn btn-pill btn-danger" style="color: white;">Send Approve Selected</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="claim_table">
                        <thead>
                            <tr>
                              <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" id="example-select-all" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </th>
                                <th>Action</th>
                                <th>ID</th>
                                <th>Polis No</th>
                                <th>Start Date Claim</th>
                                <th>Start End Claim</th>
                                <th>Estimation of Loss</th>
                                <th>Claim Amount</th>
                                <th>Claim Notes</th>
                                <th>Claim Status</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        @include('claim.modal')
        @include('claim.modal_list_filter')
        @include('claim.modal_detail_product')
<script type="text/javascript">
    var DT = $("#table_filter").DataTable({
        columnDefs: [
            {
                targets: [4, 5, 6, 7],
                className: 'text-right'
            }
        ]
    });
  $('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});

  if($("#police_number2").val()==='' || typeof $("#police_number2").val() === "undefined" ){
    }else{
    data_retview();
  }

    function showModalAdd() {
        $("#form-new")[0].reset();
        if($("#police_number2").val()==='' || typeof $("#police_number2").val() === "undefined" ){

          $("#police_number1").val($("#police_number").val());
          }else{
          $("#police_number1").val($("#police_number2").val());

        }
        $('#modal').modal('show');
    }
    function editdata(id) {
        $("#form-new")[0].reset();
        $.ajax({
            type: "GET",
            url: base_url + '/byClaim?id='+id,
            beforeSend: function () {
                loadingPage();
            },

            success: function (result) {
                console.log(result);
                console.log('data');
                console.log(result.length);
                var data = $.parseJSON(result);
                endLoadingPage();
                if(data.length!=0){

                    console.log(data);
                    /*
                    [{"id":1,"polis_no":"12345","claim_start_date":"2019-11-21","claim_amount":"120000","duration":12,"claim_end_date":"2019-11-22","user_crt_id":null,"url_doc":null,"cause_loss_id":null,"loss_amount":"12333","loss_date":null,"quotation_no":null,"url_doc_lod":null,"kronologi_kejadian":null,"url_doc_pla":null,"url_doc_dla":null,"":"qqq","claim_status_id":null}]
                    */
                    const monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                      ];

                      var today = new Date(data[0]['loss_date']);
                      var dd = today.getDate();
                      var mm = today.getMonth(); //January is 0!
                      var yyyy = today.getFullYear();

                      var today1 = new Date(data[0]['claim_start_date']);
                      var dd1 = today1.getDate();
                      var mm1 = today1.getMonth(); //January is 0!
                      var yyyy1 = today1.getFullYear();

                      var today2 = new Date(data[0]['claim_end_date']);
                      var dd2 = today2.getDate();
                      var mm2 = today2.getMonth(); //January is 0!
                      var yyyy2 = today2.getFullYear();


                      var loss_date=dd +" "+monthNames[mm]+" "+yyyy;
                      var claim_start_date=dd1 +" "+monthNames[mm1]+" "+yyyy1;
                      var claim_end_date=dd2 +" "+monthNames[mm2]+" "+yyyy2;

                        $('.flat').show();
                        $('#history').show();
                        $('#id').val(data[0]['id']);
                        $('#police_number1').val(data[0]['polis_no']);
                        $('#notes').val(data[0]['claim_notes']);
                        $('#claim_id').val(data[0]['claim_status_id']);
                        $('#kronologi').val(data[0]['kronologi_kejadian']);
                        $('#loss_id').val(data[0]['cause_loss_id']);

                        var estimation=data[0]['loss_amount'].replace(/\./g, ",");

                        $('#estimation').val(estimation).trigger('mask.maskMoney');

                        $('#tgl_loss').val(loss_date);
                        $('#tgl_claim').val(claim_start_date);
                        $('#tgl_claim_end').val(claim_end_date);

                        var claim_amount=data[0]['claim_amount'].replace(/\./g, ",");

                        $('#claim_amount').val(claim_amount).trigger('mask.maskMoney');

                        $('#pla').html('<a target="_blank" href="/upload_claim/'+data[0]['id']+'_pla_'+data[0]['url_doc_pla']+'">Download File</a>');
                        $('#dla').html('<a target="_blank" href="/upload_claim/'+data[0]['id']+'_dla_'+data[0]['url_doc_dla']+'">Download File</a>');
                        $('#lod').html('<a target="_blank" href="/upload_claim/'+data[0]['id']+'_lod_'+data[0]['url_doc_lod']+'">Download File</a>');
                        $('#claim').html('<a target="_blank" href="/upload_claim/'+data[0]['id']+'_claim_'+data[0]['url_doc']+'">Download File</a>');

                }else{
                    $('.flat').hide();
                    $('#history').hide();
                    toastr.error("Polis Number Not Found");
                }
            }
        }).done(function (msg) {
             endLoadingPage();
        }).fail(function (msg) {
             endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
        $('#modal').modal('show');
    }
    function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

var input = document.getElementById("police_number");
    input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
       event.preventDefault();
       data();


      }
    });

function data(){
    $.ajax({
            type: "GET",
            url: base_url + '/byPolis?id='+$("#police_number").val(),
            beforeSend: function () {
                loadingPage();
            },

            success: function (result) {
                console.log(result);
                console.log('data');
                console.log(result.length);
                var data = $.parseJSON(result);
                endLoadingPage();
                if(data.length!=0){
                    console.log(data);
                        $('.flat').show();
                        $('#history').show();
                        $('#product').val(data[0]['definition']);
                        $('#agent').val(data[0]['agent']);
                        $('#customer').val(data[0]['full_name']);
                        $('#quotation').val(data[0]['qs_no']);
                        $('#bank').val(data[0]['bank']);
                }else{
                    $('.flat').hide();
                    $('#history').hide();
                    toastr.error("Polis no not found");
                }
            }
        }).done(function (msg) {
             endLoadingPage();
        }).fail(function (msg) {
             endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });

        var table = $('#claim_table').DataTable({
                        aaSorting: [],
                        destroy: true,
                        processing: true,
                        serverSide: true,
                        columnDefs: [
                            { targets: [5,6], className: 'text-right' },
                            { "orderable": false, "targets": 0 }],
                        ajax: {
                            "url" : base_url + '/history_claim?id=' + $("#police_number").val(),
                            "error": function(jqXHR, textStatus, errorThrown)
                                {
                                    toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                                }
                            },
                        columns: [
                            { data: 'cek', name: 'cek' },
                            { data: 'action', name: 'action' },
                            { data: 'id', name: 'id' },
                            { data: 'polis_no', name: 'polis_no' },
                            { data: 'claim_start_date', name: 'claim_start_date' },
                            { data: 'claim_end_date', name: 'claim_end_date' },
                            { data: 'loss_amount', name: 'loss_amount' },
                            { data: 'claim_amount', name: 'claim_amount' },
                            { data: 'claim_notes', name: 'claim_notes' },
                            { data: 'status_definition', name: 'status_definition' },
                            { data: 'workflow_status_id', name: 'workflow_status_id' },
                        ]
                    });
}

function data_retview(){
    $.ajax({
            type: "GET",
            url: base_url + '/byPolis?id='+$("#police_number2").val(),
            beforeSend: function () {
                loadingPage();
            },

            success: function (result) {
                console.log(result);
                console.log('data');
                console.log(result.length);
                var data = $.parseJSON(result);
                endLoadingPage();
                if(data.length!=0){
                    console.log(data);
                        $('.flat').show();
                        $('#history').show();
                        $('#product').val(data[0]['definition']);
                        $('#agent').val(data[0]['agent']);
                        $('#customer').val(data[0]['full_name']);
                        $('#quotation').val(data[0]['qs_no']);
                        $('#bank').val(data[0]['bank']);
                }else{
                    $('.flat').hide();
                    $('#history').hide();
                    toastr.error("Polis no not found");
                }
            }
        }).done(function (msg) {
             endLoadingPage();
        }).fail(function (msg) {
             endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });

        var table = $('#claim_table').DataTable({
                        aaSorting: [],
                        destroy: true,
                        processing: true,
                        serverSide: true,
                        columnDefs: [
                            { targets: [5,6], className: 'text-right' },
                            { "orderable": false, "targets": 0 }],
                        ajax: {
                            "url" : base_url + '/history_claim?id=' + $("#police_number2").val(),
                            "error": function(jqXHR, textStatus, errorThrown)
                                {
                                    toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                                }
                            },
                        columns: [
                            { data: 'cek', name: 'cek' },
                            { data: 'action', name: 'action' },
                            { data: 'id', name: 'id' },
                            { data: 'polis_no', name: 'polis_no' },
                            { data: 'claim_start_date', name: 'claim_start_date' },
                            { data: 'claim_end_date', name: 'claim_end_date' },
                            { data: 'loss_amount', name: 'loss_amount' },
                            { data: 'claim_amount', name: 'claim_amount' },
                            { data: 'claim_notes', name: 'claim_notes' },
                            { data: 'status_definition', name: 'status_definition' },
                            { data: 'workflow_status_id', name: 'workflow_status_id' },

                        ]
                    });
}
var dataArray=[];
$.ajax({
        type: 'GET',
        url: base_url + '/refPolis',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {
                dataArray.push(v.polis_no);
            });

        }
    });

console.log(dataArray);

/*An array containing all the country names in the world:*/
var countries = dataArray;
autocomplete(document.getElementById("police_number"), countries);


/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
function simpan() {
    loadingPage();


    if($('#claim_id').val()===''){
        endLoadingPage();
        $( "#claim_id" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#claim_id").removeClass( "is-invalid" );
    }

    if ( $('#claim_id').val() == 3 ) {
        // Claim Finish (Validasi Semua Wajib diisi)

        // Claim Notes
        if($('#notes').val()===''){
            endLoadingPage();
            $( "#notes" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#notes").removeClass( "is-invalid" );
        }

        // Kronologi
        if($('#kronologi').val()===''){
            endLoadingPage();
            $( "#kronologi" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#kronologi").removeClass( "is-invalid" );
        }

        // Estimation of loss
        if($('#estimation').val()===''){
            endLoadingPage();
            $( "#estimation" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#estimation").removeClass( "is-invalid" );
        }

        // Cause OF Loss
        if($('#loss_id').val()===''){
            endLoadingPage();
            $( "#loss_id" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#loss_id").removeClass( "is-invalid" );
        }

        // PLA Document
        if($('#file_pla').val()===''){
            endLoadingPage();
            $("#file_pla").addClass( "is-invalid" );
            return false;
        }else{
            var size=$('#file_pla')[0].files[0].size;
            console.log(size);
            var extension=$('#file_pla').val().replace(/^.*\./, '');
            console.log(extension);
            if(size >= 2000002){
                endLoadingPage();
                $( "#file_pla" ).addClass("is-invalid");
                $('#ss1').hide();
                $('#ff1').hide();
                $('#mm1').show();
                return false;
            }

            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
                endLoadingPage();
                $( "#file_pla" ).addClass("is-invalid");
                $('#ss1').hide();
                $('#ff1').show();
                $('#mm1').hide();
                return false;
            }
            $( "#file_pla" ).removeClass( "is-invalid" );
        }

        // DLA Document
        if($('#file_dla').val()===''){
            endLoadingPage();
            $("#file_dla").addClass( "is-invalid" );
            return false;
        }else{
            var size=$('#file_dla')[0].files[0].size;
            console.log(size);
            var extension=$('#file_dla').val().replace(/^.*\./, '');
            console.log(extension);
            if(size >= 2000002){
                endLoadingPage();
                $( "#file_dla" ).addClass("is-invalid");
                $('#ss2').hide();
                $('#ff2').hide();
                $('#mm2').show();
                return false;

            }
            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
                endLoadingPage();
                $( "#file_dla" ).addClass("is-invalid");
                $('#ss2').hide();
                $('#ff2').show();
                $('#mm2').hide();
                return false;
            }

            $( "#file_dla" ).removeClass( "is-invalid" );

        }

        // LOD Document
        if($('#file_lod').val()===''){
            endLoadingPage();
            $("#file_lod").addClass( "is-invalid" );
            return false;
        }else{
            var size=$('#file_lod')[0].files[0].size;
            console.log(size);
            var extension=$('#file_lod').val().replace(/^.*\./, '');
            console.log(extension);
            if(size >= 2000002){
                endLoadingPage();
                $( "#file_lod" ).addClass("is-invalid");
                $('#ss3').hide();
                $('#ff3').hide();
                $('#mm3').show();
                return false;

            }
            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
                endLoadingPage();
                $( "#file_lod" ).addClass("is-invalid");
                $('#ss3').hide();
                $('#ff3').show();
                $('#mm3').hide();
                return false;
            }

            $( "#file_lod" ).removeClass( "is-invalid" );

        }

        // Claim Start Date
        if($('#tgl_claim').val()===''){
            endLoadingPage();
            $( "#tgl_claim" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#tgl_claim").removeClass( "is-invalid" );
        }

        // Claim End Date
        if($('#tgl_claim_end').val()===''){
            endLoadingPage();
            $( "#tgl_claim_end" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#tgl_claim_end").removeClass( "is-invalid" );
        }

        // Claim Document
        if($('#file_claim').val()===''){
            endLoadingPage();
            $( "#file_claim" ).addClass( "is-invalid" );
            return false;
        }else{
            var size=$('#file_claim')[0].files[0].size;
            console.log(size);
            var extension=$('#file_claim').val().replace(/^.*\./, '');
            console.log(extension);
            if(size >= 2000002){
                endLoadingPage();
                $( "#file_claim" ).addClass("is-invalid");
                $('#ss4').hide();
                $('#ff4').hide();
                $('#mm4').show();
                return false;

            }
            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
                endLoadingPage();
                $( "#file_claim" ).addClass("is-invalid");
                $('#ss4').hide();
                $('#ff4').show();
                $('#mm4').hide();
                return false;
            }

            $( "#file_claim" ).removeClass( "is-invalid" );

        }

        // Claim Amount
        if($('#claim_amount').val()===''){
            endLoadingPage();
            $( "#claim_amount" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#claim_amount").removeClass( "is-invalid" );
        }




    } else {
        // Cuman 4 yang diisi

        // Claim Notes
        if($('#notes').val()===''){
            endLoadingPage();
            $( "#notes" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#notes").removeClass( "is-invalid" );
        }

        // Claim Start Date
        if($('#tgl_claim').val()===''){
            endLoadingPage();
            $( "#tgl_claim" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#tgl_claim").removeClass( "is-invalid" );
        }

        // Kronologi
        if($('#kronologi').val()===''){
            endLoadingPage();
            $( "#kronologi" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#kronologi").removeClass( "is-invalid" );
        }

    } // end if













    if($('#tgl_loss').val()===''){
        endLoadingPage();
        $( "#tgl_loss" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_loss").removeClass( "is-invalid" );
    }







    // $('#modal').modal('hide');
    var _create_form = $("#form-new");
    var _form_data = new FormData(_create_form[0]);
    $.ajax({
        type: 'POST',
        url: base_url + '/insert_claim',
        data: _form_data,
        dataType: 'JSON',
        processData: false,
        contentType: false,
         beforeSend: function () {
                loadingPage();
            },
        success: function (res) {
            //var parse = $.parseJSON(res);
            if(res.rc==1){
                endLoadingPage();
                toastr.success("Claim Success");
                $("#form-new")[0].reset();
                $('#modal').modal('hide');
                if($("#police_number2").val()==='' || typeof $("#police_number2").val() === "undefined"){
                  // Get History Claim
                  getListHistoryClaim();
                //   data();

                }else{
                  data_retview();
                }

            }else{
                swal.fire("Info",obj.rm,"info");
            }

        }
    }).done(function( res ) {
        endLoadingPage();

       }).fail(function(res) {
        endLoadingPage();
        toastr.error("Terjadi Kesalahan");
    });



}
  function deletedata(id) {
        swal.fire({
           title: "Info",
           text: "Delete Data",
           type: "info",
           showCancelButton: true,
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
               $.ajax({
                  type: "GET",
                  url: base_url + '/delete_claim?id='+id,
                  beforeSend: function () {
                      loadingPage();
                  },
                  success: function (result) {
                      var data = $.parseJSON(result);
                      endLoadingPage();
                      if(data.length!=0){

                          console.log(data);
                          toastr.success("Delete data success");
                          var table = $('#claim_table').DataTable({
                              aaSorting: [],
                              destroy: true,
                              processing: true,
                              serverSide: true,
                              columnDefs: [
                                  { targets: [5,6], className: 'text-right' },
                                  { "orderable": false, "targets": 0 }],
                              ajax: {
                                  "url" : base_url + '/history_claim?id=' + $("#police_number").val(),
                                  "error": function(jqXHR, textStatus, errorThrown)
                                      {
                                          toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                                      }
                                  },
                               columns: [
                                    { data: 'cek', name: 'cek' },
                                    { data: 'action', name: 'action' },
                                    { data: 'id', name: 'id' },
                                    { data: 'polis_no', name: 'polis_no' },
                                    { data: 'claim_start_date', name: 'claim_start_date' },
                                    { data: 'claim_end_date', name: 'claim_end_date' },
                                    { data: 'loss_amount', name: 'loss_amount' },
                                    { data: 'claim_amount', name: 'claim_amount' },
                                    { data: 'claim_notes', name: 'claim_notes' },
                                    { data: 'status_definition', name: 'status_definition' },
                                    { data: 'workflow_status_id', name: 'workflow_status_id' },

                                ]
                          });
                      }else{
                          $('.flat').hide();
                          $('#history').hide();
                          toastr.error("failed");
                      }
                  }
              }).done(function (msg) {
                   endLoadingPage();
              }).fail(function (msg) {
                   endLoadingPage();
                  toastr.error("Terjadi Kesalahan");
              });
            }
        });

   }
   function kirim_claim() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("warning!", "Select at least one", "warning");
    }else{

        swal.fire({
            title: "Info",
            text: "Are you sure you want to approve the selected record?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/send_claim',
                    data: {value: value},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        toastr.success("Send approve success");
                          var table = $('#claim_table').DataTable({
                              aaSorting: [],
                              destroy: true,
                              processing: true,
                              serverSide: true,
                              columnDefs: [
                                  { targets: [5,6], className: 'text-right' },
                                  { "orderable": false, "targets": 0 }],
                              ajax: {
                                  "url" : base_url + '/history_claim?id=' + $("#police_number").val(),
                                  "error": function(jqXHR, textStatus, errorThrown)
                                      {
                                          toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                                      }
                                  },
                              columns: [
                                    { data: 'cek', name: 'cek' },
                                    { data: 'action', name: 'action' },
                                    { data: 'id', name: 'id' },
                                    { data: 'polis_no', name: 'polis_no' },
                                    { data: 'claim_start_date', name: 'claim_start_date' },
                                    { data: 'claim_end_date', name: 'claim_end_date' },
                                    { data: 'loss_amount', name: 'loss_amount' },
                                    { data: 'claim_amount', name: 'claim_amount' },
                                    { data: 'claim_notes', name: 'claim_notes' },
                                    { data: 'status_definition', name: 'status_definition' },
                                    { data: 'workflow_status_id', name: 'workflow_status_id' },

                                ]
                          });

                    }
                }).done(function( res ) {
                    endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
    }
}

function get_detail_proudct_list(elm) {
    var value = $(elm).val();
    if ( value ) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: base_url + '/claim/get_detail_product_list?id=' + value,
            data: {},
            beforeSend: function () {
                KTApp.block('#container-filter-polis', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'danger',
                    message: 'Please wait...'
                });
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    var data = response.data;
                    var elm = "";
                    if ( data.length > 0 ) {
                        elm += '<option value="1000" disabled selected> Select Filter Polis </option>'
                        elm += '<option value="9999"> No Polis </option>'
                        elm += '<option value="8888"> Nama Customer </option>'
                        data.forEach(function (value, index) {
                            elm += `<option value='${value.bit_id}'> ${value.label} </option>`;
                        });
                    } else {
                        // elm += '<option value="1000" disabled selected> Filter Tidak Ditemukan </option>'
                        elm += '<option value="1000" disabled selected> Select Filter Polis </option>'
                        elm += '<option value="9999"> No Polis </option>'
                        elm += '<option value="8888"> Nama Customer </option>'
                    }
                    $("#filter_polis").html(elm);
                } else {
                    toastr.error("Terjadi Kesalahan");
                }

            }
        }).done(function (msg) {
            KTApp.unblock('#container-filter-polis');
        }).fail(function (msg) {
            KTApp.unblock('#container-filter-polis');
            toastr.error("Terjadi Kesalahan");
        });
    } // endif
}

function get_list_keyword(elm) {
    var value = $(elm).val();
    if ( value ) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: base_url + '/claim/get_list_keyword?bit_id=' + value + '&prod_id=' + $("#product").val(),
            data: {},
            beforeSend: function () {
                KTApp.block('#container-keyword', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'danger',
                    message: 'Please wait...'
                });
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    var data = response.data;
                    var list =[];
                    $.each(data, function (k,v) {
                        list.push(v.value_text);
                    });
                    autocomplete(document.getElementById("keyword"), list);
                } else {
                    toastr.error("Terjadi Kesalahan");
                }

            }
        }).done(function (msg) {
            KTApp.unblock('#container-keyword');
        }).fail(function (msg) {
            KTApp.unblock('#container-keyword');
            toastr.error("Terjadi Kesalahan");
        });
    } // endif
}

function focusToKeyword() {
    $("#keyword").focus();
}

var arrData = [];
function search_polis()
{
    event.preventDefault();
    console.log('product value : ' + $("#product").val());

    if ( $("#product_insurance").val() == null ) {
        toastr.warning('Type of Insurance belum dipilih');
        return false;
    }

    if ( $("#filter_polis").val() == null ) {
        toastr.warning('Filter Polis belum dipilih');
        return false;
    }

    console.log('keyword : ' + $("#keyword").val());
    if ( !$("#keyword").val()) {
        toastr.warning('Masukkan keyword pencarian');
        return false;
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",
        url: base_url + '/claim/get_list_data_filter',
        data: {
            product_id : $("#product_insurance").val(),
            bit_id : $("#filter_polis").val(),
            keyword : $("#keyword").val()
        },
        beforeSend: function () {
            KTApp.block('#container-form-claim', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'danger',
                message: 'Please wait...'
            });
            arrData = [];
            DT.clear().draw(false);
        },

        success: function (response) {
            if (response.rc == 1) {
                var data = response.data;
                var rows = [];

                if ( data.length <= 0 ) {
                    toastr.warning('Data Tidak Ditemukan');
                    return false;
                }
                console.log(data);
                data.forEach(function (value, index) {
                    var dt = [
                                `<button class="btn btn-sm btn-outline-brand btn-pill" onclick="selectPolis('${value.polis_no}')"> Pilih </button>`,
                                value.polis_no,
                                value.full_name,
                                value.definition,
                                numFormatNew(parseFloat(value.ins_amount).toFixed(2)),
                                numFormatNew(parseFloat(value.premi_amount).toFixed(2)),
                                numFormatNew(parseFloat(value.disc_amount).toFixed(2)),
                                numFormatNew(parseFloat(value.net_amount).toFixed(2))
                            ];

                    rows.push(dt);
                    arrData.push(value);
                });

                DT.rows.add(rows).draw(false);

                $("#modal_list_filter").modal('show');
            } else {
                toastr.error("Terjadi Kesalahan");
            }

        }
    }).done(function (msg) {
        KTApp.unblock('#container-form-claim');
    }).fail(function (msg) {
        KTApp.unblock('#container-form-claim');
        toastr.error("Terjadi Kesalahan");
    });

}

function selectPolis(polis_no) {
    $("#container-filter").slideUp();
    $("#modal_list_filter").modal('hide');
    $("#police_number").val(polis_no);

    var dataFilter = arrData.filter(function(value, index) {
        return value.polis_no == polis_no
    });

    console.log(dataFilter);

    // data();
    $("#container-search-ulang").removeClass('d-none');
    $('.flat').show();
    $('#history').show();

    $("#container-police").removeAttr('style');

    $("#qs_doc_id").val(dataFilter[0].id);
    $('#product').val(dataFilter[0].definition);
    $('#agent').val(dataFilter[0].agent);
    $('#customer').val(dataFilter[0].full_name);
    $('#quotation').val();
    $('#bank').val(dataFilter[0].bank_account);

    getListHistoryClaim();
}

function search_ulang() {
    $("#container-filter").slideDown();
    $('.flat').hide();
    $('#history').hide();
    $("#container-search-ulang").addClass('d-none');
    $("#container-police").css('display', 'none');
}

function getListHistoryClaim(){
    var table = $('#claim_table').DataTable({
                    aaSorting: [],
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    columnDefs: [
                        { targets: [5,6], className: 'text-right' },
                        { "orderable": false, "targets": 0 }],
                    ajax: {
                        "url" : base_url + '/history_claim?id=' + $("#police_number").val(),
                        "error": function(jqXHR, textStatus, errorThrown)
                            {
                                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                            }
                        },
                    columns: [
                        { data: 'cek', name: 'cek' },
                        { data: 'action', name: 'action' },
                        { data: 'id', name: 'id' },
                        { data: 'polis_no', name: 'polis_no' },
                        { data: 'claim_start_date', name: 'claim_start_date' },
                        { data: 'claim_end_date', name: 'claim_end_date' },
                        { data: 'loss_amount', name: 'loss_amount' },
                        { data: 'claim_amount', name: 'claim_amount' },
                        { data: 'claim_notes', name: 'claim_notes' },
                        { data: 'status_definition', name: 'status_definition' },
                        { data: 'workflow_status_id', name: 'workflow_status_id' },

                    ]
                });
}

function detailProduct() {
    event.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'GET',
        url: base_url + '/claim/get_detail_product',
        data: {
            product_id : $("#product_insurance").val(),
            doc_id : $("#qs_doc_id").val()
        },

        beforeSend: function () {
            loadingPage();
        },

        success: function (response) {
            console.log(response);
            if ( response.rc == 1 ) {
                var view = response.view;
                var data = response.data_qs_detail;


                $(".form_content").html(view);
                initMaskMoney();
                $("#detail").modal('show');
                $("#title-detail").html("Detail");

                data.forEach(function (value, index) {
                    console.log('input name : ' + "#" + value.input_name + '-' + value.bit_id);
                    console.log('value : ' + value.value_text);
                    $("#" + value.input_name + '-' + value.bit_id).val(value.value_text);
                });
            } else {
                toastr.error("Terjadi Kesalahan");
            }
        }

    }).done(function (msg) {
        endLoadingPage();
    }).fail(function (msg) {
        endLoadingPage();
        toastr.error("Terjadi Kesalahan");
    });
}

function initMaskMoney() {
        $(".money").maskMoney({
            prefix:'',
            allowNegative: false,
            thousands:'.',
            decimal:',',
            affixesStay: false,
            // precision: 0 // Tidak ada 2 digit dibelakang koma
        });
    }
</script>
<!-- end:: Content -->
@stop
