<!--begin::Modal-->
<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title-detail"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form id="form-detail">
                @csrf
                {{-- <input type="hidden" name="sts" id="sts" value=""> --}}
                <div class="modal-body kt-scroll" style="max-height: 400px; overflow: auto;">
                    <div class="form_content"></div>
                </div>
                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
