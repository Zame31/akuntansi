<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body">
                     <form id="form-new" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="qs_doc_id" id="qs_doc_id">
                        <div class="row">
                            <div class="col-4">
                                    <div class="form-group">
                                        @php
                                        $ref_claim = \DB::select('SELECT * FROM ref_claim_status where is_active=true and company_id='.Auth::user()->company_id);
                                        @endphp
                                            <label for="exampleTextarea">Claim Status</label>
                                            <select class="form-control kt-select2" name="claim_id" id="claim_id">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($ref_claim as $item)
                                                <option value="{{$item->id}}">{{$item->status_definition}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Silahkan pilih claim status</div>
                                        </div>
                                        <div class="form-group">
                                                <label for="exampleTextarea">Claim Notes</label>
                                                <textarea class="form-control" id="notes" maxlength="200" rows="8" style="height: 130px;" name="notes"></textarea>
                                                <div class="invalid-feedback">Silahkan isi claim notes</div>
                                            </div>
                                <div class="form-group">
                                    <label>PLA Document</label>
                                    <input type="file" class="form-control" name="file_pla" id="file_pla">
                                    <div id="pla">
                                    </div>


                                    <div class="invalid-feedback" id="ss1">Silahkan Upload Dokumen</div>
                                    <div class="invalid-feedback" id="ff1">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                                    <div class="invalid-feedback" id="mm1">Max Size Dokumen 2 Mb</div>
                                </div>
                                <div class="form-group">
                                            <label for="exampleTextarea">Claim Start Date</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control" id="tgl_claim" name="tgl_claim" readonly
                                                    placeholder="Pilih Tanggal" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                                </div>
                                                <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                            </div>
                                        </div>
                                <div class="form-group">
                                            <label for="exampleTextarea">Claim Amount</label>
                                            <input type="text" class="form-control money" style="text-align: right;" id="claim_amount"  name="claim_amount" maxlength="50">
                                        </div>

                            </div>
                            <div class="col-4">
                                        <div class="form-group">
                                            <label for="exampleTextarea">Police Number</label>
                                            <input type="text" class="form-control" readonly id="police_number1" onkeypress="return huruf(event)" name="police_number">
                                            <div class="invalid-feedback" id="mm3">Silahkan isi short name</div>
                                        </div>

                                        <div class="form-group">
                                                <label for="exampleTextarea">Chronology</label>
                                                <textarea class="form-control" id="kronologi" maxlength="200" rows="8" style="height: 130px;" name="kronologi"></textarea>
                                                <div class="invalid-feedback">Silahkan isi Chronology</div>
                                        </div>
                                <div class="form-group">
                                    <label>DLA Document</label>
                                    <input type="file" class="form-control" name="file_dla" id="file_dla">
                                    <div id="dla">
                                    </div>
                                    <div class="invalid-feedback" id="ss2">Silahkan Upload Dokumen</div>
                                    <div class="invalid-feedback" id="ff2">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                                    <div class="invalid-feedback" id="mm2">Max Size Dokumen 2 Mb</div>
                                </div>
                                <div class="form-group">
                                            <label for="exampleTextarea">Claim End Date</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control" id="tgl_claim_end" name="tgl_claim_end" readonly
                                                    placeholder="Pilih Tanggal" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                                </div>
                                                <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                            </div>
                                        </div>
                            </div>
                            <div class="col-4">
                                        <div class="form-group">
                                            <label for="exampleTextarea">Date Of Loss</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control" value="{{date('d M Y')}}" id="tgl_loss" name="tgl_loss" readonly
                                                    placeholder="Pilih Tanggal" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                                </div>
                                                <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleTextarea">Estimation of Loss</label>
                                            <input type="text" class="form-control money" style="text-align: right;" id="estimation"  name="estimation" maxlength="50">
                                            <div class="invalid-feedback" id="mm6">Silahkan isi Estimation of Loss</div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleTextarea">Cause of Loss</label>
                                            @php
                                              $ref_clause = \DB::select('SELECT * FROM ref_cause_loss where is_active=true');
                                            @endphp
                                            <select class="form-control kt-select2" name="loss_id" id="loss_id">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($ref_clause as $item)
                                                <option value="{{$item->id}}">{{$item->cause_definition}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Silahkan pilih Cause of Loss</div>
                                        </div>
                                <div class="form-group">
                                    <label>LOD Document</label>
                                    <input type="file" class="form-control" name="file_lod" id="file_lod">
                                    <div id="lod">
                                    </div>
                                    <div class="invalid-feedback" id="ss3">Silahkan Upload Dokumen</div>
                                    <div class="invalid-feedback" id="ff3">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                                    <div class="invalid-feedback" id="mm3">Max Size Dokumen 2 Mb</div>
                                </div>
                                <div class="form-group">
                                    <label>Claim Document</label>
                                    <input type="file" class="form-control" name="file_claim" id="file_claim">
                                    <div id="claim">
                                    </div>
                                    <div class="invalid-feedback" id="ss4">Silahkan Upload Dokumen</div>
                                    <div class="invalid-feedback" id="ff4">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                                    <div class="invalid-feedback" id="mm4">Max Size Dokumen 2 Mb</div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-success" onclick="simpan();">Simpan</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
     <script src="{{asset('js/_global.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
         $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

$(document).ready(function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();


    var max="'"+ dd +" "+mm+" "+yyyy+"'";
    console.log(max);
    $('#tgl_claim').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

    $(document).ready(function(){
        $('#tgl_claim').change(function () {
            $('#tgl_claim_end').val('');
           $('#tgl_claim_end').datepicker('destroy');
            console.log(this.value);
            $('#tgl_claim_end').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                startDate: this.value
            });

        });
    });

    $('#tgl_loss').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

});

    </script>
