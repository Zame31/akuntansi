<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body">
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                            <div class="row">
                                <div class="col-md-6">

                                    <!--begin::Portlet-->
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Before
                                                </h3>
                                            </div>
                                        </div>

                                        <!--begin::Form-->
                                        <form class="kt-form kt-form--label-right">
                                            <div class="kt-portlet__body">
                                                <div class="form-group ">
                                                    <label>Police Number</label>
                                                    <div class="input-group">
                                                        <input type="text" id="police_number" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Claim Status</label>
                                                    <div class="input-group">
                                                        <input type="text" id="claim_status" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Date Of Loss</label>
                                                    <div class="input-group">
                                                        <input type="text" id="date_of_loss" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Claim Notes</label>
                                                    <div class="input-group">
                                                        <input type="text" id="claim_notes" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Chronology</label>
                                                    <div class="input-group">
                                                        <input type="text" id="kronologi" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Estimation of Loss</label>
                                                    <div class="input-group">
                                                        <input type="text" id="estimation" class="form-control money" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Cause of Loss</label>
                                                    <div class="input-group">
                                                        <input type="text" id="cause_of_loss" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Claim Amount</label>
                                                    <div class="input-group">
                                                        <input type="text" id="claim_amount" class="form-control money" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <!--end::Form-->
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <!--begin::Portlet-->
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    After
                                                </h3>
                                            </div>
                                        </div>

                                        <!--begin::Form-->
                                        <form class="kt-form">
                                            <div class="kt-portlet__body">
                                                <div class="form-group ">
                                                    <label>Police Number</label>
                                                    <div class="input-group">
                                                        <input type="text" id="police_number1" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Claim Status</label>
                                                    <div class="input-group">
                                                        <input type="text" id="claim_status1" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Date Of Loss</label>
                                                    <div class="input-group">
                                                        <input type="text" id="date_of_loss1" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Claim Notes</label>
                                                    <div class="input-group">
                                                        <input type="text" id="claim_notes1" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Chronology</label>
                                                    <div class="input-group">
                                                        <input type="text" id="kronologi1" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Estimation of Loss</label>
                                                    <div class="input-group">
                                                        <input type="text" id="estimation1" class="form-control money" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Cause of Loss</label>
                                                    <div class="input-group">
                                                        <input type="text" id="cause_of_loss1" class="form-control" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Claim Amount</label>
                                                    <div class="input-group">
                                                        <input type="text" id="claim_amount1" class="form-control money" readonly aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <!--end::Form-->
                                    </div>

                                    <!--end::Portlet-->
                                </div>
                            </div>
                        </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
     <script src="{{asset('js/_global.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
         $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

$(document).ready(function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    
    var max="'"+ dd +" "+mm+" "+yyyy+"'";
    console.log(max);
    $('#tgl_claim').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

    $(document).ready(function(){
        $('#tgl_claim').change(function () {
            $('#tgl_claim_end').val('');
           $('#tgl_claim_end').datepicker('destroy');
            console.log(this.value);
            $('#tgl_claim_end').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                startDate: this.value
            });

        });
    });

    $('#tgl_loss').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

});

    </script>