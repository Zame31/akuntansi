@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Claim </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Approval Claim </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            Approval Claim
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <a href="#" onclick="approve_all();" class="btn btn-pill btn-primary" style="color: white;">Approval All</a>
                            <a href="#" onclick="approve_selected();" class="btn btn-pill btn-primary" style="color: white;">Approval Selected</a>
                            <a href="#" onclick="reject_all();" class="btn btn-pill btn-danger" style="color: white;">Reject All</a>
                            <a href="#" onclick="reject_selected();" class="btn btn-pill btn-danger" style="color: white;">Reject Selected</a>
                        </div>
                    </div>
                        {{-- <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </a>
                        </div> --}}
                    </div>
            </div>
            <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" id="example-select-all" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Police Number</th>
                                    <th>Claim Start Date</th>
                                    <th>Claim End Date</th>
                                    <th>Loss Amount</th>
                                    <th>Cause Loss</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data)
                                @foreach($data as $item)
                                <tr>
                                     <td>
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="{{$item->id}}" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#" onclick="detail_claim({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Detail</a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->polis_no}}</td>
                                    <td>{{date('d M Y',strtotime($item->claim_start_date))}}</td>
                                    <td>{{date('d M Y',strtotime($item->claim_end_date))}}</td>
                                    <td>{{number_format($item->loss_amount,2,',','.')}}</td>
                                    <td>{{$item->cause_definition}}</td>        
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('claim.detail_claim')
<script type="text/javascript">
    function detail_claim(id) {
        $.ajax({
            type: "GET",
            url: base_url + '/detail_claim?id='+id,
            beforeSend: function () {
                loadingPage();
            },

            success: function (result) {
                var data = $.parseJSON(result);
                endLoadingPage();
                const monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                      ];
                 console.log(data['data']);
                 console.log(data['data1']);     
                      

                      var today1 = new Date(data['data1'][0]['loss_date']);
                      var dd1 = today1.getDate();
                      var mm1 = today1.getMonth(); //January is 0!
                      var yyyy1 = today1.getFullYear();
                      var loss_date1=dd1 +" "+monthNames[mm1]+" "+yyyy1;

                      if(data['data'].length!=0){
                        var today = new Date(data['data'][0]['loss_date']);
                      var dd = today.getDate();
                      var mm = today.getMonth(); //January is 0!
                      var yyyy = today.getFullYear();
                      var loss_date=dd +" "+monthNames[mm]+" "+yyyy;
                        $('#police_number').val(data['data'][0]['polis_no']);
                        $('#claim_status').val(data['data'][0]['status_definition']);
                        $('#date_of_loss').val(loss_date);
                        $('#claim_notes').val(data['data'][0]['claim_notes']);
                        $('#kronologi').val(data['data'][0]['kronologi_kejadian']);
                        $('#estimation').val(data['data'][0]['loss_amount']).trigger('mask.maskMoney');
                        $('#cause_of_loss').val(data['data'][0]['cause_definition']);
                        $('#claim_amount').val(data['data'][0]['claim_amount']).trigger('mask.maskMoney');
                      }
                        

                        $('#police_number1').val(data['data1'][0]['polis_no']);
                        $('#claim_status1').val(data['data1'][0]['status_definition']);
                        $('#date_of_loss1').val(loss_date1);
                        $('#claim_notes1').val(data['data1'][0]['claim_notes']);
                        $('#kronologi1').val(data['data1'][0]['kronologi_kejadian']);
                        $('#estimation1').val(data['data1'][0]['loss_amount']).trigger('mask.maskMoney');
                        $('#cause_of_loss1').val(data['data1'][0]['cause_definition']);
                        $('#claim_amount1').val(data['data1'][0]['claim_amount']).trigger('mask.maskMoney');

            }
        }).done(function (msg) {
             endLoadingPage();
        }).fail(function (msg) {
             endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
        $('#modal_detail').modal('show');
    }
$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});
function znClose() {
    znIconboxClose();
}
function approve_all() {
        swal.fire({
           title: "Info",
           text: "Approve All Data",
           type: "info",
           showCancelButton: true,
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {

                  $.ajax({
                    type: 'POST',
                    url: base_url + '/approve_claim?type=semua',
                    async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Success",
                                type: 'success',
                                confirmButtonText: 'Close',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                            }
                        }).done(function( res ) {
                           endLoadingPage();

                       }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Internal Server Error!","error");
                    });

            }
        });

   }

   function reject_all() {
        swal.fire({
           title: "Info",
           text: "Reject All Data",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {

                $.ajax({
                    type: 'POST',
                    url: base_url + '/reject_claim?type=semua',
                    async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Success",
                                type: 'success',
                                confirmButtonText: 'Close',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                            }
                        }).done(function( res ) {
                           endLoadingPage();

                       }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Internal Server Error!","error");
                    });

            }
        });
   }

function approve_selected() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Info!", "Select at least one", "warning");
    }else{
        swal.fire({
            title: "Info",
            text: "Are you sure you want to approve the selected record?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: base_url + '/approve_claim?type=satu',
                    data: {value: value},
                    async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Success",
                                type: 'success',
                                confirmButtonText: 'Close',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                            }
                        }).done(function( res ) {
                           endLoadingPage();

                       }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Internal Server Error!","error");
                    });

            }
        });
    }
}

function reject_selected() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Info!", "Select at least one", "warning");
    }else{
        swal.fire({
            title: "Info",
            text: "Are you sure you want to approve the selected record?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {

                $.ajax({
                    type: 'POST',
                    url: base_url + '/reject_claim?type=satu',
                    data: {value: value},
                    async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Success",
                                type: 'success',
                                confirmButtonText: 'Close',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                            }
                        }).done(function( res ) {
                           endLoadingPage();

                       }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Internal Server Error!","error");
                    });
            }
        });
    }
}

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}
</script>

@include('invoice.action')
@include('transaksi.invoice_modal')
@stop
