
<!-- BACK-TO-TOP -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- JQUERY SCRIPTS JS-->
<script src="{{asset('assets/js/vendors/jquery-3.2.1.min.js')}}"></script>

<!-- BOOTSTRAP SCRIPTS JS-->
<script src="{{asset('assets/js/vendors/bootstrap.bundle.min.js')}}"></script>

<!-- SPARKLINE JS -->
<script src="{{asset('assets/js/vendors/jquery.sparkline.min.js')}}"></script>

<!-- CHART-CIRCLE JS-->
<script src="{{asset('assets/js/vendors/circle-progress.min.js')}}"></script>

<!-- RATING STAR JS-->
<script src="{{asset('assets/plugins/rating/rating-stars.js')}}"></script>

<!-- POPOVER JS -->
<script src="{{asset('assets/js/popover.js')}}"></script>

<!-- CHARTJS CHART JS-->
<script src="{{asset('assets/plugins/chart/chart.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/chart/utils.js')}}"></script>

<!-- PIETY CHART JS-->
<script src="{{asset('assets/plugins/peitychart/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/plugins/peitychart/peitychart.init.js')}}"></script>
<script src="{{asset('assets/plugins/peitychart/peitychart.js')}}"></script>

<!--SIDEMENU JS-->
<script src="{{asset('assets/plugins/sidemenu/sidemenu.js')}}"></script>

<!-- SIDEMENU-RESPONSIVE-TABS JS-->
<script src="{{asset('assets/plugins/sidemenu-responsive-tabs/js/sidemenu-responsive-tabs.js')}}"></script>

<!--LEFT-MENU JS-->
<script src="{{asset('assets/js/left-menu.js')}}"></script>

<!-- SWEET-ALERT JS -->
<script src="{{asset('assets/plugins/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/sweet-alert.js')}}"></script>


<!-- P-SCROLL JS -->
<script src="{{asset('assets/plugins/p-scroll/p-scroll.js')}}"></script>
<script src="{{asset('assets/plugins/p-scroll/p-scroll-leftmenu.js')}}"></script>

<!-- COUNTERS JS-->
<script src="{{asset('assets/plugins/counters/counterup.min.js')}}"></script>
<script src="{{asset('assets/plugins/counters/waypoints.min.js')}}"></script>
<script src="{{asset('assets/plugins/counters/counters-1.js')}}"></script>

<!-- SELECT2 JS -->
<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>

<!-- SIDEBAR JS -->
<script src="{{asset('assets/plugins/right-sidebar/right-sidebar.js')}}"></script>

<!-- DATA TABLE JS-->
<script src="{{asset('assets/plugins/Datatable/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/Datatable/js/dataTables.bootstrap4.js')}}"></script>

<script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/datatable.js')}}"></script>

<!-- SELECT2 JS -->
<script src="{{asset('assets/js/select2.js')}}"></script>

<!-- INDEX-SCRIPTS  JS-->
<script src="{{asset('assets/js/index.js')}}"></script>

<!-- CUSTOM JS -->
<script src="{{asset('assets/js/custom.js')}}"></script>

<script type="text/javascript" src="{{asset('js/date_moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}}"></script>

<script src="{{asset('js/_global.js')}}"></script>





<script type="text/javascript">
 



$('.tgl').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1969,
        maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
          format: 'DD-MM-Y'
      }
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
}); 
</script>

