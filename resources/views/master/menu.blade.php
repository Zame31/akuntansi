<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid"
    {{-- style="background: url('{{ asset('img/pat1.png') }}');" --}} id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
        <ul class="kt-menu__nav mt-4">
            <li id="h-dashboard" class="kt-menu__item " aria-haspopup="true">
                <a onclick="loadPageMenu('{{ route('home') }}','s-0')" class="kt-menu__link ">
                    <svg width="34px" height="34px" viewBox="0 0 24 24" version="1.1"
                        class="kt-menu__link-icon kt-svg-icon kt-svg-icon--info">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="34" height="34" />
                            <path
                                d="M3.95709826,8.41510662 L11.47855,3.81866389 C11.7986624,3.62303967 12.2013376,3.62303967 12.52145,3.81866389 L20.0429,8.41510557 C20.6374094,8.77841684 21,9.42493654 21,10.1216692 L21,19.0000642 C21,20.1046337 20.1045695,21.0000642 19,21.0000642 L4.99998155,21.0000673 C3.89541205,21.0000673 2.99998155,20.1046368 2.99998155,19.0000673 L2.99999828,10.1216672 C2.99999935,9.42493561 3.36258984,8.77841732 3.95709826,8.41510662 Z M10,13 C9.44771525,13 9,13.4477153 9,14 L9,17 C9,17.5522847 9.44771525,18 10,18 L14,18 C14.5522847,18 15,17.5522847 15,17 L15,14 C15,13.4477153 14.5522847,13 14,13 L10,13 Z"
                                fill="#000000" />
                        </g>
                    </svg>
                    {{-- <i class="kt-menu__link-icon la la-home"></i> --}}
                    <span class="kt-menu__link-text">Dashboard</span></a>
            </li>
            {{-- SALES --}}
            <li id="h-sales" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    {{-- <i class="kt-menu__link-icon flaticon-line-graph"></i> --}}
                    <svg width="100px" height="100px" viewBox="0 0 24 24" version="1.1"
                        class="kt-menu__link-icon kt-svg-icon kt-svg-icon--danger">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="100" height="100" />
                            <path
                                d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z"
                                fill="#000000" fill-rule="nonzero" />
                            <path
                                d="M8.7295372,14.6839411 C8.35180695,15.0868534 7.71897114,15.1072675 7.31605887,14.7295372 C6.9131466,14.3518069 6.89273254,13.7189711 7.2704628,13.3160589 L11.0204628,9.31605887 C11.3857725,8.92639521 11.9928179,8.89260288 12.3991193,9.23931335 L15.358855,11.7649545 L19.2151172,6.88035571 C19.5573373,6.44687693 20.1861655,6.37289714 20.6196443,6.71511723 C21.0531231,7.05733733 21.1271029,7.68616551 20.7848828,8.11964429 L16.2848828,13.8196443 C15.9333973,14.2648593 15.2823707,14.3288915 14.8508807,13.9606866 L11.8268294,11.3801628 L8.7295372,14.6839411 Z"
                                fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        </g>
                    </svg>
                    <span class="kt-menu__link-text">Sales</span>
                    <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
                                class="kt-menu__link">
                                <span class="kt-menu__link-text">Sales</span></span></li>

                        <li id="s-1" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('invoice_baru') }}','s-1')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Create New Debit Note</span>
                            </a>
                        </li>

                        <li id="s-78" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('endorse_baru') }}','s-78')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Endorsment</span>
                            </a>
                        </li>

                        <li id="s-2" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('invoice_list') }}','s-2')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">New Debit Note List</span>
                            </a>
                        </li>
                        <li id="s-4" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('invoice_active') }}','s-4')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Active Debit Note</span>
                            </a>
                        </li>
                        <li id="s-3" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('invoice_approval') }}','s-3')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval New Debit Note</span>
                            </a>
                        </li>
                        <li id="s-5" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('invoice_approval_payment') }}','s-5')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Debit Note Payment</span>
                            </a>
                        </li>
                        <li id="s-6" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('invoice_paid') }}','s-6')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Paid Debit Note</span>
                            </a>
                        </li>
                        <li id="s-7" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('invoice_dropped') }}','s-7')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Dropped Debit Note</span>
                            </a>
                        </li>
                        <li id="s-8" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('list_overdue') }}','s-8')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Overdue Split Payment</span>
                            </a>
                        </li>
                        <li id="s-9" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('approval_split_payment') }}','s-9')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Split Payment</span>
                            </a>
                        </li>
                        <li id="s-10" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('complete_payment') }}','s-10')" class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Completed Payment</span>
                            </a>
                        </li>
                        <li id="s-11" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('list_reversal_payment') }}','s-11')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Reversal Payment</span>
                            </a>
                        </li>
                        <li id="s-12" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('list_reversal_split') }}','s-12')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Reversal Split Payment</span>
                            </a>
                        </li>
                        <li class="kt-menu__section ">
                            <h4 class="kt-menu__section-text">Installment</h4>
                            <i class="kt-menu__section-icon flaticon-more-v2"></i>
                        </li>

                        <li id="s-701" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.form','create') }}','s-701')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Create New Installment</span>
                            </a>
                        </li>

                        <li id="s-702" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.index') }}?type=new','s-702')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">New Installment List</span>
                            </a>
                        </li>

                        <li id="s-703" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.index') }}?type=success','s-703')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Active Installment List</span>
                            </a>
                        </li>
                        <li id="s-704" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.index') }}?type=approve','s-704')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval New Installment</span>
                            </a>
                        </li>
                        <li id="s-705" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.index') }}?type=approve_reversal','s-705')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Delete Installment</span>
                            </a>
                        </li>

                        <li id="s-706" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.scheduleList') }}','s-706')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Installment Payment</span>
                            </a>
                        </li>

                        <li id="s-707" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.scheduleListCancel') }}','s-707')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Cancel Installment Payment</span>
                            </a>
                        </li>

                        <li id="s-708" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.index') }}?type=deleted','s-708')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Deleted Installment</span>
                            </a>
                        </li>

                        <li id="s-709" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.scheduleListPaid') }}','s-709')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Paid Installment Schedule</span>
                            </a>
                        </li>

                        <li id="s-710" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.scheduleListComplete') }}','s-710')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Completed Payment Installment</span>
                            </a>
                        </li>


                        <li id="s-711" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.split_approval_list') }}','s-711')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Split Payment Installment Schedule</span>
                            </a>
                        </li>

                        <li id="s-712" class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('installment.split_approval_list_reversal') }}','s-712')"
                                class="kt-menu__link ">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                <span class="kt-menu__link-text">Approval Reversal Split Payment Installment</span>
                            </a>
                        </li>

                        {{--
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{ route('complete_payment') }}')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Completed Payment</span>
                        </a>
            </li>
            --}}
        </ul>
    </div>
    </li>
    {{-- CLAIM --}}
    <li id="h-claim" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon flaticon-line-graph"></i> --}}
            <svg width="100px" height="100px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon kt-svg-icon--danger">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="100" height="100" />
                    <path
                        d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z"
                        fill="#000000" fill-rule="nonzero" />
                    <path
                        d="M8.7295372,14.6839411 C8.35180695,15.0868534 7.71897114,15.1072675 7.31605887,14.7295372 C6.9131466,14.3518069 6.89273254,13.7189711 7.2704628,13.3160589 L11.0204628,9.31605887 C11.3857725,8.92639521 11.9928179,8.89260288 12.3991193,9.23931335 L15.358855,11.7649545 L19.2151172,6.88035571 C19.5573373,6.44687693 20.1861655,6.37289714 20.6196443,6.71511723 C21.0531231,7.05733733 21.1271029,7.68616551 20.7848828,8.11964429 L16.2848828,13.8196443 C15.9333973,14.2648593 15.2823707,14.3288915 14.8508807,13.9606866 L11.8268294,11.3801628 L8.7295372,14.6839411 Z"
                        fill="#000000" fill-rule="nonzero" opacity="0.3" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Claim</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link">
                        <span class="kt-menu__link-text">Claim</span></span></li>

                <li id="s-85" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('claim') }}','s-85')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Claim</span>
                    </a>
                </li>
                <li id="s-86" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('approval_claim') }}','s-86')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Claim</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

    {{-- MARKETING --}}
    <li id="h-marketing" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon flaticon-line-graph"></i> --}}
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon  kt-svg-icon--success">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <path
                        d="M6.5,16 L7.5,16 C8.32842712,16 9,16.6715729 9,17.5 L9,19.5 C9,20.3284271 8.32842712,21 7.5,21 L6.5,21 C5.67157288,21 5,20.3284271 5,19.5 L5,17.5 C5,16.6715729 5.67157288,16 6.5,16 Z M16.5,16 L17.5,16 C18.3284271,16 19,16.6715729 19,17.5 L19,19.5 C19,20.3284271 18.3284271,21 17.5,21 L16.5,21 C15.6715729,21 15,20.3284271 15,19.5 L15,17.5 C15,16.6715729 15.6715729,16 16.5,16 Z"
                        fill="#000000" opacity="0.3" />
                    <path
                        d="M5,4 L19,4 C20.1045695,4 21,4.8954305 21,6 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6 C3,4.8954305 3.8954305,4 5,4 Z M15.5,15 C17.4329966,15 19,13.4329966 19,11.5 C19,9.56700338 17.4329966,8 15.5,8 C13.5670034,8 12,9.56700338 12,11.5 C12,13.4329966 13.5670034,15 15.5,15 Z M15.5,13 C16.3284271,13 17,12.3284271 17,11.5 C17,10.6715729 16.3284271,10 15.5,10 C14.6715729,10 14,10.6715729 14,11.5 C14,12.3284271 14.6715729,13 15.5,13 Z M7,8 L7,8 C7.55228475,8 8,8.44771525 8,9 L8,11 C8,11.5522847 7.55228475,12 7,12 L7,12 C6.44771525,12 6,11.5522847 6,11 L6,9 C6,8.44771525 6.44771525,8 7,8 Z"
                        fill="#000000" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Marketing</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link">
                        <span class="kt-menu__link-text">Marketing</span></span></li>

                <li id="s-715" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('quotation_slip') }}','s-715')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Quotation Slip</span>
                    </a>
                </li>

                <li id="s-816" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('quotation_slip.client') }}','s-816')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Quotation Slip Client</span>
                    </a>
                </li>

                <li id="s-716" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('cover_note') }}','s-716')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Proposal of Insurance</span>
                    </a>
                </li>
                <li id="s-717" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('confirmation_of_cover') }}','s-717')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Confirmation of Cover</span>
                    </a>
                </li>

                <li id="s-801" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.create','policy') }}','s-801')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Create Master Policy</span>
                    </a>
                </li>

                {{-- <li id="s-803" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.create','policy_endorsment') }}','s-803')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Create Master Policy Endorsment</span>
                    </a>
                </li> --}}

                <li id="s-804" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.list','policy') }}','s-804')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">New Master Policy List</span>
                    </a>
                </li>

                <li id="s-805" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.list','success_policy') }}','s-805')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Master Policy Active</span>
                    </a>
                </li>
                <li id="s-807" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.list','approval_policy') }}','s-807')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval New Master Policy</span>
                    </a>
                </li>

                <li class="kt-menu__section ">
                    <h4 class="kt-menu__section-text ml-4">Master Policy Installment</h4>
                    <i class="kt-menu__section-icon flaticon-more-v2"></i>
                </li>

                <li id="s-802" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.create','policy_installment') }}','s-802')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Create Master Policy Installment</span>
                    </a>
                </li>

                <li id="s-806" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.list','policy_installment') }}','s-806')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">New Master Policy Installment List</span>
                    </a>
                </li>
                <li id="s-809" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.list','success_policy_installment') }}','s-809')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Master Policy Installment Active</span>
                    </a>
                </li>
                <li id="s-808" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenuSPA('{{ route('marketing.list','approval_policy_installment') }}','s-808')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval New Master Policy Installment</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>

    {{-- PROPOSAL COVERAGE --}}
    <li id="h-proposal_coverage" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon flaticon-line-graph"></i> --}}
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon  kt-svg-icon--success">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <path
                        d="M6.5,16 L7.5,16 C8.32842712,16 9,16.6715729 9,17.5 L9,19.5 C9,20.3284271 8.32842712,21 7.5,21 L6.5,21 C5.67157288,21 5,20.3284271 5,19.5 L5,17.5 C5,16.6715729 5.67157288,16 6.5,16 Z M16.5,16 L17.5,16 C18.3284271,16 19,16.6715729 19,17.5 L19,19.5 C19,20.3284271 18.3284271,21 17.5,21 L16.5,21 C15.6715729,21 15,20.3284271 15,19.5 L15,17.5 C15,16.6715729 15.6715729,16 16.5,16 Z"
                        fill="#000000" opacity="0.3" />
                    <path
                        d="M5,4 L19,4 C20.1045695,4 21,4.8954305 21,6 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6 C3,4.8954305 3.8954305,4 5,4 Z M15.5,15 C17.4329966,15 19,13.4329966 19,11.5 C19,9.56700338 17.4329966,8 15.5,8 C13.5670034,8 12,9.56700338 12,11.5 C12,13.4329966 13.5670034,15 15.5,15 Z M15.5,13 C16.3284271,13 17,12.3284271 17,11.5 C17,10.6715729 16.3284271,10 15.5,10 C14.6715729,10 14,10.6715729 14,11.5 C14,12.3284271 14.6715729,13 15.5,13 Z M7,8 L7,8 C7.55228475,8 8,8.44771525 8,9 L8,11 C8,11.5522847 7.55228475,12 7,12 L7,12 C6.44771525,12 6,11.5522847 6,11 L6,9 C6,8.44771525 6.44771525,8 7,8 Z"
                        fill="#000000" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Proposal Coverage</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link">
                        <span class="kt-menu__link-text">Proposal Coverage</span></span></li>


                <li id="s-814" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('proposal_coverage.create') }}','s-814')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Create Proposal Coverage</span>
                    </a>
                </li>

                <li id="s-815" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('proposal_coverage') }}','s-815')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">List Proposal Coverage</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>

    {{-- ACCOUNTING --}}
    <li id="h-accounting" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon la la-balance-scale"></i> --}}
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon  kt-svg-icon--warning">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <path
                        d="M6.5,16 L7.5,16 C8.32842712,16 9,16.6715729 9,17.5 L9,19.5 C9,20.3284271 8.32842712,21 7.5,21 L6.5,21 C5.67157288,21 5,20.3284271 5,19.5 L5,17.5 C5,16.6715729 5.67157288,16 6.5,16 Z M16.5,16 L17.5,16 C18.3284271,16 19,16.6715729 19,17.5 L19,19.5 C19,20.3284271 18.3284271,21 17.5,21 L16.5,21 C15.6715729,21 15,20.3284271 15,19.5 L15,17.5 C15,16.6715729 15.6715729,16 16.5,16 Z"
                        fill="#000000" opacity="0.3" />
                    <path
                        d="M5,4 L19,4 C20.1045695,4 21,4.8954305 21,6 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6 C3,4.8954305 3.8954305,4 5,4 Z M15.5,15 C17.4329966,15 19,13.4329966 19,11.5 C19,9.56700338 17.4329966,8 15.5,8 C13.5670034,8 12,9.56700338 12,11.5 C12,13.4329966 13.5670034,15 15.5,15 Z M15.5,13 C16.3284271,13 17,12.3284271 17,11.5 C17,10.6715729 16.3284271,10 15.5,10 C14.6715729,10 14,10.6715729 14,11.5 C14,12.3284271 14.6715729,13 15.5,13 Z M7,8 L7,8 C7.55228475,8 8,8.44771525 8,9 L8,11 C8,11.5522847 7.55228475,12 7,12 L7,12 C6.44771525,12 6,11.5522847 6,11 L6,9 C6,8.44771525 6.44771525,8 7,8 Z"
                        fill="#000000" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Accounting</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link">
                        <span class="kt-menu__link-text">Accounting</span></span></li>
                <li id="s-13" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('opex') }}','s-13')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Opex</span>
                    </a>
                </li>
                <li id="s-14" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('transaksi_single') }}','s-14')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Single Transaction</span>
                    </a>
                </li>
                <li id="s-15" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('transaksi_baru') }}','s-15')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Multi Overbooking</span>
                    </a>
                </li>
                <li id="s-17" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('list_tr_baru')}}','s-17')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">New Transaction List</span>
                    </a>
                </li>

                <li id="s-16" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('master_coa') }}','s-16')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Master COA</span>
                    </a>
                </li>
                <li id="s-18" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('list_app_baru') }}','s-18')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Transaction</span>
                    </a>
                </li>

                <li id="s-19" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('list_reversal') }}','s-19')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Reversal Transaction</span>
                    </a>
                </li>
                <li id="s-20" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('list_trx_success') }}','s-20')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Success Transaction List</span>
                    </a>
                </li>
                <li id="s-21" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('list_rev_success') }}','s-21')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Success Reversal List</span>
                    </a>
                </li>
                {{-- <li class="kt-menu__section ">
                            <h4 class="kt-menu__section-text" style="
                            margin-left: 20px;
                            font-size: 13px;
                            font-weight: bold;
                        ">Prepaid Expense</h4>
                            <i class="kt-menu__section-icon flaticon-more-v2"></i>
                        </li> --}}
                <li id="s-22" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('biaya_ditangguhkan.index') }}?type=new','s-22')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">New Prepaid Expense List</span>
                    </a>
                </li>

                <li id="s-23" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('biaya_ditangguhkan.index') }}?type=success','s-23')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Success Prepaid Expense List</span>
                    </a>
                </li>

                <li id="s-24" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('biaya_ditangguhkan.index') }}?type=approve','s-24')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval New Prepaid Expense </span>
                    </a>
                </li>

                <li id="s-25" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('biaya_ditangguhkan.index') }}?type=approve_reversal','s-25')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Delete Prepaid Expense</span>
                    </a>
                </li>

                <li id="s-26" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('biaya_ditangguhkan.scheduleList') }}','s-26')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Payment</span>
                    </a>
                </li>

                <li id="s-27" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('biaya_ditangguhkan.scheduleListCancel') }}','s-27')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Cancel Payment</span>
                    </a>
                </li>

                <li id="s-28" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('biaya_ditangguhkan.index') }}?type=deleted','s-28')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Deleted Prepaid Expense</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
    {{-- CUSTOMER --}}
    <li id="h-customer" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
            class="kt-menu__link-icon kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon flaticon-user"></i> --}}
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon kt-svg-icon--success">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <path
                        d="M6.182345,4.09500888 C6.73256296,3.42637697 7.56648864,3 8.5,3 L15.5,3 C16.4330994,3 17.266701,3.42600075 17.8169264,4.09412386 C17.8385143,4.10460774 17.8598828,4.11593789 17.8809917,4.1281251 L22.5900048,6.8468751 C23.0682974,7.12301748 23.2321726,7.73460788 22.9560302,8.21290051 L21.2997802,11.0816097 C21.0236378,11.5599023 20.4120474,11.7237774 19.9337548,11.4476351 L18.5,10.6198563 L18.5,19 C18.5,19.5522847 18.0522847,20 17.5,20 L6.5,20 C5.94771525,20 5.5,19.5522847 5.5,19 L5.5,10.6204852 L4.0673344,11.4476351 C3.58904177,11.7237774 2.97745137,11.5599023 2.70130899,11.0816097 L1.04505899,8.21290051 C0.768916618,7.73460788 0.932791773,7.12301748 1.4110844,6.8468751 L6.12009753,4.1281251 C6.14061376,4.11628005 6.16137525,4.10524462 6.182345,4.09500888 Z"
                        fill="#000000" opacity="0.3" />
                    <path
                        d="M9.85156673,3.2226499 L9.26236944,4.10644584 C9.11517039,4.32724441 9.1661011,4.62457583 9.37839459,4.78379594 L11,6 L10.0353553,12.7525126 C10.0130986,12.9083095 10.0654932,13.0654932 10.1767767,13.1767767 L11.6464466,14.6464466 C11.8417088,14.8417088 12.1582912,14.8417088 12.3535534,14.6464466 L13.8232233,13.1767767 C13.9345068,13.0654932 13.9869014,12.9083095 13.9646447,12.7525126 L13,6 L14.6216054,4.78379594 C14.8338989,4.62457583 14.8848296,4.32724441 14.7376306,4.10644584 L14.1484333,3.2226499 C14.0557004,3.08355057 13.8995847,3 13.7324081,3 L10.2675919,3 C10.1004153,3 9.94429962,3.08355057 9.85156673,3.2226499 Z"
                        fill="#000000" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Master
                Customer</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span
                            class="kt-menu__link-text">Master Customer</span></span>
                </li>
                <li id="s-29" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('customer_baru') }}','s-29')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Create New Customer</span>
                    </a>
                </li>
                <li id="s-30" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('customer_list') }}','s-30')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">New Customer List</span>
                    </a>
                </li>
                <li id="s-31" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('customer_list_active') }}','s-31')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Active Customer</span>
                    </a>
                </li>
                <li id="s-715" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('customer_list_non_active') }}','s-715')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Non Active Customer</span>
                    </a>
                </li>
                <li id="s-32" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('customer_approval') }}','s-32')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Customer</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>

    {{-- REPORT --}}
    <li id="h-report" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon flaticon-statistics"></i> --}}
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon kt-svg-icon--info">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <path
                        d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                        fill="#000000" opacity="0.3" />
                    <path
                        d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                        fill="#000000" />
                    <rect fill="#000000" opacity="0.3" x="7" y="10" width="5" height="2" rx="1" />
                    <rect fill="#000000" opacity="0.3" x="7" y="14" width="9" height="2" rx="1" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Report</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span
                            class="kt-menu__link-text">Report</span></span>
                </li>
                <li id="s-33" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('balance_sheet') }}','s-33')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Balance Sheet</span>
                    </a>
                </li>
                <li id="s-34" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('laba_rugi') }}','s-34')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Profit and Loss</span>
                    </a>
                </li>
                <li id="s-35" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('cashflow') }}','s-35')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Cash Flow</span>
                    </a>
                </li>
                <li id="s-36" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('sales_product') }}','s-36')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Sales by Product</span>
                    </a>
                </li>
                <li id="s-37" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('sales_officer') }}','s-37')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Sales by Agent </span>
                    </a>
                </li>
                <li id="s-38" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('sales_branch') }}','s-38')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Sales by Branch</span>
                    </a>
                </li>
                <li id="s-82" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('sales_officer_parent') }}','s-82')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Sales by Officer</span>
                    </a>
                </li>
                <li id="s-80" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('sales_customer_segment') }}','s-80')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Sales by Customer Segment</span>
                    </a>
                </li>
                <li id="s-81" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('sales_company') }}','s-81')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Sales by Company Non Company</span>
                    </a>
                </li>
                <li id="s-77" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('nominative_report') }}','s-77')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Customer Nominative Report</span>
                    </a>
                </li>
                <li id="s-83" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('download_laporan.list') }}','s-83')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Download File Laporan</span>
                    </a>
                </li>
                <li id="s-84" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('invoice_pending_split_payment') }}','s-84')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Debit Note Pending Split Payment</span>
                    </a>
                </li>
                <li id="s-87" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('due_date_installment') }}','s-87')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text"> Due Date Installment </span>
                    </a>
                </li>
                <li id="s-88" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('claim_report') }}','s-88')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text"> Claim Report </span>
                    </a>
                </li>
                <li id="s-89" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('installment_pending_split_payment') }}','s-89')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Installment Pending Split Payment</span>
                    </a>
                </li>
                <li id="s-810" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('qs_report',['aktif']) }}','s-810')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Laporan Nominatif QS</span>
                    </a>
                </li>
                <li id="s-812" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('renewal_list_report') }}','s-812')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Renewal List Report</span>
                    </a>
                </li>
                <li id="s-813" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('renewal_list_due_date') }}','s-813')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Renewal List Due Date</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>


    {{-- INVENTORY --}}
    <li id="h-inventory" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon flaticon-open-box"></i> --}}
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon kt-svg-icon--danger">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <path
                        d="M20.4061385,6.73606154 C20.7672665,6.89656288 21,7.25468437 21,7.64987309 L21,16.4115967 C21,16.7747638 20.8031081,17.1093844 20.4856429,17.2857539 L12.4856429,21.7301984 C12.1836204,21.8979887 11.8163796,21.8979887 11.5143571,21.7301984 L3.51435707,17.2857539 C3.19689188,17.1093844 3,16.7747638 3,16.4115967 L3,7.64987309 C3,7.25468437 3.23273352,6.89656288 3.59386153,6.73606154 L11.5938615,3.18050598 C11.8524269,3.06558805 12.1475731,3.06558805 12.4061385,3.18050598 L20.4061385,6.73606154 Z"
                        fill="#000000" opacity="0.3" />
                    <polygon fill="#000000"
                        points="14.9671522 4.22441676 7.5999999 8.31727912 7.5999999 12.9056825 9.5999999 13.9056825 9.5999999 9.49408582 17.25507 5.24126912" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Inventory</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span
                            class="kt-menu__link-text">Inventory</span></span>
                </li>
                <li id="s-39" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.form','create') }}','s-39')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Create New Inventory</span>
                    </a>
                </li>

                <li id="s-40" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.index') }}?type=new','s-40')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">New Inventory List</span>
                    </a>
                </li>

                <li id="s-41" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.index') }}?type=success','s-41')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Active Inventory List</span>
                    </a>
                </li>
                <li id="s-42" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.index') }}?type=approve','s-42')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval New Inventory</span>
                    </a>
                </li>
                <li id="s-43" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.index') }}?type=approve_reversal','s-43')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Delete Inventory</span>
                    </a>
                </li>

                <li id="s-44" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.scheduleList') }}','s-44')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Payment</span>
                    </a>
                </li>

                <li id="s-45" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.scheduleListCancel') }}','s-45')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Approval Cancel Payment</span>
                    </a>
                </li>

                <li id="s-46" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('inventory.index') }}?type=deleted','s-46')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Deleted Inventory</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>


    <li id="h-batch" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            {{-- <i class="kt-menu__link-icon flaticon-open-box"></i> --}}
            <svg width="34px" height="34px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon kt-svg-icon--info">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="4" y="10" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="10" y="4" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="10" y="10" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="16" y="4" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="16" y="10" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="4" y="16" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="10" y="16" width="4" height="4" rx="2" />
                    <rect fill="#000000" x="16" y="16" width="4" height="4" rx="2" />
                </g>
            </svg>
            <span class="kt-menu__link-text">Batch Proses</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span
                            class="kt-menu__link-text">Batch Proses</span></span>
                </li>
                <li id="s-47" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('batch.index') }}?type_batch=day','s-47')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">End Of Day</span>
                    </a>
                </li>
                <li id="s-48" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('batch.index') }}?type_batch=month','s-48')"
                        class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">End Of Month</span>
                    </a>
                </li>

                <li id="s-90" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="endofyear()" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">End Of Year</span>
                    </a>
                </li>


            </ul>
        </div>
    </li>

    {{-- REFERENSI --}}


    <li id="h-ref" class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
        data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                class="kt-menu__link-icon kt-svg-icon kt-svg-icon--success">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path
                        d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z"
                        fill="#000000" fill-rule="nonzero" />
                    <path
                        d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z"
                        fill="#000000" opacity="0.3" />
                </g>
            </svg>
            <span class="kt-menu__link-text">
                Reference</span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu"><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span
                            class="kt-menu__link-text">Reference</span></span>
                </li>

                <li id="s-49" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('master_company')}}','s-49')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Company</span>
                    </a>
                </li>
                <li id="s-50" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('master_branch')}}','s-50')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Branch</span>
                    </a>
                </li>
                <li id="s-51" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('branch_monitoring')}}','s-51')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Branch Monitoring</span>
                    </a>
                </li>
                <li id="s-52" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('bdd')}}','s-52')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">BDD</span>
                    </a>
                </li>
                <li id="s-53" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('master_user')}}','s-53')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">User </span>
                    </a>
                </li>
                <li id="s-54" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('agent') }}','s-54')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Agent</span>
                    </a>
                </li>
                <li id="s-55" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{ route('agent_type') }}','s-55')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Agent Type</span>
                    </a>
                </li>
                <li id="s-56" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('bank_account')}}','s-56')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Bank Account</span>
                    </a>
                </li>
                <li id="s-57" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('city')}}','s-57')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">City</span>
                    </a>
                </li>
                <li id="s-58" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('province')}}','s-58')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Province</span>
                    </a>
                </li>

                <li id="s-59" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('coa_group')}}','s-59')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">COA Group</span>
                    </a>
                </li>
                <li id="s-60" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('coa_type')}}','s-60')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">COA Type</span>
                    </a>
                </li>
                <li id="s-61" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('segment')}}','s-61')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">COA Segment</span>
                    </a>
                </li>
                <li id="s-62" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('customer_type')}}','s-62')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Customer Type</span>
                    </a>
                </li>
                <li id="s-63" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('customer_segment')}}','s-63')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Customer Segment</span>
                    </a>
                </li>
                <li id="s-64" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('customer_group')}}','s-64')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Customer Group</span>
                    </a>
                </li>
                <li id="s-65" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('paid_status')}}','s-65')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Paid Status</span>
                    </a>
                </li>
                <li id="s-66" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('quotation')}}','s-66')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Quotation</span>
                    </a>
                </li>
                <li id="s-67" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('proposal')}}','s-67')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Proposal</span>
                    </a>
                </li>
                <li id="s-75" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('product')}}','s-75')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Product</span>
                    </a>
                </li>
                <li id="s-844" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('rate_usia')}}','s-844')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Rate Usia</span>
                    </a>
                </li>
                <li id="s-68" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('inventory')}}','s-68')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Inventory</span>
                    </a>
                </li>
                <li id="s-74" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('menu')}}','s-74')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">User Role Menu</span>
                    </a>
                </li>
                <li id="s-76" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('underwriter')}}','s-76')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Underwriter</span>
                    </a>
                </li>
                <li id="s-811" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('qs_content')}}','s-811')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">QS Content</span>
                    </a>
                </li>
                <li id="s-822" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('branch_customer')}}','s-822')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Branch Customer</span>
                    </a>
                </li>
                <li id="s-833" class="kt-menu__item " aria-haspopup="true">
                    <a onclick="loadPageMenu('{{route('insured_detail')}}','s-833')" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span class="kt-menu__link-text">Insured Detail</span>
                    </a>
                </li>
                {{-- <li class="kt-menu__item " aria-haspopup="true">
                            <a onclick="loadPageMenu('{{route('sub_menu')}}')" class="kt-menu__link ">
                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                <span class="kt-menu__link-text">Sub Menu</span>
                </a>
    </li> --}}
    <li id="s-69" class="kt-menu__item " aria-haspopup="true">
        <a onclick="loadPageMenu('{{route('tx_type')}}','s-69')" class="kt-menu__link ">
            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
            <span class="kt-menu__link-text">Transaction Type</span>
        </a>
    </li>
    <li id="s-70" class="kt-menu__item " aria-haspopup="true">
        <a onclick="loadPageMenu('{{route('user_role')}}','s-70')" class="kt-menu__link ">
            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
            <span class="kt-menu__link-text">User Role</span>
        </a>
    </li>

    <li id="s-71" class="kt-menu__item " aria-haspopup="true">
        <a onclick="loadPageMenu('{{route('workflow')}}','s-71')" class="kt-menu__link ">
            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
            <span class="kt-menu__link-text">Workflow Status </span>
        </a>
    </li>
    <li id="s-72" class="kt-menu__item " aria-haspopup="true">
        <a onclick="loadPageMenu('{{route('master_workflow')}}','s-72')" class="kt-menu__link ">
            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
            <span class="kt-menu__link-text">Master Workflow </span>
        </a>
    </li>
    <li id="s-73" class="kt-menu__item " aria-haspopup="true">
        <a onclick="loadPageMenu('{{route('master_config')}}','s-73')" class="kt-menu__link ">
            <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
            <span class="kt-menu__link-text">Config</span>
        </a>
    </li>

    </ul>
</div>
</li>

</ul>
</div>
</div>
<script type="text/javascript">
    function endofyear() {

        swal.fire({
            title: "Info",
            text: "Proses End Of Year ?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then(function (result) {
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'GET',
                    url: base_url + '/endofyear',
                    success: function (res) {

                        endLoadingPage();

                        var obj = JSON.parse(res);

                        console.log(obj.msg);
                        console.log(obj['msg']);

                        if (obj.rc == 1) {
                            swal.fire({
                                title: "Informasi",
                                text: obj.msg,
                                type: "success",
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function (result) {
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        } else {
                            swal.fire("Error", obj.msg, "error");
                        }



                    }
                }).done(function (res) {
                    endLoadingPage();

                }).fail(function (res) {
                    endLoadingPage();
                    swal.fire("Error", "Terjadi Kesalahan!", "error");
                });
            }
        });





   }
</script>
