<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	@include('master.head')
	<!-- end::Head -->
	@php
		$data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
	@endphp
	<!-- begin::Body -->
	<body id="bg_new" class="scrollStyle kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

    	@include('master.component')

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="">
				
					<img style="width:40px;" class="img-fluid" alt="Logo" src="{{asset('img/'.$data_imgz->url_image)}}" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
			</div>
		</div>

		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<!-- Loading -->
			<div id="loading">
				<div class="lds-facebook">
					<div></div>
					<div></div>
					<div></div>
				</div>
            </div>

            <div id="loading2">
				
			
                <div class="zn-load-text" id="zn-load-text">
						<div id="view_batch_inventory" class="row" style="
						border-bottom: 1px solid #ffffff17;
						padding: 15px;">
								<div class="col-md-6">
										<div class="zn-load-text">Batch Inventory</div>
								</div>
								<div class="col-md-6">
									<span id="stat_batch_inv" class="btn btn-info btn-sm">On Processing</span>
								</div>
							</div>
							<div id="view_batch_bdd" class="row" style="
							border-bottom: 1px solid #ffffff17;
							padding: 15px;">
									<div class="col-md-6">
											<div class="zn-load-text">Batch BDD</div>
									</div>
									<div class="col-md-6">
										<span id="stat_batch_bdd" class="btn btn-info btn-sm">Waiting</span>
									</div>
								</div>
							<div class="row" style="
							border-bottom: 1px solid #ffffff17;
							padding: 15px;">
									<div class="col-md-6">
											<div class="zn-load-text">Financial</div>
									</div>
									<div class="col-md-6">
										<span id="stat_batch_fin" class="btn btn-info btn-sm">Waiting</span>
									</div>
								</div>
					</div>
                {{-- <div class="zn-load-text" id="zn-load-text">Financial</div> --}}
				
				<div class="lds-facebook" style="top: 60%;">
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>

			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
						<div class="kt-container  kt-container--fluid ">

							<!-- begin: Header Menu -->
							<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

							<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
								<button class="kt-aside-toggler kt-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
								<a class="kt-header__brand-logo" href="#">
                                    <div style="display: inline-block;">
                                        <img alt="Logo" src="{{asset('img/hdlogo.png')}}" style="width: 40px;margin-top: -25px;" />
                                    </div>
                                    <div style="display: inline-block;">
                                        <span class="zn-text-logo" style="display: block;"> {{$data_imgz->app_name}} </span>
                                        <span style="display: block;margin-left: 8px;color: #eb5c28;
                                        font-weight: 400;">"Simple & Inovative Solution" </span>
                                    </div>
                                </a>

							</div>

							<!-- begin:: Header Topbar -->
							<div class="kt-header__topbar kt-grid__item">

								<!--begin: User bar -->
								<div class="kt-header__topbar-item kt-header__topbar-item--user">
									<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
										@php
											 $systemDate = collect(\DB::select("select * from ref_system_date"))->first();
										@endphp
										
										<div style="margin-right: 15px;text-align: right;">
											<span style="display: inline !important;" class="kt-header__topbar-welcome kt-visible-desktop">Hi,</span>
											<span style="display: inline !important;" class="kt-header__topbar-username kt-visible-desktop">{{{Auth::user()->fullname}}}</span>
											@if ($systemDate->current_date == date('Y-m-d'))
												<span class="btn btn-label-success btn-sm zn-branch-label">Branch Operational is Open</span>
											@else
												<span class="btn btn-label-danger btn-sm zn-branch-label">Branch Operational is Closed</span>
											@endif
										</div>
										
										<img alt="Pic" src="{{asset(Auth::user()->image_url)}}" style="
                                        width: 42px;
                                        border: 2px solid #7b7b7b;
                                        border-radius: 10px;
                                    " />

                                        {{-- <i class="flaticon-user" style="
                                        font-size: 30px;
                                        background: #f5f6fc;
                                        padding: 2px 10px;
                                        border-radius: 5px;
                                        margin-left: 10px;
                                    "></i> --}}
										<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
										<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
									</div>
									<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

										<!--begin: Head -->
										<div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
											<div class="kt-user-card__avatar">
												{{-- <img class="kt-hidden-" alt="Pic" src="{{asset('assets/media/users/300_25.jpg')}}" /> --}}

												<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
												<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
											</div>
											<div class="kt-user-card__name">
                                                {{{Auth::user()->username}}}
											</div>
											<div class="kt-user-card__badge">
												@php
													$userRole = \DB::table('ref_user_role')->where('id', Auth::user()->user_role_id)->first();
												@endphp
												<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">{{ $userRole->definition }}</span>
											</div>
										</div>

										<!--end: Head -->

										<!--begin: Navigation -->
										<div class="kt-notification">

											<div class="kt-notification__custom kt-space-between">
                                                	<form action="{{ route('logout') }}" method="post">
                                                            {{ csrf_field() }}
                                                            <input type="submit" value="Sign Out" class="btn btn-label btn-label-brand btn-sm btn-bold">
                                                       </form>
													   <button onclick="mChangePassword()" class="btn btn-label btn-label-danger btn-sm btn-bold">Change Password</button>
												{{-- <a href="custom/user/login-v2&demo=demo9.html" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a> --}}
											</div>
										</div>

										<!--end: Navigation -->
									</div>
								</div>

							</div>

						</div>
					</div>

					<!-- end:: Header -->

					<!-- begin:: Aside -->
					<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
					<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

						<!-- begin:: Aside Menu -->
                            @include('master.menu')
                        <!-- end:: Aside Menu -->

                        <script>
                            var act_url = '{{ route('role.getMenu') }}';
                            $.ajax({
                                url: act_url,
                                type: 'GET',
                                beforeSend: function() {
                                    $("[id^='s-']").hide();
                                    $("[id^='h-']").hide();
                                    loadingPage();
                                },
                                success: function (res) {
									var data = $.parseJSON(res);
									console.log(data);
                                    $.each(data, function (k,v) {
                                        $('#h-'+v.type).show();
                                        $('#'+v.code_menu).show();
                                    });
                                }
                            }).done(function( msg ) {
                                endLoadingPage();
                            });
                        </script>
					</div>

					<!-- end:: Aside -->
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
							<div id="pageLoad">
								@yield('content')
							</div>
						</div>
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer kt-grid__item" id="kt_footer">
						<div class="kt-container ">
							<div class="kt-footer__wrapper">
								<div class="kt-footer__copyright">
									2019&nbsp;&copy;&nbsp; Basys</a>
								</div>
							</div>
						</div>
					</div>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->



		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->

		<div class="modal fade in" id="modal_change_password" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog ">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Change Password </h5>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					</div>
		
					<div class="modal-body">
						<form id="mChangePasswordForm">
							<div class="form-group">
								<label>Password Lama</label>
								<div class="input-group input-group-solid input-group-md mb-2 zn_sh_password">
									<input type="password" class="form-control" name="oldPassword" id="oldPassword" placeholder="XXXXXXX"/>
									<div style="cursor: pointer;" onclick="znShowPassword()">
										<span class="input-group-text zn-icon-eye" style="height: 39px;"><i class="la la-eye-slash"></i></span>
									</div>
								</div>
		
								
							</div>
							<div class="form-group">
								<label>Password Baru</label>
								<div class="input-group input-group-solid input-group-md mb-2 zn_sh_password">
									<input type="password" class="form-control" name="newPassword" id="newPassword" placeholder="XXXXXXX"/>
									<div style="cursor: pointer;" onclick="znShowPassword()">
										<span class="input-group-text zn-icon-eye" style="height: 39px;"><i class="la la-eye-slash"></i></span>
									</div>
								</div>
		
								
							</div>
							<div class="form-group">
								<label>Ketik Ulang Password Baru</label>
								<div class="input-group input-group-solid input-group-md mb-2 zn_sh_password">
									<input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="XXXXXXX"/>
									<div style="cursor: pointer;" onclick="znShowPassword()">
										<span class="input-group-text zn-icon-eye" style="height: 39px;"><i class="la la-eye-slash"></i></span>
									</div>
								</div>
		
								
							</div>
						</form>
						
					</div>
		
					<div class="modal-footer">
						<button onclick="changePasswordAct()" type="button" class="btn btn-success">Change</button>
					</div>
		
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>


	</body>

	<!-- end::Body -->
</html>
