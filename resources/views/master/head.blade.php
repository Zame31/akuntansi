<head>
    <base href="">
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ATA HD</title>
    <meta name="description" content="Akuntansi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('img/logo.jpg')}}" />
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css" />


    <link href="{{asset('assets/plugins/general/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />


    <link href="{{asset('assets/plugins/custom/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-keytable-bs4/css/keyTable.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />


    <link href="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages/invoices/invoice-1.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages/wizard/wizard-2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link href="{{asset('css/myStyle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/My.css')}}" rel="stylesheet" type="text/css" />

    <!-- Js Tree -->

    <style media="screen">
        #bg_new {
        background: url('{{ asset('img/pat1.png') }}');
        background-repeat: repeat;
        margin-bottom: 0px;
        opacity: 1;
        }
    </style>


    <script src="{{asset('assets/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/plugins/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/js/global/integration/plugins/bootstrap-datepicker.init.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/js/global/integration/plugins/datatables.init.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/jszip/dist/jszip.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/pdfmake/build/pdfmake.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/pdfmake/build/vfs_fonts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-buttons/js/buttons.print.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables.net-select/js/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/plugins/general/plugins/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js')}}" type="text/javascript"></script>

    {{-- <script src="{{asset('js/session_timeout.js')}}" type="text/javascript"></script> --}}

     {{-- <script src="{{asset('js/jquery.backDetect.js')}}"></script> --}}

    {{-- <script src="{{asset('assets/plugins/general/jquery-form/dist/jquery.form.min.js')}}" type="text/javascript"></script> --}}
    <script src="{{asset('assets/js/pages/custom/wizard/wizard-2.js')}}" type="text/javascript"></script>

    <script src="{{asset('js/chartjs.js')}}"></script>


    <script src="{{asset('js/_global.js')}}" type="text/javascript"></script>

    <script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>

    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

    <script src="{{asset('js/jspdf.debug.js')}}"></script>
    <script src="{{asset('assets/plugins/general/summernote/dist/summernote.js')}}" type="text/javascript"></script>

    <script src="{{asset('js/jquery.inputmask.bundle.min.js')}}" type="text/javascript"></script>

    <script>
        var base_url = "{{ url('/') }}"+"/";
        console.log(base_url);


        var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#591df1",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};

            function mChangePassword() {
                $("#mChangePasswordForm").bootstrapValidator({
					excluded: [':disabled'],
					fields: {
						newPassword: {
							validators: {
								notEmpty: {
									message: 'Tidak Boleh Kosong'
								},
								identical: {
									field: 'confirmPassword',
									message: 'Password Tidak Sama '
								},
								regexp: {
									regexp: /^(?=.*?[a-z])(?=.*?[0-9]).{6,20}$/,
									message: 'Password minimal 6 karakter dan maksimal 20 karakter terdiri dari huruf dan angka. Contoh : superuser1'
								}
							}
						},
						confirmPassword: {
							validators: {
								notEmpty: {
								message: 'Tidak Boleh Kosong'
							},
								identical: {
									field: 'newPassword',
									message: 'Tidak Sama Dengan Password Baru'
								}
							}
						},
						oldPassword: {
							validators: {
								notEmpty: {
									message: 'Tidak Boleh Kosong'
								}
							}
						},
					}
				}).on('success.field.bv', function(e, data) {
					var $parent = data.element.parents('.form-group');
					$parent.removeClass('has-success');
					$parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
				});

                $('#modal_change_password').modal('show');
            }

            function changePasswordAct() {
                	var $validor = $('#mChangePasswordForm').data('bootstrapValidator').validate();
                    if ($validor.isValid()) {
                        let myForm = document.getElementById('mChangePasswordForm');
                        let formData = new FormData(myForm);

                        $.ajax({
                            type: "POST",
                            url: '{{ route('user.changePassword') }}',
                            data : formData,
                            dataType:'JSON',
                                contentType: false,
                                cache: false,
                                processData: false,
                            beforeSend: function() {
                                loadingPage();
                            },
                            success: function( msg ) {
                                endLoadingPage();
                            }
                        }).done(function( msg ) {

                            if(msg.rc == 99){
                                toastr.warning('Password Lama Tidak Sesuai');
                            }else{
                                toastr.success('Password Berhasil Dirubah');
                                $('#modal_change_password').modal('hide');
                            }
                            
                        }).fail(function(msg) {
                            endLoadingPage();
                            toastr.warning('Gagal, Terjadi Kesalahan');
                        });
                    }
            }

            
				
			function znShowPassword() {
				if($('.zn_sh_password input').attr("type") == "text"){
					$('.zn_sh_password input').attr('type', 'password');
					$('.zn-icon-eye i').addClass( "la-eye-slash" );
					$('.zn-icon-eye i').removeClass( "la-eye" );
				}
				else if($('.zn_sh_password input').attr("type") == "password"){
					$('.zn_sh_password input').attr('type', 'text');
					$('.zn-icon-eye i').removeClass( "la-eye-slash" );
					$('.zn-icon-eye i').addClass( "la-eye" );
				}
			}
    </script>
</head>
