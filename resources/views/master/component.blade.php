<div class="modal fade" id="zn-iconbox" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div id="zn-iconbox-color" class="modal-body kt-portlet kt-iconbox  kt-iconbox--animate-slow m-0">
                <div class="">
                    <div class="kt-portlet__body">
                        <div class="kt-iconbox__body">
                            <div id="kt-iconbox__icon" class="kt-iconbox__icon">

                            </div>
                            <div class="kt-iconbox__desc">
                                <h3 class="kt-iconbox__title">
                                    <a class="kt-link" href="#" id="zn-iconbox-tittle"></a>
                                </h3>
                                <div class="kt-iconbox__content">
                                    <span class="zn-iconbox-tittle">
                                        <div id="zn-iconbox-text"></div>
                                        <div class="mt-3" id="zn-iconbox-action">

                                        </div>
                                    </span>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
