<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->

<head>
    <base href="">
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Akuntansi</title>
    <meta name="description" content="Login Akuntansi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <!--end::Fonts -->

    <link href="{{asset('assets/plugins/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/line-awesome/css/line-awesome.css')}}" rel="stylesheet"
        type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet"
        type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet"
        type="text/css" />

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/css/pages/login/login-3.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert CSS -->
    <link href="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet"
        type="text/css" />

    <!-- Toastr -->
    <link href="{{asset('assets/plugins/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Validator -->
    <link href="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.css')}}" rel="stylesheet"
        type="text/css" />

    <!-- My Css -->
    <link href="{{asset('css/My.css')}}" rel="stylesheet" type="text/css" />

    <!-- My Style -->
    <link href="{{asset('css/myStyle.css')}}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{asset('img/logo.jpg')}}" />

    <link rel="stylesheet" href="{{asset('assets/owl/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owl/owlcarousel/assets/owl.theme.default.min.css')}}">

    <script src="{{asset('assets/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('assets/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>

    <!-- Bootstrap Validator -->
    <script src="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.js')}}" type="text/javascript">
    </script>

    <script src="{{asset('assets/owl/owlcarousel/owl.carousel.js')}}"></script>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

        <!-- Loading -->
        <div id="loading">
            <div class="lds-facebook">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

        <!-- begin:: Page -->
        <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
                <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                        style="background-size: cover;background-image: url({{asset('img/bg3.jpg')}});">
                        <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                            <div class="kt-login__container">
                                <div class="kt-login__body">
                                    <div class="kt-login__logo mb-2">
                                        <a href="#">
                                            {{-- logo --}}
                                            @php
                                                $data_comp = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                            @endphp
                                            <img src="{{asset('img/'.$data_comp->url_image)}}" style="margin-bottom: 60px;width: 400px;">
                                        </a>
                                    </div>
                                    <div class="kt-login__signin">
                                        <div class="kt-login__head" style="
                                        color: white;text-align:center;
                                    ">
                                                <h3 class="kt-login__title" style="
                                                color: white;
                                            "> {{$data_comp->app_name}} </h3>
                                        <h5> "Simple & Inovative Solution"</h5>
                                        </div>
                                        <div class="kt-login__form">
                                            {{-- <form class="kt-form" method="POST" action="{{ route('login') }}"> --}}
                                            <form class="kt-form" id="loginForm">
                                                @csrf

                                                    <!-- Alert Error -->
                                                    <div class="alert alert-danger alert-dismissible d-none" id="alertError">
                                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    </div>

                                                    <!-- Alert Info -->
                                                    <div class="alert alert-warning alert-dismissible d-none" id="alertInfo">
                                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    </div>

                                                    <!-- Alert Success -->
                                                    <div class="alert alert-success alert-dismissible d-none" id="alertSuccess">
                                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    </div>

                                                <div class="form-group">
                                                    <input class="form-control" style="color:#fff;background: #00000026;font-weight: 500;" type="text" placeholder="Username" name="username" autocomplete="off" id="inputUsername">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control form-control-last" style="color:#fff;background: #00000026;font-weight: 500;" type="password" placeholder="Password" name="password" id="inputPassword">
                                                </div>
                                                <div class="kt-login__extra">

                                                </div>
                                                <div class="kt-login__actions">
                                                        <button onclick="login();" id="kt_login_signin_submit"
                                                        class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button>

                                                </div>

                                                {{-- <div class="owl-carousel owl-theme" style="
                                                margin-top: 30%;
                                            ">
                                                        <div class="item zn-ca">
                                                                <img class="img-fluid" src="{{asset('img/hd1.png')}}" >
                                                        </div>
                                                        <div class="item zn-ca" >
                                                                <img class="img-fluid" src="{{asset('img/hd2-new.png')}}" >
                                                        </div>
                                                        <div class="item zn-ca">
                                                                <img class="img-fluid" src="{{asset('img/hd3.png')}}">
                                                        </div>

                                                      </div> --}}

                                                {{-- <marquee behavior="" direction = "right">
                                                <div class="row" style="margin-top: 30px;">
                                                    <div class="col-4">
                                                        <img class="img-fluid" src="{{asset('img/hd1.png')}}" >
                                                    </div>
                                                    <div class="col-4">
                                                            <img class="img-fluid" src="{{asset('img/hd2.png')}}" >
                                                        </div>
                                                        <div class="col-4">
                                                            <img class="img-fluid" src="{{asset('img/hd3.png')}}">
                                                        </div>
                                                </div>
                                            </marquee> --}}

                                            </form>
                                        </div>
                                    </div>

                                    @php
                                    $runing_text = collect(\DB::select("SELECT parm_name FROM master_config where value = 99"))->first();
                                @endphp
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <marquee behavior="" direction="" style="
            color: #fff;
            font-size: 14px;
            font-weight: 500;
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            background: #00000085;
            padding: 2px;
        ">{{$runing_text->parm_name}}</marquee>
        <!-- Modal -->
        <div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="title_modal"> Ubah Password </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    </div>

                    <div class="modal-body">
                        <form id="form-data" method="POST">
                            {{-- @csrf --}}
                            <input type="hidden" name="id" value="" id="id">
                            <div class="form-group">
                                <label for="branch_name">Password Baru</label>
                                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Masukan Password Baru">
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                        <button type="button" class="btn btn-success" onclick="resetPassword();">Simpan</button>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!--end:: Vendor Plugins -->
        <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

        <!-- Action Login -->
        {{-- @include('auth.action_login'); --}}

        <script type="text/javascript">

            $(document).ready(function() {

                 // enable enter in form login
                    $('#loginForm').on('keyup keypress', function (e) {
                        var keyCode = e.keyCode || e.which;
                        if (keyCode === 13) {
                            login();
                        }
                    });

              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                pagination: false,
                dots: false,
                autoplay:true,
                autoplayTimeout:3000,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
           

            // disable enter in form reset password
            $('#form-data').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    return false;
                }
            });

            $(document).ready(function () {

                // validate reset password
                $("#form-data").bootstrapValidator({
                    excluded: [':disabled'],
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        new_password: {
                            validators: {
                                notEmpty: {
                                    message: 'Silahkan isi'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Silahkan isi minimal 6 karakter'
                                }
                            }
                        },
                    }
                }).on('success.field.bv', function (e, data) {
                    var $parent = data.element.parents('.form-group');
                    $parent.removeClass('has-success');
                    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
                });

                // validate login
                $("#loginForm").bootstrapValidator({
                    excluded: [':disabled'],
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        username: {
                            validators: {
                                notEmpty: {
                                    message: 'Silahkan isi'
                                },
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'Silahkan isi'
                                },
                            }
                        },
                    }
                }).on('success.field.bv', function (e, data) {
                    var $parent = data.element.parents('.form-group');
                    $parent.removeClass('has-success');
                    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
                });
            });

            function login(){
                var validateLogin = $('#loginForm').data('bootstrapValidator').validate();
                if (validateLogin.isValid()) {

                    var formData = document.getElementById("loginForm");
                    var objData = new FormData(formData);

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('login') }}',
                        data: objData,
                        dataType: 'JSON',
                        contentType: false,
                        cache: false,
                        processData: false,

                        beforeSend: function () {
                            $("#alertInfo").addClass('d-none');
                            $("#alertError").addClass('d-none');
                            $("#alertSuccess").addClass('d-none');
                            $("#loading").css('display', 'block');
                        },

                        success: function (response) {
                            console.log(response);
                            $("#loading").css('display', 'none');
                            switch (response.rc) {
                                // password / username invalid
                                case 0:
                                    $("#inputUsername").val('');
                                    $("#inputPassword").val('');
                                    $("#alertError").removeClass('d-none');
                                    $("#alertError").text(response.rm);
                                break;

                                // akun tidak aktif
                                case 1:
                                    $("#inputUsername").val('');
                                    $("#inputPassword").val('');
                                    $("#alertInfo").removeClass('d-none');
                                    $("#alertInfo").text(response.rm);
                                break;

                                // reset password
                                case 2:
                                    $("#inputUsername").val('');
                                    $("#inputPassword").val('');
                                    $("#form-data")[0].reset();
                                    $('#form-data').bootstrapValidator("resetForm", true);
                                    $("#modal").modal('show');
                                    $("#id").val(response.id_user);
                                break;

                                // login success
                                case 3:
                                    window.location.href = '{{ route('home') }}';
                                break;
                            }
                        }

                    }).done(function (msg) {
                        $("#loading").css('display', 'none');
                    }).fail(function (msg) {
                        $("#loading").css('display', 'none');
                        // toastr.error("Terjadi Kesalahan");
                    });
                }
            }

            function resetPassword() {
                var validatePassword = $('#form-data').data('bootstrapValidator').validate();
                if (validatePassword.isValid()) {

                    var formData = document.getElementById("form-data");
                    var objData = new FormData(formData);

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('reset_password') }}',
                        data: objData,
                        dataType: 'JSON',
                        contentType: false,
                        cache: false,
                        processData: false,

                        beforeSend: function () {
                            $("#alertInfo").addClass('d-none');
                            $("#alertError").addClass('d-none');
                            $("#alertSuccess").addClass('d-none');
                        },

                        success: function (response) {
                            console.log(response);
                            $("#modal").modal('hide');
                            window.location.href = '{{ route('login') }}';
                            toastr.success(response.rm);
                        }

                    }).done(function (msg) {
                    }).fail(function (msg) {
                        toastr.error("Terjadi Kesalahan");
                    });
                }

            }

        </script>




        {{-- @if(session('resetPass'))
            <script type="text/javascript">
                $(function() {
                    $('#modal').modal('show');
                });
            </script>
        @endif --}}

    </body>


<!-- end::Body -->


</html>
