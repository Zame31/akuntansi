
	<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

            <!-- Loading -->
            <div id="loading">
                <div class="lds-facebook">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>

            <!-- begin:: Page -->
            <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
                <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                        <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                            <div class="kt-login__wrapper">
                                <div class="kt-login__container">
                                    <div class="kt-login__body">
                                        <div class="kt-login__logo mb-2">
                                            <a href="#">
                                                {{-- logo --}}
                                                <img src="{{asset('img/logo.jpg')}}" style="width: 100px;">
                                            </a>
                                        </div>
                                        <div class="kt-login__signin">
                                            <div class="kt-login__head">
                                                <h3 class="kt-login__title">ATA HD</h3>
                                            </div>
                                            <div class="kt-login__form">
                                                {{-- <form class="kt-form" method="POST" action="{{ route('login') }}"> --}}
                                                <form class="kt-form" id="loginForm">
                                                        {{ csrf_field() }}
                                                        <!-- Alert Error -->
                                                        <div class="alert alert-danger alert-dismissible d-none" id="alertError">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        </div>

                                                        <!-- Alert Info -->
                                                        <div class="alert alert-warning alert-dismissible d-none" id="alertInfo">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        </div>

                                                        <!-- Alert Success -->
                                                        <div class="alert alert-success alert-dismissible d-none" id="alertSuccess">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        </div>

                                                    <div class="form-group">
                                                        <input class="form-control" type="text" placeholder="Username" name="username" autocomplete="off" id="inputUsername">
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control form-control-last" type="password" placeholder="Password" name="password" id="inputPassword">
                                                    </div>
                                                    <div class="kt-login__extra">

                                                    </div>
                                                    <div class="kt-login__actions">
                                                        <a class="btn btn-brand btn-pill btn-elevate text-white" onclick="login();" style="cursor: pointer;">Sign In</a>
                                                    </div>

                                                    <div class="row" style="margin-top: 110px;">
                                                        <div class="col-4">
                                                            <img class="img-fluid" src="{{asset('img/hd1.png')}}" >
                                                        </div>
                                                        <div class="col-4">
                                                                <img class="img-fluid" src="{{asset('img/hd2.png')}}" >
                                                            </div>
                                                            <div class="col-4">
                                                                <img class="img-fluid" src="{{asset('img/hd3.png')}}">
                                                            </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content"
                        style="background-image: url({{asset('img/bg3.jpg')}});">
                            <div class="kt-login__section">
                                <div class="kt-login__block">
                                    <h3 class="kt-login__title">ATA HD</h3>
                                    <div class="kt-login__desc">
                                        "Simple & Inovative Solution"
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="title_modal"> Ubah Password </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        </div>

                        <div class="modal-body">
                            <form id="form-data" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="" id="id">
                                <div class="form-group">
                                    <label for="branch_name">Password Baru</label>
                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Masukan Password Baru">
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">
                            {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                            <button type="button" class="btn btn-success" onclick="resetPassword();">Simpan</button>
                        </div>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <!--end:: Vendor Plugins -->
            <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

            <!-- Action Login -->
            @include('auth.action_login');




            {{-- @if(session('resetPass'))
                <script type="text/javascript">
                    $(function() {
                        $('#modal').modal('show');
                    });
                </script>
            @endif --}}

        </body>


	<!-- end::Body -->
