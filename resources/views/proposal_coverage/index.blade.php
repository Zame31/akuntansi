@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Proposal Coverage </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        List </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        List Proposal Coverage
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        {{-- <div class="col-12">
                            <button onclick="loadNewPage('{{ route('quotation_slip.create') }}');" class="btn btn-pill btn-primary">Add New QS </button>
                        </div> --}}

                        <a style="cursor: pointer;color:white;" onclick="approve('all','/quotation_slip/approval?type=all')" class="btn btn-pill btn-success mr-2" >Send Approval All</a>
                        <a style="cursor: pointer;color:white;" onclick="approve('selected','/quotation_slip/approval?type=selected')" class="btn btn-pill btn-primary">Send Approval Selected</a>

                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="table-responsive">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                        <thead>
                            <tr>
                                <th width="30px" class="text-center">
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                        <span></span>
                                    </label>
                                </th>
                                <th width="30px" class="text-center">Action</th>
                                <th class="text-center">No</th>
                                <th class="text-center">ID</th>
                                <th class="text-center" width="100">Status</th>
                                <th class="text-center">QS No</th>
                                <th class="text-center">Type of Insurance</th>
                                <th class="text-center">Valuta</th>
                                <th class="text-center">TSI</th>
                                <th class="text-center">Gross Premium</th>
                                <th class="text-center">Premium After Fleet</th>
                                <th class="text-center">Premium Final</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data)
                                @foreach($data as $i => $item)

                                @php
                                    $data = DB::TABLE('status_qs_client')->where('qs_id', $item->id)->first();
                                @endphp

                                <tr>
                                    <td class="text-center">
                                        @if ( $data->wf_status_client_id == 1 || $data->wf_status_client_id == 7 )
                                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                <input type="checkbox" value="{{ $item->id }}" class="kt-group-checkable">
                                                <span></span>
                                            </label>
                                        @else
                                             -
                                        @endif
                                    </td>
                                    <td align="center">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">

                                                @if ( $data->wf_status_client_id == 1 || $data->wf_status_client_id == 7 )
                                                    <a class="dropdown-item" style="cursor:pointer;" onclick="loadNewPage('{{ route('proposal_coverage.show', ['id' => $item->id]) }}')">
                                                        <i class="la la-edit"></i> Edit
                                                    </a>
                                                    <a class="dropdown-item" style="cursor:pointer;" onclick="approve('one','/quotation_slip/approval?type=selected', '{{ $item->id }}')">
                                                        <i class="la la-send"></i> Send Approve
                                                    </a>
                                                @else
                                                    <a class="dropdown-item" style="cursor:pointer;" onclick="loadNewPage('{{ route('proposal_coverage.show', ['id' => $item->id]) }}')">
                                                        <i class="la la-arrows-alt"></i> Detail
                                                    </a>
                                                @endif

                                                {{-- @if ( $item->is_active )
                                                    <a class="dropdown-item" style="cursor:pointer;" onclick="setCancel('{{ $item->id }}', 't')">
                                                        <i class="la la-close"></i> Batalkan
                                                    </a>
                                                @else
                                                    <a class="cursor-pointer" style="cursor:pointer;" onclick="setCancel('{{ $item->id }}', 'f')">
                                                        <i class="la la-check"></i> Aktifkan
                                                    </a>
                                                @endif --}}

                                                {{-- <a class="dropdown-item" style="cursor:pointer;" onclick="print('{{ route('quotation_slip.export', ['id' => $item->id]) }}')">
                                                    <i class="la la-file-pdf-o"></i> Cetak Surat (PDF)
                                                </a>
                                                <a class="dropdown-item text-dark" href="insurance/printword/qs/{{$item->id}}">
                                                    <i class="la la-file-word-o"></i> Cetak Surat (Word)
                                                </a>
                                                <a class="dropdown-item text-dark" href="insurance/printexcel/qs/{{$item->id}}">
                                                    <i class="la la-file-excel-o"></i> Cetak Lampiran (Excel)
                                                </a> --}}
                                            </div>
                                        </div>
                                    </td>
                                    <td align="center"> {{ ++$i }} </td>
                                    <td align="center"> {{ $item->id }} </td>
                                    <td align="center">

                                        @if ( $data->wf_status_client_id == 1 )
                                            <span class="kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill">New</span>
                                        @elseif ( $data->wf_status_client_id == 2 )
                                            <span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill">Waiting Approval</span>
                                        @elseif ( $data->wf_status_client_id == 3 )
                                            <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Approved (QS) </span>
                                        @elseif ( $data->wf_status_client_id == 4 )
                                            <span class="kt-badge  kt-badge--dark kt-badge--inline kt-badge--pill" style="background: #1d90c7;">Proposal </span>
                                        @elseif ( $data->wf_status_client_id == 5 )
                                            <span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill">COC</span>
                                        @elseif ( $data->wf_status_client_id == 6 )
                                            <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" style="background: #9220e8;">Polis</span>
                                        @else
                                            <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Reject</span>
                                        @endif
                                    </td>
                                    <td align="center"> {{ $item->qs_no }} </td>
                                    <td> {{ $item->definition }}</td>
                                    <td> {{ $item->mata_uang }}</td>
                                    <td align="right"> {{ number_format($item->total_sum_insured,2,',','.')}} </td>
                                    <td align="right"> {{ number_format($item->gross_premium,2,',','.')}} </td>
                                    <td align="right"> {{ number_format($item->nett_premium,2,',','.')}} </td>
                                    <td align="right"> {{ number_format($item->premium_final,2,',','.')}} </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('quotation_slip.action')
<script type="text/javascript">
    $('#example-select-all').click(function (e) {
        $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
    });

    function approve(type, action, id_approve)
    {
        var setTittle = '';
        var setText = '';
        let value = [];


        if ( type == 'one' ) {
            setTittle = 'Send Approval';
            setText = 'Yakin akan mengirimkan data ini dimintai persetujuan ?';

            value = [id_approve];

        } else if ( type == 'all' ) {
            setTittle = 'Send Approval All';
            setText = 'Yakin akan mengirimkan semua data untuk dimintai persetujuan ?';

            $('td input[type="checkbox"]').each(function(){
                value.push(this.value);
            });

            if ( value.length == 0 ){
                toastr.warning("Tidak dapat approve. Data tidak tersedia");
                return false;
            }

        } else {
            // Selected
            setTittle = 'Send Approval Selected';
            setText = 'Yakin akan mengirimkan data yang dipilih untuk dimintai persetujuan ?';

            $('td input[type="checkbox"]:checked').each(function(){
                if(this.checked){
                    value.push(this.value);
                }
            });

        }


        if ( type == 'selected' ) {
            if ( value.length == 0 ) {
                toastr.warning("Minimal Pilih 1 data untuk melakukan '"+setTittle+"' ");
                return false;
            }
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        swal.fire({
            title: setTittle,
            text: setText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F8BD89",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + action,
                    data: {
                        value: value
                    },
                    dataType: 'JSON',
                    success: function (res) {
                        endLoadingPage();

                        // CEK SCHEDULE
                        if (res.rc == 0) {
                            toastr.success(setTittle+" Success");
                            location.reload();
                        }else {
                            toastr.error("Terjadi Kesalahan");
                        }
                    }
                }).done(function( res ) {
                }).fail(function(res) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });
    }

</script>
@stop
