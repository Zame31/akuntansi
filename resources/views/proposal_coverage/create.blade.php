@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Proposal Coverage </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Create </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-product">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Proposal Coverage Form
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <a onclick="loadNewPage('{{ route('proposal_coverage') }}');" class="btn btn-clean kt-margin-r-10" style="cursor: pointer;">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body pb-0" style="background: #fbfbfb;">
                <div class="form-group row mb-2">
                    <label class="col-2 col-form-label">Select Type of Insurance </label>
                    <div class="col-8">
                        <select class="form-control kt-select2 init-select2" name="product" id="product">
                            <option value="1000" disabled selected>Select Type Of Insurance</option>
                            @forelse ($products as $item)
                                <option value="{{ $item->id }}">{{ $item->definition }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label"></label>
                    <div class="col-8">
                        <button type="button" class="btn btn-md btn-outline-primary kt-margin-t-5 kt-margin-b-5" onclick="show_form()">Submit</button>
                    </div>
                </div>
              </div>
        </div>

    </div>

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-form" style="display: none;">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Proposal Coverage Form</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a onclick="show_form_product()" class="btn btn-clean kt-margin-r-10" style="cursor: pointer;">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success" onclick="store();">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body" style="background: #fbfbfb;">
                        <form class="kt-form" id="form-data">

                            <input type="hidden" name="underwriter_id" id="underwriter_id" value="">
                            <input type="hidden" name="id" id="id" value="">

                            <div class="row">
                                <div class="col-xl-2"></div>

                                <div class="col-xl-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg" id="product_name"></h3>

                                            <input type="hidden" name="menu" value="proposal_coverage">

                                            <div class="row d-none">
                                                <div class="col-4">
                                                    <label class="col-form-label">Source Of Business</label>
                                                    <div class="form-group">
                                                        <select class="form-control kt-select2 init-select2" name="source_business" id="source_business">
                                                            <option value="1000" disabled selected> Pilih Source Business </option>
                                                            @forelse ($sourceBusiness as $item)
                                                                <option value="{{ $item->id }}">{{ $item->business_def }}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                    <label class="col-form-label control-label">Form / Wording</label>
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="form_wording" id="form_wording" rows="5"></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                    <label class="col-form-label">The Business</label>
                                                    <div class="form-group">
                                                        <div>
                                                            <textarea class="form-control" name="the_business" id="the_business" rows="5"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            {{-- <div class="row">
                                                <label class="col-3 col-form-label">Period of Insurance</label>
                                                <div class="form-group col-9">
                                                    <textarea class="form-control" name="periode_of_insurance" id="periode_of_insurance" rows="3">TBA (Both dates are inclusive until 12:00 noon local time) </textarea>
                                                </div>
                                            </div> --}}

                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-none"></div>
                                    <div class="kt-section">
                                        <div class="kt-section__body">


                                            <div class="row">
                                                {{-- <div class="col-4">
                                                    <label class="col-form-label">Number of Insured</label>
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" name="number_of_insured" id="number_of_insured" value="" onkeypress="return hanyaAngka(event)" maxlength="80">
                                                    </div>
                                                </div> --}}

                                                <div class="col-3">
                                                    <label class="col-form-label">Period of Insurance (From)</label>
                                                    <div class="form-group">
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" onkeydown="event.preventDefault()" name="from" id="from" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-3">
                                                    <label class="col-form-label">Period of Insurance (To)</label>
                                                    <div class="form-group mb-2">
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" onkeydown="event.preventDefault()" name="to" id="to" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-3">
                                                    <label class="col-form-label">Policy Name</label>
                                                    <div class="form-group mb-2">
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" name="policy_name" id="policy_name" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-3">
                                                    <label class="col-form-label">Valuta Id</label>
                                                    <div class="form-group">
                                                        <select class="form-control kt-select2 init-select2" name="valuta_id" id="valuta_id">
                                                            <option value="1000" disabled selected>Select Valuta ID</option>
                                                            @forelse ($ref_valuta as $item)
                                                                <option value="{{ $item->id }}_{{ $item->mata_uang }}">{{ $item->mata_uang }}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>



                                            </div>



                                            <div class="container-detail-vehicle bg-white" style="padding: 20px;border-radius: 4px;box-shadow: inset 0px 0px 8px 0px #0000005c">
                                                <h4 class="kt-section__title kt-section__title-lg">Details
                                                    <button class="btn btn-sm btn-outline-success" onclick="modal_add_detail('insert')"> Tambah Detail </button>
                                                    <button type="button" class="btn btn-sm btn-outline-danger" onclick="check_data_product()"> Check Data  </button>

                                                    <button type="button" class="btn btn-sm btn-success" onclick="modal_import_template()"> Import Data Excel </button>
                                                    <button type="button" class="btn btn-sm btn-info" onclick="export_excel_detail()"> Export Excel Table Detail </button>
                                                    <button type="button" class="btn btn-sm btn-danger" onclick="modal_download_template()"> Download Template Excel </button>

                                                </h4>

                                                <div class="table-responsive">
                                                    <div id="tableDiv"></div>
                                                </div>

                                                {{-- <div class="row">
                                                    <div class="col-4">
                                                        <label class="col-form-label">Make / Type</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="type" id="type" value="">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Colour</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="colour" id="colour" value="">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Year Built</label>
                                                        <div class="form-group">
                                                            <div class="input-group date">
                                                                <input class="form-control year" type="text" onkeydown="event.preventDefault()" name="year_built" id="year_built" value="">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-check-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                {{-- <div class="row">
                                                    <div class="col-4">
                                                        <label class="col-form-label">Police Reg No</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="police_reg_no" id="police_reg_no" value="">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Chassis No</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="chassis_no" id="chassis_no" value="">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Engine No</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="engine_no" id="engine_no" value="">
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                {{-- <div class="row">
                                                    <div class="col-4">
                                                        <label class="col-form-label">Accessories</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="accessories" id="accessories" value="">
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>

                                                <div class="row d-none">
                                                    <div class="col-6">
                                                        <label class="col-form-label">Coverage</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="coverage_data">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="coverage_content" id="coverage_content" value="">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Coverage', 'coverage');"><i class="fa fa-pencil-alt"></i> Fill Coverage</button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <label class="col-form-label">Main Exclusions</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="main_exclusions_data">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="main_exclusions_content" id="main_exclusions_content" value="">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Main Exclusions', 'main_exclusions');"><i class="fa fa-pencil-alt"></i> Fill Main Exclusions</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row d-none">
                                                    <div class="col-6">
                                                        <label class="col-form-label">Deductible</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="deductible_data">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="deductible_content" id="deductible_content" value="">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Deductible', 'deductible');"><i class="fa fa-pencil-alt"></i> Fill Deductible </button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <label class="col-form-label">Clauses</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="clauses_data">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="clauses_content" id="clauses_content" value="">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Clauses', 'clauses');"><i class="fa fa-pencil-alt"></i> Fill Clauses </button>
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>

                                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-none"></div>

                                            <div class="row d-none" id="calculation-premium">


                                                <div class="col-6">
                                                    <h3 class="kt-section__title kt-section__title-sm text-white text-center mb-2" style="visibility:hidden">Premium Calculation</h3>

                                                    <label class="col-form-label">Valuta Id</label>
                                                    {{-- <div class="form-group">
                                                        <select class="form-control kt-select2 init-select2" name="valuta_id" id="valuta_id" onchange="getValuta(this);">
                                                            <option value="1000" disabled selected>Select Valuta ID</option>
                                                            @forelse ($ref_valuta as $item)
                                                                <option value="{{ $item->id }}_{{ $item->mata_uang }}" @if ( $item->id == 1) selected @endif>{{ $item->mata_uang }}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div> --}}

                                                    <label class="col-form-label">Total Sum Insured</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" onkeyup="calculate('total_sum_insured')" type="text" name="total_sum_insured" id="total_sum_insured" placeholder="0" value="">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6 row-change">
                                                            <label class="col-form-label">Rate</label>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <input class="form-control text-right percentage" type="text" onkeyup="calculate();" name="rate" id="rate" aria-describedby="basic-addon2" value="">
                                                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">Additional Rate</label>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <input class="form-control text-right percentage" type="text" name="additional_rate" onkeyup="calculate('additional_rate')" id="additional_rate" aria-describedby="basic-addon2" value="">
                                                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">Third Party Limit</label>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control text-right money" onkeyup="calculate('third_party_limit')" type="text" name="third_party_limit" id="third_party_limit" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">Personal Accident</label>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control text-right money" type="text" onkeyup="calculate('personal_accident')" name="personal_accident" id="personal_accident" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="col-form-label">Discount</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input class="form-control text-right percentage" type="text" onkeyup="calculate('discount')" name="discount" id="discount" aria-describedby="basic-addon2" value="">
                                                            <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-6" id="premium_calculate" style="border-radius: 4px;color: #fff;background: #01020261 !important;box-shadow: 0px 0px 5px 1px #6b6b75;">
                                                    <h3 class="kt-section__title kt-section__title-sm text-white text-center mb-2">Premium Calculation</h3>

                                                    <div class="row">
                                                        <div class="col-6 row-change">
                                                            <label class="col-form-label label-dynamic">Comprehensive</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="comprehensive" id="comprehensive" disabled placeholder="0" value="">
                                                                </div>
                                                                <span class="form-text text-white text-right">Total Sum Insured x Rate</span>
                                                                <input type="hidden" name="comprehensive_amount" id="comprehensive_amount">
                                                            </div>
                                                        </div>

                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">Flood, EQ,SRCC, TS</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="flood" id="flood" disabled placeholder="0" value="">
                                                                </div>
                                                                <span class="form-text text-white text-right">Total Sum Insured x Additional Rate</span>
                                                                <input type="hidden" name="flood_amount" id="flood_amount">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">TPL Limit</label>
                                                        <div class="form-group mb-2">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control money text-right" type="text" name="tpl_limit" id="tpl_limit" disabled placeholder="0" value="">
                                                            </div>
                                                            <span class="form-text text-white text-right">Third Party Limit x 1%</span>
                                                            <input type="hidden" name="tpl_limit_amount" id="tpl_limit_amount">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">PA Driver</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="pa_driver" id="pa_driver" disabled placeholder="0" value="">
                                                                </div>
                                                                <span class="form-text text-white text-right">Personal Accident x 0,5%</span>
                                                                <input type="hidden" name="pa_driver_amount" id="pa_driver_amount">
                                                            </div>
                                                        </div>
                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">PA Passenger</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="pa_passenger" id="pa_passenger" disabled placeholder="0" value="">
                                                                </div>
                                                                <span class="form-text text-white text-right">Personal Accident x 0,1% x 4</span>
                                                                <input type="hidden" name="pa_passenger_amount" id="pa_passenger_amount">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">Sub Total</label>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control money text-right" type="text" name="sub_total" id="sub_total" disabled placeholder="0" value="">
                                                                <input type="hidden" name="sub_total_value" id="sub_total_value">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="col-form-label">Discount Amount</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="discount_amount" id="discount_amount" disabled placeholder="0" value="">
                                                            <input type="hidden" name="discount_amount_value" id="discount_amount_value">
                                                        </div>
                                                    </div>

                                                    <label class="col-form-label">Grand Total</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="grand_total" id="grand_total" disabled placeholder="0" value="">
                                                            <input type="hidden" name="grand_total_value" id="grand_total_value">
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                                    <div class="kt-section kt-section--last">
                                        <div class="kt-section__body">
                                            {{-- <h3 class="kt-section__title kt-section__title-lg">Others:</h3> --}}

                                            <div class="row">

                                                <div class="col-4 d-none">
                                                    <label class="col-form-label">Gross Premium</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="gross_premium" id="gross_premium" disabled placeholder="0" value="">
                                                            <input type="hidden" name="gross_premium_value" id="gross_premium_value">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-4 d-none">
                                                    <label class="col-form-label">Nett Premium</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="nett_premium" id="nett_premium" disabled placeholder="0" value="">
                                                            <input type="hidden" name="nett_premium_value" id="nett_premium_value">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->


    <!-- end:: Subheader -->

</div>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>

@include('proposal_coverage.action')
@include('quotation_slip.modal.detail')
@include('quotation_slip.modal.clauses')
@include('quotation_slip.modal.coverage')
@include('quotation_slip.modal.deductible')
@include('quotation_slip.modal.main_exclusions')
@include('quotation_slip.modal.template_excel')
@include('quotation_slip.modal.import_excel')

<script type="text/javascript">
    // var DataTable = $('#table_detail').DataTable();
    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");
    var no = 1;

    if ( !id ) {
        show_form_product();
    } else {
        // $("#container-product").fadeOut(500);
        // $("#container-form").delay(1000).slideDown(1000);
        // $("#product_name").html($("#product option[value='"+id+"'").text());
        // $("#underwriter_id").val(id);

        $("#product").val(id);

        show_form();
    }
</script>
@stop
