@section('content')
<div class="app-content">
<div class="section">


    @php
        $readOnly = "";
        $text = "";

        if ( $status->wf_status_client_id != 1 && $status->wf_status_client_id != 7 ) {
            $readOnly = 'readonly';
            $text = 'Detail';
        } else {
            $text = 'Edit';
        }

    @endphp
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Proposal Coverage </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ $text }} </a>
                    </div>
            </div>
        </div>
    </div>

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-form">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Proposal Coverage Form</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a onclick="loadNewPage('{{ route('proposal_coverage') }}')" class="btn btn-clean kt-margin-r-10" style="cursor: pointer;">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                            @if ( $status->wf_status_client_id == 1 || $status->wf_status_client_id == 7)
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success" onclick="store();">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Save</span>
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="kt-portlet__body" style="background: #fbfbfb;">
                        <form class="kt-form" id="form-data">

                            @if ( $status->wf_status_client_id == 7 )
                                <div class="alert alert-solid-warning alert-bold" role="alert">
                                    <div class="alert-text">Catatan Reject: {{ $status->notes }}</div>
                                </div>
                            @endif

                            <input type="hidden" name="menu" value="proposal_coverage">

                            <input type="hidden" name="underwriter_id" id="underwriter_id" value="{{ $data->product_id }}">
                            <input type="hidden" name="id" id="id" value="{{ $data->id }}">

                            <div class="row">
                                <div class="col-xl-2"></div>

                                <div class="col-xl-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg" id="product_name">{{ $data->product }}</h3>


                                            <div class="row d-none">
                                                <div class="col-4">
                                                    <label class="col-form-label">Source Of Business</label>
                                                    <div class="form-group">
                                                        <select class="form-control kt-select2 init-select2" name="source_business" id="source_business" {{ $readOnly }}>
                                                            <option value="1000" disabled selected>Select Source Of Business</option>
                                                            @forelse ($sourceBusiness as $item)
                                                                <option value="{{ $item->id }}" @if ( $data->buss_source_id == $item->id) selected @endif>{{ $item->business_def }}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                    <label class="col-form-label control-label">Form / Wording</label>
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="form_wording" id="form_wording" rows="5" {{ $readOnly }}>{{ $data->form_wording }} </textarea>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                    <label class="col-form-label">The Business</label>
                                                    <div class="form-group">
                                                        <div>
                                                            <textarea class="form-control" name="the_business" id="the_business" rows="5" {{ $readOnly }}>{{ $data->the_business }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-none"></div>
                                    <div class="kt-section">
                                        <div class="kt-section__body">


                                            <div class="row">
                                                {{-- <div class="col-4">
                                                    <label class="col-form-label">Number of Insured</label>
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" name="number_of_insured" id="number_of_insured" value="{{ $data->no_of_insured }}" onkeypress="return hanyaAngka(event)" maxlength="80">
                                                    </div>
                                                </div> --}}

                                                <div class="col-3">
                                                    <label class="col-form-label">Period of Insurance (From)</label>
                                                    <div class="form-group">
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" onkeydown="event.preventDefault()" name="from" id="from" value="@if (!is_null($data->from_date)) {{ date('d F Y', strtotime($data->from_date)) }} @endif" {{ $readOnly }}/>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-3">
                                                    <label class="col-form-label">Period of Insurance (To)</label>
                                                    <div class="form-group mb-2">
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" onkeydown="event.preventDefault()" name="to" id="to" value="@if ( !is_null($data->to_date)) {{ date('d F Y', strtotime($data->to_date)) }} @endif" {{ $readOnly }}/>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-3">
                                                    <label class="col-form-label">Policy Name</label>
                                                    <div class="form-group mb-2">
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" name="policy_name" id="policy_name" value="{{ $data->policy_name }}" {{ $readOnly }}/>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-3">
                                                    <label class="col-form-label">Valuta Id</label>
                                                    <div class="form-group">
                                                        <select class="form-control kt-select2 init-select2" name="valuta_id" id="valuta_id">
                                                            <option value="1000" disabled selected>Select Valuta ID</option>
                                                            @forelse ($ref_valuta as $item)
                                                                <option value="{{ $item->id }}_{{ $item->mata_uang }}" @if( $data->valuta_id == $item->id ) selected @endif>{{ $item->mata_uang }}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>

                                                <input type="hidden" name="qs_no_value" id="qs_no_value" value="{{ $data->qs_no }}">


                                            </div>

                                            <div class="container-detail-vehicle bg-white" style="padding: 20px;border-radius: 4px;box-shadow: inset 0px 0px 8px 0px #0000005c">
                                                <h4 class="kt-section__title kt-section__title-lg">
                                                    <span class="mr-4">Details</span>
                                                    <button type="button" class="btn btn-sm btn-outline-success" onclick="modal_add_detail('insert_edit', '{{ $data->product_id }}')"> Tambah Detail </button>
                                                    <button type="button" class="btn btn-sm btn-success" onclick="modal_import_template()"> Import Data Excel </button>
                                                    <button type="button" class="btn btn-sm btn-info" onclick="export_excel_detail()"> Export Excel Table Detail </button>
                                                    <button type="button" class="btn btn-sm btn-danger" onclick="modal_download_template()"> Download Template Excel </button>
                                                </h4>

                                                <div class="table-responsive">
                                                    <div id="tableDiv">
                                                        <table class="table table-striped- table-hover table-checkable" id="table_detail">
                                                            <thead>
                                                                <tr>
                                                                    <th width="30px" class="text-center all">Action</th>
                                                                    <th class="text-center all">Status</th>
                                                                    @foreach ($headerTable as $header)
                                                                        <th class="text-center @if ($header->is_showing) all @else none @endif ">{{ $header->label }}</th>
                                                                    @endforeach
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                    $idx;
                                                                @endphp
                                                                @foreach ($detail as $i => $item)
                                                                    @php
                                                                        $idx = $i + 1;
                                                                    @endphp
                                                                <tr>
                                                                    <td>
                                                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <i class="flaticon-more"></i>
                                                                        </button>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <a class="dropdown-item" onclick="modal_add_detail('edit', '{{ $idx }}')">
                                                                                <i class="la la-edit"></i> Edit
                                                                            </a>
                                                                            <a class="dropdown-item" onclick="hapus_detail('{{ $idx }}', this)">
                                                                                <i class="la la-trash"></i> Hapus
                                                                            </a>
                                                                            @foreach ( $item as $value)
                                                                                @if ( $value->bit_id == 12 )
                                                                                    @if ( $value->value_text == 't')
                                                                                        <a class="dropdown-item" onclick="set_active('{{ $idx }}', this, 'f')">
                                                                                            <i class="la la-close"></i> Non Aktif
                                                                                        </a>
                                                                                    @else
                                                                                        <a class="dropdown-item" onclick="set_active('{{ $idx }}', this, 't')">
                                                                                            <i class="la la-check"></i> Aktif
                                                                                        </a>
                                                                                    @endif
                                                                                @endif
                                                                            @endforeach
                                                                        </div>
                                                                    </td>
                                                                    @foreach ( $item as $value )
                                                                        @if ( $value->bit_id == 12 )
                                                                            @if ( $value->value_text == 't' )
                                                                            <td> <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" style="font-size: 10px;">Aktif</span> </td>
                                                                            @else
                                                                            <td> <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="font-size: 10px;">Non Aktif</span> </td>
                                                                            @endif
                                                                        @else
                                                                            <td> {{ $value->value_text }}</td>
                                                                        @endif
                                                                    @endforeach
                                                                </tr>
                                                                @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                {{-- <div class="row">
                                                    <div class="col-4">
                                                        <label class="col-form-label">Make / Type</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="type" id="type" value="{{ $data->make_type }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Colour</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="colour" id="colour" value="{{ $data->colour }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Year Built</label>
                                                        <div class="form-group">
                                                            <div class="input-group date">
                                                                <input class="form-control year" type="text" onkeydown="event.preventDefault()" name="year_built" id="year_built" value="{{ $data->year_built }}">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-check-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                {{-- <div class="row">
                                                    <div class="col-4">
                                                        <label class="col-form-label">Police Reg No</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="police_reg_no" id="police_reg_no" value="{{ $data->police_reg_no }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Chassis No</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="chassis_no" id="chassis_no" value="{{ $data->chassis_no }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="col-form-label">Engine No</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="engine_no" id="engine_no" value="{{ $data->engine_no }}">
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                {{-- <div class="row">
                                                    <div class="col-4">
                                                        <label class="col-form-label">Accessories</label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="accessories" id="accessories" value="{{ $data->accessories }}">
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>

                                                <div class="row d-none">
                                                    <div class="col-6">
                                                        <label class="col-form-label">Coverage</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="coverage_data">
                                                                    {!! $data->coverage !!}
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="coverage_content" id="coverage_content" value="{{ $data->coverage }}">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Coverage', 'coverage');"><i class="fa fa-pencil-alt"></i> Fill Coverage</button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <label class="col-form-label">Main Exclusions</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="main_exclusions_data">
                                                                    {!! $data->main_exclusions !!}
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="main_exclusions_content" id="main_exclusions_content" value="{{ $data->main_exclusions }}">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Main Exclusions', 'main_exclusions');"><i class="fa fa-pencil-alt"></i> Fill Main Exclusions</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row d-none">
                                                    <div class="col-6">
                                                        <label class="col-form-label">Deductible</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="deductible_data">
                                                                    {!! $data->deductible !!}
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="deductible_content" id="deductible_content" value="{{ $data->deductible }}">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Deductible', 'deductible');"><i class="fa fa-pencil-alt"></i> Fill Deductible </button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <label class="col-form-label">Clauses</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="clauses_data">
                                                                    {!! $data->clauses !!}
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="clauses_content" id="clauses_content" value="{{ $data->clauses }}">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Clauses', 'clauses');"><i class="fa fa-pencil-alt"></i> Fill Clauses </button>
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>

                                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>

                                            <div class="row d-none" id="calculation-premium">


                                                <div class="col-6">
                                                    <h3 class="kt-section__title kt-section__title-sm text-white text-center mb-2" style="visibility:hidden">Premium Calculation</h3>

                                                    {{-- <label class="col-form-label">Valuta Id</label>
                                                    <div class="form-group">
                                                        <select class="form-control kt-select2 init-select2" name="valuta_id" id="valuta_id" onchange="getValuta(this);">
                                                            <option value="1000" disabled selected>Select Valuta ID</option>
                                                            @forelse ($ref_valuta as $item)
                                                                <option value="{{ $item->id }}_{{ $item->mata_uang }}" @if ( $data->valuta_id == $item->id) selected @endif>{{ $item->mata_uang }}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div> --}}

                                                    <label class="col-form-label">Total Sum Insured</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" onkeyup="calculate('total_sum_insured')" type="text" name="total_sum_insured" id="total_sum_insured" placeholder="0" value="{{ number_format($data->total_sum_insured, 2, '.', ',') }}">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6 row-change">
                                                            <label class="col-form-label">Rate</label>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <input class="form-control text-right percentage" type="text" onkeyup="calculate();" name="rate" id="rate" aria-describedby="basic-addon2" value="{{ $data->rate }}">
                                                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">Additional Rate</label>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <input class="form-control text-right percentage" type="text" name="additional_rate" onkeyup="calculate('additional_rate')" id="additional_rate" aria-describedby="basic-addon2" value="{{ $data->add_rate }}">
                                                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">Third Party Limit</label>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control text-right money" onkeyup="calculate('third_party_limit')" type="text" name="third_party_limit" id="third_party_limit" value="{{ number_format($data->third_party_limit, 2, '.', ',') }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">Personal Accident</label>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control text-right money" type="text" onkeyup="calculate('personal_accident')" name="personal_accident" id="personal_accident" value="{{ number_format($data->personal_accident, 2, '.', ',') }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="col-form-label">Discount</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input class="form-control text-right percentage" type="text" onkeyup="calculate('discount')" name="discount" id="discount" aria-describedby="basic-addon2" value="{{ $data->discount }}">
                                                            <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-6" id="premium_calculate" style="border-radius: 4px;color: #fff;background: #01020261 !important;box-shadow: 0px 0px 5px 1px #6b6b75;">
                                                    <h3 class="kt-section__title kt-section__title-sm text-white text-center mb-2">Premium Calculation</h3>

                                                    <div class="row">
                                                        <div class="col-6 row-change">
                                                            <label class="col-form-label label-dynamic">Comprehensive</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="comprehensive" id="comprehensive" disabled placeholder="0" value="{{ number_format($data->comprehensive_amount, 2, '.', ',') }}">
                                                                </div>
                                                                <span class="form-text text-white text-right">Total Sum Insured x Rate</span>
                                                                <input type="hidden" name="comprehensive_amount" id="comprehensive_amount" value="{{ $data->comprehensive_amount }}">
                                                            </div>
                                                        </div>

                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">Flood, EQ,SRCC, TS</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="flood" id="flood" disabled placeholder="0" value="{{ number_format($data->flood_amount, 2, '.', ',') }}">
                                                                </div>
                                                                <span class="form-text text-white text-right">Total Sum Insured x Additional Rate</span>
                                                                <input type="hidden" name="flood_amount" id="flood_amount" value="{{ $data->flood_amount }}">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">TPL Limit</label>
                                                        <div class="form-group mb-2">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control money text-right" type="text" name="tpl_limit" id="tpl_limit" disabled placeholder="0" value="{{ number_format($data->tpl_limit, 2, '.', ',') }}">
                                                            </div>
                                                            <span class="form-text text-white text-right">Third Party Limit x 1%</span>
                                                            <input type="hidden" name="tpl_limit_amount" id="tpl_limit_amount" value="{{ $data->tpl_limit }}">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">PA Driver</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="pa_driver" id="pa_driver" disabled placeholder="0" value="{{ number_format($data->pa_driver, 2, '.', ',') }}">
                                                                </div>
                                                                <span class="form-text text-white text-right">Personal Accident x 0,5%</span>
                                                                <input type="hidden" name="pa_driver_amount" id="pa_driver_amount" value="{{ $data->pa_driver }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-6 prod-vehicle">
                                                            <label class="col-form-label">PA Passenger</label>
                                                            <div class="form-group mb-2">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                    <input class="form-control money text-right" type="text" name="pa_passenger" id="pa_passenger" disabled placeholder="0" value="{{ number_format($data->pa_passenger, 2, '.', ',') }}">
                                                                </div>
                                                                <span class="form-text text-white text-right">Personal Accident x 0,1% x 4</span>
                                                                <input type="hidden" name="pa_passenger_amount" id="pa_passenger_amount" value="{{ $data->pa_passenger }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @php
                                                        $sub_total = $data->comprehensive_amount + $data->flood_amount + $data->tpl_limit + $data->pa_driver + $data->pa_passenger;
                                                        $grand_total = $sub_total - $data->discount_amount;
                                                    @endphp

                                                    <div class="prod-vehicle">
                                                        <label class="col-form-label">Sub Total</label>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                                <input class="form-control money text-right" type="text" name="sub_total" id="sub_total" disabled placeholder="0" value="{{ number_format($sub_total, 2, '.', ',') }}">
                                                                <input type="hidden" name="sub_total_value" id="sub_total_value" value="{{ $sub_total }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="col-form-label">Discount Amount</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="discount_amount" id="discount_amount" disabled placeholder="0" value="{{ number_format($data->discount_amount, 2, '.', ',') }}">
                                                            <input type="hidden" name="discount_amount_value" id="discount_amount_value" value="{{ $data->discount_amount }}">
                                                        </div>
                                                    </div>

                                                    <label class="col-form-label">Grand Total</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="grand_total" id="grand_total" disabled placeholder="0" value="{{ number_format($grand_total, 2, '.', ',') }}">
                                                            <input type="hidden" name="grand_total_value" id="grand_total_value" value="{{ $grand_total }}">
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                                    <div class="kt-section kt-section--last">
                                        <div class="kt-section__body">
                                            {{-- <h3 class="kt-section__title kt-section__title-lg">Others:</h3> --}}

                                            <div class="row">


                                                <div class="col-4 d-none">
                                                    <label class="col-form-label">Gross Premium</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="gross_premium" id="gross_premium" disabled placeholder="0" value="{{ number_format($sub_total, 2, ',', '.') }}">
                                                            <input type="hidden" name="gross_premium_value" id="gross_premium_value" value="{{ $sub_total }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-4 d-none">
                                                    <label class="col-form-label">Nett Premium</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group mata-uang">IDR</span></div>
                                                            <input class="form-control money text-right" type="text" name="nett_premium" id="nett_premium" disabled placeholder="0" value="{{ number_format($grand_total, 2, ',', '.') }}">
                                                            <input type="hidden" name="nett_premium_value" id="nett_premium_value" value="{{ $grand_total }}">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->


    <!-- end:: Subheader -->

</div>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>

@include('proposal_coverage.action')
@include('quotation_slip.modal.detail')
@include('quotation_slip.modal.clauses')
@include('quotation_slip.modal.coverage')
@include('quotation_slip.modal.deductible')
@include('quotation_slip.modal.main_exclusions')
@include('quotation_slip.modal.template_excel')
@include('quotation_slip.modal.import_excel')

<script type="text/javascript">
    var DataTable = $("#table_detail").DataTable({
        responsive: true
    });
    var p_id = '{{ $data->product_id }}';
    appendCalculation(p_id);
    getValuta($("#valuta_id"));
    var dataDetail = {!!json_encode($detail)!!};
    console.log('data detail');
    console.log(dataDetail);
    for ( var i = 0; i < dataDetail.length; i++ ) {
        id_array_data = i+1;
        console.log('id array data : ' + id_array_data);
        temp_data = [];
        dataDetail[i].forEach(function(data, index) {
            console.log(data);
            var item = {
                    "name" : data.input_name,
                    "value": data.value_text,
                    "bit_id" : data.bit_id,
                };
            temp_data.push(item);
        });

        console.log('temp data');
        console.log(temp_data);

        data.push({
            id : id_array_data,
            value : temp_data
        });
    }

    console.log('array data detail');
    console.log(data);


</script>
@stop
