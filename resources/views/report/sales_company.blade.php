@section('content')
<style>
    span.select2-container {
        width: 200px !important;
    }
</style>

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Sales By Company Non Company </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <div class="btn-group dropleft">
                        <select class="form-control kt-select2 init-select2" name="idbranch" id="idbranch">
                            

                            @if ( count($branch) > 1)
                                <option value="konsol">Konsolidasi</option>
                            @endif
                            
                            @foreach($branch as $item)
                            <option value="{{$item->id}}" @if($item->id==$idbranch) selected @endif>{{ $item->short_code }} - {{$item->branch_name}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Sales By Company Non Company
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="form-group row pt-4">
                    <div class="col-10">
                        <div class="input-daterange input-group init-date" style="width: 400px;float: right;">
                            <input type="text" placeholder="Start Date" readonly value="{{$start_date}}" class="form-control" name="start" id="start_date" />
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-angle-double-right"></i></span>
                            </div>
                            <input type="text" placeholder="End Date" value="{{$end_date}}" readonly class="form-control" name="end" id="end_date" />
                            <div class="invalid-feedback">Pilih Periode</div>
                        </div>
                    </div>
                    <div class="col-2">
                        <button onclick="return periode();" class="btn btn-info btn-elevate">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped- table-hover">
                <thead>
                    <tr>
                        <th class="text-center">
                            <h6>No</h6>
                        </th>
                        <th class="text-center">
                            <h6>Branch Code</h6>
                        </th>
                        <th class="text-center">
                            <h6>Customer Type</h6>
                        </th>

                        <th class="text-right">
                            <h6>Premium</h6>
                        </th>
                        <th class="text-right">
                            <h6>Nett Premium</h6>
                        </th>
                        <th class="text-right">
                            <h6>Agent Fee</h6>
                        </th>
                        <th class="text-right">
                            <h6>Comm Due To Us</h6>
                        </th>
                        <th class="text-right">
                            <h6>Nett To Underwriter</h6>
                        </th>

                        {{-- <th class="text-right">
                            <h6>Gross Premi Amount</h6>
                        </th>
                        <th class="text-right">
                            <h6>Agent Fee</h6>
                        </th>
                        <th class="text-right">
                            <h6>Company Fee</h6>
                        </th>
                        <th class="text-right">
                            <h6>Nett Premi Amount</h6>
                        </th> --}}
                    </tr>
                </thead>
                <tbody>
                    @if ($data)
                        @php 
                            $no=0;
                        @endphp

                        @foreach($data as $item)

                            @php
                                $no++;
                            @endphp

                            <tr>
                                <td align="center"> {{ $no }} </td>
                                <td align="center"> {{ $short_code }} </td>
                                <td>
                                    <button onclick="sales_detail({{$item->id}});" type="button" class="btn btn-info btn-elevate btn-pill btn-sm">
                                        <i class="flaticon-layer"></i> {{ $item->definition }} </button>
                                </td>
                                {{-- <td class="text-right"> {{ number_format($item->premi_amount,2,',','.') }}</td>
                                <td class="text-right"> {{ number_format($item->agent_fee_amount,2,',','.') }}</td>
                                <td class="text-right"> {{ number_format($item->comp_fee_amount,2,',','.') }}</td>
                                <td class="text-right"> {{ number_format($item->net_amount,2,',','.') }}</td> --}}

                                <td class="text-right">{{number_format($item->premi_amount,2,',','.')}}</td>
                                <td class="text-right">{{number_format($item->net_amount,2,',','.')}}</td>
                                <td class="text-right">{{number_format($item->agent_fee_amount,2,',','.')}}</td>
                                <td class="text-right">{{number_format($item->comp_fee_amount,2,',','.')}}</td>
                                <td class="text-right">{{number_format($item->nett_to_underwriter,2,',','.')}}</td>
                            </tr>

                        @endforeach

                    @endif
                </tbody>
                <tfoot style="background: #f7f7f7;">
                    @php
                        // $tot_premi=0;
                        // $tot_agent=0;
                        // $tot_comp=0;
                        // $tot_net=0;

                        $total_net_premi_amount = 0;
                        $total_premi_amount = 0;
                        $total_agent_fee = 0;
                        $total_comm_due_to_us = 0;
                        $total_nett_to_underwriter = 0;
                    @endphp

                    @if($data)
                        @foreach($data as $item)

                            @php
                                // $tot_premi += $item->premi_amount;
                                // $tot_agent += $item->agent_fee_amount;
                                // $tot_comp += $item->comp_fee_amount;
                                // $tot_net += $item->net_amount;

                                $total_net_premi_amount += $item->net_amount;
                                $total_premi_amount += $item->premi_amount;
                                $total_agent_fee += $item->agent_fee_amount;
                                $total_comm_due_to_us += $item->comp_fee_amount;
                                $total_nett_to_underwriter += $item->nett_to_underwriter;
                            @endphp

                        @endforeach
                    @endif
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <h6>Total</h6>
                        </td>

                        <td class="text-right text-success font-weight-bold">{{number_format($total_premi_amount,2,',','.')}}</td>
                        <td class="text-right text-success font-weight-bold">{{number_format($total_net_premi_amount,2,',','.')}}</td>
                        <td class="text-right text-success font-weight-bold">{{number_format($total_agent_fee,2,',','.')}}</td>
                        <td class="text-right text-success font-weight-bold">{{number_format($total_comm_due_to_us,2,',','.')}}</td>
                        <td class="text-right text-success font-weight-bold">{{number_format($total_nett_to_underwriter,2,',','.')}}</td>

                        {{-- <td class="text-right text-info font-weight-bold">{{ number_format($tot_premi,2,',','.') }}</td>
                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_agent,2,',','.') }}</td>
                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_comp,2,',','.') }}</td>
                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_net,2,',','.') }}</td> --}}
                    </tr>
                </tfoot>
            </table>



        </div>
    </div>


</div>
<script>
    $(document).ready(function(){
        $('#start_date').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
        }).on('changeDate', function(){
            // set the "fromDate" end to not be later than "toDate" starts:
            $('#end_date').datepicker('setStartDate', new Date($(this).val()));
        });

        $('#end_date').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
        }).on('changeDate', function(){
            // set the "fromDate" end to not be later than "toDate" starts:
            $('#start_date').datepicker('setEndDate', new Date($(this).val()));
        });
    });

    function detail_(id) {
        loadingPage();
        window.location.href = base_url + 'transaksi_detail?id=' + id;
        endLoadingPage();
    }

     function sales_detail(id) {
        loadingPage();
        window.location.href = base_url + 'report/sales_detail?type=company&id=' + id  + '&start_date=' + $("#start_date").val() + '&end_date=' + $("#end_date").val();
        // window.location.href = base_url + 'report/sales_detail?type=company&id=' + id;
        endLoadingPage();
    }

    function periode(){
        if($('#start_date').val() === ''){
            endLoadingPage();
            $( "#start_date" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#start_date").removeClass( "is-invalid" );
        }

        if($('#end_date').val()===''){
            endLoadingPage();
            $( "#end_date" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#end_date").removeClass( "is-invalid" );

        }

        loadingPage();
        window.location.href = base_url + 'report/sales_company?search=search&start_date=' + $('#start_date').val() +'&end_date='+$('#end_date').val() + '&id='+$('#idbranch').val();
        endLoadingPage();
    }


    $('#idbranch').on('change', function (v) {
        if($('#start_date').val() === ''){
            endLoadingPage();
            $( "#start_date" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#start_date").removeClass( "is-invalid" );
        }

        if($('#end_date').val()===''){
            endLoadingPage();
            $( "#end_date" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#end_date").removeClass( "is-invalid" );

        }

        $.ajax({
            type: 'GET',
            url: base_url + '/report/sales_officer?id='+this.value,
            beforeSend: function () {
                $("#loading").css('display', 'block');
            },
            success: function (res) {
                //var data = $.parseJSON(res);
                $("#loading").css('display', 'none');

                // window.location.href = base_url + 'report/sales_company?id='+$('#idbranch').val();
                window.location.href = base_url + 'report/sales_company?search=search&start_date=' + $('#start_date').val() +'&end_date='+$('#end_date').val() + '&id='+$('#idbranch').val();

            }
        });

    });


</script>
@stop
