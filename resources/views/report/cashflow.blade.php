@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Cashflow </a>
            </div>
        </div>
        
        <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <div class="btn-group dropleft">
                        @if ( Auth::user()->user_role_id == 2 ) 
                            <!-- SPV -->
                            <select class="form-control kt-select2 init-select2" name="idbranch" id="idbranch">
                                @foreach($branch as $item)
                                    @if($item->id == Auth::user()->branch_id)
                                        <option value="{{$item->id}}"  selected >{{$item->branch_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        @else
                            <!-- ADMIN PUSAT -->
                            <select class="form-control kt-select2 init-select2" name="idbranch" id="idbranch">
                                <option value="konsol">Konsolidasi</option>
                                @foreach($branch as $item)
                                    <option value="{{$item->id}}" @if($item->id==$idbranch) selected @endif>{{$item->branch_name}}</option>
                                @endforeach
                            </select>
                        @endif
                        
                    </div>

                </div>
            </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Cashflow
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <form class="form-horizontal" id="form-filter" method="GET">
                <div class="form-group row pt-4">
                    {{-- <label class="col-form-label col-12">Pick Start Date and End Date</label> --}}
                    <div class="col-10">
                        <div class="input-daterange input-group init-date" style="width: 400px;float: right;">
                            <input type="text" placeholder="Start Date" @if($start) value="{{$start}}" @endif class="form-control" name="start" />
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-angle-double-right"></i></span>
                            </div>
                            <input type="text" placeholder="End Date" @if($end) value="{{$end}}" @endif class="form-control" name="end" />
                        </div>
                    </div>
                    <div class="col-2">
                        <button type="submit" id="cari" name="cari" value="cari" class="btn btn-success btn-elevate">Submit</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="kt-portlet__body">
            @if($cash)
                <div class="accordion accordion-light" id="accordionExample2">
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Saldo Fisik
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Kas
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100001
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Giro
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100002
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100003
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100004
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100005
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Tabungan
                                    </div>
                                </div>
                                <div class="col-4">
                                
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Penempatan
                                    </div>
                                </div>
                                <div class="col-4">
                                
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Pendapatan
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Komisi
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            400001
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Investasi
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            400002
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Lainnya
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            400003
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Pengeluaran
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        OPEX
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            500002
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            500003
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            500004
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Beban Pemasaran
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Fee Agent
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            50000103
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Promosi
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            50000102
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Fee Entertaintment
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            50000101
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Kewajiban
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Kewajiban Premi IDR
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            200006
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Kewajiban Premi USD
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            200007
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Hutang
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Hutang Bank
                                    </div>
                                </div>
                                <div class="col-4">
                                
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Hutang Lainnya
                                    </div>
                                </div>
                                <div class="col-4">
                                
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                    <b>Saldo Akhir</b> 
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5><b>Rp 0</b></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            @else
            <div class="accordion accordion-light" id="accordionExample2">
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Saldo Fisik
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Kas
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100001
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Giro
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100002
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100003
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100004
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            100005
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Tabungan
                                    </div>
                                </div>
                                <div class="col-4">
                                   
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Penempatan
                                    </div>
                                </div>
                                <div class="col-4">
                                   
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Pendapatan
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Komisi
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            400001
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Investasi
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            400002
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Lainnya
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            400003
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Pengeluaran
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        OPEX
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            500002
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            500003
                                        </span>
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            500004
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Beban Pemasaran
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Fee Agent
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            50000103
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Promosi
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            50000102
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Fee Entertaintment
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            50000101
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Kewajiban
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Kewajiban Premi IDR
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            200006
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Kewajiban Premi USD
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            200007
                                        </span>
                                    </div>
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Hutang
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Hutang Bank
                                    </div>
                                </div>
                                <div class="col-4">
                                  
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="card-title pl-3">
                                        Hutang Lainnya
                                    </div>
                                </div>
                                <div class="col-4">
                                   
                                </div>
                                <div class="col-4 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                       <b>Saldo Akhir</b> 
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5><b>Rp 0</b></h5>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>            
            @endif
        </div>
    </div>


</div>
<script>
function detail_(id){
	loadingPage();
	window.location.href = base_url +'transaksi_detail?id=' + id;
	endLoadingPage();
}
 $('#idbranch').on('change', function (v) {
        $.ajax({
            type: 'GET',
            url: base_url + '/report/cashflow?id='+this.value,
            beforeSend: function () {
                $("#loading").css('display', 'block');
            },
            success: function (res) {
                //var data = $.parseJSON(res);
                $("#loading").css('display', 'none');
                window.location.href = base_url + 'report/cashflow?id='+$('#idbranch').val();
            }
        });
    });
</script>
@stop
