@section('content')
<div class="app-content">
<div class="section">


    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Report </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                            Laporan Nominatif QS </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Laporan Nominatif QS
                        @if ($type == 'aktif')
                            Status Polis Aktif
                        @else
                            Status Non Polis Aktif
                        @endif 
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-md-7">
                            <select onchange="setType(this.value)" class="form-control selectpicker" title="Filter" data-style="btn-danger" name="pilih_type" id="pilih_type">
                                <option @if ($type == 'aktif') selected @endif value="aktif">Status Polis Aktif</option>
                                <option @if ($type == 'nonaktif') selected @endif value="nonaktif"> Status Polis Non Aktif</option>
                            </select>
                        </div>
                        <div class="col-md-5">
                           
                            {{-- <button onclick="exportFile('xls');" type="button" class="btn btn-outline-info">
                                    <i class="flaticon2-file"></i> Export Excel </button> --}}
                            <button onclick="exportFile('pdf');" type="button" class="btn btn-outline-info">
                                    <i class="flaticon2-file"></i> Export PDF </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable">
                            <thead>
                                <tr>
                                    <th class="text-left">No</th>
                                    <th class="text-left">Client Name</th>
                                    <th class="text-left">QS No</th>
                                    <th class="text-left">Policy No</th>
                                    <th class="text-left">Policy Start</th>
                                    <th class="text-left">Policy End</th>
                                    <th class="text-right">TSI (Total Sum Insured)</th>
                                    <th class="text-right">Nett Premium</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total_ins_amount = 0;
                                    $total_net_amount = 0;  
                                @endphp
                                @foreach ($data as $key => $v)
                                @php
                                    $total_ins_amount += $v->ins_amount;
                                    $total_net_amount += $v->net_amount;  
                                @endphp
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$v->full_name}}</td>
                                        <td>{{$v->qs_no}}</td>
                                        <td>{{$v->polis_no}}</td>
                                        <td>{{date('d-m-Y',strtotime($v->start_date_polis))}}</td>
                                        <td>{{date('d-m-Y',strtotime($v->end_date_polis))}}</td>
                                        <td class="text-right">{{number_format($v->ins_amount,2,",",".")}}</td>
                                        <td class="text-right">{{number_format($v->net_amount,2,",",".")}}</td>
                                    </tr>
                                @endforeach
                             
                            </tbody>
                            <tfoot style="background: #f7f7f7;">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">{{number_format($total_ins_amount,2,",",".")}}</td>
                                    <td class="text-right">{{number_format($total_net_amount,2,",",".")}}</td>
                                </tr>
                            </tfoot>
                           
                           
                    </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>

<script type="text/javascript">

    function setType(type) {
        let url = "{{ route('qs_report',[':type']) }}";
        url = url.replace(':type',type);
        
        window.location.href = url;
    }

    function exportFile(typeFile){
        let jenis = $('#pilih_type').val();

        if ( typeFile == 'xls' ){
            window.open(base_url + 'report/qs_report_export?file=xls&jenis='+jenis, "_blank");
        } else {
            window.open(base_url + 'report/qs_report_export?file=pdf&jenis='+jenis, "_blank");
        }
    }
</script>
@stop
