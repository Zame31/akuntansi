@section('content')
<div class="app-content">
<div class="section">


    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Report </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                            Due Date Installment </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Due Date Installment
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <button onclick="exportFile('xls');" type="button" class="btn btn-outline-info">
                                    <i class="flaticon2-file"></i> Export Excel </button>
                            <button onclick="exportFile('pdf');" type="button" class="btn btn-outline-info">
                                    <i class="flaticon2-file"></i> Export PDF </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Branch Code</th>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Customer Name</th>
                                    <th class="text-center">Product Name</th>
                                    <th class="text-center">Premi Amount</th>
                                    <th class="text-center">Paid Status</th>
                                    <th class="text-center">Nomor Polis</th>
                                    <th class="text-center"> Due Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total_premi_amount = 0; $no = 0;
                                @endphp

                                @forelse ($data as $item)
                                    @php $no++ @endphp
                                    <tr>
                                        <td align="center"> {{ $no }} </td>
                                        <td align="center"> {{ $short_code }} </td>
                                        <td align="center"> {{ $item->id }} </td>
                                        <td> {{ $item->full_name }} </td>
                                        <td> {{ $item->definition }} </td>
                                        <td align="right"> {{ number_format($item->premi_amount, 2, ',', '.') }} </td>
                                        <td align="center"> <span class="btn btn-bold btn-sm btn-font-sm btn-label-danger"> {{ $item->paid }} </span> </td>
                                        <td> {{ $item->polis_no }} </td>
                                        <td align="center"> @if($item->due_date != NULL)  {{ date('d-m-Y', strtotime($item->due_date)) }} @endif </td>
                                        {{-- <td> {{ $item->url_dokumen }} </td> --}}
                                    </tr>

                                    @php
                                        $total_premi_amount += $item->premi_amount;
                                    @endphp
                                @empty
                                    <tr>
                                        <td colspan="9" align="center"> Data tidak tersedia </td>
                                    </tr>
                                @endforelse
                            </tbody>
                            <tfoot style="background: #f7f7f7;">
                                <tr>
                                    <td> <h6> Total </h6></td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td class="text-right text-info font-weight-bold">{{ number_format($total_premi_amount,2,',','.') }}</td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                </tr>
                            </tfoot>
                    </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>

<script type="text/javascript">
    function exportFile(typeFile){
        if ( typeFile == 'xls' ){
            window.open(base_url + 'report/due_date_installment/export?file=xls', "_blank");
        } else {
            window.open(base_url + 'report/due_date_installment/export?file=pdf', "_blank");
        }
    }
</script>
@stop
