@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Balance Sheet </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                @if(Auth::user()->user_role_id==6 || Auth::user()->user_role_id==5)
                <div class="btn-group dropleft">
                    <select class="form-control kt-select2 init-select2" name="idbranch" id="idbranch">
                        @if($idbranch === 'konsol')
                            <option value="konsol" selected>Konsolidasi</option>
                            @foreach($branch as $item)
                                <option value="{{$item->id}}">{{$item->branch_name}}</option>
                            @endforeach
                        @else
                            <option value="konsol">Konsolidasi</option>
                            @foreach($branch as $item)
                                <option value="{{$item->id}}" @if($item->id == $idbranch) selected @endif>{{$item->branch_name}}</option>
                            @endforeach
                        @endif

                        
                    </select>
                </div>
                @endif

            </div>
        </div>
    </div>
</div>

@php
$data_imgz = collect(\DB::select("SELECT url_image from master_company where id = 1"))->first();
@endphp
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Balance Sheet
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                    <button onclick="cetakView()" type="button" class="btn btn-outline-info">
                        <i class="flaticon2-printer"></i> Print & Download</button>
            </div>


        </div>
        <div id="svg" class="kt-portlet__body" >
            <div class="row znHeaderShow">
                <div class="col-12 znHeadCetak" style="margin-top: -10px;">
                    @php
                        $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                    @endphp
                    <div style="display: inline-block;">
                        <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                    </div>
                    {{-- <div style="display: inline-block;">
                        <span class="zn-text-logo" style="display: block;">ATA HD</span>
                        <span style="display: block;margin-left: 8px;">"Simple & Inovative Solution" </span>
                    </div> --}}
                    <div class="text-right mt-2" style="float:right;">
                        <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:10px;">Balance Sheet Report</span>
                        <span style="display: block;font-size:10px;">{{date('d M Y')}}</span>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-6 pr-4">
                    <h5 class="zn-head-line-success mb-4">Aktiva</h5>
                    <div class="kt-widget6">
                        <div class="kt-widget6__body">
                            

                            @if($aktiva)
                            @php 
                            // dd($aktiva);
                            @endphp
                            @foreach($aktiva as $key => $item)
                            @php
                            $awal=strlen($item->coa_no);
                            
                            @endphp
                            
                            @if($awal==3)

                            <div class="kt-widget6__item rw" style="background: #f7f7f7;">
                                <span class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                @php
                                    $number = number_format($item->last_os,0,',','.');
                                    $number = preg_replace('/\-([0-9,.]+)/', '(\1)', $number);
                                    
                                @endphp
                                <span class="kt-font-success kt-font-bold">Rp
                                    {{$number}}</span>
                            </div>
                            @else
                            <div class="kt-widget6__item ml-3 rw">
                                <span><a href="#" onclick="showHistoryTx('{{ $item->coa_no }}', '{{ $idbranch }}')">{{$item->coa_no}} - {{$item->coa_name}}</a></span>
                                @php
                                $number1 = number_format($item->last_os,0,',','.');
                                $number1 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number1);
                                @endphp

                                <span class="kt-font-success kt-font-bold">Rp
                                    {{$number1}}</span>
                            </div>
                            @endif


                            @endforeach
                            @endif
                        </div>
                    </div>

                </div>
                <div class="col-6 pl-4">
                    <h5 class="zn-head-line-danger mb-4">Pasiva</h5>
                    <div class="kt-widget6">
                        <div class="kt-widget6__body">
                            @if($pasiva)
                            @foreach($pasiva as $key => $item)
                            @php
                            $awal=strlen($item->coa_no);
                            @endphp
                            @if($awal==3)
                            
                            @if($item->coa_no=='302' || $item->coa_no == '303')

                            <div class="kt-widget6__item rw" style="background: #f7f7f7;">
                                <span class=" kt-font-bold"><a href="#" onclick="showHistoryTx('{{ $item->coa_no }}', '{{ $idbranch }}')">{{$item->coa_no}} - {{$item->coa_name}}</a></span>
                                 @php
                                $number2 = number_format($item->last_os,0,',','.');
                                $number2 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number2);
                                @endphp

                                <span class="kt-font-danger kt-font-bold">Rp
                                    {{$number2}}</span>
                            </div>


                            @else

                            <div class="kt-widget6__item rw" style="background: #f7f7f7;">
                                <span class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                 @php
                                $number2 = number_format($item->last_os,0,',','.');
                                $number2 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number2);
                                @endphp

                                <span class="kt-font-danger kt-font-bold">Rp
                                    {{$number2}}</span>
                            </div>

                            @endif
                            
                            @else
                            <div class="kt-widget6__item ml-3 rw">
                                <span><a href="#" onclick="showHistoryTx('{{ $item->coa_no }}', '{{ $idbranch }}')">{{$item->coa_no}} - {{$item->coa_name}}</a></span>
                                 @php
                                $number3 = number_format($item->last_os,0,',','.');
                                $number3 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number3);
                                @endphp


                                <span class="kt-font-danger kt-font-bold">Rp
                                    {{$number3}}</span>
                            </div>
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </div>



                    
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    
                    <div class="kt-widget6 mt-2" style="border-top: 2px solid #f1f1f1;">
                        <div class="kt-widget6__body">
                            @php
                            $total=0;
                            $total_pasiva=0;
                            @endphp
                                @if($aktiva)
                                    @foreach($aktiva as $item)
                                    @php
                                        $awal=strlen($item->coa_no);
                                        if($awal==6){
                                        $total+=$item->last_os;
                                        }
                                    @endphp
                                @endforeach
                            @endif

                            @if($pasiva)
                                @foreach($pasiva as $item)
                                    @php
                                        $awal=strlen($item->coa_no);
                                        
                                        if($awal==3){
                                            $total_pasiva+=$item->last_os;
                                        }
                                    @endphp
                                @endforeach
                            @endif
                            @php
                                if ( ((round($total)-round($total_pasiva)) >= 1) || ((round($total)-round($total_pasiva)) <= -1) ) {
                                    $total = $total_pasiva;
                                }
                            @endphp
                            <div class="kt-widget6__item rw">
                                <span class=" kt-font-bold">Total Aktiva</span>
                                <span class="kt-font-success kt-font-bold">Rp
                                    {{number_format($total,0,',','.')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="kt-widget6 mt-2" style="border-top: 2px solid #f1f1f1;">
                        <div class="kt-widget6__body">
                          
                            <div class="kt-widget6__item">
                                <span class=" kt-font-bold">Total Pasiva</span>
                                <span class="kt-font-danger kt-font-bold">Rp
                                    {{number_format($total_pasiva,0,',','.')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row znSignature" style="margin-top: 30px;">
                <div class="col-3" style="font-size:10px;color: #595d6e;">
                    User Maker
                    <br><br><br><br><br>
                    <b><u>Zamzam Nur</u></b>
                </div>
                <div class="col-3" style="font-size:10px;color: #595d6e;">
                    User Approval
                    <br><br><br><br><br>
                    <b><u>Kamaludin Wak</u></b>
                </div>
            </div> --}}

        </div>
    </div>
</div>


<div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content"
        {{-- style="border-radius: 0px;border: none;" --}}
        >
                <div class="modal-header">
                        <h5 class="modal-title" id="title_modal">Print & Download Preview Balance Sheet</h5>
                        <button type="button" onclick="closeCetakView()" class="close"></button>
                    </div>
            <div class="modal-body kt-portlet m-0 p-0">
                <div class="">
                    <div class="kt-portlet__body p-0">

                        <iframe id='result' class="scrollStyle" style="width: 100%;height: 520px;border: none;"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function showHistoryTx(id, idbranch) {

        if ( idbranch == 'konsol' ) {
            toastr.warning('Konsolidasi tidak dapat melihat history tx');
        } else {
            var url_get=base_url +'ref/master_coa/history_tx/' + id +'?type=neraca' + '&idbranch=' + idbranch;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: url_get,
                // data: {},
                success: function (data) {
                },
                beforeSend: function () {
                    $('#pageLoad').fadeOut();
                    loadingPage();
                },
            }).done(function (data) {
                endLoadingPage();
                var state = { name: "name", url_get: 'History', url: url_get };
                window.history.replaceState(state, "History", url_get);

                $('#pageLoad').html(data).fadeIn();
                KTBootstrapDatepicker.init();
                $('#table_id').DataTable();

            });
        }
        

    }

    // PRINT
    $(".znHeaderShow").css({"display": "none"});
    $(".znSignature").css({"opacity": "0"});

    function closeCetakView() {
        $('#zn-print').modal('hide');
        $("#svg").removeAttr('style');
        $(".rw16").removeAttr('style');
        $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "1em", "flex": "1"});
        $(".kt-widget6__item").css({"padding": "8px"});
        $(".znHeaderShow").css({"display": "none"});
        $(".znSignature").css({"opacity": "0"});
    }

    function cetakView() {
        $(".znHeaderShow").css({"display": "block"});
        $(".znSignature").css({"opacity": "1"});

        $("#svg").css({"padding": "40px", "width": "68%","font-family": "sans-serif"});
        $(".kt-widget6__item").css({"padding": "3px"});
        $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "7px", "flex": "0.5"});
        $(".rw16").css({"margin-top": "140px"});

        $('#zn-print').modal('show');
        var pdf = new jsPDF('p', 'pt', 'a4');

        var data = document.getElementById('svg');
        pdf.html(data, {
			callback: function (pdf) {
				var iframe = document.getElementById('result');
				iframe.src = pdf.output('datauristring');
			}
		});


    }
    // ENDPRINT

    $('#idbranch').on('change', function (v) {
        $.ajax({
            type: 'GET',
            url: base_url + '/report/balance_sheet?id='+this.value,
            beforeSend: function () {
                $("#loading").css('display', 'block');
            },
            success: function (res) {
                //var data = $.parseJSON(res);
                $("#loading").css('display', 'none');
                window.location.href = base_url + 'report/balance_sheet?id='+$('#idbranch').val();
            }
        });
    });

</script>
@stop
