@section('content')
<style>
    span.select2-container {
        width: 150px !important;
    }
</style>

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Installment Pending Split Payment </a>
            </div>
        </div>

        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <div class="btn-group dropleft">
                    <select class="form-control kt-select2 init-select2" name="filter" id="filter">
                        <option value="1000" @if($sts == "1000") selected @endif>All</option>
                        <option value="1" @if($sts == "1") selected @endif>Current Month</option>
                        <option value="2" @if($sts == "2") selected @endif>Next Month</option>
                    </select>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Installment Pending Split Payment
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif onclick="exportData('xls', '{{ $sts }}');" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export Excel </button>
                        <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif onclick="exportData('pdf', '{{ $sts }}');" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export PDF </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped- table-hover">
                <thead>
                    <tr>
                        <th class="text-center"> 
                            <h6>No</h6>
                        </th>
                        <th class="text-center">
                            <h6>Customer Name</h6>
                        </th>
                        <th class="text-center">
                            <h6>Product Name</h6>
                        </th>
                        <th class="text-center">
                            <h6>Agent Name</h6>
                        </th>
                        <th class="text-center">
                            <h6>Agent Fee</h6>
                        </th>
                        <th class="text-center">
                            <h6>Commision</h6>
                        </th>
                        <th class="text-center">
                            <h6>Ins Fee</h6>
                        </th>
                    </tr>
                </thead>
                <tbody>
                        @php
                            $no=0;
                        @endphp

                    @if( !is_null($data) )
                        @forelse($data as $item)
                            @php
                                $no++;
                            @endphp
                                <tr>
                                    <td class="text-center"> {{ $no }}</td>
                                    <td> {{ $item->full_name }} </td>
                                    <td> {{ $item->definition }} </td>
                                    <td> {{ $item->agent_name }}</td>    
                                    <td class="text-right">{{number_format($item->agent_fee,2,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->commision,2,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->ins_fee,2,',','.')}}</td>
                                </tr>
                        @empty
                            <tr>
                                <td colspan="7" align="center"> Data tidak ditemukan </td>
                            </tr>
                        @endforelse
                    @else
                        <tr>
                            <td colspan="7" align="center"> Data tidak ditemukan </td>
                        </tr>
                    @endif
                </tbody>
                <tfoot style="background: #f7f7f7;">
                    @php
                        $tot_agent_fee = 0;
                        $tot_commision = 0;
                        $tot_ins_fee = 0;
                    @endphp
                    
                    @if($data)
                        @foreach($data as $item)
                            @php
                                $tot_agent_fee += $item->agent_fee;
                                $tot_commision += $item->commision;
                                $tot_ins_fee += $item->ins_fee;
                            @endphp
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="4">
                            <h6 class="text-center">Total</h6>
                        </td>
                        <td class="text-right text-success font-weight-bold">{{number_format($tot_agent_fee,2,',','.')}}</td>
                        <td class="text-right text-success font-weight-bold">{{number_format($tot_commision,2,',','.')}}</td>
                        <td class="text-right text-success font-weight-bold">{{number_format($tot_ins_fee,2,',','.')}}</td>
                    </tr>
                </tfoot>
            </table>



        </div>
    </div>


</div>
<script type="text/javascript">

    function exportData(ext, filter) {

        console.log(' extention export data : ' + ext);
        console.log(' filter : ' + filter);

        switch (filter) {
            case '1000' : 
                 window.open(base_url + 'report/installment_pending_split_payment/export?file=' + ext + '&filter=All', "_blank");
            break;
            case '1' : 
                window.open(base_url + 'report/installment_pending_split_payment/export?file=' + ext + '&filter=Current Month', "_blank");
            break;
            case '2' : 
                window.open(base_url + 'report/installment_pending_split_payment/export?file=' + ext + '&filter=Next Month', "_blank");
            break;
        }
    }

    $('#filter').on('change', function (v) {
        var filter = $(this).val();

        switch (filter) {
            case '1000' : 
                window.location.href = base_url + 'report/installment_pending_split_payment?filter=All';
            break;
            case '1' : 
                window.location.href = base_url + 'report/installment_pending_split_payment?filter=Current Month';
            break;
            case '2' : 
                window.location.href = base_url + 'report/installment_pending_split_payment?filter=Next Month';
            break;
        }
    });
    
</script>
@stop
