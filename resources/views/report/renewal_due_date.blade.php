@section('content')
<style>
    span.select2-container {
        width: 200px !important;
    }
</style>

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Renewal Due Date </a>
            </div>
        </div>        
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Renewal List Due Date
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                
                <div class="form-group pt-4 text-right ml-5">
                    <div class="col-12">
                        {{-- <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif onclick="exportData('xls', '{{ $sts }}');" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export Excel </button> --}}
                        <button onclick="exportData('xls');" id="export_excel" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export Excel </button>
                        {{-- <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif  onclick="exportData('pdf', '{{ $sts }}');" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export PDF </button> --}}
                        <button onclick="exportData('pdf');" id="export_pdf" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export PDF </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped- table-hover" id="renewal_list_table">
                <thead>
                    <tr>
                        <th class="text-center">
                            <h6>Act</h6>
                        </th>
                        <th class="text-center">
                            <h6>Agent Name</h6>
                        </th>
                        <th class="text-center">
                            <h6>Client Name</h6>
                        </th>
                        <th class="text-center">
                            <h6>Policy Name</h6>
                        </th>                        
                        <th class="text-right">
                            <h6>Product</h6>
                        </th>
                        <th class="text-right">
                            <h6>Start Date</h6>
                        </th>
                        <th class="text-right">
                            <h6>End Date</h6>
                        </th>
                        <th class="text-right">
                            <h6>Underwriter</h6>
                        </th>
                        <th class="text-right">
                            <h6>Policy No</h6>
                        </th>
                        <th class="text-right">
                            <h6>Valuta</h6>
                        </th>
                        <th class="text-right">
                            <h6>Sum Insured</h6>
                        </th>
                        <th class="text-right">
                            <h6>Gross Premi</h6>
                        </th>
                        <th class="text-right">
                            <h6>Netto Brokerage</h6>
                        </th>
                    </tr>
                </thead>
                <tbody>                    
                </tbody>                
                <tfoot style="background: #f7f7f7;">                    
                    <tr>
                        <td colspan="10" class="text-right font-weight-bold">Total</td>
                        <td class="text-right text-success font-weight-bold"></td>
                        <td class="text-right text-success font-weight-bold"></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


</div>
@include('marketing.modal.modal_detail')
<script>


    var table,
        dataPost,
        columns,
        columnsDefWant;
    $(document).ready(function(){
        table = $('#renewal_list_table');		
        
        dataTableList('{{ route('data_renewal_list_due_date') }}');		
    });    

    function dataTableList(url) {

        console.log('waww');
        

        dataPost = {            
            start_date: znFormatDateNum($('#start_date').val()),
            end_date: znFormatDateNum($('#end_date').val()),                        
        };
        columnsWant = [
            {data: '', name: '', orderable: false},
            {data: 'full_name', name: 'full_name'},
            {data: 'nama_client', name: 'nama_client'},
            {data: 'police_name', name: 'police_name'},
            {data: 'definition', name: 'definition'},
            {data: 'start_date_polis', name: 'start_date_polis'},
            {data: 'end_date_polis', name: 'end_date_polis'},
            {data: 'uw_def', name: 'uw_def'},
            {data: 'polis_no', name: 'polis_no'},
            {data: 'deskripsi', name: 'deskripsi'},
            {data: 'ins_amount', name: 'ins_amount', class: 'text-right'},
            {data: 'net_amount', name: 'net_amount', class: 'text-right'},
            {data: 'netto_brokerage', name: 'netto_brokerage'},
        ];
        columnDefsWant = [
            {
                targets: 0,
                title: 'Act',
                orderable: false,
                render: function(data, type, full, meta) {                                              
                    return `
                    <div class="dropdown dropdown-inline">
                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" style="cursor:pointer;" onclick="detail_invoice(`+full.id_msp+`,`+full.invoice_type_id+`)">
                                <i class="la la-check"></i> View Policy Details
                            </a>                            
                        </div>
                    </div>
                    `;
                },
            },
            {                
                targets: 5,
                render: function(data, type, full, meta) {
                    return znFormatDate(data)
                },
            },
            {                
                targets: 6,
                render: function(data, type, full, meta) {
                    return znFormatDate(data)
                },
            },
            {         
                width: '75px',
                targets: -3,
                render: function(data, type, full, meta) {
                    return numFormatNew(data)
                },
            },
            {                
                width: '75px',
                targets: -2,
                render: function(data, type, full, meta) {
                    return numFormatNew(data)
                },
            },
        ];
        table.DataTable({
            dom: `<'row'<'col-sm-12'f>>
            <'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'p>>`,
            // responsive: true,            
			searchDelay: 500,			
            paging: false,
            "lengthMenu": [[-1], ["All"]],
            ajax: {
				url: url,
				type: 'POST',
                data: dataPost,                                               
                error: function(xhr, ajaxOptions, thrownError)
                {                                        
                    toastr.error("Terjadi Kesalahan");
                },                                            
			},
			columns: columnsWant,
            columnDefs: columnDefsWant,
            footerCallback: function ( row, data, start, end, display ) {                
                var api = this.api(), data;                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // Total over all pages
                /* total = api
                    .column( 10 )
                    .data()
                    .reduce( function (a, b) {
                        console.log(a)
                        console.log(b)
                        return intVal(a) + intVal(b);
                    }, 0 );  */               
    
                // Total over this page
                pageTotal = api
                    .column( 10, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( 10 ).footer() ).html(
                    // numFormatNew(pageTotal) +' ( '+ numFormatNew(total) +' total SI)'
                    numFormatNew(pageTotal)
                );

                // Total over all pages
                /* totalGp = api
                    .column( 11 )
                    .data()
                    .reduce( function (a, b) {
                        console.log(a)
                        console.log(b)
                        return intVal(a) + intVal(b);
                    }, 0 ); */                
    
                // Total over this page
                pageTotalGp = api
                    .column( 11, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( 11 ).footer() ).html(
                    // numFormatNew(pageTotalGp) +' ( '+ numFormatNew(totalGp) +' total GP)'
                    numFormatNew(pageTotalGp)
                );
            }
        });        
        checkEmptyDataTable()
    }

    function checkEmptyDataTable() {
        table.DataTable().on( 'init.dt', function () {
            if(table.DataTable().data().count()==0) {
                $('#export_excel').attr('disabled','disabled')
                $('#export_excel').css('cursor','no-drop')
                $('#export_pdf').attr('disabled','disabled')
                $('#export_pdf').css('cursor','no-drop')
            } else {
                $('#export_excel').removeAttr('disabled')
                $('#export_excel').css('cursor','pointer')
                $('#export_pdf').removeAttr('disabled')
                $('#export_pdf').css('cursor','pointer')
            }
        })                  
    }

    function destroyTable() {
        table.DataTable().destroy();
    }

    function reloadTable() {
        table.DataTable().ajax.reload();
    }

    function exportData(type) {
        var query = {
            type: type,
        };
        var url = "{{URL::to('report/renewal_list_due_date/export')}}?" + $.param(query)            
        window.open(url);
    }

    function detail_invoice(id,type){
        loadingPage();
        switch (type) {

            //    ORIGINAL
            case 0:
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY');
                $('#v_fee_internal').show();
                $('#v_agent_fee_amount').show();
                $('#v_tax_amount').show();
                $('#v_ins_fee').show();
                $('#v_cf_no').show();
                var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
            break;
            //    ENDORS
            case 1:
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY ENDORSMENT');
                $('#v_fee_internal').show();
                $('#v_agent_fee_amount').show();
                $('#v_tax_amount').show();
                $('#v_ins_fee').show();
                $('#v_cf_no').hide();
                var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
            break;
            case 'installment':
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY INSTALLMENT');
                $('#v_fee_internal').hide();
                $('#v_agent_fee_amount').hide();
                $('#v_tax_amount').hide();
                $('#v_ins_fee').hide();
                $('#v_cf_no').show();
                $('#label_police_duty').html('Polis Amount');
                $('#label_stamp_duty').html('Materai Amount');
                $('#label_adm_duty').html('Admin Amount');
                var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
            break;
            case 'installment_detail':
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY INSTALLMENT SCHEDULE');
                $('#v_fee_internal').hide();
                $('#v_agent_fee_amount').hide();
                $('#v_tax_amount').hide();
                $('#v_ins_fee').hide();
                $('#v_cf_no').show();
                $('#label_police_duty').html('Polis Amount');
                $('#label_stamp_duty').html('Materai Amount');
                $('#label_adm_duty').html('Admin Amount');
                var set_url = base_url + 'marketing/data/detail_master_sales_schedule_polis/' + id;
            break;
        }
            
        $.ajax({
            type: 'GET',
            url: set_url,
            success: function (res) {
                    let data = res.rm;
                    console.log(data);
                endLoadingPage();
                
                $('#cf_no').html(data.cf_no);
                $('#polis_no').html(data.polis_no);
                $('#police_name').html(data.police_name);
                $('#underwriter').html(data.underwriter);
                $('#customer').html(data.customer);
                $('#no_of_insured').html(data.no_of_insured);
                $('#mata_uang').html(data.mata_uang);
                $('#coc_no').html(data.cf_no);
                $('#coc_no').html(data.cf_no);
                
                    $('#product').html(data.produk);
                    $('#agent').html(data.agent);
                    $('#start_date').html(formatDate(new Date(data.start_date_polis)));
                    $('#end_date').html(formatDate(new Date(data.end_date_polis)));

                    // NORMAL
                    $('#ins_amount').html(formatMoney(data.ins_amount));
                    $('#premi_amount1').html(formatMoney(data.premi_amount));
                    $('#disc_amount').html(formatMoney(data.disc_amount));
                    $('#net_amount').html(formatMoney(data.net_amount));
                    $('#agent_fee_amount').html(formatMoney(data.agent_fee_amount));
                    $('#tax_amount').html(formatMoney(data.tax_amount));
                    $('#premi_amount').html(formatMoney(data.net_amount));
                    $('#fee_internal').html(formatMoney(data.comp_fee_amount));
                    
                    $('#ins_fee').html(formatMoney(data.ins_fee));
                    // NEW
                    $('#stamp_duty').html(formatMoney(data.materai_amount));
                    $('#police_duty').html(formatMoney(data.polis_amount));
                    $('#adm_duty').html(formatMoney(data.admin_amount));
                    // NEW

                    detail_invoice_product(data.product_id,data.id);
                
                $('#detail').modal('show');
            }
        });
    }

    function detail_invoice_product(prod_id,id_sales) {
        $.ajax({
            type: "GET",
            url: base_url + '/quotation_slip/get_detail_insured/' + prod_id,
            data: {},

            beforeSend: function () {
                // loadingPage();
            },

            success: function (res) {
                var data = res.data;

                if (res.rc == 1) {
                    var tableHeaders = "";
                    // tableHeaders = `<th width="30px" class="text-center">Action</th>
                    //                 <th class="text-center"> Status </th>`;
                    data.forEach(function ( value, index) {
                        let isShow = (value.is_showing) ? 'all':'none';
                        console.log(isShow);
                        tableHeaders += `<th class="${isShow} text-center">${value.label}</th>`;
                    })
                    $("#tableDiv").empty();

                    if (data.length > 0) {
                        $("#tableDiv").append('<table id="table_detail" class="table table-striped- table-hover table-checkable"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                        DataTable = $('#table_detail').DataTable({responsive: true});

                        if (id_sales) {
                            getDetailProduct(id_sales,'detail');
                        }
                    } 
                    
                } else {
                    toastr.error(data.rm);
                }
            }
        }).done(function (msg) {
            // endLoadingPage();
        }).fail(function (msg) {
            // endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function getDetailProduct(id,typeTable) {
        data_detail_product = [];
        if (typeTable == 'detail_cf') {
            var set_url =  base_url + 'marketing/data/detail_product_cf/' + id; 
        }
        if (typeTable == 'detail_no_polis') {
            var set_url =  base_url + 'marketing/data/detail_product_no_polis/byget?polis_no=' + id; 
        }
        else{
            var set_url =  base_url + 'marketing/data/detail_product/' + id;
        }
        $.ajax({
            type: 'GET',
            url: set_url,
            beforeSend: function (res) {
                loadingPage();
            },
            success: function (res) {
                var dataDetail =res.rm;            
                dataDetail.forEach((vv,i) => {
                    id_array_data = i;
                    temp_data = [];
                    let rows = [];

                    if (typeTable == 'normal' || typeTable == 'detail_cf' || typeTable == 'detail_no_polis') {
                        var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="flaticon-more"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" onclick="modal_add_detail('edit', '${id_array_data}')">
                                            <i class="la la-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" onclick="hapus_detail('${id_array_data}', this)">
                                            <i class="la la-trash"></i> Hapus
                                        </a>`;

                        var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';

                        rows.unshift(sts);
                        rows.unshift(action);
                    }

                

                    vv.forEach(data => {

                        if (data.value_text != 't') {
                            rows.push(data.value_text);
                        }
                        

                        var item = {
                            "name" : data.input_name,
                            "value": data.value_text,
                            "bit_id" : data.bit_id,
                        };
                        temp_data.push(item);

                        console.log('temp_data',temp_data);
                        
                    });

                    data_detail_product.push({
                        id : id_array_data,
                        value : temp_data
                    });

                    DataTable.row.add(rows).draw();
                });

                endLoadingPage();

            }
        });
    }

    function formatDate(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }


    function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? "-" : "";

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
        } catch (e) {
            console.log(e)
        }
    }

</script>
@stop
