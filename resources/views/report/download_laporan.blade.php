@section('content')

@php
    if (isset($_GET["status"])) {
        $status = $_GET["status"];
    }else {
        $status = date('Y');
    }
    $bulan = array (
        1 => 'January',
        'February',
        'March',
        'April',
        'Mey',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );
    $listDir = \File::directories(public_path('batch_file'));
    
@endphp
<div class="app-content">
    <div class="section">

        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Report </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Download File Laporan {{$status}} </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet  kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Download File Laporan {{$status}}
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="form-group row pt-4">
                            <div class="col-12" style="width: 300px;float: right;">
                                <select class="form-control" name="filter_tahun" id="filter_tahun">
                                    @for ($year = date('Y'); $year >= 2017 ; $year--)
                                        <option value="{{$year}}" @if($year == $status) selected @endif>Filter File Laporan Tahun {{$year}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2"
                        data-ktwizard-state="step-first">
                        <div class="kt-grid__item kt-wizard-v2__aside pt-0">

                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v2__nav">
                                <div class="kt-wizard-v2__nav-items">

                                    <!--doc: Replace A tag with SPAN tag to disable the step link click -->
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step"
                                        data-ktwizard-state="current">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    January
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From January
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    February
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From February
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    March
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From March
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                            <div class="kt-wizard-v2__nav-body">
                                                <div class="kt-wizard-v2__nav-icon">
                                                    <i class="flaticon-folder-1"></i>
                                                </div>
                                                <div class="kt-wizard-v2__nav-label">
                                                    <div class="kt-wizard-v2__nav-label-title">
                                                        April
                                                    </div>
                                                    <div class="kt-wizard-v2__nav-label-desc">
                                                        All Report File From April
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    May
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From May
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    June
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From June
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    July
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From July
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    August
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From August
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    September
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From September
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    October
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From October
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    November
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From November
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <i class="flaticon-folder-1"></i>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Desember
                                                </div>
                                                <div class="kt-wizard-v2__nav-label-desc">
                                                    All Report File From Desember
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper pt-0">

                            <!--begin: Form Wizard Form-->
                            <form class="kt-form pt-0" id="kt_form">

                                @for ($month = 01; $month <= 12; $month++)
                                <!--begin: month-->
                                <div class="kt-wizard-v2__content" data-ktwizard-type="step-content"
                                    data-ktwizard-state="current">
                                    <div class="kt-heading kt-heading--md mb-5">
                                        {{$bulan[$month]}}
                                        <small style="display:block;">All Report File From {{$bulan[$month]}} {{$status}}</small>
                                    </div>
                                    <div class="kt-form__section kt-form__section--first">
                                            <div class="accordion accordion-light  accordion-toggle-arrow">

                                            @php
                                                if ($month < 10) {
                                                    $newMonth = "0".$month;
                                                } else {
                                                    $newMonth = $month;
                                                }
                                            @endphp

                                            @foreach ($listDir as $item)
                                                @if (substr($item, -10,7) == $status."-".$newMonth )
                                                    {{-- SUB DATE --}}
                                                    <div class="card">
                                                            <div class="card-header" id="headingOne2">
                                                                <div class="card-title collapsed" data-toggle="collapse"
                                                            data-target="#k{{substr($item, -10)}}" aria-expanded="false">
                                                                    {{date('d M Y',strtotime(substr($item, -10)))}} 
                                                                </div>
                                                            </div>
                                                            <div id="k{{substr($item, -10)}}" class="collapse" style="">

                                                                @php
                                                                    $listDirType = \File::directories(public_path('batch_file/'.substr($item, -10)));
                                                                @endphp

                                                                <div class="ml-5 accordion  accordion-toggle-arrow" id="infolder{{substr($item, -10)}}">
                                                                    {{-- SUB FOLDER INSIDE DATE --}}
                                                                    @foreach ($listDirType as $i => $dirType)
                                                                    @php
                                                                        $keyCollapse = substr($item, -10).$i;
                                                                        $nameSubFolder = substr($dirType, (strpos($dirType,substr($item, -10))+11));
                                                                        $listFile = \File::allFiles(public_path('batch_file/'.substr($item, -10).'/'.$nameSubFolder));
                                                                        $getBranch = collect(\DB::select('select * from master_branch where id = '.Auth::user()->branch_id))->first();
                                                                      
                                                                    @endphp
                                                                    {{-- SOLO --}}
                                                                    @if ($getBranch->id == 0)
                                                                        <div class="card">
                                                                            <div class="card-header" id="headingOne4">
                                                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#co_infolder{{$keyCollapse}}">
                                                                                    <i class="flaticon2-layers-1"></i> {{$nameSubFolder}} 
                                                                                </div>
                                                                            </div>
                                                                            <div id="co_infolder{{$keyCollapse}}" class="collapse" aria-labelledby="headingOne" data-parent="#infolder{{substr($item, -10)}}">
                                                                                <div class="card-body">
                                                                                    {{-- LIST DATA INSIDE SUB FOLDER --}}
                                                                                
                                                                                    <div class="card-body">
                                                                                        <div class="kt-widget4">
                    
                                                                                            @foreach ($listFile as $key=> $lf)
                                                                                            {{-- @php
                                                                                                dd(substr($listFile[$key]->getPath(), (strpos($dirType,substr($item, -10))+11)));
                                                                                            @endphp
                    --}}
                                                                                                @if (substr($listFile[$key]->getPath(), (strpos($dirType,substr($item, -10))+11)) == $nameSubFolder)
                                                                                                    <div class="kt-widget4__item">
                                                                                                        <div class="kt-widget4__pic kt-widget4__pic--icon">
                                                                                                            @if ($listFile[$key]->getExtension() == "pdf")
                                                                                                                <img src="{{asset('assets/media/files/pdf.svg')}}" alt="">
                                                                                                            @else
                                                                                                                <i class="fa fa-file-excel zn-excel-icon"></i>
                                                                                                            @endif
                    
                                                                                                        </div>
                                                                                                        <div class="kt-widget4__info">
                                                                                                            <span class="kt-widget4__username">
                                                                                                                {{$listFile[$key]->getFilename()}}
                                                                                                            </span>
                                                                                                            <p class="kt-widget4__text">
                                                                                                                {{round($listFile[$key]->getSize()/1024)}} Kb
                                                                                                            </p>
                                                                                                        </div>
                                                                                                    <a href="{{asset('batch_file')}}/{{substr($item, -10)}}/{{$nameSubFolder}}/{{$listFile[$key]->getFilename()}}" class="btn btn-sm btn-label-success btn-bold">Download</a>
                                                                                                    </div>
                                                                                                @endif
                                                                                            @endforeach
                    
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    @elseif ($getBranch->short_code == $nameSubFolder)

                                                                        <div class="card">
                                                                            <div class="card-header" id="headingOne4">
                                                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#co_infolder{{$keyCollapse}}">
                                                                                    <i class="flaticon2-layers-1"></i> {{$nameSubFolder}} 
                                                                                </div>
                                                                            </div>
                                                                            <div id="co_infolder{{$keyCollapse}}" class="collapse" aria-labelledby="headingOne" data-parent="#infolder{{substr($item, -10)}}">
                                                                                <div class="card-body">
                                                                                    {{-- LIST DATA INSIDE SUB FOLDER --}}
                                                                                   
                                                                                    <div class="card-body">
                                                                                        <div class="kt-widget4">
                    
                                                                                            @foreach ($listFile as $key=> $lf)
                                                                                            {{-- @php
                                                                                                dd(substr($listFile[$key]->getPath(), (strpos($dirType,substr($item, -10))+11)));
                                                                                            @endphp
                     --}}
                                                                                                @if (substr($listFile[$key]->getPath(), (strpos($dirType,substr($item, -10))+11)) == $nameSubFolder)
                                                                                                    <div class="kt-widget4__item">
                                                                                                        <div class="kt-widget4__pic kt-widget4__pic--icon">
                                                                                                            @if ($listFile[$key]->getExtension() == "pdf")
                                                                                                                <img src="{{asset('assets/media/files/pdf.svg')}}" alt="">
                                                                                                            @else
                                                                                                                <i class="fa fa-file-excel zn-excel-icon"></i>
                                                                                                             @endif
                    
                                                                                                        </div>
                                                                                                        <div class="kt-widget4__info">
                                                                                                            <span class="kt-widget4__username">
                                                                                                                {{$listFile[$key]->getFilename()}}
                                                                                                            </span>
                                                                                                            <p class="kt-widget4__text">
                                                                                                                {{round($listFile[$key]->getSize()/1024)}} Kb
                                                                                                            </p>
                                                                                                        </div>
                                                                                                    <a href="{{asset('batch_file')}}/{{substr($item, -10)}}/{{$nameSubFolder}}/{{$listFile[$key]->getFilename()}}" class="btn btn-sm btn-label-success btn-bold">Download</a>
                                                                                                    </div>
                                                                                                @endif
                                                                                            @endforeach
                    
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                    @endforeach
                                                                    
                                                                         
                                                                        
                                                                   
                                                                   
                                                                   
                                                                </div>

                                                                

                                                                
                                                            </div>
                                                        </div>
                                                @endif
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                                <!--end: month-->
                                @endfor


                                <!--begin: Form Actions -->
                                <div class="kt-form__actions">
                                    <button
                                        class="btn btn-danger btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                        data-ktwizard-type="action-prev">
                                        Previous Month
                                    </button>

                                    <button
                                        class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                        data-ktwizard-type="action-next">
                                        Next Month
                                    </button>
                                </div>

                                <!--end: Form Actions -->
                            </form>

                            <!--end: Form Wizard Form-->
                        </div>
                    </div>


                </div>
            </div>

        </div>


        <!-- end:: Subheader -->
    </div>


</div>

<script>
    $('#filter_tahun').select2({
        placeholder: "Pilih Tahun"
    });

    $('#filter_tahun').on('change', function (v) {
        var status = $(this).val();
        window.location.href = base_url + 'report/download_laporan?status='+status;
    });

</script>

@stop
