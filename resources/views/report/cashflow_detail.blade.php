@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Cash Inflow & Outflow Detail </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <div class="kt-portlet__head-title">
                <h4>Cash Inflow & Outflow Detail
                </h4>
                <h6>10 Oktober to 20 Desember 2019</h6>
            </div>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="form-group row pt-4">
                    {{-- <label class="col-form-label col-12">Pick Start Date and End Date</label> --}}

                    <div class="col-12">
                            <button onclick="cetakView()" type="button" class="btn btn-info">
                                    <i class="flaticon2-printer"></i> Print & Download</button>
                        <button type="button" class="btn btn-success btn-elevate">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="svg" class="kt-portlet__body">
                <div class="row znHeaderShow">
                        <div class="col-12 znHeadCetak" style="margin-top: -10px;">
                            <div style="display: inline-block;">
                                <img alt="Logo" src="{{asset('img/logo.jpg')}}" style="width: 40px;margin-top: -25px;" />
                            </div>
                            <div style="display: inline-block;">
                                <span class="zn-text-logo" style="display: block;">ATA HD</span>
                                <span style="display: block;margin-left: 8px;">"Simple & Inovative Solution" </span>
                            </div>
                            <div class="text-right mt-2" style="float:right;">
                                <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:10px;">Transaction Detail Report</span>
                                <span style="display: block;font-size:10px;">20 Oktober - 30 Oktober 2019</span>

                            </div>

                        </div>
                    </div>
            <div class="accordion accordion-light" id="accordionExample2">

                {{-- OPERATING ACTIVITIES --}}
                <div class="card">
                    <div class="card-header zn-card-header" style="background: #f7f7f7;padding: 6px 14px !important;" >
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title">
                                    Operating Activities
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-title" style="display:block;">
                                    <span
                                        class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                        Inflow
                                    </span>
                                    Pendapatan dari Penjualan
                                </div>
                            </div>
                            <div class="col-4 text-right p-3">
                                <h6>Rp {{number_format($data->inflow1,0,',','.')}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-title">
                                    <span
                                        class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Outflow
                                    </span>

                                    Beban Pemasaran

                                </div>
                            </div>
                            <div class="col-4 text-right p-3">
                                <h6>Rp {{number_format($data->outflow1,0,',','.')}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-title">
                                    <span
                                        class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Outflow
                                    </span>
                                    Beban Umum dan Administrasi (Master Beban Gedung)

                                </div>
                            </div>
                            <div class="col-4 text-right p-3">
                                <h6>Rp {{number_format($data->outflow2,0,',','.')}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-title">
                                    <span
                                        class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Outflow
                                    </span>
                                    Beban Umum dan Administrasi (Master Beban Kantor)

                                </div>
                            </div>
                            <div class="col-4 text-right p-3">
                                <h6>Rp {{number_format($data->outflow3,0,',','.')}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-title">
                                    <span
                                        class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Outflow
                                    </span>
                                    Beban Umum dan Administrasi (Master Beban Pegawai)

                                </div>
                            </div>
                            <div class="col-4 text-right p-3">
                                <h6>Rp {{number_format($data->outflow4,0,',','.')}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-5">
                    <div class="card-header zn-card-header"  id="headingOne2">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-title ">
                                        <span
                                        class="mr-2 kt-badge kt-badge--unified-info kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                        Total
                                    </span>
                                    Net Cash from Operating Activities
                                </div>
                            </div>
                            @php
                            $total1=$data->inflow1 + $data->outflow1 + $data->outflow1 + $data->outflow2 + $data->outflow3 + $data->outflow4
                            @endphp
                            <div class="col-4 text-right p-3">
                                <h5 class="text-info">Rp {{number_format($total1,0,',','.')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- INVESTING ACTIVITIES --}}
                <div id="invensView" class="card">
                    <div class="card-header zn-card-header" style="background: #f7f7f7;padding: 6px 14px !important;" >
                        <div class="row">
                            <div class="col-12">
                                <div class="card-title">
                                    Investing Activities
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-8">
                                <div class="card-title">
                                    <span
                                        class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                        Inflow
                                    </span>
                                    Property, Plan, Equipped
                                </div>
                            </div>
                            <div class="col-4 text-right p-3">
                                <h6>Rp {{number_format($data->inflow2,0,',','.')}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title">
                                    <span
                                        class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Outflow
                                    </span>

                                    Other

                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h6>Rp {{number_format($data->outflow5,0,',','.')}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-5">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title ">
                                        <span
                                        class="mr-2 kt-badge kt-badge--unified-info kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                        Total
                                    </span>
                                    Net Cash From Investing Activities

                                </div>
                            </div>
                            @php
                            $total2=$data->inflow2 + $data->outflow5;
                            @endphp
                            <div class="col-6 text-right p-3">
                                <h5 class="text-info">Rp {{number_format($total2,0,',','.')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>


                {{-- FINANCING ACTIVITIES --}}
                <div id="financeView" class="card">
                        <div  class="card-header zn-card-header" style="background: #f7f7f7;padding: 6px 14px !important;" >
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Financing Activities
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        <span
                                            class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            Inflow
                                        </span>
                                        Loan and Lines of Credit

                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h6>Rp {{number_format($data->inflow3,0,',','.')}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                            <div class="card-header zn-card-header"  id="headingOne2">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="card-title">
                                            <span
                                                class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                                Inflow
                                            </span>
                                            Owners and Shareholders


                                        </div>
                                    </div>
                                    <div class="col-6 text-right p-3">
                                        <h6>Rp {{number_format($data->inflow4,0,',','.')}}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                                <div class="card-header zn-card-header" id="headingOne2">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="card-title">
                                                <span
                                                    class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                                    Inflow
                                                </span>
                                                Others



                                            </div>
                                        </div>
                                        <div class="col-6 text-right p-3">
                                            <h6>Rp {{number_format($data->inflow5,0,',','.')}}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div class="card">
                        <div class="card-header zn-card-header" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title ">
                                            <span
                                            class="mr-2 kt-badge kt-badge--unified-info kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" style="width: 70px;">
                                            Total
                                        </span>
                                            Net Cash From Financing Activities


                                    </div>
                                </div>
                                 @php
                                $total3=$data->inflow3 +$data->inflow4 + $data->inflow5;
                                @endphp
                                <div class="col-6 text-right p-3">
                                    <h5 class="text-info">Rp {{number_format($total3,0,',','.')}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

            {{-- <div class="row znSignature" style="margin-top: 30px;padding: 0 10px;">
                    <div class="col-3" style="font-size:12px;color: #595d6e;">
                        User Maker
                        <br><br><br><br><br>
                        <b><u>Zamzam Nur</u></b>
                    </div>
                    <div class="col-3" style="font-size:12px;color: #595d6e;">
                        User Approval
                        <br><br><br><br><br>
                        <b><u>Kamaludin Wak</u></b>
                    </div>
                </div> --}}
        </div>
    </div>


</div>


<div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content"
        {{-- style="border-radius: 0px;border: none;" --}}
        >
                <div class="modal-header">
                        <h5 class="modal-title" id="title_modal">Print & Download Preview Cash Inflow and Outflow Detail</h5>
                        <button type="button" onclick="closeCetakView()" class="close"></button>
                    </div>
            <div class="modal-body kt-portlet m-0 p-0">
                <div class="">
                    <div class="kt-portlet__body p-0">

                        <iframe id='result' class="scrollStyle" style="width: 100%;height: 520px;border: none;"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function detail_(id) {
        loadingPage();
        window.location.href = base_url + 'transaksi_detail?id=' + id;
        endLoadingPage();
    }

    // PRINT
    $(".znHeaderShow").css({"display": "none"});
    $(".znSignature").css({"opacity": "0"});

    function closeCetakView() {
        $('#zn-print').modal('hide');
        $("#svg").removeAttr('style');
        // $(".rw16").removeAttr('style');
        // $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "1em", "flex": "1"});
        // $(".kt-widget6__item").css({"padding": "8px"});
        $(".znHeaderShow").css({"display": "none"});
        // $(".znSignature").css({"opacity": "0"});
        $("#financeView").removeAttr('style');
        $("#invensView").removeAttr('style');
        $(".card").css({"height": "inherit"});

        $(".card-title").css({"display": "block","padding":"1rem 1rem 1rem 0","font-size":" 1.1rem"});

    }
    function cetakView() {
        $(".znHeaderShow").css({"display": "block"});
        $(".znSignature").css({"opacity": "1"});

        $("#svg").css({"padding": "30px 40px", "width": "850px","font-family": "sans-serif"});
        $("#financeView").css({"margin-top": "70px"});
        $("#invensView").css({"margin-top": "-30px"});

        $(".card-title").css({"display": "block","padding":"5px 0","font-size":"12px"});
        $(".card").css({"height": "40px"});





        $('#zn-print').modal('show');
        var pdf = new jsPDF('l', 'pt', 'a4');

        var data = document.getElementById('svg');
        pdf.html(data, {
			callback: function (pdf) {
				var iframe = document.getElementById('result');
				iframe.src = pdf.output('datauristring');
			}
		});
    }
    // ENDPRINT

</script>
@stop
