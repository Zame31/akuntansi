@section('content')
<style>
    span.select2-container {
        width: 130px !important;
    }
</style>

<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Report </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Customer Nominative Report </a>
                        </div>
                </div>

                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <div class="btn-group dropleft">
                            <select class="form-control kt-select2 init-select2" name="status" id="status">
                                <option value="1000" @if($sts == "1000") selected @endif>All</option>
                                <option value="1" @if($sts == "1") selected @endif>Aktif</option>
                                <option value="2" @if($sts == "2") selected @endif>Paid</option>
                                <option value="3" @if($sts == "3") selected @endif>Completed</option>
                            </select>
                        </div>
    
                    </div>
                </div>

            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Customer Nominative Report
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif onclick="exportToXls('{{ $sts }}');" type="button" class="btn btn-outline-info">
                                        <i class="flaticon2-file"></i> Export Excel </button>
                                <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif onclick="exportToPDF('{{ $sts }}');" type="button" class="btn btn-outline-info">
                                        <i class="flaticon2-file"></i> Export PDF </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable">
                                <thead>
                                    <tr>
                                        <th class="text-center"> Branch Code </th>
                                        <th class="text-center"> No Polis </th>
                                        <th class="text-center">Nama Tertanggung</th>
                                        <th class="text-center"> Produk </th>
                                        <th class="text-center"> Sum Insured </th>
                                        <th class="text-center"> Premium </th>
                                        <th class="text-center"> Discount </th>
                                        <th class="text-center"> Nett Premium </th>
                                        <th class="text-center"> Commision </th>
                                        <th class="text-center"> Agent Fee </th>
                                        <th class="text-center"> Nett to Underwriter </th>
                                        <th class="text-center"> Inv Type </th>
                                        {{-- <th class="text-center"> Marketing Officer </th>
                                        <th class="text-center"> Agent Name </th> --}}
                                    </th>
                                </thead>
                                <tbody>
                                    @if ( !is_null($data) )
                                        @forelse ($data as $item)
                                            <tr>
                                                <td align="center"> {{ $short_code }} </td>
                                                <td> {{ $item->polis_no }} </td>
                                                <td> {{ $item->nama_tertanggung }} </td>
                                                <td> {{ $item->produk }} </td>
                                                <td align="right"> {{ number_format($item->sum_insured, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->premium, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->disc_amount, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->nett_premium, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->commision, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->agent_fee, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->nett_to_underwriter, 2), ',' , '.' }} </td>
                                                <td> {{ $item->inv_type }} </td>
                                                {{-- <td> {{ $item->agent_name }} </td>
                                                <td> {{ $item->segment }} </td> --}}
                                            </tr>
                                        @empty
                                            <tr>
                                                <td align="center" colspan="12"> Data tidak ditemukan </td>
                                            </tr>
                                        @endforelse
                                    @else
                                        <tr>
                                            <td align="center" colspan="12"> Data tidak ditemukan </td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot style="background: #f7f7f7;">
                                        @php
                                            $tot_insured = 0;
                                            $tot_premium = 0;
                                            $tot_disc_amount = 0;
                                            $tot_nett_premium = 0;
                                            $tot_commision = 0;
                                            $tot_agent_fee = 0;
                                            $tot_nett_to_underwriter = 0;
                                        @endphp
                    
                                    @if($data)
                                        @foreach($data as $item)
                
                                            @php
                                                $tot_insured += $item->sum_insured;
                                                $tot_premium += $item->premium;
                                                $tot_disc_amount += $item->disc_amount;
                                                $tot_nett_premium += $item->nett_premium;
                                                $tot_commision += $item->commision;
                                                $tot_agent_fee += $item->agent_fee;
                                                $tot_nett_to_underwriter += $item->nett_to_underwriter;
                                            @endphp
                
                                        @endforeach
                                    @endif
                                    <tr>
                                        <td colspan="4" align="center"> <h6> Total </h6></td>
                                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_insured,2,',','.') }}</td>
                                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_premium,2,',','.') }}</td>
                                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_disc_amount,2,',','.') }}</td>
                                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_nett_premium,2,',','.') }}</td>
                                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_commision,2,',','.') }}</td>
                                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_agent_fee,2,',','.') }}</td>
                                        <td class="text-right text-info font-weight-bold">{{ number_format($tot_nett_to_underwriter,2,',','.') }}</td>
                                        <td> </td>
                                    </tr>
                                </tfoot>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>

<script type="text/javascript">
    function exportToPDF(sts) {
        console.log('status cetak: ' + sts);
        switch (sts) {
            case '1000' : 
                 window.open(base_url + 'report/nominative_report/export?file=pdf&status=All', "_blank");
            break;
            case '1' : 
                window.open(base_url + 'report/nominative_report/export?file=pdf&status=Aktif', "_blank");
            break;
            case '2' : 
                window.open(base_url + 'report/nominative_report/export?file=pdf&status=Paid', "_blank");
            break;
            case '3' : 
                window.open(base_url + 'report/nominative_report/export?file=pdf&status=Completed', "_blank");
            break;
        }
    }

    function exportToXls(sts) {
        console.log('status cetak: ' + sts);
        switch (sts) {
            case '1000' : 
                 window.open(base_url + 'report/nominative_report/export?file=xls&status=All', "_blank");
            break;
            case '1' : 
                window.open(base_url + 'report/nominative_report/export?file=xls&status=Aktif', "_blank");
            break;
            case '2' : 
                window.open(base_url + 'report/nominative_report/export?file=xls&status=Paid', "_blank");
            break;
            case '3' : 
                window.open(base_url + 'report/nominative_report/export?file=xls&status=Completed', "_blank");
            break;
        }
    }

    $('#status').on('change', function (v) {
        var status = $(this).val();

        switch (status) {
            case '1000' : 
                window.location.href = base_url + 'report/nominative_report?status=All';
            break;
            case '1' : 
                window.location.href = base_url + 'report/nominative_report?status=Aktif';
            break;
            case '2' : 
                window.location.href = base_url + 'report/nominative_report?status=Paid';
            break;
            case '3' : 
                window.location.href = base_url + 'report/nominative_report?status=Completed';
            break;
        }
    });
</script>

@stop
