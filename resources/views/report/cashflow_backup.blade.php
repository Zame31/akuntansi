@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Cashflow </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <div class="btn-group dropleft">
                        <select class="form-control kt-select2 init-select2" name="idbranch" id="idbranch">
                            <option value="konsol">Konsolidasi</option>
                            @foreach($branch as $item)
                            <option value="{{$item->id}}" @if($item->id==$idbranch) selected @endif>{{$item->branch_name}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Cashflow
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <form class="form-horizontal" id="form-filter" method="GET">
                <div class="form-group row pt-4">
                    {{-- <label class="col-form-label col-12">Pick Start Date and End Date</label> --}}
                    <div class="col-10">
                        <div class="input-daterange input-group init-date" style="width: 400px;float: right;">
                            <input type="text" placeholder="Start Date" @if($start) value="{{$start}}" @endif class="form-control" name="start" />
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="la la-angle-double-right"></i></span>
                            </div>
                            <input type="text" placeholder="End Date" @if($end) value="{{$end}}" @endif class="form-control" name="end" />
                        </div>
                    </div>
                    <div class="col-2">
                        <button type="submit" id="cari" name="cari" value="cari" class="btn btn-success btn-elevate">Submit</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="kt-portlet__body">
            @if($cash)
            <div class="accordion accordion-light" id="accordionExample2">
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title">
                                    Starting Balance
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 >Rp {{number_format($cash[0]->caschflow1,0,',','.')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title text-success">
                                    Cash Inflow
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 class="text-success">Rp {{number_format($cash[0]->caschflow2,0,',','.')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title text-danger">
                                    Cash Outflow
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 class="text-danger">Rp {{number_format($cash[0]->caschflow3,0,',','.')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title ">
                                <button onclick="loadNewPage('{{ route('cashflow.detail') }}')" type="button" class="btn btn-success btn-elevate">Cash Inflow & Outflow Detail</button>

                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title">
                                    Net Cash Change
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 >Rp {{number_format($cash[0]->caschflow4,0,',','.')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Ending Balance
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5 >Rp {{number_format($cash[0]->caschflow5,0,',','.')}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            @else
            <div class="accordion accordion-light" id="accordionExample2">
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title">
                                    Starting Balance
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 >Rp 0</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title text-success">
                                    Cash Inflow
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 class="text-success">Rp 0</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title text-danger">
                                    Cash Outflow
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 class="text-danger">Rp 0</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header zn-card-header">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title ">
                                <button onclick="loadNewPage('{{ route('cashflow.detail') }}')" type="button" class="btn btn-success btn-elevate">Cash Inflow & Outflow Detail</button>

                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header zn-card-header" id="headingOne2">
                        <div class="row">
                            <div class="col-6">
                                <div class="card-title">
                                    Net Cash Change
                                </div>
                            </div>
                            <div class="col-6 text-right p-3">
                                <h5 >Rp 0</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                        <div class="card-header zn-card-header" style="background: #f7f7f7;" id="headingOne2">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-title">
                                        Ending Balance
                                    </div>
                                </div>
                                <div class="col-6 text-right p-3">
                                    <h5 >Rp 0</h5>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            @endif

            

        </div>
    </div>


</div>
<script>
function detail_(id){
	loadingPage();
	window.location.href = base_url +'transaksi_detail?id=' + id;
	endLoadingPage();
}
 $('#idbranch').on('change', function (v) {
        $.ajax({
            type: 'GET',
            url: base_url + '/report/cashflow?id='+this.value,
            beforeSend: function () {
                $("#loading").css('display', 'block');
            },
            success: function (res) {
                //var data = $.parseJSON(res);
                $("#loading").css('display', 'none');
                window.location.href = base_url + 'report/cashflow?id='+$('#idbranch').val();
            }
        });
    });
</script>
@stop
