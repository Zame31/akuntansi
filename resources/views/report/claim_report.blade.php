@section('content')
<style>
    span.select2-container {
        width: 165px !important;
    }
</style>

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Claim Report </a>
            </div>
        </div>

        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <div class="btn-group dropleft">
                    <select class="form-control kt-select2 init-select2" name="filter" id="filter">
                        <option value="1000" @if($sts == "1000") selected @endif>All</option>
                        <option value="1" @if($sts == "1") selected @endif>Claim On Process</option>
                        <option value="2" @if($sts == "2") selected @endif>Claim Accepted</option>
                        <option value="3" @if($sts == "3") selected @endif>Claim Finished</option>
                    </select>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Claim Report
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif onclick="exportData('xls', '{{ $sts }}');" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export Excel </button>
                        <button @if(is_null($data)) disabled="disabled" style="cursor: not-allowed;" @endif  onclick="exportData('pdf', '{{ $sts }}');" type="button" class="btn btn-outline-info">
                                <i class="flaticon2-file"></i> Export PDF </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped- table-hover">
                <thead>
                    <tr>
                        <th class="text-center"> 
                            <h6>No</h6>
                        </th>
                        <th class="text-center">
                            <h6> Branch Code </h6>
                        </th>
                        <th class="text-center">
                            <h6> ID </h6>
                        </th>
                        <th class="text-center">
                            <h6> Polis No </h6>
                        </th>
                        <th class="text-center">
                            <h6> Start Date Claim </h6>
                        </th>
                        <th class="text-center">
                            <h6> Start End Claim</h6>
                        </th>
                        <th class="text-center">
                            <h6> Estimation Of Loss </h6>
                        </th>
                        <th class="text-center">
                            <h6> Claim Amount </h6>
                        </th>
                        <th class="text-center">
                            <h6> Claim Notes </h6>
                        </th>
                        <th class="text-center">
                            <h6> Claim Status </h6>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $total_est_loss = 0;
                        $total_claim_amount = 0;
                        $no = 0;
                    @endphp

                    @if (! is_null($data) )
                        @forelse ($data as $item)
                            @php $no++; @endphp

                            <tr>
                                <td align="center"> {{ $no }} </td>
                                <td align="center"> {{ $short_code }} </td>
                                <td align="center"> {{ $item->id }} </td>
                                <td> {{ $item->polis_no }} </td>
                                <td align="center"> {{ date('d-m-Y', strtotime($item->claim_start_date)) }} </td>
                                <td align="center"> {{ date('d-m-Y', strtotime($item->claim_end_date)) }} </td>
                                <td align="right"> {{ number_format($item->loss_amount, 2, ',', '.') }} </td>
                                <td align="right"> {{ number_format($item->claim_amount, 2, ',', '.') }} </td>
                                <td> {{ $item->claim_notes }} </td>
                                <td> {{ $item->status_definition }} </td>
                            </tr>

                            @php 
                                $total_est_loss += $item->loss_amount;
                                $total_claim_amount += $item->claim_amount;
                            @endphp
                        @empty
                            <tr>
                                <td align="center" colspan="10"> Data tidak ditemukan </td>
                            </tr>
                        @endforelse
                    @else
                        <tr>
                            <td align="center" colspan="10"> Data tidak ditemukan </td>
                        </tr>
                    @endif
                </tbody>
                <tfoot style="background: #f7f7f7;">
                    <tr>
                        <td align="center">
                            <h6 class="text-center">Total</h6>
                        </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="text-right text-success font-weight-bold">{{number_format($total_est_loss,2,',','.')}}</td>
                        <td class="text-right text-success font-weight-bold">{{number_format($total_claim_amount,2,',','.')}}</td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tfoot>
            </table>



        </div>
    </div>


</div>
<script type="text/javascript">
    function exportData(ext, filter) {
        console.log(' extention export data : ' + ext);
        console.log(' filter : ' + filter);

        switch (filter) {
            case '1000' : 
                 window.open(base_url + 'report/claim/export?file=' + ext + '&filter=All', "_blank");
            break;
            case '1' : 
                window.open(base_url + 'report/claim/export?file=' + ext + '&filter=Claim On Process', "_blank");
            break;
            case '2' : 
                window.open(base_url + 'report/claim/export?file=' + ext + '&filter=Claim Accepted', "_blank");
            break;
            case '3' : 
                window.open(base_url + 'report/claim/export?file=' + ext + '&filter=Claim Finished', "_blank");
            break;
        }
    }

    $('#filter').on('change', function (v) {
        var filter = $(this).val();

        switch (filter) {
            case '1000' : 
                window.location.href = base_url + 'report/claim?filter=All';
            break;
            case '1' : 
                window.location.href = base_url + 'report/claim?filter=Claim On Process';
            break;
            case '2' : 
                window.location.href = base_url + 'report/claim?filter=Claim Accepted';
            break;
            case '3' : 
                window.location.href = base_url + 'report/claim?filter=Claim Finished';
            break;
        }

    });
    
</script>
@stop
