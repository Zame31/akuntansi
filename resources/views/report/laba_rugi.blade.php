@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Report </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Profit and Loss </a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                 @if(Auth::user()->user_role_id==6 || Auth::user()->user_role_id==5)
                <div class="btn-group dropleft">
                    <select class="form-control kt-select2 init-select2" name="idbranch" id="idbranch">
                        {{-- <option value="konsol">Konsolidasi</option>
                        @foreach($branch as $item)
                        <option value={{$item->id}} @if($item->id==$idbranch) selected @endif>{{$item->branch_name}}</option>
                        @endforeach --}}

                        {{-- @php dd($idbranch); @endphp --}}

                        @if($idbranch === "konsol")
                            <option value="konsol" selected>Konsolidasi</option>
                            @foreach($branch as $item)
                                <option value="{{$item->id}}">{{$item->branch_name}}</option>
                            @endforeach
                        @else
                            <option value="konsol">Konsolidasi</option>
                            @foreach($branch as $item)
                                <option value="{{$item->id}}" @if($item->id == $idbranch) selected @endif>{{$item->branch_name}}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
                @endif

            </div>
        </div>
    </div>
</div>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Profit and Loss
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                    <button onclick="cetakView()" type="button" class="btn btn-outline-info">
                            <i class="flaticon2-printer"></i> Print & Download</button>
            </div>
        </div>
        <div id="svg" class="kt-portlet__body">
                <div class="row znHeaderShow">
                        <div class="col-12 znHeadCetak" style="margin-top: -10px;">
                            @php
                                $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                            @endphp
                            <div style="display: inline-block;">
                                <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                            </div>
                            {{-- <div style="display: inline-block;">
                                <img alt="Logo" src="{{asset('img/'.$data_imgz->url_image)}}" style="width: 40px;margin-top: -25px;" />
                            </div>
                            <div style="display: inline-block;">
                                <span class="zn-text-logo" style="display: block;">ATA HD</span>
                                <span style="display: block;margin-left: 8px;">"Simple & Inovative Solution" </span>
                            </div> --}}
                            <div class="text-right mt-2" style="float:right;">
                                <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:10px;">Profit and Loss Report</span>
                                <span style="display: block;font-size:10px;">{{date('d M Y')}}</span>
                            </div>
                        </div>

                    </div>
            <div class="row">
                <div class="col-12">

                    <div class="kt-widget6">
                        <div class="kt-widget6__body">
                            @if($laba_rugi)
                            @foreach($laba_rugi as $key => $item)

                                @if($item->balance_type_id==0)

                                    @php
                                     $awal=strlen($item->coa_no);
                                    @endphp
                                    @if($awal==3)
                                    <div class="kt-widget6__item rw{{$key}}" style="background: #f7f7f7;">
                                        <span class=" kt-font-bold znlistTittle">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                        <span></span>
                                        @php
                                            $tot_db=0;
                                        @endphp
                                        @foreach($laba_rugi as $key => $tot)
                                            @php
                                                if($tot->coa_type_id==$item->coa_type_id){
                                                    if(strlen($tot->coa_no)==6){
                                                    $tot_db+=$tot->last_os;
                                                    }
                                                }
                                            @endphp
                                        @endforeach

                                        @php
                                            $number000 = number_format($tot_db,0,',','.');
                                            $number000 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number000);
                                        @endphp

                                        <span class="kt-font-success kt-font-bold">Rp {{$number000}}</span>
                                    </div>
                                    @else
                                    <div class="kt-widget6__item rw{{$key}}">
                                        <span class="znlistTittle"><a href="#" onclick="showHistoryTx('{{ $item->coa_no }}', '{{ $idbranch }}')">{{$item->coa_no}} - {{$item->coa_name}}</a>
                                        </span>
                                        <span></span>
                                        @php
                                            $number00 = number_format($item->last_os,0,',','.');
                                            $number00 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number00);
                                        @endphp

                                        <span class="kt-font-success kt-font-bold">Rp {{$number00}}</span>
                                    </div>
                                    @endif

                                @else

                                    @php
                                    $awal=strlen($item->coa_no);
                                    @endphp
                                    @if($awal==3)
                                    <div class="kt-widget6__item rw{{$key}}" style="background: #f7f7f7;">
                                        <span class=" kt-font-bold znlistTittle">{{$item->coa_no}} - {{$item->coa_name}}
                                        </span>
                                        @php
                                            $tot_kr=0;
                                        @endphp
                                        @foreach($laba_rugi as $tot)

                                            @php
                                                if($tot->coa_type_id==4){
                                                    if(strlen($tot->coa_no)==6){

                                                    $tot_kr+=$tot->last_os;

                                                    }
                                                }
                                            @endphp

                                        @endforeach

                                        @php
                                            $number0 = number_format($tot_kr,0,',','.');
                                            $number0 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number0);
                                        @endphp

                                        <span class="kt-font-danger kt-font-bold text-right">Rp {{$number0}}</span>
                                        <span></span>
                                    </div>
                                    @elseif($awal==6)
                                    <div class="kt-widget6__item rw{{$key}}" style="background: #f7f7f7;">
                                        <span class=" kt-font-bold znlistTittle">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="showHistoryTx('{{ $item->coa_no }}', '{{ $idbranch }}')">{{$item->coa_no}} - {{$item->coa_name}}</a>
                                        </span>
                                        @php
                                            $number = number_format($item->last_os,0,',','.');
                                            $number = preg_replace('/\-([0-9,.]+)/', '(\1)', $number);
                                        @endphp
                                        <span class="kt-font-danger kt-font-bold text-right">Rp {{$number}}</span>
                                        <span></span>
                                    </div>

                                    @else
                                    <div class="kt-widget6__item rw{{$key}}">
                                        <span class="znlistTittle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="showHistoryTx('{{ $item->coa_no }}', '{{ $idbranch }}')">{{$item->coa_no}} - {{$item->coa_name}}</a>
                                        </span>
                                        @php
                                            $number2 = number_format($item->last_os,0,',','.');
                                            $number2 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number2);
                                        @endphp

                                        <span class="kt-font-danger kt-font-bold text-right">Rp {{$number2}}</span>
                                        <span></span>

                                    </div>
                                    @endif


                                @endif
                            @endforeach
                            @endif
                            @php
                            $tot_laba=0;
                            $tot_rugi=0;
                            @endphp
                            @if($laba_rugi)
                            @foreach($laba_rugi as $tot)
                            @php
                            if($tot->coa_type_id==3){
                                if(strlen($tot->coa_no)==6){
                                    $tot_laba+=$tot->last_os;
                                }

                            }else{
                                if(strlen($tot->coa_no)==6){
                                    $tot_rugi+=$tot->last_os;
                                }

                            }
                            @endphp
                            @endforeach
                            @endif

                            <div class="kt-widget6__item" style="background: #f7f7f7;">
                                <span class=" kt-font-bold znlistTittle">Profit and Loss (Before Tax) </span>
                                <span></span>
                                
                                @if($tot_laba - $tot_rugi < 0)

                                @php
                                $number3 = number_format($tot_laba - $tot_rugi,0,',','.');
                                $number3 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number3);
                                @endphp


                                <span class="kt-font-danger kt-font-bold">Rp {{$number3}}</span>
                                @else

                                @php
                                $number3 = number_format($tot_laba - $tot_rugi,0,',','.');
                                $number3 = preg_replace('/\-([0-9,.]+)/', '(\1)', $number3);
                                @endphp


                                <span class="kt-font-success kt-font-bold">Rp {{$number3}}</span>
                                @endif
                            </div>

                            <div class="kt-widget6__item" style="background: #f7f7f7;">
                                <span class=" kt-font-bold znlistTittle"> Pajak </span>
                                <span></span>
                                
                                @php 
                                    
                                    if ( $idbranch == "" ) {
                                        // Tidak ada filter
                                        if ( Auth::user()->user_role_id == 6 || Auth::user()->user_role_id == 5 ) {
                                            // Super Admin dan Admin Pusat
                                            $dataPajak = \DB::table('master_coa')
                                                    ->where('coa_parent_id', 600)
                                                    ->where('company_id', \Auth::user()->company_id)
                                                    ->where('branch_id', \Auth::user()->branch_id)
                                                    ->get();
                                        } else {
                                            $dataPajak = \DB::table('master_coa')
                                                    ->where('coa_parent_id', 600)
                                                    ->where('company_id', \Auth::user()->company_id)
                                                    ->where('branch_id', \Auth::user()->branch_id)
                                                    ->get();
                                        }
                                    } else {

                                        if ( $idbranch == 'konsol' ) {
                                            $dataPajak = \DB::table('master_coa')
                                                    ->where('coa_parent_id', 600)
                                                    ->where('company_id', \Auth::user()->company_id)
                                                    ->get();
                                        } else {
                                            // Filter By Id Branch
                                            $dataPajak = \DB::table('master_coa')
                                                    ->where('coa_parent_id', 600)
                                                    ->where('company_id', \Auth::user()->company_id)
                                                    ->where('branch_id', $idbranch)
                                                    ->get();
                                        }
                                        
                                    }

                                    $total = 0;
                                    foreach ($dataPajak as $item) {
                                        if ( is_null($item->last_os) ) {
                                            $item->last_os = 0;
                                        }

                                        $total += $item->last_os;
                                    }
                                @endphp

                                <span class="kt-font-success kt-font-bold">Rp {{number_format($total, 0, ',', '.')}}</span>
                            </div>

                            <div class="kt-widget6__item" style="background: #f7f7f7;">
                                <span class=" kt-font-bold znlistTittle"> Profit and Loss (After Tax) </span>
                                <span></span>

                                <!-- BARU -->
                                @if($tot_laba - $tot_rugi < 0)

                                    @php
                                        $beforeTax = $tot_laba - $tot_rugi;
                                        $amount = $beforeTax - str_replace('.', '', $total);
                                        $amount = number_format($amount, 0, ',', '.');
                                        $amount = preg_replace('/\-([0-9,.]+)/', '(\1)', $amount);
                                    @endphp

                                    <span class="kt-font-danger kt-font-bold">Rp {{$amount}}</span>
                                @else


                                     @php
                                    
                                        $beforeTax = $tot_laba - $tot_rugi;
                                        $amount = $beforeTax - str_replace('.', '', $total);
                                        $amount = number_format($amount, 0, ',', '.');
                                        $amount = preg_replace('/\-([0-9,.]+)/', '(\1)', $amount);
                                    @endphp


                                    <span class="kt-font-success kt-font-bold">Rp {{$amount}}</span>
                                
                                @endif
                                

                                <!-- SEBELUM UPDATE -->
                                {{-- @php
                                    
                                    $beforeTax = $tot_laba - $tot_rugi;
                                    // $amount = str_replace('.', '', $number3) - str_replace('.', '', $total);
                                    $amount = $beforeTax - str_replace('.', '', $total);
                                    $amount = number_format($amount, 0, ',', '.');
                                @endphp


                                <span class="kt-font-success kt-font-bold">Rp {{$amount}}</span> --}}
                            </div>

                        </div>
                    </div>

                </div>

            </div>
            {{-- <div class="row znSignature" style="margin-top: 30px;">
                    <div class="col-3" style="font-size:10px;color: #595d6e;">
                        User Maker
                        <br><br><br><br><br>
                        <b><u>Zamzam Nur</u></b>
                    </div>
                    <div class="col-3" style="font-size:10px;color: #595d6e;">
                        User Approval
                        <br><br><br><br><br>
                        <b><u>Kamaludin Wak</u></b>
                    </div>
                </div> --}}

        </div>
    </div>


</div>


<div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content"
        {{-- style="border-radius: 0px;border: none;" --}}
        >
                <div class="modal-header">
                        <h5 class="modal-title" id="title_modal">Print & Download Preview Profit and Loss</h5>
                        <button type="button" onclick="closeCetakView()" class="close"></button>
                    </div>
            <div class="modal-body kt-portlet m-0 p-0">
                <div class="">
                    <div class="kt-portlet__body p-0">

                        <iframe id='result' class="scrollStyle" style="width: 100%;height: 520px;border: none;"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#idbranch').on('change', function (v) {
        $.ajax({
            type: 'GET',
            url: base_url + '/report/laba_rugi?id='+this.value,
            beforeSend: function () {
                $("#loading").css('display', 'block');
            },
            success: function (res) {
                //var data = $.parseJSON(res);
                $("#loading").css('display', 'none');
                window.location.href = base_url + 'report/laba_rugi?id='+$('#idbranch').val();
            }
        });
    });
    function showHistoryTx(id, idbranch) {

        if ( idbranch == 'konsol' ) {
            toastr.warning('Konsolidasi tidak dapat melihat history tx');
        } else {
            var url_get=base_url +'ref/master_coa/history_tx/' + id +'?type=laba_rugi' + '&idbranch=' + idbranch;;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: url_get,
                // data: {},
                success: function (data) {
                },
                beforeSend: function () {
                    $('#pageLoad').fadeOut();
                    loadingPage();
                },
            }).done(function (data) {
                endLoadingPage();
                var state = { name: "name", url_get: 'History', url: url_get };
                window.history.replaceState(state, "History", url_get);

                $('#pageLoad').html(data).fadeIn();
                KTBootstrapDatepicker.init();
                $('#table_id').DataTable();

            });
        }
        

    }
// PRINT
    $(".znHeaderShow").css({"display": "none"});
    $(".znSignature").css({"opacity": "0"});

    function closeCetakView() {
        $('#zn-print').modal('hide');

        $(".znHeaderShow").css({"display": "none"});
        $(".znSignature").css({"opacity": "0"});

        $("#svg").removeAttr('style');
        $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "1em", "flex": "1"});
        $(".kt-widget6__item").css({"padding": "8px"});

        $(".rw19").removeAttr('style');
        $(".rw39").removeAttr('style');
        $(".znlistTittle").css({"flex": "1"});


    }
    function cetakView() {
        $(".znHeaderShow").css({"display": "block"});
        $(".znSignature").css({"opacity": "1"});

        $("#svg").css({"padding": "40px", "width": "68%","font-family": "sans-serif"});
        $(".kt-widget6__item").css({"padding": "3px"});
        $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "10px", "flex": "0.5"});

        $(".rw30").css({"margin-top": "140px"});
        // $(".rw39").css({"margin-top": "140px"});

        $(".znlistTittle").css({"flex": "1.5"});

        $('#zn-print').modal('show');
        var pdf = new jsPDF('p', 'pt', 'a4');

        var data = document.getElementById('svg');
        pdf.html(data, {
			callback: function (pdf) {
				var iframe = document.getElementById('result');
				iframe.src = pdf.output('datauristring');
			}
		});
    }
    // ENDPRINT
</script>
@stop
