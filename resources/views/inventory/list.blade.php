@section('content')
<div class="app-content">
<div class="section">


    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Inventory </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                            {{$tittle}} </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            {{$tittle}}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            @if ($type == 'new')  
                                <a href="#" onclick="gActAll('sendApproval','/kirimAprovalInventory?type=semua','{{ route('data.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a>
                                <a href="#" onclick="gActSelected('sendApproval','/kirimAprovalInventory?type=satu','{{ route('data.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a>
                                <a href="#" onclick="gActAll('delete','/hapusAprovalInventory?type=semua','{{ route('data.inventory') }}');" class="btn btn-pill btn-danger" style="color: white;">Delete All</a>
                                <a href="#" onclick="gActSelected('delete','/hapusAprovalInventory?type=satu','{{ route('data.inventory') }}');" class="btn btn-pill btn-danger" style="color: white;">Delete Selected</a>
                                <button onclick="loadNewPage('{{ route('inventory.form','create') }}')" class="btn btn-pill btn-success">Tambah Data</button>
                            @elseif($type == 'approve')
                                <a href="#" onclick="gActAll('giveApproval','/giveAprovalInventory?type=semua','{{ route('data_approve.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval All</a>
                                <a href="#" onclick="gActSelected('giveApproval','/giveAprovalInventory?type=satu','{{ route('data_approve.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Selected</a>
                                <a href="#" onclick="gActAll('reject','/rejectAprovalInventory?type=semua','{{ route('data_approve.inventory') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject All</a>
                                <a href="#" onclick="gActSelected('reject','/rejectAprovalInventory?type=satu','{{ route('data_approve.inventory') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Selected</a>
                            @elseif($type == 'success')
                            <a href="#" onclick="cetakData('all',`inventory`);" class="btn btn-pill btn-danger" style="color: white;">Cetak PDF</a>
                            <a href="#" onclick="cetakDataExcel('all',`inventory`);" class="btn btn-pill btn-success" style="color: white;">Cetak Excel</a>
                            @if (Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 9)
                            
                                <a href="#" onclick="gActAll('delete','/hapusAprovalInventoryRev?type=semua','{{ route('data_success.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete All</a>
                                <a href="#" onclick="gActSelected('delete','/hapusAprovalInventoryRev?type=satu','{{ route('data_success.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete Selected</a>
                            @endif
                            @elseif($type == 'approve_reversal')
                                <a href="#" onclick="gActAll('deleteRev','/giveHapusAprovalInventoryRev?type=semua','{{ route('data_reversal.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete All</a>
                                <a href="#" onclick="gActSelected('deleteRev','/giveHapusAprovalInventoryRev?type=satu','{{ route('data_reversal.inventory') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete Selected</a>
                                <a href="#" onclick="gActAll('reject','/rejectHapusAprovalInventoryRev?type=semua','{{ route('data_reversal.inventory') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete All</a>
                                <a href="#" onclick="gActSelected('reject','/rejectHapusAprovalInventoryRev?type=satu','{{ route('data_reversal.inventory') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete Selected</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label></th>
                                    <th width="30px">Action</th>
                                    <th>Inventory Type</th>
                                    <th>Inventory Desc</th>
                                    <th>Inventory Code</th>
                                    <th>Purchase Amount</th>
                                    <th>Amor Amount</th>
                                    @if ($type == 'success')
                                    <th>Paid Amount</th>
                                    <th>Status Paid</th>
                                    @endif
                                    <th>Other Amount</th>
                                    <th>Branch Code</th>
                                    {{-- <th>last OS</th> --}}
                                    {{-- <th>Status</th> --}}
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    @if ($type == 'success')
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    @endif
                                </tr>
                            </tfoot>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('inventory.action')
@stop
