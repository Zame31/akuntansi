<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;
use DateTime;

class InvoiceController extends Controller
{
    public function invoice_baru(Request $request)
    {
        $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM ref_quotation where is_active=true  order by seq asc');
        $ref_proposal = \DB::select('SELECT * FROM ref_proposal where is_active=true  order by seq asc');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');

        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'invoice.form',$param);
        }else {
            return view('master.master')->nest('child', 'invoice.form',$param);
        }
    }

    public function edit_invoice(Request $request)
    {
       $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM ref_quotation where is_active=true  order by seq asc');
        $ref_proposal = \DB::select('SELECT * FROM ref_proposal where is_active=true  order by seq asc');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.''); $data = \DB::select('SELECT * FROM master_sales where id='.$request->get('id'));

        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;
        $param['data']=$data;


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'invoice.form_edit',$param);
        }else {
            return view('master.master')->nest('child', 'invoice.form_edit',$param);
        }
    }

    public function invoice_list(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id in (1,3) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.list',$param);

    }

    public function invoice_approval(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=2 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;
        return view('master.master')->nest('child', 'invoice.approval',$param);

    }
    public function invoice_approval_payment(Request $request)
    {

        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=5 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.approval_payment',$param);

    }
    public function invoice_paid(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=9 and paid_status_id=1 and is_overdue=false and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.invoice_paid',$param);

    }
    public function invoice_dropped(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=10 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.invoice_dropped',$param);

    }
    public function invoice_active(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=9 and paid_status_id=0 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;


        return view('master.master')->nest('child', 'invoice.invoice_active',$param);

    }

    public function insert_invoice_baru(Request $request){
        $cek_branch = collect(\DB::select("select * from master_branch where id=".Auth::user()->branch_id))->first();

        if($cek_branch->is_open=='true'){
        $results = \DB::select("SELECT max(id) AS id from master_sales");
        $id='';
        if($results){
            $id=$results[0]->id + 1;
        }else{
            $id=1;
        }

        $ref_quotation = \DB::select('SELECT * FROM ref_quotation where id='.$request->input('quotation'));
        $ref_proposal = \DB::select('SELECT * FROM ref_proposal where id='.$request->input('proposal'));
        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

        $ef_config = \DB::select("select value  as jml from master_config where id = 10");
        $ef_agent_config = \DB::select("select value  as jml from master_config where id = 11");
        $ef_company_config = \DB::select("select value  as jml from master_config where id = 12");

        $ef=$ef_config[0]->jml * str_replace('.', '', $request->input('premi')) / 100;
        $ef_agent=$ef_agent_config[0]->jml * $ef / 100;
        $ef_company=$ef_company_config[0]->jml * $ef / 100;

        $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

        $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();

        
        $str=strlen($ref_invoice->next_no);
        if($str==3){
           
           $no_invoice='MKT/'.$d_date->romawi_no.'/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual; 

        }elseif($str==2){
           $no_invoice='MKT/'.$d_date->romawi_no.'/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual; 
        }else{
            $no_invoice='MKT/'.$d_date->romawi_no.'/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual; 
        }

        DB::table('ref_invoice_no')
            ->where('company_id', Auth::user()->company_id)
            ->where('year', date('Y'))
            ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

        

        DB::table('master_sales')->insert(
                [
                    'id' => $id,
                    'inv_date' => $systemDate->current_date,
                    'polis_no' => $request->input('no_polis'),
                    'inv_no' => $no_invoice,
                    'product_id' => $request->input('product'),
                    'ins_amount' => str_replace('.', '', $request->input('insurance')),
                    'disc_amount' => str_replace('.', '', $request->input('disc_amount')),

                    'net_amount' => str_replace('.', '', $request->input('nett_amount')),
                    'agent_id' => $request->input('officer'),

                    'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                    'end_date' => date('Y-m-d',strtotime($request->input('tgl_akhir'))),
                    'afi_acc_no' => $request->input('bank'),
                    'segment_id' => $request->input('segment'),
                    'agent_fee_amount' => str_replace('.', '', $request->input('fee_agent')),

                    'comp_fee_amount' => str_replace('.', '', $request->input('fee_internal')),
                    'premi_amount' => str_replace('.', '', $request->input('premi')),

                    'polis_amount' => str_replace('.', '', $request->input('fee_polis')),
                    'materai_amount' => str_replace('.', '', $request->input('fee_materai')),
                    'admin_amount' => str_replace('.', '', $request->input('fee_admin')),
                    'tax_amount' => str_replace('.', '', $request->input('tax_amount')),
                    'is_tax_company' => $request->input('is_tax_company'),
                    //'due_date_jrn' => $request->input('email'),
                    'paid_status_id' => 0,
                    'qs_no' => $request->input('quotation'),
                    'customer_id' => $request->input('customer'),
                    'qs_date' => $ref_quotation[0]->qs_date,
                    'proposal_no' => $request->input('proposal'),
                    'proposal_date' => $ref_proposal[0]->qs_date,
                    'is_active' => 1,
                    'wf_status_id' => 1,
                    'user_crt_id' => Auth::user()->id,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
                    //'coverage_amount' => Auth::user()->branch_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'ins_fee' => str_replace('.', '', $request->input('asuransi')),
                    'ef' => $ef,
                    'ef_agent' => $ef_agent,
                    'ef_company' => $ef_company,
                ]
            );
            return json_encode(['rc'=>1,'rm'=>'Success']);
        }else{
            return json_encode(['rc'=>2,'rm'=>'Branch Operational is Closed. Transaction is not allowed']);
        }



    }

    public function update_invoice_baru(Request $request){


        $ref_quotation = \DB::select('SELECT * FROM ref_quotation where id='.$request->input('quotation'));
        $ref_proposal = \DB::select('SELECT * FROM ref_proposal where id='.$request->input('proposal'));

        //'inv_date' => date('Y-m-d',strtotime($request->input('tgl_tr'))),

        DB::table('master_sales')
                ->where('id', $request->input('id'))
                ->update([
                    'inv_no' => $request->input('invoice'),
                    'product_id' => $request->input('product'),
                    'ins_amount' => str_replace('.', '', $request->input('insurance')),
                    'disc_amount' => str_replace('.', '', $request->input('disc_amount')),

                    'net_amount' => str_replace('.', '', $request->input('nett_amount')),
                    'agent_id' => $request->input('officer'),

                    'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                    'end_date' => date('Y-m-d',strtotime($request->input('tgl_akhir'))),
                    'afi_acc_no' => $request->input('bank'),
                    'segment_id' => $request->input('segment'),
                    'agent_fee_amount' => str_replace('.', '', $request->input('fee_agent')),

                    'comp_fee_amount' => str_replace('.', '', $request->input('fee_internal')),
                    'premi_amount' => str_replace('.', '', $request->input('premi')),

                    'polis_amount' => str_replace('.', '', $request->input('fee_polis')),
                    'materai_amount' => str_replace('.', '', $request->input('fee_materai')),
                    'admin_amount' => str_replace('.', '', $request->input('fee_admin')),
                    'tax_amount' => str_replace('.', '', $request->input('tax_amount')),
                    'is_tax_company' => $request->input('is_tax_company'),

                    //'due_date_jrn' => $request->input('email'),
                    'paid_status_id' => 0,
                    'qs_no' => $request->input('quotation'),
                    'customer_id' => $request->input('customer'),
                    'qs_date' => $ref_quotation[0]->qs_date,
                    'proposal_no' => $request->input('proposal'),
                    'proposal_date' => $ref_proposal[0]->qs_date,
                    'is_active' => 1,
                    'wf_status_id' => 1,
                    'user_crt_id' => Auth::user()->id,
                    'branch_id' => Auth::user()->branch_id,
                    'company_id' => Auth::user()->company_id,
            ]);
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }

     public function kirimAproval_invoice(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id in(1,3) and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 2]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 2]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')->where('id', '=',$item->id)->delete();

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

              DB::table('master_sales')->where('id', '=',$item)->delete();
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function kirimAproval_invoice1(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);
            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice1(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=2 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 3,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 3,'notes'=>$request->get('cct'),'user_upd_id'=>Auth::user()->id,'updated_at' => date('Y-m-d H:i:s')]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function detail_invoice(Request $request){

        $data = \DB::select('select a.*,b.full_name,c.definition as produk,d.full_name as agent,e.qs_no as quotation,f.qs_no as proposal,g.definition as segment,h.definition as bank, rw.definition as wf_status,mu.fullname as user_approval, mus.fullname as user_create from master_sales a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
                    left join ref_agent d on d.id=a.agent_id
                    left join ref_quotation e on e.id=a.qs_no
                    left join ref_proposal f on f.id=a.proposal_no
                    left join ref_cust_segment g on g.id=a.segment_id
                    left join ref_bank_account h on h.id=a.afi_acc_no
                    left join ref_paid_status rw on rw.id = a.paid_status_id
                    left join master_user mu on mu.id = a.user_approval_id
                    left join master_user mus on mus.id = a.user_crt_id where a.id='.$request->get('id'));
        

        return json_encode($data);
    }


     public function kirimAproval_invoice_active(Request $request){

       if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=9 and paid_status_id=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 5]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 5]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice_active(Request $request){
        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=9 and paid_status_id=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 10]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 10]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

     public function split_paid(Request $request){

         foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 4]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }

     public function drop_selected(Request $request){

         foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 10]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
    }


    public function kirimAproval_app_split(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=4 and paid_status_id=1 and is_overdue=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 13,'notes'=>$request->get('cct'),'is_splitted'=>true,'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 13,'notes'=>$request->get('cct'),'is_splitted'=>true,'user_approval_id'=>Auth::user()->id]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_app_split(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=4 and paid_status_id=1 and is_overdue=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function ini_untuk_overdue_split(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=9 and paid_status_id=0 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            $config = \DB::select("select value from master_config where id=1");
            foreach ($results as $item) {
                $plus="'+".$config[0]->value." days'";
                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($item->inv_date)));




                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));


                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'paid_status_id'=>1,'is_overdue'=>true,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_splitted'=>false]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {
                $results = \DB::select("select * from master_sales where id=".$item);
                $config = \DB::select("select value from master_config where id=1");


                $plus="'+".$config[0]->value." days'";
                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($results[0]->inv_date)));

                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));



               DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'paid_status_id'=>1,'is_overdue'=>true,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_splitted'=>false]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function kirimAproval_invoice_paid(Request $request){

        if($request->get('type')=='semua'){
            $results = \DB::select("select * from master_sales where wf_status_id=5 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($item->inv_date)));
                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($item->inv_date)));




                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));



                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'paid_status_id'=>1,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_overdue'=>false,'user_approval_id'=>Auth::user()->id]);


                $txcode='';
                $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

                $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                $prx=date('Ymd',strtotime($item->inv_date));
                $prx_branch=Auth::user()->branch_id;
                $prx_company=Auth::user()->branch_id;

                if($results){

                  $id_max= $results[0]->max_id;
                  $sort_num = (int) substr($id_max, 1, 4);
                  $sort_num++;
                  $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                  $txcode=$new_code;
                }else{
                    $txcode=$prx.$prx_branch.$prx_company."0001";
                }

                $idmemo='';
                $results_memo = \DB::select("SELECT max(id) AS id from master_memo");
                if($results_memo){
                    $idmemo=$results_memo[0]->id + 1;
                }else{
                    $idmemo=1;
                }

                DB::table('master_memo')->insert(
                    [
                        'id' => $idmemo,
                        'tx_code' => $txcode,
                        'notes' => $request->get('cct'),
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:s:i'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


                $idsegment=$item->segment_id;
                $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=1 and company_id=".Auth::user()->company_id." order by id asc ");

                foreach ($config as $value) {
                        $id='';
                        $results = \DB::select("SELECT max(id) AS id from master_tx");
                        if($results){
                            $id=$results[0]->id + 1;
                        }else{
                            $id=1;
                        }



                        if($value->coa_no){
                            $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");

                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $value->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $item->net_amount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:s:i'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                ]
                            );

                            $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                            if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $item->net_amount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $item->net_amount;
                                }

                            //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->net_amount;

                            /*
                            if($value->tx_type_id==0){
                                $acc_os=$coa1[0]->last_os - $item->net_amount;
                            }else{
                                $acc_os=$coa1[0]->last_os + $item->net_amount;
                            }
                            */

                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('coa_no', $value->coa_no)
                            ->update(['last_os' => $acc_os]);

                          }else{
                            $coa = \DB::select("select b.coa_no from master_sales a
                            join ref_bank_account b on a.afi_acc_no=b.id
                            where a.id=".$item->id);

                            $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$coa[0]->coa_no."'");

                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $coa[0]->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $item->net_amount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:s:i'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                ]
                            );
                            $coa1 = \DB::select("select last_os from master_coa where coa_no='".$coa[0]->coa_no."'");
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                            if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $query[0]->net_amount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $query[0]->net_amount;
                                }

                            //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->net_amount;


                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('coa_no', $coa[0]->coa_no)
                            ->update(['last_os' => $acc_os]);


                    }
                }

                          }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{

            foreach ($request->value as $value) {
                $results = \DB::select("select * from master_sales where branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id." and id=".$value);

                $due_date_jrn = date('Y-m-d', strtotime('+20 days', strtotime($results[0]->inv_date)));




                $tgl1 = date('Y-m-d');  // 1 Oktober 2009
                $tgl2 = $due_date_jrn;  // 10 Oktober 2009
                $selisih = ((abs(strtotime ($tgl1) - strtotime ($tgl2)))/(60*60*24));


               DB::table('master_sales')
                ->where('id', $value)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'paid_status_id'=>1,'due_date_jrn'=>$due_date_jrn,'days_overdue'=>$selisih,'is_overdue'=>false,'user_approval_id'=>Auth::user()->id]);



                foreach ($results as $item) {

                    $txcode='';
                    $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

                    $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                    $prx=date('Ymd',strtotime($item->inv_date));
                    $prx_branch=Auth::user()->branch_id;
                    $prx_company=Auth::user()->branch_id;

                    if($results){

                      $id_max= $results[0]->max_id;
                      $sort_num = (int) substr($id_max, 1, 4);
                      $sort_num++;
                      $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                      $txcode=$new_code;
                    }else{
                        $txcode=$prx.$prx_branch.$prx_company."0001";
                    }

                    $idmemo='';
                    $results_memo = \DB::select("SELECT max(id) AS id from master_memo");
                    if($results_memo){
                        $idmemo=$results_memo[0]->id + 1;
                    }else{
                        $idmemo=1;
                    }

                    DB::table('master_memo')->insert(
                        [
                            'id' => $idmemo,
                            'tx_code' => $txcode,
                            'notes' => $request->get('cct'),
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:s:i'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );


                    $idsegment=$item->segment_id;

                    $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=1 and company_id=".Auth::user()->company_id." order by id asc ");
                      foreach ($config as $value) {
                        $id='';
                        $results = \DB::select("SELECT max(id) AS id from master_tx");
                        if($results){
                            $id=$results[0]->id + 1;
                        }else{
                            $id=1;
                        }

                        if($value->coa_no){
                            $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $value->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $item->net_amount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:s:i'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                ]
                            );

                            $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            if($value->tx_type_id==0){
                                $acc_os=$coa1[0]->last_os - $item->net_amount;
                            }else{
                                $acc_os=$coa1[0]->last_os + $item->net_amount;
                            }

                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $value->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('coa_no', $value->coa_no)
                            ->update(['last_os' => $acc_os]);

                        }else{
                             $coa = \DB::select("select b.coa_no from master_sales a
                            join ref_bank_account b on a.afi_acc_no=b.id
                            where a.id=".$item->id);

                             $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$coa[0]->coa_no."'");
                            DB::table('master_tx')->insert(
                                [
                                    'id' => $id,
                                    'tx_code' => $txcode,
                                    'tx_date' => $tgl_tr,
                                    'tx_type_id' => $value->tx_type_id,
                                    'coa_id' => $coa[0]->coa_no,
                                    'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                                    'tx_amount' => $item->net_amount,
                                    'acc_last' => 0,
                                    'acc_os' => 0,
                                    'tx_segment_id' => $idsegment,
                                    'id_workflow' => 9,
                                    'user_crt_id' => Auth::user()->id,
                                    'branch_id' => Auth::user()->branch_id,
                                    'company_id' => Auth::user()->company_id,
                                    'created_at' => date('Y-m-d H:s:i'),
                                    'coa_type_id' => $ref_coa[0]->coa_type_id,
                                    'type_tx' => 'invoice_payment',
                                    'id_rel' => $item->id,
                                ]
                            );

                            $coa1 = \DB::select("select last_os from master_coa where coa_no='".$coa[0]->coa_no."'");
                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_last' => $coa1[0]->last_os]);

                            if($value->tx_type_id==0){
                                $acc_os=$coa1[0]->last_os - $item->net_amount;
                            }else{
                                $acc_os=$coa1[0]->last_os + $item->net_amount;
                            }

                            DB::table('master_tx')
                            ->where('id', $id)
                            ->where('coa_id', $coa[0]->coa_no)
                            ->update(['acc_os' => $acc_os]);

                            DB::table('master_coa')
                            ->where('coa_no', $coa[0]->coa_no)
                            ->update(['last_os' => $acc_os]);

                        }
                    }

                    }
                }
            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }




    public function hapusAproval_invoice_paid(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=5 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function kirimAproval_invoice_paid1(Request $request){
        if($request->get('type')=='semua'){
            // is_overdue=true
            $results = \DB::select("select * from master_sales where wf_status_id=4 and paid_status_id=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 13,'is_splitted'=>true,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

                $txcode='';
                $tgl_tr=date('Y-m-d',strtotime($item->inv_date));

                $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($item->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                $prx=date('Ymd',strtotime($item->inv_date));
                $prx_branch=Auth::user()->branch_id;
                $prx_company=Auth::user()->branch_id;

                if($results){

                  $id_max= $results[0]->max_id;
                  $sort_num = (int) substr($id_max, 1, 4);
                  $sort_num++;
                  $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                  $txcode=$new_code;
                  }else{
                    $txcode=$prx.$prx_branch.$prx_company."0001";
                }

                $idmemo='';
                $results_memo = \DB::select("SELECT max(id) AS id from master_memo");
                if($results_memo){
                    $idmemo=$results_memo[0]->id + 1;
                }else{
                    $idmemo=1;
                }

                DB::table('master_memo')->insert(
                    [
                        'id' => $idmemo,
                        'tx_code' => $txcode,
                        'notes' => $request->get('cct'),
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:s:i'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


                $idsegment=$item->segment_id;

                if($item->is_tax_company==true){
                    $this->jurnal_split_payment_true($item->id);
                }else{
                    $this->jurnal_split_payment_false($item->id);
                }
            }
        //}
        
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{

            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 13,'is_splitted'=>true,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

                //is_overdue=true
                $query = \DB::select("select * from master_sales where id=".$item." and paid_status_id=1 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);


                $txcode='';
                $tgl_tr=date('Y-m-d',strtotime($query[0]->inv_date));

                $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".date('Ymd',strtotime($query[0]->inv_date))."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                $prx=date('Ymd',strtotime($query[0]->inv_date));
                $prx_branch=Auth::user()->branch_id;
                $prx_company=Auth::user()->branch_id;

                if($results){

                  $id_max= $results[0]->max_id;
                  $sort_num = (int) substr($id_max, 1, 4);
                  $sort_num++;
                  $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
                  $txcode=$new_code;
                  }else{
                    $txcode=$prx.$prx_branch.$prx_company."0001";
                }

                $idmemo='';
                $results_memo = \DB::select("SELECT max(id) AS id from master_memo");
                if($results_memo){
                    $idmemo=$results_memo[0]->id + 1;
                }else{
                    $idmemo=1;
                }

                DB::table('master_memo')->insert(
                    [
                        'id' => $idmemo,
                        'tx_code' => $txcode,
                        'notes' => $request->get('cct'),
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:s:i'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


                $idsegment=$query[0]->segment_id;
                $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and company_id=".Auth::user()->company_id." order by id asc ");
                foreach ($config as $value) {

                    $id='';
                        $results = \DB::select("SELECT max(id) AS id from master_tx");
                        if($results){
                            $id=$results[0]->id + 1;
                        }else{
                            $id=1;
                        }

                        if($value->coa_no){
                            if($value->id==7){
                                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')->insert(
                                    [
                                        'id' => $id,
                                        'tx_code' => $txcode,
                                        'tx_date' => $tgl_tr,
                                        'tx_type_id' => $value->tx_type_id,
                                        'coa_id' => $value->coa_no,
                                        'tx_notes' => $value->tx_notes.' '.$query[0]->inv_no,
                                        'tx_amount' => $query[0]->premi_amount,
                                        'acc_last' => 0,
                                        'acc_os' => 0,
                                        'tx_segment_id' => $idsegment,
                                        'id_workflow' => 9,
                                        'user_crt_id' => Auth::user()->id,
                                        'branch_id' => Auth::user()->branch_id,
                                        'company_id' => Auth::user()->company_id,
                                        'created_at' => date('Y-m-d H:s:i'),
                                        'type_tx' => 'invoice_split',
                                        'id_rel' => $query[0]->id,
                                        'coa_type_id' => $ref_coa[0]->coa_type_id


                                    ]
                                );
                                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_last' => $coa1[0]->last_os]);

                                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                                if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $query[0]->premi_amount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $query[0]->premi_amount;
                                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $query[0]->premi_amount;

                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_os' => $acc_os]);

                                DB::table('master_coa')
                                ->where('coa_no', $value->coa_no)
                                ->update(['last_os' => $acc_os]);
                            }
                            if($value->id==9){
                                $txamount=$query[0]->comp_fee_amount;
                                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')->insert(
                                    [
                                        'id' => $id,
                                        'tx_code' => $txcode,
                                        'tx_date' => $tgl_tr,
                                        'tx_type_id' => $value->tx_type_id,
                                        'coa_id' => $value->coa_no,
                                        'tx_notes' => $value->tx_notes.' '.$query[0]->inv_no,
                                        'tx_amount' => $txamount,
                                        'acc_last' => 0,
                                        'acc_os' => 0,
                                        'tx_segment_id' => $idsegment,
                                        'id_workflow' => 9,
                                        'user_crt_id' => Auth::user()->id,
                                        'branch_id' => Auth::user()->branch_id,
                                        'company_id' => Auth::user()->company_id,
                                        'created_at' => date('Y-m-d H:s:i'),
                                        'type_tx' => 'invoice_split',
                                        'id_rel' => $query[0]->id,
                                        'coa_type_id' => $ref_coa[0]->coa_type_id

                                    ]
                                );
                                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_last' => $coa1[0]->last_os]);

                                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                                if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $txamount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $txamount;
                                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_os' => $acc_os]);

                                DB::table('master_coa')
                                ->where('coa_no', $value->coa_no)
                                ->update(['last_os' => $acc_os]);
                            }
                            if($value->id==5){
                                $txamount=$query[0]->agent_fee_amount;
                                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')->insert(
                                    [
                                        'id' => $id,
                                        'tx_code' => $txcode,
                                        'tx_date' => $tgl_tr,
                                        'tx_type_id' => $value->tx_type_id,
                                        'coa_id' => $value->coa_no,
                                        'tx_notes' => $value->tx_notes.' '.$query[0]->inv_no,
                                        'tx_amount' => $txamount,
                                        'acc_last' => 0,
                                        'acc_os' => 0,
                                        'tx_segment_id' => $idsegment,
                                        'id_workflow' => 9,
                                        'user_crt_id' => Auth::user()->id,
                                        'branch_id' => Auth::user()->branch_id,
                                        'company_id' => Auth::user()->company_id,
                                        'created_at' => date('Y-m-d H:s:i'),
                                        'type_tx' => 'invoice_split',
                                        'id_rel' => $query[0]->id,
                                        'coa_type_id' => $ref_coa[0]->coa_type_id

                                    ]
                                );
                                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_last' => $coa1[0]->last_os]);

                                 $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                                if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $txamount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $txamount;
                                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_os' => $acc_os]);

                                DB::table('master_coa')
                                ->where('coa_no', $value->coa_no)
                                ->update(['last_os' => $acc_os]);
                            }

                            if($value->id==6){
                                $txamount=$query[0]->comp_fee_amount;
                                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')->insert(
                                    [
                                        'id' => $id,
                                        'tx_code' => $txcode,
                                        'tx_date' => $tgl_tr,
                                        'tx_type_id' => $value->tx_type_id,
                                        'coa_id' => $value->coa_no,
                                        'tx_notes' => $value->tx_notes.' '.$query[0]->inv_no,
                                        'tx_amount' => $txamount,
                                        'acc_last' => 0,
                                        'acc_os' => 0,
                                        'tx_segment_id' => $idsegment,
                                        'id_workflow' => 9,
                                        'user_crt_id' => Auth::user()->id,
                                        'branch_id' => Auth::user()->branch_id,
                                        'company_id' => Auth::user()->company_id,
                                        'created_at' => date('Y-m-d H:s:i'),
                                        'type_tx' => 'invoice_split',
                                        'id_rel' => $query[0]->id,
                                        'coa_type_id' => $ref_coa[0]->coa_type_id

                                    ]
                                );
                                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_last' => $coa1[0]->last_os]);

                                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                                if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $txamount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $txamount;
                                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $value->coa_no)
                                ->update(['acc_os' => $acc_os]);

                                DB::table('master_coa')
                                ->where('coa_no', $value->coa_no)
                                ->update(['last_os' => $acc_os]);
                            }

                          }else{

                            $coa = \DB::select("select b.coa_no from master_sales a
                            join ref_bank_account b on a.afi_acc_no=b.id
                            where a.id=".$query[0]->id);

                            if($value->id==8){
                                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$coa[0]->coa_no."'");

                                DB::table('master_tx')->insert(
                                    [
                                        'id' => $id,
                                        'tx_code' => $txcode,
                                        'tx_date' => $tgl_tr,
                                        'tx_type_id' => $value->tx_type_id,
                                        'coa_id' => $coa[0]->coa_no,
                                        'tx_notes' => $value->tx_notes.' '.$query[0]->inv_no,
                                        'tx_amount' => $query[0]->agent_fee_amount,
                                        'acc_last' => 0,
                                        'acc_os' => 0,
                                        'tx_segment_id' => $idsegment,
                                        'id_workflow' => 9,
                                        'user_crt_id' => Auth::user()->id,
                                        'branch_id' => Auth::user()->branch_id,
                                        'company_id' => Auth::user()->company_id,
                                        'created_at' => date('Y-m-d H:s:i'),
                                        'type_tx' => 'invoice_split',
                                        'id_rel' => $query[0]->id,
                                        'coa_type_id' => $ref_coa[0]->coa_type_id
                                    ]
                                );
                                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$coa[0]->coa_no."'");
                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $coa[0]->coa_no)
                                ->update(['acc_last' => $coa1[0]->last_os]);

                                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                                if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $query[0]->agent_fee_amount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $query[0]->agent_fee_amount;
                                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $query[0]->agent_fee_amount;

                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $coa[0]->coa_no)
                                ->update(['acc_os' => $acc_os]);

                                DB::table('master_coa')
                                ->where('coa_no', $coa[0]->coa_no)
                                ->update(['last_os' => $acc_os]);
                            }else{
                                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$coa[0]->coa_no."'");
                                DB::table('master_tx')->insert(
                                    [
                                        'id' => $id,
                                        'tx_code' => $txcode,
                                        'tx_date' => $tgl_tr,
                                        'tx_type_id' => $value->tx_type_id,
                                        'coa_id' => $coa[0]->coa_no,
                                        'tx_notes' => $value->tx_notes.' '.$query[0]->inv_no,
                                        'tx_amount' => $query[0]->premi_amount,
                                        'acc_last' => 0,
                                        'acc_os' => 0,
                                        'tx_segment_id' => $idsegment,
                                        'id_workflow' => 9,
                                        'user_crt_id' => Auth::user()->id,
                                        'branch_id' => Auth::user()->branch_id,
                                        'company_id' => Auth::user()->company_id,
                                        'created_at' => date('Y-m-d H:s:i'),
                                        'type_tx' => 'invoice_split',
                                        'id_rel' => $query[0]->id,
                                        'coa_type_id' => $ref_coa[0]->coa_type_id
                                    ]
                                );

                                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$coa[0]->coa_no."'");
                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $coa[0]->coa_no)
                                ->update(['acc_last' => $coa1[0]->last_os]);

                                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                                if($ref_jurnal[0]->operator_sign=='-'){
                                    $acc_os=$coa1[0]->last_os - $query[0]->premi_amount;
                                }else{
                                    $acc_os=$coa1[0]->last_os + $query[0]->premi_amount;
                                }

                                ///$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $query[0]->premi_amount;

                                DB::table('master_tx')
                                ->where('id', $id)
                                ->where('coa_id', $coa[0]->coa_no)
                                ->update(['acc_os' => $acc_os]);

                                DB::table('master_coa')
                                ->where('coa_no', $coa[0]->coa_no)
                                ->update(['last_os' => $acc_os]);
                            }


                        }
                    }


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function hapusAproval_invoice_paid1(Request $request){
        if($request->get('type')=='semua'){

            $results = \DB::select("select * from master_sales where wf_status_id=4 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

            foreach ($results as $item) {

                DB::table('master_sales')
                ->where('id', $item->id)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($request->value as $item) {

                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9,'notes'=>$request->get('cct'),'user_approval_id'=>Auth::user()->id]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function approval_split_payment(Request $request)
    {

      $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=4 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

       $param['data']=$data;
     return view('master.master')->nest('child', 'invoice.approval_split_payment',$param);

    }


    public function complete_payment(Request $request)
    {

      $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=13 and paid_status_id=1 and is_splitted=true and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

       $param['data']=$data;
     return view('master.master')->nest('child', 'invoice.complete_split',$param);

    }

    public function reversal_payment(Request $request){

            foreach ($request->value as $item) {
               if($request->type=='payment'){
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 14]);
               }else{
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 11]);
               }

            }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }
    public function list_reversal_payment(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=14 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.list_reversal_payment',$param);

    }

    public function list_reversal_split(Request $request)
    {
        $data = \DB::select("select a.id,b.full_name,c.definition,a.premi_amount,d.definition as paid from master_sales a
            left join master_customer b on b.id=a.customer_id
            left join ref_product c on c.id=a.product_id
            left join ref_paid_status d on d.id=a.paid_status_id where a.wf_status_id=11 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);

        $param['data']=$data;

        return view('master.master')->nest('child', 'invoice.list_reversal_split',$param);

    }

    public function kirimAproval_reversal(Request $request){

        if($request->get('type')=='semua'){
            $query = \DB::select("select * from master_sales where wf_status_id=14 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            foreach ($query as $value) {
                 DB::table('master_sales')
                        ->where('id', $value->id)
                        ->update(['wf_status_id' => 10,'paid_status_id'=>1]);

                DB::table('master_tx')
                ->where('id_rel', $value->id)
                ->update(['id_workflow' => 3]);

                $data = \DB::select("select * from master_tx where id_rel='".$value->id."'  and type_tx='invoice_payment'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:s:i'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                        ]
                    );

                }


                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true  and type_tx='reversal_invoice' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            
                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true  and type_tx='invoice_payment' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $item) {

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 3)
                    ->update(['id_workflow' => 12]);

                    DB::table('master_tx')
                    ->where('id', $item->id)
                    ->where('id_workflow', 3)
                    ->update(['is_reversal' => true,'id_workflow' => 15]);

                    DB::table('master_memo')
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 10]);
                    $coa = \DB::select("select last_os from master_coa where coa_no='".$item->coa_id."'");

                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->where('is_reversal', true)
                        ->where('id_workflow', 12)
                        ->update(['acc_last' => $coa[0]->last_os]);

                        $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                        if($ref_jurnal[0]->operator_sign=='-'){
                            $acc_os=$coa[0]->last_os - $item->tx_amount;
                        }else{
                            $acc_os=$coa[0]->last_os + $item->tx_amount;
                        }

                        //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $item->tx_amount;


                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->where('is_reversal', true)
                        ->where('id_workflow', 12)
                        ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$item->tx_notes]);

                        DB::table('master_coa')
                        ->where('coa_no', $item->coa_id)
                        ->update(['last_os' => $acc_os]);

                       
                }


            }

        }else{
            foreach ($request->value as $item) {

                 DB::table('master_sales')
                        ->where('id', $item)
                        ->update(['wf_status_id' => 10,'paid_status_id'=>0]);
                
                DB::table('master_tx')
                ->where('id_rel', $item)
                ->update(['id_workflow' => 3]);

                $data = \DB::select("select * from master_tx where id_rel='".$item."'  and type_tx='invoice_payment'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:s:i'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                        ]
                    );
                }

                DB::table('master_tx')
                ->where('tx_code', $data[0]->tx_code)
                ->where('is_reversal', true)
                ->where('id_workflow', 3)
                ->update(['id_workflow' => 12]);


                 DB::table('master_tx')
                ->where('tx_code', $data[0]->tx_code)
                ->where('id_workflow', 3)
                ->update(['is_reversal' => true,'id_workflow' => 15]);


                DB::table('master_memo')
                ->where('tx_code', $data[0]->tx_code)
                ->update(['id_workflow' => 10]);

                $results = \DB::select("select * from master_tx where tx_code='".$data[0]->tx_code."' and id_workflow=12 and is_reversal=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $dt) {

                    $coa = \DB::select("select last_os from master_coa where coa_no='".$dt->coa_id."'");

                     DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 12)
                    ->update(['acc_last' => $coa[0]->last_os]);

                    $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$dt->coa_type_id."' and tx_type_id=".$dt->tx_type_id);

                    if($ref_jurnal[0]->operator_sign=='-'){
                        $acc_os=$coa[0]->last_os - $dt->tx_amount;
                    }else{
                        $acc_os=$coa[0]->last_os + $dt->tx_amount;
                    }

                    //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $dt->tx_amount;


                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 12)
                    ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$dt->tx_notes]);

                    DB::table('master_coa')
                    ->where('coa_no', $dt->coa_id)
                    ->update(['last_os' => $acc_os]);


                 }   

                    DB::table('master_sales')
                    ->where('id', $item)
                    ->update(['wf_status_id' => 9,'paid_status_id'=>0]);
                 }

            }

        }


//    }


    public function kirimAproval_reversal1(Request $request){

        if($request->get('type')=='semua'){
            $query = \DB::select("select * from master_sales where wf_status_id=11");
            foreach ($query as $value) {
                DB::table('master_tx')
                ->where('id_rel', $value->id)
                ->update(['id_workflow' => 3]);

                $data = \DB::select("select * from master_tx where id_rel='".$value->id."' and type_tx='invoice_split'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:s:i'),
                            'type_tx' => 'reversal_invoice',
                            'coa_type_id' => $dt->coa_type_id,
                        ]
                    );

                }

                $results = \DB::select("select * from master_tx where id_workflow=3 and is_reversal=true and  and type_tx='invoice_split' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $item) {

                    DB::table('master_tx')
                    ->where('tx_code', $item->tx_code)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 3)
                    ->update(['id_workflow' => 12]);

                    DB::table('master_tx')
                    ->where('tx_code', $item->tx_code)
                    ->where('id_workflow', 3)
                    ->update(['is_reversal' => true,'id_workflow' => 15]);

                    DB::table('master_memo')
                    ->where('tx_code', $item->tx_code)
                    ->update(['id_workflow' => 10]);
                    $coa = \DB::select("select last_os from master_coa where coa_no='".$item->coa_id."'");

                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->where('is_reversal', true)
                        ->where('id_workflow', 12)
                        ->update(['acc_last' => $coa[0]->last_os]);

                        $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$item->coa_type_id."' and tx_type_id=".$item->tx_type_id);

                        if($ref_jurnal[0]->operator_sign=='-'){
                            $acc_os=$coa[0]->last_os - $item->tx_amount;
                        }else{
                            $acc_os=$coa[0]->last_os + $item->tx_amount;
                        }

                        //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $item->tx_amount;


                        DB::table('master_tx')
                        ->where('id', $item->id)
                        ->where('coa_id', $item->coa_id)
                        ->where('is_reversal', true)
                        ->where('id_workflow', 12)
                        ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$item->tx_notes]);

                        DB::table('master_coa')
                        ->where('coa_no', $item->coa_id)
                        ->update(['last_os' => $acc_os]);

                        DB::table('master_sales')
                        ->where('id', $value->id)
                        ->update(['wf_status_id' => 9]);
                }


            }

        }else{
            foreach ($request->value as $item) {


                DB::table('master_tx')
                ->where('id_rel', $item)
                ->update(['id_workflow' => 3]);



                $data = \DB::select("select * from master_tx where id_rel='".$item."'  and type_tx='invoice_split'");

                foreach ($data as $dt) {

                    $results = \DB::select("SELECT max(id) AS id from master_tx");
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }

                    if($dt->tx_type_id==0){
                        $type=1;
                    }else{
                        $type=0;
                    }

                    DB::table('master_tx')->insert(
                        [
                            'id' => $id,
                            'tx_code' => $dt->tx_code,
                            'tx_date' => $dt->tx_date,
                            'tx_type_id' => $type,
                            'coa_id' => $dt->coa_id,
                            'tx_notes' => $dt->tx_notes,
                            'tx_amount' => $dt->tx_amount,
                            'acc_last' => 0,
                            'acc_os' => 0,
                            'tx_segment_id' => $dt->tx_segment_id,
                            'id_workflow' => 3,
                            'is_reversal' => true,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            'created_at' => date('Y-m-d H:s:i'),
                            'type_tx' => 'reversal_invoice',
                            'id_rel'=>$dt->id_rel,
                            'coa_type_id' => $dt->coa_type_id,
                        ]
                    );


                }

                DB::table('master_tx')
                ->where('tx_code', $data[0]->tx_code)
                ->where('is_reversal', true)
                ->where('id_workflow', 3)
                ->update(['id_workflow' => 12]);

                /*
                 DB::table('master_tx')
                ->where('tx_code', $data[0]->tx_code)
                ->where('id_workflow', 3)
                ->update(['is_reversal' => true,'id_workflow' => 15]);
                */

                DB::table('master_memo')
                ->where('tx_code', $data[0]->tx_code)
                ->update(['id_workflow' => 10]);

                $results = \DB::select("select * from master_tx where tx_code='".$data[0]->tx_code."' and id_workflow=12 and is_reversal=true and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);

                foreach ($results as $dt) {

                    $coa = \DB::select("select last_os from master_coa where coa_no='".$dt->coa_id."'");

                     DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 12)
                    ->update(['acc_last' => $coa[0]->last_os]);

                    $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$dt->coa_type_id."' and tx_type_id=".$dt->tx_type_id);

                    if($ref_jurnal[0]->operator_sign=='-'){
                        $acc_os=$coa[0]->last_os - $dt->tx_amount;
                    }else{
                        $acc_os=$coa[0]->last_os + $dt->tx_amount;
                    }

                    //$acc_os=$coa[0]->last_os .$ref_jurnal[0]->operator_sign. $dt->tx_amount;


                    DB::table('master_tx')
                    ->where('id', $dt->id)
                    ->where('coa_id', $dt->coa_id)
                    ->where('is_reversal', true)
                    ->where('id_workflow', 12)
                    ->update(['acc_os' => $acc_os,'tx_notes'=>'rev-'.$dt->tx_notes]);

                    DB::table('master_coa')
                    ->where('coa_no', $dt->coa_id)
                    ->update(['last_os' => $acc_os]);

                 }
                 DB::table('master_sales')
                    ->where('id', $item)
                    ->update(['wf_status_id' => 9]);
            }

        }

    }

    public function hapusAproval_reversal(Request $request){

        if($request->get('type')=='semua'){
           $query = \DB::select("select * from master_sales where wf_status_id=14 and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
            foreach ($query as $value) {
                DB::table('master_sales')
                ->where('id', $value->id)
                ->update(['wf_status_id' => 9]);

            }

        }else{
            foreach ($request->value as $item) {
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9]);
            }

        }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }

    public function hapusAproval_reversal1(Request $request){

        if($request->get('type')=='semua'){
            $query = \DB::select("select * from master_sales where wf_status_id=11");
            foreach ($query as $value) {
                DB::table('master_sales')
                ->where('id', $value->id)
                ->update(['wf_status_id' => 9]);

            }

        }else{
            foreach ($request->value as $item) {
                DB::table('master_sales')
                ->where('id', $item)
                ->update(['wf_status_id' => 9]);
            }

        }
        return json_encode(['rc'=>1,'rm'=>'berhasil']);

    }
    function getRomawi($bln){
        switch ($bln){
            case 1: 
            return "I";
            break;
            case 2:
            return "II";
            break;
            case 3:
            return "III";
            break;
            case 4:
            return "IV";
            break;
            case 5:
            return "V";
            break;
            case 6:
            return "VI";
            break;
            case 7:
            return "VII";
            break;
            case 8:
            return "VIII";
            break;
            case 9:
            return "IX";
            break;
            case 10:
            return "X";
            break;
            case 11:
            return "XI";
            break;
            case 12:
            return "XII";
            break;
        }
    }


    function jurnal_split_payment_true($id){

        $item = collect(\DB::select("select * from master_sales where id=".$id))->first();

        
        $config = \DB::select("SELECT * from ref_tx_invoice where invoice_type_id=2 and is_tax_company=true and company_id=".Auth::user()->company_id." order by id asc ");

       foreach ($config as $value) {

        $id='';
        $results = \DB::select("SELECT max(id) AS id from master_tx");
        if($results){
            $id=$results[0]->id + 1;
        }else{
            $id=1;
        }
        if($value->coa_no){
            if($value->id==5){
                $txamount=$item->agent_fee_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:s:i'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id

                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }


                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                ->update(['last_os' => $acc_os]);
            }

            if($value->id==6){
                $txamount=$item->comp_fee_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:s:i'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id


                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                ->update(['last_os' => $acc_os]);
            }
            if($value->id==7){
                $txamount=$item->net_amount - $item->agent_fee_amount - $item->comp_fee_amount + $item->polis_amount + $item->materai_amount;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $tax_amount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:s:i'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $item->premi_amount;
                }else{
                    $acc_os=$coa1[0]->last_os + $item->premi_amount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->premi_amount;


                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                ->update(['last_os' => $acc_os]);
            }
            if($value->id==9){
                $txamount=$item->comp_fee_amount;
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$value->coa_no."'");
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $value->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:s:i'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$value->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);

                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $txamount;
                }else{
                    $acc_os=$coa1[0]->last_os + $txamount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $txamount;


                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $value->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $value->coa_no)
                ->update(['last_os' => $acc_os]);
            }

        }else{

            $coa = \DB::select("select b.coa_no from master_sales a
                join ref_bank_account b on a.afi_acc_no=b.id
                where a.id=".$item->id);

            if($value->id==8){
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$coa[0]->coa_no."'");

                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $coa[0]->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $item->agent_fee_amount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:s:i'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$coa[0]->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);



                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $item->agent_fee_amount;
                }else{
                    $acc_os=$coa1[0]->last_os + $item->agent_fee_amount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->agent_fee_amount;

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $coa[0]->coa_no)
                ->update(['last_os' => $acc_os]);
            }


            if($value->id==10){
                $ref_coa = \DB::select("SELECT coa_type_id from master_coa where coa_no='".$coa[0]->coa_no."'");
                $txamount=$item->net_amount - $item->agent_fee_amount - $item->comp_fee_amount + $item->polis_amount + $item->materai_amount;
                DB::table('master_tx')->insert(
                    [
                        'id' => $id,
                        'tx_code' => $txcode,
                        'tx_date' => $tgl_tr,
                        'tx_type_id' => $value->tx_type_id,
                        'coa_id' => $coa[0]->coa_no,
                        'tx_notes' => $value->tx_notes.' '.$item->inv_no,
                        'tx_amount' => $txamount,
                        'acc_last' => 0,
                        'acc_os' => 0,
                        'tx_segment_id' => $idsegment,
                        'id_workflow' => 9,
                        'user_crt_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'company_id' => Auth::user()->company_id,
                        'created_at' => date('Y-m-d H:s:i'),
                        'type_tx' => 'invoice_split',
                        'id_rel' => $item->id,
                        'coa_type_id' => $ref_coa[0]->coa_type_id
                    ]
                );
                $coa1 = \DB::select("select last_os from master_coa where coa_no='".$coa[0]->coa_no."'");
                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_last' => $coa1[0]->last_os]);



                $ref_jurnal = \DB::select("select operator_sign from ref_jurnal_opr where coa_type_id='".$ref_coa[0]->coa_type_id."' and tx_type_id=".$value->tx_type_id);

                if($ref_jurnal[0]->operator_sign=='-'){
                    $acc_os=$coa1[0]->last_os - $item->agent_fee_amount;
                }else{
                    $acc_os=$coa1[0]->last_os + $item->agent_fee_amount;
                }

                                //$acc_os=$coa1[0]->last_os .$ref_jurnal[0]->operator_sign. $item->agent_fee_amount;

                DB::table('master_tx')
                ->where('id', $id)
                ->where('coa_id', $coa[0]->coa_no)
                ->update(['acc_os' => $acc_os]);

                DB::table('master_coa')
                ->where('coa_no', $coa[0]->coa_no)
                ->update(['last_os' => $acc_os]);
            }

          
        }
    }
}


}
