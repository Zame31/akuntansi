@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Marketing </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    {{$title}} </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    {{$title}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="storePolicy();" class="btn btn-success">Simpan</button>
                        @if ($typeStore == 'edit')
                            <button onclick="loadNewPageSPA('{{ route('marketing.list','policy') }}')" class="btn btn-danger">Kembali</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body" style="background: #fbfbfb;">
        <form id="form-policy" enctype="multipart/form-data">
            <input type="hidden" id="get_id" name="get_id" value="">
            <input type="hidden" name="invoice_type_id" id="invoice_type_id" value="0">
            <div class="row zn-border-bottom mb-5">
               
                
                    @if ($typeStore == 'edit')
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select COC No</label>
                                <select onchange="setCOC(this.value)" disabled class="form-control kt-select2 init-select2" name="coc_no" id="coc_no">
                                    <option disabled value="">Silahkan Pilih</option>
                                    @foreach($master_cf_vehicle as $item)
                                        <option value="{{$item->id}}">{{$item->cf_no}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @else
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select COC No</label>
                                <select onchange="setCOC(this.value)" class="form-control kt-select2 init-select2" name="coc_no" id="coc_no">
                                    <option disabled value="">Silahkan Pilih</option>
                                    @foreach($master_cf_vehicle as $item)
                                        <option value="{{$item->id}}">{{$item->cf_no}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">Silahkan Pilih</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="searchPolicy();" class="btn btn-info" style="margin-top: 25px;">Search</button>
                        </div>
                    
                    @endif
                
                
            </div>
            <div id="dataShow" style="display: none;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Policy Number</label>
                            <input type="text" class="form-control" name="no_polis" id="no_polis" maxlength="80">
                            <div class="invalid-feedback" id="sn">Silahkan Isi No Polis</div>
                            <div class="invalid-feedback" id="mk">Maximal 80 karakter</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Policy Customer Name</label>
                            <input type="text" class="form-control" name="policy_cust_name" id="policy_cust_name" maxlength="100">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Underwriter</label>
                            <select class="form-control zn-readonly" disabled name="id_underwriter" id="id_underwriter">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_underwriter as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
                    </div>
                   
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Customer Name</label>
                            <select class="form-control zn-readonly" disabled name="customer" id="customer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_customer as $item)
                                    <option value="{{$item->id}}">{{$item->id}} | {{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
    
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Number of Interest Insured</label>
                            <input type="text" class="form-control zn-readonly" disabled name="no_of_insured" id="no_of_insured" onkeypress="return hanyaAngka(event)" maxlength="80">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
                    </div>
                </div>
    
                <div class="row zn-border-bottom mb-5">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Product Name</label>
                            <select class="form-control zn-readonly" disabled name="product" id="product" >
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_product as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
                    </div>
                   
                    <div class="col-md-4">
    
                        <div class="form-group">
                            <label>Agent</label>
                            <select class="form-control zn-readonly" disabled name="officer" id="officer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_agent as $item)
                                <option value="{{$item->id}}">{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
                    </div>
    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Valuta ID</label>
                            <select class="form-control zn-readonly" disabled name="valuta_id" id="valuta_id">
                                <option value="" selected disabled>Silahkan Pilih</option>
                                @foreach($ref_valuta as $item)
                                    <option value="{{$item->id}}">{{$item->mata_uang}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
                        
                        
    
    
                    </div>
    
                    <div class="col-md-4">
    
                        <div class="form-group">
                            <label>Start Date Policy</label>
                            <div class="input-group date">
                                <input type="text" class="form-control zn-readonly" disabled placeholder="Pilih Tanggal" name="start_date_police" id="start_date_police" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi Start Date Policy</div>
                            </div>
                        </div>
    
                        <div class="form-group">
                            <!--
                            onkeyup="convertToRupiah1(this)"
                            -->
                            <label>Total Sum Insured</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" disabled class="form-control text-right money" onkeyup="convertToRupiah1(this);" placeholder="0" name="insurance" id="insurance">
                                <div class="invalid-feedback">Silahkan Isi Sum Insured</div>
                                 <input type="hidden" name="total_sum_insured" id="total_sum_insured">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-md-4">
                        
                        <div class="form-group">
                            <label>End Date Policy</label>
                            <div class="input-group date">
                                <input type="text" class="form-control zn-readonly" disabled placeholder="Pilih Tanggal" name="end_date_police" id="end_date_police" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi End Date Policy</div>
                            </div>
                        </div>
    
                     
                       
    
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-4">
                        
                        <div class="form-group">
                            <label>Gross Premium Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input disabled type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this);feeAgentChange();"  name="premi" id="premi">
                            </div>
                            <div class="invalid-feedback" id="pa">Silahkan Isi Premi Amount</div>
                            <div class="invalid-feedback" id="pa1">Premi Amount tidak boleh melebihi Sum Insured</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Discount Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input disabled type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this);feeAgentChange();"  name="disc_amount" id="disc_amount">
                                <div class="invalid-feedback" id="sd">Silahkan Isi Disc Amount</div>
                                <div class="invalid-feedback" id="mi">Disc Amount tidak boleh melebihi Premi Amount</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>EF</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text" style="
                                    background: #fd2762;
                                    color: #fff;
                                    font-weight: bold;
                                ">%</span></div>
                                <input type="text" class="form-control text-right money" onkeyup="setEF();feeAgentChange();" maxlength="6" placeholder="0" name="ef_pct" id="ef_pct">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>EF Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" onkeyup="feeAgentChange()" disabled class="form-control text-right money" placeholder="0" name="ef" id="ef">
                            </div>
                        </div>
                    </div>
                </div>

    
                <div class="row zn-border-bottom mb-5">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                @if ($data_comp->app_name == "ATA HD")
                                Commission Due to HD Soeryo
                                @else
                                Commission Due to Us
                                @endif
                               </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money"  placeholder="0" id="fee_internal" name="fee_internal">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Policy Duty</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" maxlength="11" placeholder="0" name="fee_polis" id="fee_polis" value="{{number_format(0,2,",",".")}}">
                                <div class="invalid-feedback">Silahkan Isi Fee Polis</div>
                            </div>
                        </div>
    
    
    
                        <div class="form-group">
                            <label>Nett Premium</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text"  disabled class="form-control text-right money zn-readonly" placeholder="0" name="nett_amount" id="nett_amount">
                                </div>
                        </div>
    
    
    
    
                    </div>
                    <div class="col-md-4">
    
                        <div class="form-group">
                            <label>Commission Due to Agent</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text"  onkeyup="feeAgentChange()" class="form-control text-right money" placeholder="0" name="fee_agent" id="fee_agent">
                                </div>
                        </div>
    
                        <div class="form-group">
                            <label>Stamp Duty</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                {{-- <input type="text" class="form-control text-right money"  maxlength="11" placeholder="0" name="fee_materai" id="fee_materai" value="6.000"> --}}
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" maxlength="11" placeholder="0" name="fee_materai" id="fee_materai" value="{{number_format(0,2,",",".")}}">
                                <div class="invalid-feedback">Silahkan Isi Fee Materai</div>
                            </div>
                        </div>  
    
    
                        <div class="form-group">
                            <label>Nett To Underwriter</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" disabled class="form-control text-right money zn-readonly"  placeholder="0" id="asuransi" name="asuransi">
                            </div>
                        </div>
    
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Agent Tax Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text"  class="form-control text-right money" placeholder="0" id="tax_amount" name="tax_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                @if ($data_comp->app_name == "ATA HD")
                                HD Soeryo Duty
                                @else
                                Admin Duty
                                @endif
                                </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" placeholder="0" name="fee_admin" id="fee_admin" value="{{number_format(0,2,",",".")}}">
                                <div class="invalid-feedback">Silahkan Isi Fee Admin</div>
                            </div>
                        </div>
    
    
    
                    </div>
                </div>
                <div class="row isRealisasi">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Realisasi Sum Insured</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text" value="0" disabled class="form-control text-right money zn-readonly" placeholder="0" name="realisasi_sum_insured" id="realisasi_sum_insured">
                                </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Realisasi Premium</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text" value="0" disabled class="form-control text-right money zn-readonly" placeholder="0" name="realisasi_premium" id="realisasi_premium">
                                </div>
                        </div>
                    </div>
                </div>

                <div class="row isRealisasi">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Selisih Sum Insured</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text" value="0" disabled class="form-control text-right money zn-readonly" placeholder="0" name="selisih_realisasi_sum_insured" id="selisih_realisasi_sum_insured">
                                </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Selisih Premium</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text" value="0" disabled class="form-control text-right money zn-readonly" placeholder="0" name="selisih_realisasi_premium" id="selisih_realisasi_premium">
                                </div>
                        </div>
                    </div>
                </div>

               
                <div id="view_detail_product" class="row">
                    <div class="col-md-12">
                        <div>
                            <h5 class="mb-4">
                                <i class="flaticon-grid-menu mr-2"></i>
                                Detail Product
                            </h5>
                        </div>
                       
                    </div>
                    <div class="col-md-12">
                        <div class="container-detail-vehicle bg-white" style="padding: 20px;border-radius: 4px;border: 2px solid #f7f7f7;margin-bottom: 50px;">
                            <div class="row m-0">
                                <div class="col-md-8">
                                    <h4 class="kt-section__title kt-section__title-lg">
                                        <button class="btn btn-sm btn-outline-info" onclick="modal_add_detail('insert')"> Tambah Detail </button>
                                        <span class="isRealisasi">
                                            <button class="btn btn-sm btn-outline-warning" onclick="modal_add_detail_realisasi('insert')"> Tambah Detail Realisasi</button>

                                        </span>
                                        <button type="button" class="btn btn-sm btn-success" onclick="modal_import_template()"> Import Data Excel </button>
                                        <button type="button" class="btn btn-sm btn-info" onclick="export_excel_detail()"> Export Excel Table Detail </button>
                                        <button type="button" class="btn btn-sm btn-danger" onclick="modal_download_template()"> Download Template Excel </button>
                                    </h4>
                                </div>
                                {{-- <div class="col-md-4" style="
                                        border: 1px dashed #f7f7f7;
                                        padding: 10px 20px;
                                        background: #f9f9f9;
                                    ">
                                    <div>Status ID : </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div>0 - Non Aktif</div>
                                            <div>1 - Aktif</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div>2 - Endors Aktif</div>
                                            <div>3 - Endors Baru</div>
                                        </div>
                                    </div>
                                  
                                        
                                </div> --}}
                            </div>
                            
                            

                            <div class="table-responsive scrollStyle-v" style="padding: 10px;margin-top:20px;">
                                <div id="tableDiv"></div>
                            </div>
                          


                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button onclick="storePolicy();" class="btn btn-success">Simpan</button>
                    </div>
                </div>
        </div>
    </div>
</div>
    <script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
    @include('marketing.action.act_global')
    @include('marketing.action.global')
    @include('marketing.action.act_policy')
    @include('marketing.modal.modal_search_policy')
    @include('marketing.modal.modal_detail_product')
    @include('quotation_slip.modal.template_excel')
    @include('quotation_slip.modal.import_excel')
@stop
