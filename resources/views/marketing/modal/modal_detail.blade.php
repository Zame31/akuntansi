<div class="modal fade in" id="detail" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zn-invoice-tittle">Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body p-5">

                <ul class="nav nav-tabs nav-tabs-line">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1">
                            <span class="nav-icon"><i class="flaticon-interface-3"></i></span>
                            <span class="nav-text">Detail Data</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_2">
                            <span class="nav-icon"><i class="flaticon-list-2"></i></span>
                            <span class="nav-text">Detail Product</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content mt-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel"
                        aria-labelledby="kt_tab_pane_2">

                        <div class="row" style="
                            border-bottom: 1px dashed #d9d9d9;
                            margin-bottom: 20px;
                        ">
                            <div id="v_cf_no" class="col-md-3">
                                <div class="form-group">
                                    <label>COC No</label>
                                    <h5 id="cf_no">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Policy Number</label>
                                    <h5 id="polis_no">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Policy Customer Name</label>
                                    <h5 id="police_name">-</h5>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="
                            border-bottom: 1px dashed #d9d9d9;
                            margin-bottom: 20px;
                        ">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Underwriter</label>
                                    <h5 id="underwriter">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Customer Name</label>
                                    <h5 id="customer">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Number of Interest Insured</label>
                                    <h5 id="no_of_insured">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <h5 id="product">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Agent</label>
                                    <h5 id="agent">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Valuta ID</label>
                                    <h5 id="mata_uang">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Start Date Policy</label>
                                    <h5 id="start_date">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>End Date Policy</label>
                                    <h5 id="end_date">-</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="
                            border-bottom: 1px dashed #d9d9d9;
                            margin-bottom: 20px;
                        ">


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Total Sum Insured</label>
                                    <h5 id="ins_amount">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Gross Premium Amount</label>
                                    <h5 id="premi_amount1">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Discount Amount</label>
                                    <h5 id="disc_amount">-</h5>
                                </div>
                            </div>
                            <div id="v_fee_internal" class="col-md-3">
                                <div class="form-group">
                                    <label>Commission Due to HD Soeryo</label>
                                    <h5 id="fee_internal">-</h5>
                                </div>
                            </div>
                            <div id="v_agent_fee_amount" class="col-md-3">
                                <div class="form-group">
                                    <label>Commission Due to Agent</label>
                                    <h5 id="agent_fee_amount">-</h5>
                                </div>
                            </div>
                            <div id="v_tax_amount" class="col-md-3">
                                <div class="form-group">
                                    <label>Agent Tax Amount</label>
                                    <h5 id="tax_amount">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label id="label_police_duty">Policy Duty</label>
                                    <h5 id="police_duty">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label id="label_stamp_duty">Stamp Duty</label>
                                    <h5 id="stamp_duty">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label id="label_adm_duty">
                                        @php
                                        $data_comp = collect(\DB::select("SELECT * from master_company where id =
                                        1"))->first();
                                        @endphp
                                        @if ($data_comp->app_name == "ATA HD")
                                        HD Soeryo Duty
                                        @else
                                        Admin Duty
                                        @endif</label>
                                    <h5 id="adm_duty">-</h5>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nett Premium</label>
                                    <h5 id="net_amount">-</h5>
                                </div>
                            </div>
                            <div id="v_ins_fee" class="col-md-3">
                                <div class="form-group">
                                    <label>Nett To Underwriter</label>
                                    <h5 id="ins_fee">-</h5>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                        <div class="table-responsive scrollStyle-v" style="padding: 10px;">
                            <div id="tableDiv"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade in" id="realisasi_produk" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zn-invoice-tittle">Realisasi Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body p-5">
                <input type="hidden" value="" id="realisasi_product_id">
                <input type="hidden" value="" id="realisasi_doc_id">

                {{-- <div class="row" style="border-bottom: 1px dashed #d9d9d9;margin-bottom: 20px;">
                     <div class="col-md-3">
                        <div class="form-group">
                            <label>Product Name</label>
                            <h5 id="realisasi_product">-</h5>
                        </div>
                    </div>
                   
                </div> --}}

                <div class="row" style="border-bottom: 1px dashed #d9d9d9;margin-bottom: 20px;">
                   
                   <div class="col-md-4">
                       <div class="form-group">
                           <label>Total Sum Insured</label>
                           <h5 id="realisasi_ins_amount">-</h5>
                       </div>
                   </div>
                   <div class="col-md-4">
                        <div class="form-group">
                            <label>Realisasi Sum Insured</label>
                            <h5 id="realisasi_sum_insured">-</h5>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Selisih Realisasi Sum Insured</label>
                            <h5 class="text-success" id="selisih_realisasi_sum_insured">-</h5>
                        </div>
                    </div>
                   <div class="col-md-4">
                       <div class="form-group">
                           <label>Gross Premium Amount</label>
                           <h5 id="realisasi_premi_amount1">-</h5>
                       </div>
                   </div>
                   <div class="col-md-4">
                        <div class="form-group">
                            <label>Realisasi Premium</label>
                            <h5 id="realisasi_premium">-</h5>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Selisih Realisasi Premium</label>
                            <h5 class="text-success" id="selisih_realisasi_premium">-</h5>
                        </div>
                    </div>
                   
               </div>

                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-sm btn-success mb-3 btn-pill" onclick="modal_tambah_realisasi('insert')"> Tambah Detail Realisasi</button>
                        <button id="btn_create_endors" class="btn btn-sm btn-danger mb-3 btn-pill"> Buat Endorsment</button>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive scrollStyle-v" style="padding: 10px;height: 300px;">
                            <div id="tableDivRealisasi"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="detail_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title-detail"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form id="form-detail-realisasi">
                @csrf
                <div class="modal-body kt-scroll" style="height: 400px; overflow: auto;">
                    <div class="form_content"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="store_tambah_realisasi()" id="btn-detail"><i class="la la-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>