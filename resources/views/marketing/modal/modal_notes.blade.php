<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal-notes" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body">
                     <table class="table table-striped- table-hover table-checkable" id="notes_table">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Approval</th>
                                <th>Date</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                    </table>  
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>