<!--begin::Modal-->
<div class="modal fade" id="detail_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title-detail"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form id="form-detail">
                @csrf
                <div class="modal-body kt-scroll" style="height: 400px; overflow: auto;">
                    <div class="form_content"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info" onclick="save_detail('insert')" id="btn-detail"><i class="la la-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
