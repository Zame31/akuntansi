<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="border: none;">

            <div class="modal-body p-0" id="svg">
                <input type="hidden" id="zn_stat" value="" name="zn_stat">
                <input type="hidden" id="get_id" value="" name="get_id">
                <input type="hidden" id="zn_paid_stat" value="" name="zn_paid_stat">

                <div class="kt-invoice-1">
                    {{-- style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});" --}}
                    <div class="kt-invoice__head" style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                        <div class="kt-invoice__container">
                            <div class="text-right mb-5">
                                {{-- <button onclick="cetakViewInvoice()" type="button"
                                    class="znBtnShow btn btn-danger btn-hover-info btn-pill">
                                    <i class="flaticon2-printer"></i> Print & Download </button>

                                    <a id="setWord" href="#" class="znBtnShow btn btn-info btn-hover-success btn-pill">Export Word</a> --}}

                                <button data-dismiss="modal" aria-label="Close" type="button"
                                    class="znBtnShow btn btn-light btn-hover-info btn-pill">Kembali</button>
                            </div>
                            <div class="kt-invoice__brand">
                                <div class="row znHeaderShow" style="width: 100%;margin-bottom:10px;">
                                    <div class="col-12" style="margin-top: -10px;">

                                        <div style="display: inline-block;">
                                            <span id="zn-invoice-tittle" class="zn-text-logo"
                                                style="display: block;color:#ffffff;font-weight: bold;font-size: 20px;">Master
                                                Policy</span>
                                            <span id="wf_status"
                                                style="display: block;margin-left: 8px;color:#ffffff;font-size:10px;"></span>
                                        </div>
                                    </div>
                                </div>
                                {{-- <h1 class="kt-invoice__title" style="font-size:1.5rem;">INVOICE</h1> --}}
                                <div href="#" class="kt-invoice__logo">

                                    {{-- <a href="#" onclick="kembali();" class="btn btn-pill btn-primary">Kembali</a> --}}
                                </div>
                            </div>
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle"><b>MASTER POLICY DATE.</b></span>
                                    <span class="kt-invoice__text" id="tgl_tr"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">MASTER POLICY NO.</span>
                                    <span class="kt-invoice__text" id="inv_no"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">PRODUCT NAME.</span>
                                    <span class="kt-invoice__text" id="product"></span>
                                </div>
                            </div>
                            <div id="insView" class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">ACCOUNT OFFICER.</span>
                                    <span class="kt-invoice__text" id="agent"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">START DATE POLICY.</span>
                                    <span class="kt-invoice__text" id="start_date"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">END DATE POLICY.</span>
                                    <span class="kt-invoice__text" id="end_date"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-invoice__body">
                        <div class="kt-invoice__container">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>DESCRIPTION</th>
                                            <th>
                                                <div id="tittle_lang"></div>
                                            </th>
                                            <th>IDR AMOUNT </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="ins_amount_visible">
                                            <td>Total Sum Insured</td>
                                            <td>
                                                <div id="ins_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="ins_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gross Premium </td>
                                            <td>
                                                <div id="premi_amount1_lang"></div>
                                            </td>
                                            <td>
                                                <div id="premi_amount1"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Discount</td>
                                            <td>
                                                <div id="disc_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="disc_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nett Premium</td>
                                            <td>
                                                <div id="net_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="net_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Agent Fee</td>
                                            <td>
                                                <div id="agent_fee_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="agent_fee_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Stamp Duty</td>
                                            <td>
                                                <div id="stamp_duty_lang"></div>
                                            </td>
                                            <td>
                                                <div id="stamp_duty"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Police Duty</td>
                                            <td>
                                                <div id="police_duty_lang"></div>
                                            </td>
                                            <td>
                                                <div id="police_duty"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Comm due to Us to HD</td>
                                            <td>
                                                <div id="comp_fee_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="comp_fee_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>HD Soeryo Duty</td>
                                            <td>
                                                <div id="adm_duty_lang"></div>
                                            </td>
                                            <td>
                                                <div id="adm_duty"></div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="kt-invoice__footer">
                        <div class="kt-invoice__container">
                            {{-- <div class="kt-invoice__bank">
                                <div class="kt-invoice__title"> <b>BANK TRANSFER</b></div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__label">Account Name:</span>
                                    <span class="kt-invoice__value" id="bank"></span></span>
                                </div>
                            </div> --}}
                            <div class="kt-invoice__total">
                                <span style="font-size:14px;" class="kt-invoice__title"><b
                                        id="zn-tittle-premi-lang"></b></span>
                                <span class="kt-invoice__price" style="font-size:16px;color:#595d6e;"
                                    id="premi_amount_lang"></span>
                            </div>
                            <div class="kt-invoice__total">
                                <span id="zn-tittle-premi" style="font-size:14px;" class="kt-invoice__title"><b>GROSS
                                        PREMIUM</b></span>
                                <span class="kt-invoice__price" style="font-size:16px;" id="premi_amount"></span>
                            </div>
                            <div class="row znSignature" style="margin-top: 40px;width: 100%;display:none;">
                                <div class="col-6" style="font-size:10px;color: #595d6e;">
                                    User Maker
                                    <br><br><br><br><br>
                                    <b><u id="user_maker"></u></b>
                                </div>
                                <div class="col-6" style="font-size:10px;color: #595d6e;">
                                    User Approval
                                    <br><br><br><br><br>
                                    <b><u id="user_approval"></u></b>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content" {{-- style="border-radius: 0px;border: none;" --}}>
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Print & Download Preview Invoice</h5>
                <button type="button" onclick="closeCetakViewInvoice()" class="close"></button>
            </div>
            <div class="modal-body kt-portlet m-0 p-0">
                <div class="">
                    <div class="kt-portlet__body p-0">

                        <iframe id='znresult' class="scrollStyle"
                            style="width: 100%;height: 520px;border: none;"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
