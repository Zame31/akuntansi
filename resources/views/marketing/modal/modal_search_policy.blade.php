<div class="modal fade in" id="modal-search-policy" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_seach">List Data Master Vechile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                 <table class="table table-striped- table-hover table-checkable" id="search_policy_table">
                    <thead>
                        <tr>
                            <th class="all">ID</th>
                            <th class="all">Customer Name</th>
                            <th class="all">Underwriter</th>
                            <th class="none">Number of Interest Insured</th>
                            <th class="all">Product Name</th>
                            <th class="all">Agent</th>
                            <th class="none">Valuta ID</th>
                            <th class="none">Start Date Policy</th>
                            <th class="none">End Date Policy</th>
                            <th class="all">Total Sum Insured</th>
                            {{-- <th class="none">Gross Premium</th>
                            <th class="none">Nett Premium</th> --}}
                            {{-- <th class="none">No Proposal</th> --}}

                            <th>Act</th>
                        </tr>
                    </thead>
                </table>  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>