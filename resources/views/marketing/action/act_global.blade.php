
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script>
    var id_array_data = 0;
    var data_detail_product = [];
    var typeProduct = '';
    var tmpHeadProduct = [];
    
function detail_invoice(id,type){
   loadingPage();
   switch (type) {

    //    ORIGINAL
        case 0:
            $('#zn-invoice-tittle').html('DETAIL MASTER POLICY');
            $('#v_fee_internal').show();
            $('#v_agent_fee_amount').show();
            $('#v_tax_amount').show();
            $('#v_ins_fee').show();
            $('#v_cf_no').show();
            var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
        break;
     //    ENDORS
     case 1:
            $('#zn-invoice-tittle').html('DETAIL MASTER POLICY ENDORSMENT');
            $('#v_fee_internal').show();
            $('#v_agent_fee_amount').show();
            $('#v_tax_amount').show();
            $('#v_ins_fee').show();
            $('#v_cf_no').hide();
            var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
        break;
        case 'installment':
            $('#zn-invoice-tittle').html('DETAIL MASTER POLICY INSTALLMENT');
            $('#v_fee_internal').hide();
            $('#v_agent_fee_amount').hide();
            $('#v_tax_amount').hide();
            $('#v_ins_fee').hide();
            $('#v_cf_no').show();
            $('#label_police_duty').html('Polis Amount');
            $('#label_stamp_duty').html('Materai Amount');
            $('#label_adm_duty').html('Admin Amount');
            var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
        break;
        case 'installment_detail':
            $('#zn-invoice-tittle').html('DETAIL MASTER POLICY INSTALLMENT SCHEDULE');
            $('#v_fee_internal').hide();
            $('#v_agent_fee_amount').hide();
            $('#v_tax_amount').hide();
            $('#v_ins_fee').hide();
            $('#v_cf_no').show();
            $('#label_police_duty').html('Polis Amount');
            $('#label_stamp_duty').html('Materai Amount');
            $('#label_adm_duty').html('Admin Amount');
            var set_url = base_url + 'marketing/data/detail_master_sales_schedule_polis/' + id;
        break;
    }
    
   $.ajax({
       type: 'GET',
       url: set_url,
       success: function (res) {
            let data = res.rm;
            console.log(data);
           endLoadingPage();
           
           $('#cf_no').html(data.cf_no);
           $('#polis_no').html(data.polis_no);
           $('#police_name').html(data.police_name);
           $('#underwriter').html(data.underwriter);
           $('#customer').html(data.customer);
           $('#no_of_insured').html(data.no_of_insured);
           $('#mata_uang').html(data.mata_uang);
           $('#coc_no').html(data.cf_no);
           $('#coc_no').html(data.cf_no);
           
            $('#product').html(data.produk);
            $('#agent').html(data.agent);
            $('#start_date').html(formatDate(new Date(data.start_date_polis)));
            $('#end_date').html(formatDate(new Date(data.end_date_polis)));

            // NORMAL
            $('#ins_amount').html(formatMoney(data.ins_amount));
            $('#premi_amount1').html(formatMoney(data.premi_amount));
            $('#disc_amount').html(formatMoney(data.disc_amount));
            $('#net_amount').html(formatMoney(data.net_amount));
            $('#agent_fee_amount').html(formatMoney(data.agent_fee_amount));
            $('#tax_amount').html(formatMoney(data.tax_amount));
            $('#premi_amount').html(formatMoney(data.net_amount));
            $('#fee_internal').html(formatMoney(data.comp_fee_amount));
            
            $('#ins_fee').html(formatMoney(data.ins_fee));
            // NEW
            $('#stamp_duty').html(formatMoney(data.materai_amount));
            $('#police_duty').html(formatMoney(data.polis_amount));
            $('#adm_duty').html(formatMoney(data.admin_amount));
            // NEW

            detail_invoice_product(data.product_id,data.id);
           
           $('#detail').modal('show');
       }
   });


}

function realisasi_product(id,type){

    loadingPage();
    $('#btn_create_endors').hide();

    var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;

    $.ajax({
       type: 'GET',
       url: set_url,
       success: function (res) {
            let data = res.rm;
            console.log(data);
           endLoadingPage();
        
            $('#realisasi_product').html(data.produk);
         
            // NORMAL
            $('#realisasi_ins_amount').html(formatMoney(data.ins_amount));
            $('#realisasi_premi_amount1').html(formatMoney(data.premi_amount));
            $('#realisasi_sum_insured').html(formatMoney(data.realisasi_sum_insured));
            $('#realisasi_premium').html(formatMoney(data.realisasi_premium));

            $('#selisih_realisasi_sum_insured').html(formatMoney(Math.abs(data.ins_amount-data.realisasi_sum_insured)));
            $('#selisih_realisasi_premium').html(formatMoney(Math.abs(data.premi_amount-data.realisasi_premium)));

            $('#realisasi_product_id').val(data.product_id);
            $('#realisasi_doc_id').val(data.id);

            if( (parseFloat(data.realisasi_sum_insured) > parseFloat(data.ins_amount)) && 
                (parseFloat(data.realisasi_premium) > parseFloat(data.premi_amount)) &&
                ('{{date("Y-m-d")}}' > data.end_date_polis) ){
                    $('#btn_create_endors').show();

                    $("#btn_create_endors").click(function(){ 
                        create_endors(data.polis_no); 
                        });

               }else{
                    $('#btn_create_endors').hide();
               }

            detail_invoice_product_realisasi(data.product_id,data.id);
            $('#realisasi_produk').modal('show');
       }
   });

   
}

function create_endors(polis_no){
    $('#realisasi_produk').modal('hide');
    loadPageMenuSPA('{{ route('marketing.create','policy_endorsment') }}?get_polis_no='+polis_no,'s-803');
}

function initMaskMoney() {
    $(".money").maskMoney({
        prefix:'',
        allowNegative: false,
        thousands:'.',
        decimal:',',
        affixesStay: false,
        // precision: 0 // Tidak ada 2 digit dibelakang koma
    });
}


function detail_invoice_product(prod_id,id_sales) {
       

        $.ajax({
            type: "GET",
            url: base_url + '/quotation_slip/get_detail_insured/' + prod_id,
            data: {},

            beforeSend: function () {
                // loadingPage();
            },

            success: function (res) {
                var data = res.data;

                console.log('data detail product',data);


                if (res.rc == 1) {
                    var tableHeaders = "";
                    tableHeaders = `<th class="text-center"> Status </th>`;
                    data.forEach(function ( value, index) {
                        let isShow = (value.is_showing) ? 'all':'none';
                        
                        tableHeaders += `<th class="${isShow} text-center">${value.label}</th>`;
                    })
                    $("#tableDiv").empty();

                    if (data.length > 0) {
                        $("#tableDiv").append('<table id="table_detail" class="table table-striped- table-hover table-checkable"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                        DataTable = $('#table_detail').DataTable({responsive: true});


                        if (id_sales) {
                            getDetailProduct(id_sales,'detail_product_list');
                        }
                    }

                 
                    
                  
                } else {
                    toastr.error(data.rm);
                }
            }
        }).done(function (msg) {
            // endLoadingPage();
        }).fail(function (msg) {
            // endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
}


function detail_invoice_product_realisasi(prod_id,id_sales) {
       

       $.ajax({
           type: "GET",
           url: base_url + '/quotation_slip/get_detail_insured/' + prod_id,
           data: {},

           beforeSend: function () {
               // loadingPage();
           },

           success: function (res) {
               var data = res.data;

               console.log('data detail product',data);


               if (res.rc == 1) {
                   var tableHeaders = "";
                   tableHeaders = `<th class="text-center"> Status </th>`;
                   data.forEach(function ( value, index) {
                       let isShow = (value.is_showing) ? 'all':'none';
                       
                       tableHeaders += `<th class="${isShow} text-center">${value.label}</th>`;
                   })
                   $("#tableDivRealisasi").empty();

                   if (data.length > 0) {
                       $("#tableDivRealisasi").append('<table id="table_detail_realisasi" class="table table-striped- table-hover table-checkable"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                       DataTable = $('#table_detail_realisasi').DataTable({responsive: true});
                       if (id_sales) {
                           getDetailProduct(id_sales,'detail_realisasi');
                       }
                   }
               } else {
                   toastr.error(data.rm);
               }
           }
       }).done(function (msg) {
           // endLoadingPage();
       }).fail(function (msg) {
           // endLoadingPage();
           toastr.error("Terjadi Kesalahan");
       });
}

    function getDetailProduct(id,typeTable) {

            // detail_no_polis == ENDORS
            data_detail_product = [];

            console.log('id',id);
            console.log('typeTable',typeTable);

            if (typeTable == 'detail_cf') {
                var set_url =  base_url + 'marketing/data/detail_product_cf/' + id; 
            }
            else if (typeTable == 'detail_no_polis') {
                var set_url =  base_url + 'marketing/data/detail_product_no_polis/byget?polis_no=' + id; 
            }
            else if(typeTable == 'detail_product_list'){
                var set_url =  base_url + 'marketing/data/detail_product_list/' + id;
            }
            else if(typeTable == 'detail_realisasi'){
                var set_url =  base_url + 'marketing/data/detail_realisasi/' + id;
            }
            else{
                if (typeStore == 'edit'){
                    var set_url =  base_url + 'marketing/data/detail_product_list/' + id;
                }else{
                    var set_url =  base_url + 'marketing/data/detail_product/' + id;
                }
                
            }

            console.log(set_url);

            $.ajax({
                type: 'GET',
                url: set_url,
                beforeSend: function (res) {
                    loadingPage();
                },
                success: function (res) {

                  

                    var dataDetail =res.rm;

                    console.log(dataDetail);

                    dataDetail.forEach((vv,i) => {

                       

                        id_array_data = i;
                        temp_data = [];
                        let rows = [];

                        if (vv[0]['value_text'] != 'f') {

                            var btn_edit = `<a class="dropdown-item" onclick="modal_add_detail('edit', '${id_array_data}')">
                                                <i class="la la-edit"></i> Edit
                                            </a>`;

                            vv.forEach(data => {

                                

                                if (data.input_name == 'sts') {
                                    if (data.value_text == 't') {
                                        var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" style="width:90px;">Aktif</span>';
                                    }else if(data.value_text == 2){
                                        var sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Perubahan</span>';
                                    }else if(data.value_text == 3){
                                        var sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Baru</span>';
                                    } else {
                                        var sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="width:90px;">Non Aktif</span>';
                                    }
                                    rows.unshift(sts);
                                }

                                 // JIKA REALISASI
                                 if(typeProduct == 1){
                                    if (data.input_name == 'type') {
                                        if (data.value_text == 'R1') {
                                                btn_edit = `<a class="dropdown-item" onclick="modal_add_detail_realisasi('edit', '${id_array_data}')">
                                                    <i class="la la-edit"></i> Edit
                                                </a>`;
                                        }
                                    }
                                }

                               


                                var valueText = '';
                                if (data.is_currency) {
                                    valueText = formatMoney(clearNumFormatDec(data.value_text));
                                }else{
                                    valueText = data.value_text;
                                }

                                if (data.input_name != 'sts') {
                                    rows.push(valueText);
                                }

                                // var tmpNewColom = [];

                                // // var zzz = tmpHeadProduct.filter(p => p.bit_id == data.bit_id);


                                // tmpHeadProduct.forEach(head => {

                                //     if(head.bit_id == data.bit_id){
                                        
                                //     }
                                    
                                //     // var zzz = tmpHeadProduct.filter(p => p.bit_id == data.bit_id);
                                //     console.log('check_bit_id',data.bit_id);
                                //     console.log('check',zzz.length);
                                //     console.log('check_lagi',zzz);

                                // });

                                
                                var item = {
                                    "name" : data.input_name,
                                    "value": valueText,
                                    "bit_id" : data.bit_id,
                                };

                                    temp_data.push(item);


                            });

                        
                        if (typeTable == 'normal' || typeTable == 'detail_cf' || typeTable == 'detail_no_polis') {

                            if(typeForm == 'endors'){
                                var action = ``;
                            }else{
                                var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            ${btn_edit}
                                            <a class="dropdown-item" onclick="hapus_detail('${id_array_data}', this)">
                                                <i class="la la-trash"></i> Hapus
                                            </a>`;
                            }
                           
                          

                            rows.unshift(action);
                        }

                        data_detail_product.push({
                            id : id_array_data,
                            value : temp_data
                        });

                        DataTable.row.add(rows).draw();

                        
                        }
                    });

                        console.log(typeStore);

                        if (typeStore == 'edit' || typeStore == 'detail_list') {
                        }else{
                            setSumInsured();
                            
                        }
                    
                    endLoadingPage();

                }
            });
    }

    function getDetailInsured(getType) {
        let prod_id = $('#product').val();

        console.log(prod_id);

        $.ajax({
            type: "GET",
            url: base_url + '/quotation_slip/get_detail_insured/' + prod_id,
            data: {},

            beforeSend: function () {
                // loadingPage();
            },

            success: function (res) {
                var data = res.data;

                if (data.length == 0) {
                    $('#view_detail_product').hide();
                }else{
                    $('#view_detail_product').show();
                }

                console.log('data head',data);
                tmpHeadProduct = data;

                console.log(data.length);
                if (res.rc == 1) {
                    var tableHeaders = "";
                    tableHeaders = `<th width="30px" class="text-center all">Action</th>
                                    <th class="text-center all"> Status </th>`;
                    data.forEach(function ( value, index) {
                        let isShow = (value.is_showing) ? 'all':'none';
                        tableHeaders += `<th class="${isShow} text-center">${value.label}</th>`;
                    })
                    $("#tableDiv").empty();
                    $("#tableDiv").append('<table id="table_detail" class="table table-striped- table-hover table-checkable"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                    DataTable = $('#table_detail').DataTable({responsive: true});

                    if ($('#get_id').val()) {
                        getDetailProduct($('#get_id').val(),'normal');
                    }else{
                        if ($('#coc_no').val()) {
                            getDetailProduct($('#coc_no').val(),'detail_cf');
                        }

                        // if (getType == 'installment') {
                        //     getDetailProduct($('#coc_no').val(),'detail_cf');
                        // }

                        if (getType == 'endors') {
                            getDetailProduct($('#no_polis').val(),'detail_no_polis');
                        }
                    }

               
                    
                    
                  
                } else {
                    toastr.error(data.rm);
                }
            }
        }).done(function (msg) {
            // endLoadingPage();
        }).fail(function (msg) {
            // endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
  
    function modal_add_detail(action, id_arr) {
        event.preventDefault();

        console.log("data_detail_product",data_detail_product);
     
        var prod_id = $('#product').val();

        console.log(prod_id);
     
        $("#form-detail")[0].reset();

        if ( action == 'insert' || action == 'insert_edit') {

            if ( action == 'insert_edit' ) {
                prod_id = id_arr;
            }

            $.ajax({
                type: 'GET',
                url: '{{ route('get_insured_detail') }}',
                data: {
                    product_id : prod_id
                },

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                  
                    if ( response.rc == 1 ) {
                        var view = response.view;
                        $(".form_content").html(view);
                        initMaskMoney();
                        $("#title-detail").html("Detail");
                        $("#btn-detail").attr('onclick', 'save_detail(`insert`)');

                          // JIKA ADA REALISASI
                            if(typeProduct == 1){
                                $('#v-realisasi_sum_insured-80').hide();
                                $('#v-realisasi_premium-81').hide();
                                $('#v-type-82').hide();

                                $('#realisasi_sum_insured-80').val(0);
                                $('#realisasi_premium-81').val(0);
                                $('#type-82').val('A1');

                            }
                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        } else {

            // var prod_id_edit =  $("#underwriter_id").val();

            $.ajax({
                type: 'GET',
                url: '{{ route('get_insured_detail') }}',
                data: {
                    product_id : prod_id
                },

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    
                    if ( response.rc == 1 ) {
                        var view = response.view;
                        $(".form_content").html(``);
                        $(".form_content").append(`<input type="hidden" value="" name="sts-12" id="sts-12">`);
                        $(".form_content").append(view);
                        initMaskMoney();
                        


                        var data_edit = data_detail_product.filter(function(value, index) {
                            return value.id == id_arr;
                        });
                      
                        data_edit = data_edit[0].value;
                        data_edit.forEach(function (value, index) {
                            $("#" + value.name + '-' + value.bit_id).val(value.value);
                        });

                        $('#sts-12').val(data_edit[0]['value'])

                        $("#btn-detail").attr('onclick', 'save_detail(`edit`, `'+ id_arr +'`)');
                        $("#title-detail").html("Edit Detail");

                          // JIKA ADA REALISASI
                            if(typeProduct == 1){
                                $('#v-realisasi_sum_insured-80').hide();
                                $('#v-realisasi_premium-81').hide();
                                $('#v-type-82').hide();

                                $('#realisasi_sum_insured-80').val(0);
                                $('#realisasi_premium-81').val(0);
                                $('#type-82').val('A1');

                            }
                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // end if

      

        $("#detail_product").modal('show');
    }

    function modal_tambah_realisasi(action){
        var prod_id = $('#realisasi_product_id').val();

        if ( action == 'insert_edit' ) {
                prod_id = id_arr;
            }

            $.ajax({
                type: 'GET',
                url: '{{ route('get_insured_detail') }}',
                data: {
                    product_id : prod_id
                },

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                  
                    if ( response.rc == 1 ) {
                        var view = response.view;
                        $(".form_content").html(view);
                        initMaskMoney();
                        $("#title-detail").html("Detail");
                        $("#btn-detail").attr('onclick', 'store_tambah_realisasi()');

                          // JIKA ADA REALISASI
                            // if(typeProduct == 1){
                                $('#v-sum_insured-8').hide();
                                $('#v-premium-21').hide();
                                $('#v-type-82').hide();

                                $('#sum_insured-8').val(0);
                                $('#premium-21').val(0);
                                $('#type-82').val('R1');

                            // }
                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

            $("#detail_product").modal('show');

    }

    function store_tambah_realisasi(){

        loadingPage();

        var temp_id;
        var data_detail = $("#form-detail-realisasi").serializeArray();
        console.log('data_detail',data_detail);
        var array_input = data_detail.filter(function(item){
            return item.name != '_token';
        });

        temp_id = id_array_data+1;
        var temp_data = [];
        var rows = [];

        var item = {
            "name" : 'sts',
            "value" : 't',
            "bit_id" : 12,
        }
        
        temp_data.push(item);

        array_input.forEach( function (value, index){
            rows.push(value.value);
            var name = value.name.split('-')[0];
            var bit_id = value.name.split('-')[1];
            item = {
                "name": name,
                "value": value.value,
                "bit_id" : bit_id
            };
            temp_data.push(item);
        });

        console.log(temp_data);

        var _form_data_realisasi = new FormData();
        _form_data_realisasi.append('doc_id', $('#realisasi_doc_id').val());
        _form_data_realisasi.append('datas',JSON.stringify(temp_data));

        $.ajax({
            type: 'POST',
            url: base_url + '/marketing/store/tambah_realisasi',
            data: _form_data_realisasi,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (res) {
            }
        }).done(function (res) {
            endLoadingPage();

            $('#realisasi_sum_insured').html(formatMoney(res.realisasi_sum_insured));
            $('#realisasi_premium').html(formatMoney(res.realisasi_premium));

            $('#selisih_realisasi_sum_insured').html(formatMoney(res.selisih_realisasi_sum_insured));
            $('#selisih_realisasi_premium').html(formatMoney(res.selisih_realisasi_premium));


            toastr.success("berhasil ditambahkan !");
            $("#detail_product").modal('hide');
            
            detail_invoice_product_realisasi($('#realisasi_product_id').val(),$('#realisasi_doc_id').val());
            

        }).fail(function (res) {
            endLoadingPage();
            swal.fire("Error", "Terjadi Kesalahan!", "error");
        });
    }

    function modal_add_detail_realisasi(action, id_arr) {
        event.preventDefault();

        console.log('realisasi');
        console.log("data_detail_product",data_detail_product);
     
        var prod_id = $('#product').val();

        console.log(prod_id);
     
        $("#form-detail")[0].reset();

        if ( action == 'insert' || action == 'insert_edit') {

            if ( action == 'insert_edit' ) {
                prod_id = id_arr;
            }

            $.ajax({
                type: 'GET',
                url: '{{ route('get_insured_detail') }}',
                data: {
                    product_id : prod_id
                },

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                  
                    if ( response.rc == 1 ) {
                        var view = response.view;
                        $(".form_content").html(view);
                        initMaskMoney();
                        $("#title-detail").html("Detail");
                        $("#btn-detail").attr('onclick', 'save_detail(`insert_realisasi`)');

                          // JIKA ADA REALISASI
                            if(typeProduct == 1){
                                $('#v-sum_insured-8').hide();
                                $('#v-premium-21').hide();
                                $('#v-type-82').hide();

                                $('#sum_insured-8').val(0);
                                $('#premium-21').val(0);
                                $('#type-82').val('R1');

                            }
                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        } else {

            // var prod_id_edit =  $("#underwriter_id").val();

            $.ajax({
                type: 'GET',
                url: '{{ route('get_insured_detail') }}',
                data: {
                    product_id : prod_id
                },

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    
                    if ( response.rc == 1 ) {
                        var view = response.view;
                        $(".form_content").html(``);
                        $(".form_content").append(`<input type="hidden" value="" name="sts-12" id="sts-12">`);
                        $(".form_content").append(view);
                        initMaskMoney();
                        


                        var data_edit = data_detail_product.filter(function(value, index) {
                            return value.id == id_arr;
                        });
                      
                        data_edit = data_edit[0].value;
                        data_edit.forEach(function (value, index) {
                            $("#" + value.name + '-' + value.bit_id).val(value.value);
                        });

                        $('#sts-12').val(data_edit[0]['value'])

                        $("#btn-detail").attr('onclick', 'save_detail(`edit`, `'+ id_arr +'`)');
                        $("#title-detail").html("Edit Detail");

                          // JIKA ADA REALISASI
                          if(typeProduct == 1){
                                $('#v-sum_insured-8').hide();
                                $('#v-premium-21').hide();
                                $('#v-type-82').hide();

                                $('#sum_insured-8').val(0);
                                $('#premium-21').val(0);
                                $('#type-82').val('R1');
                            }
                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // end if

      

        $("#detail_product").modal('show');
    }

    function hapus_detail(id, elm) {
        var elmt = $(elm).parents('tr');
        data_detail_product = data_detail_product.filter(function(value){
            return value.id != id;
        });

        setSumInsured();
        DataTable.row(elmt).remove().draw(false);
    }

    function save_detail(act, id_detail) {
        if ( $("#form-detail")[0].checkValidity() ) {
            event.preventDefault();
            var temp_id;
            var data_detail = $("#form-detail").serializeArray();
            console.log('data_detail',data_detail);
            var array_input = data_detail.filter(function(item){
                return item.name != '_token';
                // return item.name != '_token';
            });

            temp_id = id_array_data+1;
            var temp_data = [];
            var rows = [];

         
            if ( act == 'insert' || act == 'insert_realisasi' ) {
                
                // <a class="dropdown-item" onclick="set_active('${temp_id}', this, 'f')">
                //                     <i class="la la-close"></i> Non Aktif
                //                 </a>

                if(act == 'insert_realisasi'){
                    var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" onclick="modal_add_detail_realisasi('edit', '${temp_id}')">
                                    <i class="la la-edit"></i> Edit
                                </a>
                                <a class="dropdown-item" onclick="hapus_detail('${temp_id}', this)">
                                    <i class="la la-trash"></i> Hapus
                                </a>
                            `;
                }else{
                    var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" onclick="modal_add_detail('edit', '${temp_id}')">
                                    <i class="la la-edit"></i> Edit
                                </a>
                                <a class="dropdown-item" onclick="hapus_detail('${temp_id}', this)">
                                    <i class="la la-trash"></i> Hapus
                                </a>
                            `;
                }

               

                var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';

                console.log('array_input',array_input);

                var item = {
                    "name" : 'sts',
                    "value" : 't',
                    "bit_id" : 12,
                }
                
                temp_data.push(item);

                array_input.forEach( function (value, index){
                    rows.push(value.value);
                    var name = value.name.split('-')[0];
                    var bit_id = value.name.split('-')[1];
                    item = {
                        "name": name,
                        "value": value.value,
                        "bit_id" : bit_id
                    };
                    temp_data.push(item);
                });


                
              

                // Insert acton at the begining index array
                rows.unshift(sts);
                rows.unshift(action);

                data_detail_product.push({
                    id : temp_id,
                    value: temp_data
                });

               

                DataTable.row.add(rows).draw();
                id_array_data++;
            } else {

                console.log('array_input',array_input);
                // var item = {
                //     "name" : 'sts',
                //     "value" : 't',
                //     "bit_id" : 12,
                // }

                // temp_data.push(item);

                array_input.forEach( function (value, index){
                    rows.push(value.value);
                    var name = value.name.split('-')[0];
                    var bit_id = value.name.split('-')[1];
                    item = {
                        "name": name,
                        "value": value.value,
                        "bit_id" : bit_id
                    };
                    temp_data.push(item);
                });



                // Remove All Data
                DataTable.clear().draw(false);
                id_detail = parseInt(id_detail);

               
                // Remove From Array
                data_detail_product = data_detail_product.filter(function(value){
                  
                    return value.id != id_detail;
                });

                // Insert to Array
                data_detail_product.push({
                    id : temp_id,
                    value: temp_data
                });


                for ( i = 0; i < data_detail_product.length; i++) {


                    action = "";
                    rows = [];

                    action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" onclick="modal_add_detail('edit', '${data_detail_product[i].id}')">
                                        <i class="la la-edit"></i> Edit
                                    </a>
                                    <a class="dropdown-item" onclick="hapus_detail('${data_detail_product[i].id}', this)">
                                        <i class="la la-trash"></i> Hapus
                                    </a>`;
                    
                    console.log(data_detail_product[i]['value'][0]['value']);
                    // if (data_detail_product[i]['value'][0]['value']) {
                        
                    // }

                    if (data_detail_product[i]['value'][0]['value'] == 'f') {
                        sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="width:90px;">Non Aktif</span>';
                    }else if(parseInt(data_detail_product[i]['value'][0]['value']) == 2){
                        sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Perubahan</span>';
                    }else if(parseInt(data_detail_product[i]['value'][0]['value']) == 3){
                        sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Baru</span>';
                    } else {
                        sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" style="width:90px;">Aktif</span>';
                    }

                    data_detail_product[i].value.forEach( function (value, index){

                        if ( value.name != 'sts' ) {
                            rows.push(value.value);
                        }
                    });

                    // Insert acton at the begining index array
                    rows.unshift(sts);
                    rows.unshift(action);
                    DataTable.row.add(rows).draw();
                    id_array_data = data_detail_product[i].id;

                    console.log(id_array_data);

                }

           
            }

            setSumInsured();

            $("#detail_product").modal('hide');
        }
    }


    
// DOWNLOAD TEMPLATE EXCEL

function modal_download_template() {

    $('#ex-select-all').prop('selected',true);

    var product_id = $("#product").val();

    // console.log(product_id);

        $.ajax({
        type: "GET",
        url: base_url + 'marketing/data/list_template_excel/' + product_id,
        data: {},

        beforeSend: function () {
            loadingPage();
        },

        success: function (res) {
            var data = res.data;
            // console.log(data);

            if (res.rc == 1) {
                let list = [];
                list[1] = "";
                list[2] = "";
                list[3] = "";

                let idx_list = 1;

                data.forEach(function ( value, index) {

                    if (value.col_list) {
                        list[value.col_list] += `<div> <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                            <input value="${value.label}" data-idname="${value.input_name}" class="ck-excel" name="ck-excel-label[]" checked type="checkbox"> ${value.label}
                                            <span></span>
                                        </label></div>
                                    `;    
                        
                    }else{
                        if (idx_list == 1 || idx_list == 2) {
                            list[idx_list] += ` <div> <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                            <input value="${value.label}" data-idname="${value.input_name}" class="ck-excel" name="ck-excel-label[]" checked type="checkbox"> ${value.label}
                                            <span></span>
                                        </label></div>
                                        `;
                            idx_list++;
                        }
                        else if(idx_list == 3){
                            list[idx_list] += `<div> <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                            <input value="${value.label}" data-idname="${value.input_name}" class="ck-excel" name="ck-excel-label[]" checked type="checkbox"> ${value.label}
                                            <span></span>
                                        </label></div>
                                        `;
                            idx_list = 1;
                        }
                    }
                        
                })
                
                $("#list_co_1").empty();
                $("#list_co_1").append(list[1]);
                $("#list_co_2").empty();
                $("#list_co_2").append(list[2]);
                $("#list_co_3").empty();
                $("#list_co_3").append(list[3]);



                // var tableHeaders = "";
                // data.forEach(function ( value, index) {
                //     tableHeaders += `<div class="col-md-4">
                //                         <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                //                             <input value="${value.label}" data-idname="${value.input_name}" class="ck-excel" name="ck-excel-label[]" checked type="checkbox"> ${value.label}
                //                             <span></span>
                //                         </label>
                //                     </div>`;
                // })
                // $("#list_check_template").empty();
                // $("#list_check_template").append(tableHeaders);
            } else {
                toastr.error(data.rm);
            }
        }
    }).done(function (msg) {
        endLoadingPage();
    }).fail(function (msg) {
        endLoadingPage();
        toastr.error("Terjadi Kesalahan");
    });

    $('#template_excel').modal('show');
}

function modal_import_template() {
    $("#excel-import")[0].reset();
    $('#excel-import').bootstrapValidator("resetForm", true);
    $('#import_template_excel').modal('show');
}

function exportData(ext) {

    let arr_ck_excel_label = [];
    let arr_ck_excel_input_name = [];

    $('input[name="ck-excel-label[]"]:checked').each(function() {
        arr_ck_excel_label.push($(this).val());
        arr_ck_excel_input_name.push($(this).data('idname'));
        // console.log($(this).val());
        // console.log($(this).data('idname'));
    });
    // $('#type_size').find(':selected').data('harga');


    if (arr_ck_excel_label.length == 0) {
        toastr.warning('Minimal Pilih 1 Kolom Untuk Melakukan Download Template');
    }else{
        var query = {
            file: ext,
            product_id: $('#product').val(),
            product_name: $('#product').html(),
            ck_excel_label: arr_ck_excel_label,
            ck_excel_input_name: arr_ck_excel_input_name,
        }

        console.log(query);
        window.open(base_url + 'report/detail_product_export?'+$.param(query), "_blank");
    }
}

function importDataExcel() {

    var validateDebit = $('#excel-import').data('bootstrapValidator').validate();
    if (validateDebit.isValid()) {
        var id = $("#id").val();
        var formData = document.getElementById("excel-import");
        var objData = new FormData(formData);
    
        var rows = [];

        $.ajax({
            type: 'POST',
            url: '{{ route('import.detail_product_import') }}',
            data: objData,
            contentType: false,
            processData: false,

            beforeSend: function () {
                loadingPage();
            },
            success: function (msg) {
                endLoadingPage();
            }

        }).done(function (msg) {
            // console.log('done',msg);

            let dataExcel = msg.rm;
            let detailHeader = msg.detail;
            let typeImport = msg.zn_type_import;

            var arrError = [];

            if (msg.product_id == '') {
                toastr.info('Template Tidak Sesuai, ID pada template tidak ditemukan !');
                return false;
            }

            if (typeImport == '') {
                toastr.info('Template Tidak Sesuai, Tipe Import tidak ditemukan !');
                return false;
            }
            
            if(msg.product_id != $('#product').val()) {
                toastr.info('Template Tidak Sesuai !');
                return false;
            }

            let tmp_dataExcel = [];

            // VALIDASI
            dataExcel.forEach((v,key) => {
                if (key > 0) {
                        if (v['sts'] == 0) {
                            arrError.push(`<div class="kt-list-timeline__item">
                                                <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                <span class="kt-list-timeline__text">Kolom Status Baris ke `+(key+2)+`</span>
                                                <span class="kt-list-timeline__time" style="width: 130px;">
                                                <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" 
                                                style="background: #fff5fe;color: #fd3162;border: 1px solid #fde0e7;">Status Tidak boleh 0</span> </span>
                                            </div>`);
                        }
                        if (v['sts'] > 3) {
                            arrError.push(`<div class="kt-list-timeline__item">
                                                <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                <span class="kt-list-timeline__text">Kolom Status Baris ke `+(key+2)+`</span>
                                                <span class="kt-list-timeline__time" style="width: 130px;">
                                                <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" 
                                                style="background: #fff5fe;color: #fd3162;border: 1px solid #fde0e7;">Status Tidak Ada</span> </span>
                                            </div>`);
                        }
                    detailHeader.forEach((v_fk,key_fk) => {
                        let headerAllow = Object.keys(v);
                        if (headerAllow.includes(v_fk.input_name)) {
                            
                            // HARUS DI ISI
                            if (v_fk.is_required) {
                                if (v[v_fk.input_name] == '' || v[v_fk.input_name] == null) {
                                    arrError.push(`<div class="kt-list-timeline__item">
                                                        <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                        <span class="kt-list-timeline__text">Kolom `+v_fk.label+` Baris ke `+(key+2)+`</span>
                                                        <span class="kt-list-timeline__time" style="width: 130px;"><span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="background: #fff5fe;color: #fd3162;border: 1px solid #fde0e7;">Tidak Boleh Kosong</span> </span>

                                                    </div>`);
                                }else{
                                    if (v_fk.is_number || v_fk.is_currency) {
                                        if (typeof v[v_fk.input_name] != 'number') {
                                            arrError.push(`<div class="kt-list-timeline__item">
                                                            <span class="kt-list-timeline__badge kt-list-timeline__badge--danger"></span>
                                                            <span class="kt-list-timeline__text">
                                                                Kolom `+v_fk.label+` Baris ke `+(key+2)+`
                                                                </span>
                                                                <span class="kt-list-timeline__time" style="width: 130px;"><span class="mb-1 kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="background: #fff5fe;color: #fd3162;border: 1px solid #fde0e7;">Harus Angka</span></span>
                                                        </div>`);
                                        }
                                    }
                                }
                            
                            // BOLEH KOSONG
                            }else{

                            }

                           
                        }
                    });
                }
                
            });
            // VALIDASI

            if (arrError.length == 0) {
                if (typeImport == 'xls_table') {
                    data_detail_product = [];
                    DataTable.clear().draw();
                }
            }



            dataExcel.forEach((v,key) => {
                if (key > 0) {
                    id_array_data += key;
                    rows = [];
                    let tmp_data_fk = [];
                    temp_data = [];

                    actSts = '';

                    // var actSts = `<a class="dropdown-item" onclick="set_active('${id_array_data}', this, 'f')">
                    //                     <i class="la la-close"></i> Non Aktif
                    //                 </a>`;

                    // if (typeImport == 'xls_table') {
                    //     if (v['sts'] == 0) {
                    //         actSts = `<a class="dropdown-item" onclick="set_active('${id_array_data}', this, 't')">
                    //                     <i class="la la-check"></i> Aktif
                    //                 </a>`;
                    //     }
                    //     else if(v['sts'] == 1) {
                    //         actSts = `<a class="dropdown-item" onclick="set_active('${id_array_data}', this, 'f')">
                    //                     <i class="la la-close"></i> Non Aktif
                    //                 </a>`;
                    //     }
                    // }
                    

                    var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="flaticon-more"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" onclick="modal_add_detail('edit', '${id_array_data}')">
                                            <i class="la la-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" onclick="hapus_detail('${id_array_data}', this)">
                                            <i class="la la-trash"></i> Hapus
                                        </a>
                                       `+actSts;

                    var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';

                    if (typeImport == 'xls_table') {
                        if (v['sts'] == 0) {
                            sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="width:90px;">Non Aktif</span>';
                        }else if(v['sts'] == 2){
                            sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Perubahan</span>';
                        }else if(v['sts'] == 3){
                            sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Baru</span>';
                            
                        } else {
                            sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" style="width:90px;">Aktif</span>';
                        }
                    }
                    else{
                        
                    }

                    rows.unshift(sts);
                    rows.unshift(action);
                
                   


                

                    if (arrError.length == 0) {

                        

                        var setStatus = 't';

                        if (typeImport == 'xls_table') {
                            if (v['sts'] == 0) {
                                setStatus = 'f';
                            }
                            else if (v['sts'] == 2) {
                                setStatus = 2;
                            }
                            else if (v['sts'] == 3) {
                                setStatus = 3;
                            }
                        }

                        var sts_item = {
                            "name" : 'sts',
                            "value" : setStatus,
                            "bit_id" : 12,
                        }

                        temp_data.push(sts_item);
                    

                        detailHeader.forEach((v_fk,key_fk) => {

                        if (v[v_fk.input_name]) {

                            let setValue = (typeof v[v_fk.input_name] == 'number') ? ((v_fk.is_currency) ? formatMoney(v[v_fk.input_name]):v[v_fk.input_name]) :v[v_fk.input_name];

                            var item = {
                                "name" : v_fk.input_name,
                                "value": setValue,
                                "bit_id" : v_fk.bit_id,
                            };
                            rows.push(setValue);
                        }else{
                            var item = {
                                "name" : v_fk.input_name,
                                "value": '',
                                "bit_id" : v_fk.bit_id,
                            };
                            rows.push('');
                        }

                        temp_data.push(item);

                        });

                        data_detail_product.push({
                        id : id_array_data,
                        value : temp_data
                        });

                        DataTable.row.add(rows).draw();

                        setSumInsured();
                    }


                }

            });

            $('#import_template_excel').modal('hide');

            if (arrError.length > 0) {
                $('#kt-iconbox__icon').hide();
                var textEx = `<div class="kt-list-timeline mt-4 mb-4 scrollStyleDanger px-2" style="max-height: 350px;overflow-y: scroll;">
                                <div class="kt-list-timeline__items">
                                    `+arrError.join("")+`
                                </div>
                            </div>`
                var actionEx = `  <button onclick="znIconboxClose();modal_import_template();" type="button"
                                    class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Import Ulang</button>
                                <button onclick="znIconboxClose()" type="button"
                                    class="btn btn-outline-danger btn-elevate btn-pill btn-elevate-air btn-sm">Tutup</button>`;
                znIconbox("Import Excel", textEx, actionEx,'danger');
            }

        }).fail(function (msg) {
            console.log('error',msg);
            endLoadingPage();
            // toastr.error("Terjadi Kesalahan");
        });

    }

}


function export_excel_detail() {

    $.ajax({
        type : 'POST',
        url: '{{ route('export.table_detail_product_export') }}',
        data:{
            file: 'xls_table',
            product_id: $('#product').val(),
            product_name: $('#product_name').html(),
            table_data: data_detail_product
        },
        beforeSend: function () {
            loadingPage();
        },
    }).then((response) => {
        endLoadingPage();
        window.open(base_url + response, "_blank");
    });
}

$(document).ready(function () {
    $('#ex-select-all').click(function (e) {
        $('.ck-excel').prop('checked', this.checked);
    });

    $("#excel-import").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            file_import: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    file: {
                        extension: 'xlsx,xls',
                        type: 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        message: 'File yang akan di import tidak sesuai'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
})

function set_active(id_detail, elm, is_active) {

    data_detail_product.forEach(function (value, index) {

        if( value.id == id_detail ) {
            value.value.forEach(function (dt, index) {
                if ( dt.bit_id == 12 && is_active == 'f') {
                    dt.value = 'f';
                } else if ( dt.bit_id == 12 && is_active == 't') {
                    dt.value = 't';
                }
            });
        }
    });

    var data_pilih = data_detail_product.filter(function (value, index) {
        return value.id == id_detail;
    });

    var elmt = $(elm).parents('tr');
    DataTable.row(elmt).remove().draw(false);

    var sts, act;
    var rows = [];
    if (is_active == 'f' ) {
        // Set Non Aktif
        sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Non Aktif</span>';
        // act = `<a class="dropdown-item" onclick="set_active('${data_pilih[0].id}', this, 't')">
        //         <i class="la la-check"></i> Aktif
        //     </a>`;

    } else {
        // Set Aktif
        sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';
        // act = `<a class="dropdown-item" onclick="set_active('${data_pilih[0].id}', this, 'f')">
        //         <i class="la la-close"></i> Non Aktif
        //     </a>`;
    }

    act = '';

    var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flaticon-more"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" onclick="modal_add_detail('edit', '${data_pilih[0].id}')">
                            <i class="la la-edit"></i> Edit
                        </a>
                        <a class="dropdown-item" onclick="hapus_detail('${data_pilih[0].id}', this)">
                            <i class="la la-trash"></i> Hapus
                        </a>` + act;

    data_pilih[0].value.forEach( function (value, index){
        if ( value.name != 'sts' ) {
            rows.push(value.value);
        }
    });

    rows.unshift(sts);
    rows.unshift(action);

    DataTable.row.add(rows).draw();

}
// END DOWNLOAD TEMPLATE EXCEL


function setSumInsured() {
    console.log(data_detail_product);
    console.log(typeForm);

    var ttlSI = 0,ttlPremi = 0,ttlAddPremi = 0,ttlGrossPremi = 0,ttlPremiFinal = 0;
    var ttlDiscFeet= 0,ttlDiscFeet= 0,ttlPremiAfterFeet= 0,ttlAddDisc = 0;ttlRealisasiSumInsured = 0;ttlRealisasiPremium = 0;
    
    
    data_detail_product.forEach(v => {
        let dp = v.value;


        let isAllowCount = false;

        if (typeForm == 'endors') {
            if (parseInt(dp[0]['value']) == 2 || parseInt(dp[0]['value']) == 3) {
                isAllowCount = false;
            }else{
                isAllowCount = true;
            }
        }else{
            isAllowCount = true;
        }

       if (isAllowCount) {
           
            dp.forEach(v => {
                
                switch (parseInt(v.bit_id)) {
                    case 8:
                        // Sum Insured
                        var si = (v.value) ? parseFloat(clearNumFormatDec(v.value)):0;
                        ttlSI += si;
                    break;
                    case 29 :
                        // Gross Premi
                        var grossPremi = (v.value) ? parseFloat(clearNumFormatDec(v.value)):0;
                        ttlGrossPremi += grossPremi;
                    break;
                    case 30 :
                        // Discount Feet
                        var discFeet = (v.value) ? parseFloat(clearNumFormatDec(v.value)):0;
                        ttlDiscFeet += discFeet;
                    break;
                    case 1 :
                        // Additional Discount
                        var addDiscount = (v.value) ? parseFloat(clearNumFormatDec(v.value)):0;
                        ttlAddDisc += addDiscount;
                    break;
                    case 80 :
                        var addRealisasiSumInsured = (v.value) ? parseFloat(clearNumFormatDec(v.value)):0;
                        ttlRealisasiSumInsured += addRealisasiSumInsured;
                    break;
                    case 81 :
                        var addRealisasiPremium = (v.value) ? parseFloat(clearNumFormatDec(v.value)):0;
                        ttlRealisasiPremium += addRealisasiPremium;
                    break;
                }
            });
       }


    });   

    $('#insurance').val(formatMoney(ttlSI));
   
    // convertToRupiah1(ttlSI);

    var ef = ($('#ef').val()) ? parseFloat(clearNumFormatDec($('#ef').val())):0;

    $('#premi').val(formatMoney(ttlGrossPremi-ef));
    $('#disc_amount').val(formatMoney(ttlAddDisc));

    if(typeProduct == 1){
        // if(ttlRealisasiSumInsured > ttlSI){
        //     swal.fire("Info", "Realisasi Sum Insured Melebihi Total Sum Insured", "info");
        //     // return false;
        // }

        // if(ttlRealisasiPremium > ttlGrossPremi){
        //     swal.fire("Info", "Realisasi Premium Melebihi Gross Premium Amount", "info");
        //     // return false;
        // }

        
        $('#selisih_realisasi_sum_insured').val(formatMoney(Math.abs(ttlSI-ttlRealisasiSumInsured)));
        $('#selisih_realisasi_premium').val(formatMoney(Math.abs(ttlGrossPremi-ttlRealisasiPremium)));

        $('#realisasi_sum_insured').val(formatMoney(ttlRealisasiSumInsured));
        $('#realisasi_premium').val(formatMoney(ttlRealisasiPremium));
    }

    convertToRupiah2(ttlGrossPremi);
}



function formatMoneyOld(amount, decimalCount = 2, decimal = ",", thousands = ".") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        
    }
}

function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        
    }
}

</script>