<script>
    var typeStore = '{{$typeStore}}';
    var getId = '{{$getId}}';
    var typeForm = 'normal';

  
    function pilihCOC(coc_id) {
        $('#coc_no').val(coc_id).trigger('change.select2');
        $('#modal-search-policy').modal('hide');
    }

    function setCOC(coc_id) {
        console.log('coc_id',coc_id);
        $.ajax({
            type: 'GET',
            url: base_url + 'marketing/data/policy/' + coc_id,
            beforeSend: function (res) {
                loadingPage();
            },
            success: function (res) {
               
                let data = res.rm;
                typeProduct = data.type_product;

                // JIKA REALISASI
                if(typeProduct == 1){
                    $('.isRealisasi').show();
                }else{
                    $('.isRealisasi').hide();
                }

                $('#insurance').val(formatMoney(data.total_sum_insured));
                $('#no_of_insured').val(data.no_of_insured);

                if (data.policy_name) {
                    $('#policy_cust_name').val(data.policy_name);
                }else{
                    $('#policy_cust_name').val(data.full_name);
                }

                
                

                // select 2
                $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
                $('#customer').val(data.customer_id).trigger('change.select2');
                $('#product').val(data.product_id).trigger('change.select2');
                $('#officer').val(data.agent_id).trigger('change.select2');
                $('#valuta_id').val(data.valuta_id).trigger('change.select2');

                //date
                $('#start_date_police').val(znFormatDateNum(data.from_date));
                $('#end_date_police').val(znFormatDateNum(data.to_date));

                var text =  $( "#valuta_id option:selected" ).text();
                $('.zn-rp-group').html(text);

                getDetailInsured();
                // convertToRupiah1(data.total_sum_insured);
                endLoadingPage();
                $('#dataShow').fadeIn();

            }
        });
    }

    function storePolicy() {

        var validateDebit = $('#form-policy').data('bootstrapValidator').validate();
        if (validateDebit.isValid()) {
            loadingPage();
            let validateC = 0;
            validateC = validateAmount();
            if (validateC > 0) {
                endLoadingPage();
                return false;
            }

            $.ajax({
                type: "GET",
                url: base_url + '/cekSalesPolis?id=' + $("#no_polis").val(),
                beforeSend: function () {
                    loadingPage();
                },

                success: function (result) {
                    var data = $.parseJSON(result);
                    if (data.length != 0 && typeStore == 'new') {
                        endLoadingPage();
                        swal.fire("Info", "Polis Sudah ada", "info");
                        return false;
                    } else {

                        var _form_data = new FormData(_create_form[0]);
                        _form_data.append('nett_amount', $('#nett_amount').val());
                        _form_data.append('asuransi', $('#asuransi').val());
                        _form_data.append('no_of_insured', $('#no_of_insured').val());
                        _form_data.append('policy_cust_name', $('#policy_cust_name').val());
                        _form_data.append('id_underwriter', $('#id_underwriter').val());
                        _form_data.append('customer', $('#customer').val());
                        _form_data.append('product', $('#product').val());
                        _form_data.append('officer', $('#officer').val());
                        _form_data.append('valuta_id', $('#valuta_id').val());
                        _form_data.append('start_date_police', $('#start_date_police').val());
                        _form_data.append('end_date_police', $('#end_date_police').val());
                        _form_data.append('coc_no', $('#coc_no').val());
                        _form_data.append('insurance', $('#insurance').val());
                        _form_data.append('premi', $('#premi').val());
                        _form_data.append('disc_amount', $('#disc_amount').val());
                        _form_data.append('ef_pct', $('#ef_pct').val());
                        _form_data.append('realisasi_sum_insured', $('#realisasi_sum_insured').val());
                        _form_data.append('realisasi_premium', $('#realisasi_premium').val());
                        _form_data.append('ef', $('#ef').val());

                        _form_data.append('detail', JSON.stringify(data_detail_product));

                        $.ajax({
                            type: 'POST',
                            url: base_url + '/marketing/store/policy',
                            data: _form_data,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            success: function (res) {
                            }
                        }).done(function (res) {
                            var obj = res;
                            endLoadingPage();

                            if (obj.rc == 1) {
                                znIconbox("Data Berhasil Disimpan", text, action);
                            } else {
                                swal.fire("Info", obj.rm, "info");
                            }

                        }).fail(function (res) {
                            endLoadingPage();
                            swal.fire("Error", "Terjadi Kesalahan!", "error");
                        });
                    }
                }
            }).done(function (msg) {
                //  endLoadingPage();

            }).fail(function (msg) {
                endLoadingPage();
                swal.fire("Info", "terjadi kesalahan", "info");
                return false;
            });


        }


    }

    function searchPolicy() {

        $('#title_modal_seach').html('List Data Master Vechile');

        var act_url = base_url + 'marketing/data/search_policy_table/all';

            var tableSearchPolicy = $('#search_policy_table').DataTable({
                aaSorting: [],
                processing: true,
                serverSide: true,
                responsive: true,
                destroy:true,
                columnDefs: [
                    { "orderable": false, "targets": 0 }],
                ajax: {
                    "url" : act_url,
                    "error": function(jqXHR, textStatus, errorThrown)
                        {
                            toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                        }
                    },
                columns: [
                    { data: 'cf_no', name: 'cf_no' },    
                    { data: 'full_name', name: 'full_name' },
                    { data: 'underwriter', name: 'underwriter' },
                    { data: 'no_of_insured', name: 'no_of_insured' },
                    { data: 'produk', name: 'produk' },
                    { data: 'agent', name: 'agent' },
                    { data: 'mata_uang', name: 'mata_uang' },
                    { data: 'from_date', name: 'from_date' },
                    { data: 'to_date', name: 'to_date' },
                    { data: 'total_sum_insured', name: 'total_sum_insured' },
                    // { data: 'gross_premium', name: 'gross_premium' },
                    // { data: 'nett_premium', name: 'nett_premium' },
                    // { data: 'cn_no', name: 'cn_no' },

                    { data: 'action', name: 'action' },   
                ]
            });
        
        $('#modal-search-policy').modal('show');
    }

    function getMasterData(id) {

        data_detail_product = [];
        $.ajax({
            type: 'GET',
            url: base_url + 'marketing/data/master_sales_polis_byid/' + id,
            beforeSend: function (res) {
                loadingPage();
            },
            success: function (res) {
                let data = res.rm;
                typeProduct = data.type_product;

                // JIKA REALISASI
                if(typeProduct == 1){
                    $('.isRealisasi').show();
                }else{
                    $('.isRealisasi').hide();
                }

                $('#get_id').val(id);
                
                $('#no_of_insured').val(data.no_of_insured);
                if (data.policy_name) {
                    $('#policy_cust_name').val(data.policy_name);
                }else{
                    $('#policy_cust_name').val(data.full_name);
                }
                $('#no_polis').val(data.polis_no);
                $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
                $('#customer').val(data.customer_id).trigger('change.select2');
                $('#product').val(data.product_id).trigger('change.select2');
                $('#officer').val(data.agent_id).trigger('change.select2');
                $('#valuta_id').val(data.valuta_id).trigger('change.select2');
                $('#start_date_police').val(znFormatDateNum(data.start_date_polis));
                $('#end_date_police').val(znFormatDateNum(data.end_date_polis));
                // SELECT 2 WITOUH TRIGER

                $('#coc_no').append(`<option value=`+data.coc_id+`>`+data.cf_no+`</option>`)                
                $('#coc_no').select2('destroy');
                $('#coc_no').val(data.coc_id).select2();

                $('#insurance').val(formatMoney(data.ins_amount));
                $('#tax_amount').val(formatMoney(data.tax_amount));
                $('#fee_admin').val(formatMoney(data.admin_amount));
                $('#fee_materai').val(formatMoney(data.materai_amount));
                $('#fee_agent').val(formatMoney(data.agent_fee_amount));
                $('#asuransi').val(formatMoney(data.ins_fee));
                $('#nett_amount').val(formatMoney(data.net_amount));
                $('#fee_polis').val(formatMoney(data.polis_amount));
                $('#fee_internal').val(formatMoney(data.comp_fee_amount));
                $('#disc_amount').val(formatMoney(data.disc_amount));
                $('#premi').val(formatMoney(data.premi_amount));
                $('#ef').val(formatMoney(data.ef));
                $('#ef_pct').val(formatMoney(data.ef_pct));
                $('#realisasi_sum_insured').val(formatMoney(data.realisasi_sum_insured));
                $('#realisasi_premium').val(formatMoney(data.realisasi_premium));
                getDetailInsured();
              
                // convertToRupiah1(data.total_sum_insured);
                endLoadingPage();
                $('#dataShow').fadeIn();

            }
        });
    }

   


    $(document).ready(function () {

        $('#coc_no').select2('destroy');
        $('#coc_no').val(null).select2();

        if (typeStore == 'edit') {
            getMasterData(getId);
            $('#dataShow').show();
        }else{
            $('#dataShow').hide();
        }

     

        $("#form-policy").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                coc_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                no_polis: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                policy_cust_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                }
                // ,
                // ef_pct: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan isi'
                //         },
                //     }
                // }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

   
</script>
