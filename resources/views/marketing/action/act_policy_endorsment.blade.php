@php
    if (isset($_GET["get_polis_no"])) {
        $get_polis_no = $_GET["get_polis_no"];
    }else {
        $get_polis_no = '';
    }
@endphp

<script>
    var typeStore = '{{$typeStore}}';
    var getId = '{{$getId}}';
    var typeForm = 'endors';


    function setPolicyNumber(id) {
        console.log(id);
        $.ajax({
            type: 'GET',
            url: base_url + 'marketing/data/master_sales_polis/byget?polis_no='+id,
            beforeSend: function (res) {
                loadingPage();
            },
            success: function (res) {
                console.log(res);
                console.log('tes');
                
                let data = res.rm;
                // $('#insurance').val(formatMoney(data.ins_amount));
                $('#no_of_insured').val(data.no_of_insured);

                if (data.policy_name) {
                    $('#policy_cust_name').val(data.policy_name);
                }else{
                    $('#policy_cust_name').val(data.full_name);
                }

                typeProduct = 1;
                

                // $('#policy_cust_name').val(data.policy_name);
                
                
                $('#coc_id').val(data.coc_id);

                $('#ef').val(formatMoney(data.ef));
                $('#ef_pct').val(formatMoney(data.ef_pct));



                // select 2
                $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
                $('#customer').val(data.customer_id).trigger('change.select2');
                $('#product').val(data.product_id).trigger('change.select2');
                $('#officer').val(data.agent_id).trigger('change.select2');
                $('#valuta_id').val(data.valuta_id).trigger('change.select2');

                //date
                $('#start_date_police').val(znFormatDateNum(data.start_date_polis));
                $('#end_date_police').val(znFormatDateNum(data.end_date_polis));

                var value = $('#valuta_id').val();
                var text = $("#valuta_id option:selected").text();
                $('.zn-rp-group').html(text);

                getDetailInsured('endors');
                // convertToRupiah1(data.ins_amount);
                endLoadingPage();
                $('#dataShow').fadeIn();

            }
        });
    }

    function pilihPolicyNumber(id) {
        $('#no_polis').val(id).trigger('change.select2');
        $('#modal-search-policy').modal('hide');
    }

    function searchPolicy() {

        $('#title_modal_seach').html('List Data Master Policy Active');

        var act_url = base_url + 'marketing/data/search_policy_number_table/all';

            var tableSearchPolicy = $('#search_policy_table').DataTable({
                aaSorting: [],
                processing: true,
                serverSide: true,
                responsive: true,
                destroy:true,
                columnDefs: [
                    { "orderable": false, "targets": 0 }],
                ajax: {
                    "url" : act_url,
                    "error": function(jqXHR, textStatus, errorThrown)
                        {
                            toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                        }
                    },
                columns: [
                    { data: 'polis_no', name: 'polis_no' },
                    { data: 'full_name', name: 'full_name' },
                    { data: 'underwriter', name: 'underwriter' },
                    { data: 'no_of_insured', name: 'no_of_insured' },
                    { data: 'produk', name: 'produk' },
                    { data: 'agent', name: 'agent' },
                    { data: 'mata_uang', name: 'mata_uang' },
                    { data: 'start_date_polis', name: 'start_date_polis' },
                    { data: 'end_date_polis', name: 'end_date_polis' },
                    { data: 'ins_amount', name: 'ins_amount' },
                    // { data: 'gross_premium', name: 'gross_premium' },
                    // { data: 'nett_premium', name: 'nett_premium' },
                    // { data: 'cn_no', name: 'cn_no' },

                    { data: 'action', name: 'action' },
                ]
            });

        $('#modal-search-policy').modal('show');
    }

    function storePolicy() {

        var validateDebit = $('#form-policy').data('bootstrapValidator').validate();
        if (validateDebit.isValid()) {
            loadingPage();
            let validateC = 0;
            validateC = validateAmount();
            if (validateC > 0) {
                endLoadingPage();
                return false;
            }

            console.log(validateC);

            var _form_data = new FormData(_create_form[0]);
            _form_data.append('nett_amount', $('#nett_amount').val());
            _form_data.append('asuransi', $('#asuransi').val());
            _form_data.append('no_of_insured', $('#no_of_insured').val());
            _form_data.append('policy_cust_name', $('#policy_cust_name').val());
            _form_data.append('id_underwriter', $('#id_underwriter').val());
            _form_data.append('customer', $('#customer').val());
            _form_data.append('product', $('#product').val());
            _form_data.append('officer', $('#officer').val());
            _form_data.append('valuta_id', $('#valuta_id').val());
            _form_data.append('start_date_police', $('#start_date_police').val());
            _form_data.append('end_date_police', $('#end_date_police').val());
            _form_data.append('coc_no', $('#coc_no').val());
            _form_data.append('insurance', $('#insurance').val());
            _form_data.append('premi', $('#premi').val());
            _form_data.append('disc_amount', $('#disc_amount').val());
            _form_data.append('ef_pct', $('#ef_pct').val());
            _form_data.append('ef', $('#ef').val());
            _form_data.append('realisasi_sum_insured', $('#realisasi_sum_insured').val());
            _form_data.append('realisasi_premium', $('#realisasi_premium').val());
            _form_data.append('detail', JSON.stringify(data_detail_product));

            $.ajax({
                type: 'POST',
                url: base_url + '/marketing/store/policy_endorsment',
                data: _form_data,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (res) {
                    console.log(res['data']);
                }
            }).done(function (res) {
                var obj = res;
                console.log(res['data']);
                endLoadingPage();

                var textEn = `pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
                var actionEn = `<button onclick="znView('policy')" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;


                if (obj.rc == 1) {
                    znIconbox("Data Berhasil Disimpan", textEn, actionEn);
                } else {
                    swal.fire("Info", obj.rm, "info");
                }

            }).fail(function (res) {
                endLoadingPage();
                swal.fire("Error", "Terjadi Kesalahan!", "error");
            });

        }


    }

    function getMasterData(id) {
        $.ajax({
            type: 'GET',
            url: base_url + 'marketing/data/master_sales_polis_byid/' + id,
            beforeSend: function (res) {
                loadingPage();
            },
            success: function (res) {
                console.log('master_data_edit',res);
                let data = res.rm;

                $('#get_id').val(id);

                $('#insured_name').val(data.insured_name);
                $('#endorse_reason').val(data.endors_reason);

                $('#no_of_insured').val(data.no_of_insured);
                
                if (data.policy_name) {
                    $('#policy_cust_name').val(data.policy_name);
                }
                else{
                    $('#policy_cust_name').val(data.full_name);
                }
                
                // $('#policy_cust_name').val(data.police_name);
                $('#no_polis').val(data.polis_no);
                $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
                $('#customer').val(data.customer_id).trigger('change.select2');
                $('#product').val(data.product_id).trigger('change.select2');
                $('#officer').val(data.agent_id).trigger('change.select2');
                $('#valuta_id').val(data.valuta_id).trigger('change.select2');
                $('#start_date_police').val(znFormatDateNum(data.start_date_polis));
                $('#end_date_police').val(znFormatDateNum(data.end_date_polis));
                // SELECT 2 WITOUH TRIGER
                // $('#coc_no').append(`<option value=`+data.coc_id+`>`+data.cf_no+`</option>`)
                $('#no_polis').select2('destroy');
                $('#no_polis').val(data.polis_no).select2();

                $('#insurance').val(formatMoney(data.ins_amount));
                $('#tax_amount').val(formatMoney(data.tax_amount));
                $('#fee_admin').val(formatMoney(data.admin_amount));
                $('#fee_materai').val(formatMoney(data.materai_amount));
                $('#fee_agent').val(formatMoney(data.agent_fee_amount));
                $('#asuransi').val(formatMoney(data.ins_fee));
                $('#nett_amount').val(formatMoney(data.net_amount));
                $('#fee_polis').val(formatMoney(data.polis_amount));
                $('#fee_internal').val(formatMoney(data.comp_fee_amount));
                $('#disc_amount').val(formatMoney(data.disc_amount));
                $('#premi').val(formatMoney(data.premi_amount));
                $('#ef').val(formatMoney(data.ef));
                $('#ef_pct').val(formatMoney(data.ef_pct));
                $('#coc_id').val(data.coc_id);


                var value = $('#valuta_id').val();
                var text = $("#valuta_id option:selected").text();
                $('.zn-rp-group').html(text);

                getDetailInsured('endors');
                // convertToRupiah1(data.total_sum_insured);
                endLoadingPage();
                $('#dataShow').fadeIn();

            }
        });
    }

    $(document).ready(function () {

        $('#no_polis').select2('destroy');
        $('#no_polis').val(null).select2();

        if (typeStore == 'edit') {
            getMasterData(getId);
            $('#dataShow').show();
        }else{

            $('#no_polis').val('{{$get_polis_no}}').trigger('change.select2');
            $('#dataShow').hide();
        }



        $("#form-policy").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                coc_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                no_polis: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                policy_cust_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                endorse_reason: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                insured_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

</script>
