<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\SalesScModel;
use App\Models\SalesModel;

class InstallmentController extends Controller
{
    public function index(Request $request)
    {
        if($request->get('type')=='new'){
            $tittle = 'New Installment List';
        }
        else if($request->get('type')=='approve') {
            $tittle = 'Approval New Installment';
        }
        else if($request->get('type')=='success') {
            $tittle = 'Active Installment List';
        }
        else if($request->get('type')=='approve_reversal') {
            $tittle = 'Approval Delete Installment';
        }
        else if($request->get('type')=='deleted') {
            $tittle = 'Deleted Installment';
        }

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.list', $param);
        }else {
            return view('master.master')->nest('child', 'installment.list', $param);
        }

    }

    public function scheduleList(Request $request)
    {

        $tittle = 'Approval Payment (Installment Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.schedule', $param);
        }else {
            return view('master.master')->nest('child', 'installment.schedule', $param);
        }

    }

    public function split_approval_list(Request $request)
    {

        $tittle = 'Approval Split Payment (Installment Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.split_approval', $param);
        }else {
            return view('master.master')->nest('child', 'installment.split_approval', $param);
        }

    }

    public function split_approval_list_reversal(Request $request)
    {

        $tittle = 'Approval Reversal Split Payment (Installment Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.split_approval_reversal', $param);
        }else {
            return view('master.master')->nest('child', 'installment.split_approval_reversal', $param);
        }

    }

    

    public function scheduleListCancel(Request $request)
    {

        $tittle = 'Approval Cancel Payment (installment Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.schedule_cancel', $param);
        }else {
            return view('master.master')->nest('child', 'installment.schedule_cancel', $param);
        }

    }

    public function scheduleListPaid(Request $request)
    {

        $tittle = 'Paid Installment Schedule';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.schedule_paid', $param);
        }else {
            return view('master.master')->nest('child', 'installment.schedule_paid', $param);
        }

    }

    public function scheduleListComplete(Request $request)
    {

        $tittle = 'Complete Payment Installment';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.schedule_complete', $param);
        }else {
            return view('master.master')->nest('child', 'installment.schedule_complete', $param);
        }

    }

    public function store_sc(Request $request)
    {

        try{

            SalesScModel::where('id_master_sales', $request->input('get_id'))->delete();

            $get_ms = collect(\DB::select("SELECT * FROM master_sales where id =".$request->input('get_id')))->first();

            
            $outstanding = $request->input('outstanding');
            $amor = $request->input('amor');
            $dateamor = $request->input('dateamor');
            
            $dateamordue = $request->input('dateamordue');
            $days_overdue = $request->input('days_overdue');

            $tenor = $request->input('tenor');


            $comm_due = $request->input('comm_due');
            $fee_agent = $request->input('fee_agent');
            $agent_tax = $request->input('agent_tax');
            $underwriter = $request->input('underwriter');
            $n_premi = $request->input('n_premi');


            for ($i=0; $i < $tenor; $i++) {
               $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_sales_schedule"))->first();

                $dsc = new SalesScModel();
                $dsc->id = $get->max_id+1;
                $dsc->id_master_sales = $request->input('get_id');
                $dsc->interval = $request->input('interval');
                $dsc->tenor = $tenor;
                $dsc->days_overdue = $days_overdue;

                $dsc->premi_amount = $this->clearSeparatorDouble($n_premi[$i]);
                $dsc->comp_fee_amount = $this->clearSeparatorDouble($comm_due[$i]);
                $dsc->agent_fee_amount = $this->clearSeparatorDouble($fee_agent[$i]);

                     
               
                $dsc->tax_amount = $this->clearSeparatorDouble($agent_tax[$i]);
                
                $dsc->underwriter_amount = $this->clearSeparatorDouble($underwriter[$i]);

                $dsc->net_amount = $this->clearSeparatorDouble($amor[$i]);
                $dsc->outstanding = $this->clearSeparatorDouble($outstanding[$i]);
                $dsc->date = $this->convertDate($dateamor[$i]);
                $dsc->due_date_jrn = $this->convertDate($dateamordue[$i]);

               
                
                // $dsc->ins_amount = $this->clearSeparatorDouble($n_premi[$i]);

                $discount_amount_config = collect(\DB::select("select value as jml from master_config where id = 9"))->first();

                // $dsc->disc_amount = $dsc->ins_amount;
                // $dsc->disc_amount = $dsc->ins_amount - $dsc->net_amount;

                $dsc->disc_amount = ($discount_amount_config->jml * $dsc->premi_amount) / 100;

                $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

                // INVOICE NO
                $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();
                $str=strlen($ref_invoice->next_no);
                if($str==3){
                   $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }elseif($str==2){
                   $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }else{
                    $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }

                DB::table('ref_invoice_no')
                ->where('company_id', Auth::user()->company_id)
                ->where('year', date('Y'))
                ->where('type',0)
                ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);


                if ($i == 0) {
                    $dsc->polis_amount = $get_ms->polis_amount;
                    $dsc->materai_amount = $get_ms->materai_amount;
                    $dsc->admin_amount = $get_ms->admin_amount;
                }else {
                    $dsc->polis_amount = 0;
                    $dsc->materai_amount = 0;
                    $dsc->admin_amount = 0;
                }          


                $dsc->inv_no = $no_invoice;
                $dsc->inv_date = $this->convertDate($dateamor[$i]);
                $dsc->afi_acc_no = $get_ms->afi_acc_no;
                $dsc->qs_no = $get_ms->qs_no;
                $dsc->qs_date = $get_ms->qs_date;
                $dsc->proposal_no = $get_ms->proposal_no;
                $dsc->proposal_date = $get_ms->proposal_date;
                $dsc->is_active = $get_ms->is_active;
                $dsc->branch_id = $get_ms->branch_id;
                $dsc->company_id = $get_ms->company_id;
                $dsc->notes = $get_ms->notes;
                
                $dsc->is_tax_company = $get_ms->is_tax_company;
                $dsc->ins_fee = $get_ms->ins_fee;
                $dsc->ef = $get_ms->ef;
                $dsc->ef_agent = $get_ms->ef_agent;
                $dsc->ef_company = $get_ms->ef_company;
                $dsc->polis_no = $get_ms->polis_no;
                $dsc->underwriter_id = $get_ms->underwriter_id;
                $dsc->invoice_type_id = $get_ms->invoice_type_id;
                $dsc->no_of_insured = $get_ms->no_of_insured;
                $dsc->url_dokumen = $get_ms->url_dokumen;
                $dsc->customer_id = $get_ms->customer_id;
                $dsc->product_id = $get_ms->product_id;
                $dsc->agent_id = $get_ms->agent_id;
                $dsc->ins_amount =  $get_ms->ins_amount;
               

                $dsc->paid_status_id = 0;
                $dsc->user_crt_id = Auth::user()->id;
                $dsc->user_upd_id = Auth::user()->id;
                $dsc->id_workflow = 1;
                $dsc->save();
            }

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function store(Request $request)
    {

        
       

        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {

            try{

                $results = \DB::select("SELECT max(id) AS id from master_sales");
                $id='';

                $get_id = $request->input('get_id');

                if ($get_id != '') {
                    SalesModel::where('id', $request->input('get_id'))->delete();
                    $id = $get_id;
                }else {
                    if($results){
                        $id=$results[0]->id + 1;
                    }else{
                        $id=1;
                    }
                }

                
               
                $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where id='.$request->input('quotation'));
                $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where id='.$request->input('proposal'));
                $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

                $ef_config = \DB::select("select value  as jml from master_config where id = 10");
                $ef_agent_config = \DB::select("select value  as jml from master_config where id = 11");
                $ef_company_config = \DB::select("select value  as jml from master_config where id = 12");

                $jml=str_replace('.', '', $request->input('premi'));


                $ef=$ef_config[0]->jml * str_replace(',', '.', $jml) / 100;
                $ef_agent=$ef_agent_config[0]->jml * $ef / 100;
                $ef_company=$ef_company_config[0]->jml * $ef / 100;

                $d_date = collect(\DB::select("SELECT romawi_no,day_of_month,month_actual,year_actual FROM d_date where date_actual='".date('Y-m-d')."'"))->first();

                $ref_invoice = collect(\DB::select("select next_no from ref_invoice_no where type=0 and company_id=".Auth::user()->company_id." and year='".date('Y')."'"))->first();


                $str=strlen($ref_invoice->next_no);
                if($str==3){

                   $no_invoice='MKT/II/'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;

                }elseif($str==2){
                   $no_invoice='MKT/II/0'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }else{
                    $no_invoice='MKT/II/00'.$ref_invoice->next_no.'/'.$d_date->day_of_month.$d_date->month_actual.'/'.$d_date->year_actual;
                }

                DB::table('ref_invoice_no')
                    ->where('company_id', Auth::user()->company_id)
                    ->where('year', date('Y'))
                    ->where('type',0)
                    ->update(['last_no' =>$ref_invoice->next_no,'next_no'=>$ref_invoice->next_no + 1]);

                $tglmulai=date('Y-m-d');
                $tglakhir = date('Y-m-d');



                $nilai=str_replace('.', '', $request->input('insurance'));
                
                if ( is_null($request->input('disc_amount')) ) {
                    $nilai1 = 0;
                } else {
                    $nilai1 = $this->clearSeparatorDouble($request->input('disc_amount'));
                }
                if ( is_null($request->input('premi')) ) {
                    $nilai5 = 0;
                } else {
                    $nilai5 = $this->clearSeparatorDouble($request->input('premi'));
                }
                if ( is_null($request->input('polis_amount')) ) {
                    $nilai6 = 0;
                } else {
                    $nilai6 = $this->clearSeparatorDouble($request->input('polis_amount'));
                }
                if ( is_null($request->input('materai_amount')) ) {
                    $nilai7 = 0;
                } else {
                    $nilai7 = $this->clearSeparatorDouble($request->input('materai_amount'));
                }
                if ( is_null($request->input('admin_amount')) ) {
                    $nilai8 = 0;
                } else {
                    $nilai8 = $this->clearSeparatorDouble($request->input('admin_amount'));
                }
                // $nilai1=str_replace('.', '', $request->input('disc_amount'));
                $nilai2=str_replace('.', '', $request->input('nett_amount'));
                $nilai3=str_replace('.', '', $request->input('insurance'));
                $nilai4=0;
                // $nilai5=str_replace('.', '', $request->input('premi'));
                // $nilai6=str_replace('.', '', $request->input('polis_amount'));
                // $nilai7=str_replace('.', '', $request->input('materai_amount'));
                // $nilai8=str_replace('.', '', $request->input('admin_amount'));
                $nilai9=0;
                $nilai10=0;
                $nilai11=str_replace('.', '', $request->input('no_of_insured'));


                if ( is_null($request->input('valas_amount')) ) {
                    $valasAmount = 0;
                } else {
                    $valasAmount = $this->clearSeparatorDouble($request->input('valas_amount'));
                }
                if ( is_null($request->input('today_kurs')) ) {
                    $today_kurs = 0;
                } else {
                    $today_kurs = $this->clearSeparatorDouble($request->input('today_kurs'));
                }


                if($request->file('file')){
                $destination_path = public_path('upload_invoice');
                $files = $request->file('file');
                $filename = $files->getClientOriginalName();
                $upload_success = $files->move($destination_path, $id."_".$filename);

                DB::table('master_sales')->insert(
                        [
                            'id' => $id,
                            'inv_date' => $systemDate->current_date,
                            'polis_no' => $request->input('no_polis'),
                            'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
                            'product_id' => $request->input('product'),
                            'ins_amount' => str_replace(',', '.', $nilai),
                            'disc_amount' => str_replace(',', '.', $nilai1),
                            // 'invoice_type_id' => 1,
                            // 'no_endors' => 1;

                            'net_amount' => str_replace(',', '.', $nilai2),
                            'agent_id' => $request->input('officer'),

                            'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                            'end_date' => $tglakhir,
                            'afi_acc_no' => $request->input('bank'),
                            'segment_id' => $request->input('segment'),
                            'agent_fee_amount' => str_replace(',', '.', $nilai3),

                            'comp_fee_amount' => str_replace(',', '.', $nilai4),
                            'premi_amount' => str_replace(',', '.', $nilai5),

                            'polis_amount' => str_replace(',', '.', $nilai6),

                            'materai_amount' => str_replace(',', '.', $nilai7),
                            'admin_amount' => str_replace(',', '.', $nilai8),
                            'tax_amount' => str_replace(',', '.', $nilai9),
                            'is_tax_company' => $request->input('is_tax_company'),
                            //'due_date_jrn' => $request->input('email'),
                            'paid_status_id' => 0,
                            'qs_no' => $request->input('quotation'),
                            'customer_id' => $request->input('customer'),
                            'qs_date' => $ref_quotation[0]->from_date,
                            'proposal_no' => $request->input('proposal'),

                            'underwriter_id' => $request->input('id_underwriter'),

                            'invoice_type_id' => $request->input('invoice_type_id'),

                            'proposal_date' => $ref_proposal[0]->from_date,
                            'is_active' => 1,
                            'wf_status_id' => 1,
                            'id_workflow' => 1,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            //'coverage_amount' => Auth::user()->branch_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'ins_fee' => str_replace(',', '.', $nilai10),
                            'no_of_insured' => str_replace(',', '.', $nilai11),
                            'ef' => $ef,
                            'ef_agent' => $ef_agent,
                            'ef_company' => $ef_company,
                            'url_dokumen' => $filename,
                            'valuta_id' => $request->input('valuta_id'),
                            'valuta_amount' => $valasAmount,
                            'kurs_today' =>  $today_kurs, 
                            'coc_id' =>  $request->input('coc_no'), 
                            'police_name' =>  $request->input('policy_cust_name')
                        ]
                    );
                }else{
                    DB::table('master_sales')->insert(
                        [
                            'id' => $id,
                            'inv_date' => $systemDate->current_date,
                            'polis_no' => $request->input('no_polis'),
                            'inv_no' => $request->input('invoice') ?  $request->input('invoice') : $no_invoice,
                            'product_id' => $request->input('product'),
                            'ins_amount' => str_replace(',', '.', $nilai),
                            'disc_amount' => str_replace(',', '.', $nilai1),
                            // 'invoice_type_id' => 1,
                            // 'no_endors' => 1;

                            'net_amount' => str_replace(',', '.', $nilai2),
                            'agent_id' => $request->input('officer'),

                            'start_date' => date('Y-m-d',strtotime($request->input('tgl_mulai'))),
                            'end_date' => $tglakhir,
                            'afi_acc_no' => $request->input('bank'),
                            'segment_id' => $request->input('segment'),
                            'agent_fee_amount' => str_replace(',', '.', $nilai3),

                            'comp_fee_amount' => str_replace(',', '.', $nilai4),
                            'premi_amount' => str_replace(',', '.', $nilai5),

                            'polis_amount' => str_replace(',', '.', $nilai6),

                            'materai_amount' => str_replace(',', '.', $nilai7),
                            'admin_amount' => str_replace(',', '.', $nilai8),
                            'tax_amount' => str_replace(',', '.', $nilai9),
                            'is_tax_company' => $request->input('is_tax_company'),
                            //'due_date_jrn' => $request->input('email'),
                            'paid_status_id' => 0,
                            'qs_no' => $request->input('quotation'),
                            'customer_id' => $request->input('customer'),
                            'qs_date' => $ref_quotation[0]->from_date,
                            'proposal_no' => $request->input('proposal'),

                            'underwriter_id' => $request->input('id_underwriter'),

                            'invoice_type_id' => $request->input('invoice_type_id'),

                            'proposal_date' => $ref_proposal[0]->from_date,
                            'is_active' => 1,
                            'wf_status_id' => 1,
                            'id_workflow' => 1,
                            'user_crt_id' => Auth::user()->id,
                            'branch_id' => Auth::user()->branch_id,
                            'company_id' => Auth::user()->company_id,
                            //'coverage_amount' => Auth::user()->branch_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'ins_fee' => str_replace(',', '.', $nilai10),
                            'no_of_insured' => str_replace(',', '.', $nilai11),
                            'ef' => $ef,
                            'ef_agent' => $ef_agent,
                            'ef_company' => $ef_company,
                            'valuta_id' => $request->input('valuta_id'),
                            'valuta_amount' => $valasAmount,
                            'kurs_today' =>  $today_kurs, 
                            'coc_id' =>  $request->input('coc_no'), 
                            'police_name' =>  $request->input('policy_cust_name')
                        ]
                    );


                }
                  return json_encode(['rc'=>1,'rm'=>'Success']);


                }
                catch (QueryException $e){

                    if($e->getCode() == '23505'){
                        $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
                    }else{
                        $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
                    }
                    return response()->json([
                        'rc' => 99,
                        'rm' => $response,
                        'msg' => $e->getMessage()
                    ]);
                }

        }


    }

    public function refinstallment(Request $request)
    {

        $id = $request->input('id');
        $data = \DB::select("SELECT * FROM ref_installment where id = '".$id."'");
        return json_encode($data);
    }

    public function form_installment($id,Request $request)
    {
        $ref_product = \DB::select('SELECT * FROM ref_product where is_active=true and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_bank = \DB::select('SELECT * FROM ref_bank_account where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_quotation = \DB::select('SELECT * FROM master_qs_vehicle where is_active=true');
        $ref_proposal = \DB::select('SELECT * FROM master_cn_vehicle where is_active=true');
        $ref_cust = \DB::select('SELECT * FROM ref_cust_segment where is_active=true  and company_id='.Auth::user()->company_id.' order by seq asc');
        $ref_agent = \DB::select('SELECT * FROM ref_agent where is_active=true order by seq asc');
        $ref_customer = \DB::select('SELECT * FROM master_customer where wf_status_id=9 and is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.'');
        $ref_underwriter = \DB::select('SELECT * FROM ref_underwriter where is_active=true and branch_id='.Auth::user()->branch_id.' and company_id='.Auth::user()->company_id.' order by seq asc');

        $param['master_sales_polis'] = \DB::select('SELECT a.coc_id as id,mcv.cf_no 
        FROM master_sales_polis a
        left join master_cf_vehicle mcv on mcv.id = a.coc_id  where invoice_type_id = 2  
        and a.coc_id not in (select DISTINCT coc_id from master_sales WHERE coc_id is not null and invoice_type_id = 2) and
        (a.wf_status_id = 9 and a.paid_status_id = 0) or 
        (a.wf_status_id = 13 and a.paid_status_id = 1) or
        (a.wf_status_id NOT IN (9, 16, 17, 18) and a.paid_status_id = 1)');


        $param['ref_customer']=$ref_customer;
        $param['ref_product']=$ref_product;
        $param['ref_bank']=$ref_bank;
        $param['ref_quotation']=$ref_quotation;
        $param['ref_proposal']=$ref_proposal;
        $param['ref_cust']=$ref_cust;
        $param['ref_agent']=$ref_agent;
        $param['ref_underwriter']=$ref_underwriter;

        if ($id == 'create') {
            $param['tittle'] = 'Create New Installment';
            $param['type'] = 'create';
            $param['get_id'] = '';
            $param['type_form'] = $request->get('type');


        }else {
            $param['type_form'] = $request->get('type');
            $param['get_id'] = $id;
            $param['tittle'] = 'Edit Installment';
            $mi = collect(\DB::select("SELECT master_sales.*,mcvvv.cf_no,msp.id as coc_polis FROM master_sales
            left join master_sales_polis msp on msp.id = master_sales.coc_id
            left join master_cf_vehicle mcvvv on mcvvv.id = msp.coc_id
            where master_sales.id = '".$id."'"))->first();
            $mc = \DB::select("SELECT * FROM master_inventory_schedule where inventory_id = ".$id." order by last_os desc");
            $mc_y_max = collect(\DB::select("SELECT max(year) as year_max FROM master_inventory_schedule where inventory_id = ".$id))->first();
            $mc_y_min = collect(\DB::select("SELECT min(year) as year_min FROM master_inventory_schedule where inventory_id = ".$id))->first();

            $param['mi'] = $mi;
            $param['mc'] = $mc;
            $param['mc_y_max'] = $mc_y_max;
            $param['mc_y_min'] = $mc_y_min;

            $param['type'] = 'edit';

        }


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.form',$param);
        }else {
            return view('master.master')->nest('child', 'installment.form',$param);
        }
    }

    public function form_sc($id,Request $request)
    {
        $param['tittle'] = 'Installment Schedule';
        $param['type'] = 'edit';
        $param['get_id'] = $id;
        $param['type_form'] = $request->get('type');

        $ms = collect(\DB::select("SELECT * FROM master_sales where id = '".$id."'"))->first();

        
        if ($ms->coc_id) {
            $ms_polis = collect(\DB::select("SELECT * FROM master_sales_polis where id = '".$ms->coc_id."'"))->first();
            $param['mc_polis'] = \DB::select("SELECT * FROM master_sales_schedule_polis where id_master_sales = ".$ms_polis->id." order by outstanding desc");
        }
       


        $mc = \DB::select("SELECT * FROM master_sales_schedule where id_master_sales = ".$id." order by outstanding desc");
        $mc_y_max = collect(\DB::select("SELECT max(year) as year_max FROM master_inventory_schedule where inventory_id = ".$id))->first();
        $mc_y_min = collect(\DB::select("SELECT min(year) as year_min FROM master_inventory_schedule where inventory_id = ".$id))->first();

        $param['ms'] = $ms;
        $param['mc'] = $mc;
        $param['mc_y_max'] = $mc_y_max;
        $param['mc_y_min'] = $mc_y_min;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.form_sc',$param);
        }else {
            return view('master.master')->nest('child', 'installment.form_sc',$param);
        }
    }

    public function installment_detail(Request $request,$id)
    {
        $mi = collect(\DB::select("SELECT master_sales.*,muc.fullname as membuat,mua.fullname as menyetujui FROM master_sales
        left join master_user muc on muc.id = master_sales.user_crt_id
        left join master_user mua on mua.id = master_sales.user_upd_id
        where master_sales.id = '".$id."'"))->first();

        $mc = \DB::select("SELECT * FROM master_sales_schedule where id_master_sales = ".$id." order by outstanding desc");


        $param['mi'] = $mi;
        $param['mc'] = $mc;
        $param['getId'] = $id;

        $param['type'] = $request->get('type');

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'installment.detail',$param);
        }else {
            return view('master.master')->nest('child', 'installment.detail',$param);
        }
    }


    public function data()
    {
       $data = \DB::select("SELECT msp.product_id as coc_product_id,a.coc_id,a.id,b.full_name,c.definition,a.premi_amount,
       d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,
       a.is_premi_paid,a.invoice_type_id,a.url_dokumen,short_code,a.kurs_today,mata_uang,rv.id as id_uang from master_sales a
       left join master_customer b on b.id=a.customer_id
       left join ref_product c on c.id=a.product_id
       left join ref_paid_status d on d.id=a.paid_status_id
             left join master_branch mb on mb.id = a.branch_id
             left join master_sales_polis msp on msp.id = a.coc_id
			 left join ref_valuta rv on rv.id = a.valuta_id where a.invoice_type_id = 2 and a.id_workflow in (1) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
       return DataTables::of($data)

       ->addColumn('action', function ($data) {
        
        $get_memo = $this->getListMemo($data->id,'master_sales');
         
        return '
        
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
          
           

                <a class="dropdown-item" href="#" onclick="loadNewPage(`'.route('installment.form',$data->id).'?type=new`)"><i class="la la-edit"></i> Ubah</a>
                <a class="dropdown-item" href="#" onClick="loadNewPage(`'.route('installment.form_sc',$data->id).'?type=new`)"><i class="la la-clipboard"></i> Installment Schedule</a>
                <a class="dropdown-item" href="#" onClick="detail_invoice_product(`'.$data->coc_product_id.'`,`'.$data->coc_id.'`)"><i class="la la-clipboard"></i> Detail Product</a>
                
                <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                    <i class="la la-sticky-note"></i>
                    <span>Memo</span>
                </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('premi_amount',function($data) {
            if ($data->id_uang == 1) {
                return number_format($data->premi_amount,2,',','.');
            }else{
                return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
            }
        })
        ->editColumn('invoice_type_id',function($data) {
            if($data->invoice_type_id==0){
                return 'Original';
            }
            elseif($data->invoice_type_id==1){
                return 'Endorsement';
            }
            else {
                return 'Installment';
            }
        })
        ->editColumn('url_dokumen',function($data) {
            return '<a target="_blank" href="'.asset("upload_invoice").'/'.$data->id.'_'.$data->url_dokumen.'">Download File</a>';
        })
        ->rawColumns(['cek', 'action','url_dokumen'])
        ->make(true);

    }

    public function data_approve()
    {
        $data = \DB::select("SELECT msp.product_id as coc_product_id,a.coc_id,a.id,b.full_name,c.definition,a.premi_amount,
        d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,
        a.is_premi_paid,a.invoice_type_id,a.url_dokumen,short_code,a.kurs_today,mata_uang,rv.id as id_uang from master_sales a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        left join ref_paid_status d on d.id=a.paid_status_id
              left join master_branch mb on mb.id = a.branch_id
              left join master_sales_polis msp on msp.id = a.coc_id
              left join ref_valuta rv on rv.id = a.valuta_id where a.invoice_type_id = 2 and a.id_workflow in (2) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
        return DataTables::of($data)
        ->addColumn('action', function ($data) {

            $get_memo = $this->getListMemo($data->id,'master_sales');
        
         return '
            <div class="dropdown dropdown-inline">
                <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-more"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" onclick="detail_invoice('.$data->id.')">
                <i class="la la-clipboard"></i>
                <span>Detail</span>
            </a>
            <a class="dropdown-item" href="#" onClick="detail_invoice_product(`'.$data->coc_product_id.'`,`'.$data->coc_id.'`)"><i class="la la-clipboard"></i> Detail Product</a>
               
                <a class="dropdown-item" onClick="loadNewPage(`'.route('installment.detail',$data->id).'?type=approve`)">
                    <i class="la la-clipboard"></i>
                    <span>Installment Schedule</span>
                </a>
                
                <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                    <i class="la la-sticky-note"></i>
                    <span>Memo</span>
                </a>
                
                </div>
            </div>
         ';
         })
         ->addColumn('cek', function ($data) {
             return '
             <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                 <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                 <span></span>
             </label>
             ';
             })
         ->editColumn('premi_amount',function($data) {
            if ($data->id_uang == 1) {
                return number_format($data->premi_amount,2,',','.');
            }else{
                return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
            }
         })
         ->editColumn('invoice_type_id',function($data) {
             if($data->invoice_type_id==0){
                 return 'Original';
             }
             elseif($data->invoice_type_id==1){
                 return 'Endorsement';
             }
             else {
                 return 'Installment';
             }
         })
         ->editColumn('url_dokumen',function($data) {
             return '<a target="_blank" href="'.asset("upload_invoice").'/'.$data->id.'_'.$data->url_dokumen.'">Download File</a>';
         })
         ->rawColumns(['cek', 'action','url_dokumen'])
         ->make(true);

    }

    public function data_success()
    {

        

        $data = \DB::select("SELECT msp.product_id as coc_product_id,a.coc_id,a.id,b.full_name,c.definition,a.premi_amount,
        d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,
        a.is_premi_paid,a.invoice_type_id,a.url_dokumen,short_code,a.kurs_today,mata_uang,rv.id as id_uang from master_sales a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        left join ref_paid_status d on d.id=a.paid_status_id
              left join master_branch mb on mb.id = a.branch_id
              left join master_sales_polis msp on msp.id = a.coc_id
              left join ref_valuta rv on rv.id = a.valuta_id where a.invoice_type_id = 2 and a.id_workflow in (9) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
        return DataTables::of($data)
        ->addColumn('action', function ($data) {

           $get_memo = $this->getListMemo($data->id,'master_sales');
            
         return '
         <div class="dropdown dropdown-inline">
             <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="flaticon-more"></i>
             </button>
             <div class="dropdown-menu dropdown-menu-right">
             <a class="dropdown-item" onclick="detail_invoice('.$data->id.')">
             <i class="la la-clipboard"></i>
             <span>Detail</span>
         </a>
         <a class="dropdown-item" href="#" onClick="detail_invoice_product(`'.$data->coc_product_id.'`,`'.$data->coc_id.'`)"><i class="la la-clipboard"></i> Detail Product</a>
               
             <a class="dropdown-item" onClick="loadNewPage(`'.route('installment.detail',$data->id).'?type=success`)">
                <i class="la la-clipboard"></i>
                <span>Installment Schedule</span>
            </a>
            <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                <i class="la la-sticky-note"></i>
                <span>Memo</span>
            </a>
             </div>
         </div>
         ';
         })
         ->addColumn('cek', function ($data) {
             return '
             <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                 <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                 <span></span>
             </label>
             ';
             })
         ->editColumn('paid',function($data) {

            $count_sc = collect(\DB::select("select count(id) as c from master_sales_schedule where id_master_sales = ".$data->id))->first();
            $count_sc_paid = collect(\DB::select("select count(id) as c from master_sales_schedule where paid_status_id = 1 and id_master_sales = ".$data->id))->first();

             return ' <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                            '.$count_sc_paid->c." / ".$count_sc->c.'
                        </span>';
         })
         ->editColumn('premi_amount',function($data) {
            if ($data->id_uang == 1) {
                return number_format($data->premi_amount,2,',','.');
            }else{
                return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
            }
        })
         ->editColumn('invoice_type_id',function($data) {
             if($data->invoice_type_id==0){
                 return 'Original';
             }
             elseif($data->invoice_type_id==1){
                 return 'Endorsement';
             }
             else {
                 return 'Installment';
             }
         })
         ->editColumn('url_dokumen',function($data) {
             return '<a target="_blank" href="'.asset("upload_invoice").'/'.$data->id.'_'.$data->url_dokumen.'">Download File</a>';
         })
         ->rawColumns(['cek', 'action','url_dokumen','paid'])
         ->make(true);

    }

    public function data_reversal()
    {
        $data = \DB::select("SELECT msp.product_id as coc_product_id,a.coc_id,a.id,b.full_name,c.definition,a.premi_amount,
        d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,
        a.is_premi_paid,a.invoice_type_id,a.url_dokumen,short_code,a.kurs_today,mata_uang,rv.id as id_uang from master_sales a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        left join ref_paid_status d on d.id=a.paid_status_id
              left join master_branch mb on mb.id = a.branch_id
              left join master_sales_polis msp on msp.id = a.coc_id
              left join ref_valuta rv on rv.id = a.valuta_id where a.invoice_type_id = 2 and a.id_workflow in (3) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
         return '
        -
         ';
         })
         ->addColumn('cek', function ($data) {
             return '
             <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                 <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                 <span></span>
             </label>
             ';
             })
         ->editColumn('premi_amount',function($data) {
            if ($data->id_uang == 1) {
                return number_format($data->premi_amount,2,',','.');
            }else{
                return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
            }
         })
         ->editColumn('invoice_type_id',function($data) {
             if($data->invoice_type_id==0){
                 return 'Original';
             }
             elseif($data->invoice_type_id==1){
                 return 'Endorsement';
             }
             else {
                 return 'Installment';
             }
         })
         ->editColumn('url_dokumen',function($data) {
             return '<a target="_blank" href="'.asset("upload_invoice").'/'.$data->id.'_'.$data->url_dokumen.'">Download File</a>';
         })
         ->rawColumns(['cek', 'action','url_dokumen'])
         ->make(true);

    }

    public function data_schedule()
    {
       $data = \DB::select("SELECT mi.id as mid,master_sales.id as sales_id, mi.*,full_name,short_code,kurs_today,mata_uang,rv.id as id_uang
       FROM master_sales_schedule mi
       left join master_sales on master_sales.id = mi.id_master_sales
			 left join master_customer on master_customer.id = mi.customer_id
			  left join master_branch mb on mb.id = master_sales.branch_id
			 left join ref_valuta rv on rv.id = master_sales.valuta_id
       where mi.id_workflow = 2 order by mi.outstanding desc");
       return DataTables::of($data)
       ->addColumn('cek', function ($data) {
        return '
        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
            <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
            <span></span>
        </label>
        ';
        })
        ->addColumn('action', function ($data) {
            $get_memo = $this->getListMemo($data->mid,'master_sales_schedule');
      
            return '
            <div class="dropdown dropdown-inline">
             <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="flaticon-more"></i>
             </button>
             <div class="dropdown-menu dropdown-menu-right">
             <a class="dropdown-item" onclick="detail_invoice_schedule('.$data->mid.')">
                <i class="la la-clipboard"></i>
                <span>Detail</span>
            </a>
            <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                <i class="la la-sticky-note"></i>
                <span>Memo</span>
            </a>
             </div>
         </div>
         
            ';
            })
        ->editColumn('ins_amount',function($data) {
            return number_format($data->net_amount,2,',','.');
        })
        ->editColumn('net_amount',function($data) {
            return number_format($data->net_amount,2,',','.');
        })
        ->editColumn('premi_amount',function($data) {
            return number_format($data->premi_amount,2,',','.');
        })
        ->editColumn('outstanding',function($data) {
            return number_format($data->outstanding,2,',','.');
        })
        ->editColumn('comp_fee_amount',function($data) {
            return number_format($data->comp_fee_amount,2,',','.');
        })
        ->editColumn('agent_fee_amount',function($data) {
            return number_format($data->agent_fee_amount,2,',','.');
        })
        ->editColumn('tax_amount',function($data) {
            return number_format($data->tax_amount,2,',','.');
        })
        ->editColumn('underwriter_amount',function($data) {
            return number_format($data->underwriter_amount,2,',','.');
        })
        ->editColumn('date',function($data) {
            return date('d-m-Y',strtotime($data->date));
        })
        ->rawColumns(['cek','action'])
        ->make(true);

    }

    public function data_split_approval()
    {
       $data = \DB::select("select id,polis_no,full_name,definition,keterangan,total,is_pilih from(

        select a.id,polis_no,b.full_name,c.definition,'agent' as keterangan,is_agent_paid as is_pilih,
        case
        when is_agent_paid=2 then agent_fee_amount
        else
        0
        end as total
        from master_sales_schedule a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        where a.id_workflow=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and is_agent_paid=2


        union all

        select a.id,polis_no,b.full_name,c.definition,'commision' as keterangan,is_company_paid as is_pilih,
        case
        when is_company_paid=2 then comp_fee_amount+admin_amount
        else
        0
        end as total
        from master_sales_schedule a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        where a.id_workflow=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and is_company_paid=2

        union all

        select a.id,polis_no,b.full_name,c.definition,'premium' as keterangan,is_premi_paid as is_pilih,
        case
        when is_premi_paid=2 then underwriter_amount+polis_amount+materai_amount
        else
        0
        end as total
        from master_sales_schedule a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        where a.id_workflow=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and is_premi_paid=2

        ) as y

        ");
       return DataTables::of($data)
       ->addColumn('cek', function ($data) {
        return '
        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
            <input type="checkbox" value="'.$data->id.'|'.$data->keterangan.'" class="kt-group-checkable">
            <span></span>
        </label>
        ';
        })
        ->editColumn('total',function($data) {
            return number_format($data->total,2,',','.');
        })
        ->addColumn('action', function ($data) {
            return '
            <a class="btn btn-success btn-sm btn-elevate btn-elevate-air" style="height: 23px;padding: 2px 10px;" 
            href="#" onclick="detail_invoice_schedule('.$data->id.')">
            <i class="la la-clipboard"></i> Detail</a>
            ';
            })
        ->rawColumns(['cek','action'])
        ->make(true);

    }

    public function data_split_approval_reversal()
    {
       $data = \DB::select("select id,polis_no,full_name,definition,keterangan,total,is_pilih from(

        select a.id,polis_no,b.full_name,c.definition,'agent' as keterangan,is_agent_paid as is_pilih,
        case
        when is_agent_paid=3 then agent_fee_amount
        else
        0
        end as total
        from master_sales_schedule a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        where a.id_workflow=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and is_agent_paid=3


        union all

        select a.id,polis_no,b.full_name,c.definition,'commision' as keterangan,is_company_paid as is_pilih,
        case
        when is_company_paid=3 then comp_fee_amount+admin_amount
        else
        0
        end as total
        from master_sales_schedule a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        where a.id_workflow=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and is_company_paid=3

        union all

        select a.id,polis_no,b.full_name,c.definition,'premium' as keterangan,is_premi_paid as is_pilih,
        case
        when is_premi_paid=3 then underwriter_amount+polis_amount+materai_amount
        else
        0
        end as total
        from master_sales_schedule a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        where a.id_workflow=9 and paid_status_id=1 and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id." and is_premi_paid=3

        ) as y

        ");
       return DataTables::of($data)
       ->addColumn('cek', function ($data) {
        return '
        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
            <input type="checkbox" value="'.$data->id.'|'.$data->keterangan.'" class="kt-group-checkable">
            <span></span>
        </label>
        ';
        })
        ->editColumn('total',function($data) {
            return number_format($data->total,2,',','.');
        })
        ->addColumn('action', function ($data) {
            return '
            <a class="btn btn-success btn-sm btn-elevate btn-elevate-air" style="height: 23px;padding: 2px 10px;" 
            href="#" onclick="detail_invoice_schedule('.$data->id.')">
            <i class="la la-clipboard"></i> Detail</a>
            ';
            })
        ->rawColumns(['cek','action'])
        ->make(true);

    }

    public function data_schedule_cancel()
    {
        $data = \DB::select("SELECT mi.id as mid,master_sales.id as sales_id, mi.*,full_name
        FROM master_sales_schedule mi
        left join master_sales on master_sales.id = mi.id_master_sales
              left join master_customer on master_customer.id = mi.customer_id
        where mi.id_workflow = 14 order by mi.outstanding desc");
        return DataTables::of($data)
        ->addColumn('cek', function ($data) {
         return '
         <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
             <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
             <span></span>
         </label>
         ';
         })
         ->editColumn('ins_amount',function($data) {
            return number_format($data->net_amount,2,',','.');
        })
         ->editColumn('net_amount',function($data) {
             return number_format($data->net_amount,2,',','.');
         })
         ->editColumn('outstanding',function($data) {
             return number_format($data->outstanding,2,',','.');
         })
         ->editColumn('comp_fee_amount',function($data) {
            return number_format($data->comp_fee_amount,2,',','.');
        })
        ->editColumn('agent_fee_amount',function($data) {
            return number_format($data->agent_fee_amount,2,',','.');
        })
        ->editColumn('tax_amount',function($data) {
            return number_format($data->tax_amount,2,',','.');
        })
        ->editColumn('underwriter_amount',function($data) {
            return number_format($data->underwriter_amount,2,',','.');
        })
         ->editColumn('date',function($data) {
             return date('d-m-Y',strtotime($data->date));
         })
         ->rawColumns(['cek'])
         ->make(true);

    }

    public function data_schedule_paid()
    {
        $data = \DB::select("SELECT mi.id as mid,master_sales.id as sales_id, mi.*,full_name
        FROM master_sales_schedule mi
        left join master_sales on master_sales.id = mi.id_master_sales
        left join master_customer on master_customer.id = mi.customer_id
        where mi.id_workflow not in (2,12,14) and mi.paid_status_id = 1 order by mi.id desc");
        return DataTables::of($data)
        ->addColumn('cek', function ($data) {
         return '
         <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
             <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
             <span></span>
         </label>
         ';
         })
         ->addColumn('action', function ($data) {
            $btn_agent = '';
            $btn_company = '';
            $btn_premi = '';
            $btn_all = '';
            
            if (Auth::user()->user_role_id == 1){

        
            if ($data->is_agent_paid == null) {
                $btn_agent =  '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=is_agent_paid&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                    <i class="la la-money"></i>
                    <span>Paid Agent Fee</span>
                </a>';           
            }elseif ($data->is_agent_paid == 9) {
                $btn_agent =  '<a class="dropdown-item" 
                onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=is_agent_paid&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                <i class="la la-money" style="color:#eb5c28;"></i>
                <span style="color:#eb5c28;">Reversal Agent Fee</span>
            </a>';     
            } else {
                $btn_agent = '';
            }

            if ($data->is_company_paid == null) {
                $btn_company =   '<a class="dropdown-item" 
                onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=is_company_paid&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                    <i class="la la-money"></i>
                    <span>Paid Commision</span>
                </a>';           
            }elseif ($data->is_company_paid == 9) {
                $btn_company =  '<a class="dropdown-item" 
                onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=is_company_paid&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                <i class="la la-money" style="color:#eb5c28;"></i>
                <span style="color:#eb5c28;">Reversal Commision</span>
            </a>';     
            }else {
                $btn_company = '';
            }

            if ($data->is_premi_paid == null) {
                $btn_premi =   '<a class="dropdown-item" 
                onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=is_premi_paid&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                    <i class="la la-money"></i>
                    <span>Paid Premium</span>
                </a>';           
            }elseif ($data->is_premi_paid == 9) {
                $btn_premi =  '<a class="dropdown-item" 
                onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=is_premi_paid&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                <i class="la la-money" style="color:#eb5c28;"></i>
                <span style="color:#eb5c28;">Reversal Premium</span>
            </a>';     
            }else {
                $btn_premi = '';
            }

            if ($data->is_agent_paid == null && $data->is_company_paid == null && $data->is_premi_paid == null) {
                $btn_all = '<a class="dropdown-item" 
                onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=all&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                    <i class="la la-money"></i>
                    <span>Paid All</span>
                </a>'; 
            }elseif ($data->is_agent_paid == 9 && $data->is_company_paid == 9 && $data->is_premi_paid == 9) {
                $btn_all = '<a class="dropdown-item" 
                onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=all&setid='.$data->mid.'`,`'.route("data_schedule_paid.installment").'`);">
                    <i class="la la-money" style="color:#eb5c28;"></i>
                    <span style="color:#eb5c28;">Reversal All</span>
                </a>'; 
            } else {
                $btn_all = '';
            }

        }
            
            
            $get_memo = $this->getListMemo($data->mid,'master_sales_schedule');
        

            return '
            <div class="dropdown dropdown-inline">
                <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-more"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                    <i class="la la-sticky-note"></i>
                    <span>Memo</span>
                </a>
                    <a class="dropdown-item" onclick="detail_invoice_schedule('.$data->mid.')">
                        <i class="la la-clipboard"></i>
                        <span>Detail</span>
                    </a>'.$btn_agent.$btn_company.$btn_premi.$btn_all.'
                </div>
            </div>
            ';
        })
         ->editColumn('premi_amount',function($data) {
            return number_format($data->premi_amount,2,',','.');
        })
         ->editColumn('net_amount',function($data) {
             return number_format($data->net_amount,2,',','.');
         })
         ->editColumn('outstanding',function($data) {
             return number_format($data->outstanding,2,',','.');
         })
         ->editColumn('is_agent_paid',function($data) {
             if ($data->is_agent_paid == 2) {
                return '  <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                            Approval (Paid)
                        </span>';
             }elseif ($data->is_agent_paid == 9) {
                return '<span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                        Paid
                        </span>';
             }elseif ($data->is_agent_paid == 3) {
                return '<span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                        Approval (Reversal)
                        </span>';
             }else {
                return '-';
             }

            })
        ->editColumn('is_company_paid',function($data) {
            if ($data->is_company_paid == 2) {
                return '  <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                Approval (Paid)
                        </span>';
             }elseif ($data->is_company_paid == 3) {
                return '<span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                Approval (Reversal)
                        </span>';
             }elseif ($data->is_company_paid == 9) {
                return '<span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                        Paid
                        </span>';
             }else {
                return '-';
             }
        })
        ->editColumn('is_premi_paid',function($data) {
            if ($data->is_premi_paid == 2) {
                return '  <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                Approval (Paid)
                        </span>';
             }elseif ($data->is_premi_paid == 3) {
                return '<span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                Approval (Reversal)
                        </span>';
             }elseif ($data->is_premi_paid == 9) {
                return '<span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                        Paid
                        </span>';
             }else {
                return '-';
             }
        })
        
         ->editColumn('date',function($data) {
             return date('d-m-Y',strtotime($data->date));
         })
         ->rawColumns(['cek','action','is_agent_paid','is_company_paid','is_premi_paid'])
         ->make(true);

    }

    public function data_schedule_complete()
    {
        $data = \DB::select("SELECT mi.id as mid,master_sales.id as sales_id, mi.*,full_name
        FROM master_sales_schedule mi
        left join master_sales on master_sales.id = mi.id_master_sales
        left join master_customer on master_customer.id = mi.customer_id
        where mi.id_workflow not in (2,12,14) and mi.paid_status_id = 1 
        and mi.is_agent_paid = 9 and mi.is_company_paid = 9 and mi.is_premi_paid = 9 order by mi.id desc");
        return DataTables::of($data)
        ->addColumn('cek', function ($data) {
         return '
         <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
             <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
             <span></span>
         </label>
         ';
         })
         ->addColumn('action', function ($data) {
            $btn_agent = '';
            $btn_company = '';
            $btn_premi = '';
            $btn_all = '';
            
            if (Auth::user()->user_role_id == 1) {
                if ($data->is_agent_paid == null) {
                    $btn_agent =  '<a class="dropdown-item" 
                        onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=is_agent_paid&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                        <i class="la la-money"></i>
                        <span>Paid Agent Fee</span>
                    </a>';           
                }elseif ($data->is_agent_paid == 9) {
                    $btn_agent =  '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=is_agent_paid&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                    <i class="la la-money" style="color:#eb5c28;"></i>
                    <span style="color:#eb5c28;">Reversal Agent Fee</span>
                </a>';     
                } else {
                    $btn_agent = '';
                }
                
                if ($data->is_company_paid == null) {
                    $btn_company =   '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=is_company_paid&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                        <i class="la la-money"></i>
                        <span>Paid Commision</span>
                    </a>';           
                }elseif ($data->is_company_paid == 9) {
                    $btn_company =  '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=is_company_paid&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                    <i class="la la-money" style="color:#eb5c28;"></i>
                    <span style="color:#eb5c28;">Reversal Commision</span>
                </a>';     
                }else {
                    $btn_company = '';
                }

                if ($data->is_premi_paid == null) {
                    $btn_premi =   '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=is_premi_paid&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                        <i class="la la-money"></i>
                        <span>Paid Premium</span>
                    </a>';           
                }elseif ($data->is_premi_paid == 9) {
                    $btn_premi =  '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=is_premi_paid&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                    <i class="la la-money" style="color:#eb5c28;"></i>
                    <span style="color:#eb5c28;">Reversal Premium</span>
                </a>';     
                }else {
                    $btn_premi = '';
                }

                if ($data->is_agent_paid == null && $data->is_company_paid == null && $data->is_premi_paid == null) {
                    $btn_all = '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplit?type=all&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                        <i class="la la-money"></i>
                        <span>Paid All</span>
                    </a>'; 
                }elseif ($data->is_agent_paid == 9 && $data->is_company_paid == 9 && $data->is_premi_paid == 9) {
                    $btn_all = '<a class="dropdown-item" 
                    onclick="gActAll(`split`,`/kirimAprovalInstallmentSplitRev?type=all&setid='.$data->mid.'`,`'.route("data_schedule_complete.installment").'`);">
                        <i class="la la-money" style="color:#eb5c28;"></i>
                        <span style="color:#eb5c28;">Reversal All</span>
                    </a>'; 
                } else {
                    $btn_all = '';
                }
            }

    

          
            
            $get_memo = $this->getListMemo($data->mid,'master_sales_schedule');
        


            return '
            <div class="dropdown dropdown-inline">
                <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-more"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                    <i class="la la-sticky-note"></i>
                    <span>Memo</span>
                </a>
                    <a class="dropdown-item" onclick="detail_invoice_schedule('.$data->mid.')">
                        <i class="la la-clipboard"></i>
                        <span>Detail</span>
                    </a>'.$btn_agent.$btn_company.$btn_premi.$btn_all.'
                </div>
            </div>
            ';
        })
         ->editColumn('premi_amount',function($data) {
            return number_format($data->premi_amount,2,',','.');
        })
         ->editColumn('net_amount',function($data) {
             return number_format($data->net_amount,2,',','.');
         })
         ->editColumn('outstanding',function($data) {
             return number_format($data->outstanding,2,',','.');
         })
         ->editColumn('is_agent_paid',function($data) {
             if ($data->is_agent_paid == 2) {
                return '  <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                            Waiting Approval
                        </span>';
             }elseif ($data->is_agent_paid == 9) {
                return '<span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                        Paid
                        </span>';
             }else {
                return '-';
             }

            })
        ->editColumn('is_company_paid',function($data) {
            if ($data->is_company_paid == 2) {
                return '  <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                            Waiting Approval
                        </span>';
             }elseif ($data->is_company_paid == 9) {
                return '<span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                        Paid
                        </span>';
             }else {
                return '-';
             }
        })
        ->editColumn('is_premi_paid',function($data) {
            if ($data->is_premi_paid == 2) {
                return '  <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                            Waiting Approval
                        </span>';
             }elseif ($data->is_premi_paid == 9) {
                return '<span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                        Paid
                        </span>';
             }else {
                return '-';
             }
        })
        
         ->editColumn('date',function($data) {
             return date('d-m-Y',strtotime($data->date));
         })
         ->rawColumns(['cek','action','is_agent_paid','is_company_paid','is_premi_paid'])
         ->make(true);

    }

    public function data_deleted()
    {
        $data = \DB::select("SELECT msp.product_id as coc_product_id,a.coc_id,a.id,b.full_name,c.definition,a.premi_amount,
        d.definition as paid,a.polis_no,a.is_agent_paid,a.is_company_paid,
        a.is_premi_paid,a.invoice_type_id,a.url_dokumen,short_code,a.kurs_today,mata_uang,rv.id as id_uang from master_sales a
        left join master_customer b on b.id=a.customer_id
        left join ref_product c on c.id=a.product_id
        left join ref_paid_status d on d.id=a.paid_status_id
              left join master_branch mb on mb.id = a.branch_id
              left join master_sales_polis msp on msp.id = a.coc_id
              left join ref_valuta rv on rv.id = a.valuta_id where a.id_workflow in (12) and a.branch_id=".Auth::user()->branch_id." and a.company_id=".Auth::user()->company_id);
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            $get_memo = $this->getListMemo($data->id,'master_sales');
        
         return '
         <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" onclick="detail_invoice('.$data->id.')">
            <i class="la la-clipboard"></i>
            <span>Detail</span>
        </a>
        <a class="dropdown-item" href="#" onClick="detail_invoice_product(`'.$data->coc_product_id.'`,`'.$data->coc_id.'`)"><i class="la la-clipboard"></i> Detail Product</a>
               
                <a class="dropdown-item" onClick="znIconbox(`List Memo`,`'.$get_memo[0].'`,`'.$get_memo[1].'`,`memo`);">
                    <i class="la la-sticky-note"></i>
                    <span>Memo</span>
                </a>
            </div>
        </div>
         ';
         })
         ->addColumn('cek', function ($data) {
             return '
             <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                 <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                 <span></span>
             </label>
             ';
             })
         ->editColumn('premi_amount',function($data) {
            if ($data->id_uang == 1) {
                return number_format($data->premi_amount,2,',','.');
            }else{
                return number_format(($data->premi_amount/$data->kurs_today),2,',','.');
            }
         })
         
         ->editColumn('invoice_type_id',function($data) {
             if($data->invoice_type_id==0){
                 return 'Original';
             }
             elseif($data->invoice_type_id==1){
                 return 'Endorsement';
             }
             else {
                 return 'Installment';
             }
         })
         ->editColumn('url_dokumen',function($data) {
             return '<a target="_blank" href="'.asset("upload_invoice").'/'.$data->id.'_'.$data->url_dokumen.'">Download File</a>';
         })
         ->rawColumns(['cek', 'action','url_dokumen'])
         ->make(true);

    }


    public function kirimAproval(Request $request){
        // dd($request->value);

        $datas = [];
        
        if ($request->get('type') == 'semua') {
            $getMs = \DB::select("select * from master_sales where id_workflow in (1) and invoice_type_id = 2");

            foreach ($getMs as $key => $getId) {
                $datas[$key] = $getId->id; 
            }

            // dd($datas);
        }else {
            $datas = $request->value;   
        }

        $notAllow = '';
        $notAllowCount = 0;

        foreach ($datas as $v) {
            if ($v != null) {
                $checkSc = \DB::select("select * from master_sales_schedule where id_master_sales = ".$v);
            
                if (count($checkSc) == 0) {
                    $notAllow .= $v.",";
                    $notAllowCount ++;
                }
            }
        
        }
        
        if ($notAllowCount > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => $notAllowCount." Data Installment Dengan ID ".$notAllow." Belum Memiliki Installment Schedule"
            ]);
        }else {
            $this->actSendApproval('master_sales',$request->get('type'),$request->value,'');
            
        }
    }


    public function giveAproval(Request $request){
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
            $this->actGiveApproval('master_sales',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function hapusAproval(Request $request){
        $this->actDeleteApproval('master_sales',$request->get('type'),$request->value);
    }

    public function rejectAproval(Request $request){
        $this->actRejectApproval('master_sales',$request->get('type'),$request->value,$request->SetMemo);
    }

    // SCHEDULE
    public function kirimAprovalSc(Request $request){
        $this->actSendApproval('master_sales_schedule',$request->get('type'),$request->value,'');
    }

    public function giveAprovalSc(Request $request){
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
            $this->actGiveApproval('master_sales_schedule',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function rejectAprovalSc(Request $request){
        $this->actRejectApproval('master_sales_schedule',$request->get('type'),$request->value,$request->SetMemo);
    }

    public function CancelPaySc(Request $request)
    {
        $this->actCancelPaySc('master_sales_schedule',$request->get('type'),$request->value);
    }

    public function GiveApprovalCancelPaySc(Request $request)
    {
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
            $this->actGiveApprovalCancelPaySc('master_sales_schedule',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function RejectCancelPayment(Request $request)
    {
        $this->actRejectCancelPayment('master_sales_schedule',$request->get('type'),$request->value);
    }

    // DELETE REVERSAL
    public function hapusAprovalRev(Request $request){


        if ($request->get('type') == 'semua') {
            $get_sc = \DB::select("SELECT master_sales.id FROM master_sales_schedule
            left join master_sales on master_sales.id = master_sales_schedule.id_master_sales where master_sales_schedule.paid_status_id = 1
            and master_sales.id_workflow = 9
            GROUP BY master_sales.id");
        }else {
            $getId = $request->value;
            $getWhere = '';
            $i = 0;
            $len = count($getId);

            foreach ($getId as $item) {

                    if ($i == $len - 1) {
                        $getWhere .= $item;
                    }else{
                        if($item != null){
                            $getWhere .= $item.",";
                        }

                    }

                $i++;
            }

            $get_sc = \DB::select("SELECT master_sales.id FROM master_sales_schedule
            left join master_sales on master_sales.id = master_sales_schedule.id_master_sales where master_sales_schedule.paid_status_id = 1
            and master_sales.id_workflow = 9 and master_sales.id in (".$getWhere.")
            GROUP BY master_sales.id");

        }

        if ($get_sc) {
            $text_warning = 'Tidak Bisa Melakukan Aksi Delete, Terdapat Status Paid Pada Installment Schedule : ';

            foreach ($get_sc as $key => $value) {
                $text_warning .= '<br> '.($key+1).'. '.$value->id;
            }
            return response()->json([
                'rc' => 88,
                'rm' => $text_warning
            ]);
        }else{
            $this->actDeleteApprovalRev('master_sales',$request->get('type'),$request->value);
        }

    }
    public function giveHapusAprovalRev(Request $request){
        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
            $this->actGiveDeleteApprovalRev('master_sales',$request->get('type'),$request->value,$request->SetMemo);
        }
    }
    public function rejectHapusAprovalRev(Request $request){
        $this->actRejectDeleteApprovalRev('master_sales',$request->get('type'),$request->value);
    }

    // SPLIT PAYMENT

    
    public function kirimAprovalSplit(Request $request){
      
        if ($request->get('type') == 'all') {

            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update(['is_agent_paid' => 2]);

            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update(['is_company_paid' => 2]);

            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update(['is_premi_paid' => 2]);


        }else {
            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update([$request->get('type') => 2]);
        }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        
    }

    public function kirimAprovalSplitRev(Request $request){
      
        if ($request->get('type') == 'all') {

            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update(['is_agent_paid' => 3]);

            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update(['is_company_paid' => 3]);

            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update(['is_premi_paid' => 3]);


        }else {
            DB::table('master_sales_schedule')
            ->where('id', $request->get('setid'))
            ->update([$request->get('type') => 3]);
        }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        
    }

    public function giveAprovalSplit(Request $request){

        // dd($request->value);

        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
            $this->actGiveApprovalSplit('master_sales_schedule',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function giveAprovalSplitRev(Request $request){

        // dd($request->value);

        $checkBranch = \DB::select("select * from master_branch where is_open = 'f' and id = ".Auth::user()->branch_id);
        if (count($checkBranch) > 0) {
            return response()->json([
                'rc' => 88,
                'rm' => "Branch Operational is Closed. Transaction is not allowed"
            ]);
        }else {
            $this->actGiveApprovalSplitRev('master_sales_schedule',$request->get('type'),$request->value,$request->SetMemo);
        }
    }

    public function rejectAprovalSplit(Request $request){
        $this->actRejectApprovalSplit('master_sales_schedule',$request->get('type'),$request->value,$request->SetMemo);
    }

    public function rejectAprovalSplitRev(Request $request){
        $this->actRejectApprovalSplitRev('master_sales_schedule',$request->get('type'),$request->value,$request->SetMemo);
    }


}
