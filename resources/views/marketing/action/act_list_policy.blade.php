<script>
     // LIST TABLE

        $('#example-select-all').click(function (e) {
            $('input[type="checkbox"]').prop('checked', this.checked);
        });

        var typeList = '{{$typeList}}';
        var typeStore = 'detail_list';

        if (typeList == 'new') {
            var act_url = '{{ route('marketing.data',["list_policy","new"]) }}';
        }
        else if(typeList == 'approve'){
            var act_url = '{{ route('marketing.data',["list_policy","approve"]) }}';
        }
        else if(typeList == 'success'){
            var act_url = '{{ route('marketing.data',["list_policy","success"]) }}';
        }


        var table = $('#zn-dt').DataTable({
            aaSorting: [],
            processing: true,
            serverSide: true,
            columnDefs: [
                { targets: [6], className: 'text-center' },
                { "orderable": false, "targets": 0 }],
            ajax: {
                "url" : act_url,
                "error": function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                    }
                },
            columns: [
                { data: 'cek', name: 'cek' },
                { data: 'action', name: 'action' },
                { data: 'id', name: 'id' },
                { data: 'full_name', name: 'full_name' },
                { data: 'short_code', name: 'short_code' },
                { data: 'definition', name: 'definition' },
                { data: 'mata_uang', name: 'mata_uang' },
                { data: 'premi_amount', name: 'premi_amount' },
                { data: 'polis_no', name: 'polis_no' },
                { data: 'invoice_type_id', name: 'invoice_type_id' },
                { data: 'stat', name: 'stat' }
            ]
        });

    // LIST TALBE


function notes(id){
     var table = $('#notes_table').DataTable({
        aaSorting: [],
        destroy: true,
        processing: true,
        serverSide: true,
        columnDefs: [
            { "orderable": false, "targets": 0 }],
        ajax: {
            "url" : base_url + '/history_notes?id=' + id,
            "error": function(jqXHR, textStatus, errorThrown)
                {
                    toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                }
            },
        columns: [
            { data: 'ref_code_type', name: 'ref_code_type' },
            { data: 'fullname', name: 'fullname' },
            { data: 'created_at', name: 'created_at' },
            { data: 'notes', name: 'notes' },

        ]
    });
   $('#modal-notes').modal('show');
}


function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}
</script>
