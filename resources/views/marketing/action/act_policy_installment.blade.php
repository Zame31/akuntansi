<script>

    var typeStore = '{{$typeStore}}';
    var getId = '{{$getId}}';
    var typeForm = 'installment';

    function setCOC(id) {
        $.ajax({
            type: 'GET',
            url: base_url + 'marketing/data/policy/' + id,
            beforeSend: function (res) {
                loadingPage();
            },
            success: function (res) {
                console.log(res);
                let data = res.rm;
                $('#insurance').val(formatMoney(data.total_sum_insured));
                $('#no_of_insured').val(data.no_of_insured);
                $('#policy_cust_name').val(data.policy_name);
                // select 2
                $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
                $('#customer').val(data.customer_id).trigger('change.select2');
                $('#product').val(data.product_id).trigger('change.select2');
                $('#officer').val(data.agent_id).trigger('change.select2');
                $('#valuta_id').val(data.valuta_id).trigger('change.select2');

                //date
                $('#start_date_police').val(znFormatDateNum(data.from_date));
                $('#end_date_police').val(znFormatDateNum(data.to_date));

                var text =  $( "#valuta_id option:selected" ).text();
                $('.zn-rp-group').html(text);

                getDetailInsured('installment');
                // convertToRupiah1(data.total_sum_insured);
                endLoadingPage();
                $('#dataShow').fadeIn();

            }
        });
    }
    
    function pilihCOC(coc_id) {
        $('#coc_no').val(coc_id).trigger('change.select2');
        $('#modal-search-policy').modal('hide');
    }
    
    function searchPolicy() {

        $('#title_modal_seach').html('List Data Master Vechile');

        var act_url = base_url + 'marketing/data/search_policy_table/all';

            var tableSearchPolicy = $('#search_policy_table').DataTable({
                aaSorting: [],
                processing: true,
                serverSide: true,
                responsive: true,
                destroy:true,
                columnDefs: [
                    { "orderable": false, "targets": 0 }],
                ajax: {
                    "url" : act_url,
                    "error": function(jqXHR, textStatus, errorThrown)
                        {
                            toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                        }
                    },
                columns: [
                    { data: 'cf_no', name: 'cf_no' },    
                    { data: 'full_name', name: 'full_name' },
                    { data: 'underwriter', name: 'underwriter' },
                    { data: 'no_of_insured', name: 'no_of_insured' },
                    { data: 'produk', name: 'produk' },
                    { data: 'agent', name: 'agent' },
                    { data: 'mata_uang', name: 'mata_uang' },
                    { data: 'from_date', name: 'from_date' },
                    { data: 'to_date', name: 'to_date' },
                    { data: 'total_sum_insured', name: 'total_sum_insured' },
                    // { data: 'gross_premium', name: 'gross_premium' },
                    // { data: 'nett_premium', name: 'nett_premium' },
                    // { data: 'cn_no', name: 'cn_no' },

                    { data: 'action', name: 'action' },   
                ]
            });

        $('#modal-search-policy').modal('show');
    }

    function storePolicy() {
       
        var validateDebit = $('#form-policy').data('bootstrapValidator').validate();
        if (validateDebit.isValid()) {
            loadingPage();
            let validateC = 0;
            validateC = validateAmount();
            if (validateC > 0) {
                endLoadingPage();
                return false;
            }

            console.log(validateC);
            

            $.ajax({
                type: "GET",
                url: base_url + '/cekSalesPolis?id=' + $("#no_polis").val(),
                beforeSend: function () {
                    loadingPage();
                },

                success: function (result) {
                    var data = $.parseJSON(result);
                    if (data.length != 0 && typeStore == 'new') {
                        endLoadingPage();
                        swal.fire("Info", "Polis Sudah ada", "info");
                        return false;
                    } else {
                        var _create_form = $("#form-policy");
                        var _form_data = new FormData(_create_form[0]);
                        _form_data.append('nett_amount', $('#nett_amount').val());
                        _form_data.append('no_of_insured', $('#no_of_insured').val());
                        _form_data.append('policy_cust_name', $('#policy_cust_name').val());
                        _form_data.append('id_underwriter', $('#id_underwriter').val());
                        _form_data.append('customer', $('#customer').val());
                        _form_data.append('product', $('#product').val());
                        _form_data.append('officer', $('#officer').val());
                        _form_data.append('valuta_id', $('#valuta_id').val());
                        _form_data.append('start_date_police', $('#start_date_police').val());
                        _form_data.append('end_date_police', $('#end_date_police').val());
                        _form_data.append('coc_no', $('#coc_no').val());
                        _form_data.append('insurance', $('#insurance').val());
                        _form_data.append('premi', $('#premi').val());
                        _form_data.append('disc_amount', $('#disc_amount').val());
                        _form_data.append('detail', JSON.stringify(data_detail_product));

                        $.ajax({
                            type: 'POST',
                            url: base_url + '/marketing/store/policy_installment',
                            data: _form_data,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            success: function (res) {
                                console.log(res['data']);
                            }
                        }).done(function (res) {
                            var obj = res;
                            console.log(res['data']);
                            endLoadingPage();

                            if (obj.rc == 1) {
                                znIconbox("Data Berhasil Disimpan", text, action);
                            } else {
                                swal.fire("Info", obj.rm, "info");
                            }

                        }).fail(function (res) {
                            endLoadingPage();
                            swal.fire("Error", "Terjadi Kesalahan!", "error");
                        });
                    }
                }
            }).done(function (msg) {
                //  endLoadingPage();

            }).fail(function (msg) {
                endLoadingPage();
                swal.fire("Info", "terjadi kesalahan", "info");
                return false;
            });


        }


    }

    function getMasterData(id) {
        $.ajax({
            type: 'GET',
            url: base_url + 'marketing/data/master_sales_polis_byid/' + id,
            beforeSend: function (res) {
                loadingPage();
            },
            success: function (res) {
                console.log(res);
                let data = res.rm;

                $('#get_id').val(id);
                
                $('#no_of_insured').val(data.no_of_insured);
                $('#policy_cust_name').val(data.police_name);
                $('#no_polis').val(data.polis_no);
                $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
                $('#customer').val(data.customer_id).trigger('change.select2');
                $('#product').val(data.product_id).trigger('change.select2');
                $('#officer').val(data.agent_id).trigger('change.select2');
                $('#valuta_id').val(data.valuta_id).trigger('change.select2');
                $('#start_date_police').val(znFormatDateNum(data.start_date_polis));
                $('#end_date_police').val(znFormatDateNum(data.end_date_polis));
                // SELECT 2 WITOUH TRIGER
                $('#coc_no').append(`<option value=`+data.coc_id+`>`+data.cf_no+`</option>`)
                $('#coc_no').select2('destroy');
                $('#coc_no').val(data.coc_id).select2();

                $('#insurance').val(formatMoney(data.ins_amount));
                $('#tax_amount').val(formatMoney(data.tax_amount));
                $('#fee_admin').val(formatMoney(data.admin_amount));
                $('#fee_materai').val(formatMoney(data.materai_amount));
                $('#fee_agent').val(formatMoney(data.agent_fee_amount));
                $('#asuransi').val(formatMoney(data.ins_fee));
                $('#nett_amount').val(formatMoney(data.net_amount));
                $('#fee_polis').val(formatMoney(data.polis_amount));
                $('#fee_internal').val(formatMoney(data.comp_fee_amount));
                $('#disc_amount').val(formatMoney(data.disc_amount));
                $('#premi').val(formatMoney(data.premi_amount));
                
                getDetailInsured('installment');
                // convertToRupiah1(data.total_sum_insured);
                endLoadingPage();
                $('#dataShow').fadeIn();

            }
        });
    }

    
function validateAmount() {
    let errorCount = 0;

    if ($('#no_polis').val() === '') {
        $("#no_polis").addClass("is-invalid");
        $('#sn').show();
        $('#mk').hide();
        errorCount++;
    } else {
        $("#no_polis").removeClass("is-invalid");

    }

    if ($("#premi").val() === '') {
        $("#premi").val("0,00");
    }

    if ($("#fee_internal").val() === '') {
        $("#fee_internal").val("0,00");
    }

    if ($("#fee_agent").val() === '') {
        $("#fee_agent").val("0,00");
    }

    if ($("#tax_amount").val() === '') {
        $("#tax_amount").val("0,00");
    }

    if ($('#fee_materai').val() === '') {
        $("#fee_materai").val("0,00");
    } else {
        $("#fee_materai").removeClass("is-invalid");

    }

    if ($('#fee_admin').val() === '') {
        $("#fee_admin").val("0,00");
    } else {
        $("#fee_admin").removeClass("is-invalid");

    }

    if ($('#insurance').val() === '') {
        $("#insurance").addClass("is-invalid");
        errorCount++;
    } else {
        $("#insurance").removeClass("is-invalid");

    }

    if ($('#disc_amount').val() === '') {
        $("#disc_amount").val("0,00");
    } else {
        $("#disc_amount").removeClass("is-invalid");
    }

    var a1 = $('#insurance').val();
    var a2 = a1.replace(/\./g, '');

    var b1 = $('#disc_amount').val();
    var b2 = b1.replace(/\./g, '');
    var smd = Math.floor(b2) > Math.floor(a2);

    if (smd) {
        endLoadingPage();
        $("#disc_amount").addClass("is-invalid");
        $('#sd').hide();
        $('#mi').show();
        errorCount++;
    } else {
        $("#disc_amount").removeClass("is-invalid");
    }

    return errorCount;
}


    $(document).ready(function () {

        $('#coc_no').select2('destroy');
        $('#coc_no').val(null).select2();
        
        if (typeStore == 'edit') {
            getMasterData(getId);
            $('#dataShow').show();
        }else{
            $('#dataShow').hide();
        }

        $("#form-policy").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                coc_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                no_polis: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                policy_cust_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

    //////////////////////////// OLD

    
$("#nett_amount").css({"pointer-events": "none", "cursor": "default","background":"#eeeeee"});
         
         
             var type = '{{$type}}';
             @if ($type == 'edit')
         
             if (type == 'edit') {
               console.log(type);
               console.log('{{$mi->is_tax_company}}');
         
               $('.editTittle').html('Form Edit Installment');
               $('.editBtn').html('Update');
         
               $("#dokumen_download").attr("href", "{{asset('upload_invoice')}}/{{$mi->id}}_{{$mi->url_dokumen}}");      
                 $('#no_polis').val('{{$mi->polis_no}}');
                 $('#product').val('{{$mi->product_id}}');
                 $('#officer').val('{{$mi->agent_id}}');
                 $('#customer').val('{{$mi->customer_id}}');
                 $('#quotation').val('{{$mi->qs_no}}');
                 $('#bank').val('{{$mi->afi_acc_no}}');
                 $('#insurance').val(formatMoney('{{$mi->ins_amount}}'));
                 $('#proposal').val('{{$mi->proposal_no}}');
                 if ('{{$mi->is_tax_company}}' == '1') {
                     
                     $('#is_tax_company').val('true');
                 }else {
                     $('#is_tax_company').val('false');
                 }
                 $('#id_underwriter').val('{{$mi->underwriter_id}}');
                 $('#no_of_insured').val(numFormat('{{$mi->no_of_insured}}'));
                 $('#premi').val(formatMoney('{{$mi->premi_amount}}'));
                 $('#disc_amount').val(formatMoney('{{$mi->disc_amount}}'));
                 $('#nett_amount').val(formatMoney('{{$mi->net_amount}}'));
         
                 // TAMBAHAN
                 $('#valuta_id').val('{{ $mi->valuta_id }}');
                 if ('{{ $mi->valuta_id }}' == '1') {
                     $("#today_kurs,#valas_amount").css({"pointer-events": "none", "cursor": "default","background":"#eeeeee"});
                 } else {
                     $("#insurance").css({"pointer-events": "none", "cursor": "default","background":"#eeeeee"});
                 }
           }
             @endif
         </script>
         
         
         
         <!-- end:: Content -->
         <script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
         <script type="text/javascript">
            
         
         
             $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
         
             var date = new Date();
         
             $('#tgl_mulai1').datepicker({
                 format: 'dd MM yyyy',
                 autoclose: true,
                 startDate: date
              });
         
             console.log(date);
         
       
             $('#tgl_tr').datepicker({
                 format: 'dd MM yyyy',
                 autoclose: true,
         
         
             });
             $(document).ready(function(){
                 $('#tgl_mulai1').change(function () {
                     $('#tgl_akhir1').val('');
                    $('#tgl_akhir1').datepicker('destroy');
                     console.log(this.value);
                     $('#tgl_akhir1').datepicker({
                         format: 'dd MM yyyy',
                         autoclose: true,
                         startDate: this.value
                     });
                 });
             });
         
             
         
             $('#invoice').keyup(function() {
                 var panjang = this.value.length;
         
                 if(panjang==80){
                     $('#invoice').addClass( "is-invalid" );
                     $('#sn').hide();
                     $('#mk').show();
                 }else{
                     $('#invoice').removeClass( "is-invalid" );
                     $('#sn').hide();
                     $('#mk').hide();
                 }
             });
         
             
         $("#valas_amount").keyup(function (elm) {
                 var today_kurs = $("#today_kurs").val();
         
                 if ( today_kurs != "" ) {
                     today_kurs = today_kurs.replace(/\./g,'');
                     today_kurs = today_kurs.replace(/\,/g,'.');
         
                     var valas_amount = $(this).val();
                     valas_amount = valas_amount.replace(/\./g,'');
                     valas_amount = valas_amount.replace(/\,/g,'.');
         
                     var hasil = today_kurs * valas_amount;
                     hasil = hasil.toFixed(2).replace(/\./g, ",");
         
                     console.log('today kurs : ' + today_kurs);
                     console.log('valas amount : ' + valas_amount);
                     console.log('hasil : ' + hasil);
                     
                     
                     $('#insurance').val(hasil).trigger('mask.maskMoney');
                     convertToRupiah1($("#insurance"));
         
                     $("#total_sum_insured").val(hasil);
         
                 } 
                 
             }); 
         
             $("#valuta_id").on('change', function(elm) {
                 var value = $(this).val();
         
                 if ( value == "1" ) {
                     // IDR SELECTED
                     // $("#today_kurs").prop('disabled', true);
                     // $("#valas_amount").prop('disabled', true);
                     // $("#insurance").prop('disabled', false);
         
                     $("#today_kurs,#valas_amount").css({"pointer-events": "none", "cursor": "default","background":"#eeeeee"});
                        
                     $("#insurance").removeAttr( 'style' );
               
                     $("#today_kurs").val('');
                     $("#valas_amount").val('');
                     $("#insurance").val('');
         
                     $('#premi').val('');
                     $('#disc_amount').val('');
                     $('#nett_amount').val('');
                     $('#fee_agent').val('');
                     $('#fee_internal').val('');
                     $('#asuransi').val('');
                     $('#tax_amount').val('');
                 } else {
                     $("#insurance").css({"pointer-events": "none", "cursor": "default","background":"#eeeeee"});
                        
                     $("#today_kurs,#valas_amount").removeAttr( 'style' );
         
                     // $("#today_kurs").prop('disabled', false);
                     // $("#valas_amount").prop('disabled', false);
                     // $("#insurance").prop('disabled', true);
                 }
         
             });
         
             $("#today_kurs").keyup(function (elm) {
                 var valas_amount = $("#valas_amount").val();
                 var today_kurs = $(this).val();
         
                 if ( valas_amount != "" ) {
                     today_kurs = today_kurs.replace(/\./g,'');
                     today_kurs = today_kurs.replace(/\,/g,'.');
         
                     valas_amount = valas_amount.replace(/\./g,'');
                     valas_amount = valas_amount.replace(/\,/g,'.');
         
                     var hasil = today_kurs * valas_amount;
                     hasil = hasil.toFixed(2).replace(/\./g, ",");
         
                     console.log('today kurs : ' + today_kurs);
                     console.log('valas amount : ' + valas_amount);
                     console.log('hasil : ' + hasil);
                     
                     $('#insurance').val(hasil).trigger('mask.maskMoney');
                     convertToRupiah1($("#insurance"));
         
                     $("#total_sum_insured").val(hasil);
                     
                 } 
                 
             }); 
         
             function convertToRupiah2 (objek) {
         
              var premi=$('#premi');
              var insurance=$('#insurance');
              var disc_amount=$('#disc_amount');
         
              if(premi.val()===''){
                  premi.addClass( "is-invalid" );
                  document.getElementById("disc_amount").value="";
              }else{
         
                  var aa1=insurance.val();
                  var aa2=aa1.replace(/\./g,'');
         
                  var bb1=premi.val();
                  var bb2=bb1.replace(/\./g,'');
                  var smd1=Math.floor(b2) > Math.floor(a2);
         
                  if(smd){
                     premi.addClass( "is-invalid" );
                     $('#pa').hide();
                     $('#pa1').show();
                  }else{
                      premi.removeClass( "is-invalid" )
                  }
         
         
         
                  var a1=premi.val();
                  var a2=a1.replace(/\./g,'');
                  var a3=a2.replace(/\,/g,'.');
         
                  var b1=disc_amount.val();
                  var b2=b1.replace(/\./g,'');
                  var b3=b2.replace(/\,/g,'.');
         
                  var smd=Math.floor(b3) > Math.floor(a3);
         
                  if(smd){
                     disc_amount.addClass( "is-invalid" );
                     $('#sd').hide();
                     $('#mi').show();
                    // disc_amount.val(insurance.val());
                  }else{
         
                     var c2= a3 - b3;
         
                     $nett=c2.toFixed(2).replace(/\./g, ",");
                      $('#nett_amount').val($nett).trigger('mask.maskMoney');
         
                      //$('#nett_amount').val(c2);
         
         
         
                  $.ajax({
                      type: 'GET',
                      url: base_url + '/fee1?jml='+a3+'&company='+$('#is_tax_company').val(),
                      success: function (res) {
                          var data = $.parseJSON(res);
         
                              $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                              $('#fee_agent').val($agent).trigger('mask.maskMoney');
         
         
                              $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                              $('#fee_internal').val($internal).trigger('mask.maskMoney');
         
         
         
                              var aa1=$('#nett_amount').val();
                              var aa2=aa1.replace(/\./g,'');
                              var aa3=aa2.replace(/\,/g,'.');
         
                              var asuransi=aa3 - data.fee_agent - data.komisi_perusahaan;
         
                              console.log('asuransi');
                              console.log(aa3);
                              console.log(aa3 - data.fee_agent - data.ko);
                              console.log(data.fee_agent);
                              console.log(data.komisi_perusahaan);
                              console.log(asuransi);
         
                              $asuransi=asuransi.toFixed(2).replace(/\./g, ",");
                              $('#asuransi').val($asuransi).trigger('mask.maskMoney');;
         
                              $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");
                              $('#tax_amount').val($amount).trigger('mask.maskMoney');;
         
                      }
                  });
         
                     disc_amount.removeClass( "is-invalid" );
                  }
                  premi.removeClass( "is-invalid" );
              }
         
          }
         
          function convertToRupiah1 (objek) {
         
              var insurance=$('#insurance');
           
              if(insurance.val()===''){
                  insurance.addClass( "is-invalid" );
              //    document.getElementById("disc_amount").value="";
              }else{
         
                  var a1=insurance.val();
                  var a2=a1.replace(/\./g,'');
         
                  $.ajax({
                      type: 'GET',
                      url: base_url + '/fee?jml='+a2+'&company='+$('#is_tax_company').val(),
                      success: function (res) {
                          var data = $.parseJSON(res);
                              //console.log(data.fee_agent);
         
                              //.trigger('mask.maskMoney')
                              $premi=data.premi_amount.toFixed(2).replace(/\./g, ",");
                              $('#premi').val($premi).trigger('mask.maskMoney');
                              //$('#premi').maskMoney("#,##0.00", {reverse: true});
                              //$("#premi").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
         
                              $discount=data.discount.toFixed(2).replace(/\./g, ",");
                              $('#disc_amount').val($discount).trigger('mask.maskMoney');
         
         
                              $nett=data.nett_amount.toFixed(2).replace(/\./g, ",");
                              $('#nett_amount').val($nett).trigger('mask.maskMoney');
         
         
                              $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                              $('#fee_agent').val($agent).trigger('mask.maskMoney');
         
         
         
                              $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                              $('#fee_internal').val($internal).trigger('mask.maskMoney');
         
         
                              $asuransi=data.asuransi.toFixed(2).replace(/\./g, ",");
                              $('#asuransi').val($asuransi).trigger('mask.maskMoney');
         
         
                              $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");
                              $('#tax_amount').val($amount).trigger('mask.maskMoney');
         
         
                      }
                  });
                  insurance.removeClass( "is-invalid" );
              }
         
          }
         
         var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
         var action = ` <button onclick="znClose(); " type="button"
                             class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                             Data</button>
                         <button onclick="znView()" type="button"
                             class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                             Data</button>`;

         function znClose() {
              znIconboxClose();
            $("#form-policy")[0].reset();
            document.getElementById("form-policy").reset();
            $('#form-policy').bootstrapValidator("resetForm", true);
            location.reload();
             
         
         }
         function znView() {
             znIconboxClose();
             loadNewPageSPA('{{ route('marketing.list',["policy_installment"]) }}');
         }
         
         
         function convertToRupiah_polis(objek) {
         
         if(objek.value==0){
             objek.value='';
          }else{
         
         separator = ".";
          a = objek.value;
          b = a.replace(/[^\d]/g, "");
          c = "";
          panjang = b.length;
          j = 0; for (i = panjang; i > 0; i--) {
              j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                  c = b.substr(i-1,1) + separator + c; } else {
                      c = b.substr(i-1,1) + c; } } objek.value = c;
         
         
         var insurance=$('#insurance');
         var disc_amount=$('#disc_amount');
         var polis=$('#fee_polis');
         var admin=$('#fee_admin');
         var materai=$('#fee_materai');
         
         if(insurance.val()===''){
             insurance.addClass( "is-invalid" );
         }else{
             insurance.removeClass( "is-invalid" );
             var a1=insurance.val();
             var a2=a1.replace(/\./g,'');
         
             var b1=disc_amount.val();
             var b2=b1.replace(/\./g,'');
         
             var c1=polis.val();
             var c2=c1.replace(/\./g,'');
         
             var d1=admin.val();
             var d2=d1.replace(/\./g,'');
         
             var e1=materai.val();
             var e2=e1.replace(/\./g,'');
         
             var netamount= a2 - b2 - c2 - d2 - e2;
         
                 $('#nett_amount').val(netamount);
         
                 separator = ".";
                 a = $('#nett_amount').val();
                 b = a.replace(/[^\d]/g,"");
                 c = "";
                 panjang = b.length;
                 j = 0; for (i = panjang; i > 0; i--) {
                    j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + separator + c; } else {
                            c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);
         
                 var g1=Math.floor(0.02 * netamount);
                 $('#tax_amount').val(g1);
         
         
                 $.ajax({
                             type: 'GET',
                             url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                             success: function (res) {
                                 var data = $.parseJSON(res);
                                 //console.log(data.fee_agent);
                                 $('#fee_agent').val(Math.floor(data.fee_agent));
                                 separator = ".";
                                  a = $('#fee_agent').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
         
         
                                 $('#fee_internal').val(Math.floor(data.fee_internal));
                                 separator = ".";
                                  a = $('#fee_internal').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
         
         
                                 $('#premi').val(Math.floor(data.premi));
                                 separator = ".";
                                  a = $('#premi').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                                 //data[fee_agent];
                             }
                         });
         
         
             }
         
         
          }
         
         }
         
         function convertToRupiah_admin(objek) {
         
         if(objek.value==0){
             objek.value='';
          }else{
         
         separator = ".";
          a = objek.value;
          b = a.replace(/[^\d]/g, "");
          c = "";
          panjang = b.length;
          j = 0; for (i = panjang; i > 0; i--) {
              j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                  c = b.substr(i-1,1) + separator + c; } else {
                      c = b.substr(i-1,1) + c; } } objek.value = c;
         
         
         var insurance=$('#insurance');
         var disc_amount=$('#disc_amount');
         var polis=$('#fee_polis');
         var admin=$('#fee_admin');
         var materai=$('#fee_materai');
         
         if(insurance.val()===''){
             insurance.addClass( "is-invalid" );
         }else{
             insurance.removeClass( "is-invalid" );
             var a1=insurance.val();
             var a2=a1.replace(/\./g,'');
         
             var b1=disc_amount.val();
             var b2=b1.replace(/\./g,'');
         
             var c1=polis.val();
             var c2=c1.replace(/\./g,'');
         
             var d1=admin.val();
             var d2=d1.replace(/\./g,'');
         
             var e1=materai.val();
             var e2=e1.replace(/\./g,'');
         
             var netamount= a2 - b2 - c2 - d2 - e2;
         
                 $('#nett_amount').val(netamount);
         
                 separator = ".";
                 a = $('#nett_amount').val();
                 b = a.replace(/[^\d]/g,"");
                 c = "";
                 panjang = b.length;
                 j = 0; for (i = panjang; i > 0; i--) {
                    j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + separator + c; } else {
                            c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);
         
                 var g1=Math.floor(0.02 * netamount);
                 $('#tax_amount').val(g1);
         
                 $.ajax({
                             type: 'GET',
                             url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                             success: function (res) {
                                 var data = $.parseJSON(res);
                                 //console.log(data.fee_agent);
                                 $('#fee_agent').val(Math.floor(data.fee_agent));
                                 separator = ".";
                                  a = $('#fee_agent').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
         
         
                                 $('#fee_internal').val(Math.floor(data.fee_internal));
                                 separator = ".";
                                  a = $('#fee_internal').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
         
         
                                 $('#premi').val(Math.floor(data.premi));
                                 separator = ".";
                                  a = $('#premi').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                                 //data[fee_agent];
                             }
                         });
         
         
             }
         
         
          }
         
         }
         
         
         function convertToRupiah_materai(objek) {
         
         if(objek.value==0){
             objek.value='';
          }else{
         
         separator = ".";
          a = objek.value;
          b = a.replace(/[^\d]/g, "");
          c = "";
          panjang = b.length;
          j = 0; for (i = panjang; i > 0; i--) {
              j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                  c = b.substr(i-1,1) + separator + c; } else {
                      c = b.substr(i-1,1) + c; } } objek.value = c;
         
         
         var insurance=$('#insurance');
         var disc_amount=$('#disc_amount');
         var polis=$('#fee_polis');
         var admin=$('#fee_admin');
         var materai=$('#fee_materai');
         
         if(insurance.val()===''){
             insurance.addClass( "is-invalid" );
         }else{
             insurance.removeClass( "is-invalid" );
             var a1=insurance.val();
             var a2=a1.replace(/\./g,'');
         
             var b1=disc_amount.val();
             var b2=b1.replace(/\./g,'');
         
             var c1=polis.val();
             var c2=c1.replace(/\./g,'');
         
             var d1=admin.val();
             var d2=d1.replace(/\./g,'');
         
             var e1=materai.val();
             var e2=e1.replace(/\./g,'');
         
             var netamount= a2 - b2 - c2 - d2 - e2;
         
                 $('#nett_amount').val(netamount);
         
                 separator = ".";
                 a = $('#nett_amount').val();
                 b = a.replace(/[^\d]/g,"");
                 c = "";
                 panjang = b.length;
                 j = 0; for (i = panjang; i > 0; i--) {
                    j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + separator + c; } else {
                            c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);
         
                 var g1=Math.floor(0.02 * netamount);
                 $('#tax_amount').val(g1);
         
                 $.ajax({
                             type: 'GET',
                             url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                             success: function (res) {
                                 var data = $.parseJSON(res);
                                 //console.log(data.fee_agent);
                                 $('#fee_agent').val(Math.floor(data.fee_agent));
                                 separator = ".";
                                  a = $('#fee_agent').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
         
         
                                 $('#fee_internal').val(Math.floor(data.fee_internal));
                                 separator = ".";
                                  a = $('#fee_internal').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
         
         
                                 $('#premi').val(Math.floor(data.premi));
                                 separator = ".";
                                  a = $('#premi').val();
                                  b = a.replace(/[^\d]/g,"");
                                  c = "";
                                  panjang = b.length;
                                  j = 0; for (i = panjang; i > 0; i--) {
                                      j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                          c = b.substr(i-1,1) + separator + c; } else {
                                              c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                                 //data[fee_agent];
                             }
                         });
         
         
             }
         
         
          }
         
         }
         
         
         

</script>
