<script>
    var set_type = '{{$typeStore}}';

    function setEF(val) {
        let premi = zclearNumformat($('#premi').val()); //Gross Premium Amount
        let ef_pct = zclearNumformat($('#ef_pct').val()); 

        console.log(ef_pct);

        let ef = (ef_pct/100)*premi;
        $('#ef').val(formatMoney(ef));
        $('#form-policy').bootstrapValidator('revalidateField', 'ef_pct');

        setSumInsured();
    }

function validateAmount() {
    let errorCount = 0;

    if ($('#no_polis').val() === '') {
        $("#no_polis").addClass("is-invalid");
        $('#sn').show();
        $('#mk').hide();
        errorCount++;
    } else {
        $("#no_polis").removeClass("is-invalid");

    }

    if ($("#premi").val() === '') {
        $("#premi").val("0,00");
    }

    if ($("#fee_internal").val() === '') {
        $("#fee_internal").val("0,00");
    }

    if ($("#fee_agent").val() === '') {
        $("#fee_agent").val("0,00");
    }

    if ($("#tax_amount").val() === '') {
        $("#tax_amount").val("0,00");
    }

    if ($('#fee_materai').val() === '') {
        $("#fee_materai").val("0,00");
    } else {
        $("#fee_materai").removeClass("is-invalid");

    }

    if ($('#fee_admin').val() === '') {
        $("#fee_admin").val("0,00");
    } else {
        $("#fee_admin").removeClass("is-invalid");

    }

    if ($('#insurance').val() === '') {
        $("#insurance").addClass("is-invalid");
        errorCount++;
    } else {
        $("#insurance").removeClass("is-invalid");

    }

    if ($('#disc_amount').val() === '') {
        $("#disc_amount").val("0,00");
    } else {
        $("#disc_amount").removeClass("is-invalid");
    }

    var a1 = $('#insurance').val();
    var a2 = a1.replace(/\./g, '');

    var b1 = $('#disc_amount').val();
    var b2 = b1.replace(/\./g, '');
    var smd = Math.floor(b2) > Math.floor(a2);

    if (smd) {
        endLoadingPage();
        $("#disc_amount").addClass("is-invalid");
        $('#sd').hide();
        $('#mi').show();
        errorCount++;
    } else {
        $("#disc_amount").removeClass("is-invalid");
    }

    return errorCount;
}


$("#valas_amount").keyup(function (elm) {
    var today_kurs = $("#today_kurs").val();

    if (today_kurs != "") {
        today_kurs = today_kurs.replace(/\./g, '');
        today_kurs = today_kurs.replace(/\,/g, '.');

        var valas_amount = $(this).val();
        valas_amount = valas_amount.replace(/\./g, '');
        valas_amount = valas_amount.replace(/\,/g, '.');

        var hasil = today_kurs * valas_amount;
        hasil = hasil.toFixed(2).replace(/\./g, ",");



        $('#insurance').val(valas_amount).trigger('mask.maskMoney');
        convertToRupiah1($("#insurance"));

        $("#total_sum_insured").val(hasil);

    }

});

$("#today_kurs").keyup(function (elm) {
    var valas_amount = $("#valas_amount").val();
    var today_kurs = $(this).val();

    if (valas_amount != "") {
        today_kurs = today_kurs.replace(/\./g, '');
        today_kurs = today_kurs.replace(/\,/g, '.');

        valas_amount = valas_amount.replace(/\./g, '');
        valas_amount = valas_amount.replace(/\,/g, '.');

        var hasil = today_kurs * valas_amount;
        hasil = hasil.toFixed(2).replace(/\./g, ",");

      

        $('#insurance').val(hasil).trigger('mask.maskMoney');
        convertToRupiah1($("#insurance"));

        $("#total_sum_insured").val(hasil);

    }

});

$("#valuta_id").on('change', function (elm) {
    var value = $(this).val();
    var text = $("#valuta_id option:selected").text();



    $('.zn-rp-group').html(text);

    if (value == "1") {
        // IDR SELECTED
        $("#today_kurs").prop('disabled', true);
        $("#valas_amount").prop('disabled', true);
        // $("#insurance").prop('disabled', false);

        $("#today_kurs").val('');
        $("#valas_amount").val('');
        $("#insurance").val('');

        $('#premi').val('');
        $('#disc_amount').val('');
        $('#nett_amount').val('');
        $('#fee_agent').val('');
        $('#fee_internal').val('');
        $('#asuransi').val('');
        $('#tax_amount').val('');
    } else {
        $("#today_kurs").prop('disabled', false);
        $("#valas_amount").prop('disabled', false);
        // $("#insurance").prop('disabled', true);
    }

});

$(".money").maskMoney({
    prefix: '',
    allowNegative: true,
    thousands: '.',
    decimal: ',',
    affixesStay: false
});

var date = new Date();

$('#tgl_mulai1').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    startDate: date
});

// Tambahan
$('#start_date_police').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    // startDate: date
});

$(document).ready(function () {
    $('#start_date_police').change(function () {
        $('#end_date_police').val('');
        $('#end_date_police').datepicker('destroy');
        
        $('#end_date_police').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            startDate: this.value
        });
        $('#form-policy').bootstrapValidator('revalidateField', 'start_date_police');
    });

    $('#end_date_police').change(function () {

        $('#form-policy').bootstrapValidator('revalidateField', 'end_date_police');

    });

});


/*
$('#end_date_police').datepicker({
   format: 'dd MM yyyy',
   autoclose: true,
   // startDate: date
});
*/


/*
    startDate: lastDay,
        endDate: lastDay,
        beforeShowDay: function(date){
            if(date==lastDay){
                return true;
            }else{
                return false;
            }

        }
        */
$('#tgl_tr').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,


});
$(document).ready(function () {
    $('#tgl_mulai1').change(function () {
        $('#tgl_akhir1').val('');
        $('#tgl_akhir1').datepicker('destroy');
       
        $('#tgl_akhir1').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            startDate: this.value
        });

        $('#form-policy').bootstrapValidator('revalidateField', 'tgl_mulai');

    });
});

/*
$('#tgl_mulai').datepicker({
            format: 'dd/mm/yyyy',
            onSelect: function(dateText) {
                console.log("Selected date: " + dateText + "; input's current value: " + this.value);
            }
        });
*/

$('#invoice').keyup(function () {
    var panjang = this.value.length;

    if (panjang == 80) {
        $('#invoice').addClass("is-invalid");
        $('#sn').hide();
        $('#mk').show();
    } else {
        $('#invoice').removeClass("is-invalid");
        $('#sn').hide();
        $('#mk').hide();
    }
});




function convertToRupiah2(objek) {

    var premi = $('#premi');
    var insurance = $('#insurance');
    var disc_amount = $('#disc_amount');

    if (premi.val() === '') {
        premi.addClass("is-invalid");
        document.getElementById("disc_amount").value = "";
    } else {

        var aa1 = insurance.val();
        var aa2 = aa1.replace(/\./g, '');

        var bb1 = premi.val();
        var bb2 = bb1.replace(/\./g, '');
        var smd1 = Math.floor(b2) > Math.floor(a2);

        if (smd) {
            premi.addClass("is-invalid");
            $('#pa').hide();
            $('#pa1').show();
        } else {
            premi.removeClass("is-invalid")
        }



        var a1 = premi.val();
        var a2 = a1.replace(/\./g, '');
        var a3 = a2.replace(/\,/g, '.');

        var b1 = disc_amount.val();
        var b2 = b1.replace(/\./g, '');
        var b3 = b2.replace(/\,/g, '.');

        var smd = Math.floor(b3) > Math.floor(a3);

        if (smd) {
            disc_amount.addClass("is-invalid");
            $('#sd').hide();
            $('#mi').show();
            // disc_amount.val(insurance.val());
        } else {

            var c2 = a3 - b3;

            $nett = c2.toFixed(2).replace(/\./g, ",");
            $('#nett_amount').val($nett).trigger('mask.maskMoney');

            //$('#nett_amount').val(c2);



            $.ajax({
                type: 'GET',
                url: base_url + '/fee1?jml=' + a3 + '&company=' + $('#is_tax_company').val(),
                success: function (res) {
                    var data = $.parseJSON(res);
                  
                   

                    $agent = data.fee_agent.toFixed(2).replace(/\./g, ",");
                    $('#fee_agent').val($agent).trigger('mask.maskMoney');


                    $internal = data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                    //$('#fee_internal').val($internal).trigger('mask.maskMoney');

                    // if($('#fee_internal').val()===''){
                    $('#fee_internal').val($internal).trigger('mask.maskMoney');
                    // }


                    var aa1 = $('#nett_amount').val();
                    var aa2 = aa1.replace(/\./g, '');
                    var aa3 = aa2.replace(/\,/g, '.');

                    var aa11 = $('#fee_internal').val();
                    var aa21 = aa11.replace(/\./g, '');
                    var aa31 = aa21.replace(/\,/g, '.');

             

                    var asuransi = aa3 - data.fee_agent - aa31;

             

                    $asuransi = asuransi.toFixed(2).replace(/\./g, ",");
                    $('#asuransi').val($asuransi).trigger('mask.maskMoney');

                    $amount = data.tax_amount.toFixed(2).replace(/\./g, ",");
                    $('#tax_amount').val($amount).trigger('mask.maskMoney');

                }
            });

            disc_amount.removeClass("is-invalid");
        }
        premi.removeClass("is-invalid");
    }

}

function zclearNumformat(num) {
    var clearNum = num;
    clearNum = clearNum.replace(/\./g, '');
    clearNum = clearNum.replace(/\,/g, '.');
    return parseFloat(clearNum);
}

function feeAgentChange() {

    var fee_agent = zclearNumformat($('#fee_agent').val()); //Commission Due to Agent
        var fee_internal = zclearNumformat($('#fee_internal').val()); //Commission Due to HD Soeryo
        var fee_polis = zclearNumformat($('#fee_polis').val()); //Policy Duty
        var fee_materai = zclearNumformat($('#fee_materai').val()); //stamp duty
        var fee_admin = zclearNumformat($('#fee_admin').val());

        var premi = zclearNumformat($('#premi').val()); //Gross Premium Amount
        var disc_amount = zclearNumformat($('#disc_amount').val()); //Discount Amount
        var ef = zclearNumformat($('#ef').val()); //EF Amount

        if ($('#fee_agent').val() == '') { fee_agent = 0;}
        if ($('#fee_internal').val() == '') {fee_internal = 0;}
        if ($('#fee_polis').val() == '') {fee_polis = 0;}
        if ($('#fee_materai').val() == '') {fee_materai = 0;}
        if ($('#fee_admin').val() == '') {fee_admin = 0;}
        if ($('#premi').val() == '') {premi = 0;}
        if ($('#disc_amount').val() == '') {disc_amount = 0;}
        if ($('#ef').val() == '') {ef = 0;}
     
        var set_nett_premium= premi - disc_amount - ef + (fee_polis+fee_materai+fee_admin);
        $set_nett_premium=set_nett_premium.toFixed(2).replace(/\./g, ",");
        $('#nett_amount').val($set_nett_premium).trigger('mask.maskMoney');

        var nett_amount = zclearNumformat($('#nett_amount').val()); //net premi
        if ($('#nett_amount').val() == '') { nett_amount = 0;}

        var asuransi=nett_amount +(fee_polis+fee_materai);
        // var asuransi=nett_amount - fee_agent - fee_internal +fee_polis +fee_materai+fee_admin; 
        $asuransi=asuransi.toFixed(2).replace(/\./g, ",");
      
        console.log(set_nett_premium);
        console.log($set_nett_premium);

        $('#asuransi').val($asuransi).trigger('mask.maskMoney');


    // var nett_amount = zclearNumformat($('#nett_amount').val()); //net premi
    // var fee_agent = zclearNumformat($('#fee_agent').val()); //Commission Due to Agent
    // var fee_internal = zclearNumformat($('#fee_internal').val()); //Commission Due to HD Soeryo
    // var fee_polis = zclearNumformat($('#fee_polis').val()); //Policy Duty
    // var fee_materai = zclearNumformat($('#fee_materai').val()); //stamp duty
    // var fee_admin = zclearNumformat($('#fee_admin').val());

    // if ($('#nett_amount').val() == '') {
    //     nett_amount = 0;
    // }
    // if ($('#fee_agent').val() == '') {
    //     fee_agent = 0;
    // }
    // if ($('#fee_internal').val() == '') {
    //     fee_internal = 0;
    // }
    // if ($('#fee_polis').val() == '') {
    //     fee_polis = 0;
    // }
    // if ($('#fee_materai').val() == '') {
    //     fee_materai = 0;
    // }


    // var asuransi = nett_amount - fee_agent - fee_internal + (fee_polis + fee_materai);
    // // var asuransi=nett_amount - fee_agent - fee_internal +fee_polis +fee_materai+fee_admin;

    // // $asuransi = asuransi.toFixed(2).replace(/\./g, ",");

    // console.log(asuransi);


    $('#asuransi').val(formatMoney(asuransi));
}

function convertToRupiah3(objek) {

    var premi = $('#premi');
    var insurance = $('#insurance');
    var disc_amount = $('#disc_amount');

    if (premi.val() === '') {
        premi.addClass("is-invalid");
        document.getElementById("disc_amount").value = "";
    } else {

        var aa1 = insurance.val();
        var aa2 = aa1.replace(/\./g, '');

        var bb1 = premi.val();
        var bb2 = bb1.replace(/\./g, '');
        var smd1 = Math.floor(b2) > Math.floor(a2);

        if (smd) {
            premi.addClass("is-invalid");
            $('#pa').hide();
            $('#pa1').show();
        } else {
            premi.removeClass("is-invalid")
        }



        var a1 = premi.val();
        var a2 = a1.replace(/\./g, '');
        var a3 = a2.replace(/\,/g, '.');

        var b1 = disc_amount.val();
        var b2 = b1.replace(/\./g, '');
        var b3 = b2.replace(/\,/g, '.');

        var smd = Math.floor(b3) > Math.floor(a3);

        if (smd) {
            disc_amount.addClass("is-invalid");
            $('#sd').hide();
            $('#mi').show();
            // disc_amount.val(insurance.val());
        } else {

            var c2 = a3 - b3;

            $nett = c2.toFixed(2).replace(/\./g, ",");
            $('#nett_amount').val($nett).trigger('mask.maskMoney');

            //$('#nett_amount').val(c2);



            $.ajax({
                type: 'GET',
                url: base_url + '/fee1?jml=' + a3 + '&company=' + $('#is_tax_company').val(),
                success: function (res) {
                    var data = $.parseJSON(res);
                   

                    $agent = data.fee_agent.toFixed(2).replace(/\./g, ",");
                    $('#fee_agent').val($agent).trigger('mask.maskMoney');

                    $internal = data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                    //$('#fee_internal').val($internal).trigger('mask.maskMoney');

                    // if($('#fee_internal').val()===''){
                    $('#fee_internal').val($internal).trigger('mask.maskMoney');
                    // }

                    //$internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                    //$('#fee_internal').val($internal).trigger('mask.maskMoney');



                    var aa1 = $('#nett_amount').val();
                    var aa2 = aa1.replace(/\./g, '');
                    var aa3 = aa2.replace(/\,/g, '.');

                    var aa11 = $('#fee_internal').val();
                    var aa21 = aa11.replace(/\./g, '');
                    var aa31 = aa21.replace(/\,/g, '.');



                    var asuransi = aa3 - data.fee_agent - aa31;

                    $asuransi = asuransi.toFixed(2).replace(/\./g, ",");
                    $('#asuransi').val($asuransi).trigger('mask.maskMoney');

                    $amount = data.tax_amount.toFixed(2).replace(/\./g, ",");
                    $('#tax_amount').val($amount).trigger('mask.maskMoney');

                }
            });

            disc_amount.removeClass("is-invalid");
        }
        premi.removeClass("is-invalid");
    }

}

function convertToRupiah1(objek) {

    var insurance = $('#insurance');
    /*
        var disc_amount=$('#disc_amount');

        var polis=$('#fee_polis');
        var admin=$('#fee_admin');
        var materai=$('#fee_materai');


    */

    var police_duty = parseFloat($("#fee_polis").val().replace('.', ''));
    var stamp_duty = parseFloat($("#fee_materai").val().replace('.', ''));

    if (insurance.val() === '') {
        insurance.addClass("is-invalid");
        //    document.getElementById("disc_amount").value="";
    } else {

        var a1 = insurance.val();
        var a2 = a1.replace(/\./g, '');


        $.ajax({
            type: 'GET',
            url: base_url + '/fee?jml=' + a2 + '&company=' + $('#is_tax_company').val(),
            success: function (res) {
                var data = $.parseJSON(res);
               
                //console.log(data.fee_agent);

                //.trigger('mask.maskMoney')
                $premi = data.premi_amount.toFixed(2).replace(/\./g, ",");
                $('#premi').val($premi).trigger('mask.maskMoney');
                //$('#premi').maskMoney("#,##0.00", {reverse: true});
                //$("#premi").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

                $discount = data.discount.toFixed(2).replace(/\./g, ",");
                $('#disc_amount').val($discount).trigger('mask.maskMoney');


                $nett = data.nett_amount.toFixed(2).replace(/\./g, ",");
                $('#nett_amount').val($nett).trigger('mask.maskMoney');


                $agent = data.fee_agent.toFixed(2).replace(/\./g, ",");
                $('#fee_agent').val($agent).trigger('mask.maskMoney');



                $internal = data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                //$('#fee_internal').val($internal).trigger('mask.maskMoney');
                // if($('#fee_internal').val()===''){

                $('#fee_internal').val($internal).trigger('mask.maskMoney');
                // }


                // nilaiAsuransi = data.asuransi;
                // $asuransi = data.asuransi + police_duty + stamp_duty;
                // $asuransi = $asuransi.toFixed(2).replace(/\./g, ",");

                // $asuransi=data.asuransi.toFixed(2).replace(/\./g, ",");
                // $('#asuransi').val($asuransi).trigger('mask.maskMoney');

                feeAgentChange();

                $amount = data.tax_amount.toFixed(2).replace(/\./g, ",");
                $('#tax_amount').val($amount).trigger('mask.maskMoney');


            }
        });
        insurance.removeClass("is-invalid");
    }

}

var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  <button onclick="znClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                    Data</button>
                <button onclick="znView('policy')" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;

function znClose() {
    znIconboxClose();
    $("#form-policy")[0].reset();
    document.getElementById("form-policy").reset();
    $('#form-policy').bootstrapValidator("resetForm", true);
    location.reload();
    // $('#coc_no').val(null).trigger('change.select2');

}

function znView(type) {
    znIconboxClose();
    switch (type) {
        case 'policy':
            loadNewPageSPA('{{ route('marketing.list',["policy"]) }}');
        break;

        default:
            break;
    }

}


var _create_form = $("#form-policy");

function convertToRupiah_polis(objek) {

    if (objek.value == 0) {
        objek.value = '';
    } else {

        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;


        var insurance = $('#insurance');
        var disc_amount = $('#disc_amount');
        var polis = $('#fee_polis');
        var admin = $('#fee_admin');
        var materai = $('#fee_materai');

        if (insurance.val() === '') {
            insurance.addClass("is-invalid");
        } else {
            insurance.removeClass("is-invalid");
            var a1 = insurance.val();
            var a2 = a1.replace(/\./g, '');

            var b1 = disc_amount.val();
            var b2 = b1.replace(/\./g, '');

            var c1 = polis.val();
            var c2 = c1.replace(/\./g, '');

            var d1 = admin.val();
            var d2 = d1.replace(/\./g, '');

            var e1 = materai.val();
            var e2 = e1.replace(/\./g, '');

            var netamount = a2 - b2 - c2 - d2 - e2;

            $('#nett_amount').val(netamount);

            separator = ".";
            a = $('#nett_amount').val();
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + separator + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            $('#nett_amount').val(c);

            var g1 = Math.floor(0.02 * netamount);
            $('#tax_amount').val(g1);


            $.ajax({
                type: 'GET',
                url: base_url + '/fee?jml=' + netamount + '&company=' + $('#is_tax_company').val() + '&jmltax=' + g1,
                success: function (res) {
                    var data = $.parseJSON(res);
                 
                    $('#fee_agent').val(Math.floor(data.fee_agent));
                    separator = ".";
                    a = $('#fee_agent').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#fee_agent').val(c);


                    $('#fee_internal').val(Math.floor(data.fee_internal));
                    separator = ".";
                    a = $('#fee_internal').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#fee_internal').val(c);


                    $('#premi').val(Math.floor(data.premi));
                    separator = ".";
                    a = $('#premi').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#premi').val(c);
                    //data[fee_agent];
                }
            });


        }


    }

}

function convertToRupiah_admin(objek) {

    if (objek.value == 0) {
        objek.value = '';
    } else {

        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;


        var insurance = $('#insurance');
        var disc_amount = $('#disc_amount');
        var polis = $('#fee_polis');
        var admin = $('#fee_admin');
        var materai = $('#fee_materai');

        if (insurance.val() === '') {
            insurance.addClass("is-invalid");
        } else {
            insurance.removeClass("is-invalid");
            var a1 = insurance.val();
            var a2 = a1.replace(/\./g, '');

            var b1 = disc_amount.val();
            var b2 = b1.replace(/\./g, '');

            var c1 = polis.val();
            var c2 = c1.replace(/\./g, '');

            var d1 = admin.val();
            var d2 = d1.replace(/\./g, '');

            var e1 = materai.val();
            var e2 = e1.replace(/\./g, '');

            var netamount = a2 - b2 - c2 - d2 - e2;

            $('#nett_amount').val(netamount);

            separator = ".";
            a = $('#nett_amount').val();
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + separator + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            $('#nett_amount').val(c);

            var g1 = Math.floor(0.02 * netamount);
            $('#tax_amount').val(g1);

            $.ajax({
                type: 'GET',
                url: base_url + '/fee?jml=' + netamount + '&company=' + $('#is_tax_company').val() + '&jmltax=' + g1,
                success: function (res) {
                    var data = $.parseJSON(res);
                   
                    $('#fee_agent').val(Math.floor(data.fee_agent));
                    separator = ".";
                    a = $('#fee_agent').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#fee_agent').val(c);


                    $('#fee_internal').val(Math.floor(data.fee_internal));
                    separator = ".";
                    a = $('#fee_internal').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#fee_internal').val(c);


                    $('#premi').val(Math.floor(data.premi));
                    separator = ".";
                    a = $('#premi').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#premi').val(c);
                    //data[fee_agent];
                }
            });


        }


    }

}

function convertToRupiah_materai(objek) {

    if (objek.value == 0) {
        objek.value = '';
    } else {

        separator = ".";
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
            }
        }
        objek.value = c;


        var insurance = $('#insurance');
        var disc_amount = $('#disc_amount');
        var polis = $('#fee_polis');
        var admin = $('#fee_admin');
        var materai = $('#fee_materai');

        if (insurance.val() === '') {
            insurance.addClass("is-invalid");
        } else {
            insurance.removeClass("is-invalid");
            var a1 = insurance.val();
            var a2 = a1.replace(/\./g, '');

            var b1 = disc_amount.val();
            var b2 = b1.replace(/\./g, '');

            var c1 = polis.val();
            var c2 = c1.replace(/\./g, '');

            var d1 = admin.val();
            var d2 = d1.replace(/\./g, '');

            var e1 = materai.val();
            var e2 = e1.replace(/\./g, '');

            var netamount = a2 - b2 - c2 - d2 - e2;

            $('#nett_amount').val(netamount);

            separator = ".";
            a = $('#nett_amount').val();
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + separator + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            $('#nett_amount').val(c);

            var g1 = Math.floor(0.02 * netamount);
            $('#tax_amount').val(g1);

            $.ajax({
                type: 'GET',
                url: base_url + '/fee?jml=' + netamount + '&company=' + $('#is_tax_company').val() + '&jmltax=' + g1,
                success: function (res) {
                    var data = $.parseJSON(res);
                   
                    $('#fee_agent').val(Math.floor(data.fee_agent));
                    separator = ".";
                    a = $('#fee_agent').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#fee_agent').val(c);


                    $('#fee_internal').val(Math.floor(data.fee_internal));
                    separator = ".";
                    a = $('#fee_internal').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#fee_internal').val(c);


                    $('#premi').val(Math.floor(data.premi));
                    separator = ".";
                    a = $('#premi').val();
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                        }
                    }
                    $('#premi').val(c);
                    //data[fee_agent];
                }
            });


        }


    }

}


</script>
