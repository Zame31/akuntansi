@section('content')
<div class="app-content">
<div class="section">


    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Marketing </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                            {{$title}} </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            {{$title}}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            @if ($typeList == 'new')
                                <a style="cursor: pointer;color:white;" onclick="gActAll('sendApproval','/marketing/approval/send?type=semua','{{ route('marketing.data',['list_policy','new']) }}');" class="btn btn-pill btn-primary">Send Approval All</a>
                                <a style="cursor: pointer;color:white;" onclick="gActSelected('sendApproval','/marketing/approval/send?type=satu','{{ route('marketing.data',['list_policy','new']) }}');" class="btn btn-pill btn-primary">Send Approval Selected</a>
                                <a style="cursor: pointer;color:white;" onclick="gActAll('delete','/marketing/approval/delete?type=semua','{{ route('marketing.data',['list_policy','new']) }}');" class="btn btn-pill btn-danger">Delete All</a>
                                <a style="cursor: pointer;color:white;" onclick="gActSelected('delete','/marketing/approval/delete?type=satu','{{ route('marketing.data',['list_policy','new']) }}');" class="btn btn-pill btn-danger">Delete Selected</a>
                                <button onclick="loadNewPageSPA('{{ route('marketing.create','policy') }}')" class="btn btn-pill btn-success">Tambah Data</button>
                            @elseif($typeList == 'approve')
                                <a style="cursor: pointer;color:white;" onclick="gActAll('giveApproval','/marketing/approval/give?type=semua','{{ route('marketing.data',['list_policy','approve']) }}');" class="btn btn-pill btn-primary">Approval All</a>
                                <a style="cursor: pointer;color:white;" onclick="gActSelected('giveApproval','/marketing/approval/give?type=satu','{{ route('marketing.data',['list_policy','approve']) }}');" class="btn btn-pill btn-primary">Approval Selected</a>
                                <a style="cursor: pointer;color:white;" onclick="gActAll('reject','/marketing/approval/reject?type=semua','{{ route('marketing.data',['list_policy','approve']) }}');" class="btn btn-pill btn-danger">Reject All</a>
                                <a style="cursor: pointer;color:white;" onclick="gActSelected('reject','/marketing/approval/reject?type=satu','{{ route('marketing.data',['list_policy','approve']) }}');" class="btn btn-pill btn-danger">Reject Selected</a>
                            {{-- @elseif($typeList == 'success')
                                @if (Auth::user()->user_role_id == 1)
                                    <a href="#" onclick="gActAll('delete','/hapusAprovalInstallmentRev?type=semua','{{ route('data_success.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete All</a>
                                    <a href="#" onclick="gActSelected('delete','/hapusAprovalInstallmentRev?type=satu','{{ route('data_success.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete Selected</a>
                                @endif
                            @elseif($typeList == 'approve_reversal')
                                <a href="#" onclick="gActAll('deleteRev','/giveHapusAprovalInstallmentRev?type=semua','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete All</a>
                                <a href="#" onclick="gActSelected('deleteRev','/giveHapusAprovalInstallmentRev?type=satu','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete Selected</a>
                                <a href="#" onclick="gActAll('reject','/rejectHapusAprovalInstallmentRev?type=semua','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete All</a>
                                <a href="#" onclick="gActSelected('reject','/rejectHapusAprovalInstallmentRev?type=satu','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete Selected</a> --}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Customer Name</th>
                                    <th>Branch Code</th>
                                    <th>Product Name</th>
                                    <th>Valuta</th>
                                    <th>Premi Amount</th>
                                    
                                    <th>Police Number</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>

@include('marketing.action.act_global')
@include('marketing.action.act_list_policy')
@include('marketing.modal.modal_detail')
@include('marketing.modal.modal_notes')

@stop
