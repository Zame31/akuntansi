@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Sales </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                @if ($data->invoice_type_id == 1)
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Edit Endorsment </a>
                @else
                <a href="" class="kt-subheader__breadcrumbs-link">
                        Edit Debit Note </a>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    @if ($data->invoice_type_id == 1)
                        Form Endorsment
                    @else
                        Form Debit Note
                    @endif
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="return simpan_new_invoice();" class="btn btn-success">Update</button>
                        <button onclick="loadNewPage('{{ route('invoice_list') }}')" class="btn btn-danger">Back</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body" style="
        background: #fbfbfb;
    ">
        <form id="form-new-invoice" enctype="multipart/form-data">
            <input type="hidden" name="invoice_type_id" id="invoice_type_id" value="0">
            <div class="row zn-border-bottom mb-5">
               
                <input type="hidden" name="id" value="{{$data->id}}">
                <div class="col-md-4">
                    
                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Underwriter</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_underwriter as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->underwriter_id) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                        <input type="hidden" name="id_underwriter" id="id_underwriter" value="{{ $data->underwriter_id }}">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @else
                            <label>Underwriter</label>
                            <select class="form-control kt-select2 init-select2" name="id_underwriter" id="id_underwriter">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_underwriter as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->underwriter_id) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Policy Number</label>
                            <input type="text" class="form-control" value="{{$data->polis_no}}" readonly name="no_polis" id="no_polis" maxlength="80">
                            <div class="invalid-feedback" id="sn">Silahkan Isi No Polis</div>
                            <div class="invalid-feedback" id="mk">Maximal 80 karakter</div>
                        @else
                            <label>Policy Number</label>
                            <input type="text" class="form-control" value="{{$data->polis_no}}" name="no_polis" id="no_polis" maxlength="80">
                            <div class="invalid-feedback" id="sn">Silahkan Isi No Polis</div>
                            <div class="invalid-feedback" id="mk">Maximal 80 karakter</div>
                        @endif
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Customer Name</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_customer as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->customer_id) selected @endif>{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="customer" id="customer" value="{{ $data->customer_id }}">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @else
                            <label>Customer Name</label>
                            <select class="form-control kt-select2 init-select2" name="customer" id="customer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_customer as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->customer_id) selected @endif>{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>
                </div>
               
                
            </div>

            <div class="row zn-border-bottom mb-5">

                <div class="col-md-4">
                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Product Name</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_product as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->product_id) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="product" id="product" value="{{ $data->product_id }}"> 
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @else
                            <label>Product Name</label>
                            <select class="form-control kt-select2 init-select2" name="product" id="product" >
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_product as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->product_id) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>
                    

                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Quotation Number</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_quotation as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->qs_no) selected @endif>{{$item->qs_no}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="quotation" id="quotation" value="{{ $data->qs_no }}">
                        @else
                            <label>Quotation Number</label>
                            <select class="form-control kt-select2 init-select2" name="quotation" id="quotation">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_quotation as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->qs_no) selected @endif>{{$item->qs_no}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>

                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Bank Account No</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_bank as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->afi_acc_no) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="bank" id="bank" value="{{ $data->afi_acc_no }}">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @else
                            <label>Bank Account No</label>
                            <select class="form-control kt-select2 init-select2" name="bank" id="bank">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_bank as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->afi_acc_no) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>

                    @if ( $data->invoice_type_id == 1)
                        <!-- Tambahan -->
                        <div class="form-group">
                            <label>Insured Name</label>
                            <input type="text" class="form-control" name="insured_name" id="insured_name" value="{{ $data->insured_name}}">
                            <div class="invalid-feedback" id="sn">Silahkan Isi Insured Name</div>
                        </div>
                    @endif

                    <div class="form-group">
                        <label>Start Date Aging</label>
                        <div class="input-group date">
                            <input type="text" class="form-control" value="{{date('d F Y',strtotime($data->start_date))}}" readonly placeholder="Pilih Tanggal" name="tgl_mulai" id="tgl_mulai1" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                            <div class="invalid-feedback" id="sn">Silahkan Isi Start Date</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Valuta ID</label>
                        <select class="form-control kt-select2 init-select2" name="valuta_id" id="valuta_id">
                            <option value="" selected disabled>Silahkan Pilih</option>
                            @foreach($ref_valuta as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->valuta_id) selected @endif>{{$item->mata_uang}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">Silahkan Pilih</div>
                    </div>



                </div>

                <div class="col-md-4">
                    
                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Segment</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_cust as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->segment_id) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="segment" id="segment" value="{{ $data->segment_id }}">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @else
                            <label>Segment</label>
                            <select class="form-control kt-select2 init-select2" name="segment" id="segment">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_cust as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->segment_id) selected @endif>{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>

                    
                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Cover Not Number</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_proposal as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->proposal_no) selected @endif>{{$item->qs_no}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="proposal" id="proposal" value="{{ $data->proposal_no }}">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @else
                            <label>Cover Not Number</label>
                            <select class="form-control kt-select2 init-select2" name="proposal" id="proposal">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_proposal as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->proposal_no) selected @endif>{{$item->qs_no}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>

                    
                    <div class="form-group">
                        <label>Pajak Ditanggung Perusahaan</label>
                        <select class="form-control kt-select2 init-select2" name="is_tax_company" id="is_tax_company">
                            <option value="true">Ya</option>
                            <option value="false">Tidak</option>
                        </select>
                    </div>

                  


                    


                    <div class="form-group">
                        <label>Start Date Policy</label>
                        <div class="input-group date">
                            <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="start_date_police" value="{{date('d F Y',strtotime($data->start_date_polis))}}"  id="start_date_police" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                            <div class="invalid-feedback" id="sn">Silahkan Isi Start Date Policy</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label> Today Kurs </label>
                        {{-- <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text" class="form-control text-right money" onkeyup="convertToRupiah1(this)"  placeholder="0" name="today_kurs" id="today_kurs">
                            <div class="invalid-feedback">Silahkan Isi Today Kurs</div>
                        </div> --}}
                        <input type="text" class="form-control text-right money" value="{{number_format($data->kurs_today,2,',','.')}}"  placeholder="0" name="today_kurs" id="today_kurs" >
                        <div class="invalid-feedback">Silahkan Isi Today Kurs</div>
                    </div>

                    
                    <div class="form-group">
                        <!--
                        onkeyup="convertToRupiah1(this)"
                        -->
                        <label>Total Sum Insured</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text" class="form-control text-right money" value="{{number_format($data->ins_amount,2,',','.')}}" onkeyup="convertToRupiah1(this)" placeholder="0" name="insurance" id="insurance">
                            <div class="invalid-feedback">Silahkan Isi Sum Insured</div>
                            <input type="hidden" name="total_sum_insured" id="total_sum_insured" value="{{number_format($data->ins_amount,2,',','.')}}"> 
                        </div>
                    </div>


                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        @if ($data->invoice_type_id == 1)
                            <label>Agent</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_agent as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->agent_id) selected @endif>{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="officer" id="officer" value="{{ $data->agent_id }}">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @else
                            <label>Agent</label>
                            <select class="form-control kt-select2 init-select2" name="officer" id="officer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_agent as $item)
                                <option value="{{$item->id}}" @if($item->id==$data->agent_id) selected @endif>{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Validate Periode (In Days)</label>
                        <div class="input-group date">
                            @php
                              $datetime1 = new DateTime($data->start_date);
                              $datetime2 = new DateTime($data->end_date);
                              $difference = $datetime1->diff($datetime2);
                            @endphp
                            <input type="text" value="{{$difference->days}}" class="form-control"  onkeypress="return hanyaAngka(event)" maxlength="3" placeholder="Validate Periode" name="tgl_akhir" id="tgl_akhir2" />
                            {{--
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                            --}}
                            <div class="invalid-feedback" id="sn">Silahkan Isi End Date</div>
                        </div>
                    </div>



                    <div class="form-group">
                        <label>Number of Interest Insured</label>
                        <input type="text" class="form-control" value="{{$data->no_of_insured}}" name="no_of_insured" id="no_of_insured" onkeypress="return hanyaAngka(event)" maxlength="80">
                        <div class="invalid-feedback">Silahkan Pilih</div>
                    </div>

                     <div class="form-group">
                        <label>End Date Policy</label>
                        <div class="input-group date">
                            <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="end_date_police" id="end_date_police" value="{{date('d F Y',strtotime($data->end_date_polis))}}" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                            <div class="invalid-feedback" id="sn">Silahkan Isi End Date Policy</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Valas Amount</label>
                        <div class="input-group">
                            <input type="text" class="form-control money" placeholder="Valas Amount" value="{{number_format($data->valuta_amount,2,',','.')}}"  name="valas_amount" id="valas_amount" />
                            <div class="invalid-feedback" id="sn">Silahkan Isi Valas Amount</div>
                        </div>
                    </div>

                     <div class="form-group">
                        <label>Upload Dokumen</label>
                        <input type="file" class="form-control" name="file" id="file">
                        <a target="_blank" href="{{asset('upload_invoice')}}/{{$data->id}}_{{$data->url_dokumen}}">Download File</a>
                        <div class="invalid-feedback" id="ss">Silahkan Upload Dokumen</div>
                        <div class="invalid-feedback" id="ff">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                        <div class="invalid-feedback" id="mm">Max Size Dokumen 2 Mb</div>
                    </div>

                    @if ( $data->invoice_type_id == 1)
                        <div class="form-group">
                            <label>Endors Reason</label>
                            <div class="input-group">
                            <input type="text" class="form-control" placeholder="Endors reason" name="endorse_reason" id="endorse_reason" value="{{ $data->endors_reason }}"/>
                                <div class="invalid-feedback" id="error-endors-reason">Silahkan Isi Endors Reason</div>
                            </div>
                        </div>
                    @endif

                    

                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Gross Premium Amount</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text" class="form-control text-right money" value="{{number_format($data->premi_amount,2,',','.')}}" placeholder="0" onkeyup="convertToRupiah2(this)"  name="premi" id="premi">
                        </div>
                        <div class="invalid-feedback" id="pa">Silahkan Isi Premi Amount</div>
                        <div class="invalid-feedback" id="pa1">Premi Amount tidak boleh melebihi Sum Insured</div>                        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Discount Amount</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this)"  name="disc_amount" id="disc_amount" value="{{number_format($data->disc_amount,2,',','.')}}">
                            <div class="invalid-feedback" id="sd">Silahkan Isi Disc Amount</div>
                            <div class="invalid-feedback" id="mi">Disc Amount tidak boleh melebihi Premi Amount</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                   
                    <div class="form-group">
                        <label>Commission Due to HD Soeryo</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text"  class="form-control text-right money"  value="{{number_format($data->comp_fee_amount,2,',','.')}}" placeholder="0" id="fee_internal" name="fee_internal">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Policy Duty</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            {{-- <input type="text" class="form-control text-right money" onkeyup="convertToRupiah(this)" maxlength="11" placeholder="0" name="fee_polis" id="fee_polis" value="32.000"> --}}
                            <input type="text" class="form-control text-right money" onkeyup="convertToRupiah(this)" maxlength="11" placeholder="0" name="fee_polis" id="fee_polis" value="{{number_format($data->polis_amount,2,',','.')}}">
                            <div class="invalid-feedback">Silahkan Isi Fee Polis</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nett Premium</label>
                        <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" readonly class="form-control text-right money" onkeyup="convertToRupiah(this)" placeholder="0" name="nett_amount" id="nett_amount" value="{{number_format($data->net_amount,2,',','.')}}">
                            </div>
                    </div>

                   

                    <div class="form-group">
                        <label>Net To Underwriter</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text" readonly class="form-control text-right money" onkeyup="convertToRupiah(this)"  placeholder="0" id="asuransi" value="{{number_format($data->ins_fee,2,',','.')}}" name="asuransi">
                        </div>
                    </div>
                    

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Fee Agent</label>
                        <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text"  class="form-control text-right money" placeholder="0" value="{{number_format($data->agent_fee_amount,2,',','.')}}" name="fee_agent" id="fee_agent">
                            </div>
                    </div>

                    <div class="form-group">
                        <label>Stamp Duty</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text" class="form-control text-right" onkeyup="convertToRupiah(this)" maxlength="11" placeholder="0" name="fee_materai" id="fee_materai" value="{{number_format($data->materai_amount,2,',','.')}}">
                            {{-- <input type="text" class="form-control text-right" onkeyup="convertToRupiah(this)" maxlength="11" placeholder="0" name="fee_materai" id="fee_materai" value="6.000"> --}}
                            <div class="invalid-feedback">Silahkan Isi Fee Materai</div>
                        </div>
                    </div>

                   

                   
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Adm Duty</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text" class="form-control text-right" onkeyup="convertToRupiah(this)" placeholder="0" name="fee_admin" id="fee_admin" value="{{number_format($data->admin_amount,2,',','.')}}">
                            {{-- <input type="text" class="form-control text-right" onkeyup="convertToRupiah(this)" placeholder="0" name="fee_admin" id="fee_admin" value="5.000"> --}}
                            <div class="invalid-feedback">Silahkan Isi Fee Admin</div>
                        </div>
                    </div>
                    
               

                    <div class="form-group">
                        <label>Agent Tax Amount</label>
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                            <input type="text"  class="form-control text-right money" value="{{number_format($data->tax_amount,2,',','.')}}" placeholder="0" id="tax_amount" name="tax_amount">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Type Invoice -->
            <input type="hidden" value={{$data->invoice_type_id}} name="invoice_type_id" id="invoice_type_id"> 
        </form>
        <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button onclick="return simpan_new_invoice();" class="btn btn-success">Update</button>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- end:: Content -->
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    //minDate: new Date(1999, 10 - 1, 25)
    /*
    $(document).ready(function(){
    $('#tgl_mulai').datepicker({ onClick: function(dateText, inst) { alert("Working"); } });
    });
    */

    $('#start_date_police').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     });

    $(document).ready(function(){
        $('#start_date_police').change(function () {
            $('#end_date_police').val('');
           $('#end_date_police').datepicker('destroy');
            console.log(this.value);
            $('#end_date_police').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                startDate: this.value
            });

        });
    });
    
    $(document).ready(function(){

        $('#tgl_mulai').change(function () {
            $('#tgl_akhir').val('');
            $('#tgl_akhir').datepicker('destroy');
            console.log(this.value);
            $('#tgl_akhir').datepicker({
                format: 'dd MM yyyy',
                startDate: this.value
            });
        });

        @if ($data->invoice_type_id == 1)
            // disabled top open select2 when enter
            $(document).on('keydown', '.select2-selection', function (evt) {
                if (evt.which === 13) {
                    console.log('disable select 2 enter');
                    $(".init-select2").select2('close');
                }
            });
        @endif

    }); // end document ready


    
    $("#valas_amount").keyup(function (elm) {
        var today_kurs = $("#today_kurs").val();

        if ( today_kurs != "" ) {
            today_kurs = today_kurs.replace(/\./g,'');
            today_kurs = today_kurs.replace(/\,/g,'.');

            var valas_amount = $(this).val();
            valas_amount = valas_amount.replace(/\./g,'');
            valas_amount = valas_amount.replace(/\,/g,'.');

            var hasil = today_kurs * valas_amount;
            hasil = hasil.toFixed(2).replace(/\./g, ",");

            console.log('today kurs : ' + today_kurs);
            console.log('valas amount : ' + valas_amount);
            console.log('hasil : ' + hasil);
            
            
            $('#insurance').val(hasil).trigger('mask.maskMoney');
            convertToRupiah1($("#insurance"));

            $("#total_sum_insured").val(hasil);

        } 
        
    }); 

    $("#today_kurs").keyup(function (elm) {
        var valas_amount = $("#valas_amount").val();
        var today_kurs = $(this).val();

        if ( valas_amount != "" ) {
            today_kurs = today_kurs.replace(/\./g,'');
            today_kurs = today_kurs.replace(/\,/g,'.');

            valas_amount = valas_amount.replace(/\./g,'');
            valas_amount = valas_amount.replace(/\,/g,'.');

            var hasil = today_kurs * valas_amount;
            hasil = hasil.toFixed(2).replace(/\./g, ",");

            console.log('today kurs : ' + today_kurs);
            console.log('valas amount : ' + valas_amount);
            console.log('hasil : ' + hasil);
            
            $('#insurance').val(hasil).trigger('mask.maskMoney');
            convertToRupiah1($("#insurance"));

            $("#total_sum_insured").val(hasil);
            
        } 
        
    });  

    $("#valuta_id").on('change', function(elm) {
        var value = $(this).val();

        if ( value == "1" ) {
            // IDR SELECTED
            $("#today_kurs").prop('disabled', true);
            $("#valas_amount").prop('disabled', true);
            $("#insurance").prop('disabled', false);

            $("#today_kurs").val('');
            $("#valas_amount").val('');
            $("#insurance").val('');

            $('#premi').val('');
            $('#disc_amount').val('');
            $('#nett_amount').val('');
            $('#fee_agent').val('');
            $('#fee_internal').val('');
            $('#asuransi').val('');
            $('#tax_amount').val('');
        } else {
            $("#today_kurs").prop('disabled', false);
            $("#valas_amount").prop('disabled', false);
            $("#insurance").prop('disabled', true);
        }

    });

    if ( $("#valuta_id").val() == "1" ) {
            // IDR SELECTED
            $("#today_kurs").prop('disabled', true);
            $("#valas_amount").prop('disabled', true);
            $("#insurance").prop('disabled', false);

        } else {
            $("#today_kurs").prop('disabled', false);
            $("#valas_amount").prop('disabled', false);
            $("#insurance").prop('disabled', true);
        }

    $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    /*
    $('#tgl_mulai').datepicker({
        format: 'dd MM yyyy'
    });
    */
    $('#start_date_police').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     });

     $('#end_date_police').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     });

    var date = new Date();
    
    $('#tgl_mulai1').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        startDate: date
     });
    
    /*
    $('#tgl_mulai').datepicker({
                format: 'dd/mm/yyyy',
                onSelect: function(dateText) {
                    console.log("Selected date: " + dateText + "; input's current value: " + this.value);
                }
            });
    */
    
    $('#invoice').keyup(function() {
        var panjang = this.value.length;

        if(panjang==80){
            $('#invoice').addClass( "is-invalid" );
            $('#sn').hide();
            $('#mk').show();
        }else{
            $('#invoice').removeClass( "is-invalid" );  
            $('#sn').hide();
            $('#mk').hide();   
        }
    });

    // var nilaiAsuransi;
    // $("#fee_polis").keyup( function() {
    //     var police_duty = parseFloat($(this).val().replace('.', ''));
    //     var stamp_duty = parseFloat($("#fee_materai").val().replace('.', ''));
    //     var hasil = police_duty + stamp_duty + nilaiAsuransi;

    //     console.log('Nilai asuransi : ' + nilaiAsuransi);
    //     console.log('police duty : ' + police_duty);
    //     console.log('stamp duty : ' + stamp_duty);
        
        
    //     $("#asuransi").val(hasil);
    //     $('#asuransi').val(hasil).trigger('mask.maskMoney');
    //     console.log(hasil);

    // });

    // $("#fee_materai").keyup( function() {
    //     var stamp_duty = parseFloat($(this).val().replace('.', ''));
    //     var police_duty = parseFloat($("#fee_polis").val().replace('.', ''));
    //     var hasil = police_duty + stamp_duty + nilaiAsuransi;

    //     console.log('Nilai asuransi : ' + nilaiAsuransi);
    //     console.log('police duty : ' + police_duty);
    //     console.log('stamp duty : ' + stamp_duty);
        
        
    //     $("#asuransi").val(hasil);
    //     $('#asuransi').val(hasil).trigger('mask.maskMoney');
    //     console.log(hasil);

    // });

    function convertToRupiah2 (objek) {
     
        var premi=$('#premi');
        var insurance=$('#insurance');
        var disc_amount=$('#disc_amount');

        if(premi.val()===''){
            premi.addClass( "is-invalid" );
            document.getElementById("disc_amount").value="";
        }else{

            var aa1=insurance.val();
            var aa2=aa1.replace(/\./g,'');

            var bb1=premi.val();
            var bb2=bb1.replace(/\./g,'');
            var smd1=Math.floor(b2) > Math.floor(a2);

            if(smd){
               premi.addClass( "is-invalid" );
               $('#pa').hide();
               $('#pa1').show();
            }else{
                premi.removeClass( "is-invalid" )
            }
            


            var a1=premi.val();
            var a2=a1.replace(/\./g,'');
            var a3=a2.replace(/\,/g,'.');

            var b1=disc_amount.val();
            var b2=b1.replace(/\./g,'');
            var b3=b2.replace(/\,/g,'.');
            
            var smd=Math.floor(b3) > Math.floor(a3);

            if(smd){
               disc_amount.addClass( "is-invalid" );
               $('#sd').hide();
               $('#mi').show();
              // disc_amount.val(insurance.val());
            }else{

               var c2= a3 - b3;

               $nett=c2.toFixed(2).replace(/\./g, ",");
                $('#nett_amount').val($nett).trigger('mask.maskMoney');

                //$('#nett_amount').val(c2);


            
            $.ajax({
                type: 'GET',
                url: base_url + '/fee1?jml='+a3+'&company='+$('#is_tax_company').val(),
                success: function (res) {
                    var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                    
                        $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                        $('#fee_agent').val($agent).trigger('mask.maskMoney');


                        $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                        $('#fee_internal').val($internal).trigger('mask.maskMoney');
                        
                       

                        var aa1=$('#nett_amount').val();
                        var aa2=aa1.replace(/\./g,'');
                        var aa3=aa2.replace(/\,/g,'.');

                        var asuransi=aa3 - data.fee_agent - data.komisi_perusahaan;
                           
                        console.log('asuransi');
                        console.log(aa3);
                        console.log(aa3 - data.fee_agent - data.ko);
                        console.log(data.fee_agent);
                        console.log(data.komisi_perusahaan);
                        console.log(asuransi);
                        
                        $asuransi=asuransi.toFixed(2).replace(/\./g, ",");               
                        $('#asuransi').val($asuransi).trigger('mask.maskMoney');;

                        $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");    
                        $('#tax_amount').val($amount).trigger('mask.maskMoney');;
                        
                }
            });             

               disc_amount.removeClass( "is-invalid" );
            }
            premi.removeClass( "is-invalid" );
        }

    }

    function convertToRupiah1 (objek) {

        var insurance=$('#insurance');

        var police_duty = parseFloat($("#fee_polis").val().replace('.', ''));
        var stamp_duty = parseFloat($("#fee_materai").val().replace('.', ''));

        /*
        var disc_amount=$('#disc_amount');

        var polis=$('#fee_polis');
        var admin=$('#fee_admin');
        var materai=$('#fee_materai');
    */
        if(insurance.val()===''){
            insurance.addClass( "is-invalid" );
        //    document.getElementById("disc_amount").value="";
        }else{

            var a1=insurance.val();
            var a2=a1.replace(/\./g,'');
            
            $.ajax({
                type: 'GET',
                url: base_url + '/fee?jml='+a2+'&company='+$('#is_tax_company').val(),
                success: function (res) {
                    var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        
                        //.trigger('mask.maskMoney')
                        $premi=data.premi_amount.toFixed(2).replace(/\./g, ",");
                        $('#premi').val($premi).trigger('mask.maskMoney');
                        //$('#premi').maskMoney("#,##0.00", {reverse: true});
                        //$("#premi").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

                        $discount=data.discount.toFixed(2).replace(/\./g, ",");
                        $('#disc_amount').val($discount).trigger('mask.maskMoney');
                                               

                        $nett=data.nett_amount.toFixed(2).replace(/\./g, ",");
                        $('#nett_amount').val($nett).trigger('mask.maskMoney');
                        

                        $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                        $('#fee_agent').val($agent).trigger('mask.maskMoney');
                        


                        $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                        $('#fee_internal').val($internal).trigger('mask.maskMoney');


                        // nilaiAsuransi = data.asuransi;
                        // $asuransi = data.asuransi + police_duty + stamp_duty;
                        // $asuransi = $asuransi.toFixed(2).replace(/\./g, ",");

                        $asuransi=data.asuransi.toFixed(2).replace(/\./g, ",");
                        $('#asuransi').val($asuransi).trigger('mask.maskMoney');
                        

                        $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");
                        $('#tax_amount').val($amount).trigger('mask.maskMoney');


                }
            });
            insurance.removeClass( "is-invalid" );
        }

    }
var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;
function znClose() {
    znIconboxClose();
    $("#form-new-invoice").data('bootstrapValidator').resetForm();
    $("#form-new-invoice")[0].reset();
}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('invoice_list') }}');
}


var _create_form = $("#form-new-invoice");
function simpan_new_invoice(){
    loadingPage();
    if($('#tgl_tr').val()===''){
        endLoadingPage();
        $( "#tgl_tr" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_tr").removeClass( "is-invalid" );
    }
    if($('#invoice').val()===''){
        endLoadingPage();
        $( "#invoice" ).addClass( "is-invalid" );
        $('#sn').show();
        $('#mk').hide();
        return false;
    }else{
        $("#invoice").removeClass( "is-invalid" );

    }
    if($('#tgl_mulai').val()===''){
        endLoadingPage();
        $( "#tgl_mulai" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_mulai").removeClass( "is-invalid" );

    }
    if($('#tgl_akhir').val()===''){
        endLoadingPage();
        $( "#tgl_akhir" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_akhir").removeClass( "is-invalid" );

    }

    if($('#insurance').val()===''){
        endLoadingPage();
        $( "#insurance" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#insurance").removeClass( "is-invalid" );

    }

    // TAMBAHAN 
    if($('#fee_polis').val()===''){
        // endLoadingPage();
        // $( "#fee_polis" ).addClass( "is-invalid" );
        // return false;
        $("#fee_polis").val("0,00");
    }else{
        $("#fee_polis").removeClass( "is-invalid" );
    }

    if ( $("#premi").val() === '' ) {
        $("#premi").val("0,00");
    }    

    if ( $("#fee_internal").val() === '' ) {
        $("#fee_internal").val("0,00");
    }    

    if ( $("#fee_agent").val() === '' ) {
        $("#fee_agent").val("0,00");
    }    

    if ( $("#tax_amount").val() === '' ) {
        $("#tax_amount").val("0,00");
    }    

    if($('#fee_materai').val()===''){
        // endLoadingPage();
        // $( "#fee_materai" ).addClass( "is-invalid" );
        // return false;

        $("#fee_materai").val("0,00");
    }else{
        $("#fee_materai").removeClass( "is-invalid" );

    }

    if($('#fee_admin').val()===''){
        // endLoadingPage();
        // $( "#fee_admin" ).addClass( "is-invalid" );
        // return false;

        $("#fee_admin").val("0,00");
    }else{
        $("#fee_admin").removeClass( "is-invalid" );

    }
    //END TAMBAHAN


    if($('#disc_amount').val()===''){
        // endLoadingPage();
        // $( "#disc_amount" ).addClass( "is-invalid" );
        // $('#sd').show();
        // $('#mi').hide();
        // return false;
        $("#disc_amount").val("0,00");
    }else{
        
        $("#disc_amount").removeClass( "is-invalid" );
    }

    if($('#valuta_id').val()===''){
        endLoadingPage();
        $( "#valuta_id" ).addClass( "is-invalid" );
        return false;
    }else{
        
        $("#valuta_id").removeClass( "is-invalid" );
    }



     var a1=$('#insurance').val();
     var a2=a1.replace(/\./g,'');

     var b1=$('#disc_amount').val();
     var b2=b1.replace(/\./g,'');
     var smd=Math.floor(b2) > Math.floor(a2);

     if(smd){
        endLoadingPage();
        $( "#disc_amount" ).addClass( "is-invalid" );
        $('#sd').hide();
        $('#mi').show();
        return false;
    }else{
        $("#disc_amount").removeClass( "is-invalid" );
    }
    if($('#id_underwriter').val()===''){
        endLoadingPage();
        $( "#id_underwriter" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#id_underwriter").removeClass( "is-invalid" );

    }
    if($('#file').val()===''){
        endLoadingPage();
    }else{
        var size=$('#file')[0].files[0].size;
        console.log(size);
        var extension=$('#file').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            endLoadingPage();
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').hide();
            $('#mm').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
            endLoadingPage();
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').show();
            $('#mm').hide();
            return false;
        }

        $( "#file" ).removeClass( "is-invalid" );

    }

    





    var _form_data = new FormData(_create_form[0]);
    $.ajax({
        type: 'POST',
        url: base_url + '/update_invoice_baru',
        data: _form_data,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (res) {
            endLoadingPage();
            //_create_form[0].reset();
            console.log(res['data']);
            znIconbox("Data Berhasil Diupdate",text,action);
            /*
            swal.fire({
                title: 'Info',
                text: "Berhasil diupdate",
                type: 'success',
                confirmButtonText: 'Tutup',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    location.reload();
                }
            });
            */
        }
    });
}    


function convertToRupiah_polis(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c; 


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );  
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');
    
    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');
    
    var c1=polis.val();
    var c2=c1.replace(/\./g,'');
    
    var d1=admin.val();
    var d2=d1.replace(/\./g,'');
    
    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);


       var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);     

        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
                        

                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
                        

                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });           


    }

    
 }   

}

function convertToRupiah_admin(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c; 


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );  
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');
    
    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');
    
    var c1=polis.val();
    var c2=c1.replace(/\./g,'');
    
    var d1=admin.val();
    var d2=d1.replace(/\./g,'');
    
    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);                

        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
                        

                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
                        

                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });           


    }

    
 }   

}


function convertToRupiah_materai(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c; 


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );  
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');
    
    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');
    
    var c1=polis.val();
    var c2=c1.replace(/\./g,'');
    
    var d1=admin.val();
    var d2=d1.replace(/\./g,'');
    
    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);                

        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
                        

                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
                        

                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });           


    }

    
 }   

}

</script>
@stop
@section('script')
@stop
