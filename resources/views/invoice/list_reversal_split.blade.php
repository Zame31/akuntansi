@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Sales </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Reversal Split Payment </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Reversal Split Payment
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <!--
                                <a href="#" onclick="kirimsemua_invoice();" class="btn btn-pill btn-primary"
                                    style="color: white;">Approval All</a>
                                 -->
                                <a href="#" onclick="kirim_invoice();" class="btn btn-pill btn-primary"
                                    style="color: white;">Approval Selected</a>
                                <!--
                                <a href="#" onclick="hapussemua_invoice();" class="btn btn-pill btn-danger"
                                    style="color: white;">Reject All</a>
                                -->
                                <a href="#" onclick="hapus_invoice();" class="btn btn-pill btn-danger"
                                    style="color: white;">Reject Selected</a>
                            </div>
                        </div>
                        {{-- <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </a>
                        </div> --}}
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" id="example-select-all" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Customer Name</th>
                                    <th>Product Name</th>
                                    <th>Police Number</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if($data)
                            @foreach($data as $item)

                            @if($item->is_pilih=='2')
                            <tr>
                                <td>
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="{{$item->id}}_{{$item->keterangan}}" class="kt-group-checkable">
                                        <span></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#" onclick="detail_invoice({{$item->id}})"><i
                                                class="la la-clipboard"></i> Detail</a>
                                            </div>
                                    </div>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->definition}}</td>
                                <td>{{$item->polis_no}}</td>
                                <td>{{$item->keterangan}}</td>
                                <td>{{number_format($item->total,2,',','.')}}</td>

                            </tr>
                            @endif

                            @endforeach
                            @endif

                        </tbody>
                        </table>
                </div>
            </div>
        </div>


        <!-- end:: Subheader -->

    </div>
</div>
@include('transaksi.invoice_modal')
<script type="text/javascript">
function ubah_invoice(id){
    loadingPage();
    window.location.href = base_url +'edit_invoice?id=' + id;
    endLoadingPage();
}
$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});
function kirimsemua_invoice() {
        swal.fire({
           title: "Info",
           text: "Kirim Semua Data",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
               loadingPage();

            $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAproval_reversal1?type=semua',
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);

                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });

                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });

   }

   function hapussemua_invoice() {
        swal.fire({
           title: "Info",
           text: "Delete Semua data",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                        type: 'POST',
                        url: base_url + '/hapusAproval_reversal1?type=semua',
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });

            }
        });
   }

function kirim_invoice() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin kirim record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/rev_split_new',
                        data: {value: value},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
    }
}

function hapus_invoice() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin delete record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                        type: 'POST',
                        url: base_url + '/rev_split_reject_new',
                        data: {value: value},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });

            }
        });
    }
}

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}
</script>

@include('invoice.action')
@stop
