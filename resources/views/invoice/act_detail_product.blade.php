<script>
     var id_array_data = 0;
    var data_detail_product = [];
    
function detail_invoice_product(prod_id,id_sales,invoice_type) {

    if (id_sales) {
        $.ajax({
           type: "GET",
           url: base_url + '/quotation_slip/get_detail_insured/' + prod_id,
           data: {},

           beforeSend: function () {
                loadingPage();
           },

           success: function (res) {
               var data = res.data;

               console.log('data detail product',data);


               console.log(data.length);
               if (res.rc == 1) {
                   var tableHeaders = "";
                   tableHeaders = `<th class="text-center all"> Status </th>`;
                   data.forEach(function ( value, index) {
                    let isShow = (value.is_showing) ? 'all':'none';
                       tableHeaders += `<th class="text-center ${isShow}">${value.label}</th>`;
                   })
                   $("#tableDiv").empty();

                   if (data.length > 0) {
                       $("#tableDiv").append('<table id="table_detail" class="table table-striped- table-hover table-checkable"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                       DataTable = $('#table_detail').DataTable();

                       if (id_sales) {
                           getDetailProduct(id_sales,'detail',invoice_type);
                       }
                   }

                
                   
                 
               } else {
                   toastr.error(data.rm);
               }
           }
       }).done(function (msg) {
           endLoadingPage();
       }).fail(function (msg) {
           endLoadingPage();
           toastr.error("Terjadi Kesalahan");
       });
    }
    else{
        toastr.info("Data Tidak Tersedia, Tidak Ada Relasi Dengan Master Policy");
    }

     
}

function getDetailProduct(id,typeTable,invoice_type) {
    console.log('idss',id);
    console.log('type',typeTable);
    console.log('invoice_type',invoice_type);

       $.ajax({
           type: 'GET',
           url: base_url + 'marketing/data/detail_product_list/' + id,
           beforeSend: function (res) {
               loadingPage();
           },
           success: function (res) {

               var dataDetail =res.rm;

               console.log('datass',dataDetail);

              
               dataDetail.forEach((vv,i) => {
                   id_array_data = i;
                   temp_data = [];
                   let rows = [];

                   if (typeTable == 'normal') {
                       var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       <i class="flaticon-more"></i>
                                   </button>
                                   <div class="dropdown-menu dropdown-menu-right">
                                       <a class="dropdown-item" onclick="modal_add_detail('edit', '${id_array_data}')">
                                           <i class="la la-edit"></i> Edit
                                       </a>
                                       <a class="dropdown-item" onclick="hapus_detail('${id_array_data}', this)">
                                           <i class="la la-trash"></i> Hapus
                                       </a>`;

                       var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';

                       rows.unshift(sts);
                       rows.unshift(action);
                   }

                    // JIKA ENDORS
                   if (invoice_type == 1) {
                           
                        // if (vv[0]['value_text'] == 2 || vv[0]['value_text'] == 3) {
                            
                            vv.forEach(data => {

                                if (data.input_name == 'sts') {
                                    if (data.value_text == 't') {
                                        var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" style="width:90px;">Aktif</span>';
                                    }else if(data.value_text == 2){
                                        var sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Endors Aktif</span>';
                                    }else if(data.value_text == 3){
                                        var sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Endors Baru</span>';
                                    } else {
                                        var sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="width:90px;">Non Aktif</span>';
                                    }
                                    rows.unshift(sts);

                                    
                                }

                                if (data.input_name != 'sts') {
                                    rows.push(data.value_text);
                                }


                                var item = {
                                    "name" : data.input_name,
                                    "value": data.value_text,
                                    "bit_id" : data.bit_id,
                                    };
                                    temp_data.push(item);

                                });

                                data_detail_product.push({
                                    id : id_array_data,
                                    value : temp_data
                                });

                                DataTable.row.add(rows).draw();
                        // }

                    }else{

                        // NORMAL
                        console.log(vv[i]);

                        console.log(vv[0]);

                        // if (vv[0]['value_text'] == 't' || vv[0]['value_text'] == 'f') {
                            vv.forEach(data => {

                                if (data.input_name == 'sts') {
                                    if (data.value_text == 't') {
                                        var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" style="width:90px;">Aktif</span>';
                                    }else if(data.value_text == 2){
                                        var sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Endors Aktif</span>';
                                    }else if(data.value_text == 3){
                                        var sts = '<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill" style="width:90px;">Endors Baru</span>';
                                    } else {
                                        var sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="width:90px;">Non Aktif</span>';
                                    }
                                    rows.unshift(sts);
                                }

                                if (data.value_text != 't') {
                                    rows.push(data.value_text);
                                }


                                var item = {
                                "name" : data.input_name,
                                "value": data.value_text,
                                "bit_id" : data.bit_id,
                                };
                                temp_data.push(item);

                            });

                            data_detail_product.push({
                                id : id_array_data,
                                value : temp_data
                            });

                            DataTable.row.add(rows).draw();
                        // }
                    }



                  
               });

               endLoadingPage();

               $('#detail_product').modal('show');

           }
       });
}
</script>