@section('content')
@php
    $data_comp = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
@endphp
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Sales </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    {{$type}} Debit Note </a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form {{$type}} Debit Note
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="return simpan_new_invoice();" class="btn btn-success">Simpan</button>
                        @if ($type == 'Edit')
                        <button onclick="loadNewPage('{{ route('invoice_list') }}')" class="btn btn-danger">Kembali</button>

                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body" style="
        background: #fbfbfb;
    ">
        <form id="form-new-invoice" enctype="multipart/form-data">
            <input type="hidden" id="get_id" name="get_id" value="">
            <input type="hidden" name="invoice_type_id" id="invoice_type_id" value="0">

            <div class="row zn-border-bottom mb-5">
                @if ($type == 'Edit')
                <div class="col-md-4">
                    <div class="form-group">
                        <label>COC No</label>
                        <select onchange="setCOC(this.value)" disabled class="form-control zn-readonly" readonly name="coc_no" id="coc_no">
                            <option disabled value="">Silahkan Pilih</option>
                            @foreach($master_sales_polis as $item)
                                <option value="{{$item->id}}">{{$item->cf_no}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @else
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select COC No</label>
                        <select onchange="setCOC(this.value)" class="form-control kt-select2 init-select2" name="coc_no" id="coc_no">
                            <option disabled value="">Silahkan Pilih</option>
                            @foreach($master_sales_polis as $item)
                                <option value="{{$item->id}}">{{$item->cf_no}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">Silahkan Pilih</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="searchPolicy();" class="btn btn-info" style="margin-top: 25px;">Search</button>
                </div>
            
            @endif

                {{-- <div class="col-md-4">
                    <div class="form-group">
                        <label>Select COC No</label>
                        <select onchange="setCOC(this.value)" class="form-control kt-select2 init-select2" name="coc_no" id="coc_no">
                            <option disabled value="">Silahkan Pilih</option>
                            @foreach($master_sales_polis as $item)
                            <option value="{{$item->id}}">{{$item->cf_no}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">Silahkan Pilih</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="searchPolicy();" class="btn btn-info" style="margin-top: 25px;">Search</button>
                </div> --}}
                
            </div>

            <div id="dataShow" style="display: none;">

                <div class="row zn-border-bottom mb-5">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Underwriter</label>
                            <select class="form-control zn-readonly" name="id_underwriter" id="id_underwriter">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_underwriter as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Policy Number</label>
                            <input type="text" readonly class="form-control" name="no_polis" id="no_polis" maxlength="80">
                            <div class="invalid-feedback" id="sn">Silahkan Isi No Polis</div>
                            <div class="invalid-feedback" id="mk">Maximal 80 karakter</div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Customer Name</label>
                            {{-- <select class="form-control kt-select2 init-select2" name="customer" id="customer" onchange="get_agent(this);"> --}}
                            <select class="form-control zn-readonly" name="customer" id="customer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_customer as $item)
                                    <option value="{{$item->id}}">{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
    
                    </div>
                    {{-- <div class="col-md-1">
                        <button type="button" onclick="modal_add_customer()"
                            class="btn btn-success btn-elevate btn-icon" style="margin-top: 25px;">
                            <i class="la la-plus"></i>
                        </button>
                    </div> --}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Policy Customer Name</label>
                            <input type="text" class="form-control" readonly name="policy_cust_name" id="policy_cust_name" maxlength="100">
                        </div>
                    </div>
    
                </div>
    
                <div class="row zn-border-bottom mb-5">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Product Name</label>
                            <select class="form-control zn-readonly" readonly name="product" id="product" >
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_product as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Segment</label>
                            <select class="form-control kt-select2 init-select2" name="segment" id="segment">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_cust as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
                    </div>
    
                    <div class="col-md-4">
    
                        <div class="form-group">
                            <label>Agent</label>
                            <div id="container-agent">
                                <select class="form-control zn-readonly" name="officer" id="officer">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach($ref_agent as $item)
                                    <option value="{{$item->id}}">{{$item->full_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
                    </div>
    
                    <div class="col-md-4">
    
    
                        <div class="form-group">
                            <label>Quotation Number</label>
                            <select class="form-control zn-readonly" name="quotation" id="quotation">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_quotation as $item)
                                <option value="{{$item->id}}">{{$item->qs_no}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
                        <div class="form-group">
                            <label>Bank Account No</label>
                            <select class="form-control kt-select2 init-select2" name="bank" id="bank">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_bank as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
    
                        <div class="form-group">
                            <label>Start Date Aging</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="tgl_mulai" id="tgl_mulai1" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi Start Date</div>
                            </div>
                        </div>
    
    
                        <div class="form-group">
                            <label>Valuta ID</label>
                            <select class="form-control kt-select2 init-select2" name="valuta_id" id="valuta_id">
                                <option value="" selected disabled>Silahkan Pilih</option>
                                @foreach($ref_valuta as $item)
                                    <option value="{{$item->id}}">{{$item->mata_uang}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
    
    
                    </div>
    
                    <div class="col-md-4">
    
                        <div class="form-group">
                            <label>Proposal</label>
                            <select class="form-control zn-readonly" name="proposal" id="proposal">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_proposal as $item)
                                <option value="{{$item->id}}">{{$item->cn_no}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
    
                        <div class="form-group">
                            <label>Pajak Ditanggung Perusahaan</label>
                            <select class="form-control kt-select2 init-select2" onchange="changePPH(this.value);feeAgentChange();" name="is_tax_company" id="is_tax_company">
                                <option value="">Pilih </option>
                                <option value="true">Ya</option>
                                <option value="false">Tidak</option>
                            </select>
                        </div>
    
    
    
    
                        <div class="form-group">
                            <label>Start Date Policy</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="start_date_police" id="start_date_police" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi Start Date Policy</div>
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label> Today Kurs </label>
                            <input type="text" class="form-control text-right money" placeholder="0" name="today_kurs" id="today_kurs">
                            <div class="invalid-feedback">Silahkan Isi Today Kurs</div>
                        </div>
    
    
                        <div class="form-group">
                            <!--
                            onkeyup="convertToRupiah1(this)"
                            -->
                            <label>Total Sum Insured</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" class="form-control text-right money" onkeyup="convertToRupiah1(this);" placeholder="0" name="insurance" id="insurance">
                                <div class="invalid-feedback">Silahkan Isi Sum Insured</div>
                                 <input type="hidden" name="total_sum_insured" id="total_sum_insured">
                            </div>
                        </div>
                    </div>
    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Validate Periode (in Days)</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" onkeypress="return hanyaAngka(event)" maxlength="3" placeholder="Validate Periode" name="tgl_akhir" id="tgl_akhir2" />
                                {{--
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                --}}
                                <div class="invalid-feedback" id="sn">Silahkan Isi End Date</div>
                            </div>
                        </div>
    
    
                        <div class="form-group">
                            <label>Number of Interest Insured</label>
                            <input type="text" class="form-control" name="no_of_insured" id="no_of_insured" onkeypress="return hanyaAngka(event)" maxlength="80">
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>
                         <div class="form-group">
                            <label>End Date Policy</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="end_date_police" id="end_date_police" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi End Date Policy</div>
                            </div>
                        </div>
    
                        <div style="display:none;" class="form-group">
                            <label>Valas Amount</label>
                            <div class="input-group">
                                <input type="text" class="form-control money" placeholder="Valas Amount" name="valas_amount" id="valas_amount" />
                                <div class="invalid-feedback" id="sn">Silahkan Isi Valas Amount</div>
                            </div>
                        </div>
    
    
                        <div class="form-group">
                            <label>Upload Dokumen</label>
                            <input type="file" class="form-control" name="file" id="file">
                            @if ($type == 'Edit')
                            <a target="_blank" href="{{asset('upload_invoice')}}/{{$data->id}}_{{$data->url_dokumen}}">Download File</a>
                            @endif
    
                            <div class="invalid-feedback" id="ss">Silahkan Upload Dokumen</div>
                            <div class="invalid-feedback" id="ff">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                            <div class="invalid-feedback" id="mm">Max Size Dokumen 2 Mb</div>
                        </div>
    
    
    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gross Premium Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this);feeAgentChange();"  name="premi" id="premi">
                            </div>
                            <div class="invalid-feedback" id="pa">Silahkan Isi Premi Amount</div>
                            <div class="invalid-feedback" id="pa1">Premi Amount tidak boleh melebihi Sum Insured</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Discount Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this);feeAgentChange();"  name="disc_amount" id="disc_amount">
                                <div class="invalid-feedback" id="sd">Silahkan Isi Disc Amount</div>
                                <div class="invalid-feedback" id="mi">Disc Amount tidak boleh melebihi Premi Amount</div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="row mb-5">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>EF</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text" style="
                                    background: #fd2762;
                                    color: #fff;
                                    font-weight: bold;
                                ">%</span></div>
                                <input onkeyup="setEF();feeAgentChange();" type="text" class="form-control text-right money" maxlength="6" placeholder="0" name="ef_pct" id="ef_pct">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>EF Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input onkeyup="feeAgentChange()" type="text" disabled class="form-control text-right money" placeholder="0" name="ef" id="ef">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>PPH</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input onkeyup="feeAgentChange()" type="text" class="form-control text-right money" placeholder="0" name="pph" id="pph">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                @if ($data_comp->app_name == "ATA HD")
                                Commission Due to HD Soeryo
                                @else
                                Commission Due to Us
                                @endif
                               </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money"  placeholder="0" id="fee_internal" name="fee_internal">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Policy Duty</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" maxlength="11" placeholder="0" name="fee_polis" id="fee_polis" value="{{number_format(0,2,",",".")}}">
                                <div class="invalid-feedback">Silahkan Isi Fee Polis</div>
                            </div>
                        </div>
    
    
    
                        <div class="form-group">
                            <label>Nett Premium</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text"  readonly class="form-control text-right money zn-readonly" placeholder="0" name="nett_amount" id="nett_amount">
                                </div>
                        </div>
    
    
    
    
                    </div>
                    <div class="col-md-4">
    
                        <div class="form-group">
                            <label>Commission Due to Agent</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                    <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" placeholder="0" name="fee_agent" id="fee_agent">
                                </div>
                        </div>
    
                        <div class="form-group">
                            <label>Stamp Duty</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                {{-- <input type="text" class="form-control text-right money"  maxlength="11" placeholder="0" name="fee_materai" id="fee_materai" value="6.000"> --}}
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" maxlength="11" placeholder="0" name="fee_materai" id="fee_materai" value="{{number_format(0,2,",",".")}}">
                                <div class="invalid-feedback">Silahkan Isi Fee Materai</div>
                            </div>
                        </div>
    
    
                        <div class="form-group">
                            <label>Nett To Underwriter</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" readonly class="form-control text-right money zn-readonly"  placeholder="0" id="asuransi" name="asuransi">
                            </div>
                        </div>
    
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Agent Tax Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text"  class="form-control text-right money" placeholder="0" id="tax_amount" name="tax_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                @if ($data_comp->app_name == "ATA HD")
                                HD Soeryo Duty
                                @else
                                Admin Duty
                                @endif
                                </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">IDR</span></div>
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" placeholder="0" name="fee_admin" id="fee_admin" value="{{number_format(0,2,",",".")}}">
                                <div class="invalid-feedback">Silahkan Isi Fee Admin</div>
                            </div>
                        </div>
    
    
    
                    </div>
                </div>

            </div>

        </form>
        <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button onclick="return simpan_new_invoice();" class="btn btn-success">Simpan</button>
                    </div>
                </div>
        </div>
    </div>
</div>
@include('invoice.modal_add_customer')
@include('marketing.modal.modal_search_policy')


@php
    if ($type == 'Edit'){
        $datetime1 = new DateTime($data->start_date);
        $datetime2 = new DateTime($data->end_date);
        $difference = $datetime1->diff($datetime2);

        if ($data->is_tax_company == 1) {
            $is_tax_c = 'true';
        }else{
            $is_tax_c = 'false';
        }
    }
@endphp

<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function () {

    $('#coc_no').select2('destroy');
    $('#coc_no').val(null).select2();

    // $('#pph').val(0);
    // $('#pph').prop('disabled',true);
 });

var typeStore = '{{$type}}';

if (typeStore == 'Edit') {
    $('#dataShow').show();
}else{
    $('#dataShow').hide();
}

function changePPH(val) {
    if (val == 'true') {
        $('#pph').val(0);
        $('#pph').prop('disabled',true);
    }else{
        $('#pph').prop('disabled',false);
    }
}

function setCOC(id) {
    console.log('coc_id',id);
    $.ajax({
        type: 'GET',
        url: base_url + 'marketing/data/master_sales_polis_byid/' + id,
        beforeSend: function (res) {
            loadingPage();
        },
        success: function (res) {
            console.log(res);
            let data = res.rm;
            $('#insurance').val(formatMoney(data.ins_amount));
            $('#no_of_insured').val(data.no_of_insured);
          
            if (data.police_name) {
                $('#policy_cust_name').val(data.police_name);
            }else{
                $('#policy_cust_name').val(data.full_name);
            }
            
            // select 2
            $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
            $('#customer').val(data.customer_id).trigger('change.select2');
            $('#product').val(data.product_id).trigger('change.select2');
            $('#officer').val(data.agent_id).trigger('change.select2');
            $('#valuta_id').val(data.valuta_id).trigger('change.select2');
            $('#quotation').val(data.qs_id).trigger('change.select2');
            $('#proposal').val(data.cn_id).trigger('change.select2');
            

            //date
            $('#start_date_police').val(znFormatDateNum(data.start_date_polis));
            $('#end_date_police').val(znFormatDateNum(data.end_date_polis));
            $('#no_polis').val(data.polis_no);
            $('#today_kurs').val(formatMoney(data.kurs_today));
            $('#valas_amount').val(formatMoney(data.valuta_amount));
            $('#premi').val(formatMoney(data.premi_amount));
            $('#disc_amount').val(formatMoney(data.disc_amount));
            $('#fee_internal').val(formatMoney(data.comp_fee_amount));
            $('#fee_polis').val(formatMoney(data.polis_amount));
            $('#nett_amount').val(formatMoney(data.net_amount));
            $('#fee_agent').val(formatMoney(data.agent_fee_amount));
            $('#fee_materai').val(formatMoney(data.materai_amount));
            $('#asuransi').val(formatMoney(data.ins_fee));
            $('#tax_amount').val(formatMoney(data.tax_amount));
            $('#fee_admin').val(formatMoney(data.admin_amount));
            $('#insurance').val(formatMoney(data.ins_amount));
            $('#ef_pct').val(formatMoney(data.ef_pct));
            $('#ef').val(formatMoney(data.ef));

            // SET PPH
            $('#pph').val(formatMoney(data.disc_amount*0.02));

            feeAgentChange();

            var text =  $( "#valuta_id option:selected" ).text();
            $('.zn-rp-group').html(text);


            // convertToRupiah1(data.ins_amount);
            endLoadingPage();
            $('#dataShow').fadeIn();

        }
    });
}

function pilihCOC(coc_id) {
    $('#coc_no').val(coc_id).trigger('change.select2');
    $('#modal-search-policy').modal('hide');
}

function searchPolicy() {

    $('#title_modal_seach').html('List Data Master Policy');

    var act_url = base_url + 'marketing/data/search_master_sales_original_table/all';

    var tableSearchPolicy = $('#search_policy_table').DataTable({
        aaSorting: [],
        processing: true,
        serverSide: true,
        responsive: true,
        destroy:true,
        columnDefs: [
            { "orderable": false, "targets": 0 }],
        ajax: {
            "url" : act_url,
            "error": function(jqXHR, textStatus, errorThrown)
                {
                    toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                }
            },
        columns: [
            { data: 'cf_no', name: 'cf_no' },    
            { data: 'full_name', name: 'full_name' },
            { data: 'underwriter', name: 'underwriter' },
            { data: 'no_of_insured', name: 'no_of_insured' },
            { data: 'produk', name: 'produk' },
            { data: 'agent', name: 'agent' },
            { data: 'mata_uang', name: 'mata_uang' },
            { data: 'start_date_polis', name: 'start_date_polis' },
            { data: 'end_date_polis', name: 'end_date_polis' },
            { data: 'ins_amount', name: 'ins_amount' },
            // { data: 'gross_premium', name: 'gross_premium' },
            // { data: 'nett_premium', name: 'nett_premium' },
            // { data: 'cn_no', name: 'cn_no' },

            { data: 'action', name: 'action' },   
        ]
    });

$('#modal-search-policy').modal('show');
}


$(document).ready(function () {
    $("#form-new-invoice").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            id_underwriter: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            no_polis: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            customer: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            product: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            segment: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            officer: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            quotation: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            bank: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            tgl_mulai: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            valuta_id: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            proposal: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            is_tax_company: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            start_date_police: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },

            tgl_akhir: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            no_of_insured: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            end_date_police: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            }
            // ,
            // ef_pct: {
            //     validators: {
            //         notEmpty: {
            //             message: 'Silahkan isi'
            //         },
            //     }
            // }

        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

function setEF() {
    let premi = zclearNumformat($('#premi').val()); //Gross Premium Amount
    let ef_pct = zclearNumformat($('#ef_pct').val()); 

    let ef = (ef_pct/100)*premi;

    console.log(ef);

    $('#ef').val(formatMoney(ef));
}

function get_agent(elm) {
    var value = $(elm).val();
    if ( value ) {
        $.ajax({
            type: 'GET',
            url: base_url + '/get_agent_by_cust_id/' + value,
            beforeSend: function () {
                KTApp.block('#container-agent', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'danger',
                    message: 'Processing...'
                });
            },
            success: function (msg) {
                console.log(msg);
                var data = msg.rm;
                $('#officer').html(``);

                if ( data != null) {
                    $('#officer').append(`<option value="`+data.id+`" selected>`+data.full_name+`</option>`);
                } else {
                    $('#officer').append(`<option value="" disabled selected>Agen tidak ditemukan</option>`);
                }

            }
        }).done(function (msg) {
            KTApp.unblock('#container-agent');
        }).fail(function (msg) {
            KTApp.unblock('#container-agent');
            toastr.info("Gagal, Koneksi ke server bermasalah");
        });
    } // end if
}

function modal_add_customer() {

    $('#cust_type').val(null).trigger('change.select2');
    $('#agent').val(null).trigger('change.select2');
    $('#cust_group').val(null).trigger('change.select2');

    $("#add_customer")[0].reset();
    $('#add_customer').bootstrapValidator("resetForm", true);

    $('#modal_add_customer').modal('show');
}

function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}


var set_type = '{{$type}}';

@if ($type == 'Edit')
if (set_type == 'Edit') {

 
    $(document).ready(function(){
        $('#coc_no').append(`<option value={{$data->coc_id}}>{{$data->cf_no}}</option>`);                
        // $('#coc_no').select2('destroy');
        $('#coc_no').val('{{$data->coc_id}}').select2();
      });
      
    $('#get_id').val('{{$data->id}}');
    $('#id_underwriter').val('{{$data->underwriter_id}}');
    $('#no_polis').val('{{$data->polis_no}}');
    $('#customer').val('{{$data->customer_id}}');
    $('#product').val('{{$data->product_id}}');
    $('#segment').val('{{$data->segment_id}}');
    $('#officer').val('{{$data->agent_id}}');
    $('#quotation').val('{{$data->qs_no}}');
    $('#bank').val('{{$data->afi_acc_no}}');
    $('#tgl_mulai1').val('{{date("d F Y",strtotime($data->start_date))}}');
    $('#valuta_id').val('{{$data->valuta_id}}');
    $('#proposal').val('{{$data->proposal_no}}');
    $('#is_tax_company').val('{{$is_tax_c}}');
    $('#start_date_police').val('{{date("d F Y",strtotime($data->start_date_polis))}}');
    $('#today_kurs').val(formatMoney('{{$data->kurs_today}}'));
    $('#tgl_akhir2').val('{{$difference->days}}');
    $('#no_of_insured').val('{{$data->no_of_insured}}');
    $('#end_date_police').val('{{date("d F Y",strtotime($data->end_date_polis))}}');
    $('#valas_amount').val(formatMoney('{{$data->valuta_amount/$data->kurs_today}}'));
    $('#premi').val(formatMoney('{{$data->premi_amount/$data->kurs_today}}'));
    $('#disc_amount').val(formatMoney('{{$data->disc_amount/$data->kurs_today}}'));
    $('#fee_internal').val(formatMoney('{{$data->comp_fee_amount/$data->kurs_today}}'));
    $('#fee_polis').val(formatMoney('{{$data->polis_amount/$data->kurs_today}}'));
    $('#nett_amount').val(formatMoney('{{$data->net_amount/$data->kurs_today}}'));
    $('#fee_agent').val(formatMoney('{{$data->agent_fee_amount/$data->kurs_today}}'));
    $('#fee_materai').val(formatMoney('{{$data->materai_amount/$data->kurs_today}}'));
    $('#asuransi').val(formatMoney('{{$data->ins_fee/$data->kurs_today}}'));
    $('#tax_amount').val(formatMoney('{{$data->tax_amount/$data->kurs_today}}'));
    $('#fee_admin').val(formatMoney('{{$data->admin_amount/$data->kurs_today}}'));
    $('#insurance').val(formatMoney('{{$data->ins_amount/$data->kurs_today}}'));
    $('#coc_no').val('{{$data->coc_id}}');
    $('#policy_cust_name').val('{{$data->police_name}}');
    $('#ef_pct').val(formatMoney('{{$data->ef_pct}}'));
    $('#ef').val(formatMoney('{{$data->ef}}'));
    $('#pph').val(formatMoney('{{$data->pph}}'));

    if ($('#valuta_id').val() == 1) {
        $("#today_kurs").prop('disabled', true);
        $("#today_kurs").val('');
    }


    var text =  $( "#valuta_id option:selected" ).text();
    $('.zn-rp-group').html(text);

}
@endif



    $("#valas_amount").keyup(function (elm) {
        var today_kurs = $("#today_kurs").val();

        if ( today_kurs != "" ) {
            today_kurs = today_kurs.replace(/\./g,'');
            today_kurs = today_kurs.replace(/\,/g,'.');

            var valas_amount = $(this).val();
            valas_amount = valas_amount.replace(/\./g,'');
            valas_amount = valas_amount.replace(/\,/g,'.');

            var hasil = today_kurs * valas_amount;
            hasil = hasil.toFixed(2).replace(/\./g, ",");



            $('#insurance').val(valas_amount).trigger('mask.maskMoney');
            convertToRupiah1($("#insurance"));

            $("#total_sum_insured").val(hasil);

        }

    });

    $("#today_kurs").keyup(function (elm) {
        var valas_amount = $("#valas_amount").val();
        var today_kurs = $(this).val();

        if ( valas_amount != "" ) {
            today_kurs = today_kurs.replace(/\./g,'');
            today_kurs = today_kurs.replace(/\,/g,'.');

            valas_amount = valas_amount.replace(/\./g,'');
            valas_amount = valas_amount.replace(/\,/g,'.');

            var hasil = today_kurs * valas_amount;
            hasil = hasil.toFixed(2).replace(/\./g, ",");

            console.log('today kurs : ' + today_kurs);
            console.log('valas amount : ' + valas_amount);
            console.log('hasil : ' + hasil);

            $('#insurance').val(hasil).trigger('mask.maskMoney');
            convertToRupiah1($("#insurance"));

            $("#total_sum_insured").val(hasil);

        }

    });

    $("#valuta_id").on('change', function(elm) {
        var value = $(this).val();
        var text =  $( "#valuta_id option:selected" ).text();



        $('.zn-rp-group').html(text);

        if ( value == "1" ) {
            // IDR SELECTED
            $("#today_kurs").prop('disabled', true);
            $("#valas_amount").prop('disabled', true);
            // $("#insurance").prop('disabled', false);

            $("#today_kurs").val('');
            $("#valas_amount").val('');
            $("#insurance").val('');

            $('#premi').val('');
            $('#disc_amount').val('');
            $('#nett_amount').val('');
            $('#fee_agent').val('');
            $('#fee_internal').val('');
            $('#asuransi').val('');
            $('#tax_amount').val('');
        } else {
            $("#today_kurs").prop('disabled', false);
            $("#valas_amount").prop('disabled', false);
            // $("#insurance").prop('disabled', true);
        }

    });
$(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    //minDate: new Date(1999, 10 - 1, 25)
    /*
    $(document).ready(function(){
    $('#tgl_mulai').datepicker({ onClick: function(dateText, inst) { alert("Working"); } });
    });
    */
    var date = new Date();

    $('#tgl_mulai1').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        startDate: date
     });

      // Tambahan
     $('#start_date_police').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     });

    $(document).ready(function(){
        $('#start_date_police').change(function () {
            $('#end_date_police').val('');
           $('#end_date_police').datepicker('destroy');
            console.log(this.value);
            $('#end_date_police').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                startDate: this.value
            });
            $('#form-new-invoice').bootstrapValidator('revalidateField', 'start_date_police');
        });

        $('#end_date_police').change(function () {
            $('#form-new-invoice').bootstrapValidator('revalidateField', 'end_date_police');
            });

    });


     /*
     $('#end_date_police').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     });
     */

    console.log(date);

/*
    startDate: lastDay,
        endDate: lastDay,
        beforeShowDay: function(date){
            if(date==lastDay){
                return true;
            }else{
                return false;
            }

        }
        */
    $('#tgl_tr').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,


    });
    $(document).ready(function(){
        $('#tgl_mulai1').change(function () {
            $('#tgl_akhir1').val('');
           $('#tgl_akhir1').datepicker('destroy');
            console.log(this.value);
            $('#tgl_akhir1').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                startDate: this.value
            });

            $('#form-new-invoice').bootstrapValidator('revalidateField', 'tgl_mulai');

        });
    });

    /*
    $('#tgl_mulai').datepicker({
                format: 'dd/mm/yyyy',
                onSelect: function(dateText) {
                    console.log("Selected date: " + dateText + "; input's current value: " + this.value);
                }
            });
    */

    $('#invoice').keyup(function() {
        var panjang = this.value.length;

        if(panjang==80){
            $('#invoice').addClass( "is-invalid" );
            $('#sn').hide();
            $('#mk').show();
        }else{
            $('#invoice').removeClass( "is-invalid" );
            $('#sn').hide();
            $('#mk').hide();
        }
    });

    // var nilaiAsuransi;
    // $("#fee_polis").keyup( function() {
    //     var police_duty = parseFloat($(this).val().replace('.', ''));
    //     var stamp_duty = parseFloat($("#fee_materai").val().replace('.', ''));
    //     var hasil = police_duty + stamp_duty + nilaiAsuransi;

    //     console.log('Nilai asuransi : ' + nilaiAsuransi);
    //     console.log('police duty : ' + police_duty);
    //     console.log('stamp duty : ' + stamp_duty);


    //     var sum = police_duty + $asuransi;

    //     $("#asuransi").val(hasil);
    //     $('#asuransi').val(hasil).trigger('mask.maskMoney');
    //     console.log(hasil);

    // });

    // $("#fee_materai").keyup( function() {
    //     var stamp_duty = parseFloat($(this).val().replace('.', ''));
    //     var police_duty = parseFloat($("#fee_polis").val().replace('.', ''));
    //     var hasil = police_duty + stamp_duty + nilaiAsuransi;

    //     console.log('Nilai asuransi : ' + nilaiAsuransi);
    //     console.log('police duty : ' + police_duty);
    //     console.log('stamp duty : ' + stamp_duty);


    //     var sum = police_duty + $asuransi;

    //     $("#asuransi").val(hasil);
    //     $('#asuransi').val(hasil).trigger('mask.maskMoney');
    //     console.log(hasil);

    // });



    function convertToRupiah2 (objek) {

        var premi=$('#premi');
        var insurance=$('#insurance');
        var disc_amount=$('#disc_amount');

        if(premi.val()===''){
            premi.addClass( "is-invalid" );
            document.getElementById("disc_amount").value="";
        }else{

            var aa1=insurance.val();
            var aa2=aa1.replace(/\./g,'');

            var bb1=premi.val();
            var bb2=bb1.replace(/\./g,'');
            var smd1=Math.floor(b2) > Math.floor(a2);

            if(smd){
               premi.addClass( "is-invalid" );
               $('#pa').hide();
               $('#pa1').show();
            }else{
                premi.removeClass( "is-invalid" )
            }



            var a1=premi.val();
            var a2=a1.replace(/\./g,'');
            var a3=a2.replace(/\,/g,'.');

            var b1=disc_amount.val();
            var b2=b1.replace(/\./g,'');
            var b3=b2.replace(/\,/g,'.');

            var smd=Math.floor(b3) > Math.floor(a3);

            if(smd){
               disc_amount.addClass( "is-invalid" );
               $('#sd').hide();
               $('#mi').show();
              // disc_amount.val(insurance.val());
            }else{

               var c2= a3 - b3;

               $nett=c2.toFixed(2).replace(/\./g, ",");
                $('#nett_amount').val($nett).trigger('mask.maskMoney');

                //$('#nett_amount').val(c2);



            $.ajax({
                type: 'GET',
                url: base_url + '/fee1?jml='+a3+'&company='+$('#is_tax_company').val(),
                success: function (res) {
                    var data = $.parseJSON(res);
                    console.log(data);
                        //console.log(data.fee_agent);

                        $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                        $('#fee_agent').val($agent).trigger('mask.maskMoney');


                        $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                        //$('#fee_internal').val($internal).trigger('mask.maskMoney');

                        // if($('#fee_internal').val()===''){
                            $('#fee_internal').val($internal).trigger('mask.maskMoney');
                        // }


                        var aa1=$('#nett_amount').val();
                        var aa2=aa1.replace(/\./g,'');
                        var aa3=aa2.replace(/\,/g,'.');

                        var aa11=$('#fee_internal').val();
                        var aa21=aa11.replace(/\./g,'');
                        var aa31=aa21.replace(/\,/g,'.');

                        console.log(aa3);
                        console.log(data.fee_agent);
                        console.log(aa31);


                        var asuransi=aa3 - data.fee_agent - aa31;

                        console.log('asuransi');
                        // console.log(aa3);
                        // console.log(aa3 - data.fee_agent - data.ko);
                        // console.log(data.fee_agent);
                        // console.log(data.komisi_perusahaan);
                        console.log(asuransi);

                        // $asuransi=asuransi.toFixed(2).replace(/\./g, ",");
                        // $('#asuransi').val($asuransi).trigger('mask.maskMoney');

                        $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");
                        $('#tax_amount').val($amount).trigger('mask.maskMoney');

                }
            });

               disc_amount.removeClass( "is-invalid" );
            }
            premi.removeClass( "is-invalid" );
        }

    }

    function zclearNumformat(num) {
        var clearNum = num;
        clearNum = clearNum.replace(/\./g,'');
        clearNum = clearNum.replace(/\,/g,'.');
        return parseFloat(clearNum);
    }

    function feeAgentChange() {
       
        var fee_agent = zclearNumformat($('#fee_agent').val()); //Commission Due to Agent
        var fee_internal = zclearNumformat($('#fee_internal').val()); //Commission Due to HD Soeryo
        var fee_polis = zclearNumformat($('#fee_polis').val()); //Policy Duty
        var fee_materai = zclearNumformat($('#fee_materai').val()); //stamp duty
        var fee_admin = zclearNumformat($('#fee_admin').val());

        var premi = zclearNumformat($('#premi').val()); //Gross Premium Amount
        var disc_amount = zclearNumformat($('#disc_amount').val()); //Discount Amount
        var ef = zclearNumformat($('#ef').val()); //EF Amount
        var pph = zclearNumformat($('#pph').val()); //EF Amount

        if ($('#fee_agent').val() == '') { fee_agent = 0;}
        if ($('#fee_internal').val() == '') {fee_internal = 0;}
        if ($('#fee_polis').val() == '') {fee_polis = 0;}
        if ($('#fee_materai').val() == '') {fee_materai = 0;}
        if ($('#fee_admin').val() == '') {fee_admin = 0;}
        if ($('#premi').val() == '') {premi = 0;}
        if ($('#disc_amount').val() == '') {disc_amount = 0;}
        if ($('#ef').val() == '') {ef = 0;}
        if ($('#pph').val() == '') {pph = 0;}

        var set_nett_premium= premi - disc_amount - ef + pph + (fee_polis+fee_materai+fee_admin);
        $set_nett_premium=set_nett_premium.toFixed(2).replace(/\./g, ",");
        $('#nett_amount').val($set_nett_premium).trigger('mask.maskMoney');

        var nett_amount = zclearNumformat($('#nett_amount').val()); //net premi
        if ($('#nett_amount').val() == '') { nett_amount = 0;}

        var asuransi=nett_amount +(fee_polis+fee_materai);
        // var asuransi=nett_amount - fee_agent - fee_internal +fee_polis +fee_materai+fee_admin; 
        $asuransi=asuransi.toFixed(2).replace(/\./g, ",");
      
        console.log(set_nett_premium);
        console.log($set_nett_premium);

        $('#asuransi').val($asuransi).trigger('mask.maskMoney');
       

     
    }

    function convertToRupiah3 (objek) {

        var premi=$('#premi');
        var insurance=$('#insurance');
        var disc_amount=$('#disc_amount');

        if(premi.val()===''){
            premi.addClass( "is-invalid" );
            document.getElementById("disc_amount").value="";
        }else{

            var aa1=insurance.val();
            var aa2=aa1.replace(/\./g,'');

            var bb1=premi.val();
            var bb2=bb1.replace(/\./g,'');
            var smd1=Math.floor(b2) > Math.floor(a2);

            if(smd){
               premi.addClass( "is-invalid" );
               $('#pa').hide();
               $('#pa1').show();
            }else{
                premi.removeClass( "is-invalid" )
            }



            var a1=premi.val();
            var a2=a1.replace(/\./g,'');
            var a3=a2.replace(/\,/g,'.');

            var b1=disc_amount.val();
            var b2=b1.replace(/\./g,'');
            var b3=b2.replace(/\,/g,'.');

            var smd=Math.floor(b3) > Math.floor(a3);

            if(smd){
               disc_amount.addClass( "is-invalid" );
               $('#sd').hide();
               $('#mi').show();
              // disc_amount.val(insurance.val());
            }else{

               var c2= a3 - b3;

               $nett=c2.toFixed(2).replace(/\./g, ",");
                $('#nett_amount').val($nett).trigger('mask.maskMoney');

                //$('#nett_amount').val(c2);



            $.ajax({
                type: 'GET',
                url: base_url + '/fee1?jml='+a3+'&company='+$('#is_tax_company').val(),
                success: function (res) {
                    var data = $.parseJSON(res);
                        //console.log(data.fee_agent);

                        $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                        $('#fee_agent').val($agent).trigger('mask.maskMoney');

                        $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                        //$('#fee_internal').val($internal).trigger('mask.maskMoney');

                        // if($('#fee_internal').val()===''){
                            $('#fee_internal').val($internal).trigger('mask.maskMoney');
                        // }

                        //$internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                        //$('#fee_internal').val($internal).trigger('mask.maskMoney');



                        var aa1=$('#nett_amount').val();
                        var aa2=aa1.replace(/\./g,'');
                        var aa3=aa2.replace(/\,/g,'.');

                        var aa11=$('#fee_internal').val();
                        var aa21=aa11.replace(/\./g,'');
                        var aa31=aa21.replace(/\,/g,'.');


                        console.log(aa3);
                        console.log(data.fee_agent);
                        console.log(aa31);

                        var asuransi=aa3 - data.fee_agent - aa31;

                        console.log('asuransi');
                        console.log(asuransi);

                        // console.log(aa3);
                        // console.log(aa3 - data.fee_agent - data.ko);
                        // console.log(data.fee_agent);
                        // console.log(data.komisi_perusahaan);
                        // console.log(asuransi);

                        $asuransi=asuransi.toFixed(2).replace(/\./g, ",");
                        $('#asuransi').val($asuransi).trigger('mask.maskMoney');

                        $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");
                        $('#tax_amount').val($amount).trigger('mask.maskMoney');

                }
            });

               disc_amount.removeClass( "is-invalid" );
            }
            premi.removeClass( "is-invalid" );
        }

    }

    function convertToRupiah1 (objek) {

        var insurance=$('#insurance');
        /*
        var disc_amount=$('#disc_amount');

        var polis=$('#fee_polis');
        var admin=$('#fee_admin');
        var materai=$('#fee_materai');


    */

        var police_duty = parseFloat($("#fee_polis").val().replace('.', ''));
        var stamp_duty = parseFloat($("#fee_materai").val().replace('.', ''));

        if(insurance.val()===''){
            insurance.addClass( "is-invalid" );
        //    document.getElementById("disc_amount").value="";
        }else{

            var a1=insurance.val();
            var a2=a1.replace(/\./g,'');


            $.ajax({
                type: 'GET',
                url: base_url + '/fee?jml='+a2+'&company='+$('#is_tax_company').val(),
                success: function (res) {
                    var data = $.parseJSON(res);
                    console.log(data);
                        //console.log(data.fee_agent);

                        //.trigger('mask.maskMoney')
                        $premi=data.premi_amount.toFixed(2).replace(/\./g, ",");
                        $('#premi').val($premi).trigger('mask.maskMoney');
                        //$('#premi').maskMoney("#,##0.00", {reverse: true});
                        //$("#premi").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

                        $discount=data.discount.toFixed(2).replace(/\./g, ",");
                        $('#disc_amount').val($discount).trigger('mask.maskMoney');


                        $nett=data.nett_amount.toFixed(2).replace(/\./g, ",");
                        $('#nett_amount').val($nett).trigger('mask.maskMoney');


                        $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                        $('#fee_agent').val($agent).trigger('mask.maskMoney');



                        $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                        //$('#fee_internal').val($internal).trigger('mask.maskMoney');
                        // if($('#fee_internal').val()===''){

                            $('#fee_internal').val($internal).trigger('mask.maskMoney');
                        // }


                        // nilaiAsuransi = data.asuransi;
                        // $asuransi = data.asuransi + police_duty + stamp_duty;
                        // $asuransi = $asuransi.toFixed(2).replace(/\./g, ",");

                        // $asuransi=data.asuransi.toFixed(2).replace(/\./g, ",");
                        // $('#asuransi').val($asuransi).trigger('mask.maskMoney');

                        feeAgentChange();

                        $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");
                        $('#tax_amount').val($amount).trigger('mask.maskMoney');


                }
            });
            insurance.removeClass( "is-invalid" );
        }

    }

var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  <button onclick="znClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                    Data</button>
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;
function znClose() {
    znIconboxClose();
    //location.reload();
    $("#form-new-invoice")[0].reset();
    document.getElementById("form-new-invoice").reset();
    $('#form-new-invoice').bootstrapValidator("resetForm", true);
    location.reload();

    //$("#form-new-invoice").data('bootstrapValidator').resetForm();

}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('invoice_list') }}');
}


var _create_form = $("#form-new-invoice");
function simpan_new_invoice(){

    var validateDebit = $('#form-new-invoice').data('bootstrapValidator').validate();
        if (validateDebit.isValid()) {

            loadingPage();

    if($('#no_polis').val()===''){
        endLoadingPage();
        $( "#no_polis" ).addClass( "is-invalid" );
        $('#sn').show();
        $('#mk').hide();
        return false;
    }else{
        $("#no_polis").removeClass( "is-invalid" );

    }

    if($('#product').val()===''){
        endLoadingPage();
        $( "#product" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#product").removeClass( "is-invalid" );

    }

    if($('#officer').val()===''){
        endLoadingPage();
        $( "#officer" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#officer").removeClass( "is-invalid" );

    }
    if($('#quotation').val()===''){
        endLoadingPage();
        $( "#quotation" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#quotation").removeClass( "is-invalid" );

    }
    if($('#proposal').val()===''){
        endLoadingPage();
        $( "#proposal" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#proposal").removeClass( "is-invalid" );

    }
    if($('#segment').val()===''){
        endLoadingPage();
        $( "#segment" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#segment").removeClass( "is-invalid" );

    }
    if($('#bank').val()===''){
        endLoadingPage();
        $( "#bank" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#bank").removeClass( "is-invalid" );

    }
    if($('#tgl_mulai').val()===''){
        endLoadingPage();
        $( "#tgl_mulai" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_mulai").removeClass( "is-invalid" );

    }
    if($('#tgl_akhir').val()===''){
        endLoadingPage();
        $( "#tgl_akhir" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_akhir").removeClass( "is-invalid" );

    }


    if($('#fee_polis').val()===''){
        // endLoadingPage();
        // $( "#fee_polis" ).addClass( "is-invalid" );
        // return false;
        $("#fee_polis").val("0,00");
    }else{
        $("#fee_polis").removeClass( "is-invalid" );
    }

    if ( $("#premi").val() === '' ) {
        $("#premi").val("0,00");
    }

    if ( $("#fee_internal").val() === '' ) {
        $("#fee_internal").val("0,00");
    }

    if ( $("#fee_agent").val() === '' ) {
        $("#fee_agent").val("0,00");
    }

    if ( $("#tax_amount").val() === '' ) {
        $("#tax_amount").val("0,00");
    }

    if($('#fee_materai').val()===''){
        // endLoadingPage();
        // $( "#fee_materai" ).addClass( "is-invalid" );
        // return false;

        $("#fee_materai").val("0,00");
    }else{
        $("#fee_materai").removeClass( "is-invalid" );

    }

    if($('#fee_admin').val()===''){
        // endLoadingPage();
        // $( "#fee_admin" ).addClass( "is-invalid" );
        // return false;

        $("#fee_admin").val("0,00");
    }else{
        $("#fee_admin").removeClass( "is-invalid" );

    }



    if($('#insurance').val()===''){
        endLoadingPage();
        $( "#insurance" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#insurance").removeClass( "is-invalid" );

    }
    if($('#id_underwriter').val()===''){
        endLoadingPage();
        $( "#id_underwriter" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#id_underwriter").removeClass( "is-invalid" );

    }

    if($('#disc_amount').val()===''){
        // endLoadingPage();
        // $( "#disc_amount" ).addClass( "is-invalid" );
        // $('#sd').show();
        // $('#mi').hide();
        // return false;
        $("#disc_amount").val("0,00");
    }else{
        $("#disc_amount").removeClass( "is-invalid" );
    }

    if($('#valuta_id').val()===''){
        endLoadingPage();
        $("#valuta_id" ).addClass( "is-invalid" );
        return false;
    }else{

        $("#valuta_id").removeClass( "is-invalid" );
    }

     var a1=$('#insurance').val();
     var a2=a1.replace(/\./g,'');

     var b1=$('#disc_amount').val();
     var b2=b1.replace(/\./g,'');
     var smd=Math.floor(b2) > Math.floor(a2);

     if(smd){
        endLoadingPage();
        $( "#disc_amount" ).addClass( "is-invalid" );
        $('#sd').hide();
        $('#mi').show();
        return false;
    }else{
        $("#disc_amount").removeClass( "is-invalid" );
    }

    if($('#file').val()===''){
    }else{
        var size=$('#file')[0].files[0].size;
        console.log(size);
        var extension=$('#file').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            endLoadingPage();
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').hide();
            $('#mm').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
            endLoadingPage();
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').show();
            $('#mm').hide();
            return false;
        }

        $( "#file" ).removeClass( "is-invalid" );

    }


    $.ajax({
            type: "GET",
            url: base_url + '/cekPolis?id='+$("#no_polis").val(),
            beforeSend: function () {
                loadingPage();
            },

            success: function (result) {
                var data = $.parseJSON(result);
                endLoadingPage();
                console.log(data.length);
                if(data.length!=0 && set_type == 'New'){

                    swal.fire("Info","Polis Sudah ada","info");
                    return false;
                }else{

                    var _form_data = new FormData(_create_form[0]);
                    _form_data.append('ef_pct', $('#ef_pct').val());
                    _form_data.append('ef', $('#ef').val());
                    _form_data.append('coc_no', $('#coc_no').val());
                    _form_data.append('pph', $('#pph').val());

                    
                    $.ajax({
                        type: 'POST',
                        url: base_url + '/insert_invoice_baru',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success: function (res) {

                            //var obj = JSON.parse(res);
                            var obj = res;
                            console.log(res['data']);
                            endLoadingPage();

                            if(obj.rc==1){
                                znIconbox("Data Berhasil Disimpan",text,action);
                            }else{
                                swal.fire("Info",obj.rm,"info");
                            }
                        }
                    }).done(function( res ) {
                        var obj = res;
                        //var obj = JSON.parse(res);
                        console.log(res['data']);
                        endLoadingPage();

                        if(obj.rc==1){
                            znIconbox("Data Berhasil Disimpan",text,action);
                        }else{
                            swal.fire("Info",obj.rm,"info");
                        }

                       }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
                }
            }
        }).done(function (msg) {
            //  endLoadingPage();

        }).fail(function (msg) {
             endLoadingPage();
            swal.fire("Info","terjadi kesalahan","info");
            return false;
        });


        }



}


function convertToRupiah_polis(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c;


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');

    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');

    var c1=polis.val();
    var c2=c1.replace(/\./g,'');

    var d1=admin.val();
    var d2=d1.replace(/\./g,'');

    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);


        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);


                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);


                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });


    }


 }

}

function convertToRupiah_admin(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c;


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');

    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');

    var c1=polis.val();
    var c2=c1.replace(/\./g,'');

    var d1=admin.val();
    var d2=d1.replace(/\./g,'');

    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);

        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);


                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);


                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });


    }


 }

}


function convertToRupiah_materai(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c;


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');

    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');

    var c1=polis.val();
    var c2=c1.replace(/\./g,'');

    var d1=admin.val();
    var d2=d1.replace(/\./g,'');

    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);

        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);


                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);


                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });


    }


 }

}

</script>
@stop
