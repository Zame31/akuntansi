@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Sales </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Dropped Debit Note </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Dropped Debit Note
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">

                    </div>
            </div>
            <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>

                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Branch Code</th>
                                    <th>Customer Name</th>
                                    <th>Product Name</th>
                                    <th>Valuta</th>
                                    <th>Premi Amount</th>
                                    <th>Paid Status</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if($data)
                                @foreach($data as $item)
                                <tr>
                                    <td>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#" onclick="detail_invoice({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Detail</a>
                                                    <a class="dropdown-item" href="#" onclick="notes({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Memo</a> 
                                                </div>
                                        </div>
                                    </td>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->short_code}}</td>
                                    <td>{{$item->full_name}}</td>
                                    <td>{{$item->definition}}</td>
                                    <td>{{$item->mata_uang}}</td>
                                    @if($item->valuta_id==1)
                                    <td>{{number_format($item->premi_amount,2,',','.')}}</td>
                                    @else
                                    <td>{{number_format($item->premi_amount/$item->kurs_today,2,',','.')}}</td>
                                    @endif
                                    <td>{{$item->paid}}</td>

                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                        </table>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
<script type="text/javascript">

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}
</script>

@include('invoice.action')
@include('transaksi.invoice_modal')
@stop
