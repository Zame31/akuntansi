@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Sales </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Active Debit Note </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Active Debit Note
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        @if (Auth::user()->user_role_id == 1)
                        <div class="col-12">
                            {{-- <a href="#" onclick="kirimsemua_invoice();" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a> --}}
                            {{-- <a href="#" onclick="kirim_invoice();" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a> --}}
                            {{-- <a href="#" onclick="modal_payment('all');" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a> --}}

                            <a href="#" onclick="modal_payment('selected');" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a>
                            <a href="#" onclick="hapussemua_invoice();" class="btn btn-pill btn-danger" style="color: white;">Drop All</a>
                            <a href="#" onclick="hapus_invoice();" class="btn btn-pill btn-danger" style="color: white;">Drop Selected</a>
                        </div>
                        @endif

                    </div>
                    </div>
            </div>
            <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" id="example-select-all" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Branch Code</th>
                                    <th>Customer Name</th>
                                    <th>Product Name</th>
                                    <th>Valuta</th>
                                    <th>Premi Amount (IDR)</th>
                                    <th>Full Payment (IDR)</th>
                                    <th>Paid Status</th>
                                    <th>Police Number</th>
                                    <th>Dokumen</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total_premi = 0;
                                    $total_full = 0;
                                @endphp
                                @if($data)
                                @foreach($data as $item)
                                @php
                                    $total_premi += $item->premi_amount;
                                    $total_full += ($item->premi_amount+$item->polis_amount+$item->materai_amount+$item->agent_fee_amount+$item->comp_fee_amount+$item->admin_amount);
                                @endphp
                                <tr>
                                    <td>
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" name="ck" value="{{$item->id}}" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">

                                                <a class="dropdown-item" href="#" onclick="detail_invoice({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Detail</a>
                                                <a class="dropdown-item" href="#" onclick="detail_invoice_product(`{{$item->coc_product_id}}`,`{{$item->coc_id}}`,`{{$item->invoice_type_id}}`)"><i
                                                    class="la la-clipboard"></i> Detail Product</a>
                                                <a class="dropdown-item" href="#" onclick="claim('{{$item->polis_no}}')"><i
                                                    class="la la-clipboard"></i> Claim</a>
                                                 <a class="dropdown-item" href="#" onclick="notes({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Memo</a>

                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->short_code}}</td>
                                    <td>{{$item->full_name}}</td>
                                    <td>{{$item->definition}}</td>
                                    <td>{{$item->mata_uang}}</td>
                                    @if($item->valuta_id==1)
                                    <td style="text-align: right;">{{number_format($item->premi_amount,2,',','.')}}</td>
                                    @else
                                    <td style="text-align: right;">{{number_format($item->premi_amount,2,',','.')}}</td>
                                    @endif
                                    <td style="text-align: right;">{{number_format(($item->premi_amount+$item->polis_amount+$item->materai_amount+$item->agent_fee_amount+$item->comp_fee_amount+$item->admin_amount),2,',','.')}}</td>

                                    <td>{{$item->paid}}</td>
                                    <td>{{$item->polis_no}}</td>
                                    <td>
                                        @if($item->url_dokumen)
                                        <a target="_blank" href="{{asset('upload_invoice')}}/{{$item->id}}_{{$item->url_dokumen}}">Download File</a>
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->invoice_type_id==0)
                                        Original
                                        @elseif($item->invoice_type_id==1)
                                        Endorsement
                                        @else
                                        Installment
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th width="30px">
                                    </th>
                                    <th width="30px"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style="text-align: right;">{{number_format($total_premi,2,',','.')}}</th>
                                    <th style="text-align: right;">{{number_format($total_full,2,',','.')}}</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>

@include('invoice.modal_payment')
@include('invoice.modal_notes')

@include('invoice.modal_detail_product')
@include('invoice.act_detail_product')

<script type="text/javascript">
$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"][name="ck"]').prop('checked', this.checked);
});

function setTotal() {
    var d1 = 0;
    var d2 = 0;
    var d3 = 0;

    if($('#set1v').is(":checked")){
        d1 = $('#set1v').val();
    }
    if($('#set2v').is(":checked")){
        d2 = $('#set2v').val();
    }
    if($('#set3v').is(":checked")){
        d3 = $('#set3v').val();
    }

    console.log(d1);


    var tt = parseFloat(d1) + parseFloat(d2) + parseFloat(d3);
    $('#setTotal').html('Rp '+numFormatNew(tt.toFixed(2)));


}

function modal_payment(type) {
    let value = [];

    $('#t_kurs').val('');
    $('#set_type').val(type);

    $("#t_kurs").removeClass( "is-invalid");

    if (type == 'selected') {
        $('input[type="checkbox"][name="ck"]').each(function(){
            if(this.checked){
                value.push(this.value);
            }
        });

        if(value.length == 0){
            swal.fire("Peringatan!", "Pilih Satu Data", "warning");
        }
        // else if(value.length > 1){
        //     swal.fire("Peringatan!", "Hanya Diperbolehkan Satu Data", "warning");
        // }
        else{
            $( "#send_approval" ).click(function() {

                if($('#t_kurs').val()===''){
                    $( "#t_kurs" ).addClass( "is-invalid" );
                    return false;
                }else{
                    $("#t_kurs").removeClass( "is-invalid" );
                }

                kirim_invoice();

            });
            $('#modal_payment').modal('show');
        }

    }else{
        $( "#send_approval" ).click(function() {
            if($('#t_kurs').val()===''){
                $( "#t_kurs" ).addClass( "is-invalid" );
                return false;
            }else{
                $("#t_kurs").removeClass( "is-invalid" );
            }
            kirimsemua_invoice();
        });
        $('#modal_payment').modal('show');
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '{{ route('invoice.get_data_payment') }}',
        data:{
            type : type,
            id : value,
            kurs : ''
        },
        success: function (ress) {

            var data = $.parseJSON(ress);
            $.each(data, function (k,res) {

                // $('#set_mata_uang').html(res.mata_uang);
                console.log(res);

                // RUMUS SPLIT

                var set1 = parseFloat(res.premi_amount) + parseFloat(res.polis_amount) + parseFloat(res.materai_amount);
                // var set2 = parseFloat(res.agent_fee_amount) + parseFloat(res.disc_amount);
                var set2 = parseFloat(res.agent_fee_amount);
                var set3 = parseFloat(res.comp_fee_amount) + parseFloat(res.admin_amount);

                var set1_other = set1/res.kurs_today;
                var set2_other = set2/res.kurs_today;
                var set3_other = set3/res.kurs_today;


                var total = set1 + set2 + set3;
                var total_other = set1_other + set2_other + set3_other;

                $('#set1').html(numFormatNew(set1.toFixed(2)));
                $('#set2').html(numFormatNew(set2.toFixed(2)));
                $('#set3').html(numFormatNew(set3.toFixed(2)));
                $('#setTotal').html(numFormatNew(total.toFixed(2)));

                $('#set1_other').html(numFormatNew(set1_other.toFixed(2)));
                $('#set2_other').html(numFormatNew(set2_other.toFixed(2)));
                $('#set3_other').html(numFormatNew(set3_other.toFixed(2)));

                $('#setTotal').html(numFormatNew(0));

                console.log(formatMoneyact(res.kurs_today));



                if (res.valuta_id == 1) {
                    $('#v_t_kurs').hide();
                    $('#set1_other').hide();
                    $('#set2_other').hide();
                    $('#set3_other').hide();
                    $('#setTotal_other').hide();
                    $('#set_mata_uang').hide();

                    $('#t_kurs').val(1);
                }else{
                    $('#v_t_kurs').show();
                    $('#t_kurs').val('');
                    $('#set1_other').show();
                    $('#set2_other').show();
                    $('#set3_other').show();
                    $('#setTotal_other').show();
                    $('#set_mata_uang').show();
                    $('#t_kurs').val(formatMoneyact(res.kurs_today));
                }
                $('#set_mata_uang').html('Amount '+res.mata_uang);


                $('#set1v').val(set1);
                $('#set2v').val(set2);
                $('#set3v').val(set3);


            });



            endLoadingPage();


        }
    }).done(function( res ) {
            endLoadingPage();

    }).fail(function(res) {
        endLoadingPage();
        swal.fire("Error","Terjadi Kesalahan!","error");
    });




}

function formatMoneyact(amount, decimalCount = 2, decimal = ",", thousands = ".") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function kirimsemua_invoice() {

    var d1 = 'n';
    var d2 = 'n';
    var d3 = 'n';

    if($('#set1v').is(":checked")){
        d1 = 't';
    }
    if($('#set2v').is(":checked")){
        d2 = 't';
    }
    if($('#set3v').is(":checked")){
        d3 = 't';
    }

    var jenis_bayar = $('#jenis_bayar').val();
    var t_kurs = $('#t_kurs').val();

        swal.fire({
           title: "Info",
           text: "Anda yakin akan mengirim approval Update Status Pembayaran?",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
               loadingPage();

            $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAproval_invoice_active?type=semua',
                    data: {
                            d1:d1,
                            d2:d2,
                            d3:d3,
                            jenis_bayar : jenis_bayar,
                            t_kurs:t_kurs
                            },
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);

                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });

                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });

   }

   function hapussemua_invoice() {
        swal.fire({
           title: "Info",
           text: "Anda yakin akan menghapus transaksi?",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                        type: 'POST',
                        url: base_url + '/hapusAproval_invoice_active?type=semua',
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });

            }
        });
   }

function kirim_invoice() {
    let value = [];
    $('input[type="checkbox"][name="ck"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });
    var d1 = 'n';
    var d2 = 'n';
    var d3 = 'n';

    if($('#set1v').is(":checked")){
        d1 = 't';
    }
    if($('#set2v').is(":checked")){
        d2 = 't';
    }
    if($('#set3v').is(":checked")){
        d3 = 't';
    }

    var jenis_bayar = $('#jenis_bayar').val();
    var t_kurs = $('#t_kurs').val();

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan mengirim approval Update Status Pembayaran?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/kirimAproval_invoice_active?type=satu',
                        data: {
                            value: value,
                            d1:d1,
                            d2:d2,
                            d3:d3,
                            jenis_bayar:jenis_bayar,
                            t_kurs:t_kurs
                            },
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
    }
}

function hapus_invoice() {
    let value = [];
    $('input[type="checkbox"][name="ck"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan menghapus transaksi?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                        type: 'POST',
                        url: base_url + '/hapusAproval_invoice_active?type=satu',
                        data: {value: value},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });

            }
        });
    }
}
</script>

@include('invoice.action')
@include('transaksi.invoice_modal')
@stop
