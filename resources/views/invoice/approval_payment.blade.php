@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Sales </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Approval Debit Note Payment </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Approval Debit Note Payment
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <a href="#" onclick="kirimsemua_app_split();" class="btn btn-pill btn-primary" style="color: white;">Approve All</a>
                            <a href="#" onclick="kirim_app_split();" class="btn btn-pill btn-primary" style="color: white;">Approve Selected</a>
                            <a href="#" onclick="hapussemua_app_split();" class="btn btn-pill btn-danger" style="color: white;">Reject All</a>
                            <a href="#" onclick="hapus_app_split();" class="btn btn-pill btn-danger" style="color: white;">Reject Selected</a>
                        </div>
                    </div>
                        {{-- <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </a>
                        </div> --}}
                    </div>
            </div>
            <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" id="example-select-all" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Branch Code</th>
                                    <th>Customer Name</th>
                                    <th>Product Name</th>
                                    <th>Valuta</th>
                                    <th>Premi Amount</th>
                                    <th>Paid Status</th>
                                    <th>Police Number</th>
                                    <th>Dokumen</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if($data)
                            @foreach($data as $item)
                            <tr>
                                <td>
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="{{$item->id}}" class="kt-group-checkable">
                                        <span></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#" onclick="detail_invoice({{$item->id}})"><i
                                                class="la la-clipboard"></i> Detail</a>
                                            </div>
                                    </div>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->short_code}}</td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->definition}}</td>
                                <td>{{$item->mata_uang}}</td>
                                    @if($item->valuta_id==1)
                                    <td>{{number_format($item->premi_amount,2,',','.')}}</td>
                                    @else
                                    <td>{{number_format($item->premi_amount/$item->kurs_today,2,',','.')}}</td>
                                    @endif
                                <td>{{$item->paid}}</td>
                                <td>{{$item->polis_no}}</td>
                                <td>
                                    @if ($item->url_dokumen)
                                    <a target="_blank" href="{{asset('upload_invoice')}}/{{$item->id}}_{{$item->url_dokumen}}">Download File</a></td>
                                    @else
                                    -
                                    @endif
                                    
                                <td>
                                        @if($item->invoice_type_id==0)
                                        Original
                                        @elseif($item->invoice_type_id==1)
                                        Endorsement
                                        @else
                                        Installment
                                        @endif
                                    </td>

                            </tr>
                            @endforeach
                            @endif

                        </tbody>
                        </table>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
<script type="text/javascript">
$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});

function znClose() {
    znIconboxClose();
}

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}
function kirimsemua_app_split() {
        swal.fire({
           title: "Info",
           text: "You sure you will approve the transaction?",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
               var SetText = `fill in the memo or notes below for the Approval response you provide. <br><br> <textarea name="inputMemoApprove" id="inputMemoApprove" class="form-control" rows="4" cols="80"></textarea>`;
                var SetAction = `<button onclick="gproses('semua')" type="button" class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Send</button><button onclick="znClose()" type="button" class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Cancel</button>`;
                znIconbox("Memo / Notes",SetText,SetAction,'warning');

            }
        });

   }

   function gproses(type){

    loadingPage();
    var cct=$('#inputMemoApprove').val();
    if(type=='semua'){
        $.ajax({
        type: 'POST',
        url: base_url + '/kirimAproval_invoice_paid?type='+type,
        data: {cct: cct},
        async: false,
            success: function (res) {

                endLoadingPage();
                console.log(res['data']);

                swal.fire({
                    title: 'Info',
                    text: "Success",
                    type: 'success',
                    confirmButtonText: 'Close',
                    reverseButtons: true
                }).then(function(result){
                    if (result.value) {
                        location.reload();
                    }
                });

                }
            }).done(function( res ) {
               endLoadingPage();

           }).fail(function(res) {
            endLoadingPage();
            swal.fire("Error","Internal Server Error!","error");
        });
    }else{
        let value = [];
        $('input[type="checkbox"]').each(function(){
            if(this.checked){
                value.push(this.value);
            }
        });
        $.ajax({
        type: 'POST',
        url: base_url + '/kirimAproval_invoice_paid?type='+type,
        data: {value:value,cct: cct},
        async: false,
        success: function (res) {

            endLoadingPage();
            console.log(res['data']);

            swal.fire({
                title: 'Info',
                text: "Success",
                type: 'success',
                confirmButtonText: 'Close',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    location.reload();
                }
            });

            }
        }).done(function( res ) {
           endLoadingPage();

       }).fail(function(res) {
        endLoadingPage();
        swal.fire("Error","Internal Server Error!","error");
    });
    }

   }

    function gproses1(type){

    loadingPage();
    var cct=$('#inputMemoApprove').val();
    console.log("masuk");

    if($('#inputMemoApprove').val()===''){
        console.log("masuk1");
        endLoadingPage();
        $( "#inputMemoApprove" ).addClass( "is-invalid" );
         swal.fire("info","Please fill in the notes!","info");
        return false;

    }else{
        $("#inputMemoApprove").removeClass( "is-invalid" );

    }
    if(type=='semua'){
        $.ajax({
        type: 'POST',
        url: base_url + '/hapusAproval_invoice_paid?type='+type,
        data: {cct: cct},
        async: false,
            success: function (res) {

                endLoadingPage();
                console.log(res['data']);

                swal.fire({
                    title: 'Info',
                    text: "Success",
                    type: 'success',
                    confirmButtonText: 'Close',
                    reverseButtons: true
                }).then(function(result){
                    if (result.value) {
                        location.reload();
                    }
                });

                }
            }).done(function( res ) {
               endLoadingPage();

           }).fail(function(res) {
            endLoadingPage();
            swal.fire("Error","Internal Server Error!","error");
        });
    }else{
        let value = [];
        $('input[type="checkbox"]').each(function(){
            if(this.checked){
                value.push(this.value);
            }
        });
        $.ajax({
        type: 'POST',
        url: base_url + '/hapusAproval_invoice_paid?type='+type,
        data: {value:value,cct: cct},
        async: false,
        success: function (res) {

            endLoadingPage();
            console.log(res['data']);

            swal.fire({
                title: 'Info',
                text: "Success",
                type: 'success',
                confirmButtonText: 'Close',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    location.reload();
                }
            });

            }
        }).done(function( res ) {
           endLoadingPage();

       }).fail(function(res) {
        endLoadingPage();
        swal.fire("Error","Internal Server Error!","error");
    });
    }

   }

   function hapussemua_app_split() {
        swal.fire({
           title: "Info",
           text: "You are sure you will reject the transaction?",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                 var SetText = `isi memo atau catatan dibawah ini untuk respon Approval yang anda berikan. <br><br> <textarea name="inputMemoApprove" id="inputMemoApprove" class="form-control" rows="4" cols="80"></textarea>`;
                var SetAction = `<button onclick="gproses1('semua')" type="button" class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Kirim</button><button onclick="znClose()" type="button" class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Batal</button>`;
                znIconbox("Memo / Catatan",SetText,SetAction,'warning');

            }
        });
   }

function kirim_app_split() {
    const value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan approve transaksi?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {

                 var SetText = `isi memo atau catatan dibawah ini untuk respon Approval yang anda berikan. <br><br> <textarea name="inputMemoApprove" id="inputMemoApprove" class="form-control" rows="4" cols="80"></textarea>`;
                var SetAction = `<button onclick="gproses('satu')" type="button" class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Kirim</button><button onclick="znClose()" type="button" class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Batal</button>`;
                znIconbox("Memo / Catatan",SetText,SetAction,'warning');

            }
        });
    }
}

function hapus_app_split() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan reject transaksi?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {

                var SetText = `isi memo atau catatan dibawah ini untuk respon Approval yang anda berikan. <br><br> <textarea name="inputMemoApprove" id="inputMemoApprove" class="form-control" rows="4" cols="80"></textarea>`;
                var SetAction = `<button onclick="gproses1('satu')" type="button" class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Kirim</button><button onclick="znClose()" type="button" class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Batal</button>`;
                znIconbox("Memo / Catatan",SetText,SetAction,'warning');

            }
        });
    }
}
</script>

@include('invoice.action')
@include('transaksi.invoice_modal')
@stop
