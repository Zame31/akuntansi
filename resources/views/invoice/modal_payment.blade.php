<div class="modal fade" id="modal_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="border: none;">
            <div class="modal-header">
                <i class="la la-clipboard zn-icon-modal"></i>
                <h5 class="modal-title" style="margin-top: 10px !important;" id="exampleModalLongTitle">
                    Approval Payment
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body p-0">

                <div class="kt-invoice-1">
                        <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="row mb-4">
                                    <input type="hidden" id="set_type">
                                  
                                    <div class="col-md-3">
                                        <div class="mb-2">Pilih Jenis Pembayaran</div>
                                        <select onchange="setJenisBayar(this.value)" class="form-control" name="jenis_bayar" id="jenis_bayar">
                                            <option value="penuh">Penuh</option>
                                            <option selected value="sebagian">Sebagian</option>
                                        </select>
                                    </div>
                                    <div id="v_t_kurs" class="col-md-3">
                                        <div class="mb-2">Today Kurs</div>
                                        <input onkeyup="setKurs()" name="t_kurs" id="t_kurs" type="text" class="form-control money">
                                    </div>
                                </div>
                                <div id="isSebagian" class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Pilih Komponen Pembayaran</th>
                                                <th id="set_mata_uang">Amount</th>
                                                <th>Amount IDR</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input onchange="setTotal()" name="d1" id="set1v"  type="checkbox" value="" class="kt-group-checkable">
                                                    <div style="width: 400px;">Nett To Underwriter</div> <span></span>
                                                </label> </td>
                                                <td id="set1_other">Rp. 0</td>
                                                <td id="set1">Rp. 0</td>
                                            </tr>
                                            <tr>
                                                <td>  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input onchange="setTotal()" name="d2" id="set2v" type="checkbox" value="" class="kt-group-checkable">
                                                    <div style="width: 400px;">Agent Fee</div>
                                                    <span></span>
                                                </label> </td>
                                                <td id="set2_other">Rp. 0</td>
                                                <td id="set2">Rp. 0</td>
                                            </tr>
                                            <tr>
                                                <td>  <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                                    <input onchange="setTotal()" name="d3" id="set3v" type="checkbox" class="kt-group-checkable">
                                                    <div style="width: 400px;">Comm due to Us + Admin Duty</div>
                                                    <span></span>
                                                </label></td>
                                                <td id="set3_other">Rp. 0</td>
                                                <td id="set3">Rp. 0</td>
                                            </tr>
                                            <tr>
                                                <td>Total Pembayaran</td>
                                                <td id="setTotal_other"></td>
                                                <td id="setTotal">Rp. 0</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a id="send_approval" href="#" class="btn btn-pill btn-primary" style="color: white;">Send Approval</a>

                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script>
    $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

    function zclearNumformat(num) {
        var clearNum = num;
        clearNum = clearNum.replace(/\./g,'');
        clearNum = clearNum.replace(/\,/g,'.');
        return parseFloat(clearNum);
    }

    function setKurs() {
        // loadingPage();
        if($('#t_kurs').val()===''){
            $( "#t_kurs" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#t_kurs").removeClass( "is-invalid" );
        }
        
        let value = [];
        var kurs = zclearNumformat($('#t_kurs').val());

        console.log(kurs);
        

        if ($('#set_type').val() == 'selected') {
            $('input[type="checkbox"][name="ck"]').each(function(){
                if(this.checked){
                    value.push(this.value);
                }
            });
        }
    

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('invoice.get_data_payment') }}',
            data:{
                type : $('#set_type').val(),
                id : value,
                kurs : kurs
            },
            beforeSend: function () {
               
            },
            success: function (ress) {

                var data = $.parseJSON(ress);
                $.each(data, function (k,res) {

                    // RUMUS SPLIT

                    var set1 = parseFloat(res.premi_amount) + parseFloat(res.polis_amount) + parseFloat(res.materai_amount);
                    // var set2 = parseFloat(res.agent_fee_amount) + parseFloat(res.disc_amount);
                    var set2 = parseFloat(res.agent_fee_amount);
                    var set3 = parseFloat(res.comp_fee_amount) + parseFloat(res.admin_amount);
                
               
                    
                    var total = set1 + set2 + set3;

                    console.log(res);
                    
                    console.log(set1);
                    console.log(set2);
                    console.log(set3);

                    $('#set1').html('Rp '+numFormatNew(set1.toFixed(2)));
                    $('#set2').html('Rp '+numFormatNew(set2.toFixed(2)));
                    $('#set3').html('Rp '+numFormatNew(set3.toFixed(2)));
                    setTotal();

                    $('#set1v').val(set1);
                    $('#set2v').val(set2);
                    $('#set3v').val(set3);

            });
            endLoadingPage();
        }
    }).done(function( res ) {
            endLoadingPage();

    }).fail(function(res) {
        endLoadingPage();
        swal.fire("Error","Terjadi Kesalahan!","error");
    });

    }

    function setJenisBayar(type) {
        if (type == 'sebagian') {
            // $('#isSebagian').show();
            $('#set1v').prop('checked', false); 
            $('#set2v').prop('checked', false);
            $('#set3v').prop('checked', false);

            $('#set1v').prop('disabled', false);
            $('#set2v').prop('disabled', false);
            $('#set3v').prop('disabled', false); 
            
            setTotal();
        }else{
            $('#set1v').prop('checked', true);
            $('#set2v').prop('checked', true);
            $('#set3v').prop('checked', true);

            $('#set1v').prop('disabled', true);
            $('#set2v').prop('disabled', true);
            $('#set3v').prop('disabled', true); 
            setTotal();

            // $('#isSebagian').hide();
        }
    }
</script>
