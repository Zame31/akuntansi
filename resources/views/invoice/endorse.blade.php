@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Sales </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Create New Endorsment </a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form Endorsment
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="return simpan_new_invoice();" class="btn btn-success">Simpan</button>
                        @if ($type == 'Edit')
                        <button onclick="loadNewPage('{{ route('invoice_list') }}')" class="btn btn-danger">Kembali</button>

                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body" style="
        background: #fbfbfb;
    ">
        <form id="form-new-invoice" enctype="multipart/form-data">
            <input type="hidden" name="invoice_type_id" id="invoice_type_id" value="1">
            <input type="hidden" name="coc_id" id="coc_id" value="">
            <input type="hidden" id="get_id" name="get_id" value="">

            <div class="row zn-border-bottom mb-5">
                @if ($type == 'Edit')
                <div class="col-md-4">
                    <div class="form-group">
                        <label>COC No</label>
                        <select disabled class="form-control" name="coc_no" id="coc_no">
                            <option disabled value="">Silahkan Pilih</option>
                            @foreach($master_sales_polis as $item)
                                <option value="{{$item->id}}">{{$item->cf_no}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @else
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select COC No</label>
                        <select onchange="setCOC(this.value)" class="form-control kt-select2 init-select2" name="coc_no" id="coc_no">
                            <option disabled value="">Silahkan Pilih</option>
                            @foreach($master_sales_polis as $item)
                                <option value="{{$item->id}}">{{$item->cf_no}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">Silahkan Pilih</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="searchPolicy();" class="btn btn-info" style="margin-top: 25px;">Search</button>
                </div>
            
            @endif
                
            </div>

            <div id="dataShow" style="display: none;">
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Policy Customer Name</label>
                            <input type="text" class="form-control" name="policy_cust_name" id="policy_cust_name" maxlength="100">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Insured Name</label>
                            <input type="text" class="form-control" name="insured_name" id="insured_name">
                            <div class="invalid-feedback" id="sn">Silahkan Isi Insured Name</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Endors Reason</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Endors reason" name="endorse_reason" id="endorse_reason" />
                                <div class="invalid-feedback" id="error-endors-reason">Silahkan Isi Endors Reason</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row zn-border-bottom mb-5">
                
                    <input type="hidden" class="form-control" name="invoice" id="invoice" maxlength="80">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Underwritter</label>
                            <select class="form-control" disabled="disabled" name="id_underwriter" id="id_underwriter">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_underwriter as $item)
                                    <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="id_underwriter" id="id_underwriter1"> 
                        </div>
                    </div>

                    {{-- <div class="col-md-4">
                        <div class="form-group">
                            <label>Policy Number</label>
                            <input type="text" class="form-control" name="no_polis" id="no_polis" maxlength="80">
                            <div class="invalid-feedback" id="sn">Silahkan Isi No Polis</div>
                            <div class="invalid-feedback" id="mk">Maximal 80 karakter</div>
                        </div>
                    </div> --}}

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Customer Name</label>
                            <select class="form-control" disabled="disabled" name="customer" id="customer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_customer as $item)
                                <option value="{{$item->id}}">{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="customer" id="customer1"> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Policy Number</label>
                            <input type="text" disabled class="form-control" name="no_polis" id="no_polis" maxlength="80">
                            <div class="invalid-feedback" id="sn">Silahkan Isi No Polis</div>
                            <div class="invalid-feedback" id="mk">Maximal 80 karakter</div>
                        </div>
                    </div>

                    {{-- <div class="col-md-4">
                        <div class="form-group">
                            <label>Product Name</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled" name="product" id="product" >
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_product as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="product" id="product1"> 
                        </div>
                    </div> --}}

                    {{-- <div class="col-md-4">
                        <div class="form-group">
                            <label>Agent</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled" name="officer" id="officer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_agent as $item)
                                <option value="{{$item->id}}">{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="officer" id="officer1"> 
                        </div>
                    </div> --}}
                    
                </div>

                <div class="row zn-border-bottom mb-5">

                    <div class="col-md-4">
                        {{-- <div class="form-group">
                            <label>Customer</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled" name="customer" id="customer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_customer as $item)
                                <option value="{{$item->id}}">{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="customer" id="customer1"> 
                        </div> --}}

                        <div class="form-group">
                            <label>Product Name</label>
                            <select class="form-control" disabled="disabled" name="product" id="product" >
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_product as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="product" id="product1"> 
                        </div>

                        <div class="form-group">
                            <label>Quotation Number</label>
                            <select class="form-control" disabled="disabled" name="quotation" id="quotation">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_quotation as $item)
                                <option value="{{$item->id}}">{{$item->qs_no}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="quotation" id="quotation1"> 

                        </div>

                        <div class="form-group">
                            <label>Bank Account No</label>
                            <select class="form-control kt-select2 init-select2" name="bank" id="bank">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_bank as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>

                        <div class="form-group">
                            <label>Start Date Aging</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="tgl_mulai" id="tgl_mulai1" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi Start Date</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Valuta ID</label>
                            <select class="form-control kt-select2 init-select2" name="valuta_id" id="valuta_id">
                                <option value="" selected disabled>Silahkan Pilih</option>
                                @foreach($ref_valuta as $item)
                                    <option value="{{$item->id}}">{{$item->mata_uang}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="form-group">
                            <label>Segment</label>
                            <select class="form-control kt-select2 init-select2" name="segment" id="segment">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_cust as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            {{-- <input type="hidden" name="segment" id="segment1">  --}}
                        </div>
                        

                        <div class="form-group">
                            <label>Proposal</label>
                            <select class="form-control" disabled="disabled" name="proposal" id="proposal">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_proposal as $item)
                                <option value="{{$item->id}}">{{$item->cn_no}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="proposal" id="proposal1"> 

                        </div>

                        

                        <div class="form-group">
                            <label>Pajak Ditanggung Perusahaan</label>
                            <select class="form-control kt-select2 init-select2" name="is_tax_company" id="is_tax_company">
                                <option value="true">Ya</option>
                                <option value="false">Tidak</option>
                            </select>
                        </div>

                        <!-- Tambahan -->
                        <div class="form-group">
                            <label>Start Date Policy</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="start_date_Policy" id="start_date_Policy" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi Start Date Policy</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label> Today Kurs </label>
                            {{-- <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" class="form-control text-right money" onkeyup="convertToRupiah1(this)"  placeholder="0" name="today_kurs" id="today_kurs">
                                <div class="invalid-feedback">Silahkan Isi Today Kurs</div>
                            </div> --}}
                            <input type="text" class="form-control text-right money" placeholder="0" name="today_kurs" id="today_kurs">
                            <div class="invalid-feedback">Silahkan Isi Today Kurs</div>
                        </div>

                        <div class="form-group">
                            <label>Total Sum Insured</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" class="form-control text-right money" onkeyup="convertToRupiah1(this)"  placeholder="0" name="insurance" id="insurance">
                                <div class="invalid-feedback">Silahkan Isi Total Sum Insured</div>
                                <input type="hidden" name="total_sum_insured" id="total_sum_insured"> 
                            </div>
                        </div>

                        <!-- Tambahan -->
                        {{-- <div class="form-group">
                            <label>Insured Name</label>
                            <input type="text" class="form-control" name="insured_name" id="insured_name">
                            <div class="invalid-feedback" id="sn">Silahkan Isi Insured Name</div>
                        </div> --}}
                        

                        {{-- <div class="form-group">
                            <label>Underwriter</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled" name="id_underwriter" id="id_underwriter">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_underwriter as $item)
                                    <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="id_underwriter" id="id_underwriter1"> 
                        </div> --}}

                    </div>

                    <div class="col-md-4">
                        {{-- <div class="form-group">
                            <label>End Date Aging</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="tgl_akhir" id="tgl_akhir1" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi End Date</div>
                            </div>
                        </div> --}}


                        <div class="form-group">
                            <label>Agent</label>
                            <select class="form-control " disabled="disabled" name="officer" id="officer">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_agent as $item)
                                <option value="{{$item->id}}">{{$item->full_name}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="officer" id="officer1"> 
                        </div>

                        {{-- <div class="form-group">
                            <label>Segment</label>
                            <select class="form-control kt-select2 init-select2" disabled="disabled" name="segment" id="segment">
                                <option value="">Silahkan Pilih</option>
                                @foreach($ref_cust as $item)
                                <option value="{{$item->id}}">{{$item->definition}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">Silahkan Pilih</div>
                            <input type="hidden" name="segment" id="segment1"> 
                        </div> --}}

                        <div class="form-group">
                            <label>Validate Periode (in days)</label>
                            <div class="input-group date">
                                <input type="text" onkeypress="return hanyaAngka(event)" class="form-control" maxlength="3" placeholder="Validate Periode" name="tgl_akhir" id="tgl_akhir2" />
                                {{--
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                --}}
                                <div class="invalid-feedback" id="sn">Silahkan Isi Validate Periode</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Number of Interest Insured</label>
                            <input type="text" class="form-control" onkeypress="return hanyaAngka(event)" name="no_of_insured" id="no_of_insured" maxlength="80">
                            <div class="invalid-feedback">Silahkan Isi</div>
                        </div>

                        <!-- Tambahan -->
                        <div class="form-group">
                            <label>End Date Policy</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" name="end_date_Policy" id="end_date_Policy" />
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar-check-o"></i>
                                    </span>
                                </div>
                                <div class="invalid-feedback" id="sn">Silahkan Isi End Date Policy</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Valas Amount</label>
                            <div class="input-group">
                                <input type="text" class="form-control money" placeholder="Valas Amount" name="valas_amount" id="valas_amount" />
                                <div class="invalid-feedback" id="sn">Silahkan Isi Valas Amount</div>
                            </div>
                        </div>

                    
                        <div class="form-group">
                            <label>Upload Dokumen</label>
                            <input type="file" class="form-control" name="file" id="file">
                            <div class="invalid-feedback" id="ss">Silahkan Upload Dokumen</div>
                            <div class="invalid-feedback" id="ff">Format Dokumen pdf,jpeg,jpg,png,xls,xlsx</div>
                            <div class="invalid-feedback" id="mm">Max Size Dokumen 2 Mb</div>
                        </div>

                        {{-- <div class="form-group">
                            <label>Endors Reason</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Endors reason" name="endorse_reason" id="endorse_reason" />
                                <div class="invalid-feedback" id="error-endors-reason">Silahkan Isi Endors Reason</div>
                            </div>
                        </div> --}}

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gross Premium Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this)"  name="premi" id="premi">
                            </div>
                            <div class="invalid-feedback" id="pa">Silahkan Isi Premi Amount</div>
                            {{-- <div class="invalid-feedback" id="pa1">Premi Amount tidak boleh melebihi Sum Insured</div>                         --}}
                            <div class="invalid-feedback" id="pa1">Gross Premium Amount tidak boleh melebihi Sum Insured</div>                        
                        </div>

                        <div class="form-group">
                            <label>Commission Due to HD Soeryo</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" onkeyup="feeAgentChange()" class="form-control text-right money" onkeyup="convertToRupiah(this)"  placeholder="0" id="fee_internal" name="fee_internal">
                            </div>
                        </div>


                        <div class="form-group">
                            <label>Policy Duty</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" class="form-control text-right money" onkeyup="feeAgentChange()"  placeholder="0" name="fee_polis" id="fee_polis" value="32.000">
                                <div class="invalid-feedback">Silahkan Isi Fee Polis</div>
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label>Premium Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this)"  name="premi" id="premi">
                            </div>
                            <div class="invalid-feedback" id="pa">Silahkan Isi Premi Amount</div>
                            <div class="invalid-feedback" id="pa1">Premi Amount tidak boleh melebihi Sum Insured</div>                        
                        </div> --}}



                        <div class="form-group">
                            <label>Nett Premium</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                    <input type="text" disabled class="form-control text-right money" onkeyup="convertToRupiah(this)"  placeholder="0" name="nett_amount" id="nett_amount">
                                </div>
                        </div>
                        

                    </div>
                    <div class="col-md-4">

                        <div class="form-group">
                            <label>Discount Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" class="form-control text-right money" placeholder="0" onkeyup="convertToRupiah2(this);feeAgentChange();"  name="disc_amount" id="disc_amount">
                                <div class="invalid-feedback" id="sd">Silahkan Isi Disc Amount</div>
                                <div class="invalid-feedback" id="mi">Disc Amount tidak boleh melebihi Premi Amount</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Commission Due To Agent</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                    <input type="text"  class="form-control text-right money" placeholder="0" onkeyup="feeAgentChange()"  name="fee_agent" id="fee_agent">
                                </div>
                        </div>

                        <div class="form-group">
                            <label>Stamp Duty</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input onkeyup="feeAgentChange()" type="text" class="form-control text-right money"  placeholder="0" name="fee_materai" id="fee_materai" value="6.000">
                                <div class="invalid-feedback">Silahkan Isi Fee Materai</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Nett To Underwriter</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" disabled class="form-control text-right money" onkeyup="convertToRupiah(this)"  placeholder="0" id="asuransi" name="asuransi">
                            </div>
                        </div>


                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="visibility: hidden">
                            <label>Nett Premium</label>
                            <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                    <input type="text" readonly class="form-control text-right money" onkeyup="convertToRupiah(this)"  placeholder="0" name="nett_amount" id="nett_amount">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Agent Tax Amount</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text"  class="form-control text-right money" onkeyup="convertToRupiah(this)"  placeholder="0" id="tax_amount" name="tax_amount">
                            </div>
                        </div>

                        <div class="form-group">
                            {{-- <label>Adm Duty</label> --}}
                            <label>HD Soeryo Duty</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text zn-rp-group">Rp</span></div>
                                <input type="text" class="form-control text-right" onkeyup="feeAgentChange()"  placeholder="0" name="fee_admin" id="fee_admin" value="5.000">
                                {{-- <div class="invalid-feedback">Silahkan Isi Fee Admin</div> --}}
                                <div class="invalid-feedback">Silahkan Isi HD Soeryo Duty</div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

        </form>
        <div class="row">
            <div class="col-12" style="text-align: right;">
                <button onclick="return simpan_new_invoice();" class="btn btn-success">Simpan</button>
            </div>
        </div>
        </div>
    </div>
</div>
@include('marketing.modal.modal_search_policy')
@php
    if ($type == 'Edit'){
        $datetime1 = new DateTime($data->start_date);
        $datetime2 = new DateTime($data->end_date);
        $difference = $datetime1->diff($datetime2);

        if ($data->is_tax_company == 1) {
            $is_tax_c = 'true';
        }else{
            $is_tax_c = 'false';
        }
    }
    // dd($data)
@endphp
<!-- end:: Content -->
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">

var typeStore = '{{$type}}';

$(document).ready(function () {
    if (typeStore == 'Edit') {
        $('#dataShow').show();
    }else{
        $("#today_kurs").prop('disabled', true);
        $("#today_kurs").val('');
        $("#valas_amount").prop('disabled', true);

        
        $('#dataShow').hide();
    }

    // $('#coc_no').select2('destroy');
    $('#coc_no').val(null).select2();
});


@if ($type == 'Edit')
if (typeStore == 'Edit') {

 
    $(document).ready(function(){
        $('#coc_no').append(`<option value={{$data->coc_id}}>{{$data->cf_no}}</option>`);                
        // $('#coc_no').select2('destroy');
        $('#coc_no').val('{{$data->coc_id}}').select2();
      });
      
    $('#get_id').val('{{$data->id}}');
    $('#id_underwriter').val('{{$data->underwriter_id}}');
    $('#no_polis').val('{{$data->polis_no}}');
    $('#customer').val('{{$data->customer_id}}');
    $('#product').val('{{$data->product_id}}');
    $('#segment').val('{{$data->segment_id}}');
    $('#officer').val('{{$data->agent_id}}');
    $('#quotation').val('{{$data->qs_no}}');
    $('#bank').val('{{$data->afi_acc_no}}');
    $('#tgl_mulai1').val('{{date("d F Y",strtotime($data->start_date))}}');
    $('#valuta_id').val('{{$data->valuta_id}}');
    $('#proposal').val('{{$data->proposal_no}}');
    $('#is_tax_company').val('{{$is_tax_c}}');
    $('#start_date_police').val('{{date("d F Y",strtotime($data->start_date_polis))}}');
    $('#today_kurs').val(formatMoney('{{$data->kurs_today}}'));
    $('#tgl_akhir2').val('{{$difference->days}}');
    $('#no_of_insured').val('{{$data->no_of_insured}}');
    $('#end_date_police').val('{{date("d F Y",strtotime($data->end_date_polis))}}');
    $('#valas_amount').val(formatMoney('{{$data->valuta_amount/$data->kurs_today}}'));
    $('#premi').val(formatMoney('{{$data->premi_amount/$data->kurs_today}}'));
    $('#disc_amount').val(formatMoney('{{$data->disc_amount/$data->kurs_today}}'));
    $('#fee_internal').val(formatMoney('{{$data->comp_fee_amount/$data->kurs_today}}'));
    $('#fee_polis').val(formatMoney('{{$data->polis_amount/$data->kurs_today}}'));
    $('#nett_amount').val(formatMoney('{{$data->net_amount/$data->kurs_today}}'));
    $('#fee_agent').val(formatMoney('{{$data->agent_fee_amount/$data->kurs_today}}'));
    $('#fee_materai').val(formatMoney('{{$data->materai_amount/$data->kurs_today}}'));
    $('#asuransi').val(formatMoney('{{$data->ins_fee/$data->kurs_today}}'));
    $('#tax_amount').val(formatMoney('{{$data->tax_amount/$data->kurs_today}}'));
    $('#fee_admin').val(formatMoney('{{$data->admin_amount/$data->kurs_today}}'));
    $('#insurance').val(formatMoney('{{$data->ins_amount/$data->kurs_today}}'));
    $('#coc_no').val('{{$data->coc_id}}');
    $('#policy_cust_name').val('{{$data->police_name}}');

    $('#start_date_Policy').val(znFormatDateNum('{{$data->start_date_polis}}'));
    $('#end_date_Policy').val(znFormatDateNum('{{$data->end_date_polis}}'));

    $('#insured_name').val('{{$data->insured_name}}');
    $('#endorse_reason').val('{{$data->endors_reason}}');
   

    if ($('#valuta_id').val() == 1) {
        $("#today_kurs").prop('disabled', true);
        $("#today_kurs").val('');
        $("#valas_amount").prop('disabled', true);
    }


    var text =  $( "#valuta_id option:selected" ).text();
    $('.zn-rp-group').html(text);

}
@endif


console.log(base_url);

function pilihCOC(coc_id) {
    $('#coc_no').val(coc_id).trigger('change.select2');
    $('#modal-search-policy').modal('hide');
}

function setCOC(id) {
    console.log('coc_id',id);
    $.ajax({
        type: 'GET',
        url: base_url + 'marketing/data/master_sales_polis_byid/' + id,
        beforeSend: function (res) {
            loadingPage();
        },
        success: function (res) {
            console.log(res);
            let data = res.rm;
            $('#insurance').val(formatMoney(data.ins_amount));
            $('#no_of_insured').val(data.no_of_insured);
            $('#policy_cust_name').val(data.full_name);
            // select 2
            $('#id_underwriter').val(data.underwriter_id).trigger('change.select2');
            $('#customer').val(data.customer_id).trigger('change.select2');
            $('#product').val(data.product_id).trigger('change.select2');
            $('#officer').val(data.agent_id).trigger('change.select2');
            $('#valuta_id').val(data.valuta_id).trigger('change.select2');
            $('#quotation').val(data.qs_id).trigger('change.select2');
            $('#proposal').val(data.cn_id).trigger('change.select2');
            
            //date
            $('#start_date_Policy').val(znFormatDateNum(data.start_date_polis));
            $('#end_date_Policy').val(znFormatDateNum(data.end_date_polis));
            $('#no_polis').val(data.polis_no);
            $('#today_kurs').val(formatMoney(data.kurs_today));
            $('#valas_amount').val(formatMoney(data.valuta_amount));
            $('#premi').val(formatMoney(Math.abs(data.premi_amount-data.realisasi_premium)));
            $('#disc_amount').val(formatMoney(data.disc_amount));
            $('#fee_internal').val(formatMoney(data.comp_fee_amount));
            $('#fee_polis').val(formatMoney(data.polis_amount));
            $('#nett_amount').val(formatMoney(data.net_amount));
            $('#fee_agent').val(formatMoney(data.agent_fee_amount));
            $('#fee_materai').val(formatMoney(data.materai_amount));
            $('#asuransi').val(formatMoney(data.ins_fee));
            $('#tax_amount').val(formatMoney(data.tax_amount));
            $('#fee_admin').val(formatMoney(data.admin_amount));
            $('#insurance').val(formatMoney(Math.abs(data.ins_amount-data.realisasi_sum_insured)));
            $('#ef_pct').val(formatMoney(data.ef_pct));
            $('#ef').val(formatMoney(data.ef));
            // $('#coc_id').val(data.coc_id);

            $('#insured_name').val(data.insured_name);
            $('#endorse_reason').val(data.endors_reason);

             // input type hidden
            $('#product1').val(data.product_id);
            $('#officer1').val(data.agent_id);
            $('#customer1').val(data.customer_id);
            $('#quotation1').val(data.qs_id);
            $('#proposal1').val(data.cn_id);
            $('#id_underwriter1').val(data.underwriter_id);

            if ($('#valuta_id').val() == 1) {
                $("#today_kurs").prop('disabled', true);
                $("#today_kurs").val('');
                $("#valas_amount").prop('disabled', true);
            }


            var text =  $( "#valuta_id option:selected" ).text();
            $('.zn-rp-group').html(text);

            // convertToRupiah1(data.ins_amount);
            endLoadingPage();
            $('#dataShow').fadeIn();

        }
    });
}

function setPolicyNumber(id) {
    console.log(base_url);
    console.log(id);

    
        $.ajax({

            type: 'GET',
        url: base_url + '/get_polis?id=' + id,

        beforeSend: function () {
            $("#alertInfo").addClass('d-none');
            $("#alertError").addClass('d-none');
            $("#alertSuccess").addClass('d-none');
            $("#loading").css('display', 'block');
        },

        success: function (response) {
            
            var data = $.parseJSON(response);
            console.log(data);

            if ( data != "" ) {
                console.log(data[0]['inv_no']);
                console.log(data[0]['wf_status_id']);
                console.log(data[0]['paid_status_id']);
                var stsInvoicePaid = [9, 16, 17, 18];

                if ( (data[0]['wf_status_id'] == 9 && data[0]['paid_status_id'] == 0) ||
                     (data[0]['wf_status_id'] == 13 && data[0]['paid_status_id'] == 1) ||
                     ($.inArray(data[0]['wf_status_id'], stsInvoicePaid) != -1 && data[0]['paid_status_id'] == 1)
                    ) {
                    // STATUS INVOICE PAID

                    $("#loading").css('display', 'none');
                    $('#invoice').val(data[0]['inv_no']);

                    $('#product').val(data[0]['product_id']).trigger('change');
                    $('#officer').val(data[0]['agent_id']).trigger('change');

                    $('#customer').val(data[0]['customer_id']).trigger('change');

                    $('#today_kurs').val(formatMoney(data[0]['kurs_today']));
                    $('#valas_amount').val(formatMoney(data[0]['valuta_amount']));
                    $('#valuta_id').val(data[0]['valuta_id']).trigger('change.select2');

                    
                    $('#quotation').val(data[0]['qs_no']).trigger('change');
                    $('#proposal').val(data[0]['proposal_no']).trigger('change');
                    $('#segment').val(data[0]['segment_id']).trigger('change');
                    $('#bank').val(data[0]['afi_acc_no']).trigger('change');
                    $('#id_underwriter').val(data[0]['underwriter_id']).trigger('change');

                    $('#start_date_Policy').val(znFormatDateNum(data[0]['start_date_polis']));
                    $('#end_date_Policy').val(znFormatDateNum(data[0]['end_date_polis']));

                    // input type hidden
                    $('#product1').val(data[0]['product_id']);
                    $('#officer1').val(data[0]['agent_id']);
                    $('#customer1').val(data[0]['customer_id']);
                    $('#quotation1').val(data[0]['qs_no']);
                    $('#proposal1').val(data[0]['proposal_no']);
                    $('#segment1').val(data[0]['segment_id']);
                    $('#id_underwriter1').val(data[0]['underwriter_id']);
                    $('#coc_id').val(data[0]['coc_id']);

                    

                    $('#insurance').val(formatMoney(data[0]['ins_amount']));

                    if ($('#valuta_id').val() == 1) {
                        $("#today_kurs").prop('disabled', true);
                        $("#today_kurs").val('');
                    }


                    var text =  $( "#valuta_id option:selected" ).text();
                    $('.zn-rp-group').html(text);

                    convertToRupiah1(data[0]['ins_amount']);
                } else {
                    toastr.warning("Invoice tidak aktif");
                }
                
            } else {
                toastr.warning("Data tidak ditemukan");
            }
            

            }
        }).done(function (msg) {
            $("#loading").css('display', 'none');
        }).fail(function (msg) {
            $("#loading").css('display', 'none');
        });

        $('#dataShow').fadeIn();
        
    }

    function pilihPolicyNumber(id) {
        $('#no_polis').val(id).trigger('change.select2');
        $('#modal-search-policy').modal('hide');
    }

    function searchPolicy() {

        $('#title_modal_seach').html('List Data Master Policy Active');

        var act_url = base_url + 'marketing/data/search_master_sales_original_table/endorsment';

            var tableSearchPolicy = $('#search_policy_table').DataTable({
                aaSorting: [],
                processing: true,
                serverSide: true,
                responsive: true,
                destroy:true,
                columnDefs: [
                    { "orderable": false, "targets": 0 }],
                ajax: {
                    "url" : act_url,
                    "error": function(jqXHR, textStatus, errorThrown)
                        {
                            toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                        }
                    },
                columns: [
                    { data: 'cf_no', name: 'cf_no' },    
                    { data: 'full_name', name: 'full_name' },
                    { data: 'underwriter', name: 'underwriter' },
                    { data: 'no_of_insured', name: 'no_of_insured' },
                    { data: 'produk', name: 'produk' },
                    { data: 'agent', name: 'agent' },
                    { data: 'mata_uang', name: 'mata_uang' },
                    { data: 'start_date_polis', name: 'start_date_polis' },
                    { data: 'end_date_polis', name: 'end_date_polis' },
                    { data: 'ins_amount', name: 'ins_amount' },
                    // { data: 'gross_premium', name: 'gross_premium' },
                    // { data: 'nett_premium', name: 'nett_premium' },
                    // { data: 'cn_no', name: 'cn_no' },

                    { data: 'action', name: 'action' },   
                ]
            });

        $('#modal-search-policy').modal('show');
    }

    
function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
}


    $(document).ready(function(){

        $('#tgl_mulai1').change(function () {
            $('#tgl_akhir1').val('');
            $('#tgl_akhir1').datepicker('destroy');
            console.log(this.value);
            $('#tgl_akhir1').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
                startDate: this.value
            });
        });

        // disabled top open select2 when enter
        $(document).on('keydown', '.select2-selection', function (evt) {
            if (evt.which === 13) {
                console.log('disable select 2 enter');
                $(".init-select2").select2('close');
            }
        });
    }); // end document ready

    
    $("#valas_amount").keyup(function (elm) {
        var today_kurs = $("#today_kurs").val();

        if ( today_kurs != "" ) {
            today_kurs = today_kurs.replace(/\./g,'');
            today_kurs = today_kurs.replace(/\,/g,'.');

            var valas_amount = $(this).val();
            valas_amount = valas_amount.replace(/\./g,'');
            valas_amount = valas_amount.replace(/\,/g,'.');

            var hasil = today_kurs * valas_amount;
            hasil = hasil.toFixed(2).replace(/\./g, ",");

            console.log('today kurs : ' + today_kurs);
            console.log('valas amount : ' + valas_amount);
            console.log('hasil : ' + hasil);
            
            
            $('#insurance').val(hasil).trigger('mask.maskMoney');
            convertToRupiah1($("#insurance"));

            $("#total_sum_insured").val(hasil);

        } 
        
    }); 

    $("#today_kurs").keyup(function (elm) {
        var valas_amount = $("#valas_amount").val();
        var today_kurs = $(this).val();

        if ( valas_amount != "" ) {
            today_kurs = today_kurs.replace(/\./g,'');
            today_kurs = today_kurs.replace(/\,/g,'.');

            valas_amount = valas_amount.replace(/\./g,'');
            valas_amount = valas_amount.replace(/\,/g,'.');

            var hasil = today_kurs * valas_amount;
            hasil = hasil.toFixed(2).replace(/\./g, ",");

            console.log('today kurs : ' + today_kurs);
            console.log('valas amount : ' + valas_amount);
            console.log('hasil : ' + hasil);
            
            $('#insurance').val(hasil).trigger('mask.maskMoney');
            convertToRupiah1($("#insurance"));

            $("#total_sum_insured").val(hasil);
            
        } 
        
    }); 

    $("#valuta_id").on('change', function(elm) {
        var value = $(this).val();

        $("#valuta_id").parent().removeClass('has-error');
        $("#valuta_id").removeClass( "is-invalid" );

        if ( value == "1" ) {
            // IDR SELECTED
            $("#today_kurs").prop('disabled', true);
            $("#valas_amount").prop('disabled', true);
            $("#insurance").prop('disabled', false);

            $("#today_kurs").val('');
            $("#valas_amount").val('');
            $("#insurance").val('');

            $('#premi').val('');
            $('#disc_amount').val('');
            $('#nett_amount').val('');
            $('#fee_agent').val('');
            $('#fee_internal').val('');
            $('#asuransi').val('');
            $('#tax_amount').val('');
        } else {
            $("#today_kurs").prop('disabled', false);
            $("#valas_amount").prop('disabled', false);
            $("#insurance").prop('disabled', true);
        }

    });
    //minDate: new Date(1999, 10 - 1, 25)
    /*
    $(document).ready(function(){
    $('#tgl_mulai').datepicker({ onClick: function(dateText, inst) { alert("Working"); } });
    });
    */

    $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

    var date = new Date();
    
    $('#tgl_mulai1').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     });

     // Tambahan 
     $('#start_date_Policy').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     }).on('changeDate', function(){
        // set the "fromDate" end to not be later than "toDate" starts:
        $('#end_date_Policy').datepicker('setStartDate', new Date($(this).val()));
    });

     $('#end_date_Policy').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        // startDate: date
     }).on('changeDate', function(){
        // set the "fromDate" end to not be later than "toDate" starts:
        $('#start_date_Policy').datepicker('setEndDate', new Date($(this).val()));
    });
    
    console.log(date);


    
/*
    startDate: lastDay,
        endDate: lastDay,
        beforeShowDay: function(date){
            if(date==lastDay){
                return true;
            }else{
                return false;
            }
            
        }
        */
    $('#tgl_tr').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        
        
    });
    
    /*
    $('#tgl_mulai').datepicker({
                format: 'dd/mm/yyyy',
                onSelect: function(dateText) {
                    console.log("Selected date: " + dateText + "; input's current value: " + this.value);
                }
            });
    */

    $('#invoice').keyup(function() {
        var panjang = this.value.length;

        if(panjang==80){
            $('#invoice').addClass( "is-invalid" );
            $('#sn').hide();
            $('#mk').show();
        }else{
            $('#invoice').removeClass( "is-invalid" );
            $('#sn').hide();
            $('#mk').hide();
        }
    });

    // var nilaiAsuransi;
    // $("#fee_polis").keyup( function() {
    //     var Policy_duty = parseFloat($(this).val().replace('.', ''));
    //     var stamp_duty = parseFloat($("#fee_materai").val().replace('.', ''));
    //     var hasil = Policy_duty + stamp_duty + nilaiAsuransi;

    //     console.log('Nilai asuransi : ' + nilaiAsuransi);
    //     console.log('Policy duty : ' + Policy_duty);
    //     console.log('stamp duty : ' + stamp_duty);
        
        
    //     var sum = Policy_duty + $asuransi;
        
    //     $("#asuransi").val(hasil);
    //     $('#asuransi').val(hasil).trigger('mask.maskMoney');
    //     console.log(hasil);

    // });

    // $("#fee_materai").keyup( function() {
    //     var stamp_duty = parseFloat($(this).val().replace('.', ''));
    //     var Policy_duty = parseFloat($("#fee_polis").val().replace('.', ''));
    //     var hasil = Policy_duty + stamp_duty + nilaiAsuransi;

    //     console.log('Nilai asuransi : ' + nilaiAsuransi);
    //     console.log('Policy duty : ' + Policy_duty);
    //     console.log('stamp duty : ' + stamp_duty);
        
        
    //     var sum = Policy_duty + $asuransi;
        
    //     $("#asuransi").val(hasil);
    //     $('#asuransi').val(hasil).trigger('mask.maskMoney');
    //     console.log(hasil);

    // });

    function convertToRupiah2 (objek) {
     
     var premi=$('#premi');
     var insurance=$('#insurance');
     var disc_amount=$('#disc_amount');

     if(premi.val()===''){
         premi.addClass( "is-invalid" );
         document.getElementById("disc_amount").value="";
     }else{

         var aa1=insurance.val();
         var aa2=aa1.replace(/\./g,'');

         var bb1=premi.val();
         var bb2=bb1.replace(/\./g,'');
         var smd1=Math.floor(b2) > Math.floor(a2);

         if(smd){
            premi.addClass( "is-invalid" );
            $('#pa').hide();
            $('#pa1').show();
         }else{
             premi.removeClass( "is-invalid" )
         }
         


         var a1=premi.val();
         var a2=a1.replace(/\./g,'');
         var a3=a2.replace(/\,/g,'.');

         var b1=disc_amount.val();
         var b2=b1.replace(/\./g,'');
         var b3=b2.replace(/\,/g,'.');
         
         var smd=Math.floor(b3) > Math.floor(a3);

         if(smd){
            disc_amount.addClass( "is-invalid" );
            $('#sd').hide();
            $('#mi').show();
           // disc_amount.val(insurance.val());
         }else{

            var c2= a3 - b3;

            $nett=c2.toFixed(2).replace(/\./g, ",");
             $('#nett_amount').val($nett).trigger('mask.maskMoney');

             //$('#nett_amount').val(c2);


         
         $.ajax({
             type: 'GET',
             url: base_url + '/fee1?jml='+a3+'&company='+$('#is_tax_company').val(),
             success: function (res) {
                 var data = $.parseJSON(res);
                     //console.log(data.fee_agent);
                 
                     $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                     $('#fee_agent').val($agent).trigger('mask.maskMoney');


                     $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                     $('#fee_internal').val($internal).trigger('mask.maskMoney');
                     
                    

                     var aa1=$('#nett_amount').val();
                     var aa2=aa1.replace(/\./g,'');
                     var aa3=aa2.replace(/\,/g,'.');

                     var asuransi=aa3 - data.fee_agent - data.komisi_perusahaan;
                        
                     console.log('asuransi');
                     console.log(aa3);
                     console.log(aa3 - data.fee_agent - data.ko);
                     console.log(data.fee_agent);
                     console.log(data.komisi_perusahaan);
                     console.log(asuransi);
                     
                     $asuransi=asuransi.toFixed(2).replace(/\./g, ",");               
                     $('#asuransi').val($asuransi).trigger('mask.maskMoney');;

                     $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");    
                     $('#tax_amount').val($amount).trigger('mask.maskMoney');;
                     
             }
         });             

            disc_amount.removeClass( "is-invalid" );
         }
         premi.removeClass( "is-invalid" );
     }

 }

 function zclearNumformat(num) {
    var clearNum = num;
    clearNum = clearNum.replace(/\./g,'');
    clearNum = clearNum.replace(/\,/g,'.');
    return parseInt(clearNum);
}

function feeAgentChange() {

    var nett_amount = zclearNumformat($('#nett_amount').val());
    var fee_agent = zclearNumformat($('#fee_agent').val());
    var fee_internal = zclearNumformat($('#fee_internal').val());
    var fee_polis = zclearNumformat($('#fee_polis').val());
    var fee_materai = zclearNumformat($('#fee_materai').val());
    var fee_admin = zclearNumformat($('#fee_admin').val());
    
    var asuransi=nett_amount - fee_agent - fee_internal +fee_polis +fee_materai+fee_admin;

    $asuransi=asuransi.toFixed(2).replace(/\./g, ",");               
    $('#asuransi').val($asuransi).trigger('mask.maskMoney');
}

 function convertToRupiah1 (objek) {

     var insurance=$('#insurance');
     /*
     var disc_amount=$('#disc_amount');

     var polis=$('#fee_polis');
     var admin=$('#fee_admin');
     var materai=$('#fee_materai');
 */
 var Policy_duty = parseFloat($("#fee_polis").val().replace('.', ''));
        var stamp_duty = parseFloat($("#fee_materai").val().replace('.', ''));

     if(insurance.val()===''){
         insurance.addClass( "is-invalid" );
     //    document.getElementById("disc_amount").value="";
     }else{

         var a1=insurance.val();
         var a2=a1.replace(/\./g,'');
         console.log(a2);
         
         $.ajax({
             type: 'GET',
             url: base_url + '/fee?jml='+a2+'&company='+$('#is_tax_company').val(),
             success: function (res) {
                 var data = $.parseJSON(res);
                     //console.log(data.fee_agent);
                     
                     //.trigger('mask.maskMoney')
                     $premi=data.premi_amount.toFixed(2).replace(/\./g, ",");
                     $('#premi').val($premi).trigger('mask.maskMoney');
                     //$('#premi').maskMoney("#,##0.00", {reverse: true});
                     //$("#premi").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

                     $discount=data.discount.toFixed(2).replace(/\./g, ",");
                     $('#disc_amount').val($discount).trigger('mask.maskMoney');
                                            

                     $nett=data.nett_amount.toFixed(2).replace(/\./g, ",");
                     $('#nett_amount').val($nett).trigger('mask.maskMoney');
                     

                     $agent=data.fee_agent.toFixed(2).replace(/\./g, ",");
                     $('#fee_agent').val($agent).trigger('mask.maskMoney');
                     


                     $internal=data.komisi_perusahaan.toFixed(2).replace(/\./g, ",");
                     $('#fee_internal').val($internal).trigger('mask.maskMoney');

                    //  nilaiAsuransi = data.asuransi;
                    // $asuransi = data.asuransi + Policy_duty + stamp_duty;
                    // $asuransi = $asuransi.toFixed(2).replace(/\./g, ",");

                    //  $asuransi=data.asuransi.toFixed(2).replace(/\./g, ",");
                    //  $('#asuransi').val($asuransi).trigger('mask.maskMoney');
                    feeAgentChange();

                     $amount=data.tax_amount.toFixed(2).replace(/\./g, ",");
                     $('#tax_amount').val($amount).trigger('mask.maskMoney');


             }
         });
         insurance.removeClass( "is-invalid" );
     }

 }

    

var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  <button onclick="znClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                    Data</button>
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;
function znClose() {
    znIconboxClose();
    //location.reload();
    $("#form-new-invoice")[0].reset();
    document.getElementById("form-new-invoice").reset();

    //$("#form-new-invoice").data('bootstrapValidator').resetForm();

}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('invoice_list') }}');
}


var _create_form = $("#form-new-invoice");
function simpan_new_invoice(){
    loadingPage();
    /*
    if($('#tgl_tr').val()===''){
        endLoadingPage();
        $( "#tgl_tr" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_tr").removeClass( "is-invalid" );

    }
    */


    if($('#no_polis').val()===''){
        endLoadingPage();
        $( "#no_polis" ).addClass( "is-invalid" );
        $('#sn').show();
        $('#mk').hide();
        return false;
    }else{
        $("#no_polis").removeClass( "is-invalid" );
    }

    if($('#product').val()===''){
        endLoadingPage();
        $( "#product" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#product").removeClass( "is-invalid" );

    }

    if($('#officer').val()===''){
        endLoadingPage();
        $( "#officer" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#officer").removeClass( "is-invalid" );

    }
    if($('#quotation').val()===''){
        endLoadingPage();
        $( "#quotation" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#quotation").removeClass( "is-invalid" );

    }
    if($('#proposal').val()===''){
        endLoadingPage();
        $( "#proposal" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#proposal").removeClass( "is-invalid" );

    }
    if($('#segment').val()===''){
        endLoadingPage();
        $( "#segment" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#segment").removeClass( "is-invalid" );
    }


    if($('#bank').val()===''){
        endLoadingPage();
        $( "#bank" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#bank").removeClass( "is-invalid" );

    }

    if($('#tgl_mulai1').val()===''){
        endLoadingPage();
        $( "#tgl_mulai1" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_mulai1").removeClass( "is-invalid" );
    }

    if($('#tgl_akhir2').val()===''){
        endLoadingPage();
        $( "#tgl_akhir2" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_akhir2").removeClass( "is-invalid" );
    }

    if ( $("#no_of_insured").val() === '' ) {
        endLoadingPage();
        $( "#no_of_insured" ).addClass( "is-invalid" );
        return false;
    } else {
        $("#no_of_insured").removeClass( "is-invalid" );
    }
    
    if($('#fee_polis').val()===''){
        // endLoadingPage();
        // $( "#fee_polis" ).addClass( "is-invalid" );
        // return false;

        $("#fee_polis").val("0,00");
    }else{
        $("#fee_polis").removeClass( "is-invalid" );

    }

    if($('#fee_materai').val()===''){
        // endLoadingPage();
        // $( "#fee_materai" ).addClass( "is-invalid" );
        // return false;

        $("#fee_materai").val("0,00");
    }else{
        $("#fee_materai").removeClass( "is-invalid" );

    }

    if($('#fee_admin').val()===''){
        // endLoadingPage();
        // $( "#fee_admin" ).addClass( "is-invalid" );
        // return false;

        $("#fee_admin").val("0,00");
    }else{
        $("#fee_admin").removeClass( "is-invalid" );
    }

    // TAMBAHAN BARU 23 DESEMBER
    if ( $("#premi").val() === '' ) {
        $("#premi").val("0,00");
    }    

    if ( $("#fee_internal").val() === '' ) {
        $("#fee_internal").val("0,00");
    }    

    if ( $("#fee_agent").val() === '' ) {
        $("#fee_agent").val("0,00");
    }    

    if ( $("#tax_amount").val() === '' ) {
        $("#tax_amount").val("0,00");
    }

    // Tambahan 
    if($('#insured_name').val()===''){
        endLoadingPage();
        $( "#insured_name" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#insured_name").removeClass( "is-invalid" );
    }

    if($('#start_date_Policy').val()===''){
        endLoadingPage();
        $( "#start_date_Policy" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#start_date_Policy").removeClass( "is-invalid" );
    }

    if($('#end_date_Policy').val()===''){
        endLoadingPage();
        $( "#end_date_Policy" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#end_date_Policy").removeClass( "is-invalid" );
    }


    if($('#valuta_id').val() == null){
        endLoadingPage();
        $("#valuta_id").parent().addClass('has-error');
        $( "#valuta_id" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#valuta_id").parent().removeClass('has-error');
        $("#valuta_id").removeClass( "is-invalid" );
    }

    if ( !$("#today_kurs").is(":disabled") ) {
        if($('#today_kurs').val()===''){
            endLoadingPage();
            $( "#today_kurs" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#today_kurs").removeClass( "is-invalid" );
        }
    }

    if ( !$("#valas_amount").is(":disabled") ) {
        if($('#valas_amount').val() === ''){
            endLoadingPage();
            $( "#valas_amount" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#valas_amount").removeClass( "is-invalid" );
        }
    }

    // end tambahan
    if ( !$("#insurance").is(":disabled") ) {
        if($('#insurance').val()===''){
            endLoadingPage();
            $( "#insurance" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#insurance").removeClass( "is-invalid" );
        }
    }

    if($('#endorse_reason').val()===''){
        endLoadingPage();
        $( "#endorse_reason" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#endorse_reason").removeClass( "is-invalid" );
    }

    if($('#disc_amount').val()===''){
        // endLoadingPage();
        // $( "#disc_amount" ).addClass( "is-invalid" );
        // $('#sd').show();
        // $('#mi').hide();
        // return false;

        $("#disc_amount").val("0,00");
    }else{

        $("#disc_amount").removeClass( "is-invalid" );
    }

     var a1=$('#insurance').val();
     var a2=a1.replace(/\./g,'');

     var b1=$('#disc_amount').val();
     var b2=b1.replace(/\./g,'');
     var smd=Math.floor(b2) > Math.floor(a2);

     if(smd){
        endLoadingPage();
        $( "#disc_amount" ).addClass( "is-invalid" );
        $('#sd').hide();
        $('#mi').show();
        return false;
    }else{
        $("#disc_amount").removeClass( "is-invalid" );
    }

    if($('#file').val()===''){
    }else{
        var size=$('#file')[0].files[0].size;
        console.log(size);
        var extension=$('#file').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            endLoadingPage();
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').hide();
            $('#mm').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlsx'){
            endLoadingPage();
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').show();
            $('#mm').hide();
            return false;
        }

        $( "#file" ).removeClass( "is-invalid" );

    }

    var _form_data = new FormData(_create_form[0]);
    _form_data.append('coc_no', $('#coc_no').val());
    _form_data.append('id_underwriter', $('#id_underwriter').val());
    _form_data.append('customer', $('#customer').val());
    _form_data.append('product', $('#product').val());
    _form_data.append('officer', $('#officer').val());
    _form_data.append('quotation', $('#quotation').val());
    _form_data.append('proposal', $('#proposal').val());
    _form_data.append('asuransi', $('#asuransi').val());
    _form_data.append('nett_amount', $('#nett_amount').val());
    _form_data.append('no_polis', $('#no_polis').val());

    
    $.ajax({
        type: 'POST',
        url: base_url + '/insert_invoice_baru_endorsement',
        data: _form_data,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (res) {
            
            //var obj = JSON.parse(res); 
            var obj = res;
            console.log(res['data']);
            endLoadingPage();

            if(obj.rc==1){
                znIconbox("Data Berhasil Disimpan",text,action);
            }else{
                swal.fire("Info",obj.rm,"info");
            }    
            
            /*
            swal.fire({
                title: 'Info',
                text: "Berhasil disimpan",
                type: 'success',
                confirmButtonText: 'Tutup',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                     
                    //location.reload();
                }
            });
            */
        }
    }).done(function( res ) {
        var obj = res;
        //var obj = JSON.parse(res); 
        console.log(res['data']);
        endLoadingPage();

        if(obj.rc==1){
            znIconbox("Data Berhasil Disimpan",text,action);
        }else{
            swal.fire("Info",obj.rm,"info");
        }    

       }).fail(function(res) {
        endLoadingPage();
        swal.fire("Error","Terjadi Kesalahan!","error");
    });
}


function convertToRupiah_polis(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c; 


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );  
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');
    
    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');
    
    var c1=polis.val();
    var c2=c1.replace(/\./g,'');
    
    var d1=admin.val();
    var d2=d1.replace(/\./g,'');
    
    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);           


        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
                        

                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
                        

                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });           


    }

    
 }   

}

function convertToRupiah_admin(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c; 


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );  
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');
    
    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');
    
    var c1=polis.val();
    var c2=c1.replace(/\./g,'');
    
    var d1=admin.val();
    var d2=d1.replace(/\./g,'');
    
    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);     
                   
        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
                        

                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
                        

                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });           


    }

    
 }   

}


function convertToRupiah_materai(objek) {

if(objek.value==0){
    objek.value='';
 }else{

separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c; 


var insurance=$('#insurance');
var disc_amount=$('#disc_amount');
var polis=$('#fee_polis');
var admin=$('#fee_admin');
var materai=$('#fee_materai');

if(insurance.val()===''){
    insurance.addClass( "is-invalid" );  
}else{
    insurance.removeClass( "is-invalid" );
    var a1=insurance.val();
    var a2=a1.replace(/\./g,'');
    
    var b1=disc_amount.val();
    var b2=b1.replace(/\./g,'');
    
    var c1=polis.val();
    var c2=c1.replace(/\./g,'');
    
    var d1=admin.val();
    var d2=d1.replace(/\./g,'');
    
    var e1=materai.val();
    var e2=e1.replace(/\./g,'');

    var netamount= a2 - b2 - c2 - d2 - e2;

        $('#nett_amount').val(netamount);

        separator = ".";
        a = $('#nett_amount').val();
        b = a.replace(/[^\d]/g,"");
        c = "";
        panjang = b.length;
        j = 0; for (i = panjang; i > 0; i--) {
           j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
                   c = b.substr(i-1,1) + c; } } $('#nett_amount').val(c);

        var g1=Math.floor(0.02 * netamount);
        $('#tax_amount').val(g1);                

        $.ajax({
                    type: 'GET',
                    url: base_url + '/fee?jml='+netamount+'&company='+$('#is_tax_company').val()+'&jmltax='+g1,
                    success: function (res) {
                        var data = $.parseJSON(res);
                        //console.log(data.fee_agent);
                        $('#fee_agent').val(Math.floor(data.fee_agent));
                        separator = ".";
                         a = $('#fee_agent').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_agent').val(c);
                        

                        $('#fee_internal').val(Math.floor(data.fee_internal));
                        separator = ".";
                         a = $('#fee_internal').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#fee_internal').val(c);
                        

                        $('#premi').val(Math.floor(data.premi));
                        separator = ".";
                         a = $('#premi').val();
                         b = a.replace(/[^\d]/g,"");
                         c = "";
                         panjang = b.length;
                         j = 0; for (i = panjang; i > 0; i--) {
                             j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                                 c = b.substr(i-1,1) + separator + c; } else {
                                     c = b.substr(i-1,1) + c; } } $('#premi').val(c);
                        //data[fee_agent];
                    }
                });           


    }

    
 }   

}


// $( "#no_polis" ).keypress(function() {
//   $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });

//   $.ajax({
//     type: 'GET',
//     url: base_url + '/get_polis?id='+this.value,

//     beforeSend: function () {
//         $("#alertInfo").addClass('d-none');
//         $("#alertError").addClass('d-none');
//         $("#alertSuccess").addClass('d-none');
//         $("#loading").css('display', 'block');
//     },

//     success: function (response) {
        
//         var data = $.parseJSON(response);
//         console.log(data);
//         console.log(data[0]['inv_no']);

//         $("#loading").css('display', 'none');
//         $('#invoice').val(data[0]['inv_no']);
        
//         $('#product').val(data[0]['product_id']);
        
//         $('#officer').val(data[0]['agent_id']);
//         $('#customer').val(data[0]['customer_id']);
        
//         $('#tgl_mulai1').val(data[0]['start_date']);
//         $('#tgl_akhir1').val(data[0]['end_date']);
        
        
//         $('#segment').val(data[0]['segment_id']);
//         $('#bank').val(data[0]['afi_acc_no']);

//         $('#proposal').val(data[0]['proposal_no']);
//         $('#quotation').val(data[0]['qs_no']);

//         }
//     }).done(function (msg) {
//         $("#loading").css('display', 'none');
//     }).fail(function (msg) {
//         $("#loading").css('display', 'none');
//     });
// });


$("#no_polis").keyup(function (e) {
    
    if (e.keyCode === 13) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $.ajax({
        type: 'GET',
        url: base_url + '/get_polis?id=' + $(this).val(),

        beforeSend: function () {
            $("#alertInfo").addClass('d-none');
            $("#alertError").addClass('d-none');
            $("#alertSuccess").addClass('d-none');
            $("#loading").css('display', 'block');
        },

        success: function (response) {
            
            var data = $.parseJSON(response);
            console.log(data);

            if ( data != "" ) {
                console.log(data[0]['inv_no']);
                console.log(data[0]['wf_status_id']);
                console.log(data[0]['paid_status_id']);
                var stsInvoicePaid = [9, 16, 17, 18];

                if ( (data[0]['wf_status_id'] == 9 && data[0]['paid_status_id'] == 0) ||
                     (data[0]['wf_status_id'] == 13 && data[0]['paid_status_id'] == 1) ||
                     ($.inArray(data[0]['wf_status_id'], stsInvoicePaid) != -1 && data[0]['paid_status_id'] == 1)
                    ) {
                    // STATUS INVOICE PAID

                    $("#loading").css('display', 'none');
                    $('#invoice').val(data[0]['inv_no']);
                    
                    $('#product').val(data[0]['product_id']).trigger('change');
                    $('#officer').val(data[0]['agent_id']).trigger('change');

                    $('#customer').val(data[0]['customer_id']).trigger('change');

                    
                    $('#quotation').val(data[0]['qs_no']).trigger('change');
                    $('#proposal').val(data[0]['proposal_no']).trigger('change');
                    $('#segment').val(data[0]['segment_id']).trigger('change');
                    $('#bank').val(data[0]['afi_acc_no']).trigger('change');
                    $('#id_underwriter').val(data[0]['underwriter_id']).trigger('change');

                    // input type hidden
                    $('#product1').val(data[0]['product_id']);
                    $('#officer1').val(data[0]['agent_id']);
                    $('#customer1').val(data[0]['customer_id']);
                    $('#quotation1').val(data[0]['qs_no']);
                    $('#proposal1').val(data[0]['proposal_no']);
                    $('#segment1').val(data[0]['segment_id']);
                    $('#id_underwriter1').val(data[0]['underwriter_id']);
                } else {
                    toastr.warning("Invoice tidak aktif");
                }
                
            } else {
                toastr.warning("Data tidak ditemukan");
            }
            

            }
        }).done(function (msg) {
            $("#loading").css('display', 'none');
        }).fail(function (msg) {
            $("#loading").css('display', 'none');
        });

    }
})
</script>
@stop