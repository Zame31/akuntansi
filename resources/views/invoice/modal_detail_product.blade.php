<div class="modal fade in" id="detail_product" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zn-invoice-tittle">Detail Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body p-5">
                <div class="table-responsive scrollStyle-v" style="padding: 10px;">
                    <div id="tableDiv"></div>
                </div>
            </div>

        </div>
    </div>
</div>
