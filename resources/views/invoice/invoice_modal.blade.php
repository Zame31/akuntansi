<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="border: none;">
            {{-- <div class="modal-header">
                <i class="la la-clipboard zn-icon-modal"></i>
                <h5 class="modal-title" style="margin-top: 10px !important;" id="exampleModalLongTitle">
                    Detail Invoice
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div> --}}
            <div class="modal-body p-0">

                    <div class="kt-invoice-1">
                    <div class="kt-invoice__head" style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                                <div class="kt-invoice__container">
                                    <div class="kt-invoice__brand">
                                        <h1 class="kt-invoice__title">INVOICE</h1>
                                        <div href="#" class="kt-invoice__logo">
                                            <button onclick="cetakView()" type="button" class="btn btn-light btn-hover-info btn-pill">
                                                        <i class="flaticon2-printer"></i> Print & Download</button>
                                            <button data-dismiss="modal" aria-label="Close" type="button"
                                                class="btn btn-light btn-hover-info btn-pill">Kembali</button>
                                            {{-- <a href="#" onclick="kembali();" class="btn btn-pill btn-primary">Kembali</a> --}}
                                        </div>
                                    </div>
                                    <div class="kt-invoice__items">
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">INVOICE DATE.</span>
                                            <span class="kt-invoice__text">20 September 2019</span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                            <span class="kt-invoice__text">GS 000014</span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">PRODUCT NAME.</span>
                                            <span class="kt-invoice__text">Fredrick Nebraska 20620</span>
                                        </div>
                                    </div>
                                    <div class="kt-invoice__items">
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">OFFICER NAME.</span>
                                            <span class="kt-invoice__text">Zamzam Nurzaman</span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">START DATE COVERAGE.</span>
                                            <span class="kt-invoice__text">20 September 2019</span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">END DATE COVERAGE.</span>
                                            <span class="kt-invoice__text">20 September 2019</span>
                                        </div>
                                    </div>
                                    <div class="kt-invoice__items zn-desc-invoice">
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">QUOTATION.</span>
                                            <span class="kt-invoice__text">Lorem, ipsum dolor sit amet consectetur
                                                adipisicing elit.</span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">PROPOSAL.</span>
                                            <span class="kt-invoice__text">Lorem, ipsum dolor sit amet consectetur
                                                adipisicing elit.</span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">SEGMENT.</span>
                                            <span class="kt-invoice__text">Lorem, ipsum dolor sit amet consectetur
                                                adipisicing elit.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-invoice__body">
                                <div class="kt-invoice__container">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>DESCRIPTION</th>
                                                    <th>AMOUNT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Insurance</td>
                                                    <td>Rp. 200.000</td>
                                                </tr>
                                                <tr>
                                                    <td>Disc</td>
                                                    <td>Rp. 100.000</td>
                                                </tr>
                                                <tr>
                                                    <td>Nett</td>
                                                    <td>Rp. 300.000</td>
                                                </tr>
                                                <tr>
                                                    <td>Fee Agent</td>
                                                    <td>Rp. 300.000</td>
                                                </tr>
                                                <tr>
                                                    <td>Fee Internal</td>
                                                    <td>Rp. 300.000</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-invoice__footer">
                                <div class="kt-invoice__container">
                                    <div class="kt-invoice__bank">
                                        <div class="kt-invoice__title">BANK TRANSFER</div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__label">Account Name:</span>
                                            <span class="kt-invoice__value">Barclays UK</span></span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__label">Account Number:</span>
                                            <span class="kt-invoice__value">1234567890934</span></span>
                                        </div>
                                    </div>
                                    <div class="kt-invoice__total">
                                        <span class="kt-invoice__title">PREMI AMOUNT</span>
                                        <span class="kt-invoice__price">Rp 20.600.000</span>
                                    </div>
                                </div>
                            </div>
                        </div>

            </div>
        </div>
    </div>
</div>
