@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Sales </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Paid Debit Note </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Paid Debit Note
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                    <div class="row">
                        <div class="col-12">
                            @if (Auth::user()->user_role_id == 1)
                                <a href="#" onclick="reversal();" class="btn btn-pill btn-danger" style="color: white;">Reversal Selected</a>

                            @endif
                         </div>
                    </div>
                        {{-- <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </a>
                        </div> --}}
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                        <thead>
                            <tr>
                                <th width="30px">
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="" id="example-select-all" class="kt-group-checkable">
                                        <span></span>
                                    </label>
                                </th>
                                <th width="30px">Action</th>
                                <th>ID</th>
                                <th>Branch Code</th>
                                <th>Customer Name</th>
                                <th>Product Name</th>
                                <th>Valuta</th>
                                <th>Split Amount (IDR) </th>
                                <th>Paid Status</th>
                                <th>Police Number</th>
                                <th>Status Agent Fee (IDR) </th>
                                <th>Status Commision (IDR) </th>
                                <th>Status Premium (IDR) </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total_premi = 0;
                                $total_agent = 0;
                                $total_comm = 0;
                                $total_status_premi = 0;
                                $total_split = 0;
                            @endphp
                            @if($data)
                            @foreach($data as $item)
                                @php
                                $set_premi = 0;
                                $set_agent = 0;
                                $set_admin = 0;



                                    if($item->is_premi_paid==null){
                                        $total_premi += $item->premi_amount + $item->polis_amount + $item->materai_amount;
                                        $set_premi = $item->premi_amount + $item->polis_amount + $item->materai_amount;
                                    }
                                    if($item->is_agent_paid==null){
                                        $total_agent += $item->agent_fee_amount;
                                        $set_agent = $item->agent_fee_amount;
                                    }

                                    if($item->is_company_paid==null){
                                        $total_comm += $item->comp_fee_amount+$item->admin_amount;
                                        $set_admin = $item->comp_fee_amount+$item->admin_amount;
                                    }



                                    $split_amount = $set_premi + $set_agent + $set_admin;

                                    // dd($set_agent);
                                    $total_split += $split_amount;
                                    $total_status_premi += $item->ins_fee;
                                @endphp

                            <tr>
                                <td>
                                    @php
                                        if ($item->d2 == 'n') {
                                            $item->is_agent_paid = 99;
                                        }
                                        if ($item->d3 == 'n') {
                                            $item->is_company_paid = 99;
                                        }
                                        if ($item->d1 == 'n') {
                                            $item->is_premi_paid = 99;
                                        }
                                    @endphp
                                    @if($item->is_agent_paid==1 || $item->is_company_paid==1 || $item->is_premi_paid==1 || $item->is_agent_paid=='0' || $item->is_company_paid=='0' || $item->is_premi_paid=='0')

                                    @else

                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="{{$item->id}}" class="kt-group-checkable">
                                            <span></span>
                                    </label>
                                    @endif
                                </td>
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#" onclick="detail_invoice({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Detail</a>
                                                 <a class="dropdown-item" href="#" onclick="claim('{{$item->polis_no}}')"><i
                                                    class="la la-clipboard"></i> Claim</a>
                                            <a class="dropdown-item" href="#" onclick="notes({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Memo</a>

                                            <!--
                                            <a class="dropdown-item" href="#" onclick="claim({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Claim</a>
                                            -->



                                            @if($item->is_agent_paid==1 && $item->d2=='t')
                                                <a href="#" onclick="rev_split('agent',{{$item->id}});" class="dropdown-item">Reversal Agent Fee</a>
                                            @endif

                                            @if($item->is_company_paid==1 && $item->d3=='t')
                                             <a href="#" onclick="rev_split('commision',{{$item->id}});" class="dropdown-item">Reversal Commission</a>
                                            @endif

                                            @if($item->is_premi_paid==1 && $item->d1=='t')
                                            <a href="#" onclick="rev_split('premium',{{$item->id}});" class="dropdown-item">Reversal Premium</a>
                                            @endif

                                            <!--
                                            <a href="#" onclick="paid1('all',{{$item->id}});" class="dropdown-item">Reversal All</a>
                                            -->

                                            @if(isset($item->is_agent_paid) && isset($item->is_company_paid) && isset($item->is_premi_paid))



                                            @else
                                            @if (Auth::user()->user_role_id == 1)
                                                @if(!isset($item->is_agent_paid))
                                                    <a href="#" onclick="paid1('agent',{{$item->id}});" class="dropdown-item">Paid Agent Fee</a>
                                                @endif

                                                @if(!isset($item->is_company_paid))
                                                <a href="#" onclick="paid1('commision',{{$item->id}});" class="dropdown-item">Paid Commission</a>
                                                @endif

                                                @if(!isset($item->is_premi_paid))
                                                <a href="#" onclick="paid1('premium',{{$item->id}});" class="dropdown-item">Paid Premium</a>
                                                @endif

                                                <a href="#" onclick="paid1('all',{{$item->id}});" class="dropdown-item">Paid All</a>
                                                @endif
                                            @endif


                                        </div>
                                    </div>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->short_code}}</td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->definition}}</td>
                                <td>{{$item->mata_uang}}</td>
                                {{-- @if($item->valuta_id==1)
                                <td style="text-align: right;">{{number_format($item->premi_amount,2,',','.')}}</td>
                                @else
                                <td style="text-align: right;">{{number_format($item->premi_amount,2,',','.')}}</td>
                                @endif --}}

                                <td style="text-align: right;">
                                    {{number_format($split_amount,2,',','.')}}

                                </td>

                                <td>{{$item->paid}}</td>
                                <td>{{$item->polis_no}} </td>
                                <td style="text-align: right;" nowrap>
                                    @if(!isset($item->is_agent_paid))
                                      {{number_format($set_agent,2,',','.')}}
                                    @elseif($item->is_agent_paid==1)
                                        <span class="kt-badge kt-badge--success kt-badge--inline">paid</span>
                                    @elseif($item->is_agent_paid==99)
                                        <span class="kt-badge kt-badge--success kt-badge--inline">Direct Paid</span>
                                    @elseif($item->is_agent_paid==2)
                                        <span class="kt-badge kt-badge--danger kt-badge--inline">reversal</span>
                                    @else
                                    <span class="kt-badge kt-badge--danger kt-badge--inline">waiting approval</span>
                                    @endif
                                </td>
                                <td style="text-align: right;" nowrap>
                                    @if(!isset($item->is_company_paid))
                                    {{number_format($set_admin,2,',','.')}}
                                    @elseif($item->is_company_paid==1)
                                            <span class="kt-badge kt-badge--success kt-badge--inline">paid</span>
                                         {{-- <span class="kt-badge kt-badge--success kt-badge--inline">paid</span> --}}
                                    @elseif($item->is_company_paid==99)
                                        <span class="kt-badge kt-badge--success kt-badge--inline">Direct Paid</span>
                                    @elseif($item->is_company_paid==2)
                                        <span class="kt-badge kt-badge--danger kt-badge--inline">reversal</span>
                                    @else
                                    <span class="kt-badge kt-badge--danger kt-badge--inline">waiting approval</span>
                                    @endif
                                </td>
                                <td style="text-align: right;" nowrap>
                                    @if(!isset($item->is_premi_paid))
                                    {{number_format($set_premi,2,',','.')}}
                                    @elseif($item->is_premi_paid==1)
                                            <span class="kt-badge kt-badge--success kt-badge--inline">paid</span>

                                    @elseif($item->is_premi_paid==99)
                                        <span class="kt-badge kt-badge--success kt-badge--inline">Direct Paid</span>
                                    @elseif($item->is_premi_paid==2)
                                        <span class="kt-badge kt-badge--danger kt-badge--inline">reversal</span>
                                    @else
                                    <span class="kt-badge kt-badge--danger kt-badge--inline">waiting approval</span>
                                    @endif
                                </td>

                            </tr>

                            @endforeach


                            @endif

                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="text-align: right;">{{number_format($total_split,2,',','.')}} </th>
                                <th></th>
                                <th></th>
                                <th style="text-align: right;"> {{number_format($total_agent,2,',','.')}} </th>
                                <th style="text-align: right;">{{number_format($total_comm,2,',','.')}}</th>
                                <th style="text-align: right;">{{number_format($total_premi,2,',','.')}}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>


        </div>




        <!-- end:: Subheader -->

    </div>
</div>
@include('invoice.modal_notes')

<script type="text/javascript">



    $('#example-select-all').click(function (e) {
        $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
    });


    function formatDate(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    function drop_selected() {
        let value = [];
        $('input[type="checkbox"]').each(function () {
            if (this.checked) {
                value.push(this.value);
            }
        });

        if (value.length == 0) {
            swal.fire("Peringatan!", "Minimal pilih satu", "warning");
        } else {
            swal.fire({
                title: "Informasi",
                text: "Anda yakin akan menghapus transaksi?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#e6b034",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (result) {
                if (result.value) {
                    loadingPage();
                    $.ajax({
                        type: 'POST',
                        url: base_url + '/drop_selected',
                        data: {
                            value: value
                        },
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function (result) {
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function (res) {
                        endLoadingPage();

                    }).fail(function (res) {
                        endLoadingPage();
                        swal.fire("Error", "Terjadi Kesalahan!", "error");
                    });
                }
            });
        }
    }

    function paid(id) {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan realisasi jurnal split payment?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/split_paid?type=' + id,
                        data: {value: value},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
    }
}


function paid1(id,idinv) {
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan realisasi jurnal split payment?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/split_paid?type=' + id +'&id='+idinv,
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
}

function reversal() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan reversal payment?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/reversal_payment',
                        data: {value: value , type:'payment'},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
    }
}

function rev_split(id,idinv) {
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan reversal split payment?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/rev_split?type=' + id +'&id='+idinv,
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
}

</script>

@include('invoice.action')
@include('transaksi.invoice_modal')
@stop
