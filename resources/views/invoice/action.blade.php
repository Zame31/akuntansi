<script>
function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}


function detail_invoice(id){
    loadingPage();
    $.ajax({
        type: 'GET',
        url: base_url + 'detail_invoice?id='+id,
        success: function (res) {
            var data = $.parseJSON(res);
            console.log(data);
            endLoadingPage();

            $('#zn_stat').val(data[0].wf_status_id);
            $('#zn_paid_stat').val(data[0].paid_status_id);

            var getWf = data[0].wf_status_id;
            var getPaid = data[0].paid_status_id;

            if (getPaid == 0) {
                $("#setWord").attr("href", base_url+'printwordSc/normal/aktif/'+id);
            }else {
                $("#setWord").attr("href", base_url+'printwordSc/normal/paid/'+id);
            }


        $('#tgl_tr').html(formatDate(new Date(data[0].inv_date)));
            $('#inv_no').html(data[0].inv_no);
            $('#product').html(data[0].produk);
            $('#agent').html(data[0].agent);
            $('#start_date').html(formatDate(new Date(data[0].start_date)));
            $('#end_date').html(formatDate(new Date(data[0].end_date)));
            $('#quotation').html(data[0].quotation);
            $('#proposal').html(data[0].proposal);
            $('#segment').html(data[0].segment);
            $('#wf_status').html("Status "+data[0].wf_status);
            $('#user_maker').html(data[0].user_create);
            $('#user_approval').html(data[0].user_approval);
            $('#ins_amount').html(formatMoney(data[0].ins_amount));
            $('#premi_amount1').html(formatMoney(data[0].premi_amount));
            $('#disc_amount').html(formatMoney(data[0].disc_amount));
            $('#net_amount').html(formatMoney(data[0].net_amount));
            $('#agent_fee_amount').html(formatMoney(data[0].agent_fee_amount));
            $('#comp_fee_amount').html(formatMoney(data[0].comp_fee_amount));

            $('#premi_amount').html(formatMoney(data[0].premi_amount));

            // NEW
            $('#stamp_duty').html(formatMoney(data[0].materai_amount));
            $('#police_duty').html(formatMoney(data[0].polis_amount));
            $('#adm_duty').html(formatMoney(data[0].admin_amount));
            // NEW

            $('#bank').html(data[0].bank);
            $('#zn_stat').val(data[0].id_workflow);
            $('#zn_paid_stat').val(data[0].paid_status_id);
            $('#get_id').val(data[0].id);

            // CETAK AKTIF
            if (data[0].inv_no == null) {
                $('#ca_inv').html(' NO: ');
            }else {
                $('#ca_inv').html(' NO: '+data[0].inv_no);
            }
            $('#ca_address').html(data[0].c_address);
            $('#ca_product').html(': '+data[0].produk);
            $('#ca_insured').html(': '+data[0].full_name);
            $('#ca_polis').html(': '+data[0].polis_no);
            $('#ca_underwriter').html(': '+data[0].underwriter);
            var new_periode = formatDateNew(new Date(data[0].start_date));
            var end_periode = formatDateNew(new Date(data[0].end_date));
            $('#ca_periode').html(': '+new_periode+' to '+end_periode);

            if (data[0].valuta_id != 1) {
               premi = premi/data[0].kurs_today;
               new_adm = (new_adm/data[0].kurs_today).toFixed(2);
               total_fix = (total_fix/data[0].kurs_today).toFixed(2);
               $('#ca_sumins').html(': '+data[0].mata_uang+' '+ formatMoney(data[0].ins_amount/data[0].kurs_today));
               $('.mata_uang_set').html(data[0].mata_uang);


           }else {
               $('.mata_uang_set').html(data[0].mata_uang);
               $('#ca_sumins').html(': '+data[0].mata_uang+' '+ formatMoney(data[0].ins_amount));

           }
            //$('#ca_sumins').html(': '+ formatMoney(data[0].ins_amount));

            //var premi = parseFloat(data[0].premi_amount);
            if(data[0].valuta_id!=1){
                var premi = parseFloat(data[0].premi_amount/data[0].kurs_today);
            }else{
                var premi = parseFloat(data[0].premi_amount);
            }

            // console.log(premi);

            premi = premi.toFixed(2);



            $('#ca_premi_amount').html(premi.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

            $('#ca_user_approval').html(data[0].user_approval);
            $('#ca_com').html(data[0].full_name);
            $('#ca_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
            $('#ca_ttd').html(data[0].leader_name);
            $('#ca_ttd_jab').html(data[0].jabatan);

            if(data[0].valuta_id!=1){
                var new_adm = (parseInt(data[0].polis_amount) + parseInt(data[0].materai_amount)) / data[0].kurs_today;
            }else{
                var new_adm = parseInt(data[0].polis_amount) + parseInt(data[0].materai_amount);
            }



            new_adm = new_adm.toFixed(2);

            $('#ca_adm').html(new_adm.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

            if(data[0].valuta_id!=1){
                var new_total =  parseFloat(new_adm) + parseFloat(premi);
            }else{
                var new_total =  parseFloat(new_adm) + parseFloat(premi);
            }



            var sisa_desimal = 0;


            if ((new_total % 1) > 0.5) {
                sisa_desimal = (1.00 - (new_total % 1).toFixed(2)).toFixed(2);
                new_total = Math.ceil(new_total);

            }else {
                sisa_desimal = (new_total % 1).toFixed(2);

                new_total = Math.floor(new_total);


            }

            $('#sisa_desimal').html("("+sisa_desimal+")");

            // console.log(Math.ceil(new_total));
            // console.log(Math.floor(new_total));


            new_total = new_total.toFixed(2);
           var total_fix = Math.round(new_total);
           var total_terbilang_fix = Math.round(new_total);



            $('#ca_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#ca_grand_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#ca_terbilang').html('In Words (Indonesia) : # '+terbilang(total_fix)+' '+data[0].deskripsi);

            // CETAK PAID

            if(data[0].valuta_id!=1){
                var new_premi = data[0].net_amount/data[0].kurs_today;
            }else{
                var new_premi = data[0].net_amount;
            }

            if ((new_premi % 1) > 0.5) {
                new_premi = Math.ceil(new_premi);

            }else {
                new_premi = Math.floor(new_premi);
            }



            if (data[0].kwitansi_no == null) {
                $('#pa_inv').html(' NO: ');
            }else {
                $('#pa_inv').html(' NO: '+data[0].kwitansi_no);
            }

            var premi_des = new_premi.toFixed(2);

            console.log(data);


            $('#pa_agent').html(data[0].polis_no+' - '+data[0].full_name);
            $('#pa_product').html(data[0].produk);

            $('#pa_premi_amount').html(data[0].mata_uang+' '+total_fix.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

            $('#pa_user_approval').html(data[0].user_approval);
            $('#pa_terbilang').html(terbilang(total_terbilang_fix) +' '+ data[0].deskripsi);
            $('#pa_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
            $('#pa_ttd').html(data[0].leader_name);
            $('#pa_ttd_jab').html(data[0].jabatan);



            if (data[0].valuta_id != 1) {

               $('#zn-tittle-premi-lang').html('GROSS PREMIUM');

               $('#tittle_lang').html(data[0].mata_uang+' AMOUNT');
               $('#ins_amount_lang').html(formatMoney(data[0].ins_amount/data[0].kurs_today));
               $('#premi_amount1_lang').html(formatMoney(data[0].premi_amount/data[0].kurs_today));
               $('#disc_amount_lang').html(formatMoney(data[0].disc_amount/data[0].kurs_today));
               $('#net_amount_lang').html(formatMoney(data[0].net_amount/data[0].kurs_today));
               $('#agent_fee_amount_lang').html(formatMoney(data[0].agent_fee_amount/data[0].kurs_today));
               $('#comp_fee_amount_lang').html(formatMoney(data[0].tax_amount/data[0].kurs_today));
               $('#premi_amount_lang').html(formatMoney(data[0].premi_amount/data[0].kurs_today));
                // NEW
               $('#stamp_duty_lang').html(formatMoney(data[0].materai_amount/data[0].kurs_today));
               $('#police_duty_lang').html(formatMoney(data[0].polis_amount/data[0].kurs_today));
               $('#adm_duty_lang').html(formatMoney(data[0].admin_amount/data[0].kurs_today));
                // NEW
           }
            $('#detail').modal('show');




        }
    });
}

function claim(id){
    loadingPage();
    window.location.href = base_url +'claim?id=' + id;
    endLoadingPage();
}

function notes(id){
     var table = $('#notes_table').DataTable({
                        aaSorting: [],
                        destroy: true,
                        processing: true,
                        serverSide: true,
                        columnDefs: [
                            { "orderable": false, "targets": 0 }],
                        ajax: {
                            "url" : base_url + '/history_notes?id=' + id,
                            "error": function(jqXHR, textStatus, errorThrown)
                                {
                                    toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                                }
                            },
                        columns: [
                            { data: 'ref_code_type', name: 'ref_code_type' },
                            { data: 'fullname', name: 'fullname' },
                            { data: 'created_at', name: 'created_at' },
                            { data: 'notes', name: 'notes' },

                        ]
                    });
   $('#modal-notes').modal('show');
}
</script>
