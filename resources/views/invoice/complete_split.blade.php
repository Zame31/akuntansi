@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Sales </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Complete Payment </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Complete Payment
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">

                    {{-- <div class="row">
                        <div class="col-12">
                            @if (Auth::user()->user_role_id == 1)
                            <a href="#" onclick="reversal();" class="btn btn-pill btn-danger" style="color: white;">Reversal Selected</a>
                            @endif
                        </div>
                    </div> --}}
                        {{-- <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </a>
                        </div> --}}
                    </div>
            </div>
            <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    {{-- <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable"
                                                id="example-select-all">
                                            <span></span>
                                        </label>
                                    </th> --}}
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Branch Code</th>
                                    <th>Customer Name</th>
                                    <th>Product Name</th>
                                    <th>Valuta</th>
                                    <th>Premi Amount</th>
                                    <th>Paid Status</th>
                                    <th>Police Number</th>
                                    <th>Status Agent Fee</th>
                                    <th>Status Commision</th>
                                    <th>Status Premium</th>
                                    <th>Dokumen</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if($data)
                            @foreach($data as $item)
                            <tr>
                                {{-- <td>
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="{{$item->id}}" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </td> --}}
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#" onclick="detail_invoice({{$item->id}})"><i
                                                class="la la-clipboard"></i> Detail</a>
                                                {{-- @if (Auth::user()->user_role_id == 1)
                                                    <a class="dropdown-item" href="#" onclick="claim('{{$item->polis_no}}')"><i
                                                    class="la la-clipboard"></i> Claim</a>
                                                @endif --}}
                                            <a class="dropdown-item" href="#" onclick="notes({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Memo</a>
{{--

                                                @if (Auth::user()->user_role_id == 1)
                                                    @if(($item->is_agent_paid==1 && $item->d2=='t') || ($item->is_agent_paid==4 && $item->d2=='t'))
                                                        <a href="#" onclick="rev_split('agent',{{$item->id}});" class="dropdown-item">Reversal Agent Fee</a>
                                                    @endif

                                                    @if( ($item->is_company_paid==1 && $item->d3=='t') || ($item->is_company_paid==4 && $item->d3=='t'))
                                                    <a href="#" onclick="rev_split('commision',{{$item->id}});" class="dropdown-item">Reversal Commission</a>
                                                    @endif

                                                    @if( ($item->is_premi_paid==1 && $item->d1=='t') || ($item->is_premi_paid==4 && $item->d1=='t'))
                                                    <a href="#" onclick="rev_split('premium',{{$item->id}});" class="dropdown-item">Reversal Premium</a>
                                                    @endif
                                                @endif --}}
                                        </div>
                                    </div>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->short_code}}</td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->definition}}</td>
                                <td>{{$item->mata_uang}}</td>
                                @if($item->valuta_id==1)
                                <td>{{number_format($item->premi_amount,2,',','.')}}</td>
                                @else
                                <td>{{number_format($item->premi_amount/$item->kurs_today,2,',','.')}}</td>
                                @endif

                                <td>{{$item->paid}}</td>
                                <td>{{$item->polis_no}}</td>
                                <td>
                                    @if($item->is_agent_paid==2)
                                    <span class="kt-badge kt-badge--danger kt-badge--inline">reversal</span>
                                     @else
                                    Paid
                                    @endif
                                </td>
                                <td>
                                    @if($item->is_company_paid==2)
                                    <span class="kt-badge kt-badge--danger kt-badge--inline">reversal</span>

                                    @else
                                    Paid
                                    @endif
                                </td>
                                <td>
                                    @if($item->is_premi_paid==2)
                                    <span class="kt-badge kt-badge--danger kt-badge--inline">reversal</span>
                                    @else
                                    Paid
                                    @endif
                                </td>
                                <td>
                                    @if($item->url_dokumen)
                                    <a target="_blank" href="{{asset('upload_invoice')}}/{{$item->id}}_{{$item->url_dokumen}}">Download File</a>
                                    @else
                                    -
                                    @endif
                                </td>

                            </tr>
                            @endforeach
                            @endif

                        </tbody>
                        </table>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('invoice.modal_notes')
<script type="text/javascript">
    $('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function drop_selected() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan menghapus transaksi?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/drop_selected',
                        data: {value: value},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
    }
}
function reversal() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan reversal payment?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/reversal_payment',
                        data: {value: value,type:'split'},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
    }
}
function rev_split(id,idinv) {
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan reversal split payment?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/rev_split?type=' + id +'&id='+idinv,
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
}
</script>

@include('invoice.action')
@include('transaksi.invoice_modal')
@stop
