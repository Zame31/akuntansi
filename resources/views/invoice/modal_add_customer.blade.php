<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal_add_customer" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                @php
                     $cust_type = \DB::select('SELECT * FROM ref_cust_type where is_active=true order by seq asc');
                    $agent = \DB::select('SELECT * FROM ref_agent where is_active=true AND is_parent=true order by seq asc');
                    $cust = \DB::select('SELECT * FROM ref_cust_group');
                @endphp
                <form id="add_customer">
                    <div class="form-group">
                        <label for="exampleTextarea">Full Name</label>
                        <input type="text" class="form-control" onkeypress="return huruf(event)" id="full_name" name="full_name" maxlength="100">
                        <div class="invalid-feedback" id="mm">Silahkan isi full name</div>
                        <div class="invalid-feedback" id="ck">Maximal 100 karakter</div>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Short Name</label>
                        <input type="text" class="form-control" id="short_name" onkeypress="return huruf(event)" name="short_name" maxlength="50">
                        <div class="invalid-feedback" id="mm1">Silahkan isi short name</div>
                        <div class="invalid-feedback" id="ck1">Maximal 50 karakter</div>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Customer Type</label>
                        <select class="form-control kt-select2 init-select2" name="cust_type" id="cust_type">
                            <option value="">Silahkan Pilih</option>
                            @foreach($cust_type as $item)
                            <option value="{{$item->id}}">{{$item->definition}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Account Officer</label>
                        <select class="form-control kt-select2 init-select2" name="agent" id="agent">
                            <option value="">Silahkan Pilih</option>
                            @foreach($agent as $item)
                            <option value="{{$item->id}}">{{$item->full_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Customer Group</label>
                        <select class="form-control kt-select2 init-select2" name="cust_group" id="cust_group">
                            <option value="">Silahkan Pilih</option>
                            @foreach($cust as $item)
                            <option value="{{$item->id}}">{{$item->group_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button onclick="simpan_customer()" type="button" class="btn btn-success">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

$(document).ready(function () {
    $("#add_customer").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            full_name: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            short_name: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            cust_type: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            agent: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
            cust_group: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});
$('#group_id').val(null).trigger('change.select2');
    function simpan_customer(){
        var validateProduk = $('#add_customer').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {

            loadingPage();

       var formData = document.getElementById("add_customer");
       var _form_data = new FormData(formData);

           $.ajax({
               type: 'POST',
               url: base_url + 'insert_customer_baru',
               data: _form_data,
               processData: false,
               contentType: false,
               beforeSend: function () {
                   loadingPage();
               },
               success: function (res) {
                    $('#modal_add_customer').modal('hide');
                    getCustomerList();

                   endLoadingPage();
                   _create_form[0].reset();

                   swal.fire({
                       title: 'Info',
                       text: "Customer Berhasil Disimpan",
                       type: 'info',
                       confirmButtonText: 'Tutup',
                       reverseButtons: true
                   }).then(function(result){
                       if (result.value) {


                       }
                   });

               }
           });
        }



    }

    function getCustomerList() {
        $.ajax({
            type: 'GET',
            url: '{{route('c.get_data_customer')}}',
            beforeSend: function () {
            },
            success: function (msg) {
                $('#customer').html(``);
                $('#customer').append(`<option value="">Pilih Customer</option>`);
                $.each(msg.rm, function (k,v) {
                    $('#customer').append(`<option value="`+v.id+`">`+v.full_name+`</option>`);
                });
            }

        }).done(function (msg) {
        }).fail(function (msg) {
            toastr.info("Gagal, Koneksi ke server bermasalah");
        });
    }
</script>
