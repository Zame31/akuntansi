<html>
    <head>
        <link href="{{asset('css/myStyle.css')}}" rel="stylesheet" type="text/css" />
         <style>
            * {
                font-family: sans-serif;
                box-sizing: border-box;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 14px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }

            .kt-font-danger {
    color: #ff5987 !important;
}
            
.kt-font-bold {
  font-weight: 500 !important; 
}
a {
    color: #5867dd;
    text-decoration: none;
    background-color: transparent;
}


.pl-4,
.px-4 {
  padding-left: 1.5rem !important; }

.col-6 {
    /* -webkit-box-flex: 0;
    -ms-flex: 0 0 20%;
    flex: 0 0 20%;
    max-width: 20%; */
}
.ml-3,
.mx-3 {
  margin-left: 1rem !important; }

.kt-font-success {
  color: #1dc9b7 !important; }

  /* .row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -10px;
    margin-left: -10px;
} */


.pr-4, .px-4 {
    padding-right: 1.5rem !important;
}
.col-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}

        </style> 
    </head>
    <title> Profit And Loss </title>
    <body>
        @php
            $system_date = collect(\DB::select("select * from ref_system_date"))->first();
        @endphp
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        {{-- <div style="display: inline-block;">
                                            <img src="{{asset('img/logo-hd-soeryo-indonesia-gemilang.png')}}" style="width: 170px;">
                                        </div> --}}
                                        <div style="display: inline-block;">
                                            @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                            <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                        </div>
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">PROFIT AND LOSS REPORT</span>
                                            <span style="display: block;font-size:14px;">{{date('d F Y',strtotime($system_date->last_date))}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 10px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 10px;">
                          
                            {{-- <div id="wrapper">
                                <div id="content">Column 1 (fluid)  </div>
                                <div id="sidebar">Column 2 (fixed)</div>
                                <div id="cleared"></div>
                              </div> --}}

                            <div class="rowss">
                                <div class="column">
                                    <div class="kt-widget6">
                                        <div class="kt-widget6__body">
                                            @if($laba_rugi)
                                            @foreach($laba_rugi as $key => $item)
            
                                            @if($item->balance_type_id==0)
            
                                            @php
                                            $awal=strlen($item->coa_no);
                                            @endphp
                                            @if($awal==3)
                                            @php
                                            $tot_db=0;
                                            @endphp
                                            @foreach($laba_rugi as $key => $tot)
                                            @php
                                            if($tot->coa_type_id==3){
                                            if(strlen($tot->coa_no)==6){
                                            $tot_db+=$tot->last_os;
                                            }
        
                                            }
                                            @endphp
                                            @endforeach
                                            <div class="zn-item " style="font-size:12px;padding: 8px;background: #f7f7f7;border-bottom: 1px dashed #ebedf2; ">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td width="50%">
                                                            <span class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                                        </td>
                                                        <td width="25%"></td>
                                                        <td width="25%" style="text-align: right;">
                                                            
                                                            <span class="kt-font-success kt-font-bold">Rp
                                                                {{number_format($tot_db,0,',','.')}}</span>
                                                        </td>
                                                    </tr>
                                                </table>       
                                            </div>

                                            @else
                                            <div class="zn-item ml-3" style="font-size:12px;padding: 8px;border-bottom: 1px dashed #ebedf2;">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td width="50%">
                                                            <span><a href="#">{{$item->coa_no}} -
                                                            {{$item->coa_name}}</a></span>
                                                        </td>
                                                        <td width="25%"></td>
                                                        <td width="25%" style="text-align: right;">
                                                            <span class="kt-font-success kt-font-bold" style="text-align:right;">Rp
                                                                {{number_format($item->last_os,0,',','.')}}</span>
                                                        </td>
                                                    </tr>
                                                </table>                                             
                                           
                                            </div>
                                            
                                            @endif
            
                                            @else
            
                                            @php
                                            $awal=strlen($item->coa_no);
                                            @endphp
                                            
                                            @if($awal==3)

                                            @php
                                            $tot_kr=0;
                                            @endphp
                                            @foreach($laba_rugi as $tot)
                                            @php
                                            if($tot->coa_type_id==4){
                                            if(strlen($tot->coa_no)==6){
        
                                            $tot_kr+=$tot->last_os;
        
                                            }
        
                                            }
                                            @endphp
                                            @endforeach

                                            <div class="zn-item " style="font-size:12px;padding: 8px;background: #f7f7f7;border-bottom: 1px dashed #ebedf2; ">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td width="50%">
                                                            <span class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                                        </td>
                                                        <td width="25%" style="text-align: right;">
                                                            
                                                            <span class="kt-font-danger kt-font-bold text-right">Rp
                                                                {{number_format($tot_kr,0,',','.')}}</span>
                                                            <span></span>
                                                        </td>
                                                        <td width="25%"></td>
                                                    </tr>
                                                </table>       
                                            </div>


                                           
                                            @elseif($awal==6)
                                            <div class="zn-item " style="font-size:12px;padding: 8px;background: #f7f7f7;border-bottom: 1px dashed #ebedf2; ">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td width="50%">
                                                            <span class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                                        </td>
                                                        <td width="25%" style="text-align: right;">
                                                            
                                                            <span class="kt-font-danger kt-font-bold text-right">Rp
                                                                {{number_format($item->last_os,0,',','.')}}</span>
                                                            <span></span>
                                                        </td>
                                                        <td width="25%"></td>
                                                    </tr>
                                                </table>       
                                            </div>

            
                                            @else

                                            <div class="zn-item " style="font-size:12px;padding: 8px;border-bottom: 1px dashed #ebedf2; ">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td width="50%">
                                                            <a class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</a>
                                                        </td>
                                                       
                                                        <td width="25%" style="text-align: right;">
                                                            
                                                            <span class="kt-font-danger kt-font-bold text-right">Rp
                                                                {{number_format($item->last_os,0,',','.')}}</span>
                                                            <span></span>
                                                        </td>
                                                        <td width="25%"></td>
                                                    </tr>
                                                </table>       
                                            </div>

                                            @endif
            
            
                                            @endif
                                            @endforeach
                                            @endif
                                            @php
                                            $tot_laba=0;
                                            $tot_rugi=0;
                                            @endphp
                                            @if($laba_rugi)
                                            @foreach($laba_rugi as $tot)
                                            @php
                                            if($tot->coa_type_id==3){
                                            if(strlen($tot->coa_no)==6){
                                            $tot_laba+=$tot->last_os;
                                            }
            
                                            }else{
                                            if(strlen($tot->coa_no)==6){
                                            $tot_rugi+=$tot->last_os;
                                            }
            
                                            }

                                            $dataPajak = \DB::table('master_coa')
                                                    ->where('coa_parent_id', 600)
                                                    ->where('company_id', \Auth::user()->company_id)
                                                    ->where('branch_id', \Auth::user()->branch_id)
                                                    ->get();


                                            $totalPajak = 0;
                                                foreach ($dataPajak as $item) {
                                                    if ( is_null($item->last_os) ) {
                                                        $item->last_os = 0;
                                                    }

                                                    $totalPajak += $item->last_os;
                                                }
                                            @endphp
                                            @endforeach
                                            @endif
                                            <div class="zn-item " style="font-size:12px;padding: 8px;background: #f7f7f7;border-bottom: 1px dashed #ebedf2; ">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td width="50%">
                                                            <span class=" kt-font-bold">Profit and Loss (Before Tax)</span>
                                                        </td>
                                                        <td width="25%"></td>
                                                        <td width="25%" style="text-align: right;">

                                                            @if($tot_laba - $tot_rugi < 0) <span class="kt-font-danger kt-font-bold">Rp
                                                                {{number_format($tot_laba - $tot_rugi,0,',','.')}}</span>
                                                                @else
                                                                <span class="kt-font-success kt-font-bold">Rp
                                                                    {{number_format($tot_laba - $tot_rugi,0,',','.')}}</span>
                                                                @endif

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">
                                                            <span class=" kt-font-bold">Pajak</span>
                                                        </td>
                                                        <td width="25%"></td>
                                                        <td width="25%" style="text-align: right;">
                                                            <span class="kt-font-success kt-font-bold">Rp
                                                                {{number_format($totalPajak,0,',','.')}}
                                                            </span>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">
                                                            <span class=" kt-font-bold">Profit and Loss (After Tax)</span>
                                                        </td>
                                                        <td width="25%"></td>
                                                        <td width="25%" style="text-align: right;">

                                                            @if($tot_laba - $tot_rugi - $totalPajak < 0) <span class="kt-font-danger kt-font-bold">Rp
                                                                {{number_format($tot_laba - $tot_rugi - $totalPajak,0,',','.')}}</span>
                                                                @else
                                                                <span class="kt-font-success kt-font-bold">Rp
                                                                    {{number_format($tot_laba - $tot_rugi - $totalPajak,0,',','.')}}</span>
                                                                @endif

                                                        </td>
                                                    </tr>
                                                </table>       
                                            </div>
                                            
                                        </div>
                                    </div>
            
                                </div>

                            </div>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
