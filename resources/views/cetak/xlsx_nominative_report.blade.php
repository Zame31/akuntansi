<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style> 
    tfoot tr td {
                font-weight: bold !important;
            }
  </style>
</head>

<body>
  <div class="page page-dashboard">

    <div class="col-md-12">
      <section class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
              <thead class="bg-thead">

                <tr>
                  <th colspan="15" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 14px;"><b>Laporan Nominatif Customer ({{ $status }})</b></h2>
                  </th>
                </tr>
                @php
                  $system_date = collect(\DB::select("select * from ref_system_date"))->first();
              @endphp
                <tr>
                  <th colspan="15" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 12px;"><b>Tanggal :{{date('d F Y H:i:s',strtotime($system_date->last_date))}}</b></h2>
                  </th>
                </tr>
                
                <tr></tr>
                <tr></tr>

                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th align="center"> Branch Code </th>
                    <th align="center"> No Polis</th>
                    <th align="center"> Nama Tertanggung </th>
                    <th align="center"> Produk </th>
                    <th align="center"> Sum Insured </th>
                    <th align="center"> Premium </th>
                    <th align="center"> Discount </th>
                    <th align="center"> Nett Premium </th>
                    <th align="center"> Commision </th>
                    <th align="center"> Agent Fee</th>
                    <th align="center"> Nett to Underwriter </th>
                    <th align="center"> Inv Type </th>
                    <th align="center"> Start Date </th>
                    <th align="center"> End Date </th>
                    <th align="center"> Agent Name </th>
                    <th align="center"> Segment </th>
                </tr>

              </thead>
              <tbody>
                @foreach ($data as $item)
                    <tr>
                        <td> {{ $short_code }} </td>
                        <td> {{ $item->polis_no }} </td>
                        <td> {{ $item->nama_tertanggung }} </td>
                        <td> {{ $item->produk }} </td>
                        <td align="right"> {{ number_format($item->sum_insured, 2) }} </td>
                        <td align="right"> {{ number_format($item->premium, 2) }} </td>
                        <td align="right"> {{ number_format($item->disc_amount, 2) }} </td>
                        <td align="right"> {{ number_format($item->nett_premium, 2) }} </td>
                        <td align="right"> {{ number_format($item->commision, 2) }} </td>
                        <td align="right"> {{ number_format($item->agent_fee, 2) }} </td>
                        <td align="right"> {{ number_format($item->nett_to_underwriter, 2) }} </td>
                        <td> {{ $item->inv_type }} </td>
                        <td> {{ date('d-m-Y ', strtotime($item->start_date)) }} </td>
                        <td> {{ date('d-m-Y ', strtotime($item->end_date)) }} </td>
                        <td> {{ $item->agent_name }} </td>
                        <td> {{ $item->segment }} </td>
                    </tr>
                @endforeach
              </tbody>
              <tfoot style="background: #f7f7f7;">
                    @php
                        $tot_insured = 0;
                        $tot_premium = 0;
                        $tot_disc_amount = 0;
                        $tot_nett_premium = 0;
                        $tot_commision = 0;
                        $tot_agent_fee = 0;
                        $tot_nett_to_underwriter = 0;
                    @endphp

                    @if($data)
                        @foreach($data as $item)

                            @php
                                $tot_insured += $item->sum_insured;
                                $tot_premium += $item->premium;
                                $tot_disc_amount += $item->disc_amount;
                                $tot_nett_premium += $item->nett_premium;
                                $tot_commision += $item->commision;
                                $tot_agent_fee += $item->agent_fee;
                                $tot_nett_to_underwriter += $item->nett_to_underwriter;
                            @endphp

                        @endforeach
                    @endif
                    <tr>
                        <td colspan="4" align="center"> <b> Total </b> </td>
                        <td align="right" class="text-right text-info font-weight-bold"> <b> {{ number_format($tot_insured,2) }} </b> </td>
                        <td align="right" class="text-right text-info font-weight-bold"> <b> {{ number_format($tot_premium,2) }} </b> </td>
                        <td align="right" class="text-right text-info font-weight-bold"> <b> {{ number_format($tot_disc_amount,2) }} </b> </td>
                        <td align="right" class="text-right text-info font-weight-bold"> <b> {{ number_format($tot_nett_premium,2) }} </b> </td>
                        <td align="right" class="text-right text-info font-weight-bold"> <b> {{ number_format($tot_commision,2) }} </b> </td>
                        <td align="right" class="text-right text-info font-weight-bold"> <b> {{ number_format($tot_agent_fee,2) }} </b> </td>
                        <td align="right" class="text-right text-info font-weight-bold"> <b> {{ number_format($tot_nett_to_underwriter,2) }} </b> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tfoot>
            </table>

          </div>
        </div>

    </div>

    </section>
  </div>
</body>

</html>