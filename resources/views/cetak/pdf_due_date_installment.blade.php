<html>
    <head> 
        <style>
            * {
                font-family: sans-serif;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 14px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }


        </style>
    </head>
    <title> PDF Due Date Installment </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                        <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 14px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 14px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">Laporan Due Date Installment</span>
                                            <span style="display: block;font-size:14px;">{{date('d F Y H:i:s')}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 50px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 50px;">
                                <table class="table table-striped- table-hover table-checkable table-data" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th class="text-center">Branch Code</th>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">Customer Name</th>
                                            <th class="text-center">Product Name</th>
                                            <th class="text-center">Premi Amount</th>
                                            <th class="text-center">Paid Status</th>
                                            <th class="text-center">Nomor Polis</th>
                                            <th class="text-center">Due Date</th>
                                        </tr>
                                    </thead>
                                    <tbody id="searchResult">
                                        @php
                                            $no = 0; $total_premi_amount = 0;
                                        @endphp

                                        @forelse ($data as $item)
                                            @php $no++; @endphp
                                            <tr>
                                                <td align="center"> {{ $no }}</td>
                                                <td align="center"> {{ $branch_code }}</td>
                                                <td align="center"> {{ $item->id }} </td>
                                                <td> {{ $item->full_name }} </td>
                                                <td> {{ $item->definition }} </td>
                                                <td align="right"> {{ number_format($item->premi_amount, 2, ',', '.') }} </td>
                                                <td align="center"> <span class="btn btn-bold btn-sm btn-font-sm btn-label-danger"> {{ $item->paid }} </span> </td>
                                                <td> {{ $item->polis_no }} </td>
                                                <td align="center"> @if($item->due_date != NULL)  {{ date('d-m-Y', strtotime($item->due_date)) }} @endif </td>

                                                {{-- <td> {{ $item->url_dokumen }} </td> --}}
                                            </tr>

                                            @php
                                                $total_premi_amount += $item->premi_amount;
                                            @endphp

                                        @empty
                                            <tr>
                                                <td colspan="9" align="center"> Data tidak tersedia </td>
                                            </tr>
                                        @endforelse

                                    </tbody>
                                    <tfoot style="background: #f7f7f7;">
                                        <tr>
                                            <td class="font-weight-bold text-center" align="center"> Total </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td class="text-right text-info font-weight-bold" align="right"> <b> {{ number_format($total_premi_amount,2,',','.') }} </b></td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr>
                                    </tfoot>
                                </table>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
