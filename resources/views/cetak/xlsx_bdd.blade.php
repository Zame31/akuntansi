<html>
    <title> Export PDF</title>
    <style>
        tfoot tr td {
                    font-weight: bold !important;
                }
      </style>
    <body>
        <table>
            <tr>
                <td>Prepaid Expense List</td>
                <td>{{date('d F Y')}}</td>
            </tr>
        </table>
        <br>
        <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
            <thead>
                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th>Prepaid Expense Name</th>
                    <th>Prepaid Expense Note</th>
                    <th>Prepaid Expense Amount</th>
                    <th>Amor Amount</th>
                    <th>Paid Amount</th>
                    <th>Status Paid</th>
                    <th>Other Amount</th>
                    <th>Outstanding</th>
                    <th>Branch Code</th>
                </tr>
              
            </thead>
            <tbody>
                @php
                    $t_purchase = 0;
                    $t_amor =0;
                    $t_paid=0;

                   
                @endphp
                @foreach($data as $key => $v)
                    @php
                        $count_sc = collect(\DB::select("select count(id) as c from master_bdd_schedule where bdd_id = ".$v->mid))->first();
                        $count_sc_paid = collect(\DB::select("select count(id) as c from master_bdd_schedule where paid_status_id = 1 and bdd_id = ".$v->mid))->first();

                 
                        $count_paid = \DB::selectOne("select sum(amor_amount) as jumlah from master_bdd_schedule 
                        where paid_status_id = 1 and bdd_id = ".$v->mid);

                        // return number_format($count_sc_paid->c * $data->amor_amount,0,',','.');
                        
                        // $paid_amount = $count_sc_paid->c * $v->amor_amount;
                        $paid_amount = $count_paid->jumlah;
                        $paid = $count_sc_paid->c." / ".$count_sc->c;

                        $other_amount = 0;
                        $txadd = \DB::select("SELECT * FROM master_bdd_txadd where tx_type_id = 0 and bdd_id = ".$v->mid);

                        foreach ($txadd as $key => $vv) {
                            $other_amount += $vv->amount;
                        }

                        $t_purchase +=$v->bdd_amount;
                        $t_amor +=$v->amor_amount;
                        $t_paid +=$paid_amount;

                    @endphp
                    <tr>
                      <td>{{$v->bdd_name}}</td>
                      <td>{{$v->bdd_note}}</td>
                      <td style="text-align: right;">{{$v->bdd_amount}}</td>
                      <td style="text-align: right;">{{$v->amor_amount}}</td>
                      <td style="text-align: right;">{{$paid_amount}}</td>
                      <td >{{$paid}}</td>
                      {{-- <td>{{$v->coa_name}}</td> --}}
                      <td style="text-align: right;">{{$other_amount}}</td>
                      <td style="text-align: right;">{{$v->bdd_amount-$paid_amount}}</td>

                      <td>{{$v->short_code}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2" style="font-weight: bold;">Total</td>
                    <td style="font-weight: bold;text-align:right;">{{$t_purchase}}</td>
                    <td style="font-weight: bold;text-align:right;">{{$t_amor}}</td>
                    <td style="font-weight: bold;text-align:right;"> {{$t_paid}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    {{-- <td></td> --}}

                </tr>
            </tfoot>
        </table>
        {{-- <span id="totalData" style="font-weight: 500;"> Total Jumlah Data : {{ count($data) }}</span> --}}
        
    </body>
</html>
