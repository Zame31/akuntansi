<html>
    <head> 
        <style>
            * {
                font-family: sans-serif;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 9px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }


        </style>
    </head>
    <title> Export PDF History Detail </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                        <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                     </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 14px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 14px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        @php
                                            $system_date = collect(\DB::select("select * from ref_system_date"))->first();
                                        @endphp
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">Laporan Nominatif Customer ({{ $status }})</span>
                                            <span style="display: block;font-size:14px;">{{date('d F Y H:i:s',strtotime($system_date->last_date))}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 50px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 50px;">
                                <table class="table table-striped- table-hover table-checkable table-data" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th> Branch Code </th>
                                            <th> No Polis</th>
                                            <th> Nama Tertanggung </th>
                                            <th> Produk </th>
                                            <th> Sum Insured </th>
                                            <th> Premium </th>
                                            <th> Discount </th>
                                            <th> Nett Premium </th>
                                            <th> Commision </th>
                                            <th> Agent Fee</th>
                                            <th> Nett to Underwriter </th>
                                            <th> Inv Type </th>
                                            <th> Start Date </th>
                                            <th> End Date </th>
                                            <th> Agent Name </th>
                                            <th> Segment </th>
                                        </th>
                                    </thead>
                                    <tbody id="searchResult">
                                        @foreach ($data as $item)
                                            <tr>
                                                <td align="center"> {{ $short_code }} </td>
                                                <td> {{ $item->polis_no }} </td>
                                                <td> {{ $item->nama_tertanggung }} </td>
                                                <td> {{ $item->produk }} </td>
                                                <td align="right"> {{ number_format($item->sum_insured, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->premium, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->disc_amount, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->nett_premium, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->commision, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->agent_fee, 2, ',' , '.') }} </td>
                                                <td align="right"> {{ number_format($item->nett_to_underwriter, 2, ',' , '.') }} </td>
                                                <td> {{ $item->inv_type }} </td>
                                                <td> {{ date('d-m-Y ', strtotime($item->start_date)) }} </td>
                                                <td> {{ date('d-m-Y ', strtotime($item->end_date)) }} </td>
                                                <td> {{ $item->agent_name }} </td>
                                                <td> {{ $item->segment }} </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot style="background: #f7f7f7;">
                                            @php
                                                $tot_insured = 0;
                                                $tot_premium = 0;
                                                $tot_disc_amount = 0;
                                                $tot_nett_premium = 0;
                                                $tot_commision = 0;
                                                $tot_agent_fee = 0;
                                                $tot_nett_to_underwriter = 0;
                                            @endphp
                        
                                        @if($data)
                                            @foreach($data as $item)
                    
                                                @php
                                                    $tot_insured += $item->sum_insured;
                                                    $tot_premium += $item->premium;
                                                    $tot_disc_amount += $item->disc_amount;
                                                    $tot_nett_premium += $item->nett_premium;
                                                    $tot_commision += $item->commision;
                                                    $tot_agent_fee += $item->agent_fee;
                                                    $tot_nett_to_underwriter += $item->nett_to_underwriter;
                                                @endphp
                    
                                            @endforeach
                                        @endif
                                        <tr>
                                            <td colspan="4" align="center"> Total </td>
                                            <td align="right" class="text-right text-info font-weight-bold">{{ number_format($tot_insured,2,',','.') }}</td>
                                            <td align="right" class="text-right text-info font-weight-bold">{{ number_format($tot_premium,2,',','.') }}</td>
                                            <td align="right" class="text-right text-info font-weight-bold">{{ number_format($tot_disc_amount,2,',','.') }}</td>
                                            <td align="right" class="text-right text-info font-weight-bold">{{ number_format($tot_nett_premium,2,',','.') }}</td>
                                            <td align="right" class="text-right text-info font-weight-bold">{{ number_format($tot_commision,2,',','.') }}</td>
                                            <td align="right" class="text-right text-info font-weight-bold">{{ number_format($tot_agent_fee,2,',','.') }}</td>
                                            <td align="right" class="text-right text-info font-weight-bold">{{ number_format($tot_nett_to_underwriter,2,',','.') }}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr>
                                    </tfoot>
                                </table>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
