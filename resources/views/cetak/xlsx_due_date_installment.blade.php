<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Due Date Installment</title>
  <style>
    tfoot tr td {
        font-weight: bold !important;
    }
  </style>
</head>

<body>
  <div class="page page-dashboard">

    <div class="col-md-12">
      <section class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
              <thead class="bg-thead">

                <tr>
                  <th colspan="7" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 14px;"><b>Laporan Due Date Installment</b></h2>
                  </th>
                </tr>

                <tr>
                  <th colspan="7" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 12px;"><b>Tanggal : {{ date ('d F Y H:i:s')}}</b></h2>
                  </th>
                </tr>
                
                <tr> <th colspan="7"> &nbsp; </th></tr>
                <tr> <th colspan="7"> &nbsp; </th></tr>

                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th class="text-center" align="center">No</th>
                    <th class="text-center" align="center">Branch Code</th>
                    <th class="text-center" align="center">ID</th>
                    <th class="text-center" align="center">Customer Name</th>
                    <th class="text-center" align="center">Product Name</th>
                    <th class="text-center" align="center">Premi Amount</th>
                    <th class="text-center" align="center">Paid Status</th>
                    <th class="text-center" align="center">Nomor Polis</th>
                    <th class="text-center" align="center">Due Date</th>
                </tr>

              </thead>
              <tbody>
                @php
                    $total_premi_amount = 0; $no = 0;
                @endphp

                @forelse ($data as $item)
                    @php $no++ @endphp
                    <tr>
                        <td align="center"> {{ $no }} </td>
                        <td align="center"> {{ $short_code }} </td>
                        <td align="center"> {{ $item->id }} </td>
                        <td> {{ $item->full_name }} </td>
                        <td> {{ $item->definition }} </td>
                        <td align="right"> {{ number_format($item->premi_amount, 2) }} </td>
                        <td align="center"> <span class="btn btn-bold btn-sm btn-font-sm btn-label-danger"> {{ $item->paid }} </span> </td>
                        <td> {{ $item->polis_no }} </td>
                        <td align="center"> @if($item->due_date != NULL)  {{ date('d-m-Y', strtotime($item->due_date)) }} @endif </td>

                        {{-- <td> {{ $item->url_dokumen }} </td> --}}
                    </tr>

                    @php
                        $total_premi_amount += $item->premi_amount;
                    @endphp
                @empty
                    <tr>
                        <td colspan="8" align="center"> Data tidak tersedia </td>
                    </tr>
                @endforelse
              </tbody>
              <tfoot style="background: #f7f7f7;">
                <tr>
                    <td align="center"> <b> Total </b> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td class="text-right text-info font-weight-bold" align="right"> <b> {{ number_format($total_premi_amount,2) }} </b> </td>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                </tr>   
              </tfoot>
            </table>

          </div>
        </div>

    </div>

    </section>
  </div>
</body>

</html>