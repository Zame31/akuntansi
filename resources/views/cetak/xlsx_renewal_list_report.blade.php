<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Claim Report</title>
  <style>
    tfoot tr td {
                font-weight: bold !important;
            }
  </style>
</head>

<body>
  <div class="page page-dashboard">

    <div class="col-md-12">
      <section class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
              <thead class="bg-thead">

                <tr>
                  <th colspan="13" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 14px;"><b>Laporan Renewal List Report</b></h2>
                  </th>
                </tr>

                <tr>
                  <th colspan="13" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 12px;"><b>Due Date : {{ $date_start }} To {{ $date_end }}</b></h2>
                  </th>
                </tr>
                
                <tr> <th colspan="9"> &nbsp; </th></tr>
                <tr> <th colspan="9"> &nbsp; </th></tr>

                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th class="text-center" align="center">
                        <h6>No</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Agent Name</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Client Name</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Policy Name</h6>
                    </th>                        
                    <th class="text-center" align="center">
                        <h6>Product</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Start Date</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>End Date</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Underwriter</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Policy No</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Valuta</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Sum Insured</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Gross Premi</h6>
                    </th>
                    <th class="text-center" align="center">
                        <h6>Netto Brokerage</h6>
                    </th>
                </tr>

              </thead>
              <tbody>
                @php 
                    $total_si = 0;
                    $total_gp = 0;
                    $no = 0;                    
                @endphp
                @if (! is_null($data->data) )
                    @forelse ($data->data as $item)
                        @php $no++; @endphp
                        <tr>
                            <td align="center"> {{ $no }} </td>
                            <td align="center"> {{ $item->full_name }} </td>
                            <td align="center"> {{ $item->nama_client }} </td>
                            <td align="center"> {{ $item->police_name }} </td>
                            <td align="center"> {{ $item->definition }} </td>
                            <td align="center"> {{ date('d-m-Y', strtotime($item->start_date_polis)) }} </td>
                            <td align="center"> {{ date('d-m-Y', strtotime($item->end_date_polis)) }} </td>
                            <td align="center"> {{ $item->uw_def }} </td>
                            <td align="center"> {{ $item->polis_no }} </td>
                            <td align="center"> {{ $item->deskripsi }} </td>
                            <td align="right"> {{ number_format($item->ins_amount, 2) }} </td>
                            <td align="right"> {{ number_format($item->net_amount, 2) }} </td>
                            <td align="center"> {{ $item->netto_brokerage }} </td>                            
                        </tr>
                        @php 
                            $total_si += $item->ins_amount;
                            $total_gp += $item->net_amount;
                        @endphp
                    @empty
                        <tr>
                            <td align="center" colspan="13"> Data tidak ditemukan </td>
                        </tr>
                    @endforelse
                @else
                    <tr>
                        <td align="center" colspan="13"> Data tidak ditemukan </td>
                    </tr>
                @endif
              </tbody>
              <tfoot style="background: #f7f7f7;">
                    <tr>
                        <td colspan="10" align="right"><b>Total</b></td>                        
                        <td class="text-right text-success font-weight-bold" align="right"><b>{{number_format($total_si,2)}} </b> </td>
                        <td class="text-right text-success font-weight-bold" align="right"> <b> {{number_format($total_gp,2)}} </b> </td>
                        <td></td>                        
                    </tr>
                </tfoot>
            </table>

          </div>
        </div>

    </div>

    </section>
  </div>
</body>
</html>