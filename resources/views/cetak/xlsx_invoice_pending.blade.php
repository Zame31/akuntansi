<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Debit Note Pending Split Payment</title>
  <style>
    tfoot tr td {
                font-weight: bold !important;
            }
  </style>
</head>

<body>
  <div class="page page-dashboard">

    <div class="col-md-12">
      <section class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
              <thead class="bg-thead">

                <tr>
                  <th colspan="7" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 14px;"><b>Laporan Debit Note Pending Split Payment ({{ $status }})</b></h2>
                  </th>
                </tr>

                <tr>
                  <th colspan="7" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 12px;"><b>Tanggal : {{ date ('d F Y H:i:s')}}</b></h2>
                  </th>
                </tr>
                
                <tr> <th colspan="7"> &nbsp; </th></tr>
                <tr> <th colspan="7"> &nbsp; </th></tr>

                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th align="center"> No </th>
                    <th align="center"> Branch Code </th>
                    <th align="center"> Customer Name  </th>
                    <th align="center"> Product Name </th>
                    <th align="center"> Agent Name </th>
                    <th align="center"> Agent Fee </th>
                    <th align="center"> Commision </th>
                    <th align="center"> Ins Fee </th>
                </tr>

              </thead>
              <tbody>
                @php
                    $no=0;
                @endphp

                @if($data)
                @foreach($data as $item)
                    @php
                        $no++;
                    @endphp
                        <tr>
                            <td align="center"> {{ $no }}</td>
                            <td align="center"> {{ $short_code }}</td>
                            <td> {{ $item->full_name }} </td>
                            <td> {{ $item->definition }} </td>
                            <td> {{ $item->agent_name }}</td>    
                            <td align="right">{{number_format($item->agent_fee,2)}}</td>
                            <td align="right">{{number_format($item->commision,2)}}</td>
                            <td align="right">{{number_format($item->ins_fee,2)}}</td>
                        </tr>
                @endforeach
                @endif
              </tbody>
              <tfoot style="background: #f7f7f7;">
                    @php
                        $tot_agent_fee = 0;
                        $tot_commision = 0;
                        $tot_ins_fee = 0;
                    @endphp
                    
                    @if($data)
                        @foreach($data as $item)
                            @php
                                $tot_agent_fee += $item->agent_fee;
                                $tot_commision += $item->commision;
                                $tot_ins_fee += $item->ins_fee;
                            @endphp
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="5" align="center"> <b> Total </b> </td>
                        <td align="right"> <b> {{number_format($tot_agent_fee,2)}} </b> </td>
                        <td align="right"> <b> {{number_format($tot_commision,2)}} </b> </td>
                        <td align="right"> <b> {{number_format($tot_ins_fee,2)}} </b> </td>
                    </tr>      
              </tfoot>
            </table>

          </div>
        </div>

    </div>

    </section>
  </div>
</body>

</html>