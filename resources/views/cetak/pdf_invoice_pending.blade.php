<html>
    <head> 
        <style>
            * {
                font-family: sans-serif;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 14px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }


        </style>
    </head>
    <title> PDF Debit Note Pending Split Payment </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                        <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 14px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 14px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">Laporan Debit Note Pending Split Payment ({{ $status }})</span>
                                            <span style="display: block;font-size:14px;">{{date('d F Y H:i:s')}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 50px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 50px;">
                                <table class="table table-striped- table-hover table-checkable table-data" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th align="center"> No </th>
                                            <th align="center"> Branch Code </th>
                                            <th align="center"> Customer Name  </th>
                                            <th align="center"> Product Name </th>
                                            <th align="center"> Agent Name </th>
                                            <th align="center"> Agent Fee </th>
                                            <th align="center"> Commision </th>
                                            <th align="center"> Ins Fee </th>
                                        </tr>
                                    </thead>
                                    <tbody id="searchResult">
                                        @php
                                            $no=0;
                                        @endphp

                                        @if($data)
                                        @foreach($data as $item)
                                            @php
                                                $no++;
                                            @endphp
                                                <tr>
                                                    <td align="center"> {{ $no }}</td>
                                                    <td align="center"> {{ $short_code }}</td>
                                                    <td> {{ $item->full_name }} </td>
                                                    <td> {{ $item->definition }} </td>
                                                    <td> {{ $item->agent_name }}</td>    
                                                    <td align="right">{{number_format($item->agent_fee,2,',','.')}}</td>
                                                    <td align="right">{{number_format($item->commision,2,',','.')}}</td>
                                                    <td align="right">{{number_format($item->ins_fee,2,',','.')}}</td>
                                                </tr>
                                        @endforeach
                                        @endif

                                    </tbody>
                                    <tfoot style="background: #f7f7f7;">
                                        @php
                                            $tot_agent_fee = 0;
                                            $tot_commision = 0;
                                            $tot_ins_fee = 0;
                                        @endphp
                                        
                                        @if($data)
                                            @foreach($data as $item)
                                                @php
                                                    $tot_agent_fee += $item->agent_fee;
                                                    $tot_commision += $item->commision;
                                                    $tot_ins_fee += $item->ins_fee;
                                                @endphp
                                            @endforeach
                                        @endif
                                        <tr>
                                            <td align="center" colspan="5"> Total </td>
                                            <td align="right">{{number_format($tot_agent_fee,2,',','.')}}</td>
                                            <td align="right">{{number_format($tot_commision,2,',','.')}}</td>
                                            <td align="right">{{number_format($tot_ins_fee,2,',','.')}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
