<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Shee1</title>
  <style>
    tfoot tr td {
                font-weight: bold !important;
            }

    .table-custom tr,
    .table-custom th,
    .table-custom td {
        border: 1px solid #000;
    }
  </style>
</head>

<body>

<table>
    <tr>
        <th colspan="5"> LAMPIRAN DETAIL PRODUCT {{ strtoupper(strtolower($data->definition)) }} </th>
    </tr>
    <tr>
      <th colspan="5"> Name : {{ strtoupper(strtolower($data->full_name)) }} </th>
    </tr>
    <tr>
      <th colspan="5"> {{ $doc_type }} No :
            @if ( $doc_type == 'QS' )
                {{ $data->qs_no }}
            @elseif ( $doc_type == 'CN' )
                {{ $data->cn_no }}
            @else
                {{ $data->cf_no }}
            @endif
    </th>
    </tr>
    <tr></tr>
</table>

  <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
    <thead class="bg-thead">
      <tr>
          @foreach ( $colsName as $item )
            <th style="background-color: #1E90FF; border: 1px solid; color:#fff;" align="center"> {{$item->label}} </th>
          @endforeach
      </tr>
      @foreach ( $detail as $items)
        <tr>
        @foreach ( $items as $item)
            @if ($item->is_currency)
                <td align="right"> {{ $item->value_text }} </td>
            @else
                <td> {{ $item->value_text }} </td>
            @endif
        @endforeach
        </tr>
      @endforeach

    </thead>
    <tbody>

    </tbody>
  </table>
</body>

</html>
