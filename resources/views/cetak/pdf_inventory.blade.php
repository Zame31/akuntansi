<html>
    <head>
        <style>
            * {
                font-family: sans-serif;
                font-size: 12px;
            }

            table.table-data,
            table.table-data tr,
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
            }

            table.table-data th {
                text-align: center;
            }


        </style>
    </head>
    <title> Export PDF</title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">

                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                                $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                            @endphp
                                            {{-- <img alt="Logo" src="{{asset('img/logo.jpg')}}" style="width: 40px;" /> --}}
                                            <img src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 150px;">

                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 20px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 20px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:15px;">Inventory List</span>
                                            <span style="display: block;font-size:15px;">{{date('d F Y')}}</span>
                                            {{-- <span style="display: block;font-size:15px;">Status {{$data[0]->status_trx}} </span> --}}
                                        </div>

                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">

                                    </div>
                                </div>


                        </div>

                    </div>

                </div>

              
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 10px;">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 10px;">
                                <table class="table table-striped- table-hover table-checkable table-data">
                                    <thead>
                                        <tr>
                                            <th>Inventory Type</th>
                                            <th>Inventory Desc</th>
                                            <th>Inventory Code</th>
                                            <th>Purchase Amount</th>
                                            <th>Amor Amount</th>
                                            <th>Paid Amount</th>
                                            <th>Status Paid</th>
                                            <th>Other Amount</th>
                                            <th>Outstanding</th>
                                            <th>Branch Code</th>
                                        </tr>
                                      
                                    </thead>
                                    <tbody>
                                        @php
                                            $t_purchase = 0;
                                            $t_amor =0;
                                            $t_paid=0;

                                           
                                        @endphp
                                        @foreach($data as $key => $v)
                                            @php
                                                $count_sc = collect(\DB::select("select count(id) as c from master_inventory_schedule where inventory_id = ".$v->mid))->first();
                                                $count_sc_paid = collect(\DB::select("select count(id) as c from master_inventory_schedule where paid_status_id = 1 and inventory_id = ".$v->mid))->first();

                                                $count_paid = \DB::selectOne("select sum(amor_amount) as jumlah from master_inventory_schedule where paid_status_id = 1 and inventory_id = ".$v->mid);

                                                $paid_amount = $count_paid->jumlah;

                                                // $paid_amount = $count_sc_paid->c * $v->amor_amount;
                                                $paid = $count_sc_paid->c." / ".$count_sc->c;

                                                $other_amount = 0;
                                                $txadd = \DB::select("SELECT * FROM master_inventory_txadd where tx_type_id = 0 and inventory_id = ".$v->mid);
                                                foreach ($txadd as $key => $vv) {
                                                    $other_amount += $vv->amount;
                                                }

                                                $t_purchase +=$v->purchase_amount;
                                                $t_amor +=$v->amor_amount;
                                                $t_paid +=$paid_amount;

                                            @endphp
                                            <tr>
                                              <td>{{$v->description}}</td>
                                              <td>{{$v->inventory_desc}}</td>
                                              <td>{{$v->inventory_code}}</td>
                                              <td style="text-align: right;">{{number_format($v->purchase_amount,0,',','.')}}</td>
                                              <td style="text-align: right;">{{number_format($v->amor_amount,0,',','.')}}</td>
                                              <td style="text-align: right;">{{number_format($paid_amount,0,',','.')}}</td>
                                              <td >{{$paid}}</td>
                                              <td style="text-align: right;">{{number_format($other_amount,0,',','.')}}</td>
                                              <td style="text-align: right;">{{number_format($v->purchase_amount-$paid_amount,0,',','.')}}</td>
                                              <td>{{$v->short_code}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" style="font-weight: bold;">Total</td>
                                            <td style="font-weight: bold;text-align:right;">{{number_format($t_purchase,0,',','.')}}</td>
                                            <td style="font-weight: bold;text-align:right;">{{number_format($t_amor,0,',','.')}}</td>
                                            <td style="font-weight: bold;text-align:right;"> {{number_format($t_paid,0,',','.')}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <span id="totalData" style="font-weight: 500;"> Total Jumlah Data : {{ count($data) }}</span>
                        </div>


                    </div>

                </div>



            <!-- end:: Subheader -->
            </div>


        </div>
    </body>
</html>
