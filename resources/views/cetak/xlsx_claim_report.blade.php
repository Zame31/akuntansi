<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Claim Report</title>
  <style>
    tfoot tr td {
                font-weight: bold !important;
            }
  </style>
</head>

<body>
  <div class="page page-dashboard">

    <div class="col-md-12">
      <section class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
              <thead class="bg-thead">

                <tr>
                  <th colspan="9" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 14px;"><b>Laporan Claim ({{ $status }})</b></h2>
                  </th>
                </tr>

                <tr>
                  <th colspan="9" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 12px;"><b>Tanggal : {{ date ('d F Y H:i:s')}}</b></h2>
                  </th>
                </tr>
                
                <tr> <th colspan="9"> &nbsp; </th></tr>
                <tr> <th colspan="9"> &nbsp; </th></tr>

                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th class="text-center" align="center"> No </th>
                    <th class="text-center" align="center"> Branch Code </th>
                    <th class="text-center" align="center"> ID </th>
                    <th class="text-center" align="center"> Polis No </th>
                    <th class="text-center" align="center"> Start Date Claim </th>
                    <th class="text-center" align="center"> Start End Claim </th>
                    <th class="text-center" align="center"> Estimation Of Loss </th>
                    <th class="text-center" align="center"> Claim Amount </th>
                    <th class="text-center" align="center"> Claim Notes </th>
                    <th class="text-center" align="center"> Claim Status </th>
                </tr>

              </thead>
              <tbody>
                @php 
                    $total_est_loss = 0;
                    $total_claim_amount = 0;
                    $no = 0;
                @endphp

                @if (! is_null($data) )
                    @forelse ($data as $item)
                        @php $no++; @endphp

                        <tr>
                            <td align="center"> {{ $no }} </td>
                            <td align="center"> {{ $short_code }} </td>
                            <td align="center"> {{ $item->id }} </td>
                            <td> {{ $item->polis_no }} </td>
                            <td align="center"> {{ date('d-m-Y', strtotime($item->claim_start_date)) }} </td>
                            <td align="center"> {{ date('d-m-Y', strtotime($item->claim_end_date)) }} </td>
                            <td align="right"> {{ number_format($item->loss_amount, 2) }} </td>
                            <td align="right"> {{ number_format($item->claim_amount, 2) }} </td>
                            <td> {{ $item->claim_notes }} </td>
                            <td> {{ $item->status_definition }} </td>
                        </tr>

                        @php 
                            $total_est_loss += $item->loss_amount;
                            $total_claim_amount += $item->claim_amount;
                        @endphp
                    @empty
                        <tr>
                            <td align="center" colspan="10"> Data tidak ditemukan </td>
                        </tr>
                    @endforelse
                @else
                    <tr>
                        <td align="center" colspan="10"> Data tidak ditemukan </td>
                    </tr>
                @endif
              </tbody>
              <tfoot style="background: #f7f7f7;">
                    <tr>
                        <td align="center"> <b> Total </b> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="text-right text-success font-weight-bold" align="right"><b> {{number_format($total_est_loss,2)}} </b> </td>
                        <td class="text-right text-success font-weight-bold" align="right"> <b> {{number_format($total_claim_amount,2)}} </b> </td>
                        <td> </td>
                        <td> </td>
                    </tr>
                </tfoot>
            </table>

          </div>
        </div>

    </div>

    </section>
  </div>
</body>

</html>