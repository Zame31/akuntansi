<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Template Detail Product</title>
  <style>
    tfoot tr td {
                font-weight: bold !important;
            }
  </style>
</head>

<body>
  <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
    <thead class="bg-thead">


      <tr style="color:#fff;">
        @if ($typeFile == 'xls_table')
          <th>sts</th>
        @endif
        @for ($i = 0; $i < count($ck_excel_input_name); $i++)
          <th align="center"> {{$ck_excel_input_name[$i]}} </th>
        @endfor
        <th style="border-bottom: none; text-align: left;">zn_product_id</th>
        <th style="border-bottom: none; text-align: left;">zn_type_import</th>

      </tr>
      <tr>
        @if ($typeFile == 'xls_table')
          <th style="background-color: #1E90FF; border: 1px solid; color:#fff;" align="center">Status</th>
        @endif
        @for ($ii = 0; $ii < count($ck_excel_label); $ii++)
          <th style="background-color: #1E90FF; border: 1px solid; color:#fff;" align="center"> {{$ck_excel_label[$ii]}} </th>
        @endfor
        <th style="border-bottom: none; text-align: left;color:#fff;">{{$product_id}}</th>
        <th style="border-bottom: none; text-align: left;color:#fff;">{{$typeFile}}</th>

      </tr>

    </thead>
    <tbody>
      @if ($table_data)
       @foreach ($table_data as $td)
        @php
            $style = 'border: 1px solid #000000;';
        @endphp
        <tr>
            @foreach ($td['value'] as $vData)
              @php
                  if ($vData['name'] == 'sts' && $vData['value'] == 'f') {
                    $style = 'background-color: #ffe6dd; border: 1px solid #000000;';
                  }
              @endphp

              @if ($vData['name'] == 'sts' && $vData['value'] == 't')
                <td style="{{$style}}" >1</td>
              @elseif($vData['name'] == 'sts' && $vData['value'] == 'f')
                <td style="{{$style}}">0</td>
              @else
                  @php
                      $d = str_replace('.','',$vData['value']);
                      $d = str_replace(',','.',$d);
                  @endphp
                <td style="{{$style}}">{{$d}}</td>
              @endif

            @endforeach
        </tr>
        @endforeach
      @else
      <tr>
        <td align="center"></td>
      </tr>
      @endif


    </tbody>
  </table>
</body>

</html>
