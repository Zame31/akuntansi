<html>
    <head> 
        <style>
            * {
                font-family: sans-serif;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 14px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }


        </style>
    </head>
    <title> PDF Claim Report </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                        <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 14px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 14px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">Laporan Claim ({{ $status }})</span>
                                            <span style="display: block;font-size:14px;">{{date('d F Y H:i:s')}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 50px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 50px;">
                                <table class="table table-striped- table-hover table-checkable table-data" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center"> No </th>
                                            <th class="text-center"> Branch Code </th>
                                            <th class="text-center"> ID </th>
                                            <th class="text-center"> Polis No </th>
                                            <th class="text-center"> Start Date Claim </th>
                                            <th class="text-center"> Start End Claim </th>
                                            <th class="text-center"> Estimation Of Loss </th>
                                            <th class="text-center"> Claim Amount </th>
                                            <th class="text-center"> Claim Notes </th>
                                            <th class="text-center"> Claim Status </th>
                                        </tr>
                                    </thead>
                                    <tbody id="searchResult">
                                        @php 
                                            $total_est_loss = 0;
                                            $total_claim_amount = 0;
                                            $no = 0;
                                        @endphp
                    
                                        @if (! is_null($data) )
                                            @forelse ($data as $item)
                                                @php $no++; @endphp
                    
                                                <tr>
                                                    <td align="center"> {{ $no }} </td>
                                                    <td align="center"> {{ $short_code }} </td>
                                                    <td align="center"> {{ $item->id }} </td>
                                                    <td> {{ $item->polis_no }} </td>
                                                    <td align="center"> {{ date('d-m-Y', strtotime($item->claim_start_date)) }} </td>
                                                    <td align="center"> {{ date('d-m-Y', strtotime($item->claim_end_date)) }} </td>
                                                    <td align="right"> {{ number_format($item->loss_amount, 2, ',', '.') }} </td>
                                                    <td align="right"> {{ number_format($item->claim_amount, 2, ',', '.') }} </td>
                                                    <td> {{ $item->claim_notes }} </td>
                                                    <td> {{ $item->status_definition }} </td>
                                                </tr>
                    
                                                @php 
                                                    $total_est_loss += $item->loss_amount;
                                                    $total_claim_amount += $item->claim_amount;
                                                @endphp
                                            @empty
                                                <tr>
                                                    <td align="center" colspan="10"> Data tidak ditemukan </td>
                                                </tr>
                                            @endforelse
                                        @else
                                            <tr>
                                                <td align="center" colspan="10"> Data tidak ditemukan </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    <tfoot style="background: #f7f7f7;">
                                        <tr>
                                            <td align="center"> Total </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td class="text-right text-success font-weight-bold" align="right">{{number_format($total_est_loss,2,',','.')}}</td>
                                            <td class="text-right text-success font-weight-bold" align="right">{{number_format($total_claim_amount,2,',','.')}}</td>
                                            <td> </td>
                                            <td> </td>
                                        </tr>
                                    </tfoot>
                                </table>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
