<html>
    <head> 
        <style>
            * {
                font-family: sans-serif;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 14px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }

            .text-left{
                text-align: left !important;
            }
            .text-right{
                text-align: right !important;
            }


        </style>
    </head>
    <title> PDF Laporan Nominatif QS </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                        <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 14px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 14px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="mt-2" style="text-align: right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">Laporan Nominatif QS  @if ($type == 'aktif')
                                                Status Polis Aktif
                                            @else
                                                Status Non Polis Aktif
                                            @endif </span>
                                            <span style="display: block;font-size:14px;">{{date('d F Y H:i:s')}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 50px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 50px;">
                                <table class="table table-striped- table-hover table-checkable table-data" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-left">No</th>
                                            <th class="text-left">Client Name</th>
                                            <th class="text-left">QS No</th>
                                            <th class="text-left">Policy No</th>
                                            <th class="text-left">Policy Start</th>
                                            <th class="text-left">Policy End</th>
                                            <th class="text-right">TSI (Total Sum Insured)</th>
                                            <th class="text-right">Nett Premium</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total_ins_amount = 0;
                                            $total_net_amount = 0;  
                                        @endphp
                                        @foreach ($data as $key => $v)
                                        @php
                                            $total_ins_amount += $v->ins_amount;
                                            $total_net_amount += $v->net_amount;  
                                        @endphp
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$v->full_name}}</td>
                                                <td>{{$v->qs_no}}</td>
                                                <td>{{$v->polis_no}}</td>
                                                <td>{{date('d-m-Y',strtotime($v->start_date_polis))}}</td>
                                                <td>{{date('d-m-Y',strtotime($v->end_date_polis))}}</td>
                                                <td class="text-right">{{number_format($v->ins_amount,2,",",".")}}</td>
                                                <td class="text-right">{{number_format($v->net_amount,2,",",".")}}</td>
                                            </tr>
                                        @endforeach
                                     
                                    </tbody>
                                    <tfoot style="background: #f7f7f7;">
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right">{{number_format($total_ins_amount,2,",",".")}}</td>
                                            <td class="text-right">{{number_format($total_net_amount,2,",",".")}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
