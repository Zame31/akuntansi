<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style> 
    tfoot tr td {
                font-weight: bold !important;
            }
  </style>
</head>

<body>
  <div class="page page-dashboard">

    <div class="col-md-12">
      <section class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
              <thead class="bg-thead">

                <tr>
                  <th colspan="3" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 14px;"><b>Laporan Master COA</b></h2>
                  </th>
                </tr>
                @php
                  $system_date = collect(\DB::select("select * from ref_system_date"))->first();
                  $datas = \DB::select("select * from master_coa where coa_parent_id is not null ORDER BY coa_no");
              @endphp
                <tr>
                  <th colspan="3" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 12px;"><b>Tanggal :{{date('d F Y',strtotime($system_date->last_date))}}</b></h2>
                  </th>
                </tr>
                
                <tr></tr>
                <tr></tr>

                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th align="center"> coa_no </th>
                    <th align="center"> coa_name</th>
                    <th align="center"> last_os </th>
                </tr>

              </thead>
              <tbody>
                @foreach ($datas as $item)
                    <tr>
                       
                        <td align="right"> {{ $item->coa_no }} </td>
                        <td align="left"> {{ $item->coa_name }} </td>
                        <td align="right"> {{ number_format($item->last_os, 2) }} </td>
                    </tr>
                @endforeach
              </tbody>
            </table>

          </div>
        </div>

    </div>

    </section>
  </div>
</body>

</html>