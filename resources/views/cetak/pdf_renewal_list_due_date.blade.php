<html>
    <head> 
        <style>
            * {
                font-family: sans-serif;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 14px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }


        </style>
    </head>
    <title> PDF Claim Report </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                        <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 14px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 14px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">Laporan Renewal List Due Date</span>
                                            {{-- <span style="display: block;font-size:14px;">Due Date : {{ $date_start }} To {{ $date_end }}</span> --}}
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 50px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 50px;">
                                <table class="table table-striped- table-hover table-checkable table-data" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" align="center">
                                                <h6>No</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Agent Name</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Client Name</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Policy Name</h6>
                                            </th>                        
                                            <th class="text-center" align="center">
                                                <h6>Product</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Start Date</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>End Date</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Underwriter</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Policy No</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Valuta</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Sum Insured</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Gross Premi</h6>
                                            </th>
                                            <th class="text-center" align="center">
                                                <h6>Netto Brokerage</h6>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="searchResult">
                                        @php 
                                            $total_si = 0;
                                            $total_gp = 0;
                                            $no = 0;                    
                                        @endphp
                                        @if (! is_null($data->data) )
                                            @forelse ($data->data as $item)
                                                @php $no++; @endphp
                                                <tr>
                                                    <td align="center"> {{ $no }} </td>
                                                    <td align="center"> {{ $item->full_name }} </td>
                                                    <td align="center"> {{ $item->nama_client }} </td>
                                                    <td align="center"> {{ $item->police_name }} </td>
                                                    <td align="center"> {{ $item->definition }} </td>
                                                    <td align="center"> {{ date('d-m-Y', strtotime($item->start_date_polis)) }} </td>
                                                    <td align="center"> {{ date('d-m-Y', strtotime($item->end_date_polis)) }} </td>
                                                    <td align="center"> {{ $item->uw_def }} </td>
                                                    <td align="center"> {{ $item->polis_no }} </td>
                                                    <td align="center"> {{ $item->deskripsi }} </td>
                                                    <td align="right"> {{ number_format($item->ins_amount, 2) }} </td>
                                                    <td align="right"> {{ number_format($item->net_amount, 2) }} </td>
                                                    <td align="center"> {{ $item->netto_brokerage }} </td>                            
                                                </tr>
                                                @php 
                                                    $total_si += $item->ins_amount;
                                                    $total_gp += $item->net_amount;
                                                @endphp
                                            @empty
                                                <tr>
                                                    <td align="center" colspan="13"> Data tidak ditemukan </td>
                                                </tr>
                                            @endforelse
                                        @else
                                            <tr>
                                                <td align="center" colspan="13"> Data tidak ditemukan </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                    <tfoot style="background: #f7f7f7;">
                                        <tr>
                                            <td colspan="10" align="right"><b>Total</b></td>                        
                                            <td class="text-right text-success font-weight-bold" align="right"><b>{{number_format($total_si,2)}} </b> </td>
                                            <td class="text-right text-success font-weight-bold" align="right"> <b> {{number_format($total_gp,2)}} </b> </td>
                                            <td></td>                        
                                        </tr>
                                    </tfoot>
                                </table>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
