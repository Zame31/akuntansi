<html>
    <head>
        {{-- <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" /> --}}
        <link href="{{asset('css/myStyle.css')}}" rel="stylesheet" type="text/css" />
        {{-- <link href="{{asset('css/My.css')}}" rel="stylesheet" type="text/css" /> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >  --}}
        <style>
            * {
                font-family: sans-serif;
                box-sizing: border-box;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                font-size: 14px;
            }

            table.table-data th {
                text-align: center;
            }

            tfoot tr td {
                font-weight: bold !important;
            }

            .kt-font-danger {
    color: #ff5987 !important;
}
            
.kt-font-bold {
  font-weight: 500 !important; 
}
a {
    color: #5867dd;
    text-decoration: none;
    background-color: transparent;
}


.pl-4,
.px-4 {
  padding-left: 1.5rem !important; }

.col-6 {
    /* -webkit-box-flex: 0;
    -ms-flex: 0 0 20%;
    flex: 0 0 20%;
    max-width: 20%; */
}
.ml-3,
.mx-3 {
  margin-left: 1rem !important; }

.kt-font-success {
  color: #1dc9b7 !important; }

  /* .row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -10px;
    margin-left: -10px;
} */


.pr-4, .px-4 {
    padding-right: 1.5rem !important;
}
.col-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}

        </style> 
    </head>
    <title> PDF Claim Report </title>
    <body>
        @php
            $system_date = collect(\DB::select("select * from ref_system_date"))->first();
        @endphp
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        {{-- <div style="display: inline-block;">
                                            <img src="{{asset('img/logo-hd-soeryo-indonesia-gemilang.png')}}" style="width: 170px;">
                                        </div> --}}
                                        <div style="display: inline-block;">
                                            @php
                                                $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                            @endphp
                                            <img alt="Logo" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 170px;margin-top: 0px;" />
                                        </div>
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;">BALANCE SHEET REPORT</span>
                                            <span style="display: block;font-size:14px;">{{date('d F Y',strtotime($system_date->last_date))}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 10px;">
                    <div class="kt-portlet">
                        <div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 10px;">
                          
                            {{-- <div id="wrapper">
                                <div id="content">Column 1 (fluid)  </div>
                                <div id="sidebar">Column 2 (fixed)</div>
                                <div id="cleared"></div>
                              </div> --}}

                            <div class="rowss">
                                <div class="column">
                                    <h5 class="zn-head-line-success mb-4">Aktiva</h5>
                                    <div class="kt-widget6">
                                        <div class="kt-widget6__body">
                                            @if($aktiva)
                                            @foreach($aktiva as $key => $item)
                                            @php
                                            $awal=strlen($item->coa_no);
                                            @endphp 
                                            @if($awal==3)
                                            <div class="zn-item " style="padding: 8px;background: #f7f7f7;border-bottom: 1px dashed #ebedf2; ">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td>
                                                            <span class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <span class="kt-font-success kt-font-bold">Rp
                                                                {{number_format($item->last_os,0,',','.')}}</span>
                                                        </td>
                                                    </tr>
                                                </table>       
                                                
                                              
                                            </div>
                                            @else
                                            <div class="zn-item ml-3" style="padding: 8px;border-bottom: 1px dashed #ebedf2;">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td >
                                                            <span><a href="#">{{$item->coa_no}} -
                                                            {{$item->coa_name}}</a></span>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <span class="kt-font-success kt-font-bold" style="text-align:right;">Rp
                                                                {{number_format($item->last_os,0,',','.')}}</span>
                                                        </td>
                                                    </tr>
                                                </table>                                             
                                           
                                            </div>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
            
                                </div>

                                <div class="column">
                                    <h5 class="zn-head-line-danger mb-4">Pasiva</h5>
                                    <div class="kt-widget6">
                                        <div class="kt-widget6__body">
                                            @if($pasiva)
                                            @foreach($pasiva as $key => $item)
                                            @php
                                            $awal=strlen($item->coa_no);
                                            @endphp
                                            @if($awal==3)
                                            <div class="zn-item " style="padding: 8px;background: #f7f7f7;border-bottom: 1px dashed #ebedf2; ">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td>
                                                            <span class=" kt-font-bold">{{$item->coa_no}} - {{$item->coa_name}}</span>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <span class="kt-font-danger kt-font-bold">Rp
                                                                {{number_format($item->last_os,0,',','.')}}</span>
                                                        </td>
                                                    </tr>
                                                </table>       
                                                
                                              
                                            </div>
                                            @else
                                            <div class="zn-item ml-3" style="padding: 8px;border-bottom: 1px dashed #ebedf2;">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td >
                                                            <span><a href="#" >{{$item->coa_no}} -
                                                            {{$item->coa_name}}</a></span>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <span class="kt-font-danger kt-font-bold" style="text-align:right;">Rp
                                                                {{number_format($item->last_os,0,',','.')}}</span>
                                                        </td>
                                                    </tr>
                                                </table>                                             
                                           
                                            </div>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
            
                                </div>
                            </div>
                            {{-- </div> --}}
                            <div class="row" style="margin-top:20px;">
                                <div class="col-md-6">
            
                                    <div class="kt-widget6 mt-2">
                                        <div style="padding: 8px;border-bottom: 1px dashed #ebedf2; ">
                                            @php
                                            $total=0;
                                            @endphp
                                            @if($aktiva)
                                            @foreach($aktiva as $item)
                                            @php
                                            $awal=strlen($item->coa_no);
                                            if($awal==6){
                                            $total+=$item->last_os;
                                            }
                                            @endphp
                                            @endforeach
                                            @endif
                                            <table style="width:100%">
                                                <tr>
                                                    <td><span class=" kt-font-bold" >Total Aktiva</span></td>
                                                    <td style="text-align: right;"> <span class="kt-font-success kt-font-bold" >Rp
                                                        {{number_format($total,0,',','.')}}</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="kt-widget6 mt-2" >
                                        <div class="kt-widget6__body" style="padding: 8px;border-bottom: 1px dashed #ebedf2; ">
                                            @php
                                            $total_pasiva=0;
                                            @endphp
                                            @if($pasiva)
                                            @foreach($pasiva as $item)
                                            @php
            
                                            $awal=strlen($item->coa_no);
                                            if($awal==3){
                                            $total_pasiva+=$item->last_os;
                                            }
                                            @endphp
                                            @endforeach
                                            @endif
                                            <table style="width:100%">
                                                <tr>
                                                    <td><span class=" kt-font-bold">Total Pasiva</span></td>
                                                    <td style="text-align: right;"> <span class="kt-font-danger kt-font-bold" >Rp
                                                        {{number_format($total_pasiva,0,',','.')}}</span></td>
                                                </tr>
                                            </table>
                                            {{-- <div style="padding: 8px;">
                                                <span class=" kt-font-bold">Total Pasiva</span>
                                                <span class="kt-font-danger kt-font-bold">Rp
                                                    {{number_format($total_pasiva,0,',','.')}}</span>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
