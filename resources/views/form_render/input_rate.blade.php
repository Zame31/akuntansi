<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <div class="input-group">
        <input class="form-control text-right percentage"
                type="text" id="{{ $name }}"
                name="{{ $name }}" aria-describedby="basic-addon2" value=""
                pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2},[0-9]{1,5}$"
                title="Format persentase tidak valid (maksimal 5 digit di belakang koma)"
                @if ($is_required) required @endif>
        <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
    </div>
</div>
