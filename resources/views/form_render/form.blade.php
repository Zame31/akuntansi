<form id="form-data" method="POST">
    @csrf
    <input type="hidden" name="id" value="" id="id">
    <div class="form-group">
        <label for="full_name">Nama Lengkap</label>
        <input type="text" class="form-control" id="full_name" name="full_name">
    </div>
    <div class="form-group">
        <label for="code_name">Kode Nama</label>
        <input type="text" class="form-control" id="code_name" name="code_name">
    </div>
    <div class="form-group">
        <label for="address">Alamat</label>
        <textarea class="form-control" id="address" rows="3" name="address"></textarea>
    </div>
    <div class="form-group">
        <label for="phone_no">No Telepon</label>
        <input type="text" class="form-control" id="phone_no" name="phone_no">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email">
    </div>
    <div class="form-group d-none" id="formInputSeq">
        <label for="seq">Seq</label>
        <input type="text" class="form-control" id="seq" name="seq">
    </div>
</form>
