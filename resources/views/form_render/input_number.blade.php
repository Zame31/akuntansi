<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <input type="text" class="form-control money text-right" id="{{ $name }}" name="{{ $name }}">
</div>
