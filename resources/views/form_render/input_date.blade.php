<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <div class="input-group">
        <input class="form-control tanggal"
                type="text" id="{{ $name }}"
                name="{{ $name }}" aria-describedby="basic-addon2" value=""
                @if ($is_required) required @endif>
        <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="far fa-calendar-alt"></i></span></div>
    </div>
</div>
