<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>
    <input type="text"
         @if( $is_number ) onkeypress="return hanyaAngka(event)" @endif
        class="form-control @if ($is_currency) money text-right @endif" id="{{ $name }}" name="{{ $name }}"
        @if ($is_required) required @endif >
</div>
