<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                product_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Silahkan isi minimal 3 sampai 100 panjang karakter. '
                        },
                    }
                },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter. '
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

    }); // end document ready
    


    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '29');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('product.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('product') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#formInputSeq").addClass('d-none');
        $('#title_modal').html("Tambah Produk");
        $('#modal').modal('show');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id_product').val('');
	    $('#seq').val('1');
    }

    function setActive(id, isActive) {
        if (isActive == 't') {
            var titleSwal = 'Non Aktif Produk';
            var textSwal = 'Anda yakin akan menonaktifkan proposal ini?';
        } else {
            var titleSwal = 'Aktif Produk';
            var textSwal = 'Anda yakin akan mengaktifkan proposal ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('product.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '26'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('product') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();

        console.log('id : ' + id);

        $('#form-data').bootstrapValidator("resetForm", true);
        var url_get = '{{ route('product.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '29'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                $("#id_product").val(response.data.id);
                $("#product_name").val(response.data.definition);
                $("#seq").val(response.data.seq);
                $('#title_modal').html("Edit Produk");
                $('#modal').modal('show');

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>