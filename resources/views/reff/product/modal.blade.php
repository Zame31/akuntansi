<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST"> 
                    @csrf
                    <input type="hidden" name="id" value="" id="id_product"> 
                    <div class="form-group">
                        <label for="qs_no">Nama Produk</label>
                        <textarea class="form-control" rows="3" name="product_name" id="product_name"></textarea>
                    </div>
                    <div class="form-group d-none" id="formInputSeq">
                        <label for="seq">Seq</label>
                        <input type="text" class="form-control" id="seq" name="seq">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>