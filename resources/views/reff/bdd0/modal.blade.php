<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
    
                <div class="modal-body">
                    <form id="form-data" method="POST"> 
                        @csrf
                        <input type="hidden" name="id" value="" id="id">
                        <div class="form-group">
                            <label for="address">BDD Name</label>
                            <input type="text" class="form-control" id="bdd_name" name="bdd_name">
                        </div>
                        <div class="form-group">
                            <label for="single">COA No</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 coa_no" name="coa_no" id="coa_no">
                                    @php
                                        $coa = \DB::table('master_coa')
                                                ->where('is_active', 't')
                                                ->where('is_parent', 'f')
                                                ->orderBy('coa_no', 'ASC')
                                                ->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih COA No</option>
                                    @forelse ($coa as $item)
                                        <option value="{{ $item->coa_no }}">{{ $item->coa_no }} - {{ $item->coa_name }}</option>
                                    @empty
                                        <option selected disabled>COA No Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="single">COA No Amor</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 coa_no" name="coa_no_amor" id="coa_no_amor">
                                    @php
                                        $coa = \DB::table('master_coa')
                                                ->where('is_active', 't')
                                                ->where('is_parent', 'f')
                                                ->orderBy('coa_no', 'ASC')
                                                ->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih COA No Amor</option>
                                    @forelse ($coa as $item)
                                        <option value="{{ $item->coa_no }}">{{ $item->coa_no }} - {{ $item->coa_name }}</option>
                                    @empty
                                        <option selected disabled>COA No Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group d-none" id="formInputSeq">
                            <label for="address">Seq</label>
                            <input type="text" class="form-control" id="seq" name="seq">
                        </div>
                    </form>
                </div>
    
                <div class="modal-footer">
                    {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
                </div>
    
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>