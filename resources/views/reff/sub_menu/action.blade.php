<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                id_menu: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih menu'
                        },
                    }
                },
                sub_menu: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 60,
                            message: 'Silahkan isi minimal 3 sampai 60 panjang karakter. '
                        },
                    }
                },
                url: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 60,
                            message: 'Silahkan isi minimal 3 sampai 60 panjang karakter. '
                        },
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });
    


    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id_sub_menu").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '27');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('sub_menu.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    if (response.rc == 0) {
                        endLoadingPage();
                        $('#modal').modal('hide');
                        toastr.success(response.rm);
                        loadNewPage('{{ route('sub_menu') }}');
                    } else {
                        endLoadingPage();
                        toastr.error(response.rm);
                    }
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $('#title_modal').html("Tambah Sub Menu");
        $('#modal').modal('show');
        $('#id_menu').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id_sub_menu').val('');
    }

    function hapus(id) {
        console.log('id : ' + id);
        
        swal.fire({
            title: 'Hapus Sub Menu',
            text: 'Anda yakin akan menghapus sub menu ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('sub_menu.delete') }}',
                    data: {
                        id: id,
                        ref_order : '27'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 0) {
                            toastr.success(data.rm);
                            loadNewPage('{{ route('sub_menu') }}');
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();
        // reset select 2
        $('#id_menu').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id);

        var url_get = '{{ route('sub_menu.show', ':id') }}';
            url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '27'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $('#id_menu').val(""+response.data.idmenu+"").trigger('change');
                    $("#sub_menu").val(response.data.submenu);
                    $("#id_sub_menu").val(response.data.id);
                    $("#url").val(response.data.url);
                    $('#title_modal').html("Edit Sub Menu");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>