<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST"> 
                    @csrf
                    <input type="hidden" name="id" value="" id="id_sub_menu"> 
                    <div class="form-group">
                        <label for="single">Menu</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 menu" name="id_menu" id="id_menu">
                                @php
                                    $prov = \DB::table('ref_menu')->get();
                                @endphp
                                    <option value="1000" selected disabled>Pilih Menu</option>
                                @forelse ($prov as $item)
                                    <option value="{{ $item->id }}">{{ $item->menu }}</option>
                                @empty
                                    <option selected disabled>Menu Tidak Tersedia</option>
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub_menu">Sub Menu</label>
                        <input type="text" class="form-control" id="sub_menu" name="sub_menu">
                    </div>
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input type="text" class="form-control" id="url" name="url">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>