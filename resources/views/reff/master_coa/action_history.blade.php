<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    // PRINT
    // $(".znHeaderShow").css({"display": "none"});
    // $(".znSignature").css({"opacity": "0"});

    // $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

    function exportPdf(){
        var coaNo = $("#coa_no").val();
        var tglMulai = $("#tgl_mulai").val();
        var tglSelesai = $("#tgl_selesai").val();
        
        var date1 = new Date(tglMulai);
        var date2 = new Date(tglSelesai);

        var tanggal1 = ((date1.getDate().toString().length == 1) ? '0' : '') + date1.getDate();
        var tanggal2 = ((date2.getDate().toString().length == 1) ? '0' : '') + date2.getDate();
        
        var month1 = date1.getMonth() + 1;
        var month2 = date2.getMonth() + 1;

        var bulan1 = ((month1.toString().length == 1) ? '0' : '') + month1;
        var bulan2 = ((month2.toString().length == 1) ? '0' : '') + month2;

        tglMulai = tanggal1 + "/" + bulan1 + "/" + date1.getFullYear();
        tglSelesai = tanggal2 + "/" + bulan2 + "/" + date2.getFullYear();
        window.open(base_url + "ref/master_coa/history_tx/ExportPdf" + "?coaNo=" + coaNo + "&tglMulai=" + tglMulai + "&tglSelesai=" + tglSelesai, '_blank');
    }

    function exportExcel(){
        var coaNo = $("#coa_no").val();
        var tglMulai = $("#tgl_mulai").val();
        var tglSelesai = $("#tgl_selesai").val();
        
        var date1 = new Date(tglMulai);
        var date2 = new Date(tglSelesai);

        var tanggal1 = ((date1.getDate().toString().length == 1) ? '0' : '') + date1.getDate();
        var tanggal2 = ((date2.getDate().toString().length == 1) ? '0' : '') + date2.getDate();
        
        var month1 = date1.getMonth() + 1;
        var month2 = date2.getMonth() + 1;

        var bulan1 = ((month1.toString().length == 1) ? '0' : '') + month1;
        var bulan2 = ((month2.toString().length == 1) ? '0' : '') + month2;

        tglMulai = tanggal1 + "/" + bulan1 + "/" + date1.getFullYear();
        tglSelesai = tanggal2 + "/" + bulan2 + "/" + date2.getFullYear();
        window.open(base_url + "ref/master_coa/history_tx/exportExcel" + "?coaNo=" + coaNo + "&tglMulai=" + tglMulai + "&tglSelesai=" + tglSelesai, '_blank');
    }
    

    function closeCetakView() {
        $('#zn-print').modal('hide');
        $("#svg").removeAttr('style');
        $(".znHeaderShow").css({"display": "none"});
        $("#containerBtn").removeClass('d-none');
        $("input[type='text']").addAttr('readonly');
        // $(".rw16").removeAttr('style');
        // $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "1em", "flex": "1"});
        // $(".kt-widget6__item").css({"padding": "8px"});
        // $(".znSignature").css({"opacity": "0"});

    }

    function cetakView() {
        
        $(".znHeaderShow").css({"display": "block"});
        // $(".znSignature").css({"opacity": "1"});

        $("#svg").css({"padding" : "40px", "width": "850px","font-family": "sans-serif", "font-size" : "12px"});
        $("#containerBtn").addClass('d-none');
        $("input[type='text']").removeAttr('readonly');

        // $(".kt-widget6__item").css({"padding": "3px"});
        // $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "10px", "flex": "auto"});
        // $(".rw16").css({"margin-top": "140px"});


        $('#zn-print').modal('show');
        var pdf = new jsPDF('p', 'pt', 'a4');

        var data = document.getElementById('svg');
        pdf.html(data, {
            callback: function (pdf) {
                var iframe = document.getElementById('result');
                iframe.src = pdf.output('datauristring');
            }
        });
    }

    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                coa_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        }
                    }
                },
                coa_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        }
                    }
                },
                tgl_mulai: {
                    validators: {
                        callback: {
                            callback: function (value, validator, $field) {
                                console.log('value : ' + value);
                                if (value != '') {
                                    // if no telp hidden (selected ATM)
                                    return { 
                                        valid: true
                                    }
                                } else {
                                    return { 
                                        valid: false,
                                        message: 'Pilih tanggal mulai'
                                    }
                                }
                            
                            }
                        }

                    }
                },
                tgl_selesai: {
                    validators: {
                        callback: {
                            callback: function (value, validator, $field) {
                                console.log('value : ' + value);
                                if (value != '') {
                                    // if no telp hidden (selected ATM)
                                    return { 
                                        valid: true
                                    }
                                } else {
                                    return { 
                                        valid: false,
                                        message: 'Pilih tanggal selesai'
                                    }
                                }
                            
                            }
                        }

                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

        // Tambahan 
        $('#tgl_mulai').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            // startDate: date
        }).on('changeDate', function(){
            // set the "fromDate" end to not be later than "toDate" starts:
            $('#tgl_selesai').datepicker('setStartDate', new Date($(this).val()));

            // Revalidate the date when user change it
            $('#form-data').bootstrapValidator('revalidateField', 'tgl_mulai');
        });

        $('#tgl_selesai').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            // startDate: date
        }).on('changeDate', function(){
            // set the "fromDate" end to not be later than "toDate" starts:
            $('#tgl_mulai').datepicker('setEndDate', new Date($(this).val()));

            // Revalidate the date when user change it
            $('#form-data').bootstrapValidator('revalidateField', 'tgl_selesai');
        });

    }); // end document ready

    function searchData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('master_coa.search_history_tx') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    $("#loading").css('display', 'block');
                },

                success: function (response) {
                    $("#loading").css('display', 'none');
                    if (response.rc == 1) {
                        $("#searchResult").html("");
                        console.log(response.data);
                        var jmlData = response.data.length;
                        console.log(jmlData); 
                        var content = '';
                        var jenisTx = '';
                        var tx_amount = 0;
                        var acc_last = 0;
                        var acc_os = 0;
                        var date = "";
                        if (jmlData > 0) {
                            for(var i = 0; i < jmlData; i++) {
                                
                                if (response.data[i].tx_type_id == 1){
                                    jenisTx = 'Kredit';
                                } else {
                                    jenisTx = 'Debet';
                                }
                                
                                if (response.data[i].tx_amount == null) {
                                    response.data[i].tx_amount = 0
                                }

                                if (response.data[i].acc_last == null) {
                                    response.data[i].acc_last = 0
                                }

                                if (response.data[i].acc_last == null) {
                                    response.data[i].acc_last = 0
                                }

                                if (response.data[i].tx_notes == null) {
                                    response.data[i].tx_notes = ""
                                }

                                // saldo awal
                                // if ( response.data[i].acc_last < 0 ) {
                                //     acc_last = "(" + currencyFormatter(Math.abs(response.data[i].acc_last)) + ")";
                                // } else {
                                //     acc_last = currencyFormatter(response.data[i].acc_last);
                                // }
                                if ( response.data[i].acc_last < 0 ) {
                                    acc_last = Math.abs(response.data[i].acc_last);
                                    acc_last = acc_last.numberFormat(2, ',', '.');
                                    acc_last = "(" + acc_last + ")";
                                } else {
                                    acc_last = parseFloat(response.data[i].acc_last);
                                    acc_last = acc_last.numberFormat(2, ',', '.');
                                }

                                // nominal transaksi
                                // if (response.data[i].tx_amount < 0) {
                                //     tx_amount = "(" + currencyFormatter(Math.abs(response.data[i].tx_amount)) + ")";
                                // } else {
                                //     tx_amount = currencyFormatter(response.data[i].tx_amount);
                                // }

                                if ( response.data[i].tx_amount < 0 ) {
                                    tx_amount = Math.abs(response.data[i].tx_amount);
                                    tx_amount = tx_amount.numberFormat(2, ',', '.');
                                    tx_amount = "(" + tx_amount + ")";
                                } else {
                                    tx_amount = parseFloat(response.data[i].tx_amount);
                                    tx_amount = tx_amount.numberFormat(2, ',', '.');
                                }

                                // saldo akhir
                                // if (response.data[i].acc_os < 0) {
                                //     acc_os = "(" + currencyFormatter(Math.abs(response.data[i].acc_os)) + ")";
                                // } else {
                                //     acc_os = currencyFormatter(response.data[i].acc_os);
                                // }

                                if ( response.data[i].acc_os < 0 ) {
                                    acc_os = Math.abs(response.data[i].acc_os);
                                    acc_os = acc_os.numberFormat(2, ',', '.');
                                    acc_os = "(" + acc_os + ")";
                                } else {
                                    acc_os = parseFloat(response.data[i].acc_os);
                                    acc_os = acc_os.numberFormat(2, ',', '.');
                                }

                            
                                // var tx_amount = currencyFormatter(response.data[i].tx_amount);
                                // var acc_last = currencyFormatter(response.data[i].acc_last);
                                // var acc_os = currencyFormatter(response.data[i].acc_os);
                                // var date = convertDateFormat(response.data[i].created_at);
                                var date = response.data[i].tx_date;
                                var arr_date = date.split('-');
                                date = arr_date[2] + '-' + arr_date[1] + '-' + arr_date[0];


                                content += `<tr> 
                                        <td> ${response.no_rek} </td>
                                        <td align="center"> ${response.data[i].tx_code} </td>
                                        <td align="center"> ${jenisTx} </td>
                                        <td align="right"> ${tx_amount} </td>
                                        <td align="right"> ${acc_os} </td>
                                        <td> ${response.data[i].tx_notes} </td>
                                        <td align="center"> ${date} </td>
                                    </tr>`;

                                
                            }
                            $("#searchResult").append(content);
                            // $("#exportBtn").removeAttr('disabled');
                            // $("#exportBtn").css('cursor', 'pointer');
                        } else {
                            // $("#exportBtn").attr('disabled', 'disabled');
                            // $("#exportBtn").css('cursor', 'not-allowed');
                            $("#searchResult").html("");
                            content += `<tr> <td colspan="8" class="text-center"> Data tidak ditemukan </td> </tr>`;
                            $("#searchResult").append(content);
                        }
                        $("#exportBtn").removeAttr('disabled');
                        $("#exportBtn").css('cursor', 'pointer');

                        $("#exportBtn2").removeAttr('disabled');
                        $("#exportBtn2").css('cursor', 'pointer');

                        $("#totalData").text('Total Jumlah Data : ' + jmlData);
                    } else {
                        toastr.error(response.rm);
                    }
                }

            }).done(function (msg) {
                $("#loading").css('display', 'none');
            }).fail(function (msg) {
                $("#loading").css('display', 'none');
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    }

    function initMaskMoney(elm) {
        console.log(elm);
        $('.' + elm + '').maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false}).trigger('mask.maskMoney');

    }

    Number.prototype.numberFormat = function(decimals, dec_point, thousands_sep) {
        dec_point = typeof dec_point !== 'undefined' ? dec_point : '.';
        thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : ',';

        var parts = this.toFixed(decimals).split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);

        return parts.join(dec_point);
    }

</script>
