<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
      $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                coa_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'COA No harus angka. '
                        },
                        stringLength : {
                            max: 20,
                            message: 'Silahkan isi maksimal 20 karakter. '
                        }
                    }
                },
                coa_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 5,
                            max: 255,
                            message: 'Silahkan isi minimal 5 sampai 255 panjang karakter. '
                        },
                    }
                },
                coa_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            max: 20,
                            message: 'Silahkan isi maksimal 20 digit angka. '
                        },
                    }
                },
                valuta_code: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Valuta Code harus diisi angka. '
                        },
                        stringLength : {
                            max: 3,
                            message: 'Silahkan isi maksimal 3 karakter. '
                        }
                    }
                },
                coa_type_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih COA Type'
                        },
                    }
                },
                coa_group_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih COA Group'
                        },
                    }
                },
                coa_parent_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih COA Parent'
                        },
                    }
                },
                is_parent: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih Is Parent'
                        },
                    }
                },
                // branch_id: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih Branch'
                //         },
                //     }
                // },
                // balance_type_id: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih Balance Type'
                //         },
                //     }
                // },
                // company_id: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih Company'
                //         },
                //     }
                // },
                last_os: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Silahkan isi. '
                        // },
                        stringLength : {
                            max: 64,
                            message: 'Silahkan isi maksimal 64 karakter. '
                        }
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

        $("#coa_no").keyup(function(){
            $("#error-coa-no").addClass('d-none');
            $("#form-data").bootstrapValidator('revalidateField', $(this));
        })
    });

    $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

    function showModalAdd() {
        $("#error-coa-no").addClass('d-none');
        checkCOAParent();
    }

    function checkCOAParent() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('master_coa.check_coa_parent') }}',
            data: {},

            beforeSend: function () {
                // loadingPage();
            },

            success: function (result) {
                console.log(result);
                // endLoadingPage();
                if (result.rc == 1) {
                    if ( result.jmlParent <= 0) {
                        toastr.warning("COA Parent belum dibuat");
                    } else {
                        $("#form-data")[0].reset();
                        $('#title_modal').html("Tambah COA");
                        $('#modal').modal('show');
                        $('#coa_type_id').val('1000').trigger('change');
                        $('#coa_group_id').val('1000').trigger('change');
                        $('#balance_type_id').val('1000').trigger('change');
                        $('#branch_id').val('1000').trigger('change');
                        $('#coa_parent_id').val('1000').trigger('change.select2');
                        $('#form-data').bootstrapValidator("resetForm", true);
                        $('#company_id').val('1000').trigger('change.select2');
                        $('#id').val('');
                        // $('#coa_no').val('1000');
                        $('#last_os').removeAttr('disabled');
                    }
                }
            }
        }).done(function (msg) {
            // endLoadingPage();
        }).fail(function (msg) {
            // endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function createCOAParent() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('master_coa.parent_coa_create') }}',
            data: {},

            beforeSend: function () {
                loadingPage();
            },

            success: function (result) {
                console.log(result);
                endLoadingPage();
                if (result.rc == 0) {
                    toastr.success(result.rm);
                    loadNewPage('{{ route('master_coa') }}');
                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();

        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $("#error-coa-no").addClass('d-none');
            $("#error-coa-no").parent().removeClass('has-feedback has-error');

            objData.append('ref_order', '22');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('master_coa.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    if (response.rc == 0) {

                        $('#modal').modal('hide');

                        toastr.success(response.rm);
                        loadNewPage('{{ route('master_coa') }}');
                    } else {
                        $("#error-coa-no").removeClass('d-none');
                        $("#error-coa-no").parent().addClass('has-feedback has-error');

                        // toastr.warning(response.rm);
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showCompany(elm) {
        var id = $(elm).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('master_coa.get_company') }}',
            data: {
                branch_id: id
            },

            beforeSend: function () {
                loadingPage();
                $('#select2-company_id-container').siblings().addClass('d-none');
                $('#select2-company_id-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);
            },

            success: function (result) {
                console.log(result);
                $('.select2-selection__arrow').removeClass('d-none');
                $('div.spinner-border').remove();
                endLoadingPage();
                var elm_option;
                var jmlData = result.data.length;
                if (result.rc == 1) {
                    $("#company_id").html("");
                    var def_elm_option = `<option value='1000' selected='selected' disabled> Pilih Company </option>`;
                    $("#company_id").append(def_elm_option);
                    for(i=0; i < jmlData; i++) {
                        elm_option += `<option value="${result.data[i].id}"> ${result.data[i].company_name} </option>`;
                    }
                    $("#company_id").append(elm_option);
                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    } // end function

    function setActive(id, isActive) {
        console.log('id : ' + id);
        console.log('is Active : ' + isActive);

        if (isActive == 't') {
            var titleSwal = 'Non Aktif Master COA';
            var textSwal = 'Anda yakin akan menonaktifkan Master COA ini?';
        } else {
            var titleSwal = 'Aktif Master COA';
            var textSwal = 'Anda yakin akan mengaktifkan Master COA ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('master_coa.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '22'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('master_coa') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    } // end function

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        // $('#modal').modal('show');
        // $('#title_modal').html("Edit Master COA");
        $("#form-data")[0].reset();
        // $('#coa_type_id').val('1000').trigger('change');
        // $('#coa_group_id').val('1000').trigger('change');
        // $('#balance_type_id').val('1000').trigger('change');
        // $('#branch_id').val('1000').trigger('change');
        // $('#coa_parent_id').val('1000').trigger('change.select2');
        // $('#company_id').val('1000').trigger('change.select2');
        $("#formInputCOAParent").removeClass('d-none');
        $('#form-data').bootstrapValidator("resetForm", true);

        $("#error-coa-no").addClass('d-none');
        $("#error-coa-no").parent().removeClass('has-feedback has-error');

        console.log('id : ' + id);

        var url_get = '{{ route('master_coa.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '22'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {

                    saldo = Math.abs(response.data.last_os);
                    saldo = saldo.numberFormat(0, ',', '.');
                    console.log('saldo: ' + saldo);

                    $("#last_os").val(saldo);
                    $('#last_os').attr('disabled', 'disabled');

                    // success get data
                    $('#modal').modal('show');
                    $('#title_modal').html("Edit Master COA");
                    $("#id").val(response.data.id);
                    $("#coa_no").val(response.data.coa_no);
                    $("#coa_group_id").val(response.data.coa_group_id).trigger('change');

                    $("#coa_parent_id").html("");
                    showCOAParentWhenEdit(""+response.data.coa_group_id+"", ""+response.data.coa_parent_id+"");
                    $("#coa_parent_id").val(response.data.coa_parent_id).trigger('change.select2');

                    $("#coa_name").val(response.data.coa_name);
                    $("#coa_type_id").val(response.data.coa_type_id).trigger('change');
                    $("#valuta_code").val(response.data.valuta_code);
                    // $("#balance_type_id").val(response.data.balance_type_id).trigger('change');
                    $("#branch_id").val(response.data.branch_id).trigger('change');

                    $("#company_id").html("");
                    showCompanyWhenEdit(""+response.data.branch_id+"", ""+response.data.company_id+"");
                    $("#company_id").val(response.data.company_id).trigger('change.select2');

                    console.log(response.data.is_parent == true);
                    if ( response.data.is_parent ) {
                        $("#is_parent_t").prop("checked", true);
                    } else {
                        $("#is_parent_f").prop("checked", true);

                    }




                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function changeForm(elm) {
        var value = $(elm).val();
        console.log(value);
        if (value == '2') {
            // as a child
            $("#formInputCOANo").addClass('d-none');
            $("#coa_no").val('11'); //  set default value to 1
            $("#formInputCOAParent").removeClass('d-none');
        } else {
            // as a parent
            $("#formInputCOANo").removeClass('d-none');
            $("#formInputCOAParent").addClass('d-none');
            $("#coa_parent_id").val("1"); // set value to 1
        }
    }

    function showHistoryTx(id) {


        var url_get=base_url +'ref/master_coa/history_tx/' + id +'?type=detail';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            // data: {},
            success: function (data) {
            },
            beforeSend: function () {
                $('#pageLoad').fadeOut();
                loadingPage();
            },
        }).done(function (data) {
            endLoadingPage();
            var state = { name: "name", url_get: 'History', url: url_get };
            window.history.replaceState(state, "History", url_get);

            $('#pageLoad').html(data).fadeIn();
            KTBootstrapDatepicker.init();
            $('#table_id').DataTable();

        });

    }

    function showCOAParent(elm) {
        var id = $(elm).val();
        $("#parent_coa_no").text("");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('master_coa.get_coa_parent') }}',
            data: {
                coa_group_id: id
            },

            beforeSend: function () {
                loadingPage();
                $('#select2-coa_parent_id-container').siblings().addClass('d-none');
                $('#select2-coa_parent_id-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);
            },

            success: function (result) {
                console.log(result);
                $('.select2-selection__arrow').removeClass('d-none');
                $('div.spinner-border').remove();
                endLoadingPage();
                var elm_option;
                var jmlData = result.data.length;
                if (result.rc == 1) {
                    console.log(jmlData);
                    if (jmlData >= 1) {
                        $("#formInputCOAParent").removeClass('d-none');
                        $("#coa_parent_id").html("");
                        var def_elm_option = `<option value='1000' selected='selected' disabled> Pilih COA Parent </option>`;
                        $("#coa_parent_id").append(def_elm_option);

                        for(i=0; i < jmlData; i++) {

                            if ( result.data[i].coa_group_id == 5 ) {

                                if ( result.data[i].coa_parent_id != null) {
                                    elm_option += `<option value="${result.data[i].coa_no}"> ${result.data[i].coa_name} </option>`;
                                }

                            } else {
                                elm_option += `<option value="${result.data[i].coa_no}"> ${result.data[i].coa_name} </option>`;
                            }

                        }
                        $("#coa_parent_id").append(elm_option);
                    }
                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    } // end function

    function showCOAParentWhenEdit(coa_group_id, id_coa_parent) {
        var id = coa_group_id;
        var idParent = id_coa_parent;
        console.log('coa group id: ' + id);
        console.log('id parent: ' + idParent);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('master_coa.get_coa_parent') }}',
            data: {
                coa_group_id: id
            },

            beforeSend: function () {
                loadingPage();
                $('#select2-coa_parent_id-container').siblings().addClass('d-none');
                $('#select2-coa_parent_id-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);
            },

            success: function (result) {
                console.log(result);
                $('.select2-selection__arrow').removeClass('d-none');
                $('div.spinner-border').remove();
                endLoadingPage();
                var elm_option;
                var jmlData = result.data.length;
                if (result.rc == 1) {
                    $("#coa_parent_id").html("");
                    for(i=0; i < jmlData; i++) {
                        if (result.data[i].coa_no == idParent) {
                            elm_option += `<option value="${result.data[i].coa_no}" selected> ${result.data[i].coa_name} </option>`;
                        } else {
                            elm_option += `<option value="${result.data[i].coa_no}"> ${result.data[i].coa_name} </option>`;
                        }
                    }
                    $("#coa_parent_id").append(elm_option);
                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    } // end function


    function showCompanyWhenEdit(branchId, company_id) {
        var id = branchId;
        var idCompany = company_id;
        console.log('branch id: ' + id);
        console.log('company in: ' + idCompany);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('master_coa.get_company') }}',
            data: {
                branch_id: id
            },

            beforeSend: function () {
                loadingPage();
                $('#select2-coa_parent_id-container').siblings().addClass('d-none');
                $('#select2-coa_parent_id-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);
            },

            success: function (result) {
                console.log(result);
                console.log('company when edit');
                $('.select2-selection__arrow').removeClass('d-none');
                $('div.spinner-border').remove();
                endLoadingPage();
                var elm_option;
                var jmlData = result.data.length;
                if (result.rc == 1) {
                    $("#company_id").html("");
                    for(i=0; i < jmlData; i++) {
                        if (result.data[i].id == idCompany) {
                            elm_option += `<option value="${result.data[i].id}" selected> ${result.data[i].company_name} </option>`;
                        } else {
                            elm_option += `<option value="${result.data[i].id}"> ${result.data[i].company_name} </option>`;
                        }
                    }
                    $("#company_id").append(elm_option);
                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    } // end function

    function getIdParent(elm) {
        var id = $(elm).val();
        $("#parent_coa_no").text(id);
    }

    Number.prototype.numberFormat = function(decimals, dec_point, thousands_sep) {
        dec_point = typeof dec_point !== 'undefined' ? dec_point : '.';
        thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : ',';

        var parts = this.toFixed(decimals).split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);

        return parts.join(dec_point);
    }
</script>
