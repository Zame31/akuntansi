<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style> 
    tfoot tr td {
                font-weight: bold !important;
            }
  </style>
</head>

<body>
  <div class="page page-dashboard">

    <div class="col-md-12">
      <section class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-custom" id="basic-usage" border='1' align='center' style="border">
              <thead class="bg-thead">

                <tr>
                  <th colspan="3" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 14px;text-align:left;"><b>HISTORY TRANSAKSI</b></h2>
                  </th>
                </tr>
                @php
                  $system_date = collect(\DB::select("select * from ref_system_date"))->first();
                  $datas = \DB::select("select * from master_coa where coa_parent_id is not null ORDER BY coa_no");
              @endphp
                <tr>
                  <th colspan="3" style="border-bottom: none; text-align: center;">
                    <h2 class="margin_left" style="font-size: 12px;text-align:left;"><b>Tanggal : {{date('d F Y')}}</b></h2>
                  </th>
                </tr>
              </thead>
            </table>

            <table>
                <tr>
                    <td> COA No </td>
                    <td style="text-align:left;">: {{ $coa->coa_no }} </td>
                </tr>
                <tr>
                    <td> COA Name </td>
                    <td>: {{ $coa->coa_name }} </td>
                </tr>
                <tr>
                    <td> Tanggal Mulai </td>
                    <td>: {{ $tglMulai }} </td>
                </tr>
                <tr>
                    <td> Tanggal Selesai </td>
                    <td>: {{ $tglSelesai }} </td>
                </tr>
            </table>

            <table>
                <thead>
                
                <tr></tr>
                <tr></tr>

                <tr style="background-color: #1E90FF; border: 1px solid; color:#fff;">
                    <th> NO REKENING</th>
                    <th> KODE TRANSAKSI </th>
                    <th> JENIS TRANSAKSI </th>
                    {{-- <th> SALDO AWAL </th> --}}
                    <th> NOMINAL TRANSAKSI </th>
                    <th> SALDO AKHIR </th>
                    <th> KETERANGAN TRANSAKSI </th>
                    <th> WAKTU TRANSAKSI </th>
                </tr>

              </thead>
              <tbody>
                @foreach ($data as $item)
                                            <tr>
                                                <td style="text-align: center;"> {{ $item->coa_id }} </td>
                                                <td style="text-align: center;"> {{ $item->tx_code }} </td>
                                                <td style="text-align: center;"> 
                                                    @if($item->tx_type_id == 1)    
                                                        Kredit
                                                    @else
                                                        Debet
                                                    @endif
                                                </td>
                                                
                                                <!-- Saldo Awal -->
                                                {{-- <td style="text-align: right;"> 
                                                    @if ( $item->acc_last < 0 )
                                                        ({{ number_format(abs($item->acc_last), 2, ',', '.') }})
                                                    @else
                                                        {{ number_format($item->acc_last, 2, ',', '.') }} 
                                                    @endif
                                                </td> --}}

                                                <td style="text-align: right;"> 
                                                    @if ( $item->tx_amount < 0 )
                                                        ({{ number_format(abs($item->tx_amount), 2, '', '') }})
                                                    @else
                                                        {{ number_format($item->tx_amount, 2, '', '') }} 
                                                    @endif
                                                </td>

                                                <!-- Saldo Akhir -->
                                                <td style="text-align: right;"> 
                                                    @if ( $item->acc_os < 0 )
                                                        ({{ number_format(abs($item->acc_os), 2, '', '') }})
                                                    @else
                                                        {{ number_format($item->acc_os, 2, '', '') }}
                                                    @endif
                                                 </td>

                                                <td> {{ $item->tx_notes }} </td>
                                                <td> {{ date('d-m-Y H:i:s', strtotime($item->created_at)) }} </td>
                                            </tr>
                                        @endforeach
              </tbody>
            </table>
            <table>
              <tr>
                <td  style="font-weight: 500;">Total Jumlah Data : {{ count($data) }}</td>
              </tr>
            </table>

          </div>
        </div>

    </div>

    </section>
  </div>
</body>

</html>