<html>
    <head> 
        <style>
            * {
                font-family: sans-serif;
                font-size: 12px;
            }

            table.table-data, 
            table.table-data tr, 
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
            }

            table.table-data th {
                text-align: center;
            }


        </style>
    </head>
    <title> Export PDF History Detail </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">
        
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                        @php
                                            $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                        @endphp
                                        {{-- <img alt="Logo" src="{{asset('img/logo.jpg')}}" style="width: 40px;" /> --}}
                                        <img src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 150px;">
                                            {{-- <img alt="Logo" src="{{asset('img/logo.jpg')}}" style="width: 40px;" /> --}}
                                            {{-- <img src="{{url('img/logo-hd-soeryo-indonesia-gemilang.png')}}" style="width: 170px;"> --}}

                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 20px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 20px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:18px;">History Transaksi</span>
                                            <span style="display: block;font-size:18px;">{{date('d F Y')}}</span>
                                        </div>
                    
                                    </div>
                                </div>
        
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">
                                        
                                    </div>
                                </div>
        
                                
                        </div>
                        
                    </div>
        
                </div>
        
        
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 50px;">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                    <table>
                                            <tr>
                                                <td> COA No </td>
                                                <td> : </td>
                                                <td> {{ $coa->coa_no }} </td>
                                            </tr>
                                            <tr>
                                                <td> COA Name </td>
                                                <td> : </td>
                                                <td> {{ $coa->coa_name }} </td>
                                            </tr>
                                            <tr>
                                                <td> Tanggal Mulai </td>
                                                <td> : </td>
                                                <td> {{ $tglMulai }} </td>
                                            </tr>
                                            <tr>
                                                <td> Tanggal Selesai </td>
                                                <td> : </td>
                                                <td> {{ $tglSelesai }} </td>
                                            </tr>
                                        </table>
                            </div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 50px;">
                                <table class="table table-striped- table-hover table-checkable table-data">
                                    <thead>
                                        <tr>
                                            <th> NO REKENING</th>
                                            <th> KODE TRANSAKSI </th>
                                            <th> JENIS TRANSAKSI </th>
                                            {{-- <th> SALDO AWAL </th> --}}
                                            <th> NOMINAL TRANSAKSI </th>
                                            <th> SALDO AKHIR </th>
                                            <th> KETERANGAN TRANSAKSI </th>
                                            <th> WAKTU TRANSAKSI </th>
                                        </th>
                                    </thead>
                                    <tbody id="searchResult">
                                        @foreach ($data as $item)
                                            <tr>
                                                <td style="text-align: center;"> {{ $item->coa_id }} </td>
                                                <td style="text-align: center;"> {{ $item->tx_code }} </td>
                                                <td style="text-align: center;"> 
                                                    @if($item->tx_type_id == 1)    
                                                        Kredit
                                                    @else
                                                        Debet
                                                    @endif
                                                </td>
                                                
                                                <!-- Saldo Awal -->
                                                {{-- <td style="text-align: right;"> 
                                                    @if ( $item->acc_last < 0 )
                                                        ({{ number_format(abs($item->acc_last), 2, ',', '.') }})
                                                    @else
                                                        {{ number_format($item->acc_last, 2, ',', '.') }} 
                                                    @endif
                                                </td> --}}

                                                <td style="text-align: right;"> 
                                                    @if ( $item->tx_amount < 0 )
                                                        ({{ number_format(abs($item->tx_amount), 2, ',', '.') }})
                                                    @else
                                                        {{ number_format($item->tx_amount, 2, ',', '.') }} 
                                                    @endif
                                                </td>

                                                <!-- Saldo Akhir -->
                                                <td style="text-align: right;"> 
                                                    @if ( $item->acc_os < 0 )
                                                        ({{ number_format(abs($item->acc_os), 2, ',', '.') }})
                                                    @else
                                                        {{ number_format($item->acc_os, 2, ',', '.') }}
                                                    @endif
                                                 </td>

                                                <td> {{ $item->tx_notes }} </td>
                                                <td> {{ date('d-m-Y H:i:s', strtotime($item->created_at)) }} </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <span id="totalData" style="font-weight: 500;"> Total Jumlah Data : {{ count($data) }}</span>
                        </div>

                    </div>
        
                </div>
        
        
        
            <!-- end:: Subheader -->
            </div>
        
        
        </div>
    </body>
</html>
