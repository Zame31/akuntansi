@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Master COA </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            History Transaksi </a>
                        </div>
                </div>
            </div>
        </div>

        
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            History Transaksi
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                    <button @if($type=='neraca') onclick="loadNewPage('{{route('balance_sheet')}}')" @elseif($type=='laba_rugi') onclick="loadNewPage('{{route('laba_rugi')}}')" @else onclick="loadNewPage('{{route('master_coa')}}')" @endif  class="btn btn-primary float-right" style="color: white;">Kembali</>
                            </div>
                        </div>        
                    </div>
                </div>
                <div class="kt-portlet__body" id="svg">
                        <div class="row">
                            <div class="col-12">
                                    <form id="form-data"> 
                                            @csrf
                                            <input type="hidden" name="id" value="" id="id">
                                            <input type="hidden" name="idbranch" value="{{ $idbranch }}">
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="branch_name">COA No</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" readonly id="coa_no" name="coa_no" value="{{ $data->coa_no }}">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="branch_name">COA Name</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" readonly id="coa_name" name="coa_name" value="{{ $data->coa_name }}">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="tgl_mulai">Tanggal Mulai</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control init-date" id="tgl_mulai" name="tgl_mulai" readonly placeholder="Pilih Tanggal Mulai" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-2">
                                                    <label for="tgl_selesai">Tanggal Selesai</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control init-date" id="tgl_selesai" name="tgl_selesai" readonly placeholder="Pilih Tanggal Selesai" />
                                                </div>
                                            </div>
                                            <div class="row form-group" id="containerBtn">
                                                <div class="col-md-8">
                                                    <button type="button" class="btn btn-success float-right" onclick="searchData();">Tampilkan Data</button>
                                                </div>
                                            </div>
                                    </form>
                            </div>
                        </div>

                        
                </div>
                
            </div>

        </div>


        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Detail History Transaksi
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <button id="exportBtn" onclick="exportExcel()" type="button" class="btn btn-outline-success mr-2" target="_blank" disabled="disabled" style="cursor: not-allowed">
                            <i class="flaticon2-file"></i> Export Excel </button>
                            <button id="exportBtn2" onclick="exportPdf()" type="button" class="btn btn-outline-danger" target="_blank" disabled="disabled" style="cursor: not-allowed">
                                <i class="flaticon2-file"></i> Export Pdf </button>
                    </div>
                </div>
                <div class="kt-portlet__body svg" id="svg2">
                        <table class="table table-striped- table-hover table-checkable">
                            <thead>
                                <tr>
                                    <th class="text-center"> NO REKENING</th>
                                    <th class="text-center"> KODE TRANSAKSI </th>
                                    <th class="text-center"> JENIS TRANSAKSI </th>
                                    {{-- <th class="text-center"> SALDO AWAL </th> --}}
                                    <th class="text-center"> NOMINAL TRANSAKSI </th>
                                    <th class="text-center"> SALDO AKHIR </th>
                                    <th class="text-center"> KETERANGAN TRANSAKSI </th>
                                    <th class="text-center"> TANGGAL TRANSAKSI </th>
                                </th>
                            </thead>
                            <tbody id="searchResult">
                            </tbody>
                        </table>
                        <span id="totalData" style="font-weight: 600;"></span>
                </div>
                
            </div>

        </div>

        <div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="title_modal">Print & Download Preview History Transaksi</h5>
                        <button type="button" onclick="closeCetakView()" class="close"></button>
                    </div>

                    <div class="modal-body kt-portlet m-0 p-0">
                        <div class="">
                            <div class="kt-portlet__body p-0">

                                <iframe id='result' class="scrollStyle" style="width: 100%;height: 520px;border: none;"></iframe>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    <!-- end:: Subheader -->
    </div>


</div>

@include('reff.master_coa.action_history')

@stop