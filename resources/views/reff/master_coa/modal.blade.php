<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body">
                    <form id="form-data" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="" id="id">
                        <div class="form-group">
                            <label for="single">COA Group</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 coa_group_id" name="coa_group_id" id="coa_group_id" onchange="showCOAParent(this);">
                                    @php
                                        $coa = \DB::table('ref_coa_group')->where('is_active', 't')->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih COA Group</option>
                                    @forelse ($coa as $item)
                                        <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                    @empty
                                        <option selected disabled>COA Group Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group d-none" id="formInputCOAParent">
                            <label for="single">COA Parent</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 coa_parent_id" name="coa_parent_id" id="coa_parent_id" onchange="getIdParent(this);">
                                    <option selected disabled value="1000">Pilih COA Parent</option>
                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label class="col-2 col-form-label">COA</label>
                            <div class="col-9">
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="coa" value="1" onclick="changeForm(this);"> Parent
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="coa" value="2" onclick="changeForm(this);"> Child
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group d-none" id="formInputCOANo">
                            <label for="branch_name">COA No</label>
                            <input type="text" class="form-control" id="coa_no" name="coa_no">
                        </div> --}}
                        {{-- <div class="form-group d-none" id="formInputCOAParent">
                            <label for="single">COA Parent</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 coa_parent_id" name="coa_parent_id" id="coa_parent_id">
                                    @php
                                        $coa = \DB::table('master_coa')->where('is_parent', 't')->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih COA Parent</option>
                                    @forelse ($coa as $item)
                                        <option value="{{ $item->coa_no }}">{{ $item->coa_name }}</option>
                                    @empty
                                        <option selected disabled>COA Parent Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <label for="coa_no">COA No</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="parent_coa_no"></span>
                                </div>
                                <input type="text" class="form-control" id="coa_no" name="coa_no" onkeypress="return hanyaAngka(event)">
                            </div>
                            <small class="help-block d-none" id="error-coa-no"> COA No sudah ada </small>
                        </div>

                        <div class="form-group">
                            <label for="address">COA Name</label>
                            <textarea class="form-control" id="coa_name" rows="3" name="coa_name"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="branch_name">Valuta Code</label>
                            <input type="text" class="form-control" id="valuta_code" name="valuta_code">
                        </div>
                        <div class="form-group">
                            <label for="single">COA Type</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 coa_type_id" name="coa_type_id" id="coa_type_id">
                                    @php
                                        $coa = \DB::table('ref_coa_type')->where('is_active', 't')->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih COA Type</option>
                                    @forelse ($coa as $item)
                                        <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                    @empty
                                        <option selected disabled>COA Type Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label for="single">Balance Type</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 balance_type_id" name="balance_type_id" id="balance_type_id">
                                    @php
                                        $coa = \DB::table('ref_tx_type')->where('is_active', 't')->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih Balance Type</option>
                                    @forelse ($coa as $item)
                                        <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                    @empty
                                        <option selected disabled>Balance Type Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label for="single">Branch</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 branch_id" name="branch_id" id="branch_id" onchange="showCompany(this);">
                                    @php
                                        if ( Auth::user()->user_role_id != 5 ) {
                                            $branch = \DB::table('master_branch')->where('is_active', 't')->where('company_id', Auth::user()->company_id)->get();
                                        } else {
                                            $branch = \DB::table('master_branch')->where('is_active', 't')->get();
                                        }

                                    @endphp
                                        <option selected disabled value="1000">Pilih Branch</option>
                                    @forelse ($branch as $item)
                                        <option value="{{ $item->id }}">{{ $item->branch_name }}</option>
                                    @empty
                                        <option selected disabled>Branch Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="single">Company</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 company_id" name="company_id" id="company_id">
                                    <option selected disabled value="1000">Pilih Company</option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <label for="last_os">Saldo</label>
                            <input type="text" class="form-control text-right money" placeholder="0" name="last_os" id="last_os">
                        </div>

                        <div class="form-group">
                            <label for="coa_parent" class="control-label"> Is Parent * </label>
                            <div class="kt-radio-inline">
                                <label class="kt-radio">
                                    <input type="radio" class="form-control" name="is_parent" id="is_parent_t" value="t"> Ya
                                    <span></span>
                                </label>
                                <label class="kt-radio">
                                    <input type="radio" class="form-control" name="is_parent" id="is_parent_f" value="f"> Tidak
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
