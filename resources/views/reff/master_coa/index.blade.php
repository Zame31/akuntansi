@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Master COA </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Master COA </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Master COA
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                @php
                                    $countCOAParent = DB::table('master_coa')
                                                        ->where('is_active', 't')
                                                        // ->where('is_parent', 't')
                                                        ->whereNull('coa_parent_id')
                                                        ->where('branch_id', Auth::user()->branch_id)
                                                        ->where('company_id', Auth::user()->company_id)
                                                        ->count();  
                                @endphp
                                @if ( $countCOAParent <= 0 )
                                    <button onclick="createCOAParent();" class="btn btn-primary mr-3" style="color: white;">Tambah COA Parent</>
                                @endif
                                <button onclick="showModalAdd();" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center" width="100px">Aksi</th>
                                        {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                        <th width="100px" class="text-center"> Status Aktif</th>
                                        <th class="text-center"> COA No </th>
                                        <th class="text-center"> COA Name </th>
                                        <th class="text-center"> COA Group </th>
                                        <th class="text-center"> COA Type </th>
                                        <th class="text-center"> COA Parent </th>
                                        <th class="text-center"> Balance Type </th>
                                        <th class="text-center"> Saldo </th>
                                    </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="flaticon-more"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <button class="dropdown-item" onclick="showEditModal('{{ $item->id }}')"><i class="la la-edit"></i> Edit </button>
                                                        <button class="dropdown-item" onclick="showHistoryTx('{{ $item->coa_no }}')"><i class="la la-edit"></i> History Tx </button>
                                                        @if($item->is_active == 't')
                                                            <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 't')"><i class="la la-times-circle-o"></i> Non Aktif </button>
                                                        @else 
                                                            <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 'f')"><i class="la la-check-circle-o"></i> Aktif </button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                @if($item->is_active == 't')
                                                    <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                                @else 
                                                    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            <td>{{ $item->coa_no }}</td>
                                            <td>{{ $item->coa_name }}</td>
                                            <td> {{ $item->coa_group }}</td>
                                            <td> {{ $item->coa_type }}</td>
                                            <td> {{ $item->coa_parent }}</td>
                                            <td>
                                                @if($item->balance_type_id == 0)
                                                    Debet
                                                @else
                                                    Kredit
                                                @endif
                                            </td>
                                            <td align="right">{{ number_format($item->last_os, 0, '.', '.') }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="10" align="center">  
                                                Data Tidak Tersedia
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>

@include('reff.master_coa.modal')

@include('reff.master_coa.action')

@stop