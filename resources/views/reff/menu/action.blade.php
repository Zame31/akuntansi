<script type="text/javascript">

    // Bootstrap Validator
    $(document).ready(function () {
        $(".containterList").hide();
        $('input[type="checkbox"]').change(checkboxChanged);

        function checkboxChanged() {
            var $this = $(this),
                checked = $this.prop("checked"),
                container = $this.parent(),
                siblings = container.siblings();

            container.find('input[type="checkbox"]').prop({
                indeterminate: false,
                checked: checked
            })
            .siblings('label')
            .removeClass('custom-checked custom-unchecked custom-indeterminate')
            .addClass(checked ? 'custom-checked' : 'custom-unchecked');

            checkSiblings(container, checked);
        }

        function checkSiblings($el, checked) {
            var parent = $el.parent().parent(),
                all = true,
                indeterminate = false;

            $el.siblings().each(function() {
                return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
            });

            if (all && checked) {
                parent.children('input[type="checkbox"]').prop({
                    indeterminate: false,
                    checked: checked
                }).siblings('label')
                  .removeClass('custom-checked custom-unchecked custom-indeterminate')
                  .addClass(checked ? 'custom-checked' : 'custom-unchecked');
                checkSiblings(parent, checked);
            } else if (all && !checked) {
                indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

                parent.children('input[type="checkbox"]')
                      .prop("checked", checked)
                      .prop("indeterminate", indeterminate)
                      .siblings('label')
                      .removeClass('custom-checked custom-unchecked custom-indeterminate')
                      .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

                checkSiblings(parent, checked);
            } else {
                $el.parents("li").children('input[type="checkbox"]')
                   .prop({
                        indeterminate: true,
                        checked: false
                   })
                    .siblings('label')
                    .removeClass('custom-checked custom-unchecked custom-indeterminate')
                    .addClass('custom-indeterminate');
            }
        } // end function

        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                role_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 60,
                            message: 'Silahkan isi minimal 3 sampai 60 panjang karakter. '
                        },
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });



    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        var jmlCheckbox = $('input[type="checkbox"]:checked').length;

        if (jmlCheckbox > 0) {
            if (validateProduk.isValid()) {
                var id = $("#id_menu").val();
                var formData = document.getElementById("form-data");
                var objData = new FormData(formData);

                objData.append('ref_order', '26');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ route('menu.store') }}',
                    data: objData,
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (response) {
                        if (response.rc == 0) {
                            endLoadingPage();
                            $('#modal').modal('hide');
                            toastr.success(response.rm);
                            loadNewPage('{{ route('menu') }}');
                        } else {
                            endLoadingPage();
                            toastr.error(response.rm);
                        }
                    }

                }).done(function (msg) {
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });

            } // endif
        } else {
            endLoadingPage();
            toastr.error("Menu belum dipilih");
        }

    } // end function

    function showChildList(elm) {
        $(elm).siblings('.containterList').slideToggle();
        $(elm).toggleClass('fa-caret-down fa-caret-right');
    }

    function showModalAdd() {
        $("#form-data")[0].reset();
        $('#title_modal').html("Tambah Menu");
        $('#modal').modal('show');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id_menu').val('');
    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();
        // reset select 2
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id);

        var url_get = '{{ route('menu.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '26'
            },
            beforeSend: function () {
                 $(".containterList").hide();
                 $(".iconList").removeClass('fa-caret-down');
                 $(".iconList").addClass('fa-caret-right');
                 loadingPage();
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id_role").val(response.data.userRole.id);
                    $("#role_name").val(response.data.userRole.definition);
                    var arrMenu = response.data.menuAccess;
                    var arrTypeMenu = response.data.typeMenu;

                    if ( arrTypeMenu.length > 0) {
                        arrTypeMenu.forEach(function(item) {
                            console.log(item.type);
                            $('input[type="checkbox"][value="' + item.type + '"]').prop("checked",true);
                            $('input[type="checkbox"][value="' + item.type + '"]').siblings(".containterList").slideDown();
                            $('input[type="checkbox"][value="' + item.type + '"]').siblings(".iconList").removeClass('fa-caret-right');
                            $('input[type="checkbox"][value="' + item.type + '"]').siblings(".iconList").addClass('fa-caret-down');
                        })
                    }

                    arrMenu.forEach(function(item){
                        console.log(item.code_menu);
                        $('input[type="checkbox"][value="' + item.code_menu + '"]').prop("checked",true);
                    })

                    $('#title_modal').html("Setting Menu");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            endLoadingPage();
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

</script>
