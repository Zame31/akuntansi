<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST"> 
                    @csrf
                    <input type="hidden" name="id" value="" id="id_role"> 
                    <div class="form-group">
                        <label for="role_name">Role</label>
                        <input type="text" readonly class="form-control" id="role_name" name="role_name">
                    </div>
                    <div class="form-group">
                        <label for="menu">Menu</label>
                        <ul class="treeview" style="padding-left: 15px;">
                            @php 
                                $parentList = \DB::table('ref_menu')->distinct()->orderBy('type', 'ASC')->get();
                                $dt = $parentList->groupBy('type');
                                $idx = 0;
                            @endphp

                            @foreach ($dt as $type => $data)
                                <li style="list-style-type: none;">
                                        <i class="fa fa-caret-right iconList" aria-hidden="true" style="
                                            font-size: 17px;
                                            position: absolute;
                                            left: 20px;
                                            cursor: pointer;
                                        " onclick="showChildList(this);"></i>
                                <input type="checkbox" name="menu[]" id="tall_{{$idx}}" value="{{ $type }}">
                                <label for="tall_{{$idx}}" class="custom-unchecked"> {{ $type }} </label>
                                        <ul style="list-style-type: none; padding-left: 20px;" class="containterList">
                                            @foreach ($data as $i => $val) 
                                                {{-- @if ( $val->id !== 75)
                                                    <li>
                                                        <input type="checkbox" name="subMenu[]" id="tall-1_{{$val->id}}" value="{{ $val->code }}">
                                                        <label for="tall-1_{{$val->id}}" class="custom-unchecked">{{ $val->menu }}</label>
                                                    </li>
                                                @endif --}}

                                                <li>
                                                    <input type="checkbox" name="subMenu[]" id="tall-1_{{$val->id}}" value="{{ $val->code }}">
                                                    <label for="tall-1_{{$val->id}}" class="custom-unchecked">{{ $val->menu }}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                </li>
                                @php $idx++; @endphp
                            @endforeach
                        </ul>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>