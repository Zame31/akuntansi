@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        User Role Menu </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List User Role Menu </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List User Role Menu
                        </h3>
                    </div>
                    {{-- <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button onclick="showModalAdd();" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center" width="130px">Aksi</th>
                                        <th class="text-center"> Role </th>
                                        <th class="text-center"> Akses Menu </th>

                                    </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="showEditModal('{{ $item->id }}')">Setting Menu</button>
                                            </td>
                                            <td>
                                                {{ $item->definition }}
                                            </td>
                                            @php 
                                                $dtType = \DB::SELECT('select distinct(type)
                                                                      from ref_rel_menu RRM LEFT JOIN ref_menu RM 
                                                                      ON RRM.code_menu = RM.code
                                                                      WHERE RRM.id_role=' . $item->id);
                                            @endphp

                                                <td align="center">
                                                    @forelse ($dtType as $item)
                                                        <span class="badge badge-success"> {{ $item->type }} </span>
                                                    @empty
                                                    @endforelse
                                                </td>
                                            
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3" align="center"> 
                                                Data Tidak Tersedia
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>

</div>

@include('reff.menu.modal')
@include('reff.menu.action')

@stop
