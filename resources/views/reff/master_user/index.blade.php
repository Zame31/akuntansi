@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Master User </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List User </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List User
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <!-- Super Admin -->
                            @if (Auth::user()->user_role_id == 5 || Auth::user()->user_role_id == 6)
                                <div class="col-12">
                                    <button onclick="showModalAdd();" class="btn btn-primary" style="color: white;">Tambah Data</>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    @if ( Auth::user()->user_role_id == 6)
                                        <tr>
                                            <th class="text-center" width="30px"> No </th>
                                            <th class="text-center" width="30px">Aksi</th>
                                            {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                            <th width="100px" class="text-center"> Status Aktif</th>
                                            <th class="text-center"> Username </th>
                                            <th class="text-center"> Fullname </th>
                                            <th class="text-center"> Role </th>
                                            <th class="text-center"> Branch Code </th>
                                            <th class="text-center"> Branch </th>
                                            {{-- <th class="text-center"> Company </th> --}}
                                        </th>
                                    @else
                                        <tr>
                                            <!-- Header untuk super admin -->
                                            <th class="text-center" width="30px"> No </th>
                                            <th class="text-center" width="30px">Aksi</th>
                                            {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                            <th width="100px" class="text-center"> Status Aktif</th>
                                            <th class="text-center"> Username </th>
                                            <th class="text-center"> Fullname </th>
                                            <th class="text-center"> Role </th>
                                            <th class="text-center"> Company </th>
                                        </th>
                                    @endif
                                </thead>
                                <tbody>
                                    @php 
                                        $j = 1; 
                                        $k = 1;
                                    @endphp
                                    @if ( Auth::user()->user_role_id == 6)
                                       
                                        <!-- Looping untuk admin pusat [BUKAN SUPER ADMIN] -->
                                        @forelse($data as $i => $item)

                                            {{-- @if ( ($item->user_role_id != Auth::user()->user_role_id) && ($item->user_role_id != 5) ) --}}
                                                <!-- BUKAN SUPER ADMIN dan ROLE ID tidak sama dengan user role id yang LOGIN -->
                                                <tr>
                                                    <td align="center"> {{ $k }} </td>
                                                    <td align="center">
                                                        <div class="dropdown dropdown-inline">
                                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="flaticon-more"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                            <button class="dropdown-item" onclick="showEditModal('{{ $item->id }}')"><i class="la la-edit"></i> Edit </button>
                                                                <button class="dropdown-item" onclick="setPassword('{{ $item->id }}')"><i class="la la-refresh"></i> Reset Password </button>
                                                            @if($item->id != Auth::user()->id)
                                                                @if($item->is_active == 't')
                                                                    <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 't')"><i class="la la-times-circle-o"></i> Non Aktif </button>
                                                                @else 
                                                                    <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 'f')"><i class="la la-check-circle-o"></i> Aktif </button>
                                                                @endif
                                                            @endif
                                                            </div>
                                                        </div>
                                                        
                                                    </td>
                                                    <td align="center">
                                                        @if($item->is_active == 't')
                                                            <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                                        @else 
                                                            <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->username }}</td>
                                                    <td>{{ $item->fullname }}</td>
                                                    <td>{{ $item->definition }}</td>
                                                    <td>{{ $item->short_code }}</td>
                                                    <td>{{ $item->branch_name }}</td>
                                                    {{-- <td>{{ $item->company_name }}</td> --}}
                                                </tr>
                                                @php $k++; @endphp
                                            {{-- @endif --}}

                                        @empty
                                            <tr>
                                                <td colspan="8" align="center">  
                                                    Data Tidak Tersedia
                                                </td>
                                            </tr>
                                        @endforelse

                                    @else
                                    
                                        <!-- Looping untuk super admin -->
                                        @forelse($data as $i => $item)
                                            @if ( $item->user_role_id == 6)
                                                <tr>
                                                    <td align="center"> {{ $j }} </td>
                                                    <td align="center">
                                                        @if (Auth::user()->user_role_id == 5)
                                                            <div class="dropdown dropdown-inline">
                                                                <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="flaticon-more"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                <button class="dropdown-item" onclick="showEditModal('{{ $item->id }}')"><i class="la la-edit"></i> Edit </button>
                                                                    <button class="dropdown-item" onclick="setPassword('{{ $item->id }}')"><i class="la la-refresh"></i> Reset Password </button>
                                                                @if($item->id != Auth::user()->id)
                                                                    @if($item->is_active == 't')
                                                                        <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 't')"><i class="la la-times-circle-o"></i> Non Aktif </button>
                                                                    @else 
                                                                        <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 'f')"><i class="la la-check-circle-o"></i> Aktif </button>
                                                                    @endif
                                                                @endif
                                                                </div>
                                                            </div>
                                                        @elseif (Auth::user()->user_role_id == 6)
                                                            <!-- Admin Company -->
                                                                <div class="dropdown dropdown-inline">
                                                                    <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        <i class="flaticon-more"></i>
                                                                    </button>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <button class="dropdown-item" onclick="showEditModal('{{ $item->id }}')"><i class="la la-edit"></i> Edit </button>
                                                                        <button class="dropdown-item" onclick="setPassword('{{ $item->id }}')"><i class="la la-refresh"></i> Reset Password </button>
                                                                    @if($item->id != Auth::user()->id)
                                                                        @if($item->is_active == 't')
                                                                            <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 't')"><i class="la la-times-circle-o"></i> Non Aktif </button>
                                                                        @else 
                                                                            <button class="dropdown-item" onclick="setActive('{{ $item->id }}', 'f')"><i class="la la-check-circle-o"></i> Aktif </button>
                                                                        @endif
                                                                    @endif
                                                                    </div>
                                                                </div>
                                                        @endif
                                                    </td>
                                                    <td align="center">
                                                        @if($item->is_active == 't')
                                                            <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                                        @else 
                                                            <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->username }}</td>
                                                    <td>{{ $item->fullname }}</td>
                                                    <td>{{ $item->definition }}</td>
                                                    <td>{{ $item->company_name }}</td>
                                                </tr>
                                                @php $j++ @endphp
                                            @endif
                                        @empty
                                        <tr>
                                            <td colspan="8" align="center">  
                                                Data Tidak Tersedia
                                            </td>
                                        </tr>
                                        @endforelse
                                    @endif
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>

@include('reff.master_user.modal')
@include('reff.master_user.action')

@stop