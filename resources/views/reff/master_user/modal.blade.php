<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="" id="id">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" name="username">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fullname">FullName</label>
                                <input type="text" class="form-control" id="fullname" name="fullname">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group" id="container-branch">
                                <label for="single">Branch</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 branch_id" name="branch_id" id="branch_id">
                                        @php
                                            if ( Auth::user()->user_role_id != 5 ) {
                                                $branch = \DB::table('master_branch')->where('is_active', 't')->where('company_id', Auth::user()->company_id)->get();
                                            } else {
                                                // super admin
                                                $branch = \DB::table('master_branch')->where('is_active', 't')->get();
                                            }
                                        @endphp
                                            <option selected disabled value="1000">Pilih Branch</option>
                                        @forelse ($branch as $item)
                                            <option value="{{ $item->id }}">{{ $item->branch_name }}</option>
                                        @empty
                                            <option selected disabled>Branch Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="single">User Type</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 user_type" name="user_type" id="user_type" onchange="append_customer();">
                                        <option selected disabled value="1000">Pilih User Type</option>
                                        <option value="1">Internal</option>
                                        <option value="2">Client </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="single">Role</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 role_id" name="role_id" id="role_id" onchange="append_customer()">
                                        @php
                                            if ( Auth::user()->user_role_id != 5 ) {

                                                $dt_branch = \DB::TABLE('master_branch')->select('is_ho')->where('id', Auth::user()->branch_id)->first();
                                                if ( $dt_branch->is_ho == TRUE ) {
                                                    $role = \DB::table('ref_user_role')->where('is_active', 't')->whereNotIn('id', [5])->get();
                                                } else {
                                                    $role = \DB::table('ref_user_role')->where('is_active', 't')->whereNotIn('id', [5,6])->get();
                                                }


                                            } else {
                                                // super admin hanya insert admin
                                                $role = \DB::table('ref_user_role')->where('is_active', 't')->where('id', 6)->get();
                                            }

                                        @endphp
                                            <option selected disabled value="1000">Pilih Role</option>
                                        @forelse ($role as $item)
                                            <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                        @empty
                                            <option selected disabled>Role Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group" id="container-customer">
                                <label for="single">Customer</label>
                                <div class="row col-12 align-select2">
                                    @php
                                        $customer = DB::TABLE('master_customer')
                                                        ->where('wf_status_id', 9) // List Customer Active
                                                        ->whereNotNull('cust_branch_id')
                                                        ->get();
                                    @endphp
                                    <select class="form-control kt-select2 init-select2 customer" name="customer" id="customer">
                                        <option selected disabled value="1000">Pilih Customer</option>
                                        @foreach ( $customer as $item )
                                            <option value="{{ $item->id }}"> {{ $item->full_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="short_code">Short Code</label>
                                <input type="text" class="form-control" id="short_code" name="short_code">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="img_user">Image User</label>
                                <div class="col-12 align-select2">
                                    <div id="wrapperImage" class="border">
                                        <img id="imgUser" onclick="toogleZoomImg(this)" style="cursor: zoom-in; width: 100%;"/>
                                    </div>
                                    <input type="file" accept="image/png, image/jpg, image/jpeg" class="form-control" name="user_image" id="user_image" onchange="readURL(this, '1')"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            @if ( Auth::user()->user_role_id == 5)
                                <!-- Munculkan company untuk user role super admin ketika menambahkan admin -->
                                <div class="form-group" id="container-company">
                                    <label for="single">Company</label>
                                    <div class="row col-12 align-select2">
                                        <select class="form-control kt-select2 init-select2 company_id" name="id_company" id="id_company">
                                            @php
                                                if ( Auth::user()->user_role_id != 5 ) {
                                                    $companies = \DB::table('master_company')->where('is_active', 't')->where('id', Auth::user()->company_id)->get();
                                                } else {
                                                    // super admin
                                                    $companies = \DB::table('master_company')->where('is_active', 't')->get();
                                                }
                                            @endphp

                                                <option selected disabled value="1000">Pilih Perusahaan</option>
                                            @forelse ($companies as $item)
                                                <option value="{{ $item->id }}">{{ $item->company_name }}</option>
                                            @empty
                                                <option selected disabled>Perusahaan Tidak Tersedia</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>




                </form>
            </div>

            <div class="modal-footer">
                {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
