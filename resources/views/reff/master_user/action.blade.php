<script type="text/javascript">
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Silahkan isi minimal 3 sampai 30 panjang karakter. '
                        },
                        regexp: {
                            regexp: /^[a-zA-Z].*/,
                            message: 'Username harus diawali dengan huruf. '
                        }
                    }
                },
                fullname: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 75,
                            message: 'Silahkan isi minimal 3 sampai 75 panjang karakter. '
                        },
                    }
                },
                branch_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih branch'
                        },
                    }
                },
                user_type: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih user type'
                        },
                    }
                },
                role_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih role'
                        },
                    }
                },
                customer: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih customer'
                        },
                    }
                },
                short_code: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            max: 5,
                            message: 'Silahkan isi maksimal 5 karakter'
                        },
                    }
                },
                id_company: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih perusahaan'
                        },
                    }
                },
                user_image: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Pilih gambar'
                        // },
                        file: {
                            extension: 'jpeg,png,jpg',
                            type: 'image/jpeg,image/png,image/jpg',
                            maxSize: 2 * 1024 * 1024,
                            message: 'File tidak valid. Masukan file berekstensi (jpg, jpeg, png) maksimal 2 MB'
                        },
                        callback: {
                            callback: function (value, validator, $field) {

                                var fileNameClass = $field[0].className;
                                var isEdit = fileNameClass.includes('edit');
                                console.log('is edit : ' + isEdit);

                                if (!isEdit) {
                                    // insert
                                    console.log('insert image');
                                    if (!value) {
                                        $('#imgUser').attr('src', '{{ asset('img/not-available.png') }}');
                                        return {
                                            valid: false,
                                            message: 'Pilih gambar'
                                        }
                                    } else {
                                        return {
                                            valid: true,
                                        }
                                    }
                                } else {
                                    // edit
                                    console.log('edit image');
                                    var urlImg = $("#imgUser").attr('src');
                                    var defaultNameImg = 'not-available';
                                    var isValid = urlImg.includes(defaultNameImg);

                                    console.log('valid input image : ' + isValid);

                                    if (!isValid) {
                                        value = urlImg;
                                    }

                                    console.log('value input image : ' + value);

                                    if (value != '') {
                                        console.log('value ada');
                                        return {
                                            valid: true,
                                        }
                                    } else {
                                        console.log('value kosong');
                                        return {
                                            valid: false,
                                            message: 'Pilih gambar'
                                        }
                                    }

                                }
                            }
                        } // end callback

                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });


    function showModalAdd() {

        $("#container-customer").addClass('d-none');
        // disabled option
        $("#branch_id").removeAttr('disabled');
        $("#id_company").removeAttr('disabled');
        $('#id_company').val('1000').trigger('change');
        $("#form-data")[0].reset();
        $("#container-company").removeClass('d-none');
        $("#user_image").addClass('insert');
        $('#branch_id').val('1000').trigger('change');
        $('#role_id').val('1000').trigger('change');
        // $('#id_company').val('1000').trigger('change');
        $('#imgUser').attr('src', '{{ asset('img/not-available.png') }}');
        $('#form-data').bootstrapValidator("resetForm", true);
        $('#title_modal').html("Tambah User");
        $('#modal').modal('show');
        $('#id').val('');
        // $("#container-branch").addClass('d-none');

    }

    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '17');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('master_user.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('master_user') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function append_customer() {
        var value = $("#user_type").val();
        var role = $("#role_id").val();

        console.log('value : ' + value);
        console.log('role ' + role);

        if ( value != null && role != undefined )
        {
            if ( value == 2 && role == 11) {
                // User Client
                $("#container-customer").removeClass('d-none');
                $("#customer").removeAttr('disabled');
            } else {
                // User Internal dan role == client
                $("#container-customer").addClass('d-none');
                $("#customer").attr('disabled', 'disabled');
            }
        }



    }

    function settingRole(elm) {
        var id_role = $(elm).val();
        console.log('id role: ' + id_role);
        if (id_role == 5) {
            // super admin

            $("#container-branch").addClass('d-none');
            $("#container-company").addClass('d-none');

            // disabled option
            $("#branch_id").attr('disabled', 'disabled');
            $("#id_company").attr('disabled', 'disabled');
        } else if ( id_role == 6 ) {
            // role admin
            $("#container-branch").addClass('d-none');
            $("#container-company").removeClass('d-none');

            $("#branch_id").attr('disabled', 'disabled');
            $("#id_company").removeAttr('disabled');
        } else {
            // role lainnya
            $("#container-branch").removeClass('d-none');
            $("#container-company").removeClass('d-none');

            $("#branch_id").removeAttr('disabled');
            $("#id_company").removeAttr('disabled');
        }
    }

    // for upload image
    function readURL(input, seq) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var typeFile = input.files[0].type;
            var extFile = typeFile.split('/');
            var validExtensions = ['jpg', 'png', 'jpeg']; //array of valid extensions

            if ($.inArray(extFile[1], validExtensions) == -1) {
                $('#imgUser').attr('src', '{{ asset('img/not-available.png') }}');
            } else {
                reader.onload = function (e) {
                    $('#imgUser').attr('src', e.target.result);
                }
            }

            reader.readAsDataURL(input.files[0]);
        }
    } // end function

    function toogleZoomImg(el) {
        var src = $(el).attr('src');
        $('<div>').css({
            background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
            backgroundSize: 'contain',
            width: '100%',
            height: '100%',
            position: 'fixed',
            zIndex: '10000',
            top: '0',
            left: '0',
            cursor: 'zoom-out'
        }).click(function () {
            $(this).remove();
        }).appendTo('body');
    }

    function setActive(id, isActive) {
        console.log('id : ' + id);
        if (isActive == 't') {
            var titleSwal = 'Non Aktif User';
            var textSwal = 'Anda yakin akan menonaktifkan user ini?';
        } else {
            var titleSwal = 'Aktif User';
            var textSwal = 'Anda yakin akan mengaktifkan user ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('master_user.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '17'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('master_user') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    } // end function


    function setPassword(id) {

        var url_get = '{{ route('master_user.reset_password', ':id') }}';
        url_get = url_get.replace(':id', id);

        swal.fire({
            title: 'Reset Password',
            text: 'Anda yakin akan reset password user ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: url_get,

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('master_user') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });
    }

    function showEditModal(id) {
        $("#formInputSeq").addClass('d-none');
        $("#container-customer").addClass('d-none');
        $("#user_image").addClass('edit');
        $("#form-data")[0].reset();
        // $('#branch_id').val('1000').trigger('change');
        // $('#role_id').val('1000').trigger('change');
        // $('#id_company').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);



        console.log('id : ' + id);

        var url_get = '{{ route('master_user.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '17'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id").val(response.data.id);
                    $("#username").val(response.data.username);
                    $("#fullname").val(response.data.fullname);
                    $('#branch_id').val(""+response.data.branch_id+"").trigger('change');
                    $('#role_id').val(""+response.data.user_role_id+"").trigger('change');
                    $('#id_company').val(""+response.data.company_id+"").trigger('change');
                    $("#short_code").val(response.data.short_code);
                    var urlImage = '{!! url('public') !!}';
                    console.log(urlImage);
                    $("#imgUser").attr('src', base_url + '/' + response.data.image_url);

                    if ( response.data.customer_id ) {
                        // User Client
                        $("#container-customer").removeClass('d-none');
                        $("#customer").val(response.data.customer_id).trigger('change.select2');
                        $("#user_type").val('2').trigger('change.select2');
                        $("#customer").removeAttr('disabled');
                    } else {
                        // User Internal
                        $("#customer").attr('disabled', 'disabled');
                        $("#user_type").val('1').trigger('change.select2');
                    }

                    $('#title_modal').html("Edit User");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    } // end function
</script>
