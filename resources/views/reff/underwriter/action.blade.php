<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                definition: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 255,
                            message: 'Silahkan isi minimal 3 sampai 255 panjang karakter. '
                        },
                    }
                },
                branch_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih branch'
                        },
                    }
                },
                underwriter_code: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            max: 10,
                            message: 'Maksimal 10 Karakter'
                        }
                    }
                },
                address: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 255,
                            message: 'Silahkan isi minimal 3 sampai 255 panjang karakter. '
                        },
                    }
                },
                phone_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        }
                    }
                },
                pic: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        emailAddress: {
                            message: 'Format Email Salah co: contoh@gmail.com'
                        }
                    }
                },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter. '
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

    }); // end document ready
    


    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '30');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('underwriter.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('underwriter') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#formInputSeq").addClass('d-none');
        $('#title_modal').html("Tambah Underwriter");
        $('#modal').modal('show');
        // $("#branch_id").val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
        $('#id_underwriter').val('');
	    $('#seq').val('1');
    }

    function setActive(id, isActive) {
        if (isActive == 't') {
            var titleSwal = 'Non Aktif Underwriter';
            var textSwal = 'Anda yakin akan menonaktifkan proposal ini?';
        } else {
            var titleSwal = 'Aktif Underwriter';
            var textSwal = 'Anda yakin akan mengaktifkan proposal ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('underwriter.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '27'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('underwriter') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();

        console.log('id : ' + id);

        $('#form-data').bootstrapValidator("resetForm", true);
        var url_get = '{{ route('underwriter.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '30'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                $("#id_underwriter").val(response.data.id);
                $("#definition").val(response.data.definition);
                $("#underwriter_code").val(response.data.underwriter_code);
                $("#address").val(response.data.address);
                $("#phone_no").val(response.data.phone_no);
                $("#fax_no").val(response.data.fax_no);
                $("#email").val(response.data.email);
                $("#pic").val(response.data.pic);
                $("#acc_no").val(response.data.acc_no);
                $("#acc_name").val(response.data.acc_name);
                $("#bank_name").val(response.data.bank_name);
                $("#seq").val(response.data.seq);
                $("#branch_id").val(response.data.branch_id).trigger('change');
                $('#title_modal').html("Edit Underwriter");
                $('#modal').modal('show');

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>