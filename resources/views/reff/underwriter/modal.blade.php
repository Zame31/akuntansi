<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST"> 
                    @csrf
                    <input type="hidden" name="id" value="" id="id_underwriter"> 
                    <div class="form-group">
                        <label for="definition">Definisi</label>
                        <textarea class="form-control" rows="4" name="definition" id="definition"></textarea>
                    </div>
                    <div class="form-group" id="container-branch">
                        <label for="single">Branch</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 branch_id" name="branch_id" id="branch_id">
                                @php
                                    if ( Auth::user()->user_role_id != 5 ) {
                                        $branch = \DB::table('master_branch')->where('is_active', 't')->where('company_id', Auth::user()->company_id)->get(); 
                                    } else {
                                        // super admin
                                        $branch = \DB::table('master_branch')->where('is_active', 't')->get(); 
                                    }
                                @endphp
                                    <option selected disabled value="1000">Pilih Branch</option>
                                @forelse ($branch as $item)
                                    <option value="{{ $item->id }}">{{ $item->branch_name }}</option>
                                @empty
                                    <option selected disabled>Branch Tidak Tersedia</option>
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="underwriter_code">Code</label>
                        <input class="form-control" name="underwriter_code" id="underwriter_code" maxlength="10">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea class="form-control" rows="4" name="address" id="address" maxlength="255"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="phone_no">Phone No</label>
                        <input class="form-control" name="phone_no" id="phone_no" maxlength="20" onkeypress="return hanyaAngka(event)">
                    </div>
                    <div class="form-group">
                        <label for="fax_no">Fax No</label>
                        <input class="form-control" name="fax_no" id="fax_no" maxlength="20" onkeypress="return hanyaAngka(event)">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" name="email" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pic">PIC</label>
                        <input class="form-control" name="pic" id="pic" maxlength="255">
                    </div>
                    <div class="form-group">
                        <label for="acc_no">Bank Account No</label>
                        <input class="form-control" name="acc_no" id="acc_no" maxlength="100">
                    </div>
                    <div class="form-group">
                        <label for="acc_name">Bank Account Name</label>
                        <input class="form-control" name="acc_name" id="acc_name" maxlength="150">
                    </div>
                    <div class="form-group">
                        <label for="bank_name">Bank Name</label>
                        <input class="form-control" name="bank_name" id="bank_name" maxlength="150">
                    </div>
                    <div class="form-group d-none" id="formInputSeq">
                        <label for="seq">Seq</label>
                        <input type="text" class="form-control" id="seq" name="seq">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>