<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                full_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 2,
                            max: 100,
                            message: 'Silahkan isi minimal 2 sampai 100 panjang karakter. '
                        },
                        // regexp: {
                        //     regexp: /^[a-zA-Z\s]*$/,
                        //     message: 'Nama lengkap hanya boleh diisi huruf. '
                        // },
                    }
                },
                code_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 2,
                            max: 5,
                            message: 'Silahkan isi minimal 2 sampai 5 panjang karakter. '
                        },
                        regexp: {
                            regexp: /^[a-zA-Z\s]*$/,
                            message: 'Kode nama hanya boleh diisi huruf. '
                        },
                    }
                },
                address: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 5,
                            max: 255,
                            message: 'Silahkan isi minimal 5 sampai 255 panjang karakter. '
                        },
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        emailAddress: {
                            message: 'Silahkan isi dengan format email yang valid. '
                        }
                    }
                },
                phone_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'No Telepon harus diisi angka. '
                        },
                        stringLength : {
                            min: 10,
                            max: 15,
                            message: 'Silahkan isi minimal 10 sampai 15 panjang karakter. '
                        }
                    }
                },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            min: 1,
                            max: 4,
                            message: 'Silahkan isi minimal 1 sampai 4 panjang karakter. '
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });
    


    function saveData() {
        
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '25');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('agent.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                        $('#modal').modal('hide');
                        loadNewPage('{{ route('agent') }}');
                    } else {
                        toastr.warning(response.rm);
                    }
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#formInputSeq").addClass('d-none');
        $('#title_modal').html("Tambah Agent");
        $('#modal').modal('show');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id').val('');
        $('#seq').val('1');
        $("#parent_id").val('1000').trigger('change');
    }

    function setActive(id, isActive) {
        if (isActive == 't') {
            var titleSwal = 'Non Aktif Jenis Agen';
            var textSwal = 'Anda yakin akan menonaktifkan jenis agen ini?';
        } else {
            var titleSwal = 'Aktif Jenis Agen';
            var textSwal = 'Anda yakin akan mengaktifkan jenis agen ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('agent.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '25'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                            loadNewPage('{{ route('agent') }}');
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();

        console.log('id : ' + id);

        $('#form-data').bootstrapValidator("resetForm", true);
        var url_get = '{{ route('agent.show', ':id') }}';
            url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '25'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id").val(response.data.id);
                    $("#full_name").val(response.data.full_name);
                    $("#code_name").val(response.data.code_name);
                    $("#address").val(response.data.address);
                    $("#phone_no").val(response.data.phone_no);
                    $("#email").val(response.data.email);
                    $("#seq").val(response.data.seq);

                    if ( response.data.is_parent == false) {
                        console.log('punya parent');
                        $("#parent_id").val(response.data.parent_id).trigger('change');
                    } else {
                        $("#parent_id").val('1000').trigger('change');                        
                    }

                    $('#modal').modal('show');
                    $('#title_modal').html("Edit Agent");
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    
    
</script>