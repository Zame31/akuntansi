<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body">
                    <form id="form-data" method="POST"> 
                        @csrf
                        <input type="hidden" name="id" value="" id="id"> 
                        <div class="form-group">
                            <label for="full_name">Nama Lengkap</label>
                            <input type="text" class="form-control" id="full_name" name="full_name">
                        </div>
                        <div class="form-group">
                            <label for="code_name">Kode Nama</label>
                            <input type="text" class="form-control" id="code_name" name="code_name">
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea class="form-control" id="address" rows="3" name="address"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="phone_no">No Telepon</label>
                            <input type="text" class="form-control" id="phone_no" name="phone_no">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="single">Officer</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 parent_id" name="parent_id" id="parent_id">
                                    @php
                                        if ( Auth::user()->user_role_id != 5 ) {
                                            $agentParent = \DB::table('ref_agent')
                                                    ->where('company_id', Auth::user()->company_id)
                                                    ->where('is_parent', 't')
                                                    ->get();                                        
                                        } else {
                                            // super admin
                                            $prov = \DB::table('ref_province')->get();
                                        }
                                    @endphp
                                        <option value="1000" selected >Pilih officer</option>
                                    @forelse ($agentParent as $item)
                                        <option value="{{ $item->id }}">{{ $item->full_name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <span class="form-text text-muted">(*) Isi officer jika mendaftarkan sebagai agen </span>
                            </div>
                        </div>
                        <div class="form-group d-none" id="formInputSeq">
                            <label for="seq">Seq</label>
                            <input type="text" class="form-control" id="seq" name="seq">
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>