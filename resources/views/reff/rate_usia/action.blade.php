<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    var dataTable;

    $(".rate").maskMoney({
        prefix:'',
        allowNegative: false,
        thousands:',',
        decimal:'.',
        affixesStay: false,
        allowZero: true,
        precision: 2 // Tidak ada 2 digit dibelakang koma
    });

    $(document).ready(function () {    
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                usia_input: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Harus diisi angka'
                        },
                        stringLength: {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter'
                        },
                    }
                },
                tenor_input: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Harus diisi angka'
                        },
                        stringLength: {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter'
                        },
                    }
                },
                rate_input: {
                    validators: {
                        stringLength: {
                            max: 6,
                            message: 'Silahkan isi maksimal 6 karakter'
                        },
                        regexp: {
                            regexp: /^100$|^[0-9]{1,2}$|^[0-9]{1,2}\.[0-9]{1,2}$/i,
                            message: 'Format rate tidak valid'
                        },
                        callback: {
                            // message: 'Silahkan isi',
                            callback: function(value, validator, $field) {
                                if (value === '0.00' || value == '') {
                                    return {
                                        valid: false,
                                        message: 'Silahkan isi'
                                    }
                                }
                                return true;
                            }
                        }
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

        dataTable = $('#dt_table').DataTable({
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "search": {
            },
            "ajax": {
                "url": base_url + 'ref/rate_usia/getData',
                "type": "GET",
                "data": {},
                "error": function(jqXHR, textStatus, errorThrown){
                    toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                }
            },
            "autoWidth":false,
            deferRender: true,
            "columns": [
                {
                    width: "30px", 
                    orderable: false, 
                    className: "text-center",
                    searchable: false, 
                    render: function (data, type, row, meta) {
                        var page = dataTable.page.info();
                        return "<div class='text-center'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
                    }
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    className: "text-center",
                    width: "100px", 
                    render: function (data, type, row, meta) {
                        var btn = `<button class="btn btn-sm btn-outline-danger" onclick="hapus('${row.id}')"> Hapus </button>`;
                        return btn;
                    }
                },
                {data: 'usia', name: 'usia', orderable: true, searchable: true, className: "text-right"},
                {data: 'tenor', name: 'tenor', orderable: true, searchable: true, className: "text-right"},
                {
                    orderable: true, 
                    width: "200px", 
                    searchable: true, 
                    className: "text-right",
                    render: function (data, type, row, meta) {
                        var rate = parseFloat(row.rate);
                        // var rate = rate.toFixed(2);
                            
                        console.log(rate);
                        var input = `<div class="form-group mb-0">
                                        <div class="input-group">
                                            <input class="form-control text-right rate"
                                                    value="${rate}" type="number" id="rate" autocomplete="off"
                                                    name="rate" aria-describedby="basic-addon2">
                                            <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" value="${row.id}">`;
                        return input;
                    }
                },
            ],
            "order": [[2, "ASC"], [3, "ASC"]]
        });

    });

    function hapus(id) {
        swal.fire({
            title: 'Hapus Data',
            text: 'Anda yakin akan menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: base_url + '/ref/rate_usia/delete',
                    data: {
                        id: id,
                        ref_order : '102'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        endLoadingPage();
                        if (data.rc == 0) {
                            toastr.success(data.rm);
                            dataTable.draw();
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });
    } // end function

    function simpanTable() {
        var table = $('#dt_table').DataTable();

        var id = table.$("input[name='id']").serializeArray();
        var rate = table.$("input[name='rate']").serializeArray();
        console.log(rate);
        
        var is_valid = true;
        rate.forEach ( function (item, index) {
            if ( item.value == '' ) {
                is_valid = false;
                return true;
            }
        });

        if ( !is_valid ) {
            toastr.warning('Rate tidak boleh kosong');
            return false;
        }

        var data = {
            id : id,
            rate : rate
        }

        var objData = new FormData();
        objData.append('data', JSON.stringify(data));
        
        objData.append('ref_order', '102');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('rate_usia.store') }}',
            data: objData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,

            beforeSend: function () {
                loadingPage();
            },

            success: function (response) {
                endLoadingPage();
                if (response.rc == 0) {
                    toastr.success(response.rm);
                    dataTable.draw();
                }
            }

        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $('#title_modal').html("Tambah Rate Usia");
        $('#modal').modal('show');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id_ref_rate').val('');
    } // end function

    function saveData() {
        var from = $('#form-data').data('bootstrapValidator').validate();
        if (from.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);
            objData.append('ref_order', '102');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('rate_usia.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    if (response.rc == 0) {
                        $('#modal').modal('hide');
                        toastr.success(response.rm);
                        dataTable.draw();
                    } else {
                        toastr.warning(response.rm);
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif
    } // end function

    function validate_rate(elm) {
        $("#form-data").bootstrapValidator('revalidateField', $(elm).prop('name'));
    } // end function
</script>