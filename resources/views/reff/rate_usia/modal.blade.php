<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST"> 
                    @csrf
                    <input type="hidden" name="type" value="insert"> 
                    <input type="hidden" name="id_ref_rate" value="" id="id_ref_rate"> 
                    <div class="form-group">
                        <label for="usia">Usia</label>
                        <input type="text" class="form-control" id="usia_input" name="usia_input">
                    </div>
                    <div class="form-group">
                        <label for="tenor_input">Tenor</label>
                        <input type="text" class="form-control" id="tenor_input" name="tenor_input">
                    </div>
                    <div class="form-group mb-0">
                        <label for="rate_input">Rate</label>
                        <div class="input-group">
                            <input class="form-control text-right rate" onkeyup="validate_rate(this)"
                                    value="" type="text" id="rate_input"
                                    name="rate_input" aria-describedby="basic-addon2">
                            <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="fa fa-percent"></i></span></div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>