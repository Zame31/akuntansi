@section('content')
<style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Rate Usia </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Rate Usia </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Rate Usia
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button onclick="showModalAdd();" class="btn btn-primary mr-2" style="color: white;">Tambah Data</>
                                <button onclick="simpanTable();" class="btn btn-success" style="color: white;">Simpan Tabel</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="dt_table">
                            <thead>
                                <tr>
                                    <th class="text-center"> No </th>
                                    <th class="text-center">Aksi</th>
                                    <th class="text-center"> Usia </th>
                                    <th class="text-center"> Tenor </th>
                                    <th class="text-center"> Rate </th>
                                </th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                </div>

                <div class="kt-portlet__head" style="border-top: 1px solid #ebedf2;">
                    <div class="kt-portlet__head-label">
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button onclick="simpanTable();" class="btn btn-success" style="color: white;">Simpan Tabel</>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


    <!-- end:: Subheader -->
    </div>

</div>

@include('reff.rate_usia.modal')
@include('reff.rate_usia.action')

@stop
