@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Province </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Province </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Province
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button onclick="showModalAdd();" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center" width="220px">Aksi</th>
                                        <th class="text-center"> Provinsi </th>
                                    @if ( Auth::user()->user_role_id == 5 )
                                        <th class="text-center"> Company </th>
                                    @endif
                                    </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="showEditModal('{{ $item->province_code }}')">Edit</button>
                                                <button type="button" class="btn btn-outline-danger btn-sm" onclick="hapus('{{ $item->province_code }}')">Hapus</button>
                                            </td>
                                            <td>{{ $item->province_name }}</td>
                                        @if ( Auth::user()->user_role_id == 5)
                                            <td> {{ $item->company_name }} </td>
                                        @endif
                                        </tr>
                                    @empty
                                        <tr>
                                        @if ( Auth::user()->user_role_id == 5)
                                            <td colspan="4" align="center"> 
                                                Data Tidak Tersedia
                                            </td>
                                        @else
                                            <td colspan="3" align="center"> 
                                                Data Tidak Tersedia
                                            </td>
                                        @endif
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>

</div>

@include('reff.province.modal')
@include('reff.province.action')

@stop
