<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                province_code: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Kode provinsi harus diisi angka. '
                        },
                        stringLength : {
                            min: 1,
                            max: 4,
                            message: 'Silahkan isi minimal 1 sampai 4 panjang karakter. '
                        }
                    }
                },
                province_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 40,
                            message: 'Silahkan isi minimal 3 sampai 40 panjang karakter. '
                        },
                    }
                },
                id_company: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih nama perusahaan'
                        },
                    }
                },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter. '
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });
    


    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '12');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('province.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    if (response.rc == 0) {
                        endLoadingPage();
                        $('#modal').modal('hide');
                        toastr.success(response.rm);
                        loadNewPage('{{ route('province') }}');
                    } else {
                        endLoadingPage();
                        toastr.error(response.rm);
                    }
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#formInputSeq").addClass('d-none');
        $('#title_modal').html("Tambah Provinsi");
        $('#modal').modal('show');
        $('#id_company').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id_province').val('');
	    $('#seq').val('100');
    }

    function hapus(id) {
        console.log('id : ' + id);
        
        swal.fire({
            title: 'Hapus Kota',
            text: 'Anda yakin akan menghapus provinsi ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('province.delete') }}',
                    data: {
                        id: id,
                        ref_order : '12'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 0) {
                            toastr.success(data.rm);
                            loadNewPage('{{ route('province') }}');
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();
        // reset select 2
        $('#id_company').val('1000').trigger('change');
        
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id);

        var url_get = '{{ route('province.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '12'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id_province").val(response.data.province_code);
                    $("#province_code").val(response.data.province_code);
                    $("#province_name").val(response.data.province_name);
                    $('#id_company').val(""+response.data.company_id+"").trigger('change');
                    $("#seq").val(response.data.seq);
                    $('#title_modal').html("Edit Provinsi");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>