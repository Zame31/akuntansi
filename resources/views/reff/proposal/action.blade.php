<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                qs_no: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 5,
                            max: 100,
                            message: 'Silahkan isi minimal 5 sampai 100 panjang karakter. '
                        },
                    }
                },
                qs_date: {
                    validators: {
                        callback: {
                            callback: function (value, validator, $field) {
                                console.log('value : ' + value);
                                if (value != '') {
                                    // if no telp hidden (selected ATM)
                                    return { 
                                        valid: true
                                    }
                                } else {
                                    return { 
                                        valid: false,
                                        message: 'Pilih tanggal'
                                    }
                                }
                            
                            }
                        }

                    }
                },
                user_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih user'
                        }
                    }
                },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter. '
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

        $('#qs_date').on('changeDate show', function(e) {
            // Revalidate the date when user change it
            $('#form-data').bootstrapValidator('revalidateField', 'qs_date');
        });

    }); // end document ready
    


    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '19');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('proposal.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('proposal') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#formInputSeq").addClass('d-none');
        $('#title_modal').html("Tambah Proposal");
        $('#modal').modal('show');
        $('#user_id').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id_proposal').val('');
	    $('#seq').val('1');
    }

    function setActive(id, isActive) {
        if (isActive == 't') {
            var titleSwal = 'Non Aktif Proposal';
            var textSwal = 'Anda yakin akan menonaktifkan proposal ini?';
        } else {
            var titleSwal = 'Aktif Proposal';
            var textSwal = 'Anda yakin akan mengaktifkan proposal ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('proposal.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '19'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('proposal') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();
        $('#user_id').val("1000").trigger('change');

        console.log('id : ' + id);

        $('#form-data').bootstrapValidator("resetForm", true);
        var url_get = '{{ route('proposal.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '19'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                var date = new Date(response.data.qs_date);
                var month = ["January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December"][date.getMonth()];
                var str = date.getDate() + ' ' + month + ' ' + date.getFullYear();
                console.log(str);
                

                
                if (response.rc == 1) {
                    // success get data
                    $("#qs_no").val(response.data.qs_no);
                    $("#qs_date").val(str);
                    $("#id_proposal").val(response.data.id);
                    $("#seq").val(response.data.seq);
                    $('#user_id').val(""+response.data.officer_id+"").trigger('change');
                    $('#title_modal').html("Edit Proposal");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>