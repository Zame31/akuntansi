<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body">
                    <form id="form-data" method="POST"> 
                        @csrf
                        <input type="hidden" name="id" value="" id="id_proposal"> 
                        <div class="form-group">
                            <label for="qs_no">QS No</label>
                            <textarea class="form-control" rows="3" name="qs_no" id="qs_no"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="qs_date">QS Date</label>
                            <input type="text" class="form-control init-date" id="qs_date" name="qs_date" readonly placeholder="Pilih Tanggal" />
                        </div>
                        <div class="form-group">
                                <label for="single">User</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 user_id" name="user_id" id="user_id">
                                        @php
                                            $users = \DB::table('master_user')->where('is_active', 't')
                                                                                ->where('company_id', Auth::user()->company_id)->get();
                                        @endphp
                                            <option value="1000" selected disabled>Pilih User</option>
                                        @forelse ($users as $item)
                                            <option value="{{ $item->id }}">{{ $item->fullname }}</option>
                                        @empty
                                            <option selected disabled>User Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        <div class="form-group d-none" id="formInputSeq">
                            <label for="seq">Seq</label>
                            <input type="text" class="form-control" id="seq" name="seq">
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>