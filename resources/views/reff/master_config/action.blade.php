<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                parm_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            max: 100,
                            message: 'Silahkan isi maksimal 100 karakter. '
                        },
                    }
                },
                value: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Value harus diisi angka. '
                        },
                        stringLength : {
                            max: 5,
                            message: 'Silahkan isi maksimal 5 karakter. '
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });
    


    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '16');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('master_config.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('master_config') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#formInputSeq").addClass('d-none');
        $('#title_modal').html("Tambah Konfigurasi");
        $('#modal').modal('show');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id').val('');
	    $('#seq').val('1');
    }

    function setActive(id, isActive) {
        console.log('id : ' + id);
        console.log('is Active : ' + isActive);

        if (isActive == 't') {
            var titleSwal = 'Non Aktif Konfigurasi';
            var textSwal = 'Anda yakin akan menonaktifkan konfigurasi ini?';
        } else {
            var titleSwal = 'Aktif Konfigurasi';
            var textSwal = 'Anda yakin akan mengaktifkan konfigurasi ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('master_config.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '16'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('master_config') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id);

        var url_get = '{{ route('master_config.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '16'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id").val(response.data.id);
                    $("#parm_name").val(response.data.parm_name);
                    $("#value").val(response.data.value);  
                    $('#title_modal').html("Edit Konfigurasi");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>