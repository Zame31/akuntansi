@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Document Type </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Document Type </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Document Type
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button href="#basic" data-toggle="modal" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center" width="220px">Aksi</th>
                                        {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                        <th width="100px" class="text-center"> Status Aktivasi</th>
                                        <th class="text-center"> Jenis Dokumen </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                <button type="button" class="btn btn-outline-primary btn-sm">Edit</button>
                                                @if($item->is_active == 't')
                                                    <button type="button" class="btn btn-outline-danger btn-sm" onclick="setActive('f', {{ $item->id }})">Non Aktif</button>
                                                @else 
                                                    <button type="button" class="btn btn-outline-success btn-sm" onclick="setActive('t', {{ $item->id }})">Aktif</button>
                                                @endif
                                            </td>
                                            <td align="center">
                                                @if($item->is_active == 't')
                                                    <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                                @else 
                                                    <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            <td>{{ $item->definition }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3"> 
                                                Data Tidak Tersedia
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>

    <!-- Modal Add / Edit Data -->
    <div class="modal fade in" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body">
                    <form id="form-data" method="POST"> 
                        @csrf
                        <input type="hidden" name="id" value=""> 
                        <div class="form-group">
                            <label for="cust_type">Jenis Dokumen</label>
                            <input type="text" class="form-control" id="cust_type" name="cust_type">
                        </div>
                        <div class="form-group">
                            <label for="single" class="control-label">Nama Perusahaan</label>
                            <div class="row col-12">
                                <select class="form-control kt-select2 init-select2 company_id" name="id_company">
                                    @php
                                        $companies = \DB::table('master_company')->get();
                                    @endphp
                                    @forelse ($companies as $item)
                                        <option selected disabled>Pilih Nama Perusahaan</option>
                                        <option value="{{ $item->id }} ">{{ $item->company_name }}</option>
                                    @empty
                                        <option selected disabled>Nama Perusahaan Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>


@stop
@section('script')
<script type="text/javascript">
    function saveData(){
        alert('save data');
    }
    

    
</script>
@stop
