@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Bank Account </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Bank Account </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Bank Account
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button onclick="showModalAdd();" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center" width="150px">Aksi</th>
                                        {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                        <th width="100px" class="text-center"> Status Aktivasi</th>
                                        <th class="text-center"> Bank </th>
                                        <th class="text-center"> COA </th>

                                    @if ( Auth::user()->user_role_id == 5) 
                                        <th class="text-center"> Cabang </th>
                                        <th class="text-center"> Perusahaan </th>
                                    @else
                                        <th class="text-center"> Cabang </th>
                                    @endif
                                        

                                    </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="showEditModal('{{ $item->id }}')">Edit</button>
                                                @if($item->is_active == 't')
                                                    <button type="button" class="btn btn-outline-danger btn-sm" onclick="setActive('{{ $item->id }}', 't')">Non Aktif</button>
                                                @else 
                                                    <button type="button" class="btn btn-outline-success btn-sm" onclick="setActive('{{ $item->id }}', 'f')">Aktif</button>
                                                @endif
                                            </td>
                                            <td align="center">
                                                @if($item->is_active == 't')
                                                    <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                                @else 
                                                    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            <td>{{ $item->definition }}</td>
                                            <td>{{ $item->coa_no }} - {{ $item->coa_name }}</td>

                                            @if ( Auth::user()->user_role_id == 5)
                                                <td>{{ $item->branch_name }}</td>
                                                <td>{{ $item->company_name }}</td>
                                            @else 
                                                <td>{{ $item->branch_name }}</td>
                                            @endif

                                        </tr>
                                    @empty
                                        <tr>
                                            @if ( Auth::user()->user_role_id == 5)
                                            <td colspan="7" align="center">  
                                                Data Tidak Tersedia
                                            </td>
                                            @else
                                            <td colspan="6" align="center">  
                                                Data Tidak Tersedia
                                            </td>
                                            @endif
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>

@include('reff.bank_account.modal')
@include('reff.bank_account.action')

@stop