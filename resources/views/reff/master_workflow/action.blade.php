<script type="text/javascript">
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                id_company: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih company'
                        },
                    }
                },
                prev_workflow: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih previous workflow status'
                        },
                    }
                },
                current_workflow: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih current workflow status'
                        },
                    }
                },
                next_workflow: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih next workflow status'
                        },
                    }
                },
                role_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih role'
                        },
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });


    function showModalAdd() {
        $("#form-data")[0].reset();
        $('#title_modal').html("Tambah Workflow");
        $('#modal').modal('show');
        $('#id').val('');
        $('#id_company').val('1000').trigger('change');
        $('#prev_workflow').val('1000').trigger('change');
        $('#current_workflow').val('1000').trigger('change');
        $('#next_workflow').val('1000').trigger('change');
        $('#role_id').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
    }

    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '20');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('master_workflow.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    if (response.rc == 0) {
                        $('#modal').modal('hide');
                        loadNewPage('{{ route('master_workflow') }}');
                        toastr.success(response.rm);
                    } else {
                        toastr.error(response.rm);
                    }
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function
    
    function showEditModal(id, current_status_id) {
        $("#formInputSeq").addClass('d-none');
        $("#form-data")[0].reset();
        $('#id_company').val('1000').trigger('change');
        $('#prev_workflow').val('1000').trigger('change');
        $('#current_workflow').val('1000').trigger('change');
        $('#next_workflow').val('1000').trigger('change');
        $('#role_id').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id); // company_id

        var url_get = '{{ route('master_workflow.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                status_id : current_status_id,
                ref_order : '20'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id").val(response.data.company_id);
                    $("#id_current_status").val(response.data.current_status_id);
                    $('#id_company').val(""+response.data.company_id+"").trigger('change');
                    $('#current_workflow').val(""+response.data.current_status_id+"").trigger('change'); 
                    $('#prev_workflow').val(""+response.data.prev_status_id+"").trigger('change');
                    $('#next_workflow').val(""+response.data.next_status_id+"").trigger('change');
                    $('#role_id').val(""+response.data.user_role_id+"").trigger('change');
                    $('#title_modal').html("Edit Workflow");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    } // end function

    function deleteWorkflow(id_company, id_current_status) {
        console.log('id company : ' + id_company);
        console.log('id current status : ' + id_current_status);
        
        swal.fire({
            title: 'Hapus Workflow',
            text: 'Anda yakin akan menghapus workflow ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('master_workflow.delete') }}',
                    data: {
                        id: id_company,
                        id_current_status : id_current_status,
                        ref_order : '20'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 0) {
                            toastr.success(data.rm);
                            loadNewPage('{{ route('master_workflow') }}');
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }
</script>