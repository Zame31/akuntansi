<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
    
                <div class="modal-body">
                    <form id="form-data" method="POST"> 
                        @csrf
                        <input type="hidden" name="id" value="" id="id"> 
                        <input type="hidden" name="id_current_status" value="" id="id_current_status"> 
                        @if ( Auth::user()->user_role_id == 5 )
                            <div class="form-group">
                                <label for="single">Company</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 company_id" name="id_company" id="id_company">
                                        @php
                                            if ( Auth::user()->user_role_id != 5 ) {
                                                $companies = \DB::table('master_company')->where('is_active', 't')->where('id', Auth::user()->company_id)->get();                                        
                                            } else {
                                                // super admin
                                                $companies = \DB::table('master_company')->where('is_active', 't')->get();
                                            }
                                        @endphp
                                            <option selected disabled value="1000">Pilih Company</option>
                                        @forelse ($companies as $item)
                                            <option value="{{ $item->id }}">{{ $item->company_name }}</option>
                                        @empty
                                            <option selected disabled>Nama Perusahaan Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            @else
                            <!-- Admin Pusat -->
                            <div class="form-group d-none">
                                <label for="single">Company</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 company_id" name="id_company" id="id_company" disabled>
                                        @php
                                            if ( Auth::user()->user_role_id != 5 ) {
                                                $companies = \DB::table('master_company')->where('is_active', 't')->where('id', Auth::user()->company_id)->get();                                        
                                            } else {
                                                // super admin
                                                $companies = \DB::table('master_company')->where('is_active', 't')->get();
                                            }
                                        @endphp
                                            <option selected disabled value="1000">Pilih Company</option>
                                        @forelse ($companies as $item)
                                            <option value="{{ $item->id }}">{{ $item->company_name }}</option>
                                        @empty
                                            <option selected disabled>Nama Perusahaan Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            @endif
                        <div class="form-group">
                            <label for="single">Previous Workflow Status</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 prev_workflow" name="prev_workflow" id="prev_workflow">
                                    @php
                                        $companies = \DB::table('ref_workflow_status')->where('is_active', 't')->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih Workflow Status</option>
                                    @forelse ($companies as $item)
                                        <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                    @empty
                                        <option selected disabled>Workflow Status Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="single">Current Workflow Status</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 current_workflow" name="current_workflow" id="current_workflow">
                                    @php
                                        $companies = \DB::table('ref_workflow_status')->where('is_active', 't')->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih Workflow Status</option>
                                    @forelse ($companies as $item)
                                        <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                    @empty
                                        <option selected disabled>Workflow Status Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="single">Next Workflow Status</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 next_workflow" name="next_workflow" id="next_workflow">
                                    @php
                                        $companies = \DB::table('ref_workflow_status')->where('is_active', 't')->get();
                                    @endphp
                                        <option selected disabled value="1000">Pilih Workflow Status</option>
                                    @forelse ($companies as $item)
                                        <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                    @empty
                                        <option selected disabled>Workflow Status Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="single"> User Role</label>
                            <div class="row col-12 align-select2">
                                <select class="form-control kt-select2 init-select2 role_id" name="role_id" id="role_id">
                                    @php
                                        if ( Auth::user()->user_role_id != 5 ) {
                                            $companies = \DB::table('ref_user_role')->where('is_active', 't')->whereNotIn('id', [5,6])->get();
                                        } else {
                                            // super admin
                                            $companies = \DB::table('ref_user_role')->where('is_active', 't')->get();
                                        }
                                    @endphp
                                        <option selected disabled value="1000">Pilih Role</option>
                                    @forelse ($companies as $item)
                                        <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                    @empty
                                        <option selected disabled>Role Tidak Tersedia</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
    
                <div class="modal-footer">
                    {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
                </div>
    
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>