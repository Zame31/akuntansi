@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Master Workflow </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Master Workflow </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Master Workflow
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button onclick="showModalAdd();" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px" rowspan="2"> No </th>
                                        <th class="text-center" rowspan="2"> Aksi </th>
                                        @if ( Auth::user()->user_role_id == 5 )
                                        <th class="text-center" rowspan="2"> Company </th>
                                        @endif
                                        <th class="text-center" rowspan="2"> Role </th>
                                        <th class="text-center" colspan="3">Workflow Status</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"> Previous </th>
                                        <th class="text-center"> Current </th>
                                        <th class="text-center"> Next </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="flaticon-more"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <button class="dropdown-item" onclick="showEditModal('{{ $item->company_id }}', '{{ $item->current_status_id }}')"><i class="la la-edit"></i> Edit </button>
                                                        <button class="dropdown-item" onclick="deleteWorkflow('{{ $item->company_id }}', '{{ $item->current_status_id }}')"><i class="la la-trash"></i> Hapus </button>
                                                    </div>
                                                </div>
                                            </td>
                                        @if ( Auth::user()->user_role_id == 5 )
                                            <td>{{ $item->company_name }}</td>
                                        @endif
                                            <td> {{ $item->user_role }} </td>
                                            <td> {{ $item->prev_status_name }} </td>
                                            <td> {{ $item->current_status_name }} </td>
                                            <td> {{ $item->next_status_name }} </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            @if ( Auth::user()->user_role_id == 5 )
                                                <td colspan="7" align="center">  
                                                   Data Tidak Tersedia
                                               </td>
                                           @else
                                               <td colspan="6" align="center">  
                                                   Data Tidak Tersedia
                                               </td>
                                           @endif
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>

@include('reff.master_workflow.modal')
@include('reff.master_workflow.action')

@stop