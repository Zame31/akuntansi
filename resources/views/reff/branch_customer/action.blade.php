<script type="text/javascript">
    // Bootstrap Validator
        $(document).ready(function () {
            $("#form-data").bootstrapValidator({
                excluded: [':disabled'],
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    branch_name: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi. '
                            },
                            stringLength: {
                                min: 5,
                                max: 50,
                                message: 'Silahkan isi minimal 5 sampai 50 panjang karakter. '
                            },
                        }
                    },
                    address: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi. '
                            },
                            stringLength: {
                                min: 5,
                                max: 100,
                                message: 'Silahkan isi minimal 5 sampai 255 panjang karakter. '
                            },
                        }
                    },
                    province_id: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih provinsi'
                            },
                        }
                    },
                    is_ho: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih Jenis Cabang'
                            },
                        }
                    },
                    city_id: {
                        validators: {
                            notEmpty: {
                                message: 'Pilih kota'
                            },
                        }
                    },
                    // latitude: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: 'Silahkan isi. '
                    //         },
                    //         between: {
                    //             min: -90,
                    //             max: 90,
                    //             message: 'Latitude harus antara -90.0 dan 90.0. '
                    //         },
                    //         regexp: {
                    //             regexp: /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/,
                    //             message: 'Silahkan isi format latitude yang valid. '
                    //         }
                    //     }
                    // },
                    // longitude: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: 'Silahkan isi'
                    //         },
                    //         between: {
                    //             min: -180,
                    //             max: 180,
                    //             message: 'Longitude harus antara -180.0 dan 180.0. '
                    //         },
                    //         regexp: {
                    //             regexp: /^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)+$/,
                    //             message: 'Silahkan isi format longitude yang valid. '
                    //         }
                    //     }
                    // },
                    short_code: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi'
                            },
                            stringLength: {
                                max: 5,
                                message: 'Silahkan isi maksimal 5 karakter. '
                            },
                        }
                    },
                    leader_name: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi. '
                            },
                            stringLength: {
                                min: 3,
                                max: 100,
                                message: 'Silahkan isi minimal 3 sampai 100 panjang karakter. '
                            },
                            regexp: {
                                regexp: /^[a-zA-Z\s]*$/,
                                message: 'Silahkan isi hanya huruf. '
                            }
                        }
                    },
                    jabatan: {
                        validators: {
                            notEmpty: {
                                message: 'Silahkan isi. '
                            },
                            stringLength: {
                                min: 3,
                                max: 100,
                                message: 'Silahkan isi minimal 3 sampai 100 panjang karakter. '
                            },
                            regexp: {
                                regexp: /^[a-zA-Z\s]*$/,
                                message: 'Silahkan isi hanya huruf. '
                            }
                        }
                    }
                }
            }).on('success.field.bv', function (e, data) {
                var $parent = data.element.parents('.form-group');
                $parent.removeClass('has-success');
                $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
            });
        });

        function showModalAdd() {
            $("#form-data")[0].reset();
            $('#province_id').val('1000').trigger('change');
            $('#city_id').val('1000').trigger('change.select2');
            $('#form-data').bootstrapValidator("resetForm", true);
            $('#title_modal').html("Tambah Branch");
            $('#modal').modal('show');
            $('#id').val('');
        }

        function showCity(elm) {
            var codeProvince = $(elm).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: '{{ route('master_company.get_city') }}',
                data: {
                    province_code: codeProvince
                },

                beforeSend: function () {
                    loadingPage();
                    $('#select2-city_id-container').siblings().addClass('d-none');
                    $('#select2-city_id-container').parent().append(`
                        <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                            <span class="sr-only">Loading...</span>
                        </div>
                    `);
                },

                success: function (result) {
                    console.log(result);
                    $('.select2-selection__arrow').removeClass('d-none');
                    $('div.spinner-border').remove();
                    endLoadingPage();
                    var elm_option;
                    var jmlData = result.data.length;
                    if (result.rc == 1) {
                        $("#city_id").html("");
                        var def_elm_option = `<option value='1000' selected='selected' disabled> Pilih Kota </option>`;
                        $("#city_id").append(def_elm_option);
                        for(i=0; i < jmlData; i++) {
                            elm_option += `<option value="${result.data[i].city_code}"> ${result.data[i].city_name} </option>`;
                        }
                        $("#city_id").append(elm_option);
                    } else {
                        toastr.error(result.rm);
                    }
                }
            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        }

        function showCityWhenEdit(elm, id_city) {
            var codeProvince = elm;
            var idCity = id_city;
            console.log(codeProvince);
            console.log(idCity);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: '{{ route('master_company.get_city') }}',
                data: {
                    province_code: codeProvince
                },

                beforeSend: function () {
                    loadingPage();
                    $('#select2-city_id-container').siblings().addClass('d-none');
                    $('#select2-city_id-container').parent().append(`
                        <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                            <span class="sr-only">Loading...</span>
                        </div>
                    `);
                },

                success: function (result) {
                    console.log(result);
                    $('.select2-selection__arrow').removeClass('d-none');
                    $('div.spinner-border').remove();
                    endLoadingPage();
                    var elm_option;
                    var jmlData = result.data.length;
                    if (result.rc == 1) {
                        $("#city_id").html("");
                        // var default_option = `<option value='1000' selected='selected' disabled> Pilih Kota </option>`;
                        // $("#city_id").append(default_option);
                        for(i=0; i < jmlData; i++) {
                            if (result.data[i].city_code == idCity) {
                                elm_option += `<option value='${result.data[i].city_code}' selected> ${result.data[i].city_name} </option>`;
                            } else {
                                elm_option += `<option value='${result.data[i].city_code}'> ${result.data[i].city_name} </option>`;
                            }
                        }
                        $("#city_id").append(elm_option);
                    } else {
                        toastr.error(result.rm);
                    }
                }
            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        }

        function saveData() {
            var validateProduk = $('#form-data').data('bootstrapValidator').validate();
            if (validateProduk.isValid()) {
                var formData = document.getElementById("form-data");
                var objData = new FormData(formData);

                objData.append('ref_order', '100');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ route('branch_customer.store') }}',
                    data: objData,
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,

                    beforeSend: function () {
                        loadingModal();
                    },

                    success: function (response) {
                        endLoadingModal();
                        $('#modal').modal('hide');
                        if (response.rc == 0) {
                            toastr.success(response.rm);
                        }
                    }

                }).done(function (msg) {
                    loadNewPage('{{ route('branch_customer') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });

            } // endif

        } // end function

        function setActive(id, isActive) {
            console.log('id : ' + id);
            console.log('is Active : ' + isActive);

            if (isActive == 't') {
                var titleSwal = 'Non Aktif Branch Customer';
                var textSwal = 'Anda yakin akan menonaktifkan branch customer ini?';
            } else {
                var titleSwal = 'Aktif Branch Customer';
                var textSwal = 'Anda yakin akan mengaktifkan branch customer ini?';
            }

            swal.fire({
                title: titleSwal,
                text: textSwal,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: '{{ route('branch_customer.setActive') }}',
                        data: {
                            id: id,
                            active: isActive,
                            ref_order : '100'
                        },

                        beforeSend: function () {
                            loadingPage();
                        },

                        success: function (data) {
                            console.log(data);
                            endLoadingPage();
                            if (data.rc == 1) {
                                toastr.success(data.rm);
                            } else {
                                toastr.error(data.rm);
                            }
                        }
                    }).done(function (msg) {
                        endLoadingPage();
                        loadNewPage('{{ route('branch_customer') }}');
                    }).fail(function (msg) {
                        endLoadingPage();
                        toastr.error("Terjadi Kesalahan");
                    });
                }
            });

        } // end function

        function showEditModal(id) {
            // $("#formInputSeq").removeClass('d-none');
            // $("#form-data")[0].reset();
            // $('#province_id').val('1000').trigger('change');
            // $('#city_id').val('1000').trigger('change.select2');
            // $('#form-data').bootstrapValidator("resetForm", true);

            console.log('id : ' + id);

            var url_get = '{{ route('branch_customer.show', ':id') }}';
            url_get = url_get.replace(':id', id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: url_get,
                data: {
                    ref_order : '100'
                },
                beforeSend: function () {
                },

                success: function (response) {
                    console.log(response);
                    if (response.rc == 1) {
                        // success get data
                        $("#id").val(response.data.id);
                        $("#branch_name").val(response.data.branch_name);
                        $("#address").val(response.data.address);

                        $('#province_id').val(""+response.data.province_id+"").trigger('change');
                        $("#city_id").html("");
                        showCityWhenEdit(""+response.data.province_id+"", ""+response.data.city_id+"");

                        $('#city_id').val(""+response.data.city_id+"").trigger('change.select2');
                        $("#longitude").val(response.data.long);
                        $("#latitude").val(response.data.lat);
                        $("#short_code").val(response.data.short_code);
                        $("#leader_name").val(response.data.leader_name);
                        $("#jabatan").val(response.data.jabatan);
                        $('#title_modal').html("Edit Branch");
                        $('#modal').modal('show');
                    }

                }
            }).done(function (msg) {
                // $('#modal').modal('show');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        }

    </script>
