<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="" id="id">
                    <div class="form-group">
                        <label for="branch_name">Nama Cabang</label>
                        <input type="text" class="form-control" id="branch_name" name="branch_name">
                    </div>
                    <div class="form-group">
                        <label for="address">Alamat</label>
                        <textarea class="form-control" id="address" rows="3" name="address"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="single">Provinsi</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 province_id" name="province_id" id="province_id" onchange="showCity(this)">
                                @php
                                    $companies = \DB::table('ref_province')->get();
                                @endphp
                                    <option selected disabled value="1000">Pilih Provinsi</option>
                                @forelse ($companies as $item)
                                    <option value="{{ $item->province_code }}">{{ $item->province_name }}</option>
                                @empty
                                    <option selected disabled>Provinsi Tidak Tersedia</option>
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="single">Kota</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 city_id" name="city_id" id="city_id">
                                <option selected disabled value="1000">Pilih Kota</option>
                            </select>
                        </div>
                    </div>
                    @php
                        $countHO = \DB::SELECT('select count(*) as jml_ho from master_branch where company_id = ' . Auth::user()->company_id . ' and is_ho = true');
                    @endphp

                    @if ( $countHO['0']->jml_ho < 1 )
                        <div class="form-group">
                            <label for="single">Jenis Cabang</label>
                            <div class="kt-radio-inline">
                                <label class="kt-radio">
                                    <input type="radio" name="is_ho" id="ho" value="0"> Bukan Head Office
                                    <span></span>
                                </label>
                                <label class="kt-radio">
                                    <input type="radio" name="is_ho" id="bukan_ho" value="1"> Head Office
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    @endif
                    {{-- <div class="form-group">
                        <label for="longitude">Longitude</label>
                        <input type="text" class="form-control" id="longitude" name="longitude">
                    </div>
                    <div class="form-group">
                        <label for="latitude">Latitude</label>
                        <input type="text" class="form-control" id="latitude" name="latitude">
                    </div> --}}
                    <div class="form-group">
                        <label for="short_code">Short Code</label>
                        <input type="text" class="form-control" id="short_code" name="short_code">
                    </div>
                    <div class="form-group">
                        <label for="leader_name">Pemimpin</label>
                        <input type="text" class="form-control" id="leader_name" name="leader_name">
                    </div>
                    <div class="form-group">
                        <label for="jabatan">Jabatan</label>
                        <input type="text" class="form-control" id="jabatan" name="jabatan">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
