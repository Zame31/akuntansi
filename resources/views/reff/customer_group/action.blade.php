<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                group_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 255,
                            message: 'Silahkan isi minimal 3 sampai 255 panjang karakter. '
                        },
                    }
                },
                address: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 255,
                            message: 'Silahkan isi minimal 3 sampai 255 panjang karakter. '
                        },
                    }
                },
                id_company: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih nama perusahaan'
                        },
                    }
                },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter. '
                        }
                    }
                },
                no_telp: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'No Telpon harus diisi angka. '
                        },
                        stringLength : {
                            min: 10,
                            max: 20,
                            message: 'Silahkan isi minimal 10 sampai 20 panjang karakter. '
                        }
                    }
                },
                img: {
                    validators: {
                        // notEmpty: {
                        //     message: 'Pilih gambar'
                        // },
                        file: {
                            extension: 'jpeg,png,jpg',
                            type: 'image/jpeg,image/png,image/jpg',
                            maxSize: 2 * 1024 * 1024,
                            message: 'File tidak valid. Masukan file berekstensi (jpg, jpeg, png) maksimal 2 MB'
                        },
                        callback: {
                            callback: function (value, validator, $field) {

                                var fileNameClass = $field[0].className;
                                var isEdit = fileNameClass.includes('edit');
                                console.log('is edit : ' + isEdit);

                                if (!isEdit) {
                                    return {
                                        valid: true,
                                    }

                                    // insert
                                    // console.log('insert image');
                                    // if (!value) {
                                    //     $('#imgUser').attr('src', '{{ asset('img/not-available.png') }}');
                                    //     return {
                                    //         valid: false,
                                    //         message: 'Pilih gambar'
                                    //     }
                                    // } else {
                                    //     return {
                                    //         valid: true,
                                    //     }
                                    // }
                                } else {
                                    // edit
                                    console.log('edit image');
                                    var urlImg = $("#imgUser").attr('src');
                                    var defaultNameImg = 'not-available';
                                    var isValid = urlImg.includes(defaultNameImg);

                                    console.log('valid input image : ' + isValid);

                                    if (!isValid) {
                                        value = urlImg;
                                    }

                                    console.log('value input image : ' + value);

                                    if (value != '') {
                                        console.log('value ada');
                                        return {
                                            valid: true,
                                        }
                                    } else {
                                        console.log('value kosong');
                                        // return {
                                        //     valid: false,
                                        //     message: 'Pilih gambar'
                                        // }

                                        return true;
                                    }

                                }
                            }
                        } // end callback

                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#img").addClass('insert');
        $('#id_company').val('1000').trigger('change');
        $('#imgUser').attr('src', '{{ asset('img/not-available.png') }}');
        $('#form-data').bootstrapValidator("resetForm", true);
        $('#title_modal').html("Tambah Customer Group");
        $('#modal').modal('show');
        $('#id_customer_group').val('');
        $('#seq').val('1');
    }

    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '23');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('customer_group.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('customer_group') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    // for upload image
    function readURL(input, seq) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var typeFile = input.files[0].type;
            var extFile = typeFile.split('/');
            var validExtensions = ['jpg', 'png', 'jpeg']; //array of valid extensions

            if ($.inArray(extFile[1], validExtensions) == -1) {
                $('#imgUser').attr('src', '{{ asset('img/not-available.png') }}');
            } else {
                reader.onload = function (e) {
                    $('#imgUser').attr('src', e.target.result);
                }
            }

            reader.readAsDataURL(input.files[0]);
        }
    } // end function

    function toogleZoomImg(el) {
        var src = $(el).attr('src');
        $('<div>').css({
            background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
            backgroundSize: 'contain',
            width: '100%',
            height: '100%',
            position: 'fixed',
            zIndex: '10000',
            top: '0',
            left: '0',
            cursor: 'zoom-out'
        }).click(function () {
            $(this).remove();
        }).appendTo('body');
    }

    function setActive(id, isActive) {
        console.log('id : ' + id);
        if (isActive == 't') {
            var titleSwal = 'Non Aktif Customer Group';
            var textSwal = 'Anda yakin akan menonaktifkan customer group?';
        } else {
            var titleSwal = 'Aktif Customer Group';
            var textSwal = 'Anda yakin akan mengaktifkan customer group?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('customer_group.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '23'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('customer_group') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    } // end function

    function showEditModal(id) {
        $("#formInputSeq").addClass('d-none');
        $("#img").addClass('edit');
        $("#form-data")[0].reset();
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id);

        var url_get = '{{ route('customer_group.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '23'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id_customer_group").val(response.data.id);
                    $("#group_name").val(response.data.group_name);
                    $("#address").val(response.data.address);
                    $("#no_telp").val(response.data.phone_no);
                    $("#seq").val(response.data.seq);
                    $('#id_company').val(""+response.data.company_id+"").trigger('change');
                    var urlImage = '{!! asset('response.data.url_image') !!}';
                    console.log(urlImage);

                    if ( response.data.url_image ) {
                        $("#imgUser").attr('src', base_url + '/' + response.data.url_image);
                    } else {
                        $("#imgUser").attr('src', base_url + '/img/not-available.png');
                    }

                    $('#title_modal').html("Edit Customer Group");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    } // end function

</script>
