<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
    
                <div class="modal-body">
                    <form id="form-data" method="POST"> 
                        @csrf
                        <input type="hidden" name="id" value="" id="id_customer_group"> 
                        <div class="form-group">
                            <label for="group_name">Nama Group</label>
                            <input type="text" class="form-control" id="group_name" name="group_name">
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea class="form-control" id="address" name="address" rows="3"> </textarea>
                        </div>
                        <div class="form-group">
                            <label for="no_telp">No Telpon</label>
                            <input type="text" class="form-control" id="no_telp" name="no_telp">
                        </div>
                        @if ( Auth::user()->user_role_id == 5 )
                            <div class="form-group">
                                <label for="single">Nama Perusahaan</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 company_id" name="id_company" id="id_company">
                                        @php
                                            if ( Auth::user()->user_role_id != 5 ) {
                                                $companies = \DB::table('master_company')->where('is_active', 't')->where('id', Auth::user()->company_id)->get();                                        
                                            } else {
                                                // super admin
                                                $companies = \DB::table('master_company')->where('is_active', 't')->get();
                                            }
                                        @endphp
                                            <option selected disabled value="1000">Pilih Perusahaan</option>
                                        @forelse ($companies as $item)
                                            <option value="{{ $item->id }}">{{ $item->company_name }}</option>
                                        @empty
                                            <option selected disabled>Nama Perusahaan Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            @else
                            <!-- Admin Pusat -->
                            <div class="form-group d-none">
                                <label for="single">Nama Perusahaan</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 company_id" name="id_company" id="id_company" disabled>
                                        @php
                                            if ( Auth::user()->user_role_id != 5 ) {
                                                $companies = \DB::table('master_company')->where('is_active', 't')->where('id', Auth::user()->company_id)->get();                                        
                                            } else {
                                                // super admin
                                                $companies = \DB::table('master_company')->where('is_active', 't')->get();
                                            }
                                        @endphp
                                            <option selected disabled value="1000">Pilih Perusahaan</option>
                                        @forelse ($companies as $item)
                                            <option value="{{ $item->id }}">{{ $item->company_name }}</option>
                                        @empty
                                            <option selected disabled>Nama Perusahaan Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            @endif
                        <div class="form-group d-none" id="formInputSeq">
                            <label for="seq">Seq</label>
                            <input type="text" class="form-control" id="seq" name="seq">
                        </div>
                        <div class="form-group">
                            <label for="img_user">Gambar</label>
                            <div class="col-12 align-select2">
                                <div id="wrapperImage" class="border">
                                    <img id="imgUser" onclick="toogleZoomImg(this)" style="cursor: zoom-in; width: 100%;"/> 
                                </div>
                                <input type="file" accept="image/png, image/jpg, image/jpeg" class="form-control" name="img" id="img" onchange="readURL(this, '1')"/>
                            </div>
                        </div>
                    </form>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
                </div>
    
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>