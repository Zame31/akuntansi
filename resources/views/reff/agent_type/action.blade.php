<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                definition: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 5,
                            max: 100,
                            message: 'Silahkan isi minimal 5 sampai 100 panjang karakter. '
                        },
                    }
                },
                id_company: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih nama perusahaan'
                        },
                    }
                },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            min: 1,
                            max: 4,
                            message: 'Silahkan isi minimal 1 sampai 4 panjang karakter. '
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });
    


    function saveData() {
        
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '1');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('agent_type.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('agent_type') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $("#formInputSeq").addClass('d-none');
        $('#title_modal').html("Tambah Jenis Agen");
        $('#modal').modal('show');
        $('#id_company').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id').val('');
	    $('#seq').val('1');
    }

    function setActive(id, isActive) {
        if (isActive == 't') {
            var titleSwal = 'Non Aktif Jenis Agen';
            var textSwal = 'Anda yakin akan menonaktifkan jenis agen ini?';
        } else {
            var titleSwal = 'Aktif Jenis Agen';
            var textSwal = 'Anda yakin akan mengaktifkan jenis agen ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('agent_type.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '1'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('agent_type') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();
        $('#id_company').val('1000').trigger('change');

        console.log('id : ' + id);

        $('#form-data').bootstrapValidator("resetForm", true);
        var url_get = '{{ route('agent_type.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '1'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#definition").val(response.data.definition);
                    $("#seq").val(response.data.seq);
                    $("#id_agent_type").val(response.data.id);
                    $('#id_company').val(""+response.data.company_id+"").trigger('change');
                    $('#modal').modal('show');
                    $('#title_modal').html("Edit Jenis Agen");
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>