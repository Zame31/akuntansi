<script type="text/javascript">
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                // coa_no: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih COA No. '
                //         }
                //     }
                // },
                // coa_no_amor: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih COA No Amor. '
                //         }
                //     }
                // },
                bdd_name: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        stringLength: {
                            min: 3,
                            max: 150,
                            message: 'Silahkan isi minimal 3 sampai 150 panjang karakter. '
                        },
                    }
                },
                branch_id: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih branch'
                        },
                    }
                },
                // id_company: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Pilih perusahaan'
                //         },
                //     }
                // },
                seq: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi. '
                        },
                        regexp: {
                            regexp: /^\d+$/,
                            message: 'Sequence harus diisi angka. '
                        },
                        stringLength : {
                            max: 4,
                            message: 'Silahkan isi maksimal 4 karakter. '
                        }
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

    function showBranch(elm) {
        var idCompany = $(elm).val();
        console.log('id Company: ' + idCompany);

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
            $.ajax({
                type: "GET",
                url: '{{ route('bdd.get_branch') }}',
                data: {
                    id_company: idCompany
                },
    
                beforeSend: function () {
                    loadingPage();
                    $('#select2-branch_id-container').siblings().addClass('d-none');
                    $('#select2-branch_id-container').parent().append(`
                        <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                            <span class="sr-only">Loading...</span>
                        </div>
                    `);
                },
    
                success: function (result) {
                    console.log(result);
                    $('.select2-selection__arrow').removeClass('d-none');
                    $('div.spinner-border').remove();
                    endLoadingPage();
                    var elm_option;
                    var jmlData = result.data.length;
                    if (result.rc == 1) {
                        $("#branch_id").html("");
                        var def_elm_option = `<option value='1000' selected='selected' disabled> Pilih Branch </option>`;    
                        $("#branch_id").append(def_elm_option);
                        for(i=0; i < jmlData; i++) {
                            elm_option += `<option value="${result.data[i].id}"> ${result.data[i].branch_name} </option>`;
                        }
                        $("#branch_id").append(elm_option);
                    } else {
                        toastr.error(result.rm);
                    }
                }
            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
    }

    function showBranchWhenEdit(elm, id_branch) {
        var idCompany = elm;
        var idBranch = id_branch;
        console.log(idCompany);
        console.log(idBranch);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('bdd.get_branch') }}',
            data: {
                id_company: idCompany
            },

            beforeSend: function () {
                loadingPage();
                $('#select2-branch_id-container').siblings().addClass('d-none');
                $('#select2-branch_id-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);
            },

            success: function (result) {
                console.log(result);
                $('.select2-selection__arrow').removeClass('d-none');
                $('div.spinner-border').remove();
                endLoadingPage();
                var elm_option;
                var jmlData = result.data.length;
                if (result.rc == 1) {
                    $("#branch_id").html("");
                    // var default_option = `<option value='1000' selected='selected' disabled> Pilih Kota </option>`;    
                    // $("#city_id").append(default_option);
                    for(i=0; i < jmlData; i++) {
                        if (result.data[i].id == idBranch) {
                            elm_option += `<option value='${result.data[i].id}' selected> ${result.data[i].branch_name} </option>`;    
                        } else {
                            elm_option += `<option value='${result.data[i].id}'> ${result.data[i].branch_name} </option>`;
                        }
                    }
                    $("#branch_id").append(elm_option);
                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }


    function showCOA(elm) {
        var id_branch = $(elm).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('bdd.get_coa') }}',
            data: {
                idBranch: id_branch
            },

            beforeSend: function () {
                loadingPage();
                $('#select2-coa_no-container').siblings().addClass('d-none');
                $('#select2-coa_no-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);

                $('#select2-coa_no_amor-container').siblings().addClass('d-none');
                $('#select2-coa_no_amor-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);
            },

            success: function (result) {
                console.log(result);
                $('.select2-selection__arrow').removeClass('d-none');
                $('div.spinner-border').remove();
                endLoadingPage();
                var elm_option;
                var elm;
                var def;
                var jmlData = result.data.length;
                if (result.rc == 1) {
                    $("#coa_no").html("");
                    var default_option = `<option value='1000' selected='selected' disabled> Pilih COA No </option>`;    
                    $("#coa_no").append(default_option);
                    for(i=0; i < jmlData; i++) {
                        elm_option += `<option value='${result.data[i].coa_no}'> ${result.data[i].coa_no} -  ${result.data[i].coa_name}</option>`;
                    }
                    $("#coa_no").append(elm_option);

                    $("#coa_no_amor").html("");
                    def = `<option value='1000' selected='selected' disabled> Pilih COA No Amor </option>`;    
                    $("#coa_no_amor").append(def);
                    for(i=0; i < jmlData; i++) {
                        elm += `<option value='${result.data[i].coa_no}'> ${result.data[i].coa_no} -  ${result.data[i].coa_name}</option>`;
                    }
                    $("#coa_no_amor").append(elm);

                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function showCOAWhenEdit(id_branch, coa_no, coa_no_amor) {
        var id_branch = id_branch;
        var coaNo = coa_no;
        var coaNoAmor = coa_no_amor;


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '{{ route('bdd.get_coa') }}',
            data: {
                idBranch: id_branch
            },

            beforeSend: function () {
                loadingPage();
                $('#select2-coa_no-container').siblings().addClass('d-none');
                $('#select2-coa_no-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);

                $('#select2-coa_no_amor-container').siblings().addClass('d-none');
                $('#select2-coa_no_amor-container').parent().append(`
                    <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                        <span class="sr-only">Loading...</span>
                    </div>
                `);

                $("#coa_no").html("");
                $("#coa_no_amor").html("");
            },

            success: function (result) {
                console.log(result);
                $('.select2-selection__arrow').removeClass('d-none');
                $('div.spinner-border').remove();
                endLoadingPage();
                var elm_option;
                var elm;
                var deft;
                var jmlData = result.data.length;
                if (result.rc == 1) {
                    $("#coa_no").html("");
                    var default_option = `<option value='1000' selected='selected' disabled> Pilih COA No </option>`;    
                    $("#coa_no").append(default_option);
                    for(i=0; i < jmlData; i++) {
                        if (result.data[i].coa_no == coaNo) {
                            elm_option += `<option value='${result.data[i].coa_no}' selected> ${result.data[i].coa_no} -  ${result.data[i].coa_name} </option>`;    
                        } else {
                            elm_option += `<option value='${result.data[i].coa_no}'> ${result.data[i].coa_no} -  ${result.data[i].coa_name} </option>`;
                        }
                    }
                    $("#coa_no").append(elm_option);

                    $("#coa_no_amor").html("");
                    deft = `<option value='1000' selected='selected' disabled> Pilih COA No Amor </option>`;    
                    $("#coa_no_amor").append(deft);
                    for(i=0; i < jmlData; i++) {
                        if (result.data[i].coa_no == coaNoAmor) {
                            elm += `<option value='${result.data[i].coa_no}' selected> ${result.data[i].coa_no} -  ${result.data[i].coa_name} </option>`;    
                        } else {
                            elm += `<option value='${result.data[i].coa_no}'> ${result.data[i].coa_no} -  ${result.data[i].coa_name} </option>`;
                        }
                    }
                    $("#coa_no_amor").append(elm);

                } else {
                    toastr.error(result.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function showModalAdd() {
        $("#form-data")[0].reset();
        $('#title_modal').html("Tambah BDD");
        $('#modal').modal('show');
        $('#branch_id').val('1000').trigger('change');
        $('#coa_no').val('1000').trigger('change');
        $('#coa_no_amor').val('1000').trigger('change');
        // $('#id_company').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
        $("#formInputSeq").addClass('d-none');
        $('#id').val('');
        $('#seq').val('1');
        $('#coa_no').val('1000');
    }

    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    
        if (validateProduk.isValid()) {
            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '24');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('bdd.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    endLoadingPage();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }
                }

            }).done(function (msg) {
                loadNewPage('{{ route('bdd') }}');
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function setActive(id, isActive) {
        console.log('id : ' + id);
        console.log('is Active : ' + isActive);

        if (isActive == 't') {
            var titleSwal = 'Non Aktif BDD';
            var textSwal = 'Anda yakin akan menonaktifkan BDD ini?';
        } else {
            var titleSwal = 'Aktif BDD';
            var textSwal = 'Anda yakin akan mengaktifkan BDD ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('bdd.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '24'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    loadNewPage('{{ route('bdd') }}');
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    } // end function 

    function showEditModal(id) {
        $("#formInputSeq").removeClass('d-none');
        $("#form-data")[0].reset();
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id);

        var url_get = '{{ route('bdd.show', ':id') }}';
            url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '24'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $('#modal').modal('show');
                    $('#title_modal').html("Edit BDD");
                    $("#id").val(response.data.id);
                    // $("#coa_no").val(response.data.coa_no).trigger('change');
                    // $("#coa_no_amor").val(response.data.coa_no_amor).trigger('change');

                    // $('#branch_id').val(""+response.data.branch_id+"").trigger('change');
                    // $('#id_company').val(""+response.data.company_id+"").trigger('change');
                    $("#branch_id").html("");
                    showBranchWhenEdit(""+response.data.company_id+"", ""+response.data.branch_id+"");

                    showCOAWhenEdit(""+response.data.branch_id+"", ""+response.data.coa_no+"", ""+response.data.coa_no_amor+"");


                    $("#bdd_name").val(response.data.bdd_name);
                    $("#seq").val(response.data.seq);
                    
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

</script>