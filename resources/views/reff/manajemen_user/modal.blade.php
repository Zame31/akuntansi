<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST"> 
                    @csrf
                    <input type="hidden" name="id" value="" id="id_manajemen_user"> 
                    <div class="form-group">
                        <label for="single">User Role</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 menu" name="id_role" id="id_role">
                                @php
                                    $role = \DB::table('ref_user_role')->where('is_active', 't')->get();
                                @endphp
                                    <option value="1000" selected disabled>Pilih User Role</option>
                                @forelse ($role as $item)
                                    <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                @empty
                                    <option selected disabled>User Role Tidak Tersedia</option>
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="single">Sub Menu</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 menu" name="id_sub_menu" id="id_sub_menu">
                                @php
                                    $subMenu = \DB::table('ref_sub_menu')->get();
                                @endphp
                                    <option value="1000" selected disabled>Pilih Sub Menu</option>
                                @forelse ($subMenu as $item)
                                    <option value="{{ $item->id }}">{{ $item->menu }}</option>
                                @empty
                                    <option selected disabled>Sub Menu Tidak Tersedia</option>
                                @endforelse
                            </select>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                {{-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button> --}}
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>