<script type="text/javascript">
    // Bootstrap Validator
    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                id_role: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih User Role'
                        },
                    }
                },
                id_sub_menu: {
                    validators: {
                        notEmpty: {
                            message: 'Pilih Sub Menu'
                        },
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });
    


    function saveData() {
        var validateProduk = $('#form-data').data('bootstrapValidator').validate();
        if (validateProduk.isValid()) {
            var id = $("#id_manajemen_user").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '28');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('manajemen_user.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    if (response.rc == 0) {
                        endLoadingPage();
                        $('#modal').modal('hide');
                        toastr.success(response.rm);
                        loadNewPage('{{ route('manajemen_user') }}');
                    } else {
                        endLoadingPage();
                        toastr.error(response.rm);
                    }
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif

    } // end function

    function showModalAdd() {
        $("#form-data")[0].reset();
        $('#title_modal').html("Tambah Manajemen User");
        $('#modal').modal('show');
        $('#id_sub_menu').val('1000').trigger('change');
        $('#id_role').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);
	    $('#id_manajemen_user').val('');
    }

    function hapus(id) {
        console.log('id : ' + id);
        
        swal.fire({
            title: 'Hapus Manajemen User',
            text: 'Anda yakin akan menghapus manajemen user ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('manajemen_user.delete') }}',
                    data: {
                        id: id,
                        ref_order : '28'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 0) {
                            toastr.success(data.rm);
                            loadNewPage('{{ route('manajemen_user') }}');
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function showEditModal(id) {
        $("#form-data")[0].reset();
        // reset select 2
        $('#id_sub_menu').val('1000').trigger('change');
        $('#id_role').val('1000').trigger('change');
        $('#form-data').bootstrapValidator("resetForm", true);

        console.log('id : ' + id);

        var url_get = '{{ route('manajemen_user.show', ':id') }}';
            url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '28'
            },
            beforeSend: function () {
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    // success get data
                    $("#id_manajemen_user").val(response.data.id);
                    $('#id_role').val(""+response.data.idrole+"").trigger('change');
                    $('#id_sub_menu').val(""+response.data.id_sub_menu+"").trigger('change.select2');
                    $('#title_modal').html("Edit Manajemen User");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            // $('#modal').modal('show');
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }
    
</script>