@section('content')
<div class="app-content">
    <div class="section">
    
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Branch Monitoring </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Branch Monitoring </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Branch Monitoring
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        {{-- <div class="row">
                            <div class="col-12">
                                <button onclick="showModalAdd();" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center" width="150px">Aksi</th>
                                        {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                        <th width="100px" class="text-center"> Status Cabang</th>
                                        <th width="250px" class="text-center"> Nama Cabang </th>
                                        <th class="text-center"> Provinsi </th>
                                        <th class="text-center"> Kota </th>
                                    </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                @if($item->is_open == 't')
                                                    <button type="button" class="btn btn-outline-danger btn-sm" onclick="setOpen('{{ $item->id }}', 'f')">Close</button>
                                                @else 
                                                    <button type="button" class="btn btn-outline-success btn-sm" onclick="setOpen('{{ $item->id }}', 't')">Open</button>
                                                @endif
                                            </td>
                                            <td align="center">
                                                @if($item->is_open == 't')
                                                    <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Open</span>
                                                @else 
                                                    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Close</span>
                                                @endif
                                            </td>
                                            <td>{{ $item->branch_name }}</td>
                                            <td>{{ $item->province_name }}</td>
                                            <td>{{ $item->city_name }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" align="center">  
                                                Data Tidak Tersedia
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>

<script type="text/javascript">
    function setOpen(id, isOpen) {
            console.log('id : ' + id);
            console.log('is Open : ' + isOpen);
    
            if (isOpen == 't') {
                var titleSwal = 'Open Branch';
                var textSwal = 'Anda yakin akan membuka branch ini?';
            } else {
                var titleSwal = 'Close Branch';
                var textSwal = 'Anda yakin akan menutup branch ini?';
            }
    
            swal.fire({
                title: titleSwal,
                text: textSwal,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        type: "GET",
                        url: '{{ route('master_branch.setOpen') }}',
                        data: {
                            id: id,
                            open: isOpen,
                        },
    
                        beforeSend: function () {
                            loadingPage();
                        },
    
                        success: function (data) {
                            console.log(data);
                            endLoadingPage();
                            if (data.rc == 1) {
                                toastr.success(data.rm);
                                loadNewPage('{{ route('branch_monitoring') }}');

                            } else {
                                toastr.error(data.rm);
                            }
                        }
                    }).done(function (msg) {
                        endLoadingPage();
                    }).fail(function (msg) {
                        endLoadingPage();
                        toastr.error("Terjadi Kesalahan");
                    });
                }
            });
    
        } // end function 
</script>


@stop