<script type="text/javascript">
$(document).ready(function () {

    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            product: {
                validators: {
                    notEmpty: {
                        message: 'Pilih product'
                    },
                }
            },
            'label[]': {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength: {
                        max: 30,
                        message: 'Silahkan isi maksimal 30 karakter'
                    },
                    regexp: {
                        regexp: /^[\w ]+$/,
                        message: 'Format salah. Silakan diisi huruf, angka, underscore atau spasi'
                    },
                }
            },
            'format_input[]': {
                validators: {
                    notEmpty: {
                        message: 'Silakan pilih'
                    },
                }
            },
            'is_required[]': {
                validators: {
                    notEmpty: {
                        message: 'Silakan pilih'
                    },
                }
            },
            'is_showing[]': {
                validators: {
                    notEmpty: {
                        message: 'Silakan pilih'
                    },
                }
            },
            'is_filterable[]': {
                validators: {
                    notEmpty: {
                        message: 'Silakan pilih'
                    },
                }
            },
        }
    })

        // Add button click handler
    .on('click', '.addButton', function() {
        event.preventDefault();
        var $template = $('#label_template'),
            $clone    = $template
                            .clone()
                            .removeClass('hide')
                            .removeClass('d-none')
                            .addClass('d-flex')
                            .removeAttr('id')
                            .insertBefore($template),
            $option   = $clone.find('.dynamic_field').removeAttr('disabled');

            $('html,body').animate({
                scrollTop: $(document).height()
            },'slow');

            $(this).addClass('d-none');

            // if ( $(this).hasClass('tambahan') ) {
            //     $(this).addClass('d-none');
            // } else {

            // }

        // Add new field
        $('#form-data').bootstrapValidator('addField', $option);
    })

    // Remove button click handler
    .on('click', '.removeButton', function() {
        event.preventDefault();
        var $row    = $(this).parents('.container-additional'),
            $option   = $row.find('.dynamic_field');

        // Remove element containing the option
        $row.remove();

        console.log($('.container-additional:last-child'));
        $('.container-additional').last().prev().find('button.tambahan').removeClass('d-none');

        if ( $('.container-additional:not(.d-none)').length == 0 ) {
            // show button in first element
            $("#btn-add-first").removeClass('d-none');
        };



        // Remove field
        $('#form-data').bootstrapValidator('removeField', $option);
    })

    .on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });


}); // end document ready

    function showEditModal(id)
    {
        console.log('id : ' + id);

        var url_get = '{{ route('insured_detail.show', ':id') }}';
        url_get = url_get.replace(':id', id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url_get,
            data: {
                ref_order : '101'
            },
            beforeSend: function () {
                loadingPage();
            },

            success: function (response) {
                console.log(response);
                if (response.rc == 1) {
                    var data = response.data;

                    $("#id").val(id);
                    $("#product_id").val(data.product_id);
                    $("#product").val(data.definition);
                    $("#label").val(data.label);

                    if ( data.is_number == null ) {
                        data.is_number = false;
                    }

                    if ( data.is_currency == null ) {
                        data.is_currency = false;
                    }

                    if ( data.is_filterable == null ) {
                        data.is_filterable = false;
                    }

                    if ( data.is_rate == null ) {
                        data.is_rate = false;
                    }

                    if ( data.is_required == null ) {
                        data.is_required = false;
                    }

                    if ( data.is_showing == null ) {
                        data.is_showing = false;
                    }


                    console.log(data.is_currency);
                    console.log(data.is_number);
                    console.log(data.is_currency);
                    console.log(data.is_rate);

                    if ( data.is_currency || (data.is_number && data.is_currency) ) {
                        // Format mata uang
                        console.log('format mata uang');
                        $("#format_input").val('2').trigger('change.select2');
                    } else if ( data.is_number && !data.is_rate ) {
                        console.log('format number');
                        $("#format_input").val('1').trigger('change.select2');
                    } else if ( data.is_rate && data.is_number ) {
                        console.log('format presentase');
                        $("#format_input").val('3').trigger('change.select2');
                    } else if ( data.is_date && !data.is_rate && !data.is_number && !data.is_currency) {
                        console.log('format tanggal');
                        $("#format_input").val('5').trigger('change.select2');
                    } else {
                        $("#format_input").val('4').trigger('change.select2');
                        console.log('salah');
                    }


                    $("input[name='is_number'][value='"+data.is_number+"']").prop('checked', true);
                    $("input[name='is_currency'][value='"+data.is_currency+"']").prop('checked', true);
                    $("input[name='is_filterable'][value='"+data.is_filterable+"']").prop('checked', true);
                    $("input[name='is_rate'][value='"+data.is_rate+"']").prop('checked', true);
                    $("input[name='is_required'][value='"+data.is_required+"']").prop('checked', true);
                    $("input[name='is_showing'][value='"+data.is_showing+"']").prop('checked', true);

                    // success get data
                    $('#title_modal').html("Edit Insured Detail");
                    $('#modal').modal('show');
                }

            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });
    }

    function saveEditSingle()
    {
        var validate = $('#form-edit-single').data('bootstrapValidator').validate();
        if (validate.isValid()) {
            var formData = document.getElementById("form-edit-single");
            var objData = new FormData(formData);

            objData.append('ref_order', '101');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('insured_detail.store_single_input') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingModal();
                },

                success: function (response) {
                    endLoadingModal();
                    $('#modal').modal('hide');
                    if (response.rc == 0) {
                        toastr.success(response.rm);
                        loadNewPage('{{ route('insured_detail') }}');
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // end if

    } // end function

    function store() {
        var validate = $('#form-data').data('bootstrapValidator').validate();
        if (validate.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            objData.append('ref_order', '101');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('insured_detail.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    console.log(response);
                    if ( response.rc == 0 ) {

                        if ( !$("#id").val() ) {
                            var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
                            var action = `<button onclick="znView()" type="button"
                                                class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                                                Data</button>
                                            <button onclick="znClose()" type="button"
                                                class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                                                Data</button>`;
                        } else {
                            var text = `Data berhasil disimpan`;
                            var action = `<button onclick="znView()" type="button"
                                                class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                                                Data</button>`;
                        }

                        znIconbox("Sukses",text,action);

                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif
    } // end function

    function znView() {
        znIconboxClose();
        loadNewPage('{{ route('insured_detail') }}');
    }

    function znClose() {
        znIconboxClose();
        $("#form-data")[0].reset();
        document.getElementById("form-data").reset();
        $('#form-data').bootstrapValidator("resetForm", true);
        loadNewPage('{{ route('insured_detail.create') }}');
    }

    function selectData(type) {
        let value = [];
        var titleSwal, textSwal, route;

        $('td input[type="checkbox"]').each(function(){
            if(this.checked){
                value.push(this.value);
            }
        });

        if(value.length == 0){
            toastr.warning('Minimal pilih satu data');
        }else{

            switch ( type ) {
                case 'non_aktif' :
                    titleSwal = 'Non Aktif Data';
                    textSwal = 'Anda yakin akan menonaktifkan data yang dipilih?';
                break;
                case 'aktif' :
                    titleSwal = 'Aktif Data';
                    textSwal = 'Anda yakin akan mengaktifkan data yang dipilih?';
                break;
                case 'non_required' :
                    titleSwal = 'Non Required Data';
                    textSwal = 'Anda yakin akan membuat data yang dipilih menjadi tidak wajib diisi?';
                break;
                case 'required' :
                    titleSwal = 'Required Data';
                    textSwal = 'Anda yakin akan membuat data yang dipilih menjadi wajib diisi?';
                break;
                case 'non_filterable' :
                    titleSwal = 'Non Filterable Data';
                    textSwal = 'Anda yakin akan membuat data yang dipilih menjadi tidak filterable?';
                break;
                case 'filterable' :
                    titleSwal = 'Filterable';
                    textSwal = 'Anda yakin akan membuat data yang dipilih menjadi filterable?';
                break;
            }

            var route = base_url + '/ref/insured_detail/change_data_selected'
            swal.fire({
                title: titleSwal,
                text: textSwal,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: "GET",
                        url: route,
                        data: {
                            type : type,
                            data: value
                        },

                        beforeSend: function () {
                            loadingPage();
                        },

                        success: function (data) {
                            console.log(data);
                            if (data.rc == 1) {
                                toastr.success(data.rm);
                                location.reload();
                            } else {
                                toastr.error("Terjadi Kesalahan");
                            }
                        }
                    }).done(function (msg) {
                        endLoadingPage();
                    }).fail(function (msg) {
                        endLoadingPage();
                        toastr.error("Terjadi Kesalahan");
                    });
                }
            });

        }
    } // end function

    function setActive(id, isActive) {
        if (isActive == 't') {
            var titleSwal = 'Non Aktif Insured Detail';
            var textSwal = 'Anda yakin akan menonaktifkan insured detail ini?';
        } else {
            var titleSwal = 'Aktif Insured Detail';
            var textSwal = 'Anda yakin akan mengaktifkan insured detail ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: base_url + '/ref/insured_detail/active',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '101'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                            location.reload();
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    function hapusData(id, product_id)
    {

        swal.fire({
            title: 'Hasil Data Insured Detail',
            text: 'Anda yakin akan menghapus data insured detail ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: base_url + '/ref/insured_detail/delete',
                    data: {
                        id: id,
                        product_id : product_id,
                        ref_order : '101'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        endLoadingPage();
                        if (data.rc == 0) {
                            toastr.success(data.rm);
                            location.reload();
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }

    // TEMPLATE EXCEL


    function setProduct(product_id) {

            console.log(product_id);

            $.ajax({
            type: "GET",
            url: base_url + 'marketing/data/list_template_excel/' + product_id,
            data: {},

            beforeSend: function () {
                loadingPage();
            },

            success: function (res) {
                var data = res.data;
                console.log(data);

                if (res.rc == 1) {
                    var tableHeaders = "";
                    let list = [];
                    list[1] = "";
                    list[2] = "";
                    list[3] = "";

                    let idx_list = 1;

                    data.forEach(function ( value, index) {

                        if (value.col_list) {
                            list[value.col_list] += `<div class="btn btn-outline-secondary btn-sm text-left w-100 my-2" style="cursor: pointer;">${value.label}
                                            <input type="hidden" value="${value.input_name}" name="list_template_excel[]" >
                                            <input type="hidden" value="" name="col_template_excel[]" class="set_col">
                                        </div>
                                        `;

                        }else{
                            if (idx_list == 1 || idx_list == 2) {
                                list[idx_list] += `<div class="btn btn-outline-secondary btn-sm text-left w-100 my-2" style="cursor: pointer;">${value.label}
                                                <input type="hidden" value="${value.input_name}" name="list_template_excel[]" >
                                                <input type="hidden" value="" name="col_template_excel[]" class="set_col">
                                            </div>
                                            `;
                                idx_list++;
                            }
                            else if(idx_list == 3){
                                list[idx_list] += `<div class="btn btn-outline-secondary btn-sm text-left w-100 my-2" style="cursor: pointer;">${value.label}
                                                <input type="hidden" value="${value.input_name}" name="list_template_excel[]" >
                                                <input type="hidden" value="" name="col_template_excel[]" class="set_col">
                                            </div>
                                            `;
                                idx_list = 1;
                            }
                        }

                    })

                    $("#list_co_1").empty();
                    $("#list_co_1").append(list[1]);
                    $("#list_co_2").empty();
                    $("#list_co_2").append(list[2]);
                    $("#list_co_3").empty();
                    $("#list_co_3").append(list[3]);

                    $('.sortable').sortable({
                        connectWith: ".connectedSortable"
                    }).disableSelection();

                } else {
                    toastr.error(res.rm);
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error("Terjadi Kesalahan");
        });

    }

    function modal_download_template() {
        $('#ex_product').val(null).select2();
        $("#list_co_1").empty();
        $("#list_co_2").empty();
        $("#list_co_3").empty();
        $('#template_excel').modal('show');

    }

    function storeListTemplate() {

        $('#list_co_1 .set_col').val(1);
        $('#list_co_2 .set_col').val(2);
        $('#list_co_3 .set_col').val(3);

        var _create_form = $("#excel-template-form");
        var _form_data = new FormData(_create_form[0]);

        $.ajax({
            type: 'POST',
            url: base_url + '/marketing/store/list_template',
            data: _form_data,
            processData: false,
            contentType: false,
            dataType: 'json',
            beforeSend: function () {
                loadingPage();
            },
            success: function (res) {
            }
        }).done(function (res) {
            var obj = res;
            endLoadingPage();
            $('#template_excel').modal('hide');

            if (obj.rc == 1) {
                toastr.success('Data Berhasil Disimpan !');
            } else {
                swal.fire("Info", obj.rm, "info");
            }

        }).fail(function (res) {
            endLoadingPage();
            swal.fire("Error", "Terjadi Kesalahan!", "error");
        });
    }



    // TEMPALTE EXCEL
</script>
