@section('content')
<style>
    .container-input {
        margin: 10px;
        width: 190px;
    }

    .container-input:last-child {
        width: 70px;
    }
</style>
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Insured Detail </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Create </a>
                    </div>
            </div>
        </div>
    </div>



    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-form">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Form Create Insured Detail</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a onclick="loadNewPage('{{ route('insured_detail') }}')" class="btn btn-clean kt-margin-r-10" style="cursor: pointer;">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success" onclick="store();">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Save</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body" style="background: #fbfbfb;">
                        <form class="kt-form" id="form-data">

                            <div class="row">
                                <label class="col-form-label col-md-1">Product</label>
                                <div class="form-group col-md-6">
                                    <select class="form-control kt-select2 init-select2" name="product" id="product">
                                        <option value="1000" disabled selected>Select Product</option>
                                        @forelse ($product as $item)
                                            <option value="{{ $item->id }}">{{ $item->definition }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>

                            <div class="container-detail-vehicle bg-white" style="padding: 20px;border-radius: 4px;box-shadow: inset 0px 0px 8px 0px #0000005c">
                                <h4 class="kt-section__title kt-section__title-lg">Input Details</h4>
                                <span class="d-block mb-4">Label <span style="font-weight: 600">Sum Insured, Premium, Total Additional Premi, </span> dan <span style="font-weight: 600"> Gross Premium </span> sudah ditambahkan secara otomatis</span>

                                <div class="row d-flex">
                                    <div class="container-input">
                                        <label class="col-form-label">Label</label>
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="label[]">
                                        </div>
                                    </div>
                                    <div class="container-input">
                                        <label class="col-form-label">Format Input</label>
                                        <div class="form-group">
                                            <select class="form-control format_input" name="format_input[]">
                                                <option value="">Pilih Format Input</option>
                                                <option value="1">Angka</option>
                                                <option value="2">Mata Uang</option>
                                                <option value="3">Persentase</option>
                                                <option value="4">Text</option>
                                                <option value="5">Tanggal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="container-input">
                                        <label class="col-form-label">Wajib Diisi</label>
                                        <div class="form-group">
                                            <select class="form-control" name="is_required[]">
                                                <option value="">Pilih Pilihan</option>
                                                <option value="t">Ya</option>
                                                <option value="f">Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="container-input">
                                        <label class="col-form-label">Tampil Header Detail</label>
                                        <div class="form-group">
                                            <select class="form-control" name="is_showing[]">
                                                <option value="">Pilih Pilihan</option>
                                                <option value="t">Ya</option>
                                                <option value="f">Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="container-input">
                                        <label class="col-form-label">Filterable</label>
                                        <div class="form-group">
                                            <select class="form-control" name="is_filterable[]">
                                                <option value="">Pilih Pilihan</option>
                                                <option value="t">Ya</option>
                                                <option value="f">Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="container-input" style="position: relative;top: 39px;">
                                        <button class="btn btn-sm btn-icon btn-outline-success addButton" id="btn-add-first">
                                            <i class="flaticon2-plus"></i>
                                        </button>
                                    </div>
                                </div>

                                <div class="row container-additional d-none" id="label_template" style="border-top: 1px dashed #ebedf2;">

                                    <div class="container-input">
                                        <label class="col-form-label">Label</label>
                                        <div class="form-group">
                                            <input class="form-control dynamic_field" type="text" name="label[]" disabled="disabled">
                                        </div>
                                    </div>

                                    <div class="container-input">
                                        <label class="col-form-label">Format Input</label>
                                        <div class="form-group">
                                            <select class="form-control format_input dynamic_field" name="format_input[]" disabled="disabled">
                                                <option value="">Pilih Format Input</option>
                                                <option value="1">Angka</option>
                                                <option value="2">Mata Uang</option>
                                                <option value="3">Persentase</option>
                                                <option value="4">Text</option>
                                                <option value="5">Tanggal</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="container-input">
                                        <label class="col-form-label">Wajib Diisi</label>
                                        <div class="form-group">
                                            <select class="form-control dynamic_field" name="is_required[]" disabled="disabled">
                                                <option value="">Pilih Pilihan</option>
                                                <option value="t">Ya</option>
                                                <option value="f">Tidak</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="container-input">
                                        <label class="col-form-label">Tampil Header Detail</label>
                                        <div class="form-group">
                                            <select class="form-control dynamic_field" name="is_showing[]" disabled="disabled">
                                                <option value="">Pilih Pilihan</option>
                                                <option value="t">Ya</option>
                                                <option value="f">Tidak</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="container-input">
                                        <label class="col-form-label">Filterable</label>
                                        <div class="form-group">
                                            <select class="form-control dynamic_field" name="is_filterable[]" disabled="disabled">
                                                <option value="">Pilih Pilihan</option>
                                                <option value="t">Ya</option>
                                                <option value="f">Tidak</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="container-input" style="position: relative;top: 39px;">
                                        <button class="btn btn-sm btn-icon btn-outline-danger removeButton">
                                            <i class="flaticon2-delete"></i>
                                        </button>
                                        <button class="btn btn-sm btn-icon btn-outline-success addButton tambahan">
                                            <i class="flaticon2-plus"></i>
                                        </button>
                                    </div>

                                </div>

                            </div>

                        </form>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->


    <!-- end:: Subheader -->

</div>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>

@include('reff.insured_detail.action')

<script type="text/javascript">

    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");
    var no = 1;


</script>
@stop
