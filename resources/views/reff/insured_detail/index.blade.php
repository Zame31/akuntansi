@section('content')
<style>
    table.dataTable tr.dtrg-group td {
        background-color: #bdbdbd40 !important;
    }
</style>
<div class="app-content">
    <div class="section">

        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Insured Detail </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List Insured Detail </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List Insured Detail
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <div class="dropdown d-inline-block mr-3">
                                    <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select Data
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 36px, 0px);">
                                        <a class="dropdown-item" style="cursor: pointer" onclick="selectData('non_aktif')"> <i class="fa fa-square"></i> Non Aktif</a>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="selectData('aktif')"> <i class="fa fa-check-square "></i> Aktif</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="selectData('non_required')"> <i class="fa fa-square"></i> Tidak Wajib Diisi</a>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="selectData('required')"> <i class="fa fa-pen-square"></i> Wajib Diisi </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="selectData('non_filterable')"> <i class="fa fa-square"></i> Tidak Filterable</a>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="selectData('filterable')"> <i class="fa fa-filter"></i> Filterable </a>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-danger" onclick="modal_download_template()">Susunan List Template Excel </button>
                                <button onclick="loadNewPage('{{ route('insured_detail.create') }}')" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <div class="table-responsive">
                        <table class="table table-striped- table-hover table-checkable" id="datatable">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px">
                                            <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--success">
                                                <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                                <span></span>
                                            </label>
                                        </th>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center" width="30px"> ID </th>
                                        <th class="text-center" width="200px">Aksi</th>
                                        <th width="100px" class="text-center"> Status Aktif</th>
                                        <th class="text-center"> Product </th>
                                        <th class="text-center"> Label </th>
                                        <th class="text-center"> Format Input </th>
                                        <th class="text-center"> Wajib Diisi </th>
                                        <th class="text-center"> Filterable </th>
                                        <th class="text-center"> Tampil Header Detail </th>
                                    </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td>
                                                <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--success">
                                                    <input type="checkbox" value="{{ $item->id }}" class="kt-group-checkable">
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center"> {{ $item->id }} </td>
                                            <td align="center">
                                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="showEditModal('{{ $item->id }}')">Edit</button>
                                                @if($item->is_active == 't')
                                                    <button type="button" class="btn btn-outline-info btn-sm" onclick="setActive('{{ $item->id }}', 't')">Non Aktif</button>
                                                @else
                                                    <button type="button" class="btn btn-outline-success btn-sm" onclick="setActive('{{ $item->id }}', 'f')">Aktif</button>
                                                @endif
                                                <button type="button" class="btn btn-outline-danger btn-sm" onclick="hapusData('{{ $item->id }}', '{{ $item->product_id }}')">Delete</button>
                                            </td>
                                            <td align="center">
                                                @if($item->is_active == 't')
                                                    <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                                @else
                                                    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ $item->definition }}
                                                {{-- <button type="button" class="btn btn-sm btn-outline-primary btn-rounded" style="float: right;" onclick="loadNewPage('{{ route('insured_detail.show_product', ['id' => $item->product_id]) }}')"> Edit Input Grup </button> --}}
                                            </td>
                                            <td>{{ $item->label }}</td>
                                            <td class="">
                                                @if ( $item->is_currency || ($item->is_number && $item->is_currency))
                                                    Mata Uang
                                                @elseif ( $item->is_number && !$item->is_rate)
                                                    Angka
                                                @elseif ( $item->is_rate && $item->is_number )
                                                    Persentase
                                                @elseif ( $item->is_date && !$item->is_number && !$item->is_rate && !$item->is_currency)
                                                    Tanggal
                                                @else
                                                    Text
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if ($item->is_required)
                                                    <i class="la la-check"></i>
                                                @else
                                                    <i class="la la-close"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if ($item->is_filterable)
                                                    <i class="la la-check"></i>
                                                @else
                                                    <i class="la la-close"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if ($item->is_showing)
                                                    <i class="la la-check"></i>
                                                @else
                                                    <i class="la la-close"></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="9" align="center">
                                                Data Tidak Tersedia
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>


@include('reff.insured_detail.modal')
@include('reff.insured_detail.action')
<script type="text/javascript">
    $(document).ready(function() {

      var groupColumn = [5];
      var table = $('#datatable').DataTable({
          "columnDefs": [
              { "visible": false, "targets": [5] }
          ],
          "rowGroup": {
              dataSrc: [ 5 ]
          },
          "displayLength": 30,
      } );

      $('#example-select-all').click(function (e) {
            $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
        });

    });
</script>

@stop
