<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-edit-single" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="" id="id">
                    <input type="hidden" name="product_id" id="product_id" value="">

                    <div class="form-group">
                        <label for="branch_name">Product</label>
                        <input type="text" class="form-control" id="product" name="product" disabled>
                    </div>

                    <div class="form-group">
                        <label for="branch_name">Label</label>
                        <input type="text" class="form-control" id="label" name="label">
                    </div>

                    <div class="form-group">
                        <label for="single">Format Input</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 format_input" name="format_input" id="format_input">
                                <option value="">Pilih Format Input</option>
                                <option value="1">Angka</option>
                                <option value="2">Mata Uang</option>
                                <option value="3">Persentase</option>
                                <option value="4">Text</option>
                                <option value="5">Tanggal</option>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="single">Wajib Diisi</label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="is_required" id="is_required_ya" value="true"> Ya
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="is_required" id="is_required_tidak" value="false"> Tidak
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="single">Tampil Header Detail</label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="is_showing" id="is_showing_ya" value="true"> Ya
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="is_showing" id="is_showing_tidak" value="false"> Tidak
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="single">Filterable</label>
                                <div class="kt-radio-inline">
                                    <label class="kt-radio">
                                        <input type="radio" name="is_filterable" id="is_filterable_ya" value="true"> Ya
                                        <span></span>
                                    </label>
                                    <label class="kt-radio">
                                        <input type="radio" name="is_filterable" id="is_filterabla_tidak" value="false"> Tidak
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveEditSingle();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--begin::Modal-->
<div class="modal fade" id="template_excel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Susunan Template Excel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
                <div class="modal-body">
                    <form id="excel-template-form" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    @php
                                        $ref_product = \DB::select('SELECT * from ref_product where is_active is true');
                                    @endphp
                                    <label>Product Name</label>
                                    <select onchange="setProduct(this.value)" class="form-control kt-select2 init-select2" name="product" id="ex_product" >
                                        <option value="">Silahkan Pilih</option>
                                        @foreach($ref_product as $item)
                                        <option value="{{$item->id}}">{{$item->definition}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Silahkan Pilih</div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div style="border-bottom: 1px dashed #ebedf2;margin-bottom: 15px;margin-top: 15px;"></div>
                                <h6 class="mb-4">Tarik dan geser kotak untuk menentukan posisi :</h6>
                                <div class="row" id="list_check_template">
                                    <div id="list_co_1" class="col-md-4 sortable connectedSortable">

                                    </div>
                                    <div id="list_co_2" class="col-md-4 sortable connectedSortable">

                                    </div>
                                    <div id="list_co_3" class="col-md-4 sortable connectedSortable">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success " onclick="storeListTemplate();"><i class="la la-save"></i>Simpan</button>
                </div>
        </div>
    </div>
</div>
<!--end::Modal-->


<script type="text/javascript">

    $(document).ready(function () {
        $("#form-edit-single").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                label: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            max: 30,
                            message: 'Silahkan isi maksimal 30 karakter'
                        },
                        regexp: {
                            regexp: /^[\w ]+$/,
                            message: 'Format salah. Silakan diisi huruf, angka, underscore atau spasi'
                        },
                    }
                }
            }
        })

        .on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

    });
</script>
