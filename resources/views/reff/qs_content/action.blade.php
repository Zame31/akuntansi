<script type="text/javascript">

    $(document).ready(function () {
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                form_wording: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            max: 255,
                            message: 'Silahkan isi maksimal 255 karakter'
                        },
                    }
                },
                product: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                // the_business: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan isi'
                //         },
                //         stringLength: {
                //             max: 255,
                //             message: 'Silahkan isi maksimal 255 karakter'
                //         },
                //     }
                // },
                // period_default: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Silahkan isi'
                //         },
                //         stringLength: {
                //             max: 255,
                //             message: 'Silahkan isi maksimal 255 karakter'
                //         },
                //     }
                // },
                coverage_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi coverage'
                        },
                    }
                },
                main_exclusions_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi main exlusions'
                        },
                    }
                },
                deductible_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi deductible'
                        },
                    }
                },
                clauses_content: {
                    validators: {
                        notEmpty: {
                            message: 'Silakan isi clauses'
                        },
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    }); // end document ready

    function showModalTextEditor(tittle_modal, class_name) {

        $("#" + class_name + ' .modal-title').html(tittle_modal);
        $("#" + class_name).modal('show');
        $('.' + class_name).summernote({
            focus: true,
            height: 300,
            toolbar: [
                ['style', ['style']],
                ['fontsize', ['fontsize']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['view', ['fullscreen', 'help']],
            ],

        });

        // Set Content
        $("." + class_name).summernote("code", $("#" + class_name + '_content').val());
    }

    function save(class_name) {
        var markup = $('.' + class_name).summernote('code');
        $('.' + class_name).summernote('destroy');
        console.log(markup);
        $("#" + class_name).modal('hide');
        $("#" + class_name + '_data').html(markup);
        $("#" + class_name + '_content').val(markup);
        $("#form-data").bootstrapValidator('revalidateField', $("#" + class_name + '_content').prop('name'));
    }

    function store() {
        var validate = $('#form-data').data('bootstrapValidator').validate();
        if (validate.isValid()) {
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);
            objData.append('ref_order', '99');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('qs_content.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },

                success: function (response) {
                    console.log(response);
                    if ( response.rc == 0 ) {

                        if ( !$("#id").val() ) {
                            var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
                            var action = `<button onclick="znView()" type="button"
                                                class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                                                Data</button>
                                            <button onclick="znClose()" type="button"
                                                class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                                                Data</button>`;
                        } else {
                            var text = `Data berhasil disimpan`;
                            var action = `<button onclick="znView()" type="button"
                                                class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                                                Data</button>`;
                        }

                        znIconbox("Sukses",text,action);

                    } else {
                        toastr.error("Terjadi Kesalahan");
                    }
                }

            }).done(function (msg) {
                endLoadingPage();
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });

        } // endif
    }

    function znClose() {
        znIconboxClose();
        $("#form-data")[0].reset();
        document.getElementById("form-data").reset();
        $('#form-data').bootstrapValidator("resetForm", true);
        loadNewPage('{{ route('qs_content.action', ['act' => 'create']) }}');
    }

    function znView() {
        znIconboxClose();
        loadNewPage('{{ route('qs_content') }}');
    }

    function setActive(id, isActive) {
        if (isActive == 't') {
            var titleSwal = 'Non Aktif QS Content';
            var textSwal = 'Anda yakin akan menonaktifkan content ini?';
        } else {
            var titleSwal = 'Aktif QS Content';
            var textSwal = 'Anda yakin akan mengaktifkan content ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('qs_content.setActive') }}',
                    data: {
                        id: id,
                        active: isActive,
                        ref_order : '99'
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                            location.reload();
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }
</script>
