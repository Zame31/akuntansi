@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    QS Content </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        {{ $action }} </a>
                    </div>
            </div>
        </div>
    </div>

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="container-form">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">{{ ucwords($action) }} QS Content</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a onclick="loadNewPage('{{ route('qs_content') }}')" class="btn btn-clean kt-margin-r-10" style="cursor: pointer;">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Back</span>
                            </a>
                            @if ( $action != 'detail')
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success" onclick="store();">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Save</span>
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="kt-portlet__body" style="background: #fbfbfb;">
                        <form class="kt-form" id="form-data">

                            <input type="hidden" name="id" id="id" value="@if($action == 'edit') {{ $data->id }} @endif">

                            <div class="row">
                                <div class="col-xl-2"></div>

                                <div class="col-xl-8">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <h3 class="kt-section__title kt-section__title-lg" id="product_name"></h3>


                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Product </label>
                                                        <select class="form-control kt-select2 init-select2" name="product" id="product">
                                                            <option value="1000" disabled selected>Select Product</option>
                                                            @forelse ($products as $item)
                                                                <option value="{{ $item->id }}" @if($action == 'edit' || $action == 'detail') @if($item->id == $data->product_id) selected @endif @endif >{{ $item->definition }}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-4">
                                                    <label class="col-form-label control-label">Form / Wording</label>
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="form_wording" id="form_wording" rows="5">@if($action == 'edit' || $action == 'detail') {{ $data->form_wording }} @endif</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                    <label class="col-form-label">The Business</label>
                                                    <div class="form-group">
                                                        <div>
                                                            <textarea class="form-control" name="the_business" id="the_business" rows="5">@if($action == 'edit' || $action == 'detail') {{ $data->the_business }} @endif</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-4">
                                                    <label class="col-form-label">Period Default</label>
                                                    <div class="form-group">
                                                        <div>
                                                            <textarea class="form-control" name="period_default" id="period_default" rows="5">@if($action == 'edit' || $action == 'detail') {{ $data->period_default }} @endif</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                                    <div class="kt-section">
                                        <div class="kt-section__body">
                                            <div class="container-detail-vehicle bg-white" style="padding: 20px;border-radius: 4px;box-shadow: inset 0px 0px 8px 0px #0000005c">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <label class="col-form-label">Coverage</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="coverage_data">
                                                                    @if ( $action == 'edit' || $action == 'detail') {!! $data->coverage !!} @endif
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="coverage_content" id="coverage_content" value="@if ( $action == 'edit' || $action == 'detail') {{ $data->coverage }} @endif">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Coverage', 'coverage');"><i class="fa fa-pencil-alt"></i> Fill Coverage</button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <label class="col-form-label">Main Exclusions</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="main_exclusions_data">
                                                                    @if ( $action == 'edit' || $action == 'detail') {!! $data->main_exclusions !!} @endif
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="main_exclusions_content" id="main_exclusions_content" value="@if ( $action == 'edit' || $action == 'detail') {{ $data->main_exclusions }} @endif">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Main Exclusions', 'main_exclusions');"><i class="fa fa-pencil-alt"></i> Fill Main Exclusions</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6">
                                                        <label class="col-form-label">Deductible</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="deductible_data">
                                                                    @if ( $action == 'edit' || $action == 'detail') {!! $data->deductible !!} @endif
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="deductible_content" id="deductible_content" value="@if ( $action == 'edit' || $action == 'detail') {{ $data->deductible }} @endif">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Deductible', 'deductible');"><i class="fa fa-pencil-alt"></i> Fill Deductible </button>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <label class="col-form-label">Clauses</label>
                                                        <div class="form-group">
                                                            <div class="container-texteditor">
                                                                <h5 class="tittle"> Konten </h5>
                                                                <div id="clauses_data">
                                                                    @if ( $action == 'edit' || $action == 'detail' ) {!! $data->clauses !!} @endif
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="clauses_content" id="clauses_content" value="@if ( $action == 'edit' || $action == 'detail') {{ $data->clauses }} @endif">
                                                            <button type="button" class="btn btn-outline-hover-dark btn-sm mt-2" onclick="showModalTextEditor('Clauses', 'clauses');"><i class="fa fa-pencil-alt"></i> Fill Clauses </button>
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->


    <!-- end:: Subheader -->

</div>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>

@include('reff.qs_content.action')
@include('reff.qs_content.modal.clauses')
@include('reff.qs_content.modal.coverage')
@include('reff.qs_content.modal.deductible')
@include('reff.qs_content.modal.main_exclusions')
<script type="text/javascript">
</script>
@stop
