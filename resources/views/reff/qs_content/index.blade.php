@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        QS Content </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            List QS Content </a>
                        </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            List QS Content
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button onclick="loadNewPage('{{ route('qs_content.action', ['create']) }}')" class="btn btn-primary" style="color: white;">Tambah Data</>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="30px"> No </th>
                                        <th class="text-center">Aksi</th>
                                        <th width="100px" class="text-center"> Status</th>
                                        <th class="text-center"> Product </th>
                                        <th class="text-center"> Form Wording </th>
                                        <th class="text-center"> The Business </th>
                                        <th class="text-center"> Period Default </th>
                                    </th>
                                </thead>
                                <tbody>
                                    @forelse($data as $i => $item)
                                        <tr>
                                            <td align="center"> {{ ++$i }} </td>
                                            <td align="center">
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="flaticon-more"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" onclick="loadNewPage('{{ route('qs_content.action', ['edit', $item->id]) }}')">
                                                            <i class="la la-edit"></i> Edit
                                                        </a>
                                                        <a class="dropdown-item" onclick="loadNewPage('{{ route('qs_content.action', ['detail', $item->id]) }}')">
                                                            <i class="la la-arrows-alt"></i> Detail
                                                        </a>
                                                        @if ( $item->is_active )
                                                            <a class="dropdown-item"  onclick="setActive('{{ $item->id }}', 't')">
                                                                <i class="la la-close"></i> Non Aktif
                                                            </a>
                                                        @else
                                                            <a class="dropdown-item"  onclick="setActive('{{ $item->id }}', 'f')">
                                                                <i class="la la-check"></i> Aktif
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                @if($item->is_active == 't')
                                                    <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                                @else
                                                    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                                @endif
                                            </td>
                                            <td align="justify">{{ $item->definition }}</td>
                                            <td align="justify">{{ $item->form_wording }}</td>
                                            <td align="justify">{{ $item->the_business }}</td>
                                            <td align="justify">{{ $item->period_default }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" align="center">
                                                Data Tidak Tersedia
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                </div>
            </div>

        </div>


    <!-- end:: Subheader -->
    </div>


</div>

{{-- @include('reff.underwriter.modal') --}}
@include('reff.qs_content.action')

@stop
