@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Transaction </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                        Transaction Detail </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                        Transaction Detail
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                            {{-- <button onclick="cetakView()" type="button" class="btn btn-info">
                                <i class="flaticon2-printer"></i> Print & Download</button> --}}
                            <button onclick="cetakTransaksiDetail()" type="button" class="btn btn-info">
                                <i class="flaticon2-printer"></i> Print & Download</button>

                        <button class="btn btn-success" onclick="kembali_det('{{$type}}')">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="svg" class="kt-portlet__body">

                <div class="row znHeaderShow">
                        <div class="col-12 znHeadCetak" style="margin-top: -10px;">
                            <div style="display: inline-block;">
                                <img alt="Logo" src="{{asset('img/logo.jpg')}}" style="width: 40px;margin-top: -25px;" />
                            </div>
                            <div style="display: inline-block;">
                                <span class="zn-text-logo" style="display: block;">ATA HD</span>
                                <span style="display: block;margin-left: 8px;">"Simple & Inovative Solution" </span>
                            </div>
                            <div class="text-right mt-2" style="float:right;">
                                <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:10px;">Transaction Detail Report</span>
                                <span style="display: block;font-size:10px;">{{date('d M Y')}}</span>
                                <span style="display: block;font-size:10px;">Status {{$data[0]->status_trx}}</span>

                            </div>

                        </div>
                    </div>

            <div class="row ">
                <div class="col-3">
                    <div class="row mb-3 pb-2 zn-border-bottom">
                        <div class="col-6 pl-4">
                            <h5 class="text-success">Total Debet</h5>
                            @php
                            $totaldb=0;
                            @endphp
                            @foreach($data as $item)
                            @php
                                if ($_GET["type"] == 'reversal_sukses') {
                                    if($item->tx_type_id==0 && $item->is_reversal==null){
                                        $totaldb+=$item->tx_amount;
                                    }
                                }else{
                                    if($item->tx_type_id==0 ){
                                        $totaldb+=$item->tx_amount;
                                    }
                                }
                                
                            
                            @endphp
                            @endforeach
                            <h6>Rp {{number_format($totaldb,2,',','.')}}</h6>
                        </div>
                        <div class="col-6">
                            <h5 class="text-danger">Total Kredit</h5>
                            @php
                            $totalkr=0;
                            @endphp
                            @foreach($data as $item)
                            @php
                           
                           if ($_GET["type"] == 'reversal_sukses') {
                                if($item->tx_type_id==1 && $item->is_reversal==null){
                                    $totalkr+=$item->tx_amount;
                                }
                            }else{
                                if($item->tx_type_id==1 ){
                                    $totalkr+=$item->tx_amount;
                                }
                            }
                            
                            @endphp
                            @endforeach
                            <h6>Rp {{number_format($totalkr,2,',','.')}}</h6>
                        </div>
                    </div>

                    <div class="form-group col-12">
                        <h6>Kode Transaksi</h6>
                        <div class="input-group">
                            {{$data[0]->tx_code}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Segment</h6>
                        <div class="input-group">
                            {{$data[0]->definition}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Tanggal Transaksi</h6>
                        <div class="input-group">
                            {{date('d M Y',strtotime($data[0]->tx_date))}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Jumlah</h6>
                        <div class="input-group">
                            @php
                            $total=0;
                            @endphp
                            @foreach($data as $item)
                            @php
                                if($item->tx_type_id==0){
                                $total+=$item->tx_amount;
                                }
                            
                            @endphp
                            @endforeach
                            {{number_format($total,2,',','.')}}
                        </div>
                    </div>
                    <div id="userView" class="form-group col-12">
                        <h6>User</h6>
                        <div class="input-group">
                            {{$data[0]->fullname}}
                        </div>
                    </div>
                    <div id="userView" class="form-group col-12">
                        <h6>User Approval</h6>
                        <div class="input-group">
                            {{$data[0]->user_approve}}
                        </div>
                    </div>
                    <div class="form-group col-12 zn-border-bottom pb-4">
                        <h6>Keterangan</h6>
                        <div class="input-group">
                            {{$data[0]->notes}}
                        </div>
                    </div>

                    {{--
                    <div class="row znSignature" style="padding-left: 10px;">
                        <div class="col-12" style="font-size:10px;color: #595d6e;">
                            <b>User Maker</b>
                            <br><br><br><br><br>
                            <u>{{$data[0]->fullname}}</u>
                        </div>
                        <div class="col-12 mt-3" style="font-size:10px;color: #595d6e;">
                            <b>User Approval </b>
                            <br><br><br><br><br>
                            <u>{{$data[0]->user_approve}}</u>
                        </div>
                    </div>
                    --}}

                    {{-- <div class="btn-group btn-group w-100 mb-2" role="group">
                            <span class="btn btn-success" style="flex: none;width: 120px;">Total Debet</span>
                            <span class="btn btn-secondary text-right">Rp.500.000</span>
                        </div>

                        <div class="btn-group btn-group w-100 mb-5" role="group">
                            <span class="btn btn-danger" style="flex: none;width: 120px;">Total Kredit</span>
                            <span class="btn btn-secondary text-right">Rp.500.000</span>
                        </div> --}}



                </div>
                <div class="col-9">

                    {{-- DEBET KREDIT --}}
                    <div class="row " id="scrollStyle" style="height: 500px; overflow: auto;" >
                        <div class="col-12">
                            <div class="kt-timeline-v1 kt-timeline-v1--justified ">
                                <div class="kt-timeline-v1__items">
                                    <div class="kt-timeline-v1__marker"></div>
                                    @foreach($data as $key => $item)
                                    <div class="kt-timeline-v1__item rw{{$key}}" @if ($key == 0)
                                        style="margin-top: 10px !important;"
                                    @endif>
                                        <div class="kt-timeline-v1__item-circle">
                                            <div class="kt-bg-danger"></div>
                                        </div>
                                        <span class="kt-timeline-v1__item-time kt-font-brand">
                                            {{date('H:i',strtotime($item->created_at))}}
                                        </span>
                                        <div class="kt-timeline-v1__item-content">
                                            <div class="kt-timeline-v1__item-body">
                                                <div class="row">
                                                    <div class="col-md-4 ">
                                                        <h6>{{$item->coa_id}} - {{$item->coa_name}}</h6>
                                                        <p>
                                                            {{$item->tx_notes}}
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4 text-right">
                                                        <div class="btn-group btn-group w-100" role="group">
                                                            @if($item->tx_type_id==0)
                                                            <span
                                                                class="btn btn-light text-right" style="border: 1px solid white;">Rp.{{number_format($item->tx_amount,2,',','.')}}</span>
                                                            @else
                                                            <span class="btn btn-light text-right" style="border: 1px solid white;">Rp.0</span>
                                                            @endif
                                                            <span class="btn btn-success"
                                                                style="flex: none;border: 1px solid white;color:#ffffff;">Debet</span>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-4 text-right">

                                                        <div class="btn-group btn-group w-100" role="group">
                                                            @if($item->tx_type_id==1)
                                                            <span
                                                                class="btn btn-light text-right" style="border: 1px solid white;">Rp.{{number_format($item->tx_amount,2,',','.')}}</span>
                                                            @else
                                                            <span class="btn btn-light text-right" style="border: 1px solid white;">Rp.0</span>
                                                            @endif
                                                            <span class="btn btn-danger"
                                                                style="flex: none;border: 1px solid white;color:#ffffff;">Kredit</span>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- DOKUMEN --}}
                    <div id="allDokumenImage" class="row mt-4 pt-4">


                           @if($dokumen)
                           @foreach($dokumen as $item)
                           <div class="col-md-4 mb-5">
                           <div class="zn-doc-list ">
                                @php
                                    $path      = parse_url($item->url, PHP_URL_PATH);       // get path from url
                                    $extension = pathinfo($path, PATHINFO_EXTENSION); // get ext from path
                                @endphp
                               @if ($extension == 'jpg' || $extension == 'png' )
                                    <img onclick="toogleZoomImg(this)" style="cursor: zoom-in;" src="{{asset('upload_dokumen')}}/{{$item->tx_code}}_{{$item->url}}" alt="" srcset="">
                               @else
                                    <i class="fa fa-file-pdf" style="
                                    font-size: 100px;
                                    color: #ff3838;
                                "></i>
                               @endif

                                 <h5 class="mt-4">{{$item->definition}}</h5>
                                <h6>{{$item->url}}</h6>
                                <a href="{{asset('upload_dokumen')}}/{{$item->tx_code}}_{{$item->url}}" download class="btn btn-info btn-sm mt-4" style="display:block;width: 30%;">Download</a>

                            </div>
                        </div>
                           @endforeach
                           @endif



                    </div>
                </div>
            </div>



        </div>
    </div>
</div>


<div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content"
        {{-- style="border-radius: 0px;border: none;" --}}
        >
                <div class="modal-header">
                        <h5 class="modal-title" id="title_modal">Print & Download Preview Transaction Detail</h5>
                        <button type="button" onclick="closeCetakView()" class="close"></button>
                    </div>
            <div class="modal-body kt-portlet m-0 p-0">
                <div class="">
                    <div class="kt-portlet__body p-0">

                        <iframe id='result' class="scrollStyle" style="width: 100%;height: 520px;border: none;"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

// PRINT
$(".znHeaderShow").css({"display": "none"});
    $(".znSignature").css({"opacity": "0"});

    function closeCetakView() {
        $('#zn-print').modal('hide');
        $("#svg").removeAttr('style');
        $(".rw2").removeAttr('style');
        $(".znHeaderShow").css({"display": "none"});
        $(".znSignature").css({"opacity": "0"});
        $("h6").css({"font-weight": "bold","font-size": "1rem","margin-bottom":"0"});
        $("h5").css({"font-size": "1rem"});
        $(".input-group").css({"font-size": "1rem"});
        $(".form-group").css({"margin-bottom": "2rem"});

        $(".btn-group > .btn:not(:last-child):not(.dropdown-toggle), .btn-group > .btn-group:not(:last-child) > .btn").css({"font-size": "1rem"});
        $(".btn-group > .btn:not(:first-child), .btn-group > .btn-group:not(:first-child) > .btn").css({"font-size": "1rem"});
        $("#scrollStyle").css({"height": "500px"});

        $("#allDokumenImage").css({"opacity": "1"});
        $("#userView").css({"display": "block"});

    }
    function cetakView() {
        $(".znHeaderShow").css({"display": "block"});
        $(".znSignature").css({"opacity": "1"});

        $("#svg").css({"padding": "40px", "width": "850px","font-family": "sans-serif"});
        $(".kt-widget6__item").css({"padding": "3px"});
        $(".kt-widget6 .kt-widget6__body .kt-widget6__item > span").css({"font-size": "10px", "flex": "auto"});
        $(".rw16").css({"margin-top": "140px"});
        $("h6").css({"font-weight": "bold","font-size": "9px","margin-bottom":"0"});
        // $("h6").css({});

        $("h5").css({"font-size": "10px"});
        $(".input-group").css({"font-size": "10px"});
        $(".form-group").css({"margin-bottom": "12px"});

        $(".btn-group > .btn:not(:last-child):not(.dropdown-toggle), .btn-group > .btn-group:not(:last-child) > .btn").css({"font-size": "10px"});
        $(".btn-group > .btn:not(:first-child), .btn-group > .btn-group:not(:first-child) > .btn").css({"font-size": "10px"});

        $("#scrollStyle").css({"height": "inherit"});

        $(".rw2").attr('style', 'margin-top: 250px !important');

        $("#allDokumenImage").css({"opacity": "0"});
        $("#userView").css({"display": "none"});


        $('#zn-print').modal('show');
        var pdf = new jsPDF('l', 'pt', 'a4');

        var data = document.getElementById('svg');
        pdf.html(data, {
			callback: function (pdf) {
				var iframe = document.getElementById('result');
				iframe.src = pdf.output('datauristring');
			}
		});
    }
    // ENDPRINT

    function cetakTransaksiDetail() {
        // var url = window.location.href;
        var id = getURLParameter('id');
        var type = getURLParameter('type');
        console.log('id : ' + id);
        console.log('type : ' + type);
        window.open(base_url + "transaksi_detail/ExportPdf" + "?id=" + id + "&type=" + type, '_blank');
    }

    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    }


    function kembali_det(id) {
        if(id=='trx_sukses'){
             window.location.href = base_url + 'accouting/list_trx_success';

        }else if(id=='det_reversal'){
            window.location.href = base_url + 'accouting/list_reversal';
        }else if(id=='reversal_sukses'){
            window.location.href = base_url + 'accouting/list_rev_success';
        }else if(id=='app_trx'){
            window.location.href = base_url + 'list_app_baru';

        }else{
          window.location.href = base_url + 'list_tr_baru';
        }

    }


function toogleZoomImg(el) {
        var src = $(el).attr('src');
        $('<div>').css({
            background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
            backgroundSize: '600px',
            width: '100%',
            height: '100%',
            position: 'fixed',
            zIndex: '10000',
            top: '0',
            left: '0',
            cursor: 'zoom-out'
        }).click(function () {
            $(this).remove();
        }).appendTo('body');
    }

</script>
@stop
