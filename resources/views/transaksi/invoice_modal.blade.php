<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="border: none;">
            {{-- <div class="modal-header">
                <i class="la la-clipboard zn-icon-modal"></i>
                <h5 class="modal-title" style="margin-top: 10px !important;" id="exampleModalLongTitle">
                    Detail Invoice
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div> --}}
            <div class="modal-body p-0" id="svg">
                <input type="hidden" id="zn_stat" value="" name="zn_stat">
                <input type="hidden" id="get_id" value="" name="get_id">

                <input type="hidden" id="zn_paid_stat" value="" name="zn_paid_stat">

                {{-- INVOICE AKTIF --}}
                <div id="cetak_aktif" style="font-size:9px;color:#000000;padding:40px;display:none;">

                    <style>
                        table tr td {
                            padding: 5px;
                        }

                        table.zn-table-cetak tr td {
                            border: 1px solid black;
                        }

                    </style>
                    <div class="row">
                        <div class="col-7">
                                @php
                                $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                            @endphp
                            <img class="img-fluid" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 150px;">
                        </div>
                        <div class="col-5">
                           {!! $data_imgz->address_cetak !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <b style="font-size:14px;display: block;text-align:center;margin-top: 40px;">DEBIT NOTE</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">

                        </div>
                        <div class="col-4">
                            <b><i id="ca_inv"></i> </b>
                        </div>
                    </div>
                    <div style="border:1px solid #000000;">
                        <br>
                        <table>
                            <tr>
                                <td style="font-weight: bold;">Customer Name</td>
                                <td style="font-weight: bold;">:</td>
                                <td id="ca_com" style="font-weight: bold;"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Received Name</td>
                                <td style="font-weight: bold;">:</td>
                                <td id="ca_agent" style="font-weight: bold;">-</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Address</td>
                                <td style="font-weight: bold;">:</td>
                                <td id="ca_address" style="font-weight: bold;">J</td>
                            </tr>
                        </table>
                        <br>
                        <table class="zn-table-cetak" style="width: 100%;">
                            <tr>
                                <td style="font-weight: bold;">No</td>
                                <td style="font-weight: bold;">Description(s)</td>
                                <td style="font-weight: bold;">Amount</td>
                            </tr>
                            <tr>
                                <td style="
                                        vertical-align: top;
                                    ">1</td>
                                <td>
                                    <div class="row">
                                        <div class="col-3">In Respect Of</div>
                                        <div id="ca_product" class="col-9">: MOTOR VEHICLE INSURANCE</div>
                                        <div class="col-3">Insured</div>
                                        <div id="ca_insured" class="col-9">: -</div>
                                        <div class="col-3">Policy No</div>
                                        <div id="ca_polis" class="col-9">: -</div>
                                        <div class="col-3">Period From</div>
                                        <div id="ca_periode" class="col-9">: -</div>
                                        <div class="col-3">Underwriter</div>
                                        <div id="ca_underwriter" class="col-9">: -</div>
                                        <div class="col-3">Sum Insured</div>
                                        <div id="ca_sumins" class="col-9">: -</div>
                                    </div>
                                    <br><br><br>
                                    <div class="row">
                                        <div class="col-12">Gross Premium</div>
                                        <br><br>
                                        <div class="col-12">Polis & Stamp Duty Cost</div>
                                        <div class="col-12 text-right">(+/-)</div>
                                    </div>
                                </td>
                                <td>
                                    <br><br><br><br><br><br><br><br><br>
                                    <div class="row">
                                        <div class="col-4 mata_uang_set">IDR</div>
                                        <div id="ca_premi_amount" class="col-8 text-right">0</div>
                                        <br><br>
                                        <div class="col-4 mata_uang_set">IDR</div>
                                        <div id="ca_adm" class="col-8 text-right">0</div>
                                        <div class="col-4 mata_uang_set">IDR</div>
                                        <div id="sisa_desimal" class="col-8 text-right">(0.00)</div>
                                    </div>
                                </td>
                            </tr>
                            {{-- <tr>
                                <td colspan="2" class="text-right">Total</td>
                                <td>
                                    <div class="row">
                                        <div class="col-4">IDR</div>
                                        <div id="ca_total" class="col-8 text-right">0</div>
                                    </div>

                                </td>
                            </tr> --}}
                            <tr>
                                <td colspan="2" class="text-right" style="font-weight: bold;">Grand Total</td>
                                <td>
                                    <div class="row">
                                        <div class="col-4 mata_uang_set" style="font-weight: bold;">IDR</div>
                                        <div id="ca_grand_total" class="col-8 text-right" style="font-weight: bold;">0</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-left" style="font-weight: bold;" id="ca_terbilang"></td>

                            </tr>

                        </table>

                    </div>

                    <br>

                    <div class="row">
                        <div class="col-8">
                            <u>Notes:</u>
                            <br>
                            <ol>
                                <li>Payment should be made in the form oft the crossed cheque(Giro) payable to PT. HD Soeryo Indonesia Gemilang</li>
                                <li>Please attach the <span style="letter-spacing: 1px;">PAYMENT ADVICE SLIP</span>  together with your payment and send to Finance
                                    Division of PT. HD Soeryo Indonesia Gemilang</li>
                                <li>Transfer to our bank account : <br>
                                    <table>
                                        <tr>
                                            <td>Bank</td>
                                            <td>: BCA Slipi</td>
                                        </tr>

                                        <tr>
                                            <td style="
                                            vertical-align: top;
                                        ">Account Number</td>
                                            <td>: 084.23.233.29 (IDR Currency)
                                                <br>
                                                 <span style="margin-left:5px;">084.23.238.68 (USD Currency)</span>
                                                <br>
                                                <b style="margin-left:5px;">PT.HD Soeryo Indonesia Gemilang</b>
                                            </td>
                                        </tr>
                                    </table>
                            </ol>
                        </div>
                        <div class="col-4" style="text-align:center;display:block;">
                            Jakarta, <span id="ca_date_ttd"></span>

                            <br><br><br><br><br><br><br>
                            <b><u id="ca_ttd">Tuty Rusli</u></b><br>
                            <b id="ca_ttd_jab">GM Marketing & Business</b>
                        </div>
                    </div>

                </div>

                {{-- INVOICE PAID --}}
                <div id="cetak_paid" style="font-size:9px;color:#000000;padding:40px;display:none;">

                    <style>
                        table tr td {
                            padding: 5px;
                        }

                        .zn-checkbox {
                            height: 10px;
                            width: 10px;
                            border: 2px solid #000000;
                            display: inline-block;
                            margin-right: 5px;
                        }

                    </style>



                    <div class="row">
                        <div class="col-7">
                            <img class="img-fluid" src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 150px;">
                        </div>
                        <div class="col-5">

                            {!! $data_imgz->address_cetak !!}
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-12">
                            <b style="font-size:10px;display: block;text-align:center;margin-top: 10px;">OFFICIAL
                                RECEIPT</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">

                        </div>
                        <div class="col-4" style="
                        display: block;
                        text-align: right;
                    ">
                            {{-- <b class="text-right;"><i>ORIGINAL <br> --}}
                               <span id="pa_inv"></span></i> </b>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-5">
                            <b><u>Customer Name (Nama pelanggan) </u></b>
                        </div>
                        <div class="col-1">
                            <b> : </b>
                        </div>
                        <div class="col-6">
                            <b id="pa_agent">Tn. Faprianto</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <b><u>Being Paymenf of (Untuk Pembayaran) </u></b>
                        </div>
                        <div class="col-1">
                            <b> : </b>
                        </div>
                        <div class="col-6">
                            <b id="pa_product">Tn. Faprianto</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <b><u>Amount (Jumlah) </u></b>
                        </div>
                        <div class="col-1">
                            <b> : </b>
                        </div>
                        <div class="col-6">
                            <b id="pa_premi_amount">Tn. Faprianto</b>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-5">
                            <b><u>The Sum of (Uang Sejumlah) </u></b>
                        </div>
                        <div class="col-1">
                            <b> : </b>
                        </div>
                        <div class="col-6" style="
                        border-bottom: 1px solid #000000;
                    ">
                            <b id="pa_terbilang">SATU JUTA</b>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-3">
                            <div class="zn-checkbox"></div>
                            <span style="font-weight: bold;">Cheque/Giro</span>
                        </div>
                        <div class="col-3">
                            <div class="zn-checkbox"></div>
                            <span style="font-weight: bold;">Cash</span>
                        </div>
                        <div class="col-3">
                                <div class="zn-checkbox"></div>
                                <span style="font-weight: bold;">Transfer</span>
                            </div>
                    </div>
                    <div class="row">
                            <div class="col-5">
                                <b><u>Remarks (Catatan) </u></b>
                            </div>
                            <div class="col-1">
                                <b> : </b>
                            </div>
                            <div class="col-6">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-8">
                                <br>
                                <div style="border: 2px solid #000000;
                                padding: 5px;
                                width: 216px;
                                margin-top: 10px;
                            }">
                                    Pembayaran premi dianggap lunas setelah dana diterima di rekening PT. HD Soeryo Indonesia Gemilang
                                </div>
                            </div>
                            <div class="col-4" style="text-align:center;display:block;">
                                    <b>Jakarta, <span id="pa_date_ttd"></span> </b>
                                    <br>
                                    <b>Authorized By </b>
                                    <br><br><br><br>
                                    <b><u id="pa_ttd"></u></b><br>
                                    <b id="pa_ttd_jab">GM Marketing & Business</b>
                                </div>
                        </div>

                </div>


                <div class="kt-invoice-1">
                    {{-- style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});" --}}
                    <div class="kt-invoice__head" style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                        <div class="kt-invoice__container">
                            <div class="text-right mb-5">
                                <button onclick="cetakViewInvoice()" type="button"
                                    class="znBtnShow btn btn-danger btn-hover-info btn-pill">
                                    <i class="flaticon2-printer"></i> Print & Download </button>

                                    <a id="setWord" href="#" class="znBtnShow btn btn-info btn-hover-success btn-pill">Export Word</a>

                                    <button data-dismiss="modal" aria-label="Close" type="button"
                                    class="znBtnShow btn btn-light btn-hover-info btn-pill">Kembali</button>
                            </div>
                            <div class="kt-invoice__brand">
                                <div class="row znHeaderShow" style="width: 100%;margin-bottom:10px;">
                                    <div class="col-12" style="margin-top: -10px;">
                                        <div style="display: inline-block;">
                                            <img alt="Logo" src="{{asset('img/'.$data_imgz->url_image)}}"
                                                style="width: 100px;margin-top: -25px;" />
                                        </div>
                                        <div style="display: inline-block;">
                                            <span id="zn-invoice-tittle" class="zn-text-logo"
                                                style="display: block;color:#ffffff;font-weight: bold;font-size: 20px;">DEBIT NOTE</span>
                                            <span id="wf_status"
                                                style="display: block;margin-left: 8px;color:#ffffff;font-size:10px;"></span>
                                        </div>
                                        {{-- <div class="text-right mt-2" style="float:right;">
                                                        <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;color:#ffffff;">Invoice</span>
                                                        <span style="display: block;font-size:10px;color:#ffffff;"></span>
                                                    </div> --}}
                                    </div>
                                </div>
                                {{-- <h1 class="kt-invoice__title" style="font-size:1.5rem;">INVOICE</h1> --}}
                                <div href="#" class="kt-invoice__logo">

                                    {{-- <a href="#" onclick="kembali();" class="btn btn-pill btn-primary">Kembali</a> --}}
                                </div>
                            </div>
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle"><b>DEBIT NOTE DATE.</b></span>
                                    <span class="kt-invoice__text" id="tgl_tr"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">DEBIT NOTE NO.</span>
                                    <span class="kt-invoice__text" id="inv_no"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">PRODUCT NAME.</span>
                                    <span class="kt-invoice__text" id="product"></span>
                                </div>
                            </div>
                            <div id="insView" class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">ACCOUNT OFFICER.</span>
                                    <span class="kt-invoice__text" id="agent"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">START DATE AGING.</span>
                                    <span class="kt-invoice__text" id="start_date"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">END DATE AGING.</span>
                                    <span class="kt-invoice__text" id="end_date"></span>
                                </div>
                            </div>
                            <div class="kt-invoice__items zn-desc-invoice">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">QUOTATION NUMBER.</span>
                                    <span class="kt-invoice__text" id="quotation"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">COVER NOTE NUMBER.</span>
                                    <span class="kt-invoice__text" id="proposal"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle"  id="segmentView">SEGMENT.</span>
                                    <span class="kt-invoice__text" id="segment"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-invoice__body">
                        <div class="kt-invoice__container">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>DESCRIPTION</th>
                                            <th><div id="tittle_lang"></div></th>
                                            <th>IDR AMOUNT </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="ins_amount_visible">
                                            <td>Total Sum Insured</td>
                                            <td>
                                                <div id="ins_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="ins_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gross Premium </td>
                                            <td>
                                                <div id="premi_amount1_lang"></div>
                                            </td>
                                            <td>
                                                <div id="premi_amount1"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Discount</td>
                                            <td>
                                                <div id="disc_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="disc_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nett Premium</td>
                                            <td>
                                                <div id="net_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="net_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Agent Fee</td>
                                            <td>
                                                <div id="agent_fee_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="agent_fee_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Stamp Duty</td>
                                            <td>
                                                <div id="stamp_duty_lang"></div>
                                            </td>
                                            <td>
                                                <div id="stamp_duty"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Police Duty</td>
                                            <td>
                                                <div id="police_duty_lang"></div>
                                            </td>
                                            <td>
                                                <div id="police_duty"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Comm due to Us to HD</td>
                                            <td>
                                                <div id="comp_fee_amount_lang"></div>
                                            </td>
                                            <td>
                                                <div id="comp_fee_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>HD Soeryo Duty</td>
                                            <td>
                                                <div id="adm_duty_lang"></div>
                                            </td>
                                            <td>
                                                <div id="adm_duty"></div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="kt-invoice__footer">
                        <div class="kt-invoice__container">
                            <div class="kt-invoice__bank">
                                <div class="kt-invoice__title"> <b>BANK TRANSFER</b></div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__label">Account Name:</span>
                                    <span class="kt-invoice__value" id="bank"></span></span>
                                </div>
                            </div>
                            <div class="kt-invoice__total">
                                    <span  style="font-size:14px;" class="kt-invoice__title"><b id="zn-tittle-premi-lang"></b></span>
                                    <span class="kt-invoice__price" style="font-size:16px;color:#595d6e;" id="premi_amount_lang"></span>
                                </div>
                            <div class="kt-invoice__total">
                                <span id="zn-tittle-premi"  style="font-size:14px;" class="kt-invoice__title"><b>GROSS PREMIUM</b></span>
                                <span class="kt-invoice__price" style="font-size:16px;" id="premi_amount"></span>
                            </div>
                            <div class="row znSignature" style="margin-top: 40px;width: 100%;display:none;">
                                <div class="col-6" style="font-size:10px;color: #595d6e;">
                                    User Maker
                                    <br><br><br><br><br>
                                    <b><u id="user_maker"></u></b>
                                </div>
                                <div class="col-6" style="font-size:10px;color: #595d6e;">
                                    User Approval
                                    <br><br><br><br><br>
                                    <b><u id="user_approval"></u></b>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content" {{-- style="border-radius: 0px;border: none;" --}}>
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Print & Download Preview Invoice</h5>
                <button type="button" onclick="closeCetakViewInvoice()" class="close"></button>
            </div>
            <div class="modal-body kt-portlet m-0 p-0">
                <div class="">
                    <div class="kt-portlet__body p-0">

                        <iframe id='znresult' class="scrollStyle"
                            style="width: 100%;height: 520px;border: none;"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
