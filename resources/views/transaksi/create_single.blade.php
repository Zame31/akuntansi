@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Accounting </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Single Transaction </a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form Single Transaction
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="return simpan('single')" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                {{-- <div class="dropdown dropdown-inline">
                    <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flaticon-more-1"></i>
                    </a>
                </div> --}}
            </div>
        </div>
        <div class="kt-portlet__body">
            <form id="form-new" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-3">
                        <div class="row">
                            
                            <div class="form-group col-12">
                                <label>Tanggal Transaksi</label>
                                <div class="input-group date">
                                    <input type="hidden" name="type" id="type" value="single">
                                    <input type="text" class="form-control" value="{{date('d M Y')}}" id="tgl_tr" name="tgl_tr" readonly
                                        placeholder="Pilih Tanggal" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                </div>
                            </div>
                           
                            <div class="form-group  col-12">
                                <label for="exampleTextarea">Catatan</label>
                                <textarea id="catatan" name="catatan" class="form-control" maxlength="80" rows="3"></textarea>
                                <div class="invalid-feedback" id="co">Silahkan isi catatan</div>
                                  <div class="invalid-feedback" id="ck">Maximal 80 karakter</div>
                            </div>
                            <input type="hidden" name="idsegment" id="idsegment" value="0">
                        <!--    
                            <div class="form-group  col-12 form-group-last">
                                <label for="exampleTextarea">Pilih Segment</label>
                                <select class="form-control kt-select2 init-select2" name="idsegment" id="idsegment">
                                    @foreach($segment as $item)
                                    <option value={{$item->id}}>{{$item->definition}}</option>
                                    @endforeach
                                </select>
                            </div>
                        -->
                        </div>

                    </div>
                    <div class="col-9">
                        <div class="alert alert-light alert-elevate fade show" id="ll" role="alert">
                            <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                            <div class="alert-text">
                                Nilai debet atau kredit masih 0
                            </div>
                        </div>





                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-5">
                                        <h5 class="zn-head-line-success mb-4">Debet</h5>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Account Number</label>
                                            <select class="form-control kt-select2 coano" name="coa_no[]" id="coba2">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($coa as $item1)
                                                <option value="{{$item1->coa_no}}">{{$item1->coa_no}} - {{$item1->coa_name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Pilih No Akun</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Keterangan</label>
                                            <textarea class="form-control" name="keterangan[]" id="keterangan" rows="1" maxlength="100"></textarea>

                                            <div class="invalid-feedback">Keterangan Tidak Boleh Kosong </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Amount</label>
                                            <input type="text" onkeyup="convertToRupiah1(this)" id="debet" name="debet[]" class="form-control money" style="text-align: right;">
                                            <input type="hidden" name="kredit[]" value="0">

                                        </div>
                                    </div>
                                    <div class="offset-1 col-5">
                                            <h5 class="zn-head-line-danger mb-4">Kredit</h5>
                                            <div class="form-group">
                                                <label for="exampleTextarea">Account Number</label>
                                                <select class="form-control kt-select2 " name="coa_no[]" id="coba1">
                                                </select>
                                                <div class="invalid-feedback">Pilih No Akun</div>
                                            </div>
                                            <div class="form-group">
                                            <label for="exampleTextarea">Keterangan</label>
                                             <textarea class="form-control" name="keterangan[]" readonly id="keterangan1" rows="1" maxlength="100"></textarea>
                                            <div class="invalid-feedback">Keterangan Tidak Boleh Kosong </div>
                                        </div>
                                            <div class="form-group">
                                                <label for="exampleTextarea">Amount</label>
                                                <input type="hidden" name="debet[]" value="0">
                                                <input type="text" onkeyup="convertToRupiah(this)"  class="form-control money" id="kredit" name="kredit[]" style="text-align: right;">
                                            </div>
                                    </div>

                                </div>


                                {{-- DOKUMEN --}}
                                <div class="row"
                                    style="border-top: 2px solid #f5f6fc;margin-top: 50px;padding-top: 50px;margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                    <div class="col-5">
                                        <h6>Jenis Dokumen</h6>
                                    </div>
                                    <div class="offset-1 col-5">
                                        <h6>Preview Dokumen</h6>
                                    </div>

                                    <div class="col-1">
                                        <button type="button" onclick="add_row_new_dok()"
                                            class="btn btn-success btn-elevate btn-icon"><i
                                                class="la la-plus"></i></button>
                                    </div>
                                </div>
                                <table class="table table-dok" id="tbl_dok">
                                    <tr>
                                        <div class="row mt-2">
                                            <div class="col-5">
                                                <select class="form-control kt-select2 init-select2 dokno" name="jns_dok[]"
                                                    id="jns_dok">
                                                    @foreach($dokumen as $item)
                                                    <option value={{$item->id}}>{{$item->definition}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="offset-1 col-5">
                                                <input type="file" class="form-control" name="file[]" id="file">
                                                <div class="invalid-feedback" id="ss">Silahkan Upload Dokumen</div>
                                                <div class="invalid-feedback" id="ff">Format Dokumen
                                                    pdf,jpeg,jpg,png,xls,xlxs</div>
                                                <div class="invalid-feedback" id="mm">Max Size Dokumen 2 Mb</div>
                                            </div>
                                        </div>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </form>    
            <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button onclick="return simpan('single')" class="btn btn-success">Simpan</button>
                    </div>
                </div>    
        </div>
    </div>
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
     $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
$(document).ready(function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    
    var max="'"+ dd +" "+mm+" "+yyyy+"'";
    console.log(max);
    $('#tgl_tr').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

});
$('#coba1').select2({
    placeholder: "Silahkan Pilih"
});
$('#coba2').select2({
    placeholder: "Silahkan Pilih"
});


 $('#coba2').on('change', function (v) {
        loadingPage();
        document.getElementById("kredit").value=0;
        document.getElementById("debet").value=0;
        $.ajax({
                type: 'GET',
                url: base_url + '/dbcr?id='+this.value,
                success: function (res) {
                    endLoadingPage();
                var data = $.parseJSON(res);
                if(data[0]['balance_type_id']==0){
                    $('#debet').css("background-color", "#00FFFF");
                    $('#kredit').css("background-color", "");

                    $(debet).addClass('zn-is-debet');
                    $(kredit).removeClass('zn-is-kredit');
                    document.getElementById("kredit").readOnly = true;
                    document.getElementById("debet").readOnly = false;

                }else{
                     $('#debet').css("background-color", "#00FFFF");
                    $('#kredit').css("background-color", "");

                    $(debet).addClass('zn-is-debet');
                    $(kredit).removeClass('zn-is-kredit');
                    document.getElementById("kredit").readOnly = true;
                    document.getElementById("debet").readOnly = false;


                }
             }
        });


        let value = [];
        $(".coano > option:selected").each(function() {

        value.push(this.value);
        //Add operations here

        });

        loadingPage();
        $.ajax({
                type: 'GET',
                url: base_url + '/coa',
                data: {value: value},
                async: false,
                success: function (res) {
                    var data = $.parseJSON(res);
                    console.log(data);
                    _items='<option value="">Silahkan pilih</option>';
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.coa_no+"'>"+v.coa_no+" - "+v.coa_name+"</option>";
                    });
                    $('#coba1').html(_items);
                    endLoadingPage();
                }
            });


    });

 $('#catatan').keyup(function() {
        var panjang = this.value.length;

        if(panjang==80){
            $('#invoice').addClass( "is-invalid" );
            $('#co').hide();
            $('#ck').show();
        }else{
            $('#invoice').removeClass( "is-invalid" );  
            $('#co').hide();
            $('#ck').hide();   
        }
    });

 $('#keterangan').keyup(function() {
        var panjang = this.value.length;

        $('#keterangan1').val(this.value);
        $('#catatan').val(this.value);    
    });
function convertToRupiah1 (objek) {
    if(objek.value==0){
       objek.value=''; 
    }else{

    var value = objek.value;
     value = value.replace(/^(0*)/,"");
     objek.value=value;



    $('#kredit').val(objek.value).trigger('mask.maskMoney');

    }
    

}



</script>
<!-- end:: Content -->
@include('transaksi.action')
@stop
