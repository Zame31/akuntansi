<html>
    <head>
        <style>
            * {
                font-family: sans-serif;
                font-size: 12px;
            }

            table.table-data,
            table.table-data tr,
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
            }

            table.table-data th {
                text-align: center;
            }


        </style>
    </head>
    <title> Export PDF Detail Transaksi </title>
    <body>
        <div class="app-content">
            <div class="section">
                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top:30px;">

                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                        </div>
                        <div class="kt-portlet__body" id="svg">
                                <div class="znHeaderShow">
                                    <div class="col-12 znHeadCetak">
                                        <div style="display: inline-block;">
                                            @php
                                                $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
                                            @endphp
                                            {{-- <img alt="Logo" src="{{asset('img/logo.jpg')}}" style="width: 40px;" /> --}}
                                            <img src="{{asset('img/'.$data_imgz->image_cetak)}}" style="width: 150px;">

                                        </div>
                                        {{-- <div style="display: inline-block;">
                                            <span class="zn-text-logo" style="display: block; font-size: 20px">ATA HD</span>
                                            <span style="display: block;margin-left: 8px; font-size: 20px;">"Simple & Inovative Solution"</span>
                                        </div> --}}
                                        <div class="text-right mt-2" style="float:right;">
                                            <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:15px;"> Transaction Report</span>
                                            <span style="display: block;font-size:15px;">{{date('d F Y')}}</span>
                                            <span style="display: block;font-size:15px;">Status {{$data[0]->status_trx}} </span>
                                        </div>

                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-12">

                                    </div>
                                </div>


                        </div>

                    </div>

                </div>


                <div class="kt-container  kt-grid__item kt-grid__item--fluid" style="margin-top: 10px;">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                    <table>
                                            <tr>
                                                <td> Total Debet </td>
                                                <td> : </td>
                                                <td>
                                                    @php
                                                        $totaldb=0;
                                                    @endphp

                                                    @foreach($data as $item)
                                                        @php
                                                         if ($_GET["type"] == 'reversal_sukses') {
                                                            if($item->tx_type_id==0 && $item->is_reversal==null){
                                                                $totaldb+=$item->tx_amount;
                                                            }
                                                        }else{
                                                            if($item->tx_type_id==0 ){
                                                                $totaldb+=$item->tx_amount;
                                                            }
                                                        }

                                                            // if($item->tx_type_id==0){
                                                            //     $totaldb+=$item->tx_amount;
                                                            // }
                                                        @endphp
                                                    @endforeach
                                                    Rp {{number_format($totaldb,2,',','.')}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Total Kredit </td>
                                                <td> : </td>
                                                <td>
                                                    @php
                                                        $totalkr=0;
                                                    @endphp
                                                    @foreach($data as $item)
                                                        @php
                                                         if ($_GET["type"] == 'reversal_sukses') {
                                                            if($item->tx_type_id==1 && $item->is_reversal==null){
                                                                $totalkr+=$item->tx_amount;
                                                            }
                                                        }else{
                                                            if($item->tx_type_id==1 ){
                                                                $totalkr+=$item->tx_amount;
                                                            }
                                                        }
                                                            // if($item->tx_type_id==1){
                                                            //     $totalkr+=$item->tx_amount;
                                                            // }
                                                        @endphp
                                                    @endforeach
                                                    Rp {{number_format($totalkr,2,',','.')}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Kode Transaksi </td>
                                                <td> : </td>
                                                <td> {{$data[0]->tx_code}} </td>
                                            </tr>
                                            <tr>
                                                <td> Tanggal Transaksi </td>
                                                <td> : </td>
                                                <td> {{date('d F Y',strtotime($data[0]->tx_date))}} </td>
                                            </tr>
                                            <tr>
                                                <td> Jumlah </td>
                                                <td> : </td>
                                                <td>
                                                    @php
                                                        $total=0;
                                                    @endphp

                                                    @foreach($data as $item)
                                                        @php
                                                            if($item->tx_type_id==0){
                                                                $total+=$item->tx_amount;
                                                            }
                                                        @endphp
                                                    @endforeach

                                                    Rp {{ number_format($total,2,',','.') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> User Approval </td>
                                                <td> : </td>
                                                <td> {{$data[0]->user_approve}} </td>
                                            </tr>
                                            <tr>
                                                <td> Keterangan </td>
                                                <td> : </td>
                                                <td> {{$data[0]->notes}} </td>
                                            </tr>
                                        </table>
                            </div>
                        </div>
                        <div class="kt-portlet__body svg" id="svg2" style="margin-top: 10px;">
                                <table class="table table-striped- table-hover table-checkable table-data" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th> COA Name </th>
                                            <th> KETERANGAN </th>
                                            <th> DEBET </th>
                                            <th> KREDIT </th>
                                            <th> WAKTU TRANSAKSI </th>
                                        </th>
                                    </thead>
                                    <tbody id="searchResult">
                                        @foreach($data as $key => $item)
                                        @if ($item->tx_amount > 0)
                                            <tr>
                                                <td tyle="width: 25%;"> {{$item->coa_id}} - {{$item->coa_name}} </td>
                                                <td style="width: 30%;word-wrap: break-word;"> {{$item->tx_notes}} </td>
                                                <td align="right">
                                                    @if($item->tx_type_id==0)
                                                        Rp.{{number_format($item->tx_amount,2,',','.')}}
                                                    @else
                                                        Rp.0
                                                    @endif
                                                </td>
                                                <td align="right">
                                                    @if($item->tx_type_id==1)
                                                        Rp.{{number_format($item->tx_amount,2,',','.')}}
                                                    @else
                                                        Rp.0
                                                    @endif
                                                </td>
                                                <td tyle="width: 1%;" align="center">
                                                    {{date('H:i',strtotime($item->created_at))}}
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                                <span id="totalData" style="font-weight: 500;"> Total Jumlah Data : {{ count($data) }}</span>
                        </div>

                        <div>
                            <br><br>
                            <table style="width:350px;">
                                <tr>
                                    <td>Staff</td>
                                    <td>Checker</td>
                                    <td>Approval</td>
                                </tr>
                                <tr>
                                    <td><br><br><br><br><br> <u>{{$data[0]->fullname}}</u> </td>
                                    <td><br><br><br><br><br> <u>
                                        @php
                                            $userChecker = collect(\DB::select("SELECT * from master_user where id = 2"))->first();
                                        @endphp
                                        @if ($data_imgz->app_name == 'ATA HD')
                                            {{$userChecker->fullname}}
                                        @else
                                            {{$data[0]->fullname}}
                                        @endif
                                       </u></td>
                                    <td><br><br><br><br><br> <u>{{$data[0]->user_approve}}</u></td>
                                </tr>
                            </table>
                        </div>

                    </div>

                </div>



            <!-- end:: Subheader -->
            </div>


        </div>
    </body>
</html>
