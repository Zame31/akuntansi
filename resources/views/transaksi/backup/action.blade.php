<script type="text/javascript">
var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  <button onclick="znClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                    Data</button>
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;
function znClose() {
    znIconboxClose();
    //$("#form-new").data('bootstrapValidator').resetForm();
    $("#form-new")[0].reset();
    location.reload();
    //$("#dataAmotisasi").html('');
}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('list_tr_baru') }}');
}

	var _create_form = $("#form-new");
	var tbl_dok = $("#tbl_dok");
	$('#ll').hide();
	function simpan(type){
		 //$("#global-loader").show();
		loadingPage();
		if(type=='multi'){
			var cek_dbt = $("#dbt").html();
			var cek_dbt1=cek_dbt.replace(/\./g,'');
			var cek_crt = $("#crt").html();
			var cek_crt1=cek_crt.replace(/\./g,'');
		}else{
			var cek_dbt = $("#debet").val();
			var cek_dbt1=cek_dbt.replace(/\./g,'');
			var cek_crt = $("#kredit").val();
			var cek_crt1=cek_crt.replace(/\./g,'');
		}





		if(cek_dbt1==cek_crt1){
			/*
			let value = [];
			$("#form-new").find("input[type=file]").each(function(index, field){
			  value.push(field.value);
			  console.log(field.files.length);
			for(var i=0;i<field.files.length;i++) {
			  	//const file = field.fidetail_les[i];
			    const ext=$(field).val().replace(/^.*\./, '');

			    console.log(i);
			   if(i==0){
			   	var size=$('#file')[0].files[0].size;
				console.log(size);
		 		var extension=$('#file').val().replace(/^.*\./, '');
		 		console.log(extension);
		 		if(size >= 2000002){
		 			endLoadingPage();
			 		$( "#file" ).addClass("is-invalid");
					$('#ss').hide();
					$('#ff').hide();
					$('#mm').show();
					return false;

		 		}
		 		//[,'jpeg','jpg','png','xls','xlxs']
		 		if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
		 			endLoadingPage();
		 			$( "#file" ).addClass("is-invalid");
					$('#ss').hide();
					$('#ff').show();
					$('#mm').hide();
					return false;
		 		}

				$( "#file" ).removeClass( "is-invalid" );

			   }else{
			   alert('aaa');
				}
			  }
			});


			if(value == 0){
				endLoadingPage();
				$( "#file" ).addClass("is-invalid");
				$('#ss').show();
				$('#ff').hide();
				$('#mm').hide();
			}
			*/



			if($('#tgl_tr').val()===''){
	 			endLoadingPage();
	 			$( "#tgl_tr" ).addClass( "is-invalid" );
	 			return false;
		 	}else{
		 		$("#tgl_tr").removeClass( "is-invalid" );

		 	}
			if($('#catatan').val()===''){
	 			endLoadingPage();
	 			$( "#catatan" ).addClass( "is-invalid" );
	 			return false;
		 	}else{
		 		$("#catatan").removeClass( "is-invalid" );

		 	}
		 	if($('#idsegment').val()===''){
		 		endLoadingPage();
	 			$( "#idsegment" ).addClass( "is-invalid" );
	 			return false;
		 	}else{
		 		$("#idsegment").removeClass( "is-invalid" );

		 	}

		 	if(cek_dbt1==0 && cek_crt1==0){
				endLoadingPage();
				$('#ll').show();
				//swal("Info","Tidak Boleh Kosong / 0","error");
				return false;
			}else{
				$('#ll').hide();
				var _form_data = new FormData(_create_form[0]);
				$.ajax({
	                type: 'POST',
	                url: base_url + '/insert_tr_baru',
	                data: _form_data,
	                processData: false,
	                contentType: false,
	       			dataType: 'json',
	                success: function (res) {
	                    //var parse = $.parseJSON(res);
	                    if(res.rc==1){
	                    	//$("#global-loader").hide();
	                		endLoadingPage();
	                		znIconbox("Data Berhasil Disimpan",text,action);


			                    /*
			                    swal.fire({
		                            title: 'Info',
		                            text: "Berhasil",
		                            type: 'success',
		                            confirmButtonText: 'Tutup',
		                            reverseButtons: true
		                        }).then(function(result){
		                            if (result.value) {
		                                _create_form[0].reset();
		                                //location.reload();
		                            }
		                        });
		                        */
	                		//swal("Info","Transaksi Berhasil Disimpan","info")
	                    }

	                }
	            });

			}



		}else{
			endLoadingPage();
			swal.fire("Info","Total Debet dan Kredit tidak seimbang","error");

		}




	}



	var _items='';
	$.ajax({
        type: 'GET',
        url: base_url + '/coa',
        success: function (res) {
            var data = $.parseJSON(res);
            _items='<option value="">Silahkan pilih</option>';
            $.each(data, function (k,v) {
                _items += "<option value='"+v.coa_no+"'>"+v.coa_no+" - "+v.coa_name+"</option>";
            });

            $('#coba').html(_items);
        }
    });
    $("#dbt").html(0);
    //.toFixed() method will roundoff the final sum to 2 decimal places
    $("#crt").html(0);

    $('#coba').on('change', function (v) {
    	loadingPage();
	    document.getElementById("kredit").value=0;
		document.getElementById("debet").value=0;
	    $.ajax({
		        type: 'GET',
		        url: base_url + '/dbcr?id='+this.value,
		        success: function (res) {
		        	endLoadingPage();
		        var data = $.parseJSON(res);
		        if(data[0]['balance_type_id']==0){
		        	$('#debet').css("background-color", "#00FFFF");
		        	$('#kredit').css("background-color", "");

		        	$(debet).addClass('zn-is-debet');
		        	$(kredit).removeClass('zn-is-kredit');
		        	document.getElementById("kredit").readOnly = true;
		        	document.getElementById("debet").readOnly = false;

		        }else{
		        	$('#debet').css("background-color", "");
		        	$('#kredit').css("background-color", "#00FFFF");

                    $(debet).removeClass('zn-is-debet');
		        	$(kredit).addClass('zn-is-kredit');
					document.getElementById("kredit").readOnly = false;
		        	document.getElementById("debet").readOnly = true;

		        }
		     }
		});
	    calculateSum();
	});
	calculateSum();
	 $(document).ready(function () {
    	var counter = 1;
    	var def = 'new';
    	var counter1 = 1;
    	var query1=[];

		$("#addrow1").on("click", function () {
	        var newRow = $("<tr>");
	        var cols = "";

	        cols += '<td><select class="form-control" id="jns_dok'+ counter1 +'" name="jns_dok[]">'+
						               	'<option>a</option>'+
						               	'<option>b</option>'+
						               '</select></td>';
	        cols += '<td><input type="file" class="form-control" name="file[]" id="file' + counter1 + '"/></td>';
	        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="-"></td>';
	        newRow.append(cols);
	        $("table.dokumen-list").append(newRow);
	        counter1++;
	    });

	    $("table.dokumen-list").on("click", ".ibtnDel", function (event) {
	        $(this).closest("tr").remove();
	        counter1 -= 1
	        calculateSum();
	    });






    $("#addrow").on("click", function () {
    	console.log(def);
   		if(def=='new'){
				var coba="#coba" + counter;
				var ket="#keterangan" + counter;
				var debet="#debet" + counter;
				var kredit="#kredit" + counter;
				var bdebet="debet" + counter;
				var ckredit="kredit" + counter;

			}else{
				var aa =counter -1;
				var coba="#coba" + aa;
				var ket="#keterangan" + aa;
				var debet="#debet" + aa;
				var kredit="#kredit" + aa;
				var bdebet="debet" + aa;
				var ckredit="kredit" + aa;


			}

		if($('#coba').val()===''){
	 		$( "#coba" ).addClass( "is-invalid state-invalid" );
	 		return false;
	 	}else{
	 		$( "#coba" ).removeClass( "is-invalid state-invalid" );

	 	}
		if($('#keterangan').val()===''){
	 		$( "#keterangan" ).addClass( "is-invalid state-invalid" );
	 		return false;

	 	}else{
	 		$( "#keterangan" ).removeClass( "is-invalid state-invalid" );

	 	}

	 	if($('#kredit').is('[readonly]')){

	 	}else{
	 		var a1=$('#kredit').val();
       		var a=a1.replace(/\./g,'');
	 		if(a=='' || a==0){
	 			$( "#debet" ).removeClass( "is-invalid state-invalid" );
	 			$( "#kredit" ).addClass( "is-invalid state-invalid" );
	 			return false;
	 		}else{

	 			$( "#kredit" ).removeClass("is-invalid state-invalid");
	 		}
	 	}

	 	if($('#debet').is('[readonly]')){


	 	}else{
	 		var b1=$('#debet').val();
       		var b=b1.replace(/\./g,'');

	 		if(b=='' || b==0){
	 			$( "#kredit" ).removeClass( "is-invalid state-invalid" );
	 			$( "#debet" ).addClass( "is-invalid state-invalid" );
	 			return false;

	 		}else{
	 			$( "#debet" ).removeClass( "is-invalid state-invalid" );
	 		}
	 	}




		if($(coba).val()===''){
	 		$(coba).addClass( "is-invalid state-invalid" );
	 		return false;
	 	}else{
	 		$(coba).removeClass( "is-invalid state-invalid" );

	 	}
		if($(ket).val()===''){
	 		$(ket).addClass( "is-invalid state-invalid" );
	 		return false;

	 	}else{
	 		$(ket).removeClass( "is-invalid state-invalid" );

	 	}

	 	if($(kredit).is('[readonly]')){

	 	}else{
	 		var a1=$(kredit).val();
       		//var a=a1.replace(/\./g,'');
	 		if(a1=='' || a1==0){
	 			$(debet).removeClass( "is-invalid state-invalid" );
	 			$(kredit).addClass( "is-invalid state-invalid" );
	 			return false;
	 		}else{
	 			$(kredit).removeClass("is-invalid state-invalid");

	 		}
	 	}

	 	if($(debet).is('[readonly]')){


	 	}else{
	 		var b1=$(debet).val();
       		//var b=b1.replace(/\./g,'');

	 		if(b1=='' || b1==0){
				$(kredit).removeClass( "is-invalid state-invalid" );
	 			$(debet).addClass( "is-invalid state-invalid" );
	 			return false;

	 		}else{
	 			$(debet).removeClass( "is-invalid state-invalid" );

	 		}
	 	}






	 	var newRow = $("<tr>");
        var cols = "";

        cols += '<td><select class="form-control select2-show-search" name="coa_no[]" id="coba'+ counter +'" tabindex="-1" aria-hidden="true"></select><div class="invalid-feedback">Pilih No Akun</div></td>';
        cols += '<td><input type="text" class="form-control" name="keterangan[]" id="keterangan' + counter + '"/><div class="invalid-feedback">Keterangan Tidak Boleh Kosong </div></td>';
        cols += '<td><input type="text" class="form-control txt" style="text-align: right;" value="0" onkeyup="convertToRupiah(this)" id="debet'+ counter +'" name="debet[]"/><div class="invalid-feedback">Nominal Debet Tidak Boleh Kosong atau 0</div></td>';
        cols += '<td><input type="text" class="form-control txt1" style="text-align: right;" value="0" onkeyup="convertToRupiah(this)" id="kredit'+ counter +'" name="kredit[]"/><div class="invalid-feedback">Nominal Kredit Tidak Boleh Kosong atau 0</div></td>';
        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger" style="width:50px;"  value="-"></td>';
        newRow.append(cols);
		$("table.order-list").append(newRow);
		var coba="#coba" + counter;
		var ket="#keterangan" + counter;
		var debet="#debet" + counter;
		var kredit="#kredit" + counter;
		var bdebet="debet" + counter;
		var ckredit="kredit" + counter;

		var _items='';
	 	$.ajax({
	 		type: 'GET',
	 		url: base_url + '/coa',
	 		success: function (res) {
	 			var data = $.parseJSON(res);
	 			_items='<option value="">Silahkan pilih</option>';
	 			$.each(data, function (k,v) {
	 				_items += "<option value='"+v.coa_no+"'>"+v.coa_no+" - "+v.coa_name+"</option>";
	 			});
	 			console.log(coba);
	 			$(coba).html(_items);
	 		}
	 	});

	 	$(coba).select2();
	    $(coba).on('change', function (v) {

	    	document.getElementById(ckredit).value=0;
		    document.getElementById(bdebet).value=0;
		    $.ajax({
			        type: 'GET',
			        url: base_url + '/dbcr?id='+this.value,
			        success: function (res) {
			        var data = $.parseJSON(res);
			        if(data[0]['balance_type_id']==0){
			        	$(debet).addClass('zn-is-debet');
		        		$(kredit).removeClass('zn-is-kredit');

			        	document.getElementById(ckredit).readOnly = true;
			        	document.getElementById(bdebet).readOnly = false;

			        }else{
			        	$(debet).removeClass('zn-is-debet');
		        		$(kredit).addClass('zn-is-kredit');

						document.getElementById(ckredit).readOnly = false;
			        	document.getElementById(bdebet).readOnly = true;

			        }
			     }
			});
		    calculateSum();
		});
		def='old';
	    counter++;
    });

    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        counter -= 1
        calculateSum();
    });


});




function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}
function calculateSum() {
    var sum = 0;
    var sum1 = 0;
    //iterate through each textboxes and add the values
    $(".txt").each(function () {
    	var a1=this.value;
        var a=a1.replace(/\./g,'');

        //add only if the value is number
        if (!isNaN(a) && a.length != 0) {
            sum += parseFloat(a);
        }

    });
    $(".txt1").each(function () {
    	var a1=this.value;
        var a=a1.replace(/\./g,'');

        //add only if the value is number
        if (!isNaN(a) && a.length != 0) {
            sum1 += parseFloat(a);
        }

    });
    $("#dbt").html(sum.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    //.toFixed() method will roundoff the final sum to 2 decimal places
    $("#crt").html(sum1.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}
/*
$(".txt").on("keyup", function () {
    calculateSum();
});
$(".txt1").on("keyup", function () {
    calculateSum();
});
*/
</script>
<script type="text/javascript">

var counter_ = 1;
var def = 'new';
function add_row_new(){

	let value = [];
    $(".coano > option:selected").each(function() {

	value.push(this.value);
	//Add operations here

	});

	if(def=='new'){
		var coba="#coba" + counter_;
		var ket="#keterangan" + counter_;
		var debet="#debet" + counter_;
		var kredit="#kredit" + counter_;
		var bdebet="debet" + counter_;
		var ckredit="kredit" + counter_;

	}else{
		var aa =counter_ -1;
		var coba="#coba" + aa;
		var ket="#keterangan" + aa;
		var debet="#debet" + aa;
		var kredit="#kredit" + aa;
		var bdebet="debet" + aa;
		var ckredit="kredit" + aa;


	}
	if($('#coba').val()===''){
	 		$( "#coba" ).addClass( "is-invalid" );
	 		return false;
	 	}else{
	 		$( "#coba" ).removeClass( "is-invalid" );

	 	}
		if($('#keterangan').val()===''){
	 		$( "#keterangan" ).addClass( "is-invalid" );
	 		return false;

	 	}else{
	 		$( "#keterangan" ).removeClass( "is-invalid" );

	 	}

	 	if($('#kredit').is('[readonly]')){

	 	}else{
	 		var a1=$('#kredit').val();
       		var a=a1.replace(/\./g,'');
	 		if(a=='' || a==0){
	 			$( "#debet" ).removeClass( "is-invalid" );
	 			$( "#kredit" ).addClass( "is-invalid" );
	 			return false;
	 		}else{

	 			$( "#kredit" ).removeClass("is-invalid");
	 		}
	 	}

	 	if($('#debet').is('[readonly]')){


	 	}else{
	 		var b1=$('#debet').val();
       		var b=b1.replace(/\./g,'');

	 		if(b=='' || b==0){
	 			$( "#kredit" ).removeClass( "is-invalid" );
	 			$( "#debet" ).addClass( "is-invalid" );
	 			return false;

	 		}else{
	 			$( "#debet" ).removeClass( "is-invalid" );
	 		}
	 	}




		if($(coba).val()===''){
	 		$(coba).addClass( "is-invalid" );
	 		return false;
	 	}else{
	 		$(coba).removeClass( "is-invalid" );

	 	}
		if($(ket).val()===''){
	 		$(ket).addClass( "is-invalid" );
	 		return false;

	 	}else{
	 		$(ket).removeClass( "is-invalid" );

	 	}

	 	if($(kredit).is('[readonly]')){

	 	}else{
	 		var a1=$(kredit).val();
       		//var a=a1.replace(/\./g,'');
	 		if(a1=='' || a1==0){
	 			$(debet).removeClass( "is-invalid" );
	 			$(kredit).addClass( "is-invalid" );
	 			return false;
	 		}else{
	 			$(kredit).removeClass("is-invalid");

	 		}
	 	}

	 	if($(debet).is('[readonly]')){


	 	}else{
	 		var b1=$(debet).val();
       		//var b=b1.replace(/\./g,'');

	 		if(b1=='' || b1==0){
				$(kredit).removeClass( "is-invalid" );
	 			$(debet).addClass( "is-invalid" );
	 			return false;

	 		}else{
	 			$(debet).removeClass( "is-invalid" );

	 		}
	 	}
	var newRow = $("<tr>");
	var cols = "";
	cols +='<div class="row mt-2 ph">';
	cols += '<div class="col-3"><select class="form-control kt-select2 init-select2 coano" name="coa_no[]" id="coba'+counter_+'"></select><div class="invalid-feedback">Pilih No Akun</div></div>';

	cols += '<div class="col-4"><textarea class="form-control" name="keterangan[]" id="keterangan'+counter_+'" rows="1"></textarea><div class="invalid-feedback">Keterangan Tidak Boleh Kosong </div></div>';
	cols += '<div class="col-2"><input style="text-align:right;" id="debet'+counter_+'" name="debet[]" placeholder="0" class="form-control txt" maxlength="11" onkeyup="convertToRupiah___(this)" type="text"><div class="invalid-feedback">Nominal Debet Tidak Boleh Kosong atau 0</div></div>';

	cols += '<div class="col-2"><input style="text-align:right;" id="kredit'+counter_+'" name="kredit[]" placeholder="0" class="form-control txt1" maxlength="11" onkeyup="convertToRupiah___(this)" type="text"><div class="invalid-feedback">Nominal Kredit Tidak Boleh Kosong atau 0</div></div>';

	cols += '<div class="col-1"><button type="button" class="btn btn-danger btn-elevate btn-icon ibtnDel"><i class="la la-trash"></i></button></div>';
	cols +='</div>';
	newRow.append(cols);
	$("table.order-list").append(newRow);
	//$(".table").append(cols);
	var coba="#coba" + counter_;
	var ket="#keterangan" + counter_;
	var debet="#debet" + counter_;
	var kredit="#kredit" + counter_;
	var bdebet="debet" + counter_;
	var ckredit="kredit" + counter_;
	var _items='';
	 	loadingPage();
	 	$.ajax({
	 		type: 'GET',
	 		url: base_url + '/coa',
	 		data: {value: value},
            async: false,
	 		success: function (res) {
	 			var data = $.parseJSON(res);
	 			_items='<option value="">Silahkan pilih</option>';
	 			$.each(data, function (k,v) {
	 				_items += "<option value='"+v.coa_no+"'>"+v.coa_no+" - "+v.coa_name+"</option>";
	 			});
	 			console.log(coba);
	 			$(coba).html(_items);
	 			endLoadingPage();
	 		}
	 	});

	 	//$(coba).select2();
	 	$(coba).select2({
	 		placeholder: "Silahkan Pilih"
	 	});

	    $(coba).on('change', function (v) {

	    	document.getElementById(ckredit).value=0;
		    document.getElementById(bdebet).value=0;

		    $.ajax({
			        type: 'GET',
			        url: base_url + '/dbcr?id='+this.value,
			        success: function (res) {
			        var data = $.parseJSON(res);
			        if(data[0]['balance_type_id']==0){
			        	$(debet).css("background-color", "#00FFFF");
		        		$(kredit).css("background-color", "");
			        	$(debet).addClass('zn-is-debet');
		        		$(kredit).removeClass('zn-is-kredit');

			        	document.getElementById(ckredit).readOnly = true;
			        	document.getElementById(bdebet).readOnly = false;

			        }else{

			        	$(debet).css("background-color", "");
		        		$(kredit).css("background-color", "#00FFFF");

			        	$(debet).removeClass('zn-is-debet');
		        		$(kredit).addClass('zn-is-kredit');

						document.getElementById(ckredit).readOnly = false;
			        	document.getElementById(bdebet).readOnly = true;

			        }
			     }
			});

		    calculateSum();
		});
		calculateSum();
	def='old';
	counter_ ++;
}

$("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        //$( "ph" ).eq( 1 ).removeClass();
       // counter_ -= 1
        calculateSum();
    });

///dokumen

var counter1_ = 1;
var def_ = 'new';
function add_row_new_dok(){
	let value1 = [];
    $(".dokno > option:selected").each(function() {

    value1.push(this.value);
    //Add operations here

    });

	if(def_=='new'){
		var file="#file" + counter1_;

	}else{
		var aa =counter1_ -1;
		var file="#file" + aa;
		var ss="#ss" + aa;
		var ff="#ff" + aa;
		var mm="#mm" + aa;
		var jns_dok="#jns_dok" + aa;

	}
	//document.getElementById("videoUploadFile").files.length == 0


	if($('#file').val()===''){
		$("#file").addClass("is-invalid");
		$('#ss').show();
		$('#ff').hide();
		$('#mm').hide();
		return false;
	}else{
		var size=$('#file')[0].files[0].size;
		console.log(size);
 		var extension=$('#file').val().replace(/^.*\./, '');
 		console.log(extension);
 		if(size >= 2000002){
	 		$( "#file" ).addClass("is-invalid");
			$('#ss').hide();
			$('#ff').hide();
			$('#mm').show();
			return false;

 		}
 		//[,'jpeg','jpg','png','xls','xlxs']
 		if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
 			$( "#file" ).addClass("is-invalid");
			$('#ss').hide();
			$('#ff').show();
			$('#mm').hide();
			return false;
 		}

		$( "#file" ).removeClass( "is-invalid" );

	}
	if($(jns_dok).val() ===''){
		$(jns_dok).addClass( "is-invalid" );
		return false;
	}else{
		$( jns_dok ).removeClass( "is-invalid" );

	}
	if($(file).val() ===''){
		$(file).addClass( "is-invalid" );
		$(ss).show();
		$(ff).hide();
		$(mm).hide();
		return false;
	}else{
		if(typeof  $(file).val() === 'undefined'){
			//console.log('eweuh');
		}else{
			//console.log($(file).val());
			var size=$(file)[0].files[0].size;
			//console.log(size);
	 		var extension=$(file).val().replace(/^.*\./, '');
	 		//console.log(extension);
	 		if(size >= 2000002){
		 		$( file ).addClass("is-invalid");
				$(ss).hide();
				$(ff).hide();
				$(mm).show();
				return false;

	 		}
	 		//[,'jpeg','jpg','png','xls','xlxs']
	 		if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
	 			$( file ).addClass("is-invalid");
				$(ss).hide();
				$(ff).show();
				$(mm).hide();
				return false;
	 			}
			$( file ).removeClass( "is-invalid" );
		}


	}
	var newRow = $("<tr>");
	var cols = "";
	cols +='<div class="row mt-2">';
	cols += '<div class="col-5"><select class="form-control kt-select2 init-select2 dokno" name="jns_dok[]" id="jns_dok'+counter1_+'"></select><div class="invalid-feedback">Silahkan Pilih Jenis Dokumen</div></div>';
    cols += ' <div class="offset-1 col-5"><input type="file"  class="form-control" name="file[]" id="file'+counter1_+'"><div class="invalid-feedback" id="ss'+counter1_+'">Silahkan Upload Dokumen</div><div class="invalid-feedback" id="ff'+counter1_+'">Format Dokumen pdf,jpeg,jpg,png,xls,xlxs</div><div class="invalid-feedback" id="mm'+counter1_+'">Max Size Dokumen 2 Mb</div></div>';
    cols += '<div class="col-1"><button type="button" class="btn btn-danger btn-elevate btn-icon ibtnDel_"><i class="la la-trash"></i></button></div>';
	cols +='</div>';
	newRow.append(cols);
	$("table.table-dok").append(newRow);
	//$(".table-dok").append(cols);
	var jns_dok="#jns_dok" + counter1_;
	var _items='';
	 	$.ajax({
	 		type: 'GET',
	 		url: base_url + '/ref_dokumen',
	 		data: {value: value1},
            async: false,
	 		success: function (res) {
	 			var data = $.parseJSON(res);
	 			console.log(data);
	 			if(data.length!=0){
	 				_items='<option value="">Silahkan pilih</option>';
		 			$.each(data, function (k,v) {
		 				_items += "<option value='"+v.id+"'>"+v.definition+"</option>";
		 			});
		 			console.log(jns_dok);
		 			$(jns_dok).html(_items);
	 			}else{
	 				console.log('ga ada data');
	 				$(jns_dok).closest("tr").remove();
	 			}

	 		}
	 	});
	$(jns_dok).select2();
	def_='old';
	counter1_ ++;
}
$("table.table-dok").on("click", ".ibtnDel_", function (event) {
        $(this).closest("tr").remove();
        //$( "ph" ).eq( 1 ).removeClass();
       // counter_ -= 1
      //  calculateSum();
    });



///detail
function detail_(id,type){
	loadingPage();
	window.location.href = base_url +'transaksi_detail?id=' + id +'&type='+type;
	endLoadingPage();
}

$('#example-select-all').click(function (e) {
    	$(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
	});
function kirimsemua() {
        swal.fire({
           title: "Info",
           text: "Kirim Semua Approval",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAproval?type=semua',
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);

                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });

                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });

   }

   function hapussemua() {
        swal.fire({
           title: "Info",
           text: "Hapus Semua Transaksi",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/hapusAproval?type=semua',
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
   }

function kirim() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin mengirim record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAproval?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                    endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
    }
}

function hapus() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin menghapus record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/hapusAproval?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                    endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
    }
}

function convertToRupiah___ (objek) {
if(objek.value==0){
    objek.value='';
 }else{
 var value = objek.value;
 value = value.replace(/^(0*)/,"");
 objek.value=value;

  separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g,"");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c;
calculateSum();

 }
}


function ubah(type_tx,tx_code){

	if(type_tx=='opex'){

		window.location.href = base_url +'update_opex?id=' + tx_code;


	}

}


</script>
