@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Accounting </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Overdue Payment</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet  kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Overdue Payment
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <a href="#" onclick="paid();" class="btn btn-pill btn-primary" style="color: white;">Paid Selected</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                        <thead>
                            <tr>
                                <th width="30px">
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="" class="kt-group-checkable"
                                            id="example-select-all">
                                        <span></span>
                                    </label>
                                </th>
                                <th width="30px">Aksi</th>
                                {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                <th>ID</th>
                                <th>Branch Code</th>
                                <th>Customer Name</th>
                                <th>Product Name</th>
                                <th>Premi Amount</th>
                                <th>Paid Status</th>

                            </tr>
                        </thead>
                        <tbody>
                                @if($data)
                                @foreach($data as $item)
                                <tr>
                                    <td>
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="{{$item->id}}" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#" onclick="detail_invoice({{$item->id}})"><i
                                                    class="la la-clipboard"></i> Detail</a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->short_code}}</td>
                                    <td>{{$item->full_name}}</td>
                                    <td>{{$item->definition}}</td>
                                    <td>{{number_format($item->premi_amount,2,',','.')}}</td>
                                    <td>{{$item->paid}}</td>

                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                    </table>
                </div>
            </div>
        </div>


        <!-- end:: Subheader -->

    </div>
</div>
<script type="text/javascript">
    $('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});

function detail_invoice(id){
    loadingPage();
    $.ajax({
        type: 'GET',
        url: base_url + 'detail_invoice?id='+id,
        success: function (res) {
            var data = $.parseJSON(res);
            console.log(data);
            endLoadingPage();
            $('#tgl_tr').html(formatDate(new Date(data[0].inv_date)));
            $('#inv_no').html(data[0].inv_no);
            $('#product').html(data[0].produk);
            $('#agent').html(data[0].agent);
            $('#start_date').html(formatDate(new Date(data[0].start_date)));
            $('#end_date').html(formatDate(new Date(data[0].end_date)));
            $('#quotation').html(data[0].quotation);
            $('#proposal').html(data[0].proposal);
            $('#segment').html(data[0].segment);
            $('#wf_status').html("Status "+data[0].wf_status);
            $('#user_maker').html(data[0].user_create);
            $('#user_approval').html(data[0].user_approval);

            $('#ins_amount').html(data[0].ins_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#disc_amount').html(data[0].disc_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#net_amount').html(data[0].net_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#agent_fee_amount').html(data[0].agent_fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#comp_fee_amount').html(data[0].comp_fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#premi_amount').html(data[0].premi_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

            $('#bank').html(data[0].bank);

            $('#zn_stat').val(data[0].wf_status_id);
            $('#zn_paid_stat').val(data[0].paid_status_id);

            // CETAK AKTIF
            $('#ca_inv').html(' NO: '+data[0].inv_no);

            $('#ca_agent').html(data[0].full_name);
            $('#ca_address').html(data[0].c_address);
            $('#ca_product').html(': '+data[0].produk);
            $('#ca_terbilang').html('In Words (Indonesia) : # '+terbilang(data[0].premi_amount));

            var new_periode = formatDate(new Date(data[0].start_date)) +' - '+formatDate(new Date(data[0].end_date));

            $('#ca_periode').html(': '+new_periode);
            $('#ca_sumins').html(': '+data[0].ins_amount);
            $('#ca_premi_amount').html(data[0].premi_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#ca_total').html(data[0].premi_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#ca_grand_total').html(data[0].premi_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#ca_user_approval').html(data[0].user_approval);
            $('#ca_com').html(data[0].company_name);
            // CETAK PAID

            $('#pa_inv').html(' NO: '+data[0].inv_no);
            $('#pa_agent').html(data[0].agent);
            $('#pa_product').html(data[0].produk);
            $('#pa_premi_amount').html(data[0].premi_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            $('#pa_user_approval').html(data[0].user_approval);

            $('#pa_terbilang').html(terbilang(data[0].premi_amount));


            $('#detail').modal('show');

        }
    });
}
function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}
function paid() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan realisasi jurnal split payment?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/split_paid',
                        data: {value: value},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    });
            }
        });
    }
}
</script>
@include('transaksi.invoice_modal')
@stop
