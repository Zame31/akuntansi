@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Accounting </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                            Success Transaction List </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet  kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            Success Transaction List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            @if (Auth::user()->user_role_id == 1)
                                <a href="#" onclick="approve_reversal()" class="btn btn-pill btn-primary" style="color: white;">Reversal Selected</a>
                            @endif
                        </div>
                    </div>
                        {{-- <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-more-1"></i>
                            </a>
                        </div> --}}
                    </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="30px">Aksi</th>
                                    {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                    <th>Branch Code</th>
                                    <th>Kode Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Jumlah</th>
                                    <th>User Name</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if($data)
                                @foreach($data as $item)
                                <tr>
                                    <td>
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="{{$item->tx_code}}" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                    </td>
                                    <td>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" onclick="detail_('{{$item->tx_code}}','trx_sukses')"  href="#"><i class="la la-clipboard"></i> Detail</a>
                                                {{-- <a class="dropdown-item" href="#"><i class="la la-edit"></i> Ubah</a> --}}
                                            </div>
                                        </div>
                                    </td>
                                     {{-- <td style="padding:15px;" align="center"><input type="checkbox" name="id[]" value="{{$item->tx_code}}"/></td> --}}
                                    <td>{{$item->short_code}}</td>
                                    <td>{{$item->tx_code}}</td>
                                    <td>{{ date('d M Y',strtotime($item->tx_date)) }}</td>
                                    <td>{{ number_format($item->total,2,',','.') }}</td>
                                    <td>{{ $item->fullname }}</td>

                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
<script type="text/javascript">
function approve_reversal() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin akan melakukan reversal dan mengirim permintaan approval ?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
               $.ajax({
                        type: 'POST',
                        url: base_url + '/kirim_reversal',
                        data: {value: value},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    }); 
            }
        });
    }
}
</script>
@include('transaksi.action')
@stop
