@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Accounting </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                            Success Reversal List</a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet  kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            Success Reversal List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                   
                    </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    
                                    <th width="30px">Aksi</th>
                                    {{-- <th align="center"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th> --}}
                                    <th>Branch Code</th>
                                    <th>Kode Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Jumlah</th>
                                    <th>User Name</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if($data)
                                @foreach($data as $item)
                                <tr>
                                    <td>
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" onclick="detail_('{{$item->tx_code}}','reversal_sukses')"  href="#"><i class="la la-clipboard"></i> Detail</a>
                                                {{-- <a class="dropdown-item" href="#"><i class="la la-edit"></i> Ubah</a> --}}
                                            </div>
                                        </div>
                                    </td>
                                     {{-- <td style="padding:15px;" align="center"><input type="checkbox" name="id[]" value="{{$item->tx_code}}"/></td> --}}
                                    <td>{{$item->short_code}}</td>
                                    <td>{{$item->tx_code}}</td>
                                    <td>{{ date('d M Y',strtotime($item->tx_date)) }}</td>
                                    <td>{{ number_format($item->total,2,',','.') }}</td>
                                    <td>{{ $item->fullname }}</td>

                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('transaksi.action')
@stop
