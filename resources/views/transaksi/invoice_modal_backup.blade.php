<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="border: none;">
            {{-- <div class="modal-header">
                <i class="la la-clipboard zn-icon-modal"></i>
                <h5 class="modal-title" style="margin-top: 10px !important;" id="exampleModalLongTitle">
                    Detail Invoice
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div> --}}
            <div class="modal-body p-0" id="svg">

                <div class="kt-invoice-1">
                        {{-- style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});" --}}
                    <div class="kt-invoice__head" style="background: #c020ff;">
                        <div class="kt-invoice__container">
                            <div class="text-right">
                            <button onclick="cetakViewInvoice()" type="button"
                            class="mb-5 znBtnShow btn btn-danger btn-hover-info btn-pill mb-2">
                            <i class="flaticon2-printer"></i> Print & Download</button>
                                <button data-dismiss="modal" aria-label="Close" type="button"
                                    class="mb-5 znBtnShow btn btn-light btn-hover-info btn-pill">Kembali</button>
                            </div>
                            <div class="kt-invoice__brand">
                                    <div class="row znHeaderShow" style="width: 100%;margin-bottom:10px;">
                                            <div class="col-12" style="margin-top: -10px;">
                                                <div style="display: inline-block;">
                                                    <img alt="Logo" src="{{asset('img/logo_cetak.jpg')}}" style="width: 40px;margin-top: -25px;" />
                                                </div>
                                                <div style="display: inline-block;">
                                                    <span class="zn-text-logo" style="display: block;color:#ffffff;font-weight: bold;font-size: 20px;">INVOICE</span>
                                                    <span  id="wf_status" style="display: block;margin-left: 8px;color:#ffffff;font-size:10px;"></span>
                                                </div>
                                                {{-- <div class="text-right mt-2" style="float:right;">
                                                        <span style="text-transform: uppercase;display: block;font-weight: bold;font-size:14px;color:#ffffff;">Invoice</span>
                                                        <span style="display: block;font-size:10px;color:#ffffff;"></span>
                                                    </div> --}}
                                            </div>
                                        </div>
                                {{-- <h1 class="kt-invoice__title" style="font-size:1.5rem;">INVOICE</h1> --}}
                                <div href="#" class="kt-invoice__logo">

                                    {{-- <a href="#" onclick="kembali();" class="btn btn-pill btn-primary">Kembali</a> --}}
                                </div>
                            </div>
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle"><b>INVOICE DATE.</b></span>
                                    <span class="kt-invoice__text" id="tgl_tr"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                    <span class="kt-invoice__text" id="inv_no"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">PRODUCT NAME.</span>
                                    <span class="kt-invoice__text" id="product"></span>
                                </div>
                            </div>
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">OFFICER NAME.</span>
                                    <span class="kt-invoice__text" id="agent"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">START DATE COVERAGE.</span>
                                    <span class="kt-invoice__text" id="start_date"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">END DATE COVERAGE.</span>
                                    <span class="kt-invoice__text" id="end_date"></span>
                                </div>
                            </div>
                            <div class="kt-invoice__items zn-desc-invoice">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">QUOTATION.</span>
                                    <span class="kt-invoice__text" id="quotation"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">PROPOSAL.</span>
                                    <span class="kt-invoice__text" id="proposal"></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">SEGMENT.</span>
                                    <span class="kt-invoice__text" id="segment"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-invoice__body">
                        <div class="kt-invoice__container">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>DESCRIPTION</th>
                                            <th>AMOUNT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Insurance</td>
                                            <td>
                                                <div id="ins_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Disc</td>
                                            <td>
                                                <div id="disc_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nett</td>
                                            <td>
                                                <div id="net_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Fee Agent</td>
                                            <td>
                                                <div id="agent_fee_amount"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Fee Internal</td>
                                            <td>
                                                <div id="comp_fee_amount"></div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="kt-invoice__footer">
                        <div class="kt-invoice__container">
                            <div class="kt-invoice__bank">
                                <div class="kt-invoice__title"> <b>BANK TRANSFER</b></div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__label">Account Name:</span>
                                    <span class="kt-invoice__value" id="bank"></span></span>
                                </div>
                            </div>
                            <div class="kt-invoice__total">
                                <span class="kt-invoice__title"><b>PREMI AMOUNT</b></span>
                                <span class="kt-invoice__price" id="premi_amount"></span>
                            </div>
                            <div class="row znSignature" style="margin-top: 40px;width: 100%;display:none;">
                                <div class="col-6" style="font-size:10px;color: #595d6e;">
                                    User Maker
                                    <br><br><br><br><br>
                                    <b><u id="user_maker"></u></b>
                                </div>
                                <div class="col-6" style="font-size:10px;color: #595d6e;">
                                    User Approval
                                    <br><br><br><br><br>
                                    <b><u id="user_approval"></u></b>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade scrollStyle" data-backdrop="static" data-keyboard="false" id="zn-print" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content" {{-- style="border-radius: 0px;border: none;" --}}>
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Print & Download Preview Invoice</h5>
                <button type="button" onclick="closeCetakViewInvoice()" class="close"></button>
            </div>
            <div class="modal-body kt-portlet m-0 p-0">
                <div class="">
                    <div class="kt-portlet__body p-0">

                        <iframe id='znresult' class="scrollStyle"
                            style="width: 100%;height: 520px;border: none;"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
