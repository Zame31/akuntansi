<script type="text/javascript">
    $('#example-select-all').click(function (e) {
        $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
    });
    function detail_app(id){

        loadingPage();
        window.location.href = base_url +'Approval/detail?id=' + id;
        endLoadingPage();
    }

function kirimsemua1() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
       value.push(this.value);
    });
    if(value.length == 1){
        swal.fire("Info!", "Tidak ada data", "warning");
    }else{
        swal.fire({
            title: "Info",
           text: "Approve Semua",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAprove?type=semua',
                    success: function (res) {

                        var data = $.parseJSON(res);
                        console.log(data.rm);
                        


                        swal.fire({
                            title: "Informasi",
                            text: data.rm,
                            type: "success",
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });

                        endLoadingPage();
                        console.log(res);
                        
                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });

    }

        
   }

   function hapussemua1() {
     let value = [];
    $('input[type="checkbox"]').each(function(){
       value.push(this.value);
    });
    if(value.length == 1){
        swal.fire("Info!", "Tidak ada data", "warning");
    }else{
        swal.fire({
        title: "Info",
        text: "Tolak Semua Transaksi",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/hapusAprove?type=semua',
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: "Informasi",
                            text: "Berhasil",
                            type: "success",
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });

    }

       
   }

function kirim1() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{

        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin approve record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAprove?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {
                        endLoadingPage();

                        console.log(res);
                        var data = $.parseJSON(res);

                        if (data.rc == '99') {
                            swal.fire("Error",data.rm,"error");
                        }else{
                            swal.fire({
                                title: "Informasi",
                                text: "Berhasil",
                                type: "success",
                                confirmButtonColor: "#8cd4f5",
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });
                        }

                        
                        
                    }
                }).done(function( res ) {
                    endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
    }
}

function hapus1() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin menolak record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/hapusAprove?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: "Informasi",
                            text: "Berhasil",
                            type: "success",
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                    endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
    }
}
</script>