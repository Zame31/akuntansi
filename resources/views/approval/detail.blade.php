@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Transaksi </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Detail Transaksi </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Detail Transaksi
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button class="btn btn-success" onclick="kembali_det()">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <div class="row">
                <div class="col-3">
                    <div class="form-group col-12">
                        <h6>Kode Transaksi</h6>
                        <div class="input-group">
                            {{$data[0]->tx_code}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Segment</h6>
                        <div class="input-group">
                            {{$data[0]->definition}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Tanggal Transaksi</h6>
                        <div class="input-group">
                            {{date('d M Y',strtotime($data[0]->tx_date))}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Jumlah</h6>
                        <div class="input-group">
                            @php
                            $total=0;
                            @endphp
                            @foreach($data as $item)
                                @php
                                if($item->tx_type_id==0){
                                $total+=$item->tx_amount;
                                }
                                @endphp
                            @endforeach
                            {{number_format($total,0,',','.')}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Keterangan</h6>
                        <div class="input-group">
                            {{$data[0]->notes}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>User</h6>
                        <div class="input-group">
                            {{$data[0]->fullname}}
                        </div>
                    </div>
                </div>
                <div class="col-9">
                    <div class="row">
                        <div class="col-xl-3"></div>
                        <div class="col-xl-6">
                            <div class="kt-timeline-v1 kt-timeline-v1--justified">
                                <div class="kt-timeline-v1__items">
                                    <div class="kt-timeline-v1__marker"></div>
                                    @foreach($data as $item)
                                    <div class="kt-timeline-v1__item ">
                                        <div class="kt-timeline-v1__item-circle">
                                            <div class="kt-bg-danger"></div>
                                        </div>
                                        <span class="kt-timeline-v1__item-time kt-font-brand">
                                            {{date('H:i',strtotime($item->created_at))}}
                                        </span>
                                        <div class="kt-timeline-v1__item-content">
                                            <div class="kt-timeline-v1__item-title">
                                                {{$item->coa_id}} - {{$item->coa_name}}
                                            </div>
                                            <div class="kt-timeline-v1__item-body">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <p>
                                                            {{$item->tx_notes}}
                                                        </p>
                                                    </div>

                                                    <div class="col-4 text-right">
                                                        <div class="btn-group btn-group  mb-2" style="width: 100%;display: block;" role="group"
                                                            aria-label="...">
                                                            <span class="btn btn-secondary">Debet</span>
                                                            @if($item->tx_type_id==0)
                                                            <span class="btn btn-success ">Rp.{{number_format($item->tx_amount,0,',','.')}}</span>
                                                            @else
                                                            <span class="btn btn-success ">Rp.0</span>
                                                            @endif
                                                        </div>
                                                        <div class="btn-group btn-group" style="width: 100%;display: block;" role="group" aria-label="...">
                                                            <span class="btn btn-secondary">Kredit</span>
                                                            @if($item->tx_type_id==1)
                                                            <span class="btn btn-danger ">Rp.{{number_format($item->tx_amount,0,',','.')}}</span>
                                                            @else
                                                            <span class="btn btn-danger ">Rp.0</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3"></div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script type="text/javascript">
function kembali_det(){
    window.location.href = base_url +'list_app_baru';
}
</script>
@stop
