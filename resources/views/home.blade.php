@section('content')

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Dashboard </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>


                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                            <span class="btn btn-label-info">
                                @php
                                    $dateC = collect(\DB::select("select * from ref_system_date"))->first();
                                @endphp
                                {{ date("d M Y", strtotime($dateC->current_date))}}
                            </span>
                        <div class="btn-group dropleft">
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Filter
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" onclick="get_dashboard('year_t_date')"  href="#">Year to Date</a>
                                <a class="dropdown-item" onclick="get_dashboard('t_month')" href="#">Current Month</a>
                                <a class="dropdown-item" onclick="get_dashboard('t_last_month')" href="#">Last Month</a>
                            </div>
                        </div>


{{--
                        @if(Auth::user()->user_role_id==5 || Auth::user()->user_role_id==6)
                        @if($role==5 || $role==6)

                        <div class="btn-group dropleft">
                            <select class="form-control kt-select2 init-select2" name="idbranch" id="idbranch">
                                <option value="konsol">All Branch</option>
                                @foreach($branch as $item)
                                <option value="{{$item->id}}">{{$item->branch_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        @if(Auth::user()->user_role_id)
                        <div class="btn-group dropleft">
                            <select class="form-control kt-select2 init-select2" name="idcompany" id="idcompany">
                                <option value="konsol">All Company</option>
                                @foreach($company as $item)
                                <option value="{{$item->id}}">{{$item->company_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
--}}


                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                <div class="col-12">


                    <!--begin:: Widgets/Activity-->
                    <div
                        class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid">
                        <div class="kt-portlet__head kt-portlet__head--noborder kt-portlet__space-x">
                            <div class="kt-portlet__head-label">
                            </div>
                            <div class="kt-portlet__head-toolbar">
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-widget17">
                                <div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides"
                                    style="background: url('{{ asset('img/pat2.png') }}');
                                                    background-repeat: repeat;">
                                    <div class="kt-widget17__chart" style="height:20px;">
                                        <canvas id="kt_chart_activities"></canvas>
                                    </div>
                                </div>
                                <div class="kt-widget17__stats">
                                    <div class="kt-widget17__items">
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--info">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M6.182345,4.09500888 C6.73256296,3.42637697 7.56648864,3 8.5,3 L15.5,3 C16.4330994,3 17.266701,3.42600075 17.8169264,4.09412386 C17.8385143,4.10460774 17.8598828,4.11593789 17.8809917,4.1281251 L22.5900048,6.8468751 C23.0682974,7.12301748 23.2321726,7.73460788 22.9560302,8.21290051 L21.2997802,11.0816097 C21.0236378,11.5599023 20.4120474,11.7237774 19.9337548,11.4476351 L18.5,10.6198563 L18.5,19 C18.5,19.5522847 18.0522847,20 17.5,20 L6.5,20 C5.94771525,20 5.5,19.5522847 5.5,19 L5.5,10.6204852 L4.0673344,11.4476351 C3.58904177,11.7237774 2.97745137,11.5599023 2.70130899,11.0816097 L1.04505899,8.21290051 C0.768916618,7.73460788 0.932791773,7.12301748 1.4110844,6.8468751 L6.12009753,4.1281251 C6.14061376,4.11628005 6.16137525,4.10524462 6.182345,4.09500888 Z"
                                                            fill="#000000" opacity="0.3" />
                                                        <path
                                                            d="M9.85156673,3.2226499 L9.26236944,4.10644584 C9.11517039,4.32724441 9.1661011,4.62457583 9.37839459,4.78379594 L11,6 L10.0353553,12.7525126 C10.0130986,12.9083095 10.0654932,13.0654932 10.1767767,13.1767767 L11.6464466,14.6464466 C11.8417088,14.8417088 12.1582912,14.8417088 12.3535534,14.6464466 L13.8232233,13.1767767 C13.9345068,13.0654932 13.9869014,12.9083095 13.9646447,12.7525126 L13,6 L14.6216054,4.78379594 C14.8338989,4.62457583 14.8848296,4.32724441 14.7376306,4.10644584 L14.1484333,3.2226499 C14.0557004,3.08355057 13.8995847,3 13.7324081,3 L10.2675919,3 C10.1004153,3 9.94429962,3.08355057 9.85156673,3.2226499 Z"
                                                            fill="#000000" />
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="kt-widget17__desc mt-3">
                                                    <b id="c_customer">0</b> IDR
                                                </span>
                                            <span class="kt-widget17__subtitle mt-1">
                                                Kewajiban Tampungan Premi
                                            </span>

                                        </div>
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <path d="M4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 Z" fill="#000000" opacity="0.3"/>
                                                                <path d="M18.5,11 L5.5,11 C4.67157288,11 4,11.6715729 4,12.5 L4,13 L8.58578644,13 C8.85100293,13 9.10535684,13.1053568 9.29289322,13.2928932 L10.2928932,14.2928932 C10.7456461,14.7456461 11.3597108,15 12,15 C12.6402892,15 13.2543539,14.7456461 13.7071068,14.2928932 L14.7071068,13.2928932 C14.8946432,13.1053568 15.1489971,13 15.4142136,13 L20,13 L20,12.5 C20,11.6715729 19.3284271,11 18.5,11 Z" fill="#000000"/>
                                                                <path d="M5.5,6 C4.67157288,6 4,6.67157288 4,7.5 L4,8 L20,8 L20,7.5 C20,6.67157288 19.3284271,6 18.5,6 L5.5,6 Z" fill="#000000"/>
                                                            </g>
                                                        </svg>
                                            </span>
                                            <span class="kt-widget17__desc mt-3">
                                                    <b id="c_insured">0</b> IDR
                                                </span>
                                            <span class="kt-widget17__subtitle mt-1">
                                                Premium Unpaid
                                            </span>

                                        </div>
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--brand">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                                                            fill="#000000" opacity="0.3" />
                                                        <path
                                                            d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                                                            fill="#000000" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="9" width="7"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="9" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="13" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="13" width="7"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="17" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="17" width="7"
                                                            height="2" rx="1" />
                                                    </g>
                                                </svg> </span>
                                                <span class="kt-widget17__desc mt-3">
                                                        <b id="c_premi">0</b> IDR
                                                    </span>
                                            <span class="kt-widget17__subtitle mt-1">
                                                Agent Fee Unpaid
                                            </span>

                                        </div>
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--info">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z"
                                                            fill="#000000" opacity="0.3"
                                                            transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) " />
                                                        <path
                                                            d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z"
                                                            fill="#000000" />
                                                    </g>
                                                </svg> </span>

                                            <span class="kt-widget17__desc mt-3">
                                                <b id="c_fee">0</b> IDR
                                            </span>
                                            <span class="kt-widget17__subtitle mt-1">
                                                    HD Commision Unpaid
                                                </span>

                                        </div>
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--danger">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" opacity="0.3" x="12" y="4" width="3"
                                                            height="13" rx="1.5" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="9" width="3"
                                                            height="8" rx="1.5" />
                                                        <path
                                                            d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z"
                                                            fill="#000000" fill-rule="nonzero" />
                                                        <rect fill="#000000" opacity="0.3" x="17" y="11" width="3"
                                                            height="6" rx="1.5" />
                                                    </g>
                                                </svg> </span>
                                                <span class="kt-widget17__desc mt-3">
                                                        <b id="c_profit">0</b> IDR
                                                </span>
                                            <span class="kt-widget17__subtitle mt-1">
                                                Profit / Loss
                                            </span>

                                        </div>
                                    </div>
                                    <div class="kt-widget17__items">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end:: Widgets/Activity-->
                </div>



            </div>

            <div class="row">
                <div class="col-12">
                        <div class="kt-portlet kt-portlet--height-fluid" style="padding: 40px;">
                                <canvas id="crt" height="400"></canvas>

                        </div>
                            </div>

            </div>

            <div class="row">
                    <div class="col-6">

                        <!--begin:: Widgets/Profit Share-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                        Sales By Customer Type
                                    </h3>
                                    <span class="kt-widget14__desc">
                                        Give Information How Many Sales and Presentase Sales By Customer Type
                                    </span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                        <canvas id="sales_customer" width="250"></canvas>
                                    </div>
                                    <div class="kt-widget14__legends" id="cust_presentase">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end:: Widgets/Profit Share-->
                    </div>
                    <div class="col-6">

                        <!--begin:: Widgets/Revenue Change-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                        Sales by Segment
                                    </h3>
                                    <span class="kt-widget14__desc">
                                        Give Information How Many Sales and Presentase Sales By Segment
                                    </span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                            <canvas id="sales_segment" width="250"></canvas>
                                    </div>
                                    <div class="kt-widget14__legends" id="seg_presentase">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end:: Widgets/Revenue Change-->
                    </div>
                </div>


            <!--Begin::Row-->
            <div class="row">

                <div class="col-6">
                    <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    <i class="flaticon-trophy zn-trophy"></i> TOP 3 Sales Performance
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">

                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <div class="kt-widget4" id="top_sales">

                            </div>


                        </div>
                    </div>

                    <!--end:: Widgets/New Users-->
                </div>
                <div class="col-6">
                    <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    <i class="flaticon-trophy zn-trophy"></i> TOP 3 Client
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">

                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <div class="kt-widget4" id="top_premi">

                            </div>


                        </div>
                    </div>

                    <!--end:: Widgets/New Users-->
                </div>
            </div>


            <!--End::Row-->

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>

<script>
get_dashboard("year_t_date");

function get_dashboard(filter) {

        $('#cust_presentase').html('');
        $('#seg_presentase').html('');
        $('#top_sales').html('');
        $('#top_premi').html('');

		var act_url = '{{ route('data.dashboard', ':id') }}';
        act_url = act_url.replace(':id', filter);

        $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingPage();
           },
        success: function (res) {

            var data = $.parseJSON(res);


			$('#c_insured').html(numFormatNew(parseFloat(data['c_insured']['d']).toFixed(2)));
			$('#c_customer').html(numFormatNew(parseFloat(data['c_customer']['d']).toFixed()));
			$('#c_premi').html(numFormatNew(parseFloat(data['c_premi']['d']).toFixed(2)));
			$('#c_fee').html(numFormatNew(parseFloat(data['c_fee']['d']).toFixed(2)));
			$('#c_profit').html(numFormatNew(parseFloat(data['c_profit']['d']).toFixed(2)));


            var data_graf =[];
			var data_color = ["#00C5DC","#F4516C","#FFB822","#8859E0","#0C5484","#66BB6A","#00838F","#e57373"];
			var data_tanggal = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

            var btn_color = ['brand','warning','danger'];

            var color_i = 0;

            console.log(data['grafik_premi']);


            $.each(data['grafik_premi'], function (k,v) {

                data_graf.push({
                    label: k,
                    data: v,
                    fill: false,
                    backgroundColor: data_color[color_i],
                    borderColor: data_color[color_i],
                });

                color_i++;
            });


            console.log(data['cust_pres']);

            $.each(data['cust_pres'], function (k,v) {

                $('#cust_presentase').append(`
                    <div class="kt-widget14__legend">
                        <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                        <span class="kt-widget14__stats">`+parseFloat(v).toFixed(2)+` % `+data['cust_def'][k]+`</span>
                    </div>
                `);

            });

            $.each(data['seg_pres'], function (k,v) {

                $('#seg_presentase').append(`
                    <div class="kt-widget14__legend">
                        <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                        <span class="kt-widget14__stats">`+parseFloat(v).toFixed(2)+` % `+data['seg_def'][k]+`</span>
                    </div>
                `);

            });

            $.each(data['t_sales_data'], function (k,v) {

                $('#top_sales').append(`
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                            <img src="assets/media/users/default.jpg" alt="">
                        </div>
                        <div class="kt-widget4__info">
                            <a href="#" class="kt-widget4__username">
                                `+data['t_sales_nama'][k]+`
                            </a>
                            <p class="kt-widget4__text">
                                `+numFormatNew( parseFloat(data['t_sales_data'][k]).toFixed(2))+` IDR
                            </p>
                        </div>
                        <a href="#" class="btn btn-sm btn-label-`+btn_color[k]+` btn-bold">`+parseFloat(data['t_sales_pres'][k]).toFixed(2)+`% to Overall Sales</a>

                    </div>
                `);

            });

            $.each(data['t_premi_data'], function (k,v) {

                $('#top_premi').append(`
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                            <img src="assets/media/users/default.jpg" alt="">
                        </div>
                        <div class="kt-widget4__info">
                            <a href="#" class="kt-widget4__username">
                                `+data['t_premi_nama'][k]+`
                            </a>
                            <p class="kt-widget4__text">
                            `+numFormatNew( parseFloat(data['t_premi_data'][k]).toFixed(2))+` IDR
                            </p>
                        </div>
                        <a href="#" class="btn btn-sm btn-label-`+btn_color[k]+` btn-bold">`+parseFloat(data['t_premi_pres'][k]).toFixed(2)+`% to Overall Premi</a>

                    </div>
                `);

            });



            var ctx = document.getElementById("crt");

            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                labels: data_tanggal,
                datasets: data_graf
            },
                options: {
                            responsive: true,
                            title: {
                                display: false,
                                text: 'Premi'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Bulan'
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Frekuensi'
                                    }
                                }]
                            }
                        }
            });


            var ctx = document.getElementById('sales_customer').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: data['cust_def'],
                    datasets: [{
                        backgroundColor: data_color,
                        data: data['cust_data']
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scaleLabel: function(label){return label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                }
            });


            var ctx = document.getElementById('sales_segment').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: data['seg_def'],
                    datasets: [{
                        backgroundColor: data_color,
                        data: data['seg_data']
                    }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });

        }
    }).done(function( msg ) {
        endLoadingPage();
    });
	}



</script>
@stop
