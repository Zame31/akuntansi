@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Installment </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    {{$tittle}}</a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form {{$tittle}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">

                        <button onclick="saveData()" class="btn btn-success">Simpan</button>
                        @if ($type == 'edit')
                            <button onclick="loadNewPage('{{ route('installment.index') }}?type={{$type_form}}')" class="btn btn-danger">Kembali</button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
           
                <div class="row">
                <form id="form-valid">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Interval Periode (Days)</label>
                                    <div class="kt-spinner kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                        <input value="{{ ($mc) ? $mc[0]->interval: (($mc_polis) ? $mc_polis[0]->interval : 0) }}" type="text" maxlength="2" onkeyup="convertToRupiah(this)" class="form-control" id="interval" name="interval">
                                    </div>

                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Validate Periode (Days)</label>
                                    <div class="kt-spinner kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                        <input value="{{ ($mc) ? $mc[0]->days_overdue:(($mc_polis) ? $mc_polis[0]->days_overdue : 0) }}" type="text" maxlength="2" onkeyup="convertToRupiah(this)" class="form-control" id="days_overdue" name="days_overdue">
                                    </div>

                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Installment Periode No</label>
                                    <div class="kt-spinner kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                        <input value="{{ ($mc) ? $mc[0]->tenor:(($mc_polis) ? $mc_polis[0]->tenor : 0) }}" type="text" maxlength="2" onkeyup="convertToRupiah(this)" class="form-control" id="tenor" name="tenor">
                                    </div>

                                </div>
                            </div>
                             <div class="col-2">
                                <div class="form-group">
                                    <label>Start Date Installment</label>
                                    <div class="kt-spinner kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                        <input value="{{ ($mc) ? date('d-m-Y',strtotime($mc[0]->date)):(($mc_polis) ? date('d-m-Y',strtotime($mc_polis[0]->date)) : "")  }}" type="text" class="form-control" id="start_date" name="start_date">
                                    </div>

                                </div>


                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Nett Premium</label>
                            <input type="text" readonly value="{{number_format($ms->net_amount,2,",",".")}}" class="form-control" id="purchase_amount" name="purchase_amount">
                                
                                </div>
                            </div>
                            
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Premium</label>
                                <input type="text" readonly value="{{number_format($ms->premi_amount,2,",",".")}}" class="form-control" id="premi_amount" name="premi_amount">
                                </div>
                            </div>


                        </div>

                        <button onclick="generateAmortisasi();" class="btn btn-success">Generate Installment Schedule</button>
                        <br><br>
                    </div>
                </form>

            <form id="form-data" enctype="multipart/form-data" style="overflow: auto;">
                <input type="hidden" name="get_id" id="get_id" value="{{$ms->id}}">

                    <div class="col-12">


                        <div class="kt-portlet" id="scrollStyle" style="height: 500px; overflow: auto;">
                            <div class="kt-portlet__body" style="width: 1800px;">
                                <h5 class="zn-head-line-success" style="margin-bottom:40px;">Installment Schedule</h5>
                                <div class="row"
                                    style="margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                    <div class="col-1">
                                        <h6>Date</h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>Date Validate</h6>
                                    </div>
                                    <div class="col-2 ">
                                        <h6>Premium</h6> 
                                    </div>
                                    <div class="col-2">
                                        <h6>Net Premium </h6>
                                    </div>
                                    <div class="col-2">
                                        <h6>Outstanding</h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>Commission Due to Us</h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>Agent Fee</h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>Agent Tax Amount</h6>
                                    </div>  
                                    <div class="col-1">
                                        <h6>Nett to Underwriter</h6>
                                    </div>
                                </div>
                                <div id="dataAmotisasi">
                                    @if ($mc)
                                        @foreach ($mc as $key => $v)
                                        <div class="row">
                                        <div class="col-1">
                                            <div class="form-group">
                                                <input style="text-align:right;" type="text" value="{{ date('d-m-Y',strtotime($v->date)) }}" class="form-control date-amor" id="dateamor`+i+`" name="dateamor[]">
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <input style="text-align:right;" type="text" value="{{ date('d-m-Y',strtotime($v->due_date_jrn)) }}" class="form-control date-amor" id="dateamordue`+i+`" name="dateamordue[]">
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <input style="text-align:right;" value="{{number_format($v->premi_amount,2,",",".")}}" id="n_premi{{$key+1}}" name="n_premi[]" placeholder="0"
                                                class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
                                        </div>
                                        <div class="col-2">
                                            <input style="text-align:right;" value="{{number_format($v->net_amount,2,",",".")}}" id="amor{{$key+1}}" name="amor[]" placeholder="0"
                                                class="form-control txt" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
                                        </div>
                                        <div class="col-2">
                                            <input readonly style="text-align:right;" value="{{number_format($v->outstanding,2,",",".")}}" id="outstanding{{$key+1}}" name="outstanding[]" placeholder="0"
                                                class="form-control txt1" type="text">
                                        </div>
                                        <div class="col-1">
                                            <input style="text-align:right;" value="{{number_format($v->comp_fee_amount,2,",",".")}}" id="comm_due{{$key+1}}" name="comm_due[]" placeholder="0"
                                                class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
                                        </div>
                                        <div class="col-1">
                                            <input style="text-align:right;" value="{{number_format($v->agent_fee_amount,2,",",".")}}" id="fee_agent{{$key+1}}" name="fee_agent[]" placeholder="0"
                                                class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
                                        </div>
                                        <div class="col-1">
                                            <input style="text-align:right;" value="{{number_format($v->tax_amount,2,",",".")}}" id="agent_tax{{$key+1}}" name="agent_tax[]" placeholder="0"
                                                class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
                                        </div>
                                        <div class="col-1">
                                            <input style="text-align:right;" value="{{number_format($v->underwriter_amount,2,",",".")}}" id="underwriter{{$key+1}}" name="underwriter[]" placeholder="0"
                                                class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
                                        </div>
                                    </div>
                                        @endforeach
                                    @endif


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="kt-portlet__head" style="border-top: 1px solid #e2e5ec;">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    {{-- <i class="flaticon-grid-menu"></i> --}}
                </span>
                <h3 class="kt-portlet__head-title">
                  
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">

                        <button onclick="saveData()" class="btn btn-success">Simpan</button>
                        @if ($type == 'edit')
                            <button onclick="loadNewPage('{{ route('installment.index') }}?type={{$type_form}}')" class="btn btn-danger">Kembali</button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('installment.action')
@stop
