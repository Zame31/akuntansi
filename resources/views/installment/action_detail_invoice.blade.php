<script>

function detail_invoice_schedule(id){
   
   loadingPage();
   $.ajax({
       type: 'GET',
       url: base_url + 'detail_invoice_schedule?id='+id,
       success: function (res) {
           var data = $.parseJSON(res);
           console.log(data);
           endLoadingPage();

           $('#ins_amount_visible').hide();
           $('#zn-invoice-tittle').html('INSTALLMENT DEBIT NOTE');
           $('#zn-tittle-premi').html('NET PREMI AMOUNT');
           
           $('#segmentView').hide();
           $('#insView').hide();
           
           $('#zn_stat').val(data[0].id_workflow);
           $('#zn_paid_stat').val(data[0].paid_status_id);

           var getWf = data[0].id_workflow;
           var getPaid = data[0].paid_status_id;

       if (getPaid == 0) {
           $("#setWord").attr("href", base_url+'printwordSc/sc/aktif/'+id);
       }else {
           $("#setWord").attr("href", base_url+'printwordSc/sc/paid/'+id);
       }
           $('#tgl_tr').html(formatDate(new Date(data[0].inv_date)));
           $('#inv_no').html(data[0].inv_no);
           $('#product').html(data[0].produk);
           $('#agent').html(data[0].agent);
           $('#start_date').html(formatDate(new Date(data[0].start_date)));
           $('#end_date').html(formatDate(new Date(data[0].end_date)));
           $('#quotation').html(data[0].quotation);
           $('#proposal').html(data[0].proposal);
           $('#segment').html(data[0].segment);
           $('#wf_status').html("Status "+data[0].wf_status);
           $('#user_maker').html(data[0].user_create);
           $('#user_approval').html(data[0].user_approval);

           // NORMAL
           $('#ins_amount').html(formatMoney(data[0].ins_amount));
           $('#premi_amount1').html(formatMoney(data[0].premi_amount));
           $('#disc_amount').html(formatMoney(data[0].disc_amount));
           $('#net_amount').html(formatMoney(data[0].net_amount));
           $('#agent_fee_amount').html(formatMoney(data[0].agent_fee_amount));
           $('#comp_fee_amount').html(formatMoney(data[0].tax_amount));
           $('#premi_amount').html(formatMoney(data[0].net_amount));
           
           // VALUTA

           if (data[0].valuta_id != 1) {
               $('#zn-tittle-premi-lang').html('NETT PREMI AMOUNT');

               $('#tittle_lang').html(data[0].mata_uang+' AMOUNT');
               $('#ins_amount_lang').html(formatMoney(data[0].ins_amount/data[0].kurs_today));
               $('#premi_amount1_lang').html(formatMoney(data[0].premi_amount/data[0].kurs_today));
               $('#disc_amount_lang').html(formatMoney(data[0].disc_amount/data[0].kurs_today));
               $('#net_amount_lang').html(formatMoney(data[0].net_amount/data[0].kurs_today));
               $('#agent_fee_amount_lang').html(formatMoney(data[0].agent_fee_amount/data[0].kurs_today));
               $('#comp_fee_amount_lang').html(formatMoney(data[0].tax_amount/data[0].kurs_today));
               $('#premi_amount_lang').html(formatMoney(data[0].net_amount/data[0].kurs_today));
               
           }
      
           $('#bank').html(data[0].bank);
           $('#zn_stat').val(data[0].id_workflow);
           $('#zn_paid_stat').val(data[0].paid_status_id);
           $('#get_id').val(data[0].id);

           // CETAK AKTIF
           if (data[0].inv_no == null) {
               $('#ca_inv').html(' NO: ');
           }else {
               $('#ca_inv').html(' NO: '+data[0].inv_no);
           }
           $('#ca_address').html(data[0].c_address);
           $('#ca_product').html(': '+data[0].produk);
           $('#ca_insured').html(': '+data[0].full_name);
           $('#ca_polis').html(': '+data[0].polis_no);
           $('#ca_underwriter').html(': '+data[0].underwriter);
           var new_periode = formatDateNew(new Date(data[0].date));
           $('#ca_periode').html(': '+new_periode);
           
           //var premi = parseFloat(data[0].premi_amount);
           var premi = parseFloat(data[0].net_amount);
           // console.log(premi);

           premi = premi.toFixed(2);

      
           $('#ca_user_approval').html(data[0].user_approval);
           $('#ca_com').html(data[0].full_name);
           $('#ca_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
           $('#ca_ttd').html(data[0].leader_name);
           $('#ca_ttd_jab').html(data[0].jabatan);
           var new_adm = parseFloat(data[0].polis_amount) + parseFloat(data[0].materai_amount) + parseFloat(data[0].admin_amount);        
           new_adm = new_adm.toFixed(2);

           var new_total =  parseFloat(new_adm) + parseFloat(premi);

           if (data[0].valuta_id != 1) {
               new_total = (new_total/data[0].kurs_today).toFixed(2);
           }

           var sisa_desimal = 0;

           if ((new_total % 1) > 0.5) {
               sisa_desimal = (1.00 - (new_total % 1).toFixed(2)).toFixed(2);
               new_total = Math.ceil(new_total);
           }else {
               sisa_desimal = (new_total % 1).toFixed(2);
               new_total = Math.floor(new_total);
           }


           new_total = new_total.toFixed(2);
           var total_fix = Math.round(new_total);
           var total_terbilang_fix = Math.round(new_total);


           
           if (data[0].valuta_id != 1) {
               premi = premi/data[0].kurs_today;
               new_adm = (new_adm/data[0].kurs_today).toFixed(2);               
               total_fix = (total_fix/data[0].kurs_today).toFixed(2);
               $('#ca_sumins').html(': '+data[0].mata_uang+" "+ formatMoney(data[0].ins_amount/data[0].kurs_today));
               $('.mata_uang_set').html(data[0].mata_uang);

               
               premi = premi.toFixed(2);
               
           }else {
               
               $('#ca_sumins').html(': '+data[0].mata_uang+" "+ formatMoney(data[0].ins_amount));
           
           }

           $('#ca_premi_amount').html(premi.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#ca_adm').html(new_adm.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#sisa_desimal').html("("+sisa_desimal+")");
           $('#ca_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#ca_grand_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#ca_terbilang').html('In Words (Indonesia) : # '+terbilang(total_terbilang_fix)+data[0].deskripsi);

           // CETAK PAID
           if (data[0].kwitansi_no == null) {
               $('#pa_inv').html(' NO: ');
           }else {
               $('#pa_inv').html(' NO: '+data[0].kwitansi_no);
           }

           $('#pa_agent').html(data[0].polis_no+' - '+data[0].full_name);
           $('#pa_product').html(data[0].produk);
           $('#pa_premi_amount').html(data[0].mata_uang+" "+new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#pa_user_approval').html(data[0].user_approval);
           $('#pa_terbilang').html(terbilang(total_terbilang_fix)+data[0].deskripsi);
           $('#pa_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
           $('#pa_ttd').html(data[0].leader_name);
           $('#pa_ttd_jab').html(data[0].jabatan);
           $('#detail').modal('show');

       }
   });
}


function detail_invoice(id){
   
   loadingPage();
   $.ajax({
       type: 'GET',
       url: base_url + 'detail_invoice?id='+id,
       success: function (res) {
           var data = $.parseJSON(res);
           console.log(data);
           endLoadingPage();

           $('#zn-invoice-tittle').html('INSTALLMENT DEBIT NOTE');
           $('#zn-tittle-premi').html('NET PREMI AMOUNT');
           
           $('#segmentView').hide();
           $('#insView').hide();
           
           $('#zn_stat').val(data[0].id_workflow);
           $('#zn_paid_stat').val(data[0].paid_status_id);

           var getWf = data[0].id_workflow;
           var getPaid = data[0].paid_status_id;

       if (getPaid == 0) {
           $("#setWord").attr("href", base_url+'printwordSc/normal/aktif/'+id);
       }else {
           $("#setWord").attr("href", base_url+'printwordSc/normal/paid/'+id);
       }
       $('#tgl_tr').html(formatDate(new Date(data[0].inv_date)));
           $('#inv_no').html(data[0].inv_no);
           $('#product').html(data[0].produk);
           $('#agent').html(data[0].agent);
           $('#start_date').html(formatDate(new Date(data[0].start_date)));
           $('#end_date').html(formatDate(new Date(data[0].end_date)));
           $('#quotation').html(data[0].quotation);
           $('#proposal').html(data[0].proposal);
           $('#segment').html(data[0].segment);
           $('#wf_status').html("Status "+data[0].wf_status);
           $('#user_maker').html(data[0].user_create);
           $('#user_approval').html(data[0].user_approval);

           // NORMAL
           $('#ins_amount').html(formatMoney(data[0].ins_amount));
           $('#premi_amount1').html(formatMoney(data[0].premi_amount));
           $('#disc_amount').html(formatMoney(data[0].disc_amount));
           $('#net_amount').html(formatMoney(data[0].net_amount));
           $('#agent_fee_amount').html(formatMoney(data[0].agent_fee_amount));
           $('#comp_fee_amount').html(formatMoney(data[0].tax_amount));
           $('#premi_amount').html(formatMoney(data[0].net_amount));
           
           // VALUTA

           if (data[0].valuta_id != 1) {
               $('#zn-tittle-premi-lang').html('NETT PREMI AMOUNT');

               $('#tittle_lang').html(data[0].mata_uang+' AMOUNT');
               $('#ins_amount_lang').html(formatMoney(data[0].ins_amount/data[0].kurs_today));
               $('#premi_amount1_lang').html(formatMoney(data[0].premi_amount/data[0].kurs_today));
               $('#disc_amount_lang').html(formatMoney(data[0].disc_amount/data[0].kurs_today));
               $('#net_amount_lang').html(formatMoney(data[0].net_amount/data[0].kurs_today));
               $('#agent_fee_amount_lang').html(formatMoney(data[0].agent_fee_amount/data[0].kurs_today));
               $('#comp_fee_amount_lang').html(formatMoney(data[0].tax_amount/data[0].kurs_today));
               $('#premi_amount_lang').html(formatMoney(data[0].net_amount/data[0].kurs_today));
               
           }
      
           $('#bank').html(data[0].bank);
           $('#zn_stat').val(data[0].id_workflow);
           $('#zn_paid_stat').val(data[0].paid_status_id);
           $('#get_id').val(data[0].id);

           // CETAK AKTIF
           if (data[0].inv_no == null) {
               $('#ca_inv').html(' NO: ');
           }else {
               $('#ca_inv').html(' NO: '+data[0].inv_no);
           }
           $('#ca_address').html(data[0].c_address);
           $('#ca_product').html(': '+data[0].produk);
           $('#ca_insured').html(': '+data[0].full_name);
           $('#ca_polis').html(': '+data[0].polis_no);
           $('#ca_underwriter').html(': '+data[0].underwriter);
           if (data[0].start_date = '1970-01-01') {
            var new_periode = '';
           }else{
            var new_periode = formatDateNew(new Date(data[0].start_date));
           }
           
           $('#ca_periode').html(': '+new_periode);
           
           //var premi = parseFloat(data[0].premi_amount);
           var premi = parseFloat(data[0].net_amount);
           // console.log(premi);

           premi = premi.toFixed(2);

      
           $('#ca_user_approval').html(data[0].user_approval);
           $('#ca_com').html(data[0].full_name);
           $('#ca_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
           $('#ca_ttd').html(data[0].leader_name);
           $('#ca_ttd_jab').html(data[0].jabatan);
           var new_adm = parseFloat(data[0].polis_amount) + parseFloat(data[0].materai_amount) + parseFloat(data[0].admin_amount);        
           new_adm = new_adm.toFixed(2);

           var new_total =  parseFloat(new_adm) + parseFloat(premi);

           if (data[0].valuta_id != 1) {
               new_total = (new_total/data[0].kurs_today).toFixed(2);
           }

           var sisa_desimal = 0;

           if ((new_total % 1) > 0.5) {
               sisa_desimal = (1.00 - (new_total % 1).toFixed(2)).toFixed(2);
               new_total = Math.ceil(new_total);
           }else {
               sisa_desimal = (new_total % 1).toFixed(2);
               new_total = Math.floor(new_total);
           }


           new_total = new_total.toFixed(2);
           var total_fix = Math.round(new_total);
           var total_terbilang_fix = Math.round(new_total);


           console.log(premi);
           
           if (data[0].valuta_id != 1) {
               premi = premi/data[0].kurs_today;
               new_adm = (new_adm/data[0].kurs_today).toFixed(2);               
               total_fix = (total_fix/data[0].kurs_today).toFixed(2);
               $('#ca_sumins').html(': '+data[0].mata_uang+" "+ formatMoney(data[0].ins_amount/data[0].kurs_today));
               $('.mata_uang_set').html(data[0].mata_uang);
               premi = premi.toFixed(2);
           }else {
               
               $('#ca_sumins').html(': '+data[0].mata_uang+" "+ formatMoney(data[0].ins_amount));
           
           }

         

           console.log(premi);
           
           $('#ca_premi_amount').html(premi.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#ca_adm').html(new_adm.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#sisa_desimal').html("("+sisa_desimal+")");
           $('#ca_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#ca_grand_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#ca_terbilang').html('In Words (Indonesia) : # '+terbilang(total_terbilang_fix)+' '+data[0].deskripsi);

           // CETAK PAID
           if (data[0].kwitansi_no == null) {
               $('#pa_inv').html(' NO: ');
           }else {
               $('#pa_inv').html(' NO: '+data[0].kwitansi_no);
           }

           $('#pa_agent').html(data[0].polis_no+' - '+data[0].full_name);
           $('#pa_product').html(data[0].produk);
           $('#pa_premi_amount').html(data[0].mata_uang+" "+new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
           $('#pa_user_approval').html(data[0].user_approval);
           $('#pa_terbilang').html(terbilang(total_terbilang_fix)+data[0].deskripsi);
           $('#pa_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
           $('#pa_ttd').html(data[0].leader_name);
           $('#pa_ttd_jab').html(data[0].jabatan);
           $('#detail').modal('show');

       }
   });
}
</script>