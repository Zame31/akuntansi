<script>

$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});


var act_url = '{{ route('data_schedule_cancel.installment') }}';

var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { targets: [3,4,5,6,7], className: 'text-right' },
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'cek', name: 'cek' },
        // { data: 'action', name: 'action' },
        { data: 'full_name', name: 'full_name' },
        { data: 'date', name: 'date' },
        // { data: 'ins_amount', name: 'ins_amount' },
        { data: 'net_amount', name: 'net_amount' },
        // { data: 'outstanding', name: 'outstanding' },
        { data: 'comp_fee_amount', name: 'comp_fee_amount' },
        { data: 'agent_fee_amount', name: 'agent_fee_amount' },
        { data: 'tax_amount', name: 'tax_amount' },
        { data: 'underwriter_amount', name: 'underwriter_amount' }


        // { data: 'is_active', name: 'is_active' }
    ]
});
</script>
