@section('content')
@php
function convertMonth($data)
{
$bulan = array (
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);

echo $bulan[$data];
}
@endphp
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Installment </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Installment Schedule Payment </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Installment Schedule Payment
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                            @if ($mi->id_workflow == 9)
                            @if (Auth::user()->user_role_id == 1)
                                
                           
                        {{-- <button type="button"
                        onclick="gActAll('payAmor','/kirimAprovalInstallmentSc?type=semua','{{ route('data.installment') }}','{{route('installment.detail',$getId)}}?type=success');"
                        class="btn btn-info btn-elevate btn-elevate-air">Payment All</button> --}}
                        <button type="button"
                        onclick="gActSelected('payInstallment','/kirimAprovalInstallmentSc?type=satu','{{ route('data.installment') }}','{{route('installment.detail',$getId)}}?type=success');"
                        class="btn btn-info btn-elevate btn-elevate-air">Paid Selected</button>
                        @endif
                        @endif
                        <button onclick="loadNewPage('{{ route('installment.index') }}?type={{$type}}')" class="btn btn-success">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <div class="row">
        
                <div class="col-12">
                    <div class="kt-portlet" id="scrollStyle" style="height: 500px; overflow: auto;">
                        <div class="kt-portlet__body" style="width: 1800px;">
                            <h5 class="zn-head-line-success" style="margin-bottom:40px;">Installment Schedule</h5>
                            <div class="row" style="margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                @if ($mi->id_workflow == 9)
                                    <div class="col-1 text-right" style="flex: 0;">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label>
                                    </div>
                                @endif
                                <div class="col-1">
                                    <h6>Action</h6>
                                </div>
                                <div class="col-2">
                                    <h6>Status</h6>
                                </div>
                              
                                <div class="col-1 text-right">
                                    <h6>Date</h6>
                                </div>
                                <div class="col-1 text-right">
                                    <h6>Premium</h6>
                                </div>
                                <div class="col-1 text-right">
                                    <h6>Net Amount </h6>
                                </div>
                                <div class="col-1 text-right">
                                    <h6>Outstanding</h6>
                                </div>
                                <div class="col-1 text-right">
                                    <h6>Commission Due to Us</h6>
                                </div>
                                <div class="col-1 text-right">
                                    <h6>Agent Fee</h6>
                                </div>
                                <div class="col-1 text-right">
                                    <h6>Agent Tax Amount</h6>
                                </div>  
                                <div class="col-1 text-right">
                                    <h6>Net to Underwriter</h6>
                                </div>
                            </div>
                            @foreach ($mc as $v)
                            <div class="row mt-3" style="height:30px;border-bottom: 1px solid #f1f1f1;padding-bottom: 35px;">
                                @if ($mi->id_workflow == 9)
                                <div class="col-1 text-right" style="flex: 0;">
                                        @if ($v->id_workflow == 1)
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid ">
                                            <input type="checkbox" value="{{$v->id}}" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                        @elseif($v->id_workflow == 2 || $v->id_workflow == 14)
                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--warning">
                                            <input disabled type="checkbox">
                                            <span></span>
                                        </label>
                                        @elseif($v->id_workflow == 9)
                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--success">
                                            <input disabled type="checkbox">
                                            <span></span>
                                        </label>
                                        @elseif($v->id_workflow == 12)
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid ">
                                                <input type="checkbox" value="{{$v->id}}" class="kt-group-checkable">
                                                <span></span>
                                            </label>
                                        @endif
                                    </div>
                                @endif
                                <div class="col-1" style="font-size: 14px;">
                                    @php
                                        
                                        $data_memo = \DB::select("select * from master_memo where ref_code = ".$v->id." and ref_code_type = 'master_sales_schedule' order by created_at desc");
       
                                        $list_memo = '';
                                        foreach ($data_memo as $key => $value) {
                                            $list_memo .= " <div class='kt-list-timeline__item'>
                                                <span class='kt-list-timeline__badge kt-list-timeline__badge--success'></span>
                                                <span class='kt-list-timeline__text'>".$value->notes."</span>
                                                <span class='kt-list-timeline__time'>".date("d M Y H:i:s", strtotime($value->created_at))."</span>
                                            </div>";
                                        }

                                        $text = "<div class='kt-list-timeline'>
                                                    <div class='kt-list-timeline__items'>
                                                        ".$list_memo."
                                                    </div>
                                                </div>";

                                        $action = "<button onclick='znIconboxClose()' type='button' class='btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm'>Close</button>";

                                        
                                    @endphp
                                        <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" onclick="detail_invoice_schedule({{$v->id}})">
                                                   <i class="la la-clipboard"></i>
                                                   <span>Detail</span>
                                               </a>
                                               <a class="dropdown-item" onClick="znIconbox(`List Memo`,`{{$text}}`,`{{$action}}`,`memo`);">
                                                   <i class="la la-sticky-note"></i>
                                                   <span>Memo</span>
                                               </a>
                                                </div>
                                            </div>
                                    {{-- <a class="btn btn-success btn-sm btn-elevate btn-elevate-air" style="height: 23px;padding: 2px 10px;" 
                                    href="#" onclick="detail_invoice_schedule({{$v->id}})">
                                    <i class="la la-clipboard"></i> Detail Invoice</a> --}}
                                </div>
                                <div class="col-2">
                                    @if ($v->id_workflow == 2)
                                        <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            Approval Payment
                                        </span>

                                    @elseif($v->id_workflow == 12)
                                    <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Not Paid
                                    </span>
                                    {{-- <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Payment Canceled
                                    </span> --}}

                                    @elseif($v->id_workflow == 14)

                                        <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            Approval Cancel Payment
                                        </span>
                                    @else
                                        @if ($v->paid_status_id == 0)
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            Not Paid
                                        </span>
                                        @elseif($v->paid_status_id == 1)



                                            <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                                                Paid
                                            </span>

                                            @if (Auth::user()->user_role_id == 1)

                                            <button type="button"
                                            onclick="gCancelPay('cancelPayAmor','/CancelPayInstallmentSc?type=satu','{{$v->id}}','{{route('installment.detail',$getId)}}?type=success');"
                                            class="btn btn-success btn-sm btn-elevate btn-elevate-air" style="height: 23px;padding: 2px 10px;">Cancel Payment</button>
                                            @endif
                                            {{-- <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                                Cancel Payment
                                            </span> --}}
                                        @endif

                                    @endif

                                </div>
                               
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{ date('d-m-Y',strtotime($v->date)) }}
                                </div>
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{number_format($v->premi_amount,2,",",".")}}
                                </div>
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{number_format($v->net_amount,2,",",".")}}
                                </div>
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{number_format($v->outstanding,2,",",".")}}
                                </div>
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{number_format($v->comp_fee_amount,2,",",".")}}
                                </div>
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{number_format($v->agent_fee_amount,2,",",".")}}
                                </div>
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{number_format($v->tax_amount,2,",",".")}}
                                </div>
                                <div class="col-1 text-right" style="font-size: 14px;">
                                    {{number_format($v->underwriter_amount,2,",",".")}}
                                </div>

                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script>

$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}


function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


</script>

@include('installment.action_detail_invoice')
@include('transaksi.invoice_modal')
@stop
