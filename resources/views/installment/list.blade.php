@section('content')
<div class="app-content">
<div class="section">


    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Installment </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                            {{$tittle}} </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            {{$tittle}}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            @if ($type == 'new')
                                <a href="#" onclick="gActAll('sendApproval','/kirimAprovalInstallment?type=semua','{{ route('data.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a>
                                <a href="#" onclick="gActSelected('sendApproval','/kirimAprovalInstallment?type=satu','{{ route('data.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a>
                                <a href="#" onclick="gActAll('delete','/hapusAprovalInstallment?type=semua','{{ route('data.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Delete All</a>
                                <a href="#" onclick="gActSelected('delete','/hapusAprovalInstallment?type=satu','{{ route('data.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Delete Selected</a>
                                <button onclick="loadNewPage('{{ route('installment.form','create') }}')" class="btn btn-pill btn-success">Tambah Data</button>
                            @elseif($type == 'approve')
                                <a href="#" onclick="gActAll('giveApproval','/giveAprovalInstallment?type=semua','{{ route('data_approve.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval All</a>
                                <a href="#" onclick="gActSelected('giveApproval','/giveAprovalInstallment?type=satu','{{ route('data_approve.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Selected</a>
                                <a href="#" onclick="gActAll('reject','/rejectAprovalInstallment?type=semua','{{ route('data_approve.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject All</a>
                                <a href="#" onclick="gActSelected('reject','/rejectAprovalInstallment?type=satu','{{ route('data_approve.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Selected</a>
                            @elseif($type == 'success')
                                @if (Auth::user()->user_role_id == 1)
                                    <a href="#" onclick="gActAll('delete','/hapusAprovalInstallmentRev?type=semua','{{ route('data_success.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete All</a>
                                    <a href="#" onclick="gActSelected('delete','/hapusAprovalInstallmentRev?type=satu','{{ route('data_success.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Send Delete Selected</a>
                                @endif
                            @elseif($type == 'approve_reversal')
                                <a href="#" onclick="gActAll('deleteRev','/giveHapusAprovalInstallmentRev?type=semua','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete All</a>
                                <a href="#" onclick="gActSelected('deleteRev','/giveHapusAprovalInstallmentRev?type=satu','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-primary" style="color: white;">Approval Delete Selected</a>
                                <a href="#" onclick="gActAll('reject','/rejectHapusAprovalInstallmentRev?type=semua','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete All</a>
                                <a href="#" onclick="gActSelected('reject','/rejectHapusAprovalInstallmentRev?type=satu','{{ route('data_reversal.installment') }}');" class="btn btn-pill btn-danger" style="color: white;">Reject Delete Selected</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label></th>
                                        <th width="30px">Action</th>
                                        <th>ID</th>
                                        <th>Customer Name</th>
                                        <th>Product Name</th>
                                        <th>Branch Code</th>
                                        <th>Mata Uang</th>
                                        <th>Premi Amount</th>
                                        <th>Paid Status</th>
                                        <th>Nomor Polis</th>
                                        <th>Dokumen</th>
                                        {{-- <th>Keterangan</th> --}}
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('installment.action')
@include('installment.action_detail_invoice')
@include('transaksi.invoice_modal')
@include('invoice.act_detail_product')
@include('invoice.modal_detail_product')

@stop
