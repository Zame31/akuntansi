<script>

$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});


var act_url = '{{ route('data_schedule_complete.installment') }}';


var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { targets: [3,4,5,6,7,8,9], className: 'text-right' },
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'cek', name: 'cek' },
        { data: 'action', name: 'action' },
        { data: 'full_name', name: 'full_name' },
        { data: 'date', name: 'date' },
        { data: 'premi_amount', name: 'premi_amount' },
        { data: 'net_amount', name: 'net_amount' },
        { data: 'outstanding', name: 'outstanding' },
        { data: 'is_agent_paid', name: 'is_agent_paid' },
        { data: 'is_company_paid', name: 'is_company_paid' },
        { data: 'is_premi_paid', name: 'is_premi_paid' }


        // { data: 'is_active', name: 'is_active' }
    ]
});


function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}


function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


</script>
