
@php
    $premi_amount_config = collect(\DB::select("select value as jml from master_config where id = 13"))->first();
    $discount_amount_config = collect(\DB::select("select value as jml from master_config where id = 9"))->first();
    $fee_agent_config = collect(\DB::select("select value as jml from master_config where id = 2"))->first();
    $fee_internal_config = collect(\DB::select("select value  as jml from master_config where id = 3"))->first();
    $tax_amount_config = collect(\DB::select("select value  as jml from master_config where id = 8"))->first();
    
@endphp

<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script>


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});


$('#month').val({{date('m')}});
$('#year').val({{date('Y')}});

var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;

$(document).ready(function () {
    $("#form-valid").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            
            days_overdue: {
                validators: {
                    notEmpty: {
                        message: 'Belum Terisi'
                    },
                    stringLength: {
                            max:2,
                            message: 'Maksimal 2 Digit'
                        }
                }
            },
            tenor: {
                validators: {
                    notEmpty: {
                        message: 'Tenor Belum Terisi'
                    },
                    stringLength: {
                            max:2,
                            message: 'Maksimal 2 Digit'
                        }
                }
            },
            interval: {
                validators: {
                    notEmpty: {
                        message: 'Interval Belum Terisi'
                    },
                    stringLength: {
                            max:2,
                            message: 'Maksimal 2 Digit'
                        }
                }
            },
            start_date: {
                validators: {
                    notEmpty: {
                        message: 'Start Date Installment Belum Terisi'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

var date = new Date();
// $('#start_date').datepicker('setDate', 'today');

$('#start_date').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    setDate: 'today'
});

$('#start_date').change(function () {

    $('#form-valid').data('bootstrapValidator').validate();
    // $('#tgl_akhir1').val('');
    // $('#tgl_akhir1').datepicker('destroy');
    // console.log(this.value);
    // $('#tgl_akhir1').datepicker({
    //     format: 'dd MM yyyy',
    //     autoclose: true,
    //     startDate: this.value
    // });
});


var typeList = '{{$type}}';

if (typeList == 'new') {
    var act_url = '{{ route('data.installment') }}';
}
else if(typeList == 'approve'){
    var act_url = '{{ route('data_approve.installment') }}';
}
else if(typeList == 'success'){
    var act_url = '{{ route('data_success.installment') }}';
}
else if(typeList == 'approve_reversal'){
    var act_url = '{{ route('data_reversal.installment') }}';
}
else if(typeList == 'deleted'){
    var act_url = '{{ route('data_deleted.installment') }}';
}

var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { targets: [5], className: 'text-right' },
        { targets: [6], className: 'text-center' },
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'cek', name: 'cek' },
        { data: 'action', name: 'action' },
        { data: 'id', name: 'id' },
        { data: 'full_name', name: 'full_name' },
        { data: 'definition', name: 'definition' },
        { data: 'short_code', name: 'short_code' },
        { data: 'mata_uang', name: 'mata_uang' },
        { data: 'premi_amount', name: 'premi_amount' },
        { data: 'paid', name: 'paid' },
        { data: 'polis_no', name: 'polis_no' },
        { data: 'url_dokumen', name: 'url_dokumen' }
        // { data: 'invoice_type_id', name: 'invoice_type_id' }

    ]
});

function znClose() {
    znIconboxClose();
    $("#form-valid").data('bootstrapValidator').resetForm();
    $("#form-valid")[0].reset();
    $("#dataAmotisasi").html('');
}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('installment.index') }}?type=new');
}

$(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

function DateAddDay(getdate,interval) {
    var tt = getdate;
    var newinterval = parseInt(interval);

    var date = new Date(tt);
    var newdate = new Date(date);

    newdate.setDate(newdate.getDate() + newinterval);

    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var y = newdate.getFullYear();

    var someFormattedDate = dd + '-' + mm + '-' + y;
    return someFormattedDate;
}

function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}

function generateAmortisasi() {
    var validateProduk = $('#form-valid').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {
        loadingPage();
    $('#dataAmotisasi').html('');

    var tenor = $('#tenor').val();
    var purchase_amount = $('#purchase_amount').val();
    var premi_amount = $('#premi_amount').val();
  

    var month = $('#month').val();
    var year = $('#year').val();
    var interval = $('#interval').val();
    var days_overdue = $('#days_overdue').val();

    var start_date = $('#start_date').datepicker('getDate');

    days_overdue = parseInt(days_overdue);
    interval = parseInt(interval);
    month = parseInt(month);
    year = parseInt(year);

    purchase_amount = purchase_amount.split('.').join("");
    purchase_amount = purchase_amount.split(',').join(".");
    
    // purchase_amount = parseInt(clearNumFormat(purchase_amount));

    
    // premi_amount = clearNumFormat(premi_amount);
    
    premi_amount = premi_amount.split('.').join("");
    premi_amount = premi_amount.split(',').join(".");
    
    tenor = parseInt(tenor);

    var n_premi = premi_amount/tenor;
    n_premi = n_premi.toFixed(2);

    var comm_due = (( '{{$fee_internal_config->jml}}' * premi_amount) /tenor) / 100;
    var fee_agent = (( '{{$fee_agent_config->jml}}' * premi_amount) /tenor) / 100;
    var agent_tax = '{{$tax_amount_config->jml}}' * fee_agent / 100;
    // var underwriter = n_premi - fee_agent - comm_due;


    // var n_premi = premi_amount/tenor;


    // n_premi = n_premi.toFixed(2);
    

    // n_premi = n_premi.toFixed(2).replace(/\./g, ",");


    var n_amor = purchase_amount/tenor;

  
    n_amor = n_amor.toFixed(2);
    var endYear = (tenor/12);
    var outstanding = purchase_amount - n_amor;
    var yearlist = '';

    var newInterval = interval;

    for (let i = 1; i <= tenor; i++) {
        yearlist = '';
        // MONTH YEAR
        var m = [];
        for (let im = 1; im <= 12; im++) {
            m[im] = (month == im) ? "selected" : "";
        }

        var y = [];
        for (let iyy = year; iyy <= year+endYear; iyy++) {
            m[iyy] = (year == iyy) ? "selected" : "";
        }

        for (let iy = 0; iy < endYear; iy++) {
            yearlist += '<option '+m[year+iy]+' value="'+(year+iy)+'">'+(year+iy)+'</option>';
        }

        var new_date = DateAddDay(start_date,0);
        var due_date = DateAddDay(start_date,days_overdue);
        if (i > 1) {
            new_date = DateAddDay(start_date,newInterval);

            due_date = DateAddDay(start_date,(newInterval+days_overdue));
            newInterval += interval;
        }

        var underwriter = n_amor - fee_agent - comm_due;


        

        // MONTH YEAR
        $('#dataAmotisasi').append(`
            <div class="row">
            <div class="col-1">
                <div class="form-group">
                    <input style="text-align:right;" type="text" value="`+new_date+`" class="form-control date-amor" id="dateamor`+i+`" name="dateamor[]">
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <input style="text-align:right;" type="text" value="`+due_date+`" class="form-control date-amor" id="dateamordue`+i+`" name="dateamordue[]">
                </div>
            </div>
            <div class="col-2">
                <input style="text-align:right;" maxlength="14" value="`+formatMoney(n_premi)+`" id="n_premi`+i+`" name="n_premi[]" placeholder="0"
                    class="form-control txt money" onkeyup="reGenerateAmortisasi();" type="text">
            </div>
            <div class="col-2">
                <input style="text-align:right;" value="`+formatMoney(n_amor)+`" id="amor`+i+`" name="amor[]" placeholder="0"
                    class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
            </div>
            <div class="col-2">
                <input readonly style="text-align:right;" value="`+formatMoney(outstanding)+`" id="outstanding`+i+`" name="outstanding[]" placeholder="0"
                    class="form-control txt1" type="text">
            </div>
            <div class="col-1">
                <input style="text-align:right;" value="`+formatMoney(comm_due)+`" id="comm_due`+i+`" name="comm_due[]" placeholder="0"
                    class="form-control txt money" maxlength="14" onkeyup=" reGenerateAmortisasi();" type="text">
            </div>
            <div class="col-1">
                <input style="text-align:right;" value="`+formatMoney(fee_agent)+`" id="fee_agent`+i+`" name="fee_agent[]" placeholder="0"
                    class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
            </div>
            <div class="col-1">
                <input style="text-align:right;" value="`+formatMoney(agent_tax)+`" id="agent_tax`+i+`" name="agent_tax[]" placeholder="0"
                    class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
            </div>
            <div class="col-1">
                <input style="text-align:right;" value="`+formatMoney(underwriter)+`" id="underwriter`+i+`" name="underwriter[]" placeholder="0"
                    class="form-control txt money" maxlength="14" onkeyup="reGenerateAmortisasi();" type="text">
            </div>
            </div>

        `);

        

        

        outstanding -= n_amor;

        month += 1;
        if (month > 12) {
            month = 1;
            year += 1;
        }

        if (i == tenor) {
            endLoadingPage();

            $('.date-amor').datepicker({
                format: 'dd-m-yyyy',
            });

            $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        }
    }

    $('.zn-date').select2({
        placeholder:"Silahkan Pilih"
    });

    }
}

function reGenerateAmortisasi() {


    var newTenor = $('#tenor').val();

    var budget = $('#purchase_amount').val();
    
    budget = budget.split('.').join("");
    budget = budget.split(',').join(".");

    console.log(budget);
        
    
    for (let z = 1; z <= newTenor; z++) {
        if (z == 1) {

            var first_n_amo = $("#amor"+z).val();


            first_n_amo = first_n_amo.split('.').join("");
            first_n_amo = first_n_amo.split(',').join(".");

            console.log(first_n_amo);
            console.log(budget-first_n_amo);
            
            console.log(formatMoney(budget-first_n_amo));
            

            $("#outstanding1").val(formatMoney(budget-first_n_amo));

        }else{

            var out_amo_temp = $("#outstanding"+(z-1)).val();
            var nt_amo_temp = $("#amor"+z).val();

            out_amo_temp = out_amo_temp.split('.').join("");
            out_amo_temp = out_amo_temp.split(',').join(".");

            nt_amo_temp = nt_amo_temp.split('.').join("");
            nt_amo_temp = nt_amo_temp.split(',').join(".");

            out_amo_temp -= nt_amo_temp;
            $("#outstanding"+z).val(formatMoney(out_amo_temp));
        }
    }
}

function saveData() {
    var validateProduk = $('#form-valid').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

        var tenor = $('#tenor').val();
        var interval = $('#interval').val();
        var start_date = $('#start_date').val();
        var days_overdue = $('#days_overdue').val();


        var outstanding = $('#outstanding'+tenor).val();

        outstanding = outstanding.split('.').join("");
        outstanding = outstanding.split(',').join(".");
        console.log(outstanding);
        

        if (outstanding != 0) {
            toastr.warning('Nilai Outstanding harus 0');
        }else {

            var id = $("#id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);
            objData.append('interval', interval);
            objData.append('tenor', tenor);
            objData.append('start_date', start_date);
            objData.append('days_overdue', days_overdue);

            

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('installment.store_sc') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingPage();
                },
                success: function (response) {
                    endLoadingPage();
                    if (response.rc == 0) {
                        znIconbox("Data Berhasil Disimpan",text,action);

                    }else {
                        toastr.warning(response.rm);

                    }
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                endLoadingPage();
                toastr.error("Terjadi Kesalahan");
            });
        }
    } // endif

} // end function

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

</script>
