@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Accounting </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Opex Transaction </a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form Opex Transaction
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="return simpan('single')" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                {{-- <div class="dropdown dropdown-inline">
                    <a href="#" class="btn btn-default btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flaticon-more-1"></i>
                    </a>
                </div> --}}
            </div>
        </div>
        <div class="kt-portlet__body">
            <form id="form-new" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-3">
                        <div class="row">
                            {{--
                            <div class="form-group col-12">
                                <label>Tanggal Transaksi</label>
                                <div class="input-group date">
                                    <input type="hidden" name="type" id="type" value="single">
                                    <input type="text" class="form-control" value="{{date('d M Y')}}" id="tgl_tr" name="tgl_tr" readonly
                                        placeholder="Pilih Tanggal" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                    <div class="invalid-feedback">Silahkan pilih tanggal</div>
                                </div>
                            </div>
                            --}}
                            <div class="form-group  col-12">
                                <label for="exampleTextarea">Catatan</label>
                                <textarea id="catatan" name="catatan" class="form-control" maxlength="80" rows="3"></textarea>
                                <div class="invalid-feedback" id="co">Silahkan isi catatan</div>
                                  <div class="invalid-feedback" id="ck">Maximal 80 karakter</div>
                            </div>
                            <!--
                            <div class="form-group  col-12 form-group-last">
                                <label for="exampleTextarea">Pilih Segment</label>
                                <select class="form-control kt-select2 init-select2" name="idsegment" id="idsegment">
                                    @foreach($segment as $item)
                                    <option value={{$item->id}}>{{$item->definition}}</option>
                                    @endforeach
                                </select>
                            </div>
                        -->
                        </div>

                    </div>
                    <div class="col-9">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-3">
                                        <h5 class="zn-head-line-success mb-4">Pilih Jenis Beban</h5>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Segment Beban</label>
                                            <select class="form-control kt-select2" name="coa_no[]" id="segment_beban">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($segment_beban as $item)
                                                <option value="{{$item->coa_no}}">{{$item->tx_definition}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">Pilih No Akun</div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <h5 class="mb-4">&nbsp;</h5>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Keterangan</label>
                                            <textarea class="form-control" name="keterangan" id="keterangan" rows="1" maxlength="100"></textarea>
                                            <div class="invalid-feedback">Keterangan Tidak Boleh Kosong </div>
                                        </div>
                                    </div>    

                                    
                                    <div class="col-3">
                                        <h5 class="mb-4">&nbsp;</h5>
                                        <div class="form-group">
                                                <label for="exampleTextarea">Amount</label>
                                                <input type="hidden" name="debet[]" value="0">
                                                <input type="text" class="form-control money" id="amount" name="amount" style="text-align: right;">
                                        </div>
                                        <div class="invalid-feedback">Silahkan isi Amount</div>
                                    </div>
                                    <div class="col-3">
                                        <h5 class="mb-4">&nbsp;</h5>
                                            <div class="form-group">
                                                <label for="exampleTextarea">Sumber Pembayaran</label>
                                                <select class="form-control kt-select2 " name="coa_no[]" id="bank">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($bank as $item)
                                                <option value="{{$item->coa_no}}">{{$item->definition}}</option>
                                                @endforeach
                                                </select>
                                                <div class="invalid-feedback">Pilih No Akun</div>
                                            </div>
                                            
                                    </div>

                                </div>


                                {{-- DOKUMEN --}}
                                <div class="row"
                                    style="border-top: 2px solid #f5f6fc;margin-top: 50px;padding-top: 50px;margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                    <div class="col-5">
                                        <h6>Jenis Dokumen</h6>
                                    </div>
                                    <div class="offset-1 col-5">
                                        <h6>Preview Dokumen</h6>
                                    </div>

                                    <div class="col-1">
                                        <button type="button" onclick="add_row_new_dok()"
                                            class="btn btn-success btn-elevate btn-icon"><i
                                                class="la la-plus"></i></button>
                                    </div>
                                </div>
                                <table class="table table-dok" id="tbl_dok">
                                    <tr>
                                        <div class="row mt-2">
                                            <div class="col-5">
                                                <select class="form-control kt-select2 init-select2 dokno" name="jns_dok[]"
                                                    id="jns_dok">
                                                    @foreach($dokumen as $item)
                                                    <option value={{$item->id}}>{{$item->definition}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="offset-1 col-5">
                                                <input type="file" class="form-control" name="file[]" id="file">
                                                <div class="invalid-feedback" id="ss">Silahkan Upload Dokumen</div>
                                                <div class="invalid-feedback" id="ff">Format Dokumen
                                                    pdf,jpeg,jpg,png,xls,xlxs</div>
                                                <div class="invalid-feedback" id="mm">Max Size Dokumen 2 Mb</div>
                                            </div>
                                        </div>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </form>    
            
         <div class="row">
                    <div class="col-12" style="text-align: right;">
                        <button onclick="return simpan('single')" class="btn btn-success">Simpan</button>
                    </div>
                </div>
        </div>
    </div>
   
</div>
<script src="{{asset('js/jquery.maskMoney.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(".money").maskMoney({prefix:'', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
$(document).ready(function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    
    var max="'"+ dd +" "+mm+" "+yyyy+"'";
    console.log(max);
    $('#tgl_tr').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        endDate: max
    });

});

$('#segment_beban').select2({
    placeholder: "Silahkan Pilih"
});
$('#bank').select2({
    placeholder: "Silahkan Pilih"
});

 $('#catatan').keyup(function() {
        var panjang = this.value.length;

        if(panjang==80){
            $('#invoice').addClass( "is-invalid" );
            $('#co').hide();
            $('#ck').show();
        }else{
            $('#invoice').removeClass( "is-invalid" );  
            $('#co').hide();
            $('#ck').hide();   
        }
    });
var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
var action = `  <button onclick="znClose()" type="button"
                    class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                    Data</button>
                <button onclick="znView()" type="button"
                    class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                    Data</button>`;
function znClose() {
    znIconboxClose();
    //$("#form-new").data('bootstrapValidator').resetForm();
    $("#form-new")[0].reset();
    location.reload(); 
    //$("#dataAmotisasi").html('');
}
function znView() {
    znIconboxClose();
    loadNewPage('{{ route('list_tr_baru') }}');
}

var _create_form = $("#form-new");

function simpan() {
    loadingPage();
    /*
    if($('#tgl_tr').val()===''){
        endLoadingPage();
        $( "#tgl_tr" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#tgl_tr").removeClass( "is-invalid" );

    }
    */

    if($('#catatan').val()===''){
        endLoadingPage();
        $( "#catatan" ).addClass( "is-invalid" );
        $('#co').show();
        $('#ck').hide();
        return false;
    }else{
        
        $("#catatan").removeClass( "is-invalid" );

    }

    if($('#segment_beban').val()===''){
        endLoadingPage();
        $( "#segment_beban" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#segment_beban").removeClass( "is-invalid" );

    }

    if($('#keterangan').val()===''){
        endLoadingPage();
        $( "#keterangan" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#keterangan").removeClass( "is-invalid" );

    }

    if($('#amount').val()===''){
        endLoadingPage();
        $( "#amount" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#amount").removeClass( "is-invalid" );

    }

    if($('#bank').val()===''){
        endLoadingPage();
        $( "#bank" ).addClass( "is-invalid" );
        return false;
    }else{
        $("#bank").removeClass( "is-invalid" );

    }


    /*
    let value = [];
            $("#form-new").find("input[type=file]").each(function(index, field){
              value.push(field.value);
              console.log(field.files.length);
            for(var i=0;i<field.files.length;i++) {
                //const file = field.fidetail_les[i];
                const ext=$(field).val().replace(/^.*\./, '')
                console.log(i);
               if(i==0){
                var size=$('#file')[0].files[0].size;
                console.log(size);
                var extension=$('#file').val().replace(/^.*\./, '');
                console.log(extension);
                if(size >= 2000002){
                    endLoadingPage();
                    $( "#file" ).addClass("is-invalid");
                    $('#ss').hide();
                    $('#ff').hide();
                    $('#mm').show();
                    return false;

                }
                //[,'jpeg','jpg','png','xls','xlxs']
                if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
                    endLoadingPage();
                    $( "#file" ).addClass("is-invalid");
                    $('#ss').hide();
                    $('#ff').show();
                    $('#mm').hide();
                    return false;
                }

                $( "#file" ).removeClass( "is-invalid" );

               }else{
               alert('aaa');
                

                }
              }
            });


            if(value == 0){
                endLoadingPage();
                $( "#file" ).addClass("is-invalid");
                $('#ss').show();
                $('#ff').hide();
                $('#mm').hide();
            }
    */        
    var _form_data = new FormData(_create_form[0]);        
    $.ajax({
        type: 'POST',
        url: base_url + '/insert_opex',
        data: _form_data,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (res) {
            //var parse = $.parseJSON(res);
            if(res.rc==1){
                endLoadingPage();
                znIconbox("Data Berhasil Disimpan",text,action);
                /*
                
                swal.fire({
                    title: 'Info',
                    text: "Berhasil",
                    type: 'success',
                    confirmButtonText: 'Tutup',
                    reverseButtons: true
                }).then(function(result){
                    if (result.value) {
                        
                        //_create_form[0].reset();
                        //location.reload();
                    }
                });
                */
            }else{
                swal.fire("Info",obj.rm,"info");
            }

        }
    }).done(function( res ) {
        var obj = JSON.parse(res); 
        console.log(res['data']);
        endLoadingPage();

        if(obj.rc==1){
            znIconbox("Data Berhasil Disimpan",text,action);
        }else{
            swal.fire("Info",obj.rm,"info");
        }    

       }).fail(function(res) {
        endLoadingPage();
        swal.fire("Error","Terjadi Kesalahan!","error");
    });


}

var counter1_ = 1;
var def_ = 'new';
function add_row_new_dok(){
    let value = [];
    $(".dokno > option:selected").each(function() {

    value.push(this.value);
    //Add operations here

    });


    if(def_=='new'){
        var file="#file" + counter1_;

    }else{
        var aa =counter1_ -1;
        var file="#file" + aa;
        var ss="#ss" + aa;
        var ff="#ff" + aa;
        var mm="#mm" + aa;
        var jns_dok="#jns_dok" + aa;

    }
    //document.getElementById("videoUploadFile").files.length == 0


    if($('#file').val()===''){
        $("#file").addClass("is-invalid");
        $('#ss').show();
        $('#ff').hide();
        $('#mm').hide();
        return false;
    }else{
        var size=$('#file')[0].files[0].size;
        console.log(size);
        var extension=$('#file').val().replace(/^.*\./, '');
        console.log(extension);
        if(size >= 2000002){
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').hide();
            $('#mm').show();
            return false;

        }
        //[,'jpeg','jpg','png','xls','xlxs']
        if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
            $( "#file" ).addClass("is-invalid");
            $('#ss').hide();
            $('#ff').show();
            $('#mm').hide();
            return false;
        }

        $( "#file" ).removeClass( "is-invalid" );

    }
    if($(jns_dok).val() ===''){
        $(jns_dok).addClass( "is-invalid" );
        return false;
    }else{
        $( jns_dok ).removeClass( "is-invalid" );

    }
    if($(file).val() ===''){
        $(file).addClass( "is-invalid" );
        $(ss).show();
        $(ff).hide();
        $(mm).hide();
        return false;
    }else{
        if(typeof  $(file).val() === 'undefined'){
            console.log('eweuh');
        }else{
            console.log($(file).val());
            var size=$(file)[0].files[0].size;
            console.log(size);
            var extension=$(file).val().replace(/^.*\./, '');
            console.log(extension);
            if(size >= 2000002){
                $( file ).addClass("is-invalid");
                $(ss).hide();
                $(ff).hide();
                $(mm).show();
                return false;

            }
            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='pdf' && extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='xls' && extension!='xlxs'){
                $( file ).addClass("is-invalid");
                $(ss).hide();
                $(ff).show();
                $(mm).hide();
                return false;
                }
            $( file ).removeClass( "is-invalid" );
        }


    }


    var newRow = $("<tr>");
    var cols = "";
    cols +='<div class="row mt-2">';
    cols += '<div class="col-5"><select class="form-control kt-select2 init-select2 dokno" name="jns_dok[]" id="jns_dok'+counter1_+'"></select><div class="invalid-feedback">Silahkan Pilih Jenis Dokumen</div></div>';
    cols += ' <div class="offset-1 col-5"><input type="file"  class="form-control" name="file[]" id="file'+counter1_+'"><div class="invalid-feedback" id="ss'+counter1_+'">Silahkan Upload Dokumen</div><div class="invalid-feedback" id="ff'+counter1_+'">Format Dokumen pdf,jpeg,jpg,png,xls,xlxs</div><div class="invalid-feedback" id="mm'+counter1_+'">Max Size Dokumen 2 Mb</div></div>';
    cols += '<div class="col-1"><button type="button" class="btn btn-danger btn-elevate btn-icon ibtnDel_"><i class="la la-trash"></i></button></div>';
    cols +='</div>';

    newRow.append(cols);
    $("table.table-dok").append(newRow);
    //$(".table-dok").append(cols);
    var jns_dok="#jns_dok" + counter1_;
    var _items='';
        $.ajax({
            type: 'GET',
            url: base_url + '/ref_dokumen',
            data: {value: value},
            async: false,
            success: function (res) {
                var data = $.parseJSON(res);
                if(data.length!=0){
                    _items='<option value="">Silahkan pilih</option>';
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.id+"'>"+v.definition+"</option>";
                    });
                    console.log(jns_dok);
                    $(jns_dok).html(_items);
                }else{
                    console.log('ga ada data');
                    $(jns_dok).closest("tr").remove();
                }
            }
        });
    $(jns_dok).select2();
    def_='old';
    counter1_ ++;
    console.log('asup');
}




$("table.table-dok").on("click", ".ibtnDel_", function (event) {
        $(this).closest("tr").remove();
        //$( "ph" ).eq( 1 ).removeClass();
       // counter_ -= 1
      //  calculateSum();
    });

</script>
<!-- end:: Content -->
@stop
