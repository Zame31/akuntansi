<!--begin::Modal-->
<div class="modal fade" id="modal_filter_qs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Quotation Slip</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>

            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped- table-hover table-checkable" id="table_filter">
                        <thead>
                            <tr>
                                <th width="30px" class="text-center">Action</th>
                                <th class="text-center">QS No</th>
                                <th class="text-center">Customer Name</th>
                                <th class="text-center">Type of Insurance</th>
                                <th class="text-center">Valuta</th>
                                <th class="text-center">TSI</th>
                                <th class="text-center">Gross Premium</th>
                                <th class="text-center">Premium After Fleet</th>
                                <th class="text-center">Premium Final</th>
                            </tr>
                        </thead>
                        {{-- <tbody id="table_data"> --}}
                            {{-- @if($dataQS)
                                @foreach($dataQS as $i => $item)
                                <tr>
                                    <td align="center">
                                        <button class="btn btn-sm btn-outline-brand btn-pill" onclick="selectQS('{{ $item->qs_no }}')"> Pilih </button>
                                    </td>
                                    <td align="center"> {{ $item->qs_no }} </td>
                                    <td> {{ $item->full_name }} </td>
                                    <td> {{ $item->definition }}</td>
                                    <td align="right"> {{ number_format($item->total_sum_insured,2,'.',',')}} </td>
                                    <td align="right"> {{ number_format($item->gross_premium,2,'.',',')}} </td>
                                    <td align="right"> {{ number_format($item->discount_amount,2,'.',',')}} </td>
                                    <td align="right"> {{ number_format($item->nett_premium,2,'.',',')}} </td>
                                </tr>
                                @endforeach
                            @endif --}}
                        {{-- </tbody> --}}
                    </table>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
