<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>

            header {
                position: fixed;
                top: 0px;
                left: 0px;
                right: 0px;
                height: 100px;
                color: white;
                line-height: 35px;
            }

            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 0.5cm;

                color: white;
                text-align: right;
                line-height: 1.5cm;
            }

            footer .pagenum:before {
                content: counter(page);
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2cm;
                margin-left: 0cm;
                margin-right: 1cm;
                margin-bottom: 1cm;
            }

            * {
                font-family: sans-serif !important;
            }

            table.table-data,
            table.table-data tr,
            table.table-data td,
            table.table-data th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
            }

            table.table-data th {
                text-align: center;
            }

            h1.tittle {
                font-size: 18px;
                text-decoration: underline;
                margin-bottom: 0;
            }

            .container-tittle {
                text-align: center;
            }

            .container-tittle h4 {
                margin-top: 0;
                font-size: 13px;
            }

            .container-table {
                font-size: 11px !important;
            }

            .container-table tr td:last-child {
                text-align: justify;
            }

            .container-table tr td:first-child {
                width: 25%;
            }

            .container-table tr td:nth-child(2) {
                width: 5%;
                text-align: center;
            }

            .container-table tr td:last-child {
                width: 75%;
            }

            .calculate_premium tr td:first-child{
                width: 60%;
            }

            .calculate_premium tr td:last-child{
                width: 25%;
                text-align: right;
            }

            /* .calculate_premium tr td:nth-child(2){
                width: 25%;
                text-align: center;
            } */

            .calculate_premium tr td:nth-child(3){
                width: 40%;
            }

            /* .calculate_premium tr td:nth-child(4){
                width: 5%;
            } */

            table.bottomtable {
                page-break-before: always;
                page-break-after:auto
            }

            table.bottomtable tr {
                page-break-inside:avoid;
                page-break-after:auto
            }

            table.bottomtable tr td {
                page-break-inside:avoid;
                page-break-after:auto
            }

            table.ttd {
                width: 100%;
            }

            table.ttd tr td:first-child,
            table.ttd tr td:last-child{
                width: 50%;
            }

            table.ttd tr td:last-child {
                text-align: right;
            }

            .break-page {
                page-break-before: always !important;
                page-break-after:auto;
            }

            .break-page table {
                font-size: 11px !important;
            }



        </style>
    </head>
    <title> Export PDF Insurance Proposal </title>
    <body>

        <header>
            @php
                $data_imgz = collect(\DB::select("SELECT * from master_company where id = 1"))->first();
            @endphp
            <img src="{{asset('img/'.$data_imgz->image_cetak)}}" width="300">
        </header>

        <footer>

            <script type="text/php">
                if (isset($pdf)) {
                    $text = "{PAGE_NUM}";
                    $size = 10;
                    $font = $fontMetrics->getFont("Verdana");
                    $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                    $x = ($pdf->get_width() - $width);
                    $y = $pdf->get_height() - 35;
                    $pdf->page_text($x, $y, $text, $font, $size);
                }
            </script>
        </footer>

        <main>

            <div class="container-tittle">
                <h1 class="tittle"> PROPOSAL OF INSURANCE </h1>
                <h4> {{ $data->cn_no }}</h4>
            </div>

            <div class="container-table">
                <table>
                    <tr>
                        <td> Source Of Business </td>
                        <td> : </td>
                        <td> {{ $data->business_def }}</td>
                    </tr>
                    <tr>
                        <td> Type of Insurance </td>
                        <td> : </td>
                        <td> {{ $data->definition }}</td>
                    </tr>
                    <tr>
                        <td> Form / Wording </td>
                        <td> : </td>
                        <td> {{ $data->form_wording }}</td>
                    </tr>
                    <tr>
                        <td> The Insured </td>
                        <td> : </td>
                        <td> <b> {{ strtoupper(strtolower($data->full_name)) }} </b> </td>
                    </tr>
                    <tr>
                        <td valign="top"> Address of The Insured </td>
                        <td valign="top"> : </td>
                        <td> {{ $data->address }} </td>
                    </tr>
                    <tr>
                        <td valign="top"> The Business </td>
                        <td valign="top"> : </td>
                        <td> {{ $data->the_business }} </td>
                    </tr>
                    <tr>
                        <td valign="top"> Period of Insurance </td>
                        <td valign="top"> : </td>
                        <td>
                            @if ( is_null($data->from_date) && is_null($data->to_date) )
                               @php
                                   $dt = DB::TABLE('ref_qs_content')
                                               ->where('product_id', $data->product_id)
                                               ->first();
                               @endphp
                                   @if ( !is_null($dt) )
                                       {{ $dt->period_default }}
                                   @endif
                            @else
                               <b> From </b> {{ date('d/m/Y', strtotime($data->from_date)) }} <b> To </b> {{ date('d/m/Y', strtotime($data->to_date)) }}
                            @endif
                       </td>
                    </tr>
                    <tr>
                        <td valign="top"> Interest Insured </td>
                        <td valign="top"> : </td>
                        <td>
                                {{ $data->no_of_insured }}
                                @if ( count($detail) == 1 )
                                    @foreach ( $detail as $item )
                                    <table style="margin-bottom: 15px;">
                                        @foreach ( $item as $value )
                                            <tr>
                                                <td> {{ $value->label }} </td>
                                                <td> : </td>
                                                <td>
                                                    {{ $value->value_text }}
                                                </td>
                                            </tr>
                                            @endforeach
                                    </table>
                                    @endforeach
                                @endif

                        </td>
                    </tr>
                    <tr>
                        <td valign="top"> Valuta ID </td>
                        <td valign="top"> : </td>
                        <td> <b> {{ $data->mata_uang }} </td>
                    </tr>
                    <tr>
                        <td valign="top"> Total Sum Insured </td>
                        <td valign="top"> : </td>
                        <td> <b> {{ $data->mata_uang }} {{ number_format($data->total_sum_insured, 0, ',', '.') }} </b> </td>
                    </tr>
                    <tr>
                        <td valign="top"> Coverage </td>
                        <td valign="top"> : </td>
                        <td style="">
                            {!! $data->coverage !!}

                        </td>
                    </tr>
                    <tr>
                        <td valign="top"> Main Exclusions </td>
                        <td valign="top"> : </td>
                        <td style="">
                            {!! $data->main_exclusions !!}
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"> Deductible </td>
                        <td valign="top"> : </td>
                        <td> {!! $data->deductible !!} </td>
                    </tr>
                    <tr>
                        <td valign="top"> Clauses </td>
                        <td valign="top"> : </td>
                        <td> {!! $data->clauses !!} </td>
                    </tr>
                    <tr>
                        <td valign="top"> Rate </td>
                        <td valign="top"> : </td>
                        {{-- <td> {{ $data->rate }} % + Extension Rate SRCC TS EQ FLOOD {{ $data->add_rate }} = {{ $data->rate + $data->add_rate }} %</td> --}}
                    </tr>
                    <tr>
                        <td valign="top"> Premium Calculation </td>
                        <td valign="top"> : </td>
                        <td>
                            @php
                                $ttl1 = $data->comprehensive_amount + $data->flood_amount;
                                $subTotal = $ttl1 + $data->tpl_limit + $data->pa_driver + $data->pa_passenger;
                            @endphp
                            <br>
                            <table class="calculate_premium" style="width: 100%;">
                                <tr>
                                    <td valign="top">  <b> Premium </b>  </td>
                                    <td> : </td>
                                    <td> {{ number_format($data->comprehensive_amount, 2, ',', '.') }} </td>
                                </tr>
                                <tr>
                                    <td valign="top">  <b> Total Additional Premi  </b>  </td>
                                    <td> : </td>
                                    <td> {{ number_format($data->add_premi, 2, ',', '.') }} </td>
                                </tr>
                                <tr>
                                    <td valign="top">  <b> Gross Premium  </b>  </td>
                                    <td> : </td>
                                    <td> {{ number_format($data->gross_premium, 2, ',', '.') }} </td>
                                </tr>
                                @if ( $data->product_id == 4 )
                                <tr>
                                    <td valign="top">  <b> Discount Fleet  </b>  </td>
                                    <td> : </td>
                                    <td> {{ number_format($data->discount_amount, 2, ',', '.') }} </td>
                                </tr>
                                <tr>
                                    <td valign="top">  <b> Premium After Fleet  </b>  </td>
                                    <td> : </td>
                                    <td> {{ number_format($data->nett_premium, 2, ',', '.') }} </td>
                                </tr>
                                @endif
                                <tr>
                                    <td valign="top">  <b> Additional Discount  </b>  </td>
                                    <td> : </td>
                                    <td> {{ number_format($data->add_disc, 2, ',', '.') }} </td>
                                </tr>
                                <tr>
                                    <td valign="top">  <b> Premium Final </b>  </td>
                                    <td> : </td>
                                    <td> {{ number_format($data->premium_final, 2, ',', '.') }} </td>
                                </tr>
                                {{-- <tr>
                                    <td valign="top"> @if ( $data->product_id == 4 ) <b> Comprehensive </b> @else <b> Premium </b> @endif </td>
                                    <td valign="top" align="center"> : </td>
                                    <td valign="top">
                                        {{ number_format($data->total_sum_insured, 0, '.', ',') }} X {{ $data->rate }}%
                                    </td>
                                    <td valign="top"> : </td>
                                    <td></td>
                                    <td valign="top" style="text-align: right">
                                        {{ number_format($data->comprehensive_amount, 2, '.', ',') }}
                                    </td>
                                </tr>
                                @if ( $data->product_id == 4 )
                                <tr>
                                    <td valign="top"> <b> Flood, EQ,SRCC, TS </b> </td>
                                    <td valign="top" align="center"> : </td>
                                    <td valign="top">
                                        {{ number_format($data->total_sum_insured, 0, '.', ',') }} X {{ $data->add_rate }}%
                                    </td>
                                    <td valign="top"> : </td>
                                    <td></td>
                                    <td valign="top" style="text-align: right">
                                        <span style="text-decoration: underline"> {{ number_format($data->flood_amount, 2, '.', ',') }} </span>
                                    </td>

                                </tr>
                                <tr>
                                    <td>  </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td></td>
                                    <td style="text-align: right">
                                        <b> {{ number_format($ttl1, 2, '.', ',') }} </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <span style="font-weight: bold;"> TPL limit {{ $data->mata_uang }} {{ number_format($data->personal_accident, 0, '.', ',') }} </span>
                                    </td>
                                    <td valign="top" align="center"> : </td>
                                    <td valign="top">
                                        {{ $data->mata_uang }} {{ number_format($data->personal_accident, 0, '.', ',') }} X 1%
                                    </td>
                                    <td valign="top"> : </td>
                                    <td> </td>
                                    <td style="text-align: right" valign="top">
                                        <b> {{ number_format($data->tpl_limit, 2, '.', ',') }} </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top"> PA Driver </td>
                                    <td valign="top"> : </td>
                                    <td valign="top">
                                        {{ $data->mata_uang }} {{ number_format($data->personal_accident, 0, '.', ',') }} X 0.5%
                                    </td>
                                    <td valign="top"> : </td>
                                    <td valign="top"> </td>
                                    <td style="text-align: right" valign="top">
                                        <b> {{ number_format($data->pa_driver, 2, '.', ',') }} </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top"> PA Passenger </td>
                                    <td valign="top" align="left"> : </td>
                                    <td valign="top">
                                        {{ $data->mata_uang }} {{ number_format($data->personal_accident, 0, '.', ',') }} X 0.1% X 4 persons
                                    </td>
                                    <td valign="top"> : </td>
                                    <td valign="top"> </td>
                                    <td style="text-align: right; text-decoration: underline;" valign="top">
                                        <b> {{ number_format($data->pa_passenger, 2, '.', ',') }} </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td style="text-align: right;" valign="top">
                                        <b> {{ number_format($subTotal, 2, '.', ',') }} </b>
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <td valign="top"> <b> Discount {{ $data->discount }}%  </b> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td> </td>
                                    <td style="text-align: right" valign="top">
                                        <b style="text-decoration: underline"> ({{ number_format($data->discount_amount, 2, '.', ',') }}) </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                    <td style="text-align: right;" valign="top">
                                        @php
                                            $grandTotal = $subTotal - $data->discount_amount;
                                        @endphp

                                        <b> {{ number_format($grandTotal, 2, '.', ',') }} </b>
                                    </td>
                                </tr> --}}
                                <tr>
                                    <td valign="top"> <b> Excluding Policy Cost and Stamp Duty </b> </td>
                                    <td valign="top"> </td>
                                    <td valign="top"> </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"> Acquisition Cost </td>
                        <td valign="top"> : </td>
                        <td> {{ $data->acquitition_cost }} % </td>
                    </tr>
                    <tr>
                        <td valign="top"> Security </td>
                        <td valign="top"> : </td>
                        <td> {{ $data->security_note }} </td>
                    </tr>
                    <tr>
                        <td valign="top"> Info Underwriting </td>
                        <td valign="top"> : </td>
                        <td> {{ $data->info_underwriting }} </td>
                    </tr>

                    <tr>
                        <td valign="top"> Security </td>
                        <td valign="top"> : </td>
                        <td> {{ strtoupper(strtolower($data->security)) }} </td>
                    </tr>
                </table>

                <br><br><br>
                <table class="ttd bottomtable">
                    <tr>
                        <td> </td>
                        <td style="text-align: right;"> Jakarta, {{ date('d F Y') }} </td>
                    </tr>
                    <tr>
                        <td> Confirmed by: </td>
                        <td>  </td>
                    </tr>
                    @for( $i = 0; $i < 25; $i++)
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    @endfor
                    <tr>
                        <td> (Authorized Signature) </td>
                        <td> (Authorized Signature) </td>
                    </tr>
                </table>
            </div>

            {{-- @if ( count($detail) > 1 )
            <div class="break-page">
                <div class="container-tittle">
                    <h1 class="tittle"> Lampiran</h1>
                    <h4> Detail Insured </h4>
                </div>

                @foreach ( $detail as $item )
                <table style="margin-bottom: 15px;" class="detail-insured">
                    @foreach ( $item as $value )
                        <tr>
                            <th> {{ $value->label }} </th>
                            <td> : </td>
                            <td>
                                {{ $value->value_text }}
                            </td>
                        </tr>
                        @endforeach
                </table>
                <div style="border-bottom: 1px solid #000; margin-bottom: 10px;"></div>
                @endforeach
            </div>
            @endif --}}

        </main>


    </body>
</html>
