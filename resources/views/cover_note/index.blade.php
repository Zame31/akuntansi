@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Marketing </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Proposal </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Proposal List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <button onclick="loadNewPage('{{ route('cover_note.create') }}');" class="btn btn-pill btn-primary">Add New Proposal </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="table-responsive">
                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                        <thead>
                            <tr>
                                <th width="30px" class="text-center">Action</th>
                                <th class="text-center">No</th>
                                <th class="text-center">ID</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Proposal No</th>
                                <th class="text-center">Change Counter</th>
                                <th class="text-center">Customer Name</th>
                                <th class="text-center">Type of Insurance</th>
                                <th class="text-center">Valuta</th>
                                <th class="text-center">TSI</th>
                                <th class="text-center">Gross Premium</th>
                                <th class="text-center">Premium After Fleet</th>
                                <th class="text-center">Premium Final</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data)
                                @foreach($data as $i => $item)
                                <tr>
                                    <td align="center">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" onclick="loadNewPage('{{ route('cover_note.show', ['id' => $item->id]) }}')">
                                                    <i class="la la-edit"></i> Edit
                                                </a>
                                                @if ( $item->is_active )
                                                    <a class="dropdown-item"  onclick="setCancel('{{ $item->id }}', 't')">
                                                        <i class="la la-close"></i> Batalkan
                                                    </a>
                                                @else
                                                    <a class="dropdown-item"  onclick="setCancel('{{ $item->id }}', 'f')">
                                                        <i class="la la-check"></i> Aktifkan
                                                    </a>
                                                @endif
                                                <a class="dropdown-item" onclick="print('{{ route('cover_note.export', ['id' => $item->id]) }}')">
                                                    <i class="la la-file-pdf-o"></i> Cetak Surat (PDF)
                                                </a>
                                                <a class="dropdown-item text-dark" href="insurance/printword/cn/{{$item->id}}">
                                                    <i class="la la-file-word-o"></i> Cetak Surat (Word)
                                                </a>
                                                <a class="dropdown-item text-dark" href="insurance/printexcel/cn/{{$item->id}}">
                                                    <i class="la la-file-excel-o"></i> Cetak Lampiran (Excel)
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td align="center"> {{ ++$i }} </td>
                                    <td align="center"> {{ $item->id }} </td>
                                    <td align="center" width="150">
                                        @if($item->is_active == 't')
                                            <span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>
                                        @else
                                            <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Tidak Aktif</span>
                                        @endif
                                    </td>
                                    <td align="center"> {{ $item->cn_no }} </td>
                                    <td align="right">
                                        @if ( is_null($item->count_edit) )
                                            0
                                        @else
                                            {{ $item->count_edit }}
                                        @endif
                                    </td>
                                    <td> {{ $item->full_name }} </td>
                                    <td> {{ $item->definition }}</td>
                                    <td> {{ $item->mata_uang }}</td>
                                    <td align="right"> {{ number_format($item->total_sum_insured,2,',','.')}} </td>
                                    <td align="right"> {{ number_format($item->gross_premium,2,',','.')}} </td>
                                    <td align="right"> {{ number_format($item->nett_premium,2,',','.')}} </td>
                                    <td align="right"> {{ number_format($item->premium_final,2,',','.')}} </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
@include('cover_note.action')

<script type="text/javascript">
    // $('#example-select-all').click(function (e) {
    //     $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
    // });
</script>
@stop
