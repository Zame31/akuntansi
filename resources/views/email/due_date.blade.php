<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato', sans-serif;
            text-align: center;
            color: #56565A;
            font-size: 12px;
        }

        .zn-email-bg {
            background: #f7f7f7;
            padding: 30px;
            text-align: center;
            display: inline-block;
            border-radius: 10px;
        }

        .zn-email-tittle {
            font-size: 16px;
            font-weight: bold;
            color: #EB5C28;
        }

        .zn-email-user {
            margin-top: 3px;
            font-size: 14px;
            font-weight: bold;
            color: #565656;
        }

        .zn-email-t {
            margin-top: 20px;
            font-size: 12px;
            color: #adadad;
            padding-bottom: 40px;
            margin-bottom: 40px;
            border-bottom: 2px dashed #eaeaea;
        }

        .zn-email-tt {
            margin-top: 20px;
            font-size: 12px;
            color: #adadad;
        }

        .zn-email-btn {
            text-decoration: none;
            display: inline-block;
            background: #b5c737;
            color: #fff;
            padding: 10px 60px;
            border-radius: 5px;
            margin-top: 10px;
            font-weight: 700;
            text-transform: uppercase;
            font-size: 14px;
        }

        .zn-email-ttt {
            margin-top: 20px;
            font-size: 12px;
            color: #adadad;
        }

        .zn-email-link {
            font-size: 12px;
            display: inline-block;
            background: #EDFFAB;
            padding: 10px;
            margin-top: 20px;
            color: #b5c737;
        }

        .zn-email-bg-in {
            background: #fff;
            padding: 15px;
            border-radius: 10px;
            border-bottom: 1px dashed #eb5c28;
        }

        .zn-email-cp {
            font-size: 12px;
            color: #adadad;
            padding: 20px;
        }

        .zn-product-count {
            color: #5d5c52;
            font-size: 12px;
            font-weight: 100;
            display: block;
            margin-top: 10px;
            text-align: left;
        }

        .zn-text-green {
            color: #c89c7d !important;
        }

        .zn-product-tittle {
            color: #696969;
            font-size: 12px;
            font-weight: 600;
            display: block;
            text-align: left;
        }

        .row {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
            padding-left: 30px;
        }

        .col-12 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .mb-2,
        .my-2 {
            margin-bottom: .5rem !important;
        }

        .col-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }

        .col-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
        }

        .mb-4,
        .my-4 {
            margin-bottom: 1.5rem !important;
        }

        .col-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .text-right {
            text-align: right !important;
        }

        .mt-4,
        .my-4 {
            margin-top: 1.5rem !important;
        }

        .col-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
        
.col-5 {
  -webkit-box-flex: 0;
  -ms-flex: 0 0 41.66667%;
  flex: 0 0 41.66667%;
  max-width: 41.66667%; }

        .mb-4,
        .my-4 {
            margin-bottom: 1.5rem !important;
        }

    </style>
</head>

<body>
    <div class="zn-email-bg">
        <div class="zn-email-bg-in">
            <img alt="Logo" src="http://dev-ata-hdic.basys.co.id/img/hdlogo.png" style="width: 40px;">
            <div class="zn-email-tittle">
               {{$company->app_name}}
            </div>
            <br>
            <div class="zn-email-user mb-2">Laporan Renewal List Due Date</div>
            
            <div class="row">
                <div class="col-12" style="margin-right:40px;">
                    <div class="mb-4">
                        <div style="color:#5d5d5d;">Terdapat polis yang akan jatuh tempo dalam tempo {{$company->due_date}} hari ke depan sebanyak {{$count_data_renewal}} record.</div>
                       <div style="color:#5d5d5d;">Silahkan login ke halaman web <a style="color: #EB5C28;text-decoration: none;"
                        href="{{$company->link_web}}"> {{$company->link_web}} </a>untuk melihat data renewal.</div>
                    </div>
                    
                </div>
            </div>

        </div>
        {{-- <div class="zn-email-cp">Copyrights &copy; 2020 <a style="color: #c89c7d;text-decoration: none;"
                href="#">ata-hd.basys.co.id</a>. All Rights Reserved.</div> --}}
    </div>
</body>


</html>
