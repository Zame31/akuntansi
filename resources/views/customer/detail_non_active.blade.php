@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Customer </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Customer Profile </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">


            <div class="row">
                <div class="col-3">

                    <div class="kt-portlet kt-portlet--height-fluid-">
                        <div class="kt-portlet__head  kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">

                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit-y">

                            <!--begin::Widget -->
                            <div class="kt-widget kt-widget--user-profile-1">
                                <div class="kt-widget__head">
                                    <div class="kt-widget__media">
                                        <img src="{{asset('upload_customer')}}/{{$data[0]->id}}_{{$data[0]->image_url}}" alt="image">
                                    </div>
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__section">
                                            <a href="#" class="kt-widget__username">
                                                {{$data[0]->full_name}}
                                                <i class="flaticon2-correct kt-font-success"></i>
                                            </a>
                                            <span class="kt-widget__subtitle">
                                                {{$data[0]->cust_type}}
                                            </span>
                                        </div>
                                        <div class="kt-widget__action">
                                            <button type="button" class="btn btn-info btn-sm">{{$data[0]->definition}}</button>

                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__body">
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Short Name:</span>
                                            <span class="kt-widget__data">{{$data[0]->short_name}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Email:</span>
                                            <span class="kt-widget__data">{{$data[0]->email}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Phone:</span>
                                            <span class="kt-widget__data">{{$data[0]->phone_no}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">City:</span>
                                            <span class="kt-widget__data">{{$data[0]->city_name}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Province:</span>
                                            <span class="kt-widget__data">{{$data[0]->province_name}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label"
                                                style="vertical-align: top;height: 100px;">Address:</span>
                                            <span class="kt-widget__data"
                                                style="vertical-align: top;height: 100px;text-align: right;">{{$data[0]->address}}</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__items">
                                        <div class="row">
                                            <div class="col-12 ">
                                                <button onclick="editProfile()" type="button"
                                                    class="btn btn-info btn-elevate btn-pill" style="width: 100%;">
                                                    {{-- <i class="la la-edit"></i> --}}
                                                    Edit Profile</button>
                                            </div>
                                            <div class="col-12 mt-2 text-left">
                                                    <button onclick="loadNewPage('{{ route('customer_list_non_active') }}')" type="button"
                                                        class="btn btn-danger btn-elevate btn-pill" style="width: 100%;">
                                                        {{-- <i class="la la-angle-double-left"></i>  --}}
                                                        Kembali</button>
                                                </div>
                                        </div>
                                        {{-- <a href="#" class="kt-widget__item kt-widget__item--active"
                                            style="padding: 10px 20px;">
                                            <span class="kt-widget__section">
                                                <i class="la la-edit" style="font-size: 24px;"></i>
                                                <span class="kt-widget__desc">
                                                    Edit Profile
                                                </span>
                                            </span>
                                        </a> --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-9">
                    {{-- INSURANCE --}}
                    <div class="kt-portlet" id="insurance_view">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="flaticon-grid-menu"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    Insurance Facility
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th width="30px">Action</th>
                                        <th>Insurance Type</th>
                                        <th>Nett Premi</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($ins)
                                    @php
                                    $no=0;
                                    @endphp
                                    @foreach($ins as $item)
                                    @php
                                    $no++;
                                    @endphp
                                    
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>
                                            <button onclick="showDetail({{$item->id}})" type="button"
                                                class="btn btn-info btn-icon"><i
                                                    class="fa fa-clipboard-list"></i></button>
                                        </td>
                                        <td>{{$item->definition}}</td>
                                        <td>{{number_format($item->premi_amount,0,',','.')}}</td>
                                        <td>{{date('d M Y',strtotime($item->start_date))}}</td>
                                        <td>{{date('d M Y',strtotime($item->end_date))}}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    


                                </tbody>

                            </table>
                        </div>
                    </div>

                    {{-- DETAIL INSURANCE --}}
                    <div class="kt-portlet" id="detail_insurance">
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-invoice-1">
                    <div class="kt-invoice__head" style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                                <div class="kt-invoice__container">
                                    <div class="kt-invoice__brand">
                                        <h1 class="kt-invoice__title">INVOICE</h1>
                                        <div href="#" class="kt-invoice__logo">
                                            <button onclick="kembali();" type="button"
                                                class="btn btn-light btn-hover-info btn-pill">Kembali</button>
                                            {{-- <a href="#" onclick="kembali();" class="btn btn-pill btn-primary">Kembali</a> --}}
                                        </div>
                                    </div>
                                    <div class="kt-invoice__items">
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">INVOICE DATE.</span>
                                            <span class="kt-invoice__text" id="tgl_tr"></span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                            <span class="kt-invoice__text" id="inv_no"></span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">PRODUCT NAME.</span>
                                            <span class="kt-invoice__text" id="product"></span>
                                        </div>
                                    </div>
                                    <div class="kt-invoice__items">
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">OFFICER NAME.</span>
                                            <span class="kt-invoice__text" id="agent"></span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">START DATE COVERAGE.</span>
                                            <span class="kt-invoice__text" id="start_date"></span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">END DATE COVERAGE.</span>
                                            <span class="kt-invoice__text" id="end_date"></span>
                                        </div>
                                    </div>
                                    <div class="kt-invoice__items zn-desc-invoice">
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">QUOTATION.</span>
                                            <span class="kt-invoice__text" id="quotation"></span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">PROPOSAL.</span>
                                            <span class="kt-invoice__text" id="proposal"></span>
                                        </div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__subtitle">SEGMENT.</span>
                                            <span class="kt-invoice__text" id="segment"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-invoice__body">
                                <div class="kt-invoice__container">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>DESCRIPTION</th>
                                                    <th>AMOUNT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Insurance</td>
                                                    <td><div id="ins_amount"></div></td>
                                                </tr>
                                                <tr>
                                                    <td>Disc</td>
                                                    <td><div id="disc_amount"></div></td>
                                                </tr>
                                                <tr>
                                                    <td>Nett</td>
                                                    <td><div id="net_amount"></div></td>
                                                </tr>
                                                <tr>
                                                    <td>Fee Agent</td>
                                                    <td><div id="agent_fee_amount"></div></td>
                                                </tr>
                                                <tr>
                                                    <td>Fee Internal</td>
                                                    <td><div id="comp_fee_amount"></div></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-invoice__footer">
                                <div class="kt-invoice__container">
                                    <div class="kt-invoice__bank">
                                        <div class="kt-invoice__title">BANK TRANSFER</div>
                                        <div class="kt-invoice__item">
                                            <span class="kt-invoice__label">Account Name:</span>
                                            <span class="kt-invoice__value" id="bank"></span></span>
                                        </div>
                                    </div>
                                    <div class="kt-invoice__total">
                                        <span class="kt-invoice__title">PREMI AMOUNT</span>
                                        <span class="kt-invoice__price" id="premi_amount"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>

                    {{-- EDIT CUSTOMER --}}

                    <div class="kt-portlet" id="edit-profile">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="flaticon-grid-menu"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    Edit Profile
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-success" onclick="return update_customer()">Simpan</button>
                                        <button onclick="batalProfile()" class="btn btn-danger">Batal</button>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <form id="form-edit-customer" enctype="multipart/form-data">
                                <input type="hidden" name="id" id="idcust" value="{{$upd[0]->id}}">
                                <div class="row">

                                    <div class="col-2">
                                        <div class="form-group">
                                            <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                                <div class="kt-avatar__holder" style="width: 160px;
                                                height: 160px;background-size: 160px 160px;background-image: url(../upload_customer/{{$upd[0]->id}}_{{$upd[0]->image_url}})"></div>
                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Change Foto">
                                                    <i class="fa fa-pen"></i>
                                                    <input type="file" name="img" id="img">
                                                </label>
                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Cancel Foto">
                                                    <i class="fa fa-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1"></div>
                                    <div class=" col-3">
                                        <div class="form-group">
                                            <label for="exampleTextarea">Full Name</label>
                                            <input type="text" class="form-control" onkeypress="return huruf(event)" id="full_name" name="full_name" maxlength="100" value="{{$upd[0]->full_name}}">
                                            <div class="invalid-feedback" id="mm">Silahkan isi full name</div>
                                            <div class="invalid-feedback" id="ck">Maximal 100 karakter</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Short Name</label>
                                            <input type="text" class="form-control" id="short_name" value="{{$upd[0]->short_name}}" onkeypress="return huruf(event)" name="short_name" maxlength="50">
                                            <div class="invalid-feedback" id="mm">Silahkan isi short name</div>
                                            <div class="invalid-feedback" id="ck1">Maximal 50 karakter</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Email</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">
                                                    <i class="flaticon2-email"></i></span>
                                                </div>
                                                <input type="text" class="form-control" name="email" id="email" value="{{$upd[0]->email}}" maxlength="100">
                                                 <div class="invalid-feedback" id="ks">Silahkan isi email</div>
                                                <div class="invalid-feedback" id="vv">Email tidak valid</div>
                                                <div class="invalid-feedback" id="vv1">Email sudah digunakan</div>
                                            </div>
                                            <span class="form-text text-muted">ex : example@email.com</span>
                                        </div>

                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">
                                                    <i class="la la-phone"></i></span>
                                                </div>
                                                <input type="text" class="form-control" id="phone" onkeypress="return hanyaAngka(event)" maxlength="15" name="phone" value="{{$upd[0]->phone_no}}">
                                                <div class="invalid-feedback" id="mm">Silahkan isi phone number</div>
                                            </div>
                                            <span class="form-text text-muted">ex : 085xxxxxxxxxx</span>
                                        </div>



                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="exampleTextarea">City</label>
                                            <select class="form-control kt-select2" name="city" id="city">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($city as $item1)
                                                <option value="{{$item1->city_code}}" @if($upd[0]->city_id==$item1->city_code) selected @endif>{{$item1->city_name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback" id="mpp">Silahkan pilih city</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Province</label>
                                            <select class="form-control kt-select2" name="province" id="province">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($province as $item)
                                                <option value="{{$item->province_code}}" @if($upd[0]->province_id==$item->province_code) selected @endif>{{$item->province_name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback" id="mp1">Silahkan pilih province</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Address</label>
                                            <textarea class="form-control" maxlength="200" id="alamat" rows="6" style="height: 150px;" name="alamat">{{$upd[0]->address}}</textarea>
                                            <div class="invalid-feedback" id="mm2">Silahkan isi address</div>
                                            <div class="invalid-feedback" id="mm3">Maximal 200 karakter</div>
                                        </div>

                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="exampleTextarea">Customer Type</label>
                                            <select class="form-control kt-select2 init-select2" name="cust_type" id="cust_type">
                                                @foreach($cust_type as $item)
                                                <option value="{{$item->id}}" @if($upd[0]->cust_type_id==$item->id) selected @endif>{{$item->definition}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Account Officer</label>
                                            <select class="form-control kt-select2 init-select2" name="agent" id="agent">
                                                @php
                                                $agents = \DB::select('SELECT * FROM ref_agent where is_active=true AND is_parent=true order by seq asc');
                                            @endphp
                                              @foreach($agents as $item)
                                              <option value="{{$item->id}}" @if($data[0]->agent_id==$item->id) selected @endif>{{$item->full_name}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Customer Group</label>
                                            <select class="form-control kt-select2 init-select2" name="cust_group" id="cust_group">
                                                <option value="">Silahkan Pilih</option>
                                                @php
                                                     $cust = \DB::select('SELECT * FROM ref_cust_group');
                                                @endphp
                                                @foreach($cust as $item)
                                                <option value="{{$item->id}}" @if($data[0]->cust_group_id==$item->id) selected @endif>{{$item->group_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#full_name').keyup(function() {
        var panjang = this.value.length;

        if(panjang==100){
            $('#full_name').addClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').show();
        }else{
            $('#full_name').removeClass( "is-invalid" );  
            $('#mm').hide();
            $('#ck').hide();   
        }
    });
    
    $('#alamat').keyup(function() {
        var panjang = this.value.length;

        if(panjang==200){
            $('#alamat').addClass( "is-invalid" );
            $('#mm2').hide();
            $('#mm3').show();
        }else{
            $('#alamat').removeClass( "is-invalid" );  
            $('#mm2').hide();
            $('#mm3').hide();   
        }
    });


    $('#short_name').keyup(function() {
        var panjang = this.value.length;

        if(panjang==50){
            $('#short_name').addClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').show();
        }else{
            $('#short_name').removeClass( "is-invalid" );  
            $('#mm1').hide();
            $('#ck1').hide();   
        }
    });
     $('#city').select2({
        placeholder:"Silahkan Pilih"
    });
    $('#province').select2({
        placeholder:"Silahkan Pilih"
    });
    var _create_form = $("#form-edit-customer");
    $('#city').on('change', function (v) {
        var _items='';
        loadingPage();
        $.ajax({
                type: 'GET',
                url: base_url + 'province/'+this.value,
                success: function (res) {
                var data = $.parseJSON(res);
                $.each(data, function (k,v) {
                    _items += "<option value='"+v.province_code+"'>"+v.province_name+"</option>";
                });

                $('#province').html(_items);
                endLoadingPage();
             }
        }); 
    });
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
     function update_customer(){
        loadingPage();
        if($('#full_name').val()===''){
            endLoadingPage();
            $( "#full_name" ).addClass( "is-invalid" );
            $('#mm').show();
            $('#ck').hide();
            return false;
        }else{
            $("#full_name").removeClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').hide();
        }
        if($('#short_name').val()===''){
            endLoadingPage();
            $( "#short_name" ).addClass( "is-invalid" );
            $('#mm1').show();
            $('#ck1').hide();
            return false;
        }else{
            $("#short_name").removeClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').hide();
        }
        if($('#short_name').val()===''){
            endLoadingPage();
            $( "#short_name" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#short_name").removeClass( "is-invalid" );

        }
        if($('#email').val()===''){
            endLoadingPage();
            $( "#email" ).addClass( "is-invalid" );
            $('#vv').hide();
            $('#vv1').hide();
            $('#ks').show();
            return false;
        }else{
            var email=$('#email').val();
            if (validateEmail(email)) {
               $("#email").removeClass( "is-invalid" ); 
              } else {
                endLoadingPage();
                $( "#email" ).addClass( "is-invalid" );
                $('#vv').show();
                $('#ks').hide();
                $('#vv1').hide();
                return false;
              }
        }
        if($('#phone').val()===''){
            endLoadingPage();
            $( "#phone" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#phone").removeClass( "is-invalid" );

        }
        if($('#city').val()===''){
            endLoadingPage();
            $("#city").addClass( "is-invalid" );
            $('#mpp').show();
            return false;
        }else{
            $("#city").removeClass( "is-invalid" );

        }

        if($('#province').val()===''){
            endLoadingPage();
            $("#province").addClass( "is-invalid" );
            $('#mp1').show();
            return false;
        }else{
            $("#province").removeClass( "is-invalid" );

        }

        if($('#alamat').val()===''){
            endLoadingPage();
            $( "#alamat" ).addClass( "is-invalid" );
            $('#mm2').show();
            $('#mm3').hide();
            return false;
        }else{
            $('#mm2').hide();
            $('#mm3').hide();
            $("#alamat").removeClass( "is-invalid" );

        }
        if($('#img').val()===''){
        }else{
            var size=$('#img')[0].files[0].size;
            var extension=$('#img').val().replace(/^.*\./, '');
            if(size >= 2000002){
                endLoadingPage();
                swal.fire('info','Maximal size upload 2Mb','info');
                return false;   

            }
            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='PNG' && extension!='JPG' && extension!='JPEG'){
                endLoadingPage();
                swal.fire('info','Format upload jpeg , jpg , png','info');
                return false;   
            }
        }


         $.ajax({
                type: 'GET',
                url: base_url + 'cek_email?id='+$('#email').val()+'&idcust='+$('#idcust').val(),
                success: function (res) {
                var data = $.parseJSON(res);
                
                if(data!='null' && data!=null && data!=''){
                    endLoadingPage();
                    $( "#email" ).addClass( "is-invalid" );
                    $('#vv1').show();
                    $('#vv').hide();
                    $('#ks').hide();
                    return false;

                }else{
                    $("#email").removeClass( "is-invalid" );
                    $('#vv1').hide();
                    $('#vv').hide();
                    $('#ks').hide();

                    var _form_data = new FormData(_create_form[0]);
                    $.ajax({
                        type: 'POST',
                        url: base_url + 'update_customer_baru',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            //if(res.rc==1){
                                endLoadingPage();
                                _create_form[0].reset();
                                
                                swal.fire({
                                    title: 'Info',
                                    text: "Customer Berhasil Diupdate",
                                    type: 'info',
                                    confirmButtonText: 'Tutup',
                                    reverseButtons: true
                                }).then(function(result){
                                    if (result.value) {
                                        //window.location.href = base_url +'customer_list';
                                        location.reload();
                                    }
                                });
                            //}
                        }
                    });


                } 

                
             }
        }); 

    }
    $('#detail_insurance').hide();
    $('#edit-profile').hide();

    function editProfile() {
        animateCSS('#insurance_view', 'fadeOutDown', function () {
            $('#insurance_view').hide();
            $('#edit-profile').show();
        });

        animateCSS('#edit-profile', 'fadeInDown', function () {});
    }

    function batalProfile() {
        animateCSS('#edit-profile', 'fadeOutDown', function () {
            $('#edit-profile').hide();
            $('#insurance_view').show();
        });

        animateCSS('#insurance_view', 'fadeInDown', function () {});
    }

    function kembali() {
        animateCSS('#detail_insurance', 'fadeOutDown', function () {
            $('#detail_insurance').hide();
            $('#insurance_view').show();
        });

        animateCSS('#insurance_view', 'fadeInDown', function () {});
    }

    function showDetail(id) {
        
        animateCSS('#insurance_view', 'fadeOutDown', function () {
            
            loadingPage();
            $.ajax({
                type: 'GET',
                url: base_url + 'detail_invoice?id='+id,
                success: function (res) {
                    var data = $.parseJSON(res);
                    console.log(data);
                    endLoadingPage();
                    $('#tgl_tr').html(formatDate(new Date(data[0].inv_date)));
                    $('#inv_no').html(data[0].inv_no);
                    $('#product').html(data[0].produk);
                    $('#agent').html(data[0].agent);
                    $('#start_date').html(formatDate(new Date(data[0].start_date)));
                    $('#end_date').html(formatDate(new Date(data[0].end_date)));
                    $('#quotation').html(data[0].quotation);
                    $('#proposal').html(data[0].proposal);
                    $('#segment').html(data[0].segment);

                    $('#ins_amount').html(data[0].ins_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                    $('#disc_amount').html(data[0].disc_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                    $('#net_amount').html(data[0].net_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                    $('#agent_fee_amount').html(data[0].agent_fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));    
                    $('#comp_fee_amount').html(data[0].comp_fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                    $('#premi_amount').html(data[0].premi_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

                    $('#bank').html(data[0].bank);

                    //$('#detail').modal('show');
                    $('#insurance_view').hide();
                    $('#detail_insurance').show();
                }
            });

            
        });

        animateCSS('#detail_insurance', 'fadeInDown', function () {});

    }

    function formatDate(date) {
      var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
      ];

      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();

      return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }


</script>
@stop
