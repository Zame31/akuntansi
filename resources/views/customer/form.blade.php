@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Customer </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    New Customer</a>
            </div>
        </div>
    </div>
</div>

<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon-grid-menu"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Form New Client
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                        <button onclick="return simpan_customer()" class="btn btn-success">Simpan Client</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form id="form-new-customer" enctype="multipart/form-data">
            <div class="row">

                <div class="col-2">
                    <div class="form-group">
                        <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                <div class="kt-avatar__holder" style="width: 160px;
                                height: 160px;background-size: 160px 160px;background-image: url(./assets/media/users/default.jpg)"></div>
                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Change Foto">
                                    <i class="fa fa-pen"></i>
                                    <input type="file" name="img" id="img">
                                </label>
                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Cancel Foto">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                    </div>


                </div>

                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleTextarea">Full Name</label>
                            <input type="text" class="form-control" onkeypress="return huruf(event)" id="full_name" name="full_name" maxlength="100">
                            <div class="invalid-feedback" id="mm">Silahkan isi full name</div>
                            <div class="invalid-feedback" id="ck">Maximal 100 karakter</div>
                        </div>

                        <div class="form-group">
                            <label for="exampleTextarea">Client Code</label>
                            <input type="text" class="form-control" id="short_name" onkeypress="return huruf(event)" name="short_name" maxlength="50">
                            <div class="invalid-feedback" id="mm1">Silahkan isi client code</div>
                            <div class="invalid-feedback" id="ck1">Maximal 50 karakter</div>
                        </div>

                        <div class="form-group">
                            <label for="exampleTextarea">Email</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">
                                    <i class="flaticon2-email"></i></span>
                                </div>
                                <input type="text" class="form-control" name="email" id="email" maxlength="100">
                                <div class="invalid-feedback" id="ks">Silahkan isi email</div>
                                <div class="invalid-feedback" id="vv">Email tidak valid</div>
                                <div class="invalid-feedback" id="vv1">Email sudah digunakan</div>
                            </div>
                            <span class="form-text text-muted">ex : example@email.com</span>
                        </div>

                        <div class="form-group">
                            <label>Phone Number</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">
                                    <i class="la la-phone"></i></span>
                                </div>
                                {{-- <input type="text" class="form-control" id="phone" onkeypress="return hanyaAngka(event)" maxlength="15" name="phone"> --}}
                                <textarea class="form-control" id="phone" maxlength="200" rows="2" name="phone" onkeypress="return hanyaAngka(event)"></textarea>
                                <div class="invalid-feedback" id="mm">Silahkan isi phone number</div>
                            </div>
                            <span class="form-text text-muted">ex : 085xxxxxxxxxx</span>
                        </div>

                        <div class="form-group">
                            <label>Home Phone Number</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">
                                    <i class="la la-home"></i></span>
                                </div>
                                {{-- <input type="text" class="form-control" id="home_phone_number" onkeypress="return hanyaAngka(event)" maxlength="15" name="home_phone_number"> --}}
                                <textarea class="form-control" id="home_phone_number" maxlength="200" rows="2" name="home_phone_number" onkeypress="return hanyaAngka(event)"></textarea>
                                <div class="invalid-feedback" id="mm">Silahkan isi home phone number</div>
                            </div>
                            <span class="form-text text-muted">ex : 022xxxxxxxxxx</span>
                        </div>
                    </div>

                <div class="col-3">
                        <div class="form-group">
                                <label for="exampleTextarea">City</label>
                                <select class="form-control kt-select2" name="city" id="city">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach($city as $item)
                                    <option value="{{$item->city_code}}">{{$item->city_name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback" id="mpp">Silahkan pilih city</div>
                            </div>
                            <div class="form-group">
                                <label for="exampleTextarea">Province</label>
                                <select class="form-control kt-select2" name="province" id="province">
                                    <option value="">Silahkan Pilih</option>
                                </select>
                                <div class="invalid-feedback" id="mp1">Silahkan pilih province</div>
                            </div>
                            <div class="form-group">
                                    <label for="exampleTextarea">Address</label>
                                    <textarea class="form-control" id="alamat" maxlength="200" rows="8" name="alamat"></textarea>
                                    <div class="invalid-feedback" id="mm2">Silahkan isi address</div>
                                    <div class="invalid-feedback" id="mm3">Maximal 200 karakter</div>
                                </div>

                                <div class="form-group">
                                    <label>Fax Number</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">
                                            <i class="la la-fax"></i></span>
                                        </div>
                                        {{-- <input type="text" class="form-control" id="fax_number" onkeypress="return hanyaAngka(event)" maxlength="15" name="fax_number"> --}}
                                        <textarea class="form-control" id="fax_number" maxlength="200" rows="2" name="fax_number" onkeypress="return hanyaAngka(event)"></textarea>
                                        <div class="invalid-feedback" id="mm">Silahkan isi fax number</div>
                                    </div>
                                    <span class="form-text text-muted">ex : 022xxxxxxxxxx</span>
                                </div>

                </div>

                <div class="col-3">
                        <div class="form-group">
                                <label for="exampleTextarea">Customer Type</label>
                                <select class="form-control kt-select2 init-select2" name="cust_type" id="cust_type">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach($cust_type as $item)
                                    <option value="{{$item->id}}">{{$item->definition}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback" id="ct">Silahkan pilih customer type</div>
                            </div>

                            <div class="form-group">
                                <label for="exampleTextarea">Account Officer</label>
                                <div id="container-officer" style="display: flex;">
                                    <select class="form-control kt-select2 init-select2" name="agent" id="agent">
                                        <option value="">Silahkan Pilih</option>
                                        @foreach($agent as $item)
                                        <option value="{{$item->id}}">{{$item->full_name}}</option>
                                        @endforeach
                                    </select>
                                    <button type="button" onclick="modal_add_officer()" style="margin-left: 5px;"
                                        class="btn btn-success btn-elevate btn-icon">
                                        <i class="la la-plus"></i>
                                    </button>
                                </div>
                                <div class="invalid-feedback" id="agt">Silahkan pilih officer</div>
                            </div>

                            <div class="form-group">
                                <label for="exampleTextarea">Customer Group</label>
                                <select class="form-control kt-select2 init-select2" name="cust_group" id="cust_group">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach($cust as $item)
                                    <option value="{{$item->id}}">{{$item->group_name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback" id="cg">Silahkan pilih customer group</div>
                            </div>

                            <div class="form-group">
                                <label for="exampleTextarea">Source Type</label>
                                <select class="form-control kt-select2 init-select2" name="source_type" id="source_type" onchange="append_input_agent(this)">
                                    <option value="">Silahkan Pilih</option>
                                    <option value="1"> Agent </option>
                                    <option value="2"> Non Agent </option>
                                </select>
                                <div class="invalid-feedback" id="ctype">Silahkan pilih source type</div>
                            </div>

                            <div class="form-group" id="container-agent" style="display: none;">
                                <label for="exampleTextarea">Select Agent </label>
                                <div id="container-agent" style="display: flex;">
                                    <select class="form-control kt-select2 init-select2" name="select_agent" id="select_agent">
                                        <option value="">Silahkan Pilih</option>
                                        @foreach($selectAgent as $item)
                                            <option value="{{$item->id}}">{{$item->full_name}}</option>
                                        @endforeach
                                    </select>

                                    <button type="button" id="btn_add_agent" onclick="modal_add_agent()"
                                        class="btn btn-success btn-elevate btn-icon d-none" style="margin-left: 5px;">
                                        <i class="la la-plus"></i>
                                    </button>
                                </div>
                                <div class="invalid-feedback" id="slagent">Silahkan pilih agent </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleTextarea">Have a Branch</label>
                                <select class="form-control kt-select2 init-select2" name="is_branch" id="is_branch" onchange="append_cust_branch(this)">
                                    <option value="">Silahkan Pilih</option>
                                    <option value="1"> Yes </option>
                                    <option value="2"> Non </option>
                                </select>
                                <div class="invalid-feedback" id="cbranch">Silahkan pilih pilihan</div>
                            </div>

                            <div class="form-group" id="container-cust-branch" style="display: none;">
                                <label for="exampleTextarea">Select Customer Branch </label>
                                <div id="container-agent" style="display: flex;">
                                    <select class="form-control kt-select2 init-select2" name="cust_branch_id" id="cust_branch_id">
                                        <option value="">Silahkan Pilih</option>
                                        @foreach($custBranch as $item)
                                            <option value="{{$item->id}}">{{$item->branch_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="invalid-feedback" id="custbranch">Silahkan pilih customer branch </div>
                            </div>
                </div>


            </div>
        </form>
        </div>
    </div>
</div>

@include('customer.modal_add_officer')
@include('customer.modal_add_agent')
<script type="text/javascript">

    function append_input_agent(elm) {
        var value = $(elm).val();
        if ( value == '1' ) {
            // Agent
            $("#container-agent").slideDown();
            $("#btn_add_agent").removeClass('d-none');
        } else {
            $("#container-agent").slideUp();
            $("#btn_add_agent").addClass('d-none');
        }
    }

    function append_cust_branch(elm) {
        var value = $(elm).val();
        if ( value == '1' ) {
            // Agent
            $("#container-cust-branch").slideDown();
        } else {
            $("#container-cust-branch").slideUp();
        }
    }

    function modal_add_officer() {

        $("#add_officer")[0].reset();
        $('#add_officer').bootstrapValidator("resetForm", true);

        $('#modal_add_officer').modal('show');
    }

    function modal_add_agent() {
        $("#add_agent")[0].reset();
        $('#add_agent').bootstrapValidator("resetForm", true);

        $('#modal_add_agent').modal('show');
    }

    $('#full_name').keyup(function() {
        var panjang = this.value.length;

        if(panjang==100){
            $('#full_name').addClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').show();
        }else{
            $('#full_name').removeClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').hide();
        }
    });

    $('#alamat').keyup(function() {
        var panjang = this.value.length;

        if(panjang==200){
            $('#alamat').addClass( "is-invalid" );
            $('#mm2').hide();
            $('#mm3').show();
        }else{
            $('#alamat').removeClass( "is-invalid" );
            $('#mm2').hide();
            $('#mm3').hide();
        }
    });


    $('#short_name').keyup(function() {
        var panjang = this.value.length;

        if(panjang==50){
            $('#short_name').addClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').show();
        }else{
            $('#short_name').removeClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').hide();
        }
    });

    $('#city').select2({
        placeholder:"Silahkan Pilih"
    });
    $('#province').select2({
        placeholder:"Silahkan Pilih"
    });
    var _create_form = $("#form-new-customer");
    $('#city').on('change', function (v) {
        var _items='';
        loadingPage();
        $.ajax({
                type: 'GET',
                url: base_url + 'province/'+this.value,
                success: function (res) {
                var data = $.parseJSON(res);
                $.each(data, function (k,v) {
                    _items += "<option value='"+v.province_code+"'>"+v.province_name+"</option>";
                });

                $('#province').html(_items);
                endLoadingPage();
             }
        });
    });

    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    var text = `Pilih "Tambah Data" untuk kembali menambahkan data atau pilih "Lihat Data" untuk menampilkan data yang sudah di tambahkan`;
    var action = `  <button onclick="znClose()" type="button"
                        class="btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm">Tambah
                        Data</button>
                    <button onclick="znView()" type="button"
                        class="btn btn-info btn-elevate btn-pill btn-elevate-air btn-sm">Lihat
                        Data</button>`;
    function znClose() {
        znIconboxClose();
        $("#form-new-customer").data('bootstrapValidator').resetForm();
        $("#form-new-customer")[0].reset();

    }
    function znView() {
        znIconboxClose();
        loadNewPage('{{ route('customer_list_active') }}');
    }


    function simpan_customer(){
        loadingPage();

        if($('#full_name').val()===''){
            endLoadingPage();
            $( "#full_name" ).addClass( "is-invalid" );
            $('#mm').show();
            $('#ck').hide();
            return false;
        }else{
            $("#full_name").removeClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').hide();
        }
        if($('#short_name').val()===''){
            endLoadingPage();
            $( "#short_name" ).addClass( "is-invalid" );
            $('#mm1').show();
            $('#ck1').hide();
            return false;
        }else{
            $("#short_name").removeClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').hide();
        }
        if($('#short_name').val()===''){
            endLoadingPage();
            $( "#short_name" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#short_name").removeClass( "is-invalid" );

        }
        if($('#email').val()===''){
            endLoadingPage();
            $( "#email" ).addClass( "is-invalid" );
            $('#vv').hide();
            $('#vv1').hide();
            $('#ks').show();
            return false;
        }else{
            // var email=$('#email').val();
            // if (validateEmail(email)) {
            //    $("#email").removeClass( "is-invalid" );
            //   } else {
            //     endLoadingPage();
            //     $( "#email" ).addClass( "is-invalid" );
            //     $('#vv1').hide();
            //     $('#vv').show();
            //     $('#ks').hide();
            //     return false;
            //   }
        }
        if($('#phone').val()===''){
            endLoadingPage();
            $( "#phone" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#phone").removeClass( "is-invalid" );
        }

        if($('#city').val()===''){
            endLoadingPage();
            $( "#city" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#city").removeClass( "is-invalid" );
        }

        if($('#city').val()===''){
            endLoadingPage();
            $("#city").addClass( "is-invalid" );
            $('#mpp').show();
            return false;
        }else{
            $("#city").removeClass( "is-invalid" );
        }

        if($('#province').val()===''){
            endLoadingPage();
            $("#province").addClass( "is-invalid" );
            $('#mp1').show();
            return false;
        }else{
            $("#province").removeClass( "is-invalid" );

        }

        if($('#alamat').val()===''){
            endLoadingPage();
            $( "#alamat" ).addClass( "is-invalid" );
            $('#mm2').show();
            $('#mm3').hide();
            return false;
        }else{
            $('#mm2').hide();
            $('#mm3').hide();
            $("#alamat").removeClass( "is-invalid" );
        }

        if($('#img').val()===''){
        }else{
            var size=$('#img')[0].files[0].size;
            var extension=$('#img').val().replace(/^.*\./, '');
            if(size >= 2000002){
                endLoadingPage();
                swal.fire('info','Maximal size upload 2Mb','info');
                return false;

            }
            if(extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='PNG' && extension!='JPG' && extension!='JPEG'){
                endLoadingPage();
                swal.fire('info','Format upload jpeg , jpg , png','info');
                return false;
            }
        }


        if($('#cust_type').val()===''){
            endLoadingPage();
            $("#cust_type").addClass( "is-invalid" );
            $('#ct').show();
            return false;
        }else{
            $("#cust_type").removeClass( "is-invalid" );
        }

        if($('#agent').val()===''){
            endLoadingPage();
            $("#agent").addClass( "is-invalid" );
            $('#agt').show();
            return false;
        }else{
            $('#agt').hide();
            $("#agent").removeClass( "is-invalid" );
        }

        if($('#cust_group').val()===''){
            endLoadingPage();
            $("#cust_group").addClass( "is-invalid" );
            $('#cg').show();
            return false;
        }else{
            $("#cust_group").removeClass( "is-invalid" );
        }

        if($('#source_type').val()===''){
            endLoadingPage();
            $("#source_type").addClass( "is-invalid" );
            $('#ctype').show();
            return false;
        }else{
            $("#source_type").removeClass( "is-invalid" );
        }


        if ( $('#source_type').val() == '1' ) {

            if($('#select_agent').val()===''){
                endLoadingPage();
                $("#select_agent").addClass( "is-invalid" );
                $('#slagent').show();
                return false;
            }else{
                $('#slagent').hide();
                $("#select_agent").removeClass( "is-invalid" );
            }

        }


        if($('#is_branch').val()===''){
            endLoadingPage();
            $("#is_branch").addClass( "is-invalid" );
            $('#cbranch').show();
            return false;
        }else{
            $("#is_branch").removeClass( "is-invalid" );
        }

        if ( $('#is_branch').val() == '1' ) {
            if($('#cust_branch_id').val()===''){
                endLoadingPage();
                $("#cust_branch_id").addClass( "is-invalid" );
                $('#custbranch').show();
                return false;
            }else{
                $('#custbranch').hide();
                $("#cust_branch_id").removeClass( "is-invalid" );
            }
        }

        // $.ajax({
        //         type: 'GET',
        //         url: base_url + 'cek_email?id='+$('#email').val(),
        //         success: function (res) {
        //         var data = $.parseJSON(res);

        //         if(data!='null' && data!=null && data!=''){
        //             endLoadingPage();
        //             $( "#email" ).addClass( "is-invalid" );
        //             $('#vv1').show();
        //             $('#vv').hide();
        //             $('#ks').hide();
        //             return false;

        //         }else{
        //             $("#email").removeClass( "is-invalid" );
        //             $('#vv1').hide();
        //             $('#vv').hide();
        //             $('#ks').hide();

                    var _form_data = new FormData(_create_form[0]);
                    $.ajax({
                        type: 'POST',
                        url: base_url + 'insert_customer_baru',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            if( res.rc == 1 ){

                                endLoadingPage();
                                _create_form[0].reset();
                                znIconbox("Data Berhasil Disimpan",text,action);
                                /*
                                swal.fire({
                                    title: 'Info',
                                    text: "Customer Berhasil Disimpan",
                                    type: 'info',
                                    confirmButtonText: 'Tutup',
                                    reverseButtons: true
                                }).then(function(result){
                                    if (result.value) {
                                        loc ation.reload();
                                    }
                                });
                                */
                            } else {
                                endLoadingPage();
                                toastr.error('Terjadi Kesalahan');
                            }
                        }
                    }).done(function (msg) {
                        endLoadingPage();
                    }).fail(function (msg) {
                        endLoadingPage();
                        toastr.error('Terjadi Kesalahan');
                    });

        //         }


        //      }
        // });



    }
</script>

<!-- end:: Content -->

@stop
@section('script')
@stop
