<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <i class="la la-clipboard zn-icon-modal"></i>
                <h5 class="modal-title" style="margin-top: 10px !important;" id="exampleModalLongTitle">
                    Detail Customer
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-2">
                        <div id="img"></div>


                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleTextarea">Full Name</label>
                            <h5><div id="full_name"></div></h5>
                        </div>
                        <div class="form-group">
                            <label for="exampleTextarea">Short Name</label>
                            <h5><div id="short_name"></div></h5>
                        </div>
                        <div class="form-group">
                            <label for="exampleTextarea">Email</label>
                            <h5><div id="email"></div></h5>
                        </div>




                    </div>
                    <div class="col-3">
                            <div class="form-group">
                                    <label>Phone Number</label>
                                    <h5><div id="phone_no"></div></h5>
                                </div>
                            <div class="form-group">
                                    <label for="exampleTextarea">Customer Type</label>
                                    <h5><div id="cust_type"></div></h5>
                                </div>
                                <div class="form-group">
                                    <label for="exampleTextarea">Officer Name</label>
                                    <h5><div id="off_name"></div></h5>
                                </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="exampleTextarea">City</label>
                            <h5><div id="city"></div></h5>
                        </div>
                        <div class="form-group">
                            <label for="exampleTextarea">Province</label>
                            <h5><div id="province"></div></h5>
                        </div>
                        <div class="form-group">
                            <label for="exampleTextarea">Address</label>
                            <h5 style="line-height: 25px;"><div id="alamat"></div></h5>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
