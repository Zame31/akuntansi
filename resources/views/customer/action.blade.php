<script type="text/javascript">
    function setActive(id, isActive) {
        if (isActive == 'f') {
            var titleSwal = 'Non Active Customer';
            var textSwal = 'Anda yakin akan menonaktifkan customer ini?';
        } else {
            var titleSwal = 'Active Customer';
            var textSwal = 'Anda yakin akan mengaktifkan customer ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "GET",
                    url: '{{ route('customer.set_active') }}',
                    data: {
                        id: id,
                        active: isActive
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        console.log(data);
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                            location.reload();
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                });
            }
        });

    }
</script>