@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Customer </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        New Customer List </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            New Customer List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="row">
                        <div class="col-12">
                            <a href="#" onclick="kirimsemua_cust();" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a>
                            <a href="#" onclick="kirim_cust();" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a>
                            <a href="#" onclick="hapussemua_cust();" class="btn btn-pill btn-danger" style="color: white;">Delete All</a>
                            <a href="#" onclick="hapus_cust();" class="btn btn-pill btn-danger" style="color: white;">Delete Selected</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    <th width="30px">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label>
                                    </th>
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Customer Name</th>
                                    <th>Address</th>
                                    <th>Phone No</th>
                                    <th>Account Officer</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data)
                                @foreach($data as $item)
                                <tr>
                                <td>
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="{{$item->id}}" class="kt-group-checkable">
                                        <span></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" onclick="edit_cust({{$item->id}})" href="#"><i class="la la-edit"></i> Edit</a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->address}}</td>
                                <td>{{$item->phone_no}}</td>
                                <td>{{$item->definition}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->

</div>
</div>
<script type="text/javascript">
$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});
function edit_cust(id){
    loadingPage();
    window.location.href = base_url +'edit_customer?id=' + id;
    endLoadingPage();
}    
function kirimsemua_cust() {
        swal.fire({
           title: "Info",
           text: "Kirim Semua Approval",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAproval_cust?type=semua',
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);

                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });

                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });

   }

   function hapussemua_cust() {
        swal.fire({
           title: "Info",
           text: "Haous Semua Customer",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/hapusAproval_cust?type=semua',
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
   }

function kirim_cust() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin mengirim record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAproval_cust?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                    endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
    }
}

function hapus_cust() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin menghapus record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/hapusAproval_cust?type=satu',
                    data: {value: value},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);
                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }).done(function( res ) {
                    endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });
            }
        });
    }
}
</script>
@stop
