<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal_add_agent" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Agent </h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="add_agent" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="" id="id">
                    <div class="form-group">
                        <label for="full_name_officer">Full Name </label>
                        <input type="text" class="form-control" id="full_name_agent" name="full_name_agent">
                    </div>
                    <div class="form-group">
                        <label for="code_name_officer">Code Name</label>
                        <input type="text" class="form-control" id="code_name_agent" name="code_name_agent">
                    </div>
                    <div class="form-group">
                        <label for="address_officer">Address</label>
                        <textarea class="form-control" id="address_agent" rows="3" name="address_agent"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="phone_no_officer">Phone No</label>
                        <input type="text" class="form-control" id="phone_no_agent" name="phone_no_agent" onkeypress="return hanyaAngka(event)">
                    </div>
                    <div class="form-group">
                        <label for="email_officer">Email</label>
                        <input type="email_officer" class="form-control" id="email_agent" name="email_agent">
                    </div>
                    <div class="form-group">
                        <label for="single">Officer</label>
                        <div class="row col-12 align-select2">
                            <select class="form-control kt-select2 init-select2 parent_id" name="officer_agent" id="officer_agent">
                                @php
                                    if ( Auth::user()->user_role_id != 5 ) {
                                        $agentParent = \DB::table('ref_agent')
                                                ->where('company_id', Auth::user()->company_id)
                                                ->where('is_parent', 't')
                                                ->get();
                                    }
                                @endphp
                                    <option value="" selected disabled>Pilih officer</option>
                                @forelse ($agentParent as $item)
                                    <option value="{{ $item->id }}">{{ $item->full_name }}</option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button onclick="simpan_agent()" type="button" class="btn btn-success">Save</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

$(document).ready(function () {
    $("#add_agent").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            full_name_agent: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength: {
                        max: 150,
                        message: 'Silahkan isi maksimal 150 karakter'
                    },
                }
            },
            code_name_agent: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength: {
                        max: 5,
                        message: 'Silahkan isi maksimal 5 karakter'
                    },
                }
            },
            address_agent: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength: {
                        max: 255,
                        message: 'Silahkan isi maksimal 255 karakter'
                    },
                }
            },
            phone_no_agent: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength : {
                        min: 8,
                        max: 20,
                        message: 'Silahkan isi minimal 8 sampai 20 digit angka'
                    }
                }
            },
            email_agent: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    emailAddress: {
                        message: 'Format email tidak valid'
                    }
                }
            },
            officer_agent: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan pilih'
                    },
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

function simpan_agent(){
    var validateForm = $('#add_agent').data('bootstrapValidator').validate();

    if (validateForm.isValid()) {
        var formData = document.getElementById("add_agent");
        var _form_data = new FormData(formData);

        $.ajax({
            type: 'POST',
            url: base_url + 'insert_agent_baru',
            data: _form_data,
            processData: false,
            contentType: false,
            beforeSend: function () {
                loadingPage();
            },

            success: function (res) {

                if ( res.rc == 1 )  {
                    $('#modal_add_agent').modal('hide');
                    swal.fire({
                        title: 'Info',
                        text: "Agent Berhasil Disimpan",
                        type: 'info',
                        confirmButtonText: 'Tutup',
                        reverseButtons: true
                    }).then(function(result){
                        if (result.value) {
                            getAgentList();
                        }
                    });
                } else {
                    toastr.error('Terjadi Kesalahan');
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error('Terjadi Kesalahan');
        });
    }



}

function getAgentList() {
    $.ajax({
        type: 'GET',
        url: '{{route('get_list_agent')}}',
        beforeSend: function () {
            KTApp.block('#container-agent', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'danger',
                message: 'Processing...'
            });
        },
        success: function (msg) {
            console.log(msg);
            $('#select_agent').html(``);
            $('#select_agent').append(`<option value="">Silahkan Pilih</option>`);
            $.each(msg.rm, function (k,v) {
                $('#select_agent').append(`<option value="`+v.id+`">`+v.full_name+`</option>`);
            });
        }
    }).done(function (msg) {
        KTApp.unblock('#container-agent');
    }).fail(function (msg) {
        KTApp.unblock('#container-agent');
        toastr.info("Gagal, Koneksi ke server bermasalah");
    });
}
</script>
