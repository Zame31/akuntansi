@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Customer </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Customer Profile </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">


            <div class="row">
                <div class="col-3">

                    <div class="kt-portlet kt-portlet--height-fluid-">
                        <div class="kt-portlet__head  kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">

                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit-y">

                            <!--begin::Widget -->
                            <div class="kt-widget kt-widget--user-profile-1">
                                <div class="kt-widget__head">
                                    <div class="kt-widget__media">
                                        <img src="{{asset('upload_customer')}}/{{$data[0]->id}}_{{$data[0]->image_url}}" alt="image">
                                    </div>
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__section">
                                            <a href="#" class="kt-widget__username">
                                                {{$data[0]->full_name}}
                                                <i class="flaticon2-correct kt-font-success"></i>
                                            </a>
                                            <span class="kt-widget__subtitle">
                                                {{$data[0]->cust_type}}
                                            </span>
                                        </div>
                                        <div class="kt-widget__action">
                                            <button type="button" class="btn btn-info btn-sm">{{$data[0]->definition}}</button>

                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget__body">
                                    <div class="kt-widget__content">
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Short Name:</span>
                                            <span class="kt-widget__data">{{$data[0]->short_name}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Email:</span>
                                            <span class="kt-widget__data">{{$data[0]->email}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Phone:</span>
                                            <span class="kt-widget__data">{{$data[0]->phone_no}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">City:</span>
                                            <span class="kt-widget__data">{{$data[0]->city_name}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label">Province:</span>
                                            <span class="kt-widget__data">{{$data[0]->province_name}}</span>
                                        </div>
                                        <div class="kt-widget__info">
                                            <span class="kt-widget__label"
                                                style="vertical-align: top;height: 100px;">Address:</span>
                                            <span class="kt-widget__data"
                                                style="vertical-align: top;height: 100px;text-align: right;">{{$data[0]->address}}</span>
                                        </div>
                                    </div>
                                    <div class="kt-widget__items">
                                        <div class="row">
                                            <div class="col-12 ">
                                                <button onclick="editProfile()" type="button"
                                                    class="btn btn-info btn-elevate btn-pill" style="width: 100%;">
                                                    {{-- <i class="la la-edit"></i> --}}
                                                    Edit Profile</button>
                                            </div>
                                            <div class="col-12 mt-2 text-left">
                                                    <button onclick="loadNewPage('{{ route('customer_list_active') }}')" type="button"
                                                        class="btn btn-danger btn-elevate btn-pill" style="width: 100%;">
                                                        {{-- <i class="la la-angle-double-left"></i>  --}}
                                                        Kembali</button>
                                                </div>
                                        </div>
                                        {{-- <a href="#" class="kt-widget__item kt-widget__item--active"
                                            style="padding: 10px 20px;">
                                            <span class="kt-widget__section">
                                                <i class="la la-edit" style="font-size: 24px;"></i>
                                                <span class="kt-widget__desc">
                                                    Edit Profile
                                                </span>
                                            </span>
                                        </a> --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-9">
                    {{-- INSURANCE --}}
                    <div class="kt-portlet" id="insurance_view">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="flaticon-grid-menu"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    Master Policy Active
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <table class="table table-striped- table-hover table-checkable" id="table_id">
                                <thead>
                                    <tr>
                                        <th width="30px">Action</th>
                                        <th>ID</th>
                                        <th>Customer Name</th>
                                        <th>Branch Code</th>
                                        <th>Product Name</th>
                                        <th>Valuta</th>
                                        <th>Premi Amount</th>
                                        
                                        <th>Police Number</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div>
                    </div>


                    {{-- EDIT CUSTOMER --}}

                    <div class="kt-portlet" id="edit-profile">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="flaticon-grid-menu"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    Edit Profile
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-success" onclick="return update_customer()">Simpan</button>
                                        <button onclick="batalProfile()" class="btn btn-danger">Batal</button>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <form id="form-edit-customer" enctype="multipart/form-data">
                                <input type="hidden" name="id" id="idcust" value="{{$upd[0]->id}}">
                                <div class="row">

                                    <div class="col-2">
                                        <div class="form-group">
                                            <div class="kt-avatar kt-avatar--outline" id="kt_user_add_avatar">
                                                <div class="kt-avatar__holder" style="width: 160px;
                                                height: 160px;background-size: 160px 160px;background-image: url(../upload_customer/{{$upd[0]->id}}_{{$upd[0]->image_url}})"></div>
                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Change Foto">
                                                    <i class="fa fa-pen"></i>
                                                    <input type="file" name="img" id="img">
                                                </label>
                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Cancel Foto">
                                                    <i class="fa fa-times"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1"></div>
                                    <div class=" col-3">
                                        <div class="form-group">
                                            <label for="exampleTextarea">Full Name</label>
                                            <input type="text" class="form-control" onkeypress="return huruf(event)" id="full_name" name="full_name" maxlength="100" value="{{$upd[0]->full_name}}">
                                            <div class="invalid-feedback" id="mm">Silahkan isi full name</div>
                                            <div class="invalid-feedback" id="ck">Maximal 100 karakter</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Short Name</label>
                                            <input type="text" class="form-control" id="short_name" value="{{$upd[0]->short_name}}" onkeypress="return huruf(event)" name="short_name" maxlength="50">
                                            <div class="invalid-feedback" id="mm">Silahkan isi short name</div>
                                            <div class="invalid-feedback" id="ck1">Maximal 50 karakter</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Email</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">
                                                    <i class="flaticon2-email"></i></span>
                                                </div>
                                                <input type="text" class="form-control" name="email" id="email" value="{{$upd[0]->email}}" maxlength="100">
                                                 <div class="invalid-feedback" id="ks">Silahkan isi email</div>
                                                <div class="invalid-feedback" id="vv">Email tidak valid</div>
                                                <div class="invalid-feedback" id="vv1">Email sudah digunakan</div>
                                            </div>
                                            <span class="form-text text-muted">ex : example@email.com</span>
                                        </div>

                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">
                                                    <i class="la la-phone"></i></span>
                                                </div>
                                                {{-- <input type="text" class="form-control" id="phone" onkeypress="return hanyaAngka(event)" maxlength="15" name="phone" value="{{$upd[0]->phone_no}}"> --}}
                                                <textarea class="form-control" id="phone" maxlength="200" rows="2" name="phone" onkeypress="return hanyaAngka(event)">{{$upd[0]->phone_no}}</textarea>
                                                <div class="invalid-feedback" id="mm">Silahkan isi phone number</div>
                                            </div>
                                            <span class="form-text text-muted">ex : 085xxxxxxxxxx</span>
                                        </div>

                                        <div class="form-group">
                                            <label>Home Number</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">
                                                    <i class="la la-home"></i></span>
                                                </div>
                                                {{-- <input type="text" class="form-control" id="phone" onkeypress="return hanyaAngka(event)" maxlength="15" name="home_phone_number" value="{{$upd[0]->home_phone}}"> --}}
                                                <textarea class="form-control" id="home_phone_number" maxlength="200" rows="2" name="home_phone_number" onkeypress="return hanyaAngka(event)">{{$upd[0]->home_phone}}</textarea>
                                                <div class="invalid-feedback" id="mm">Silahkan isi home phone number</div>
                                            </div>
                                            <span class="form-text text-muted">ex : 022xxxxxxxxxx</span>
                                        </div>



                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="exampleTextarea">City</label>
                                            <select class="form-control kt-select2" name="city" id="city">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($city as $item1)
                                                <option value="{{$item1->city_code}}" @if($upd[0]->city_id==$item1->city_code) selected @endif>{{$item1->city_name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback" id="mpp">Silahkan pilih city</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Province</label>
                                            <select class="form-control kt-select2" name="province" id="province">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach($province as $item)
                                                <option value="{{$item->province_code}}" @if($upd[0]->province_id==$item->province_code) selected @endif>{{$item->province_name}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback" id="mp1">Silahkan pilih province</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Address</label>
                                            <textarea class="form-control" id="alamat" rows="8" name="alamat">{{$upd[0]->address}}</textarea>
                                            <div class="invalid-feedback" id="mm2">Silahkan isi address</div>
                                            <div class="invalid-feedback" id="mm3">Maximal 200 karakter</div>
                                        </div>

                                        <div class="form-group">
                                            <label>Fax Number</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">
                                                    <i class="la la-fax"></i></span>
                                                </div>
                                                {{-- <input type="text" class="form-control" id="fax_number" onkeypress="return hanyaAngka(event)" maxlength="15" name="fax_number" value="{{ $upd[0]->fax_no }}"> --}}
                                                <textarea class="form-control" id="fax_number" maxlength="200" rows="2" name="fax_number" onkeypress="return hanyaAngka(event)">{{ $upd[0]->fax_no }}</textarea>
                                                <div class="invalid-feedback" id="mm">Silahkan isi fax number</div>
                                            </div>
                                            <span class="form-text text-muted">ex : 022xxxxxxxxxx</span>
                                        </div>

                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="exampleTextarea">Customer Type</label>
                                            <select class="form-control kt-select2 init-select2" name="cust_type" id="cust_type">
                                                @foreach($cust_type as $item)
                                                <option value="{{$item->id}}" @if($upd[0]->cust_type_id==$item->id) selected @endif>{{$item->definition}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Account Officer</label>
                                            <select class="form-control kt-select2 init-select2" name="agent" id="agent">
                                                @php
                                                $agents = \DB::select('SELECT * FROM ref_agent where is_active=true AND is_parent=true order by seq asc');
                                            @endphp
                                              @foreach($agents as $item)
                                              <option value="{{$item->id}}" @if($data[0]->agent_id==$item->id) selected @endif>{{$item->full_name}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleTextarea">Customer Group</label>
                                            <select class="form-control kt-select2 init-select2" name="cust_group" id="cust_group">
                                                <option value="">Silahkan Pilih</option>
                                                @php
                                                     $cust = \DB::select('SELECT * FROM ref_cust_group');
                                                @endphp
                                                @foreach($cust as $item)
                                                <option value="{{$item->id}}" @if($data[0]->cust_group_id==$item->id) selected @endif>{{$item->group_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleTextarea">Source Type</label>
                                            <select class="form-control kt-select2 init-select2" name="source_type" id="source_type" onchange="append_input_agent(this)">
                                                <option value="">Silahkan Pilih</option>
                                                <option value="1" @if (!is_null($data[0]->ext_agent_id)) selected @endif> Agent </option>
                                                <option value="2" @if (is_null($data[0]->ext_agent_id)) selected @endif> Non Agent </option>
                                            </select>
                                            <div class="invalid-feedback" id="ctype">Silahkan pilih source type</div>
                                        </div>

                                        <div class="form-group" id="container-agent" @if (is_null($data[0]->ext_agent_id)) style="display: none;" @endif>
                                            <label for="exampleTextarea">Select Agent </label>
                                            <div id="container-agent" style="display: flex;">
                                                <select class="form-control kt-select2 init-select2" name="select_agent" id="select_agent">
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach($selectAgent as $item)
                                                        <option value="{{$item->id}}" @if($data[0]->ext_agent_id==$item->id) selected @endif>{{$item->full_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="invalid-feedback" id="slagent">Silahkan pilih agent </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleTextarea">Have a Branch</label>
                                            <select class="form-control kt-select2 init-select2" name="is_branch" id="is_branch" onchange="append_cust_branch(this)">
                                                <option value="">Silahkan Pilih</option>
                                                <option value="1" @if ($data[0]->is_branch) selected @endif> Yes </option>
                                                <option value="2" @if ( !$data[0]->is_branch || is_null($data[0]->is_branch) ) selected @endif> Non </option>
                                            </select>
                                            <div class="invalid-feedback" id="cbranch">Silahkan pilih pilihan</div>
                                        </div>

                                        <div class="form-group" id="container-cust-branch" @if (is_null($data[0]->cust_branch_id)) style="display: none;" @endif>
                                            <label for="exampleTextarea">Select Customer Branch </label>
                                            <div id="container-agent" style="display: flex;">
                                                <select class="form-control kt-select2 init-select2" name="cust_branch_id" id="cust_branch_id">
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach($custBranch as $item)
                                                        <option value="{{$item->id}}" @if($data[0]->cust_branch_id==$item->id) selected @endif>{{$item->branch_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="invalid-feedback" id="custbranch">Silahkan pilih customer branch </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('marketing.modal.modal_detail')
@include('marketing.modal.modal_detail')

<script>
///////ZAME/////
var act_url = '{{$routeTable}}';

var table = $('#table_id').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { targets: [6], className: 'text-center' },
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'id', name: 'id' },
        { data: 'full_name', name: 'full_name' },
        { data: 'short_code', name: 'short_code' },
        { data: 'definition', name: 'definition' },
        { data: 'mata_uang', name: 'mata_uang' },
        { data: 'premi_amount', name: 'premi_amount' },
        { data: 'polis_no', name: 'polis_no' },
        { data: 'invoice_type_id', name: 'invoice_type_id' },
        { data: 'stat', name: 'stat' }
    ]
});


function getDetailProduct(id,typeTable) {

    data_detail_product = [];

    console.log('id',id);
    console.log('typeTable',typeTable);

    if (typeTable == 'detail_cf') {
        var set_url =  base_url + 'marketing/data/detail_product_cf/' + id; 
    }
    else if (typeTable == 'detail_no_polis') {
        var set_url =  base_url + 'marketing/data/detail_product_no_polis/byget?polis_no=' + id; 
    }
    else if(typeTable == 'detail_product_list'){
        var set_url =  base_url + 'marketing/data/detail_product_list/' + id;
    }
    else{
        var set_url =  base_url + 'marketing/data/detail_product/' + id;
    }

    console.log(set_url);

    $.ajax({
        type: 'GET',
        url: set_url,
        beforeSend: function (res) {
            loadingPage();
        },
        success: function (res) {

            var dataDetail =res.rm;

            console.log(dataDetail);

            dataDetail.forEach((vv,i) => {
                id_array_data = i;
                temp_data = [];
                let rows = [];

                vv.forEach(data => {

                    console.log(data);

                    if (data.input_name == 'sts') {
                        if (data.value_text == 't') {
                            var sts = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Aktif</span>';
                        }else{
                            var sts = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" style="width: 70px;">Non Aktif</span>';
                        }
                        rows.unshift(sts);
                    }

                    var valueText = '';
                    if (data.is_currency) {
                        valueText = formatMoney(clearNumFormatDec(data.value_text));
                    }else{
                        valueText = data.value_text;
                    }

                    if (data.input_name != 'sts') {
                        rows.push(valueText);
                    }
                    

                    var item = {
                        "name" : data.input_name,
                        "value": valueText,
                        "bit_id" : data.bit_id,
                    };
                    temp_data.push(item);

                    
                });

                if (typeTable == 'normal' || typeTable == 'detail_cf' || typeTable == 'detail_no_polis') {
                    var action = `<button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flaticon-more"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" onclick="modal_add_detail('edit', '${id_array_data}')">
                                        <i class="la la-edit"></i> Edit
                                    </a>
                                    <a class="dropdown-item" onclick="hapus_detail('${id_array_data}', this)">
                                        <i class="la la-trash"></i> Hapus
                                    </a>`;

                    console.log(vv);                

                
                    rows.unshift(action);
                }

                data_detail_product.push({
                    id : id_array_data,
                    value : temp_data
                });

                DataTable.row.add(rows).draw();
            });
            
            endLoadingPage();

        }
    });
}

///////ZAME/////

    function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}

    function append_cust_branch(elm) {
        var value = $(elm).val();
        if ( value == '1' ) {
            // Agent
            $("#container-cust-branch").slideDown();
        } else {
            $("#container-cust-branch").slideUp();
        }
    }

    function append_input_agent(elm) {
        var value = $(elm).val();
        if ( value == '1' ) {
            // Agent
            $("#container-agent").slideDown();
            $("#btn_add_agent").removeClass('d-none');
        } else {
            $("#container-agent").slideUp();
            $("#btn_add_agent").addClass('d-none');
        }
    }

    $('#full_name').keyup(function() {
        var panjang = this.value.length;

        if(panjang==100){
            $('#full_name').addClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').show();
        }else{
            $('#full_name').removeClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').hide();
        }
    });

    $('#alamat').keyup(function() {
        var panjang = this.value.length;

        if(panjang==200){
            $('#alamat').addClass( "is-invalid" );
            $('#mm2').hide();
            $('#mm3').show();
        }else{
            $('#alamat').removeClass( "is-invalid" );
            $('#mm2').hide();
            $('#mm3').hide();
        }
    });


    $('#short_name').keyup(function() {
        var panjang = this.value.length;

        if(panjang==50){
            $('#short_name').addClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').show();
        }else{
            $('#short_name').removeClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').hide();
        }
    });
     $('#city').select2({
        placeholder:"Silahkan Pilih"
    });
    $('#province').select2({
        placeholder:"Silahkan Pilih"
    });
    var _create_form = $("#form-edit-customer");
    $('#city').on('change', function (v) {
        var _items='';
        loadingPage();
        $.ajax({
                type: 'GET',
                url: base_url + 'province/'+this.value,
                success: function (res) {
                var data = $.parseJSON(res);
                $.each(data, function (k,v) {
                    _items += "<option value='"+v.province_code+"'>"+v.province_name+"</option>";
                });

                $('#province').html(_items);
                endLoadingPage();
             }
        });
    });
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
     function update_customer(){
        loadingPage();
        if($('#full_name').val()===''){
            endLoadingPage();
            $( "#full_name" ).addClass( "is-invalid" );
            $('#mm').show();
            $('#ck').hide();
            return false;
        }else{
            $("#full_name").removeClass( "is-invalid" );
            $('#mm').hide();
            $('#ck').hide();
        }

        if($('#short_name').val()===''){
            endLoadingPage();
            $( "#short_name" ).addClass( "is-invalid" );
            $('#mm1').show();
            $('#ck1').hide();
            return false;
        }else{
            $("#short_name").removeClass( "is-invalid" );
            $('#mm1').hide();
            $('#ck1').hide();
        }

        if($('#short_name').val()===''){
            endLoadingPage();
            $( "#short_name" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#short_name").removeClass( "is-invalid" );
        }

        if($('#email').val()===''){
            endLoadingPage();
            $( "#email" ).addClass( "is-invalid" );
            $('#vv').hide();
            $('#vv1').hide();
            $('#ks').show();
            return false;
        }

        // else{
        //     var email=$('#email').val();
        //     if (validateEmail(email)) {
        //        $("#email").removeClass( "is-invalid" );
        //       } else {
        //         endLoadingPage();
        //         $( "#email" ).addClass( "is-invalid" );
        //         $('#vv').show();
        //         $('#ks').hide();
        //         $('#vv1').hide();
        //         return false;
        //       }
        // }
        if($('#phone').val()===''){
            endLoadingPage();
            $( "#phone" ).addClass( "is-invalid" );
            return false;
        }else{
            $("#phone").removeClass( "is-invalid" );

        }
        if($('#city').val()===''){
            endLoadingPage();
            $("#city").addClass( "is-invalid" );
            $('#mpp').show();
            return false;
        }else{
            $("#city").removeClass( "is-invalid" );

        }

        if($('#province').val()===''){
            endLoadingPage();
            $("#province").addClass( "is-invalid" );
            $('#mp1').show();
            return false;
        }else{
            $("#province").removeClass( "is-invalid" );

        }

        if($('#alamat').val()===''){
            endLoadingPage();
            $( "#alamat" ).addClass( "is-invalid" );
            $('#mm2').show();
            $('#mm3').hide();
            return false;
        }else{
            $('#mm2').hide();
            $('#mm3').hide();
            $("#alamat").removeClass( "is-invalid" );

        }

        if($('#img').val()===''){
        }else{
            var size=$('#img')[0].files[0].size;
            var extension=$('#img').val().replace(/^.*\./, '');
            if(size >= 2000002){
                endLoadingPage();
                swal.fire('info','Maximal size upload 2Mb','info');
                return false;

            }
            //[,'jpeg','jpg','png','xls','xlxs']
            if(extension!='jpeg' && extension!='jpg' && extension!='png' && extension!='PNG' && extension!='JPG' && extension!='JPEG'){
                endLoadingPage();
                swal.fire('info','Format upload jpeg , jpg , png','info');
                return false;
            }
        }

        if($('#cust_type').val()===''){
            endLoadingPage();
            $("#cust_type").addClass( "is-invalid" );
            $('#ct').show();
            return false;
        }else{
            $("#cust_type").removeClass( "is-invalid" );
        }

        if($('#agent').val()===''){
            endLoadingPage();
            $("#agent").addClass( "is-invalid" );
            $('#agt').show();
            return false;
        }else{
            $("#agent").removeClass( "is-invalid" );
        }

        if($('#cust_group').val()===''){
            endLoadingPage();
            $("#cust_group").addClass( "is-invalid" );
            $('#cg').show();
            return false;
        }else{
            $("#cust_group").removeClass( "is-invalid" );
        }

        if($('#source_type').val()===''){
            endLoadingPage();
            $("#source_type").addClass( "is-invalid" );
            $('#ctype').show();
            return false;
        }else{
            $("#source_type").removeClass( "is-invalid" );
        }

        if ( $('#source_type').val() == '1' ) {

            if($('#select_agent').val()===''){
                endLoadingPage();
                $("#select_agent").addClass( "is-invalid" );
                $('#slagent').show();
                return false;
            }else{
                $('#slagent').hide();
                $("#select_agent").removeClass( "is-invalid" );
            }

        }

        if($('#is_branch').val()===''){
            endLoadingPage();
            $("#is_branch").addClass( "is-invalid" );
            $('#cbranch').show();
            return false;
        }else{
            $("#is_branch").removeClass( "is-invalid" );
        }

        if ( $('#is_branch').val() == '1' ) {
            if($('#cust_branch_id').val()===''){
                endLoadingPage();
                $("#cust_branch_id").addClass( "is-invalid" );
                $('#custbranch').show();
                return false;
            }else{
                $('#custbranch').hide();
                $("#cust_branch_id").removeClass( "is-invalid" );
            }
        }


        //  $.ajax({
        //         type: 'GET',
        //         url: base_url + 'cek_email?id='+$('#email').val()+'&idcust='+$('#idcust').val(),
        //         success: function (res) {
        //         var data = $.parseJSON(res);

        //         if(data!='null' && data!=null && data!=''){
        //             endLoadingPage();
        //             $( "#email" ).addClass( "is-invalid" );
        //             $('#vv1').show();
        //             $('#vv').hide();
        //             $('#ks').hide();
        //             return false;

        //         }else{
        //             $("#email").removeClass( "is-invalid" );
        //             $('#vv1').hide();
        //             $('#vv').hide();
        //             $('#ks').hide();

                    var _form_data = new FormData(_create_form[0]);
                    $.ajax({
                        type: 'POST',
                        url: base_url + 'update_customer_baru',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            //if(res.rc==1){
                                endLoadingPage();
                                _create_form[0].reset();

                                swal.fire({
                                    title: 'Info',
                                    text: "Customer Berhasil Diupdate",
                                    type: 'info',
                                    confirmButtonText: 'Tutup',
                                    reverseButtons: true
                                }).then(function(result){
                                    if (result.value) {
                                        //window.location.href = base_url +'customer_list';
                                        location.reload();
                                    }
                                });
                            //}
                        }
                    });


        //         }


        //      }
        // });

    }
    $('#detail_insurance').hide();
    $('#edit-profile').hide();

    function editProfile() {
        animateCSS('#insurance_view', 'fadeOutDown', function () {
            $('#insurance_view').hide();
            $('#edit-profile').show();
        });

        animateCSS('#edit-profile', 'fadeInDown', function () {});
    }

    function batalProfile() {
        animateCSS('#edit-profile', 'fadeOutDown', function () {
            $('#edit-profile').hide();
            $('#insurance_view').show();
        });

        animateCSS('#insurance_view', 'fadeInDown', function () {});
    }

    function kembali() {
        animateCSS('#detail_insurance', 'fadeOutDown', function () {
            $('#detail_insurance').hide();
            $('#insurance_view').show();
        });

        animateCSS('#insurance_view', 'fadeInDown', function () {});
    }


    function detail_invoice(id,type){
    loadingPage();
    switch (type) {

        //    ORIGINAL
            case 0:
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY');
                $('#v_fee_internal').show();
                $('#v_agent_fee_amount').show();
                $('#v_tax_amount').show();
                $('#v_ins_fee').show();
                $('#v_cf_no').show();
                var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
            break;
        //    ENDORS
        case 1:
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY ENDORSMENT');
                $('#v_fee_internal').show();
                $('#v_agent_fee_amount').show();
                $('#v_tax_amount').show();
                $('#v_ins_fee').show();
                $('#v_cf_no').hide();
                var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
            break;
            case 'installment':
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY INSTALLMENT');
                $('#v_fee_internal').hide();
                $('#v_agent_fee_amount').hide();
                $('#v_tax_amount').hide();
                $('#v_ins_fee').hide();
                $('#v_cf_no').show();
                $('#label_police_duty').html('Polis Amount');
                $('#label_stamp_duty').html('Materai Amount');
                $('#label_adm_duty').html('Admin Amount');
                var set_url = base_url + 'marketing/data/detail_master_sales_polis/' + id;
            break;
            case 'installment_detail':
                $('#zn-invoice-tittle').html('DETAIL MASTER POLICY INSTALLMENT SCHEDULE');
                $('#v_fee_internal').hide();
                $('#v_agent_fee_amount').hide();
                $('#v_tax_amount').hide();
                $('#v_ins_fee').hide();
                $('#v_cf_no').show();
                $('#label_police_duty').html('Polis Amount');
                $('#label_stamp_duty').html('Materai Amount');
                $('#label_adm_duty').html('Admin Amount');
                var set_url = base_url + 'marketing/data/detail_master_sales_schedule_polis/' + id;
            break;
        }
        
    $.ajax({
        type: 'GET',
        url: set_url,
        success: function (res) {
                let data = res.rm;
                console.log(data);
            endLoadingPage();
            
            $('#cf_no').html(data.cf_no);
            $('#polis_no').html(data.polis_no);
            $('#police_name').html(data.police_name);
            $('#underwriter').html(data.underwriter);
            $('#customer').html(data.customer);
            $('#no_of_insured').html(data.no_of_insured);
            $('#mata_uang').html(data.mata_uang);
            $('#coc_no').html(data.cf_no);
            $('#coc_no').html(data.cf_no);
            
                $('#product').html(data.produk);
                $('#agent').html(data.agent);
                $('#start_date').html(formatDate(new Date(data.start_date_polis)));
                $('#end_date').html(formatDate(new Date(data.end_date_polis)));

                // NORMAL
                $('#ins_amount').html(formatMoney(data.ins_amount));
                $('#premi_amount1').html(formatMoney(data.premi_amount));
                $('#disc_amount').html(formatMoney(data.disc_amount));
                $('#net_amount').html(formatMoney(data.net_amount));
                $('#agent_fee_amount').html(formatMoney(data.agent_fee_amount));
                $('#tax_amount').html(formatMoney(data.tax_amount));
                $('#premi_amount').html(formatMoney(data.net_amount));
                $('#fee_internal').html(formatMoney(data.comp_fee_amount));
                
                $('#ins_fee').html(formatMoney(data.ins_fee));
                // NEW
                $('#stamp_duty').html(formatMoney(data.materai_amount));
                $('#police_duty').html(formatMoney(data.polis_amount));
                $('#adm_duty').html(formatMoney(data.admin_amount));
                // NEW

                detail_invoice_product(data.product_id,data.id);
            
            $('#detail').modal('show');
        }
    });
}


function detail_invoice_product(prod_id,id_sales) {
       

       $.ajax({
           type: "GET",
           url: base_url + '/quotation_slip/get_detail_insured/' + prod_id,
           data: {},

           beforeSend: function () {
               // loadingPage();
           },

           success: function (res) {
               var data = res.data;

               console.log('data detail product',data);


               if (res.rc == 1) {
                   var tableHeaders = "";
                   tableHeaders = `<th class="text-center"> Status </th>`;
                   data.forEach(function ( value, index) {
                       let isShow = (value.is_showing) ? 'all':'none';
                       
                       tableHeaders += `<th class="${isShow} text-center">${value.label}</th>`;
                   })
                   $("#tableDiv").empty();

                   if (data.length > 0) {
                       $("#tableDiv").append('<table id="table_detail" class="table table-striped- table-hover table-checkable"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                       DataTable = $('#table_detail').DataTable({responsive: true});


                       if (id_sales) {
                           getDetailProduct(id_sales,'detail_product_list');
                       }
                   }

                
                   
                 
               } else {
                   toastr.error(data.rm);
               }
           }
       }).done(function (msg) {
           // endLoadingPage();
       }).fail(function (msg) {
           // endLoadingPage();
           toastr.error("Terjadi Kesalahan");
       });
}


// function detail_invoice(id){
//     loadingPage();
//     $.ajax({
//         type: 'GET',
//         url: base_url + 'detail_invoice?id='+id,
//         success: function (res) {
//             var data = $.parseJSON(res);
//             console.log(data);
//             endLoadingPage();

//             $('#zn_stat').val(data[0].wf_status_id);
//             $('#zn_paid_stat').val(data[0].paid_status_id);

//             var getWf = data[0].wf_status_id;
//             var getPaid = data[0].paid_status_id;

//             if (getPaid == 0) {
//                 $("#setWord").attr("href", base_url+'printwordSc/normal/aktif/'+id);
//             }else {
//                 $("#setWord").attr("href", base_url+'printwordSc/normal/paid/'+id);
//             }


//         $('#tgl_tr').html(formatDate(new Date(data[0].inv_date)));
//             $('#inv_no').html(data[0].inv_no);
//             $('#product').html(data[0].produk);
//             $('#agent').html(data[0].agent);
//             $('#start_date').html(formatDate(new Date(data[0].start_date)));
//             $('#end_date').html(formatDate(new Date(data[0].end_date)));
//             $('#quotation').html(data[0].quotation);
//             $('#proposal').html(data[0].proposal);
//             $('#segment').html(data[0].segment);
//             $('#wf_status').html("Status "+data[0].wf_status);
//             $('#user_maker').html(data[0].user_create);
//             $('#user_approval').html(data[0].user_approval);

//             $('#ins_amount').html(formatMoney(data[0].ins_amount));

//             $('#premi_amount1').html(formatMoney(data[0].premi_amount));

//             $('#disc_amount').html(formatMoney(data[0].disc_amount));

//             $('#net_amount').html(formatMoney(data[0].net_amount));
//             $('#agent_fee_amount').html(formatMoney(data[0].agent_fee_amount));
//             $('#comp_fee_amount').html(formatMoney(data[0].tax_amount));

//             $('#premi_amount').html(formatMoney(data[0].net_amount));

//             $('#bank').html(data[0].bank);
//             $('#zn_stat').val(data[0].id_workflow);
//             $('#zn_paid_stat').val(data[0].paid_status_id);
//             $('#get_id').val(data[0].id);

//             // CETAK AKTIF
//             if (data[0].inv_no == null) {
//                 $('#ca_inv').html(' NO: ');
//             }else {
//                 $('#ca_inv').html(' NO: '+data[0].inv_no);
//             }
//             $('#ca_address').html(data[0].c_address);
//             $('#ca_product').html(': '+data[0].produk);
//             $('#ca_insured').html(': '+data[0].full_name);
//             $('#ca_polis').html(': '+data[0].polis_no);
//             $('#ca_underwriter').html(': '+data[0].underwriter);
//             var new_periode = formatDateNew(new Date(data[0].start_date));
//             var end_periode = formatDateNew(new Date(data[0].end_date));
//             $('#ca_periode').html(': '+new_periode+' to '+end_periode);

//             if (data[0].valuta_id != 1) {
//                premi = premi/data[0].kurs_today;
//                new_adm = (new_adm/data[0].kurs_today).toFixed(2);
//                total_fix = (total_fix/data[0].kurs_today).toFixed(2);
//                $('#ca_sumins').html(': '+data[0].mata_uang+' '+ formatMoney(data[0].ins_amount/data[0].kurs_today));
//                $('.mata_uang_set').html(data[0].mata_uang);


//            }else {
//                $('.mata_uang_set').html(data[0].mata_uang);
//                $('#ca_sumins').html(': '+data[0].mata_uang+' '+ formatMoney(data[0].ins_amount));

//            }
//            console.log(data[0].mata_uang);
//             //$('#ca_sumins').html(': '+ formatMoney(data[0].ins_amount));

//             //var premi = parseFloat(data[0].premi_amount);
//             if(data[0].valuta_id!=1){
//                 var premi = parseFloat(data[0].net_amount/data[0].kurs_today);
//             }else{
//                 var premi = parseFloat(data[0].net_amount);
//             }

//             // console.log(premi);

//             premi = premi.toFixed(2);



//             $('#ca_premi_amount').html(premi.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

//             $('#ca_user_approval').html(data[0].user_approval);
//             $('#ca_com').html(data[0].full_name);
//             $('#ca_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
//             $('#ca_ttd').html(data[0].leader_name);
//             $('#ca_ttd_jab').html(data[0].jabatan);

//             if(data[0].valuta_id!=1){
//                 var new_adm = (parseInt(data[0].polis_amount) + parseInt(data[0].materai_amount) + parseInt(data[0].admin_amount)) / data[0].kurs_today;
//             }else{
//                 var new_adm = parseInt(data[0].polis_amount) + parseInt(data[0].materai_amount) + parseInt(data[0].admin_amount);
//             }


//             console.log(new_adm);

//             new_adm = new_adm.toFixed(2);

//             $('#ca_adm').html(new_adm.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

//             if(data[0].valuta_id!=1){
//                 var new_total =  parseFloat(new_adm) + parseFloat(premi);
//             }else{
//                 var new_total =  parseFloat(new_adm) + parseFloat(premi);
//             }



//             var sisa_desimal = 0;


//             if ((new_total % 1) > 0.5) {
//                 sisa_desimal = (1.00 - (new_total % 1).toFixed(2)).toFixed(2);
//                 new_total = Math.ceil(new_total);

//             }else {
//                 sisa_desimal = (new_total % 1).toFixed(2);

//                 new_total = Math.floor(new_total);


//             }

//             $('#sisa_desimal').html("("+sisa_desimal+")");

//             // console.log(Math.ceil(new_total));
//             // console.log(Math.floor(new_total));


//             new_total = new_total.toFixed(2);
//            var total_fix = Math.round(new_total);
//            var total_terbilang_fix = Math.round(new_total);

//             console.log(total_fix);


//             $('#ca_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//             $('#ca_grand_total').html(new_total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
//             $('#ca_terbilang').html('In Words (Indonesia) : # '+terbilang(total_fix)+' '+data[0].deskripsi);

//             // CETAK PAID

//             if(data[0].valuta_id!=1){
//                 var new_premi = data[0].net_amount/data[0].kurs_today;
//             }else{
//                 var new_premi = data[0].net_amount;
//             }

//             if ((new_premi % 1) > 0.5) {
//                 new_premi = Math.ceil(new_premi);

//             }else {
//                 new_premi = Math.floor(new_premi);
//             }



//             if (data[0].kwitansi_no == null) {
//                 $('#pa_inv').html(' NO: ');
//             }else {
//                 $('#pa_inv').html(' NO: '+data[0].kwitansi_no);
//             }

//             var premi_des = new_premi.toFixed(2);

//             console.log(data);


//             $('#pa_agent').html(data[0].polis_no+' - '+data[0].full_name);
//             $('#pa_product').html(data[0].produk);

//             $('#pa_premi_amount').html(data[0].mata_uang+' '+total_fix.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

//             $('#pa_user_approval').html(data[0].user_approval);
//             $('#pa_terbilang').html(terbilang(total_terbilang_fix) +' '+ data[0].deskripsi);
//             $('#pa_date_ttd').html(formatDate(new Date("{{date('Y-m-d')}}")));
//             $('#pa_ttd').html(data[0].leader_name);
//             $('#pa_ttd_jab').html(data[0].jabatan);



//             if (data[0].valuta_id != 1) {

//                $('#zn-tittle-premi-lang').html('NETT PREMI AMOUNT');

//                $('#tittle_lang').html(data[0].mata_uang+' AMOUNT');
//                $('#ins_amount_lang').html(formatMoney(data[0].ins_amount/data[0].kurs_today));
//                $('#premi_amount1_lang').html(formatMoney(data[0].premi_amount/data[0].kurs_today));
//                $('#disc_amount_lang').html(formatMoney(data[0].disc_amount/data[0].kurs_today));
//                $('#net_amount_lang').html(formatMoney(data[0].net_amount/data[0].kurs_today));
//                $('#agent_fee_amount_lang').html(formatMoney(data[0].agent_fee_amount/data[0].kurs_today));
//                $('#comp_fee_amount_lang').html(formatMoney(data[0].tax_amount/data[0].kurs_today));
//                $('#premi_amount_lang').html(formatMoney(data[0].net_amount/data[0].kurs_today));

//            }
//             $('#detail').modal('show');




//         }
//     });
// }

    // function showDetail(id) {

    //     animateCSS('#insurance_view', 'fadeOutDown', function () {

    //         loadingPage();
    //         $.ajax({
    //             type: 'GET',
    //             url: base_url + 'detail_invoice?id='+id,
    //             success: function (res) {
    //                 var data = $.parseJSON(res);
    //                 console.log(data);
    //                 endLoadingPage();
    //                 $('#tgl_tr').html(formatDate(new Date(data[0].inv_date)));
    //                 $('#inv_no').html(data[0].inv_no);
    //                 $('#product').html(data[0].produk);
    //                 $('#agent').html(data[0].agent);
    //                 $('#start_date').html(formatDate(new Date(data[0].start_date)));
    //                 $('#end_date').html(formatDate(new Date(data[0].end_date)));
    //                 $('#quotation').html(data[0].quotation);
    //                 $('#proposal').html(data[0].proposal);
    //                 $('#segment').html(data[0].segment);

    //                 $('#ins_amount').html(data[0].ins_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    //                 $('#disc_amount').html(data[0].disc_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    //                 $('#net_amount').html(data[0].net_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    //                 $('#agent_fee_amount').html(data[0].agent_fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    //                 $('#comp_fee_amount').html(data[0].comp_fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    //                 $('#premi_amount').html(data[0].premi_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

    //                 $('#bank').html(data[0].bank);

    //                 //$('#detail').modal('show');
    //                 $('#insurance_view').hide();
    //                 $('#detail_insurance').show();
    //             }
    //         });


    //     });

    //     animateCSS('#detail_insurance', 'fadeInDown', function () {});

    // }

    function formatDate(date) {
      var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
      ];

      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();

      return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }


</script>
@stop
