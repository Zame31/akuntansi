@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Customer </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Approval Customer </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Approval Customer
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <a href="#" onclick="kirimsemua_app();" class="btn btn-pill btn-primary"
                                    style="color: white;">Approval All</a>
                                <a href="#" onclick="kirim_app();" class="btn btn-pill btn-primary"
                                    style="color: white;">Approval Selected</a>
                                <a href="#" onclick="hapussemua_app();" class="btn btn-pill btn-danger"
                                    style="color: white;">Reject All</a>
                                <a href="#" onclick="hapus_app();" class="btn btn-pill btn-danger"
                                    style="color: white;">Reject Selected</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                        <thead>
                            <tr>
                                <th width="30px">
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="" id="example-select-all" class="kt-group-checkable">
                                        <span></span>
                                    </label>
                                </th>
                                <th width="30px">Action</th>
                                <th>ID</th>
                                <th>Customer Name</th>
                                <th>Address</th>
                                <th>Phone No</th>
                                <th>Officer Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data)
                                @foreach($data as $item)
                                <tr>
                                <td>
                                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" value="{{$item->id}}" class="kt-group-checkable">
                                        <span></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#" onclick="det_customer('{{$item->id}}');"><i
                                                class="la la-clipboard"></i> Detail</a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->address}}</td>
                                <td>{{$item->phone_no}}</td>
                                <td>{{$item->definition}}</td>
                                </tr>
                                @endforeach
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <!-- end:: Subheader -->

    </div>
</div>
<script type="text/javascript">
$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});
function kirimsemua_app() {
        swal.fire({
           title: "Info",
           text: "Approve Semua Data",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
               
            swal.fire({
            title: 'Catatan Approve',
            html:"<div class='b'></div><input id='swal-input2' class='swal2-input form-control' maxlength='100' required/><div class='invalid-feedback'>Silahkan isi Catatan</div>",
            preConfirm: () => {
              if (document.getElementById('swal-input2').value) {
                 // Handle return value 
                 //$("#swal-input2").removeClass( "is-invalid" );
              } else {
                Swal.showValidationMessage('Silahkan Isi Catatan')   
                //$( "#swal-input2" ).addClass( "is-invalid" );
              }
            },
            confirmButtonText: 'Save'   
            }).then((result) => {
               loadingPage(); 
               var cct=$('#swal-input2').val();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimAproval_app?type=semua',
                    data: {cct: cct},
                    async: false,
                    success: function (res) {

                        endLoadingPage();
                        console.log(res['data']);

                        swal.fire({
                            title: 'Info',
                            text: "Berhasil",
                            type: 'success',
                            confirmButtonText: 'Tutup',
                            reverseButtons: true
                        }).then(function(result){
                            if (result.value) {
                                location.reload();
                            }
                        });

                    }
                }).done(function( res ) {
                     endLoadingPage();

                }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                }); 

            });
            }
        });

   }

   function hapussemua_app() {
        swal.fire({
           title: "Info",
           text: "Reject Semua data",
           type: "info",
           showCancelButton: true,
           confirmButtonColor: "#e6b034",
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {

                swal.fire({
                title: 'Catatan Reject',
                html:"<div class='b'></div><input id='swal-input2' class='swal2-input form-control' maxlength='100' required/><div class='invalid-feedback'>Silahkan isi Catatan</div>",
                preConfirm: () => {
                  if (document.getElementById('swal-input2').value) {
                     // Handle return value 
                     //$("#swal-input2").removeClass( "is-invalid" );
                  } else {
                    Swal.showValidationMessage('Silahkan Isi Catatan')   
                    //$( "#swal-input2" ).addClass( "is-invalid" );
                  }
                },
                confirmButtonText: 'Save'   
                }).then((result) => {
                   loadingPage(); 
                   var cct=$('#swal-input2').val();
                    $.ajax({
                        type: 'POST',
                        url: base_url + '/hapusAproval_app?type=semua',
                        data: {cct: cct},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    }); 

                });

            }
        });
   }

function kirim_app() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin approve record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {

                swal.fire({
                title: 'Catatan Approve',
                html:"<div class='b'></div><input id='swal-input2' class='swal2-input form-control' maxlength='100' required/><div class='invalid-feedback'>Silahkan isi Catatan</div>",
                preConfirm: () => {
                  if (document.getElementById('swal-input2').value) {
                     // Handle return value 
                     //$("#swal-input2").removeClass( "is-invalid" );
                  } else {
                    Swal.showValidationMessage('Silahkan Isi Catatan')   
                    //$( "#swal-input2" ).addClass( "is-invalid" );
                  }
                },
                confirmButtonText: 'Save'   
                }).then((result) => {
                   loadingPage(); 
                   var cct=$('#swal-input2').val();
                    $.ajax({
                        type: 'POST',
                        url: base_url + '/kirimAproval_app?type=satu',
                        data: {value: value,cct: cct},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    }); 

                });
            }
        });
    }
}

function hapus_app() {
    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal.fire("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        swal.fire({
            title: "Informasi",
            text: "Anda yakin ingin reject record yang dipilih?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {
                swal.fire({
                title: 'Catatan Reject',
                html:"<div class='b'></div><input id='swal-input2' class='swal2-input form-control' maxlength='100' required/><div class='invalid-feedback'>Silahkan isi Catatan</div>",
                preConfirm: () => {
                  if (document.getElementById('swal-input2').value) {
                     // Handle return value 
                     //$("#swal-input2").removeClass( "is-invalid" );
                  } else {
                    Swal.showValidationMessage('Silahkan Isi Catatan')   
                    //$( "#swal-input2" ).addClass( "is-invalid" );
                  }
                },
                confirmButtonText: 'Save'   
                }).then((result) => {
                   loadingPage(); 
                   var cct=$('#swal-input2').val();
                    $.ajax({
                        type: 'POST',
                        url: base_url + '/hapusAproval_app?type=satu',
                        data: {value: value,cct: cct},
                        async: false,
                        success: function (res) {

                            endLoadingPage();
                            console.log(res['data']);

                            swal.fire({
                                title: 'Info',
                                text: "Berhasil",
                                type: 'success',
                                confirmButtonText: 'Tutup',
                                reverseButtons: true
                            }).then(function(result){
                                if (result.value) {
                                    location.reload();
                                }
                            });

                        }
                    }).done(function( res ) {
                         endLoadingPage();

                    }).fail(function(res) {
                        endLoadingPage();
                        swal.fire("Error","Terjadi Kesalahan!","error");
                    }); 

                });
            }
        });
    }
}

function det_customer(id){
    loadingPage();
    $.ajax({
        type: 'GET',
        url: base_url + 'det_cust?id='+id,
        success: function (res) {
            var data = $.parseJSON(res);
            console.log(data);
            endLoadingPage();
            $('#full_name').html(data[0].full_name);
            $('#short_name').html(data[0].short_name);
            $('#email').html(data[0].email);
            $('#phone_no').html(data[0].phone_no);
            $('#cust_type').html(data[0].cust_type);
            $('#off_name').html(data[0].definition);
            $('#city').html(data[0].city_name);
            $('#province').html(data[0].province_name);
            $('#alamat').html(data[0].address);
            $('#img').html('<img src="'+base_url+'upload_customer/'+data[0].id+'_'+data[0].image_url+'" style="width: 100px;border-radius: 5px;">');

            $('#detail').modal('show');
           
        }
    });
}
</script>
@include('customer.detail')
@stop
