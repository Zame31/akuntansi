@section('content')
<div class="app-content">
<div class="section">

    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Customer </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Non Active Customer List </a>
                    </div>
            </div>
        </div>
    </div>

    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-portlet--head-lg">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon-grid-menu"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                            Non Active Customer List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                        {{-- <div class="row">
                            <div class="col-12">
                                <a href="#" onclick="kirimsemua();" class="btn btn-pill btn-primary" style="color: white;">Send Approval All</a>
                                <a href="#" onclick="kirim();" class="btn btn-pill btn-primary" style="color: white;">Send Approval Selected</a>
                                <a href="#" onclick="hapussemua();" class="btn btn-pill btn-danger" style="color: white;">Delete All</a>
                                <a href="#" onclick="hapus();" class="btn btn-pill btn-danger" style="color: white;">Delete Selected</a>
                            </div>
                        </div> --}}
                    </div>
            </div>
            <div class="kt-portlet__body">

                    <table class="table table-striped- table-hover table-checkable" id="table_id">
                            <thead>
                                <tr>
                                    <th width="30px">Action</th>
                                    <th>ID</th>
                                    <th>Customer Name</th>
                                    <th>Address</th>
                                    <th>Phone No</th>
                                    <th>Account Officer</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data)
                                @foreach($data as $item)
                                <tr>
                                <td>
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon-more"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" onclick="loadNewPage('{{ route('customer_detail_non_active',$item->id) }}')" data-toggle="modal" data-target="#detail"><i
                                                class="la la-clipboard"></i> Detail</a>
                                            <a class="dropdown-item" onclick="setActive('{{ $item->id }}', 't')" data-toggle="modal" data-target="#detail">
                                                <i class="la la-check-circle-o"></i> Active </a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->address}}</td>
                                <td>{{$item->phone_no}}</td>
                                <td>{{$item->definition}}</td>
                            </tr>
                                @endforeach
                                @endif
                                </tbody>

                        </table>
            </div>
        </div>
    </div>


    <!-- end:: Subheader -->
    @include('customer.action')
</div>
</div>
@stop
@section('script')
@stop
