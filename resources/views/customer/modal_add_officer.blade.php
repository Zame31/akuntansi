<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal_add_officer" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Account Officer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="add_officer" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="" id="id">
                    <div class="form-group">
                        <label for="full_name_officer">Full Name </label>
                        <input type="text" class="form-control" id="full_name_officer" name="full_name_officer">
                    </div>
                    <div class="form-group">
                        <label for="code_name_officer">Code Name</label>
                        <input type="text" class="form-control" id="code_name_officer" name="code_name_officer">
                    </div>
                    <div class="form-group">
                        <label for="address_officer">Address</label>
                        <textarea class="form-control" id="address_officer" rows="3" name="address_officer"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="phone_no_officer">Phone No</label>
                        <input type="text" class="form-control" id="phone_no_officer" name="phone_no_officer" onkeypress="return hanyaAngka(event)">
                    </div>
                    <div class="form-group">
                        <label for="email_officer">Email</label>
                        <input type="email_officer" class="form-control" id="email_officer" name="email_officer">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button onclick="simpan_officer()" type="button" class="btn btn-success">Save</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>

$(document).ready(function () {
    $("#add_officer").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            full_name_officer: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength: {
                        max: 150,
                        message: 'Silahkan isi maksimal 150 karakter'
                    },
                }
            },
            code_name_officer: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength: {
                        max: 5,
                        message: 'Silahkan isi maksimal 5 karakter'
                    },
                }
            },
            address_officer: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength: {
                        max: 255,
                        message: 'Silahkan isi maksimal 255 karakter'
                    },
                }
            },
            phone_no_officer: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    stringLength : {
                        min: 8,
                        max: 20,
                        message: 'Silahkan isi minimal 8 sampai 20 digit angka'
                    }
                }
            },
            email_officer: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan isi'
                    },
                    emailAddress: {
                        message: 'Format email tidak valid'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

function simpan_officer(){
    var validateForm = $('#add_officer').data('bootstrapValidator').validate();

    if (validateForm.isValid()) {
        var formData = document.getElementById("add_officer");
        var _form_data = new FormData(formData);

        $.ajax({
            type: 'POST',
            url: base_url + 'insert_officer_baru',
            data: _form_data,
            processData: false,
            contentType: false,
            beforeSend: function () {
                loadingPage();
            },

            success: function (res) {

                if ( res.rc == 1 )  {
                    $('#modal_add_officer').modal('hide');
                    swal.fire({
                        title: 'Info',
                        text: "Account Officer Berhasil Disimpan",
                        type: 'info',
                        confirmButtonText: 'Tutup',
                        reverseButtons: true
                    }).then(function(result){
                        if (result.value) {
                            getOfficerList();
                        }
                    });
                } else {
                    toastr.error('Terjadi Kesalahan');
                }
            }
        }).done(function (msg) {
            endLoadingPage();
        }).fail(function (msg) {
            endLoadingPage();
            toastr.error('Terjadi Kesalahan');
        });
    }



}

function getOfficerList() {
    $.ajax({
        type: 'GET',
        url: '{{route('get_list_officer')}}',
        beforeSend: function () {
            KTApp.block('#container-officer', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'danger',
                message: 'Processing...'
            });
        },
        success: function (msg) {
            console.log(msg);
            $('#agent').html(``);
            $('#agent').append(`<option value="">Silahkan Pilih</option>`);
            $.each(msg.rm, function (k,v) {
                $('#agent').append(`<option value="`+v.id+`">`+v.full_name+`</option>`);
            });
        }
    }).done(function (msg) {
        KTApp.unblock('#container-officer');
    }).fail(function (msg) {
        KTApp.unblock('#container-officer');
        toastr.info("Gagal, Koneksi ke server bermasalah");
    });
}
</script>
